﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using SKF.RS.MicrologInspector.Packets;
using SKF.RS.MicrologInspector.Database;
using System.Collections.Generic;


namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// This class decompresses and desrializes the received XML
   /// packets, and then writes the returned object to the database
   /// </summary>
   public class UpdateDB
   {
      #region Private Fields

      private DataConnect mSqlConnect = null;
      private IDataProgress mOverallProgress = null;
      private XmlDeserializer mDeserializer = null;

      #endregion


      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iSqlConnect">Reference to database connection</param>
      /// <param name="iDeserializer">Reference to xml deserializer</param>
      /// <param name="iOverall">Reference to "overall" progress bar</param>
      public UpdateDB( DataConnect iSqlConnect,
          XmlDeserializer iDeserializer,
          IDataProgress iOverall )
      {
         mSqlConnect = iSqlConnect;
         mDeserializer = iDeserializer;
         mOverallProgress = iOverall;

      }/* end constructor */


      #region Clear and reset tables

      /// <summary>
      /// Clear all download profile tables
      /// </summary>
      public Boolean ClearDownloadProfileTables()
      {
         Boolean result = mSqlConnect.Clear.DownloadProfile();

         // raise a progress event
         mOverallProgress.Update( true );

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Clear the current profile flags
      /// </summary>
      public Boolean ClearCurrentProfileFlags()
      {
         Boolean result = mSqlConnect.Clear.CurrentProfileFlags();

         // raise a progress event
         mOverallProgress.Update( true );

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Decompress the received packet, then clear and populate 
      /// the five CMMS tables
      /// </summary>
      /// <param name="iStateObject">state object with cmms object</param>
      /// <returns>true if success, else false</returns>
      public Boolean UpdateCMMSTables( ClientStateObject iStateObject )
      {
         // clear the current CMMS tables
         if ( !mSqlConnect.Clear.DownloadCmms() )
            return false;

         // decompress the raw packet
         iStateObject.ReadBuffer =
             iStateObject.CmmsOptionsTableCompressed.Decompress(
             iStateObject.CmmsOptionsTableCompressed.ByteData );

         // deserialize the decompressed packet
         PacketCmmsSettings cmmsSettings = 
                PacketBase.DeserializeObject<PacketCmmsSettings>( iStateObject.ReadBuffer );

         // populate the corrective action table
         if ( !mSqlConnect.Populate.CmmsCorrectiveActionTable(
             cmmsSettings.CMMS.CorrectiveActionsTable ) )
            return false;

         // raise a progress event
         mOverallProgress.Update( true );

         // populate the priority table
         if ( !mSqlConnect.Populate.CmmsPriorityTable(
             cmmsSettings.CMMS.PrioritiesTable ) )
            return false;

         // raise a progress event
         mOverallProgress.Update( true );

         // populate the problems table
         if ( !mSqlConnect.Populate.CmmsProblemTable(
             cmmsSettings.CMMS.ProblemsTable ) )
            return false;

         // raise a progress event
         mOverallProgress.Update( true );

         // populate the work type table
         if ( !mSqlConnect.Populate.CmmsWorkTypeTable(
             cmmsSettings.CMMS.WorkTypesTable ) )
            return false;

         // raise a progress event
         mOverallProgress.Update( true );

         // populate the work notifications table
         if ( !mSqlConnect.Populate.CmmsWorkNotificationTable(
             cmmsSettings.CMMS.WorkNotifications ) )
            return false;

         // raise a progress event
         mOverallProgress.Update( true );

         // if we reached this far then return true!
         return true;

      }/* end method */


      /// <summary>
      /// Resets all saved media upload flags
      /// </summary>
      public void UpdateSavedMedia( )
      {
         mSqlConnect.SetAllMediaFileUploadFlags();

      }/* end method */

      #endregion


      #region Deserialize packets and update database

      /// <summary>
      /// Decompress the received packet and populate the CODEDNOTES table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateCodedNotesTable( ClientStateObject iStateObject )
      {
         try
         {
            // clear the coded notes table and fail on error
            if ( !mSqlConnect.Clear.C_NOTE() )
            {
               return false;
            }

            if ( iStateObject.CodedNotesCompressed != null )
            {
               // decompress the raw packet
               iStateObject.ReadBuffer =
                   iStateObject.CodedNotesCompressed.Decompress(
                   iStateObject.CodedNotesCompressed.ByteData );

               // deserialize the decompressed packet
               EnumCodedNotes codedNotes =
                mDeserializer.DeserializeCodedNotes( iStateObject.ReadBuffer );

               // populate the table
               Boolean result = mSqlConnect.Populate.CodedNotesTable( codedNotes );

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the CONDITIONALPOINT table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateConditionalPointTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.ConditionalPointsCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.ConditionalPointsCompressed.DecompressToXmlFile(
                     iStateObject.ConditionalPointsCompressed.ByteData,
                     PacketBase.XML_ELEMENT_ID,
                     PacketBase.CacheFile( TableName.CONDITIONALPOINT ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // deserialize the decompressed packet
                  EnumConditionalPoints conditionalPoints =
                         mDeserializer.DeserializeConditionalPoints( elements );

                  // populate the table
                  result = mSqlConnect.Populate.ConditionalPointTable( conditionalPoints );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the DERIVEDITEMS table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateDerivedItemsTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.DerivedItemsTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.DerivedItemsTableCompressed.DecompressToXmlFile(
                     iStateObject.DerivedItemsTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_ID,
                     PacketBase.CacheFile( TableName.DERIVEDITEMS ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of Derived Item records
                  EnumDerivedItems derivedItems =
                     mDeserializer.DeserializeDerivedItems( elements );

                  // and populate the FFTPOINTS table
                  result = mSqlConnect.Populate.DerivedItemsTable( derivedItems );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the DERIVEDPOINT table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateDerivedPointTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.DerivedPointTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.DerivedPointTableCompressed.DecompressToXmlFile(
                     iStateObject.DerivedPointTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_POINT_UID,
                     PacketBase.CacheFile( TableName.DERIVEDPOINT ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of Derived Point records
                  EnumDerivedPoints derivedPoints =
                     mDeserializer.DeserializeDerivedPoints( elements );

                  // and populate the FFTPOINTS table
                  result = mSqlConnect.Populate.DerivedPointTable( derivedPoints );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the FFTPOINT table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateFFTPointsTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.FFTPointsTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.FFTPointsTableCompressed.DecompressToXmlFile(
                     iStateObject.FFTPointsTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.FFTPOINT ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of FFTPoint records
                  EnumFFTPoints fftPoints =
                     mDeserializer.DeserializeFFTPointsList( elements );

                  // and populate the FFTPOINTS table
                  result = mSqlConnect.Populate.FFTPointsTable( fftPoints );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the INSPECTION table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateInspectionTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.InspectionTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.InspectionTableCompressed.DecompressToXmlFile(
                     iStateObject.InspectionTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.INSPECTION ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of inspection records
                  EnumInspection inspection =
                     mDeserializer.DeserializeInspectionList( elements );

                  // and populate the INSPECTION table
                  result = mSqlConnect.Populate.InspectionTable( inspection );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the INSTRUCTIONS table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateInstructionsTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.InstructionTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.InstructionTableCompressed.DecompressToXmlFile(
                     iStateObject.InstructionTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_POINT_UID,
                     PacketBase.CacheFile( TableName.INSTRUCTIONS ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of instructions
                  EnumInstructions instructions =
                     mDeserializer.DeserializeInstructions( elements );

                  // and populate the INSTRUCTIONS table
                  result = mSqlConnect.Populate.InstructionsTable( instructions );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the MCC table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateMCCTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.MCCTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.MCCTableCompressed.DecompressToXmlFile(
                     iStateObject.MCCTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.MCC ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of MCC records
                  EnumMCC mcc =
                     mDeserializer.DeserializeMCCList( elements );

                  // and populate the MCC table
                  result = mSqlConnect.Populate.MCCTable( mcc );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the MESSAGE table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateMessageTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.MessageTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.MessageTableCompressed.DecompressToXmlFile(
                     iStateObject.MessageTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.MESSAGE ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of messages
                  EnumMessages messages =
                     mDeserializer.DeserializeMessages( elements );

                  // and populate the MESSAGE table
                  result = mSqlConnect.Populate.MessageTable( messages );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the NODE table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateNodeTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.NodeTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.NodeTableCompressed.DecompressToXmlFile(
                     iStateObject.NodeTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.NODE ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of nodes
                  EnumNodes nodes =
                     mDeserializer.DeserializeNodeList( elements, true );

                  // and populate the NODE table
                  result = mSqlConnect.Populate.NodeTable( nodes );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet, then clear and populate 
      /// the OPERATORS table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateOperatorsTable( ClientStateObject iStateObject )
      {
         try
         {
            if ( iStateObject.OperatorSetsCompressed != null )
            {
               // clear all operator settings
               mSqlConnect.Clear.OperatorSettings();

               // decompress the raw packet
               iStateObject.ReadBuffer =
                   iStateObject.OperatorSetsCompressed.Decompress(
                   iStateObject.OperatorSetsCompressed.ByteData );

               // deserialize the decompressed packet
               PacketOperatorSet opSet = 
                PacketBase.DeserializeObject<PacketOperatorSet>( iStateObject.ReadBuffer );

               // populate the table
               Boolean result = mSqlConnect.Populate.OperatorsTable( opSet.OperatorSet );

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the POINTS table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdatePointsTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.PointsTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.PointsTableCompressed.DecompressToXmlFile(
                     iStateObject.PointsTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.POINTS ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of points
                  EnumPoints points =
                     mDeserializer.DeserializePointsList( elements, true );

                  // and populate the POINTS table
                  result = mSqlConnect.Populate.PointsTable( points );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the PROCESS table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateProcessTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.ProcessTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.ProcessTableCompressed.DecompressToXmlFile(
                     iStateObject.ProcessTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.PROCESS ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of process records
                  EnumProcess process =
                     mDeserializer.DeserializeProcessList( elements );

                  // and populate the PROCESS table
                  result = mSqlConnect.Populate.ProcessTable( process );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the TEXTDATA table
      /// </summary>
      /// <param name="iStateObject"></param>
      /// <param name="desr"></param>
      public Boolean UpdateTextDataTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.TextDataTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.TextDataTableCompressed.DecompressToXmlFile(
                     iStateObject.TextDataTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.TEXTDATA ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of textData records
                  EnumTextData textData =
                     mDeserializer.DeserializeTextDataList( elements );

                  // and populate the TEXTDATA table
                  result = mSqlConnect.Populate.TextDataTable( textData );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the REGERENCEMEDIA table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateReferenceMediaTable( ClientStateObject iStateObject )
      {
         // set the initial return state
         Boolean result = false;

         try
         {
            if ( iStateObject.ReferenceMediaTableCompressed != null )
            {
               //
               // decompress the received byte array to a file and (based
               // on the element identifier) get return how many records
               // were actually written to file
               //
               int elements = 
                  iStateObject.ReferenceMediaTableCompressed.DecompressToXmlFile(
                     iStateObject.ReferenceMediaTableCompressed.ByteData,
                     PacketBase.XML_ELEMENT_UID,
                     PacketBase.CacheFile( TableName.REFERENCEMEDIA ) );

               // assuming one or more xml elements were saved then ..
               if ( elements > 0 )
               {
                  // .. deserialize to an enumerated list of media records
                  EnumReferenceMedia reference =
                     mDeserializer.DeserializeReferenceMediaList( elements );

                  // and populate the REFEREMCEMEDIA table
                  result = mSqlConnect.Populate.ReferenceMediaTable( reference );
               }
               else
               {
                  result = false;
               }

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }



      }/* end method */


      /// <summary>
      /// Decompress the received packet and populate the STRUCTUREDROUTE table
      /// </summary>
      /// <param name="iStateObject"></param>
      public Boolean UpdateStructuredRouteTable( ClientStateObject iStateObject )
      {
         try
         {
            if ( iStateObject.StructuredRouteTableCompressed != null )
            {
               // decompress the raw packet
               iStateObject.ReadBuffer =
                   iStateObject.StructuredRouteTableCompressed.Decompress(
                   iStateObject.StructuredRouteTableCompressed.ByteData );

               // deserialize the decompressed packet
               EnumStructuredRoutes structured = 
                mDeserializer.DeserializeStructuredRoutes( iStateObject.ReadBuffer );

               // populate the table
               Boolean result = mSqlConnect.Populate.StructuredRouteTable( structured );

               // raise a progress event
               mOverallProgress.Update( true );

               // return result
               return result;
            }
            else
            {
               mOverallProgress.Update( true );
               return true;
            }
         }
         catch
         {
            mOverallProgress.Update( true );
            return false;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 11th September 2012
//  Add support for new TextData Measurement type
//
//  Revision 1.0 APinkerton 17nd October 2010
//  Re-engineer to support caching to file
//
//  Revision 0.0 APinkerton 2nd October 2009
//  Add to project
//----------------------------------------------------------------------------