﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// This class tests both the server availability and the operability 
   /// of the Middleware Service running on that server.
   /// </summary>
   public interface ITestServer
   {
      /// <summary>
      /// Events raised on server or middleware availability
      /// </summary>
      event TestServer.ClientStatus ClientStatusEvent;


      /// <summary>
      /// Event raised on busy flag of test
      /// </summary>
      event TestServer.ConnectionState ConnectionStateEvent;
      

      /// <summary>
      /// Test the Server Connection 
      /// </summary>
      void TestConnection( );


      /// <summary>
      /// Release the Server Connection
      /// </summary>
      void Dispose();


      /// <summary>
      /// Gets or sets the host name or IP address
      /// </summary>
      string SocketHost
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the IP Address number
      /// </summary>
      short TcpPortNumber
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets flag stipulating whether we should use the host name
      /// or the IP Address to make a link to the server
      /// </summary>
      bool UseIpAddress
      {
         get;
         set;
      }

   }/* end interface */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th January 2010
//  Add to project
//----------------------------------------------------------------------------