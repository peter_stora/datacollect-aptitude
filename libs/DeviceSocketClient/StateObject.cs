﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using SKF.RS.MicrologInspector.Packets;
using System.IO;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// Base state object to manage all socket data transfer
   /// </summary>
   public class StateObject : IDisposable
   {
      /// <summary>
      /// TCP download packet size (must be the same on client and server)
      /// </summary>
      private const int BUFFER_BYTES = 32000;

      /// <summary>
      /// Maximum bytes per download before error message is thrown
      /// </summary>
      private const int MAXIMUM_BYTES = 0x200000;

      /// <summary>
      /// Set Media upload block size to 64KB
      /// Note: This seems to be the optimal block size, as values below
      /// and above this value actually slow the upload down.
      /// </summary>
      private const int MEDIA_BLOCK_SIZE = 0x40000;

      /// <summary>
      /// Define the TCP read stream buffer
      /// </summary>
      private byte[] mReadBuffer = new byte[ 0 ];

      /// <summary>
      /// Define the incremental data buffer
      /// </summary>
      private byte[] mDataBuffer = new byte[ BUFFER_BYTES ];

      /// <summary>
      /// Define the received media file buffer
      /// </summary>
      private byte[] mMediaBuffer = new byte[ 0 ];

      /// <summary>
      /// Class Constructor
      /// </summary>
      /// <param name="socket">Pointer to work socket</param>
      /// <param name="clientNumber">Connected client index</param>
      protected StateObject( Socket iWorkerSocket )
      {
         WorkSocket = iWorkerSocket;

      }/* end constructor */


      /// <summary>
      /// Dispose of all buffer contents
      /// </summary>
      public void Dispose()
      {
         FlushBuffers();
         FlushMediaBuffer();
      }/* end dispose */

      /// <summary>
      /// Gets the Media Block Size
      /// </summary>
      public int MediaBlockSize
      {
         get
         {
            return MEDIA_BLOCK_SIZE;
         }
      }

      /// <summary>
      /// Pointer to open socket endpoint
      /// </summary>
      public Socket WorkSocket
      {
         get;
         set;
      }

      /// <summary>
      /// Read data buffer of variable length
      /// </summary>
      public byte[] ReadBuffer
      {
         get
         {
            return mReadBuffer;
         }
         set
         {
            mReadBuffer = value;
         }
      }

      /// <summary>
      /// Fixed size data buffer
      /// </summary>
      public byte[] DataBuffer
      {
         get
         {
            return mDataBuffer;
         }
         set
         {
            mDataBuffer = value;
         }
      }

      /// <summary>
      /// Fixed size Media buffer
      /// </summary>
      public byte[] MediaBuffer
      {
         get
         {
            return mMediaBuffer;
         }
         set
         {
            mMediaBuffer = value;
         }
      }

      /// <summary>
      /// Device UID of the connected client
      /// </summary>
      public string DeviceUID
      {
         get;
         set;
      }

      /// <summary>
      /// Device Name of the connected client
      /// </summary>
      public string DeviceName
      {
         get;
         set;
      }

      /// <summary>
      /// <see cref="DeviceType"/> device type and firmware version for connecting device (found from IAM packet)
      /// </summary>
      public DeviceType DeviceType
      {
         get;
         set;
      }

      /// <summary>
      /// Current connection type
      /// </summary>
      public FieldTypes.ConnectionType ConnectionType
      {
         get;
         set;
      }

      /// <summary>
      /// Device SubKey for the connected client (not @ptitude!)
      /// </summary>
      public string SubKey
      {
         get;
         set;
      }

      /// <summary>
      /// Authentication key for the connected client (not @ptitude!) 
      /// </summary>
      public string AuthenticationKey
      {
         get;
         set;
      }

      /// <summary>
      /// Current Session ID
      /// </summary>
      public string SessionID
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the current session expiry time
      /// </summary>
      public DateTime SessionExpires
      {
         get;
         set;
      }

      /// <summary>
      /// Operator set ID assigned to the current profile
      /// </summary>
      public int OperatorSetId
      {
         get;
         set;
      }


      /// <summary>
      /// Clear saved media, data and read buffers
      /// </summary>
      public void FlushBuffers()
      {
         Array.Clear( mDataBuffer, 0, mDataBuffer.Length );
         Array.Clear( mReadBuffer, 0, mReadBuffer.Length );
         Array.Resize<byte>( ref mReadBuffer, 0 );

      }/* end method */


      /// <summary>
      /// Clear saved media buffer
      /// </summary>
      public void FlushMediaBuffer()
      {
         Array.Clear( mMediaBuffer, 0, mMediaBuffer.Length );
         Array.Resize<byte>( ref mMediaBuffer, 0 );
      }/* end method */


      /// <summary>
      /// Append the data buffer to the read buffer 
      /// </summary>
      /// <param name="iBytesRead">number of bytes to process</param>
      /// <returns>false on exception raised</returns>
      public Boolean AppendDataToReadBuffer( int iBytesRead )
      {
         try
         {
            if ( ( mReadBuffer.Length + iBytesRead ) < MAXIMUM_BYTES )
            {
               if ( iBytesRead < mDataBuffer.Length )
               {
                  int index = mReadBuffer.Length;
                  Array.Resize<byte>( ref mReadBuffer, ( mReadBuffer.Length + iBytesRead ) );
                  Array.Copy( DataBuffer, 0, mReadBuffer, index, iBytesRead );
               }
               else
               {
                  int index = mReadBuffer.Length;
                  Array.Resize<byte>( ref mReadBuffer, ( mReadBuffer.Length + mDataBuffer.Length ) );
                  Array.Copy( DataBuffer, 0, mReadBuffer, index, mDataBuffer.Length );
               }
               Array.Clear( mDataBuffer, 0, mDataBuffer.Length );
               return true;
            }
            else
            {
               Array.Resize( ref mReadBuffer, 0 );
               Array.Resize( ref mDataBuffer, 0 );
               return false;
            }
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Append received media file block to end of the media file buffer
      /// </summary>
      /// <param name="iMediaBlock">Received media file block</param>
      /// <returns>false on exception raised</returns>
      public Boolean AppendToMediaBuffer( byte[] iMediaBlock )
      {
         try
         {
            int index = mMediaBuffer.Length;
            Array.Resize<byte>( ref mMediaBuffer, ( mMediaBuffer.Length + iMediaBlock.Length ) );
            Array.Copy( iMediaBlock, 0, mMediaBuffer, index, iMediaBlock.Length );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */

   }/* end class */



   /// <summary>
   /// State object to maintain client-side data management
   /// </summary>
   public class ClientStateObject : StateObject
   {
      // default profile update flag to true!
      private Boolean mProfileUpdateRequired = true;

      // was the upload file received 
      private Boolean mUploadReceived = false;

      // reset the media ready for upload flag
      private Boolean mMediaReady = false;

      // what is the current packet type 
      private PacketType mCurrentPacket = PacketType.UNKNOWN;

      /// <summary>
      /// Override constructor
      /// </summary>
      /// <param name="socket">Pointer to worker socker</param>
      public ClientStateObject( Socket iSocket )
         : base( iSocket )
      {
      }/* end constructor */

      /// <summary>
      /// Empty all internal byte buffers
      /// </summary>
      public new void Dispose()
      {
         base.Dispose();
         FlushCompressedPacketBuffers();
      }/* end dispose */

      /// <summary>
      /// Simple flag which is set to indicate that the current device
      /// profile is still valid and does not updating
      /// </summary>
      public Boolean ProfileUpdateRequired
      {
         get
         {
            return mProfileUpdateRequired;
         }
         set
         {
            mProfileUpdateRequired = value;
         }
      }

      /// <summary>
      /// Was the upload packet received and cached by the server
      /// </summary>
      public Boolean UploadReceivedByServer
      {
         get
         {
            return mUploadReceived;
         }
         set
         {
            mUploadReceived = value;
         }
      }

      /// <summary>
      /// Gets or sets value of the Media ready for upload flag
      /// </summary>
      public Boolean MediaReady
      {
         get
         {
            return mMediaReady;
         }
         set
         {
            mMediaReady = value;
         }
      }


      /// <summary>
      /// Gets or sets what the current active packet type is
      /// </summary>
      public PacketType CurrentPacket
      {
         get
         {
            return mCurrentPacket;
         }
         set
         {
            mCurrentPacket = value;
         }
      }


      /// <summary>
      /// Operator Set with all named operators and associated settings
      /// </summary>
      public OperatorSet OperatorSettings
      {
         get;
         set;
      }

      /// <summary>
      /// Profile Data
      /// </summary>
      public ProfileSettings ProfileData
      {
         get;
         set;
      }

      /// <summary>
      /// Tables pending in the available profile
      /// </summary>
      public EnumPendingTableNames PendingTables
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Operator Set packet from server
      /// </summary>
      public PacketCompressed OperatorSetsCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Node table packet from server
      /// </summary>
      public PacketCompressed NodeTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Points table packet from server
      /// </summary>
      public PacketCompressed PointsTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Process table packet from server
      /// </summary>
      public PacketCompressed ProcessTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed TextData table packet from server
      /// </summary>
      public PacketCompressed TextDataTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Inspection table packet from server
      /// </summary>
      public PacketCompressed InspectionTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Instruction table packet from the server
      /// </summary>
      public PacketCompressed InstructionTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed MCC table packet from the server
      /// </summary>
      public PacketCompressed MCCTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Coded Notes table packet from the server
      /// </summary>
      public PacketCompressed CodedNotesCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Conditional Points table packet from the server
      /// </summary>
      public PacketCompressed ConditionalPointsCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Message table packet from the server
      /// </summary>
      public PacketCompressed MessageTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Derived Items table packet from the server
      /// </summary>
      public PacketCompressed DerivedItemsTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Derived Point table packet from the server
      /// </summary>
      public PacketCompressed DerivedPointTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Derived Point table packet from the server
      /// </summary>
      public PacketCompressed StructuredRouteTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Cmms Option tables packet from the server
      /// </summary>
      public PacketCompressed CmmsOptionsTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed FFTPoints tables packet from the server
      /// </summary>
      public PacketCompressed FFTPointsTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Compressed Reference Media table packet from the server
      /// </summary>
      public PacketCompressed ReferenceMediaTableCompressed
      {
         get;
         set;
      }

      /// <summary>
      /// Saved Media references
      /// </summary>
      public EnumSavedMedia SavedMedia
      {
         get;
         set;
      }

      /// <summary>
      /// Delete all compressed packet objects
      /// </summary>
      public void FlushCompressedPacketBuffers()
      {
         OperatorSetsCompressed = null;
         NodeTableCompressed = null;
         PointsTableCompressed = null;
         ProcessTableCompressed = null;
         InspectionTableCompressed = null;
         InstructionTableCompressed = null;
         MCCTableCompressed = null;
         CodedNotesCompressed = null;
         ConditionalPointsCompressed = null;
         MessageTableCompressed = null;
         DerivedItemsTableCompressed = null;
         DerivedPointTableCompressed = null;
         StructuredRouteTableCompressed = null;
         CmmsOptionsTableCompressed = null;
         FFTPointsTableCompressed = null;
         ReferenceMediaTableCompressed = null;
      }/* end method */

   }/* end class */



   /// <summary>
   /// State object to maintain server-side data management
   /// </summary>
   public class ServerStateObject : StateObject
   {
      /// <summary>
      /// How many times should we try to read a bad media file before we give in?
      /// </summary>
      private const int MEDIA_FILE_READ_ATTEMPTS = 2;

      /// <summary>
      /// Class Constructor
      /// </summary>
      /// <param name="socket">Pointer to worker socket</param>
      /// <param name="clientNumber">Connected client index</param>
      public ServerStateObject( Socket iSocket )
         : base( iSocket )
      {
         ClientID = Guid.NewGuid();
         this.MediaRetries = 0;
      }/* end constructor */

      /// <summary>
      /// Number of client devices currently connected
      /// </summary>
      public Guid ClientID
      {
         get;
         set;
      }

      /// <summary>
      /// Does the connected client use a Key license? 
      /// </summary>
      public Boolean DeviceKeyLicensed
      {
         get;
         set;
      }

      /// <summary>
      /// What is the current state of the connected client
      /// </summary>
      public DeviceStatus Status
      {
         get;
         set;
      }

      /// <summary>
      /// Profile Table pending direction to client
      /// </summary>
      public PendingTableSet PendingTable
      {
         get;
         set;
      }

      /// <summary>
      /// Saved Media references
      /// </summary>
      public EnumUploadSavedMediaRecords SavedMedia
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Saved Media block counter
      /// </summary>
      public int MediaBlockID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current Media file being processed
      /// </summary>
      public Guid MediaFileUID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current Media file index
      /// </summary>
      public int MediaFileIndex
      {
         get;
         set;
      }

      /// <summary>
      /// Gets how many times we should attempt to read a bad media file
      /// </summary>
      public int MaximumRetries
      {
         get
         {
            return MEDIA_FILE_READ_ATTEMPTS;
         }
      }

      /// <summary>
      /// Gets or sets the number of times that we have tried to receive
      /// </summary>
      public int MediaRetries
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 11th October 2011
//  Added support for Media File processing to both client and server objects
//
//  Revision 1.0 APinkerton 8th July 2010
//  Added Device Name to the base object
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------

