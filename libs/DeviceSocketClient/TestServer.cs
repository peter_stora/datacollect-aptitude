﻿//------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 to 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//------------------------------------------------------------------------------

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// This class tests both the server availability and the operability 
   /// of the Middleware Service running on that server.
   /// </summary>
   public class TestServer : TCPCommsBase, ITestServer, IDisposable
   {
      //------------------------------------------------------------------------

      #region Public Constructors and Destructors

      /// <summary>
      /// Base Constructor:
      /// </summary>
      /// <param name="iSender">reference to parent object</param>
      public TestServer( object iSender )
         : base( iSender )
      {
      }/* base constructor */


      /// <summary>
      /// Close any open connection and dispose of the 
      /// SQL ResultSet data Connection object
      /// </summary>
      public void Dispose()
      {
         try
         {
            Disconnect();
            //
            // only dispose of StateObject if it has not already
            // marked for collected by the GC
            //
            if ( ( !mStateObjectDisposing ) && ( mStateObject != null ) )
            {
               mStateObject.Dispose();
               mStateObject = null;
               mStateObjectDisposing = true;
            }
         }
         finally
         {
         }

      }/*end dispose */

      #endregion

      //------------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// Connect to the Middleware Device Server
      /// </summary>
      public void TestConnection()
      {
         // we are now officialy busy!
         ReturnClientStatusEvent( true );

         // Queue the task and data.
         if ( ThreadPool.QueueUserWorkItem( new WaitCallback( ConnectToServer ), null ) )
         {
            Thread.Sleep( 200 );
         }

      }/* end method */


      /// <summary>
      /// Disconnect from the Device Server. This is a somewhat "messy"
      /// way of doing things, with a number of read\write exceptions
      /// being raised. It is however, the method that Microsoft actually
      /// stipulate in their documentation!
      /// </summary>
      public override void Disconnect()
      {
         try
         {
            if ( mClientSocket != null )
            {
               if ( mClientSocket.Connected )
               {
                  // disable all socket send and end receives
                  mClientSocket.Shutdown( SocketShutdown.Both );

                  // take a break!
                  System.Threading.Thread.Sleep( 100 );

                  // close and dispose!
                  mClientSocket.Close();
                  mClientSocket = null;
               }
            }
         }
         finally
         {
            // release the busy flag!
            ReturnClientStatusEvent( false );
         }

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Core comms methods

      /// <summary>
      /// Connect to the Middleware Device Server
      /// </summary>
      /// <param name="iUseIPAddress">null</param>
      protected override void ConnectToServer( object result )
      {
         try
         {
            // Create the socket instance
            mClientSocket = new Socket( AddressFamily.InterNetwork,
               SocketType.Stream,
               ProtocolType.Tcp );

            // reset the state object disposing flag
            mStateObjectDisposing = false;

            //
            // if use IP address then get address directly, otherwise 
            // use the hostname instead
            //
            if ( UseIpAddress )
            {
               mIpAddress = IPAddress.Parse( SocketHost );
            }
            else
            {
               mIpAddress = GetIPAddress( SocketHost, AddressFamily.InterNetwork );
            }

            // Create the end point 
            IPEndPoint ipEnd = new IPEndPoint( mIpAddress, TcpPortNumber );

            // reference method to manage connection events
            AsyncCallback onconnect = new AsyncCallback( OnConnect );

            // initiate the new connection event
            IAsyncResult onConnect = mClientSocket.BeginConnect( ipEnd, onconnect, mClientSocket );

            // lets wait and see if can connect to a server. If not then bomb out here
            if ( !onConnect.AsyncWaitHandle.WaitOne( TEST_READ_TIMEOUT, false ) )
            {
               ConnectionEventRaised( false, ConnectionCode.ERR_NO_SERVER );
               this.Disconnect();
            }
         }

         catch
         {
            // raise event on no server found!
            ConnectionEventRaised( false, ConnectionCode.ERR_NO_SERVER );

            // release the busy flag!
            ReturnClientStatusEvent( false );
         }

      }/* end method */


      /// <summary>
      /// Process management for on Connection event 
      /// </summary>
      /// <param name="ar">pointer to interface ar</param>
      protected override void OnConnect( IAsyncResult ar )
      {
         // reset the connection error state
         mConnectedAs = ConnectionCode.ERR_NO_SERVER;
         
         try
         {
            if ( mClientSocket.Connected )
            {
               // create a new state object
               ClientStateObject stateObject = new ClientStateObject( mClientSocket );

               // and reference!
               mStateObject = stateObject;

               // and wait for data!
               WaitForNextData( stateObject );
            }
            else
            {
               // raise event on on server found - but no Middleware Service!
               ConnectionEventRaised( false, ConnectionCode.SERVER_NOT_AVAILABLE );

               // release the busy flag!
               ReturnClientStatusEvent( false );
            }
         }
         catch
         {
            // raise event on no server found!
            ConnectionEventRaised( false, ConnectionCode.ERR_NO_SERVER );

            // release the busy flag!
            ReturnClientStatusEvent( false );
         }

      }/* end method */


      /// <summary>
      /// Point receive data event to 
      /// </summary>
      protected override void WaitForNextData( ClientStateObject iStateObject )
      {
         try
         {
            if ( mPfnCallBack == null )
            {
               mPfnCallBack = new AsyncCallback( OnDataReceived );
            }

            // Start listening to the data asynchronously
            mAsyncResult = mClientSocket.BeginReceive( iStateObject.DataBuffer,
                                                    0, iStateObject.DataBuffer.Length,
                                                    SocketFlags.None,
                                                    mPfnCallBack,
                                                    iStateObject );

            //
            // lets wait and see if that worked. If it didn't then we can assume
            // that the server is either not listening, or we're connected to
            // the wrong Port (i.e. the Transaction Service) [OTD #2307]
            //
            if ( !mAsyncResult.AsyncWaitHandle.WaitOne( TEST_READ_TIMEOUT, false ) )
            {
               ConnectionEventRaised( false, ConnectionCode.ERR_INVALID_SERVICE_PORT );
               iStateObject.FlushBuffers();
               this.Disconnect();
            }
         }
         catch
         {
         }

      }/* end method */


      /// <summary>
      /// Action on data received * END HERE *
      /// </summary>
      /// <param name="asyn"></param>
      protected override void OnDataReceived( IAsyncResult asyn )
      {
         // Retrieve the state object from the AsyncResult pointer
         ClientStateObject stateObject = (ClientStateObject) asyn.AsyncState;

         // make sure this is well protected - many exceptions from here!
         try
         {
             // are we still connected?
             if ( stateObject.WorkSocket.Connected )
             {
               // ok, stop receiving data (for the moment)
               int iBytesReceived = stateObject.WorkSocket.EndReceive( asyn );

               // how much data did we receive there?
               if ( iBytesReceived > 0 )
               {
                  // append the cache buffer contents to the main read buffer
                  stateObject.AppendDataToReadBuffer( iBytesReceived );

                  // process the received data packet
                  ProcessReceiveBytes( stateObject );

                  // wait for the next dataset to arrive
                  WaitForNextData( stateObject );
               }
               // if nothing, then clear the platform!
               else
               {
                  stateObject.WorkSocket.Shutdown( SocketShutdown.Both );
                  stateObject.WorkSocket.Close();
               }
             }
            else
            {
               // fix for OTD# 3013 - the socket 'connected' but we have no true connection
               ConnectionEventRaised( false, mConnectedAs );
               if ( mConnectedAs == ConnectionCode.ERR_NO_SERVER )
               {
                  Disconnect();
               }
            }
         }
         
         catch ( SocketException ex )
         {
            HandleSocketException( ex, stateObject );
         }

      }/* end method */


      /// <summary>
      /// Manage any Socket Exception (i.e. Operator pulls device from cradle)
      /// </summary>
      /// <param name="ex"></param>
      /// <param name="iStateObject"></param>
      protected void HandleSocketException(
         SocketException ex,
         ClientStateObject iStateObject )
      {
         //
         // If the error was related to an unexpected disconnection 
         // (i.e. The operator pulled the device from the cradle), then
         // we need to return an exception
         //
         if ( ( ex.ErrorCode == SocketErrorCodes.InterruptedFunctionCall )
         | ( ex.ErrorCode == SocketErrorCodes.SocketOperationOnNonSocket ) )
         {
            // close the open socket (any more and we kill the app!)
            // also check for nulls here (OTD #4459)
            if ( mClientSocket != null && mClientSocket.Connected )
            {
               mClientSocket.Close();
            }

            ConnectionEventRaised( false, ConnectionCode.ERR_UNCRADLED );
         }
         else
         {
            // full disconnect only for non-uncradle events (OTD #1401)
            // also check for nulls here (OTD #4459)
            if ( mClientSocket != null && mClientSocket.Connected )
            {
               // disable all socket send and end receives
               mClientSocket.Shutdown( SocketShutdown.Both );

               // take a break!
               System.Threading.Thread.Sleep( 100 );

               // close and dispose!
               mClientSocket.Close();
               mClientSocket = null;
            }

            ConnectionEventRaised( false, ConnectionCode.SERVER_NOT_AVAILABLE );
         }

         // release the busy flag!
         ReturnClientStatusEvent( false );

         //we're done here, lets dispose of the state object
         if ( !mStateObjectDisposing )
         {
            mStateObjectDisposing = true;
         }

      }/* end method */


      /// <summary>
      /// This method process all receives and deserializes all received
      /// byte array data. Given that all packets are braced between the
      /// fields "ACK" and "/ACK", datagrams not correctly terminated are
      /// simply rejected, and the buffer re-read when more bytes have been
      /// revived from the server.
      /// Note: This method operates within the COMMS thread - and so has
      /// no connectivity with the user interface!
      /// </summary>
      /// <param name="iStateObject">Current State Object</param>
      protected override void ProcessReceiveBytes( ClientStateObject iStateObject )
      {
         // get the server packet type
         PacketType serverPacketType = PacketBase.GetPacketType( iStateObject.ReadBuffer );

         // ensure packet type is known
         if ( serverPacketType != PacketType.UNKNOWN )
         {
            switch ( serverPacketType )
            {
               // for packet type 'UR' (on connection)
               case PacketType.UR:
                  ConnectionEventRaised( true, ConnectionCode.CONNECTED );
                  mConnectedAs = ConnectionCode.CONNECTED;
                  this.Disconnect();
                  break;

               // for packet type 'UR' (on connection)
               case PacketType.NULL_BUSY:
                  ConnectionEventRaised( true, ConnectionCode.ERR_SERVER_BUSY );
                  mConnectedAs = ConnectionCode.ERR_SERVER_BUSY;
                  this.Disconnect();
                  break;

               // if all else fails
               default:
                  break;
            }
            // clear all internal state object buffers
            iStateObject.FlushBuffers();
         }

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 4th July 2012
//  Add some fixes to prevent null reference exceptions
//
//  Revision 0.0 APinkerton 18th January 2010
//  Add to project
//------------------------------------------------------------------------------