﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// Internal static class for processing media files at the byte level
   /// </summary>
   internal static class MediaIO
   {
      /// <summary>
      /// Returns the saved media file object for a given file UID
      /// </summary>
      /// <param name="iFileUID">file UID to lookup</param>
      /// <param name="iStateObject">populated client state object</param>
      /// <returns>file object or null on </returns>
      public static SavedMediaRecord GetSavedMediaInfo( Guid iFileUID, ClientStateObject iStateObject )
      {
         // reset the media reference list
         iStateObject.SavedMedia.Reset();

         // and query..
         if ( iStateObject.SavedMedia != null )
         {
            foreach ( SavedMediaRecord esm in iStateObject.SavedMedia )
            {
               if ( esm.Uid == iFileUID )
               {
                  if ( File.Exists( esm.FilePath + "\\" + esm.FileName ) )
                  {
                     return esm;
                  }
               }
            }
         }
         return null;
      
      }/* end methdo */


      /// <summary>
      /// Get an indexed byte block from the saved media file
      /// </summary>
      /// <param name="iMediaFile">file path and name</param>
      /// <param name="iBlockID">byte block to read</param>
      /// <param name="iBlockSize">current block index</param>
      /// <returns>byte array</returns>
      public static byte[] GetMediaBlock( string iMediaFile, int iBlockID, int iBlockSize )
      {
         byte[] fileBytes = new byte[ 0 ];
         try
         {
            using ( FileStream fsSource = new FileStream( iMediaFile,
                FileMode.Open, FileAccess.Read ) )
            {
               // get the start and end bytes positions
               int startByte = iBlockID * iBlockSize;
               int endByte = startByte + iBlockSize;

               // set the default number of bytes to read
               int bytesToRead = iBlockSize;

               //
               // if the end byte position exceeds the number of bytes in
               // the file, then recalculate the number of bytes to read
               //
               if ( endByte > fsSource.Length )
               {
                  bytesToRead = (int)fsSource.Length;
               }

               // open a binary reader and read the target block
               using ( BinaryReader reader = new BinaryReader( fsSource ) )
               {
                  reader.BaseStream.Position = startByte;
                  fileBytes = reader.ReadBytes( bytesToRead );
               }
            }
         }
         catch
         {
            ByteArrayProcess.ClearByteBuffer( ref fileBytes );
         }

         return fileBytes;

      }/* end method */

   } /* end class */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 15th October 2011
//  Add to project
//----------------------------------------------------------------------------