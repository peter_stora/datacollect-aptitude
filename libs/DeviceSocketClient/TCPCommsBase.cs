﻿//------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 to 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;
using SKF.RS.MicrologInspector.Packets;
using SKF.RS.MicrologInspector.HardwareConnect;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// TCP Communications base class
   /// </summary>
   public class TCPCommsBase
   {
      //------------------------------------------------------------------------

      #region Private Constants

      /// <summary>
      /// Default Port
      /// </summary>
      protected const Int16 DEFAULT_PORT = 8086;
      
      /// <summary>
      /// Default host name
      /// </summary>
      protected const String DEFAULT_HOST = "localhost";
      
      /// <summary>
      /// Initially do not use the IP Address
      /// </summary>
      protected const Boolean DEFAULT_USE_IP_ADDRESS = false;

      /// <summary>
      /// Invalid device UID
      /// </summary>
      protected const String DEFAULT_UID = "MICROLOGINSPECTORTESTUID";
      
      /// <summary>
      /// Base communications version
      /// </summary>
      protected const string DEFAULT_VERSION = "1.0.1";
      
      /// <summary>
      /// Timeout after ten seconds
      /// </summary>
      protected const int TEST_READ_TIMEOUT = 30000;

      #endregion

      //------------------------------------------------------------------------

      #region Private Fields

      /// <summary>
      /// Comms callback on data read
      /// </summary>
      protected IAsyncResult mAsyncResult;
      
      /// <summary>
      /// Callback on received data
      /// </summary>
      protected AsyncCallback mPfnCallBack;
      
      /// <summary>
      /// Target socket
      /// </summary>
      protected Socket mClientSocket;
      
      /// <summary>
      /// IP Address to connect to
      /// </summary>
      protected IPAddress mIpAddress;

      /// <summary>
      /// Client state object
      /// </summary>
      protected ClientStateObject mStateObject;

      /// <summary>
      /// Default host name (initially localhost)
      /// </summary>
      protected String mHostName = DEFAULT_HOST;
      
      /// <summary>
      /// Default Port Number
      /// </summary>
      protected Int16 mPortNumber = DEFAULT_PORT;
      
      /// <summary>
      /// Use IP Address over host name when true
      /// </summary>
      protected Boolean mUseIPAddress = DEFAULT_USE_IP_ADDRESS;

      /// <summary>
      /// Inspector client private fields
      /// </summary>
      protected Boolean mStateObjectDisposing = false;

      /// <summary>
      /// reference to the parent form (to synch UI threads)
      /// </summary>
      protected Form mParent = null;

      /// <summary>
      /// link to the Registry
      /// </summary>
      protected IRegistryConnect mRegistry = new RegistryConnect();

      /// <summary>
      /// reference the current firmware version
      /// </summary>
      protected HwVersion mFirmware;

      /// <summary>
      /// reference the current packets version
      /// </summary>
      protected HwVersion mPacketsVersion;

      /// <summary>
      /// Connection code return value (primarily for testing)
      /// </summary>
      protected ConnectionCode mConnectedAs = ConnectionCode.ERR_NO_SERVER;

      /// <summary>
      /// Server Synchronization Time
      /// </summary>
      protected static DateTime mServerTime;

      #endregion

      //------------------------------------------------------------------------

      #region Delegates, Events and Event Processing

      /// <summary>
      /// Background communication status delegate
      /// </summary>
      /// <param name="iBusy">Comms busy state</param>
      public delegate void ClientStatus( Boolean iBusy );


      /// <summary>
      /// Background thread connection state delegate 
      /// </summary>
      /// <param name="iConnectionState">true on connection else false</param>
      /// <param name="iCode">connection enum type</param>
      public delegate void ConnectionState( Boolean iConnectionState, ConnectionCode iCode );


      /// <summary>
      /// Communications status thread synched to the UI thread
      /// </summary>
      /// <param name="iBusy"></param>
      private delegate void SynchUIClientStatus( Boolean iBusy );


      /// <summary>
      /// Connection state delegate synched to UI thread 
      /// </summary>
      /// <param name="iConnectionState">true on connection else false</param>
      /// <param name="iCode">connection enum type</param>
      private delegate void SynchUIConnectionState( Boolean iConnectionState, ConnectionCode iCode );


      /// <summary>
      /// Event raised on change in client busy state
      /// </summary>
      public event ClientStatus ClientStatusEvent;


      /// <summary>
      /// Event raised on any change in connection status
      /// </summary>
      public event ConnectionState ConnectionStateEvent;

      #endregion

      //------------------------------------------------------------------------

      #region Delegate support methods

      /// <summary>
      /// Raise event on change in client status (busy or done)
      /// </summary>
      /// <param name="iBusy"></param>
      protected void SetClientStatus( Boolean iBusy )
      {
         if ( ClientStatusEvent != null )
         {
            ClientStatusEvent( iBusy );
         }

      }/* end method */


      /// <summary>
      /// Synch status event to parent UI thread
      /// </summary>
      /// <param name="iBusy"></param>
      protected void ReturnClientStatusEvent( Boolean iBusy )
      {
         if ( mParent.InvokeRequired )
         {
            SynchUIClientStatus theDelegate = new SynchUIClientStatus( ReturnClientStatusEvent );
            mParent.Invoke( theDelegate, new object[] { iBusy } );
         }
         else
         {
            SetClientStatus( iBusy );
         }

      }/* end method */


      /// <summary>
      /// Delegate support method: Raised whenever the current state of
      /// the server connection is changed (either connected or disconnected)
      /// or an error condition is raised that needs to be returned to the 
      /// device user
      /// </summary>
      /// <param name="iConnectionState">Is the device currently connected</param>
      /// <param name="iCode">Connection status code</param>
      protected void SetConnectState( Boolean iConnectionState, ConnectionCode iCode )
      {
         if ( ConnectionStateEvent != null )
         {
            ConnectionStateEvent( iConnectionState, iCode );
         }

      }/* end method */


      /// <summary>
      /// Synch connection event to parent UI thread
      /// </summary>
      /// <param name="iConnectionState">Is the device currently connected</param>
      /// <param name="iCode">Connection status code</param>
      protected void ConnectionEventRaised( Boolean iConnectionState, ConnectionCode iCode )
      {
         if ( mParent.InvokeRequired )
         {
            SynchUIConnectionState theDelegate = new SynchUIConnectionState( ConnectionEventRaised );
            mParent.Invoke( theDelegate, new object[] { iConnectionState, iCode } );
         }
         else
         {
            SetConnectState( iConnectionState, iCode );
         }

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Public Constructors

      /// <summary>
      /// Base Constructor:
      /// </summary>
      /// <param name="iSender">reference to parent object</param>
      public TCPCommsBase( object iSender )
      {
         // reference the parent form (for event thread synching)
         mParent = (Form) iSender;

      }/* base constructor */

      #endregion

      //------------------------------------------------------------------------

      #region Public Properties

      /// <summary>
      /// Property: Get or set the connection host name
      /// </summary>
      public String SocketHost
      {
         get
         {
            return mHostName;
         }
         set
         {
            mHostName = value;
         }
      }


      /// <summary>
      /// Property: Get or set the connection port number
      /// </summary>
      public Int16 TcpPortNumber
      {
         get
         {
            return mPortNumber;
         }
         set
         {
            mPortNumber = value;
         }
      }


      /// <summary>
      /// Property: Use either the IP address or Hostname
      /// </summary>
      public Boolean UseIpAddress
      {
         get
         {
            return mUseIPAddress;
         }
         set
         {
            mUseIPAddress = value;
         }
      }

      #endregion

      //------------------------------------------------------------------------

      #region Core comms methods

      /// <summary>
      /// Connect to the Middleware Device Server
      /// </summary>
      public virtual void Connect()
      {
      }/* end method */


      /// <summary>
      /// Disconnect from the Device Server. This is a somewhat "messy"
      /// way of doing things, with a number of read\write exceptions
      /// being raised. It is however, the method that Microsoft actually
      /// stipulate in their documentation!
      /// </summary>
      public virtual void Disconnect()
      {
      }/* end method */


      /// <summary>
      /// This method returns the IP Address of the Host in either IPV4 or IPV6
      /// format. Although IPV4 is fine for XP machines and Windows Server 2003,
      /// for Vista (or Windows 7) and Server 2008, IPV6 is required.
      /// </summary>
      /// <param name="iHostName">Hostname of the target device</param>
      /// <param name="addressFamily">Address family type (i.e. IPV4 etc)</param>
      /// <returns></returns>
      protected IPAddress GetIPAddress( string iHostName, AddressFamily addressFamily )
      {
         try
         {
            IPAddress[] addresses = Dns.GetHostEntry( iHostName ).AddressList;

            // Try to avoid problems with IPV6 addresses
            foreach ( IPAddress address in addresses )
            {
               if ( address.AddressFamily == addressFamily )
               {
                  return address;
               }
            }
            return addresses[ 0 ];
         }
         catch ( Exception ex )
         {
            // If it's not possible to get the list of IP adress associated to 
            // the Data Source we try to check if Data Source is already an IP Address
            // and return it
            try
            {
               return IPAddress.Parse( iHostName );
            }
            catch
            {
               // In this case we want to rethrow the first exception
               throw ex;
            }
         }

      }/* end method */


      /// <summary>
      /// Connect to the Middleware Device Server
      /// </summary>
      /// <param name="iUseIPAddress">null</param>
      protected virtual void ConnectToServer( object result )
      {
      }/* end method */


      /// <summary>
      /// Process management for on Connection event 
      /// </summary>
      /// <param name="ar">pointer to interface ar</param>
      protected virtual void OnConnect( IAsyncResult ar )
      {
      }/* end method */


      /// <summary>
      /// Point receive data event to 
      /// </summary>
      protected virtual void WaitForNextData( ClientStateObject iStateObject )
      {
      }/* end method */


      /// <summary>
      /// Action on data received * END HERE *
      /// </summary>
      /// <param name="asyn"></param>
      protected virtual void OnDataReceived( IAsyncResult asyn )
      {
      }/* end method */


      /// <summary>
      /// Send a byte array message to the Device Server
      /// </summary>
      /// <param name="iBuffer">Formatted byte array</param>
      protected virtual void SendMessage( byte[] iBuffer )
      {
      }/* end method */


      /// <summary>
      /// This method process all receives and deserializes all received
      /// byte array data. Given that all packets are braced between the
      /// fields "ACK" and "/ACK", datagrams not correctly terminated are
      /// simply rejected, and the buffer re-read when more bytes have been
      /// revived from the server.
      /// Note: This method operates within the COMMS thread - and so has
      /// no connectivity with the user interface!
      /// </summary>
      /// <param name="iStateObject">Current State Object</param>
      protected virtual void ProcessReceiveBytes( ClientStateObject iStateObject )
      {
      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Socket Exception handler

      /// <summary>
      /// Handle all fatal Socket Exceptions
      /// </summary>
      /// <param name="ex"></param>
      protected virtual void HandleSocketException( SocketException ex )
      {
      }/* end method */

      /// <summary>
      /// Handle all Socket Exceptions (fatal and non-fatal)
      /// </summary>
      /// <param name="ex"></param>
      /// <param name="iStateObject"></param>
      protected virtual void HandleCommsException( SocketException ex,
         ClientStateObject iStateObject )
      {
      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region DateTime and Locale Processing

      /// <summary>
      /// Set the DeviceTimeZone based on data within the Profile Packet
      /// </summary>
      /// <param name="packet">Server Profile Packet</param>
      protected void SetDeviceTimeZone( PacketProfile packet )
      {
         // create a timezone manager object
         IDeviceTimeManager timeManager = new DeviceTimeManager();

         // get the timezone information for this device
         IDeviceTimeZoneInformation tziDevice = timeManager.GetTimeZoneInformation();

         // create a new timezone record
         TimeZoneRecord tzrDevice = new TimeZoneRecord();

         // .. and populate with the time tone info
         tzrDevice.Bias = tziDevice.Bias;
         tzrDevice.DaylightBias = tziDevice.DaylightBias;
         tzrDevice.DaylightName = tziDevice.DaylightName;
         tzrDevice.DSTDay = tziDevice.DSTDay;
         tzrDevice.DSTHour = tziDevice.DSTHour;
         tzrDevice.DSTMonth = tziDevice.DSTMonth;
         tzrDevice.StandardBias = tziDevice.StandardBias;
         tzrDevice.StandardName = tziDevice.StandardName;
         tzrDevice.STDay = tziDevice.STDay;
         tzrDevice.STHour = tziDevice.STHour;
         tzrDevice.STMonth = tziDevice.STMonth;

         // get the target timezone record from the server profile packet
         TimeZoneRecord tzrServer = packet.Profile.DeviceTimeZoneInfo;

         //
         // compare the target timezone to the current device timezone. If they
         // don't match then we need to update the timezone in the device to
         // the timezone referenced by the server
         //
         if ( !CompareTimeZoneRecords( tzrDevice, tzrServer ) )
         {
            // create new TimeZoneInfo object
            IDeviceTimeZoneInformation tziServer = new DeviceTimeZoneInformation();

            //
            // These fields are left blank to correct for a bug within WinCE
            // that returns bogus start and end summer time dates
            //
            tziServer.STDay = packet.Profile.DeviceTimeZoneInfo.STDay;
            tziServer.STHour = packet.Profile.DeviceTimeZoneInfo.STHour;
            tziServer.STMonth = packet.Profile.DeviceTimeZoneInfo.STMonth;
            tziServer.DSTDay = packet.Profile.DeviceTimeZoneInfo.DSTDay;
            tziServer.DSTHour = packet.Profile.DeviceTimeZoneInfo.DSTHour;
            tziServer.DSTMonth = packet.Profile.DeviceTimeZoneInfo.DSTMonth;
            tziServer.CurrentYear = packet.Profile.DeviceTimeZoneInfo.CurrentYear;

            // .. and populate with the server TimeZoneRecord
            tziServer.Bias = packet.Profile.DeviceTimeZoneInfo.Bias;
            tziServer.DaylightBias = packet.Profile.DeviceTimeZoneInfo.DaylightBias;
            tziServer.DaylightName = packet.Profile.DeviceTimeZoneInfo.DaylightName;
            tziServer.StandardBias = packet.Profile.DeviceTimeZoneInfo.StandardBias;
            tziServer.StandardName = packet.Profile.DeviceTimeZoneInfo.StandardName;

            // set the TimeZoneInformation
            timeManager.SetTimeZoneInformation( tziServer );
         }

      }/* end method */


      /// <summary>
      /// This method simply compares the contents of two timezone records
      /// </summary>
      /// <param name="tzrDevice">Device TimeZoneRecord</param>
      /// <param name="tzrServer">ServerTimeZoneRecord</param>
      /// <returns>true if object are the same</returns>
      protected bool CompareTimeZoneRecords( TimeZoneRecord tzrDevice, TimeZoneRecord tzrServer )
      {
         if ( ( tzrDevice != null ) & ( tzrServer != null ) )
         {
            if ( ( tzrServer.Bias == tzrDevice.Bias ) &
               ( tzrServer.DaylightName == tzrDevice.DaylightName ) &
               ( tzrServer.DaylightBias == tzrDevice.DaylightBias ) &
               ( tzrServer.DSTDay == tzrDevice.DSTDay ) &
               ( tzrServer.DSTHour == tzrDevice.DSTHour ) &
               ( tzrServer.DSTMonth == tzrDevice.DSTMonth ) &
               ( tzrServer.StandardBias == tzrDevice.StandardBias ) &
               ( tzrServer.StandardName == tzrDevice.StandardName ) &
               ( tzrServer.STDay == tzrDevice.STDay ) &
               ( tzrServer.STHour == tzrDevice.STHour ) &
               ( tzrServer.STMonth == tzrDevice.STMonth ) )
            {
               return true;
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Synchronize the device to the server time and correct for
      /// errors caused by shifting this time across the daylight
      /// saving and standard time boundries
      /// </summary>
      /// <param name="packet">Session packet object</param>
      protected static void SynchDeviceToServerTime( PacketSession iPacket )
      {
         // convert server time to UTC
         mServerTime = iPacket.Session.Begins.ToUniversalTime();

         // synchronize the device time
         SetDeviceTime(  );

      }/* end method */


      /// <summary>
      /// Synchronize the device time with the server time
      /// </summary>
      /// <param name="iServerTime"></param>
      protected static void SetDeviceTime( )
      {
         // create a time manager object
         IDeviceTimeManager timeManager = new DeviceTimeManager();

         // force the server to the correct time
         timeManager.SetDeviceSystemTime( mServerTime );

      }/* end method */


      /// <summary>
      /// Returns the Server's Kernel32.TimeZoneInfo structure as
      /// a populated TimeZoneRecord object
      /// </summary>
      /// <returns>Populated TimeZoneRecord object</returns>
      protected TimeZoneRecord GetTimeZoneRecord()
      {
         // create a timezoen manager object
         IDeviceTimeManager timeManager = new DeviceTimeManager();

         // now get the timezone information for this server
         IDeviceTimeZoneInformation tzi = timeManager.GetTimeZoneInformation();

         // create a new timezone record
         TimeZoneRecord tzr = new TimeZoneRecord();

         // .. and populate
         tzr.Bias = tzi.Bias;
         tzr.DaylightBias = tzi.DaylightBias;
         tzr.DaylightName = tzi.DaylightName;
         tzr.DSTDay = tzi.DSTDay;
         tzr.DSTHour = tzi.DSTHour;
         tzr.DSTMonth = tzi.DSTMonth;
         tzr.StandardBias = tzi.StandardBias;
         tzr.StandardName = tzi.StandardName;
         tzr.STDay = tzi.STDay;
         tzr.STHour = tzi.STHour;
         tzr.STMonth = tzi.STMonth;

         // return the timezone record
         return tzr;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Registry Read/Write

      /// <summary>
      /// Gets the current InspectorPackets version
      /// </summary>
      protected void GetPacketsVersion()
      {
         mPacketsVersion = VersionInfo.Current;
      }/* end method */


      /// <summary>
      /// Get the name of the last connected operator
      /// </summary>
      /// <returns></returns>
      protected string GetLastOperator()
      {
         return mRegistry.Operators.OperatorName;
      }/* end method */


      /// <summary>
      /// Gets the current firmware version
      /// </summary>
      /// <returns>firmware version object</returns>
      protected void GetCurrentFirmware()
      {
         mFirmware = new HwVersion( DeviceInfo.Get_Firmware_Version( ) );
      }/* end method */


      /// <summary>
      /// Gets the current device friendly name from the registry 
      /// </summary>
      /// <returns></returns>
      protected string GetDeviceFriendlyName()
      {
         return mRegistry.DeviceSerialNo;
      }/* end method */


      /// <summary>
      /// Sets the device friendly name
      /// </summary>
      /// <param name="iName">new device name</param>
      protected void SetDeviceFriendlyName( string iName )
      {
         mRegistry.DeviceSerialNo = iName;
      }/* end method */

      #endregion

      //------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th January 2010
//  Add to project
//------------------------------------------------------------------------------