﻿//------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 to 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using SKF.RS.MicrologInspector.Packets;
using SKF.RS.MicrologInspector.Database;
using System.Xml;
using System.Windows.Forms;
using System.Threading;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
    public class SynchSocketClient : TCPCommsBase, ISynchSocketClient, IDisposable
    {
        //------------------------------------------------------------------------

        #region Private Constants

        private const Boolean DEFAULT_USES_LICENSE_KEY = false;
        private const Boolean USE_MA_WORK_NOTIFICATIONS = true;
        private const int PROGRESS_CONNECTION_STEPS = 10;
        private const int PROGRESS_UPDATE_PROFILE_STEPS = 31;
        private const int PROGRESS_UPDATE_CMMS_STEPS = 2;
        private const int NO_ERRORS = -1;
        private const int MEDIA_UPLOAD_TIMEOUT = 10000;

        #endregion

        //------------------------------------------------------------------------

        #region Private Fields

        /// <summary>
        /// Device UID
        /// </summary>
        private String mUID;

        /// <summary>
        /// Are we licensing the device using a key
        /// </summary>
        private Boolean mDeviceKeyLicensed = DEFAULT_USES_LICENSE_KEY;

        /// <summary>
        /// Are we using @ptitude Work Notifications
        /// </summary>
        private Boolean mUseAnalystCMMS = USE_MA_WORK_NOTIFICATIONS;

        /// <summary>
        /// Synch upload packet
        /// </summary>
        private PacketUploadCompressed mUploadPacket = null;

        /// <summary>
        /// SQL Data Connection Layer
        /// </summary>
        private DataConnect mSqlConnect = null;

        /// <summary>
        /// Packet deserialization object
        /// </summary>
        private XmlDeserializer mDeserializer = null;

        /// <summary>
        /// Table updates
        /// </summary>
        private UpdateDB mUpdateDB = null;

        /// <summary>
        /// Data progress object
        /// </summary>
        private IDataProgress mProgressPosition = null;

        /// <summary>
        /// Media upload progress
        /// </summary>
        private IDataProgress mMediaUploadProgress = null;

        /// <summary>
        /// Tables were successfully updated flag
        /// </summary>
        private Boolean mTablesWereUpdated = false;

        /// <summary>
        /// Has the server raised a fatal error?
        /// </summary>
        private int mServerError = NO_ERRORS;

        /// <summary>
        /// Enable rollback of the device profile
        /// </summary>
        private DeviceProfile mProfileRollback;

        /// <summary>
        /// Track whether Dispose has been called.
        /// </summary>
        private bool mDisposed = false;

        /// <summary>
        /// Are media files to be added to the upload
        /// </summary>
        private bool mAllowSavedMedia = true;

        /// <summary>
        /// Set true to mute overall progress bar updates
        /// </summary>
        private bool mMuteOverallProgress = false;

        /// <summary>
        /// Set true if the Media upload process fails (non-fatal error)
        /// </summary>
        private bool mMediaUploadFailed = false;

        #endregion

        //------------------------------------------------------------------------

        #region Delegates and Events

        // public delegates
        public delegate void ReturnPacketData(PacketType iPacketType, byte[] iBuffer);
        public delegate void ServerException(Exception ex);
        public delegate void ClientDataException(DataConnectError iError);
        public delegate void ProgressValue(IDataProgress iDataProgress);

        // Private Delegates to synch current event thread with parent UI thread
        private delegate void SynchUIPacketData(PacketType iPacketType, byte[] iBuffer);
        private delegate void SynchUIServerException(Exception ex);
        private delegate void SynchUIClientDataException(DataConnectError iError);
        private delegate void SynchUIProgressValue(IDataProgress iDataProgress);

        /// <summary>
        /// Event raised whenever an XML data packet is received from the server
        /// </summary>
        public event ReturnPacketData ReturnPacketDataEvent;

        /// <summary>
        /// Event raised on occurance of any socket comms exception
        /// </summary>
        public event ServerException ServerExceptionEvent;

        /// <summary>
        /// Event raised on database connection or IO error
        /// </summary>
        public event ClientDataException ClientDataExceptionEvent;

        /// <summary>
        /// Event raised on change in Progress value
        /// </summary>
        public event ProgressValue ProgressValueEvent;

        #endregion

        //------------------------------------------------------------------------

        #region Public Constructors and Destructors

        /// <summary>
        /// Base Constructor:
        /// </summary>
        /// <param name="iSender">reference to parent object</param>
        public SynchSocketClient(object iSender) :
           this(iSender, DEFAULT_UID)
        {
        }/* base constructor */


        /// <summary>
        /// Overload Constructor: with reference to DataConnect
        /// </summary>
        /// <param name="iSender">reference to parent object</param>
        /// <param name="iDataConnect">reference to SQL DataConnection object</param>
        public SynchSocketClient(object iSender, DataConnect iDataConnect) :
           this(iSender, DEFAULT_UID, iDataConnect)
        {
        }/* base constructor */


        /// <summary>
        /// Overload Constructor: 
        /// </summary>
        /// <param name="iSender">reference to parent object</param>
        /// <param name="iUID">device UID</param>
        /// <param name="iFirmware">current firmware version</param>
        public SynchSocketClient(object iSender, String iUID) :
           this(iSender, iUID, null)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor: with reference to DataConnect
        /// </summary>
        /// <param name="iSender">reference to parent object</param>
        /// <param name="iUID">device UID</param>
        /// <param name="iFirmware">current firmware version</param>
        /// <param name="iDataConnect">reference to SQL DataConnection object</param>
        public SynchSocketClient(object iSender, String iUID,
           DataConnect iDataConnect)
           : base(iSender)
        {
            // reference the parent form (for event thread synching)
            mParent = (Form)iSender;

            // get the current firmware version from the registry
            GetCurrentFirmware();

            //
            // get the current packets version (we use this to confirm
            // comms between the device and the server
            //
            GetPacketsVersion();

            // get the device UID
            mUID = iUID;

            // create (or reference) the SqlCe connection object
            if (iDataConnect == null)
                mSqlConnect = new DataConnect(iSender);
            else
                mSqlConnect = iDataConnect;

            // manage current progress events returned from the database 
            mSqlConnect.ProgressValueEvent +=
                new DataConnect.ProgressValue(OnCurrentProgressUpdate);

            // manage external SQL error events
            mSqlConnect.ErrorEvent += new DataConnect.DataError(OnSqlErrorEvent);

            // create the local data progress object and target as "overall" 
            mProgressPosition = new DataProgress();
            mProgressPosition.Target = ProgressTarget.Overall;

            // set the default conditions (maximum)
            SetTotalProgressSteps(true, false);

            // manage overall progress events raised
            mProgressPosition.OnUpdate +=
                new DataProgress.ProgressPosition(OnOverallProgressUpdate);

            // create a secondary progress update object for media processing
            mMediaUploadProgress = new DataProgress();
            mMediaUploadProgress.Target = ProgressTarget.Current;

            // pass events through the current progress value
            mMediaUploadProgress.OnUpdate +=
               new DataProgress.ProgressPosition(OnCurrentProgressUpdate);

            // create the deserializer object
            mDeserializer = new XmlDeserializer();

            // manage current progress events returned from the deserializer 
            mDeserializer.ProgressPosition.OnUpdate +=
                new DataProgress.ProgressPosition(OnCurrentProgressUpdate);

            // create the table update manager
            mUpdateDB = new UpdateDB(mSqlConnect, mDeserializer, mProgressPosition);

        }/* end constructor */


        /// <summary>
        /// Implement IDisposable.
        /// Note: A derived class must not be able to override this method!
        /// </summary>
        public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            //
            // tell the Garbage Collector that the Finalize process no 
            // longer needs to be run for this object.
            //
            GC.SuppressFinalize(this);

        }/*end dispose */


        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios.
        /// If disposing equals true, the method has been called directly
        /// or indirectly by a user's code. Managed and unmanaged resources
        /// can be disposed.
        /// If disposing equals false, the method has been called by the
        /// runtime from inside the finalizer and you should not reference
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="iDisposeManagedResources"></param>
        private void Dispose(bool iDisposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (iDisposing)
                {
                    try
                    {
                        Disconnect();
                        //
                        // only dispose of StateObject if it has not already
                        // marked for collected by the GC
                        //
                        if ((!mStateObjectDisposing) && (mStateObject != null))
                        {
                            // dump the state object
                            mStateObject.Dispose();
                            mStateObject = null;
                            mStateObjectDisposing = true;
                        }

                        // release all progress events
                        if (mProgressPosition != null)
                            mProgressPosition.OnUpdate -= OnOverallProgressUpdate;

                        if (mMediaUploadProgress != null)
                            mMediaUploadProgress.OnUpdate -= OnCurrentProgressUpdate;

                        if (mDeserializer != null)
                            mDeserializer.ProgressPosition.OnUpdate -= OnCurrentProgressUpdate;

                        if (mSqlConnect != null)
                            mSqlConnect.ProgressValueEvent -= OnCurrentProgressUpdate;
                    }
                    finally
                    {
                        mUploadPacket = null;
                        if (mSqlConnect != null)
                        {
                            mSqlConnect.Dispose();
                            mSqlConnect = null;
                        }
                    }
                }
                // Note disposing has been done.
                mDisposed = true;
            }
        }/*end dispose */


        /// <summary>
        /// This destructor will run only if the Dispose method
        /// does not get called.
        /// It gives your base class the opportunity to finalize.
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~SynchSocketClient()
        {
            Dispose(false);

        }/*end dispose */

        #endregion

        //------------------------------------------------------------------------

        #region Public Properties

        /// <summary>
        /// Gets or sets the Device UID as an encrypted 
        /// </summary>
        public String DeviceUID
        {
            get
            {
                return mUID;
            }
            set
            {
                mUID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Work Notifications soure type
        /// </summary>
        public Boolean UseAnalystCMMS
        {
            get
            {
                return mUseAnalystCMMS;
            }
            set
            {
                mUseAnalystCMMS = value;
            }
        }

        /// <summary>
        /// Gets or sets the device Firmware Version
        /// </summary>
        public HwVersion FirmwareVersion
        {
            get
            {
                return mFirmware;
            }
            set
            {
                mFirmware = value;
            }
        }

        /// <summary>
        /// Gets or sets whether the device has a manual license key installed
        /// </summary>
        public Boolean DeviceKeyLicensed
        {
            get
            {
                return mDeviceKeyLicensed;
            }
            set
            {
                mDeviceKeyLicensed = value;
            }
        }

        /// <summary>
        /// Gets whether the tables were updated
        /// </summary>
        public Boolean TablesWereUpdated
        {
            get
            {
                return mTablesWereUpdated;
            }
        }

        /// <summary>
        /// Gets any synch error code (no error = -1)
        /// </summary>
        public int SynchErrorCode
        {
            get
            {
                return mServerError;
            }
        }

        /// <summary>
        /// Gets or sets whether Saved Media should be added to the upload
        /// </summary>
        public Boolean UploadMediaFiles
        {
            get
            {
                return mAllowSavedMedia;
            }
            set
            {
                mAllowSavedMedia = value;
            }
        }

        #endregion

        //------------------------------------------------------------------------

        #region Public Methods

        /// <summary>
        /// Connect to the Middleware Device Server
        /// </summary>
        public override void Connect()
        {
            // reset the overall progress mute flag
            mMuteOverallProgress = false;

            // connect and raise error on exception
            if (mSqlConnect.Connect())
            {
                // reset the tables updated flag
                mTablesWereUpdated = false;

                // reset the server error flag
                mServerError = NO_ERRORS;

                // reset the media upload failure flag
                mMediaUploadFailed = false;

                // reset the overall progress position
                mProgressPosition.Reset();

                // update the progress position
                mProgressPosition.Update(true);

                // we are now officialy busy!
                ReturnClientStatusEvent(true);

                // Queue the task and data.
                if (ThreadPool.QueueUserWorkItem(new WaitCallback(ConnectToServer), null))
                {
                    Thread.Sleep(200);
                }
            }
            else
            {
                // create error object and return
                ReturnClientDataError(TableName.ALL, ErrorType.Connection);
            }

        }/* end method */


        /// <summary>
        /// Disconnect from the Device Server. This is a somewhat "messy"
        /// way of doing things, with a number of read\write exceptions
        /// being raised. It is however, the method that Microsoft actually
        /// stipulate in their documentation!
        /// </summary>
        public override void Disconnect()
        {
            try
            {
                if (mClientSocket != null)
                {
                    if (mClientSocket.Connected)
                    {
                        // disable all socket send and end receives
                        mClientSocket.Shutdown(SocketShutdown.Both);

                        // take a break!
                        System.Threading.Thread.Sleep(100);

                        // close and dispose!
                        mClientSocket.Close();
                        mClientSocket = null;

                        // return the disconnection event
                        if (mMediaUploadFailed)
                        {
                            ConnectionEventRaised(false, ConnectionCode.ERR_MEDIA_UPLOAD_FAILURE);
                        }
                        else
                        {
                            ConnectionEventRaised(false, ConnectionCode.DISCONNECTED);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        /// <summary>
        /// Send a single ACK query to the Device Server
        /// </summary>
        public void SendACK()
        {
            try
            {
                if (mClientSocket != null)
                {
                    if (mClientSocket.Connected)
                    {
                        SendNULL();
                    }
                    else
                    {
                        ConnectionEventRaised(false, ConnectionCode.ERR_GENERAL);
                    }
                }
                else
                {
                    ConnectionEventRaised(false, ConnectionCode.ERR_NO_SERVER);
                }
            }

            // an attempt was made to send data with no available server connection
            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Process events for raised packet data

        /// <summary>
        /// Raise event on update to packet data status (Primarily for Debug purposes)
        /// </summary>
        /// <param name="iPacketType">current packet</param>
        /// <param name="iBuffer">xml packet data</param>
        private void RaisePacketDataEvent(PacketType iPacketType, byte[] iBuffer)
        {
            if (ReturnPacketDataEvent != null)
            {
                ReturnPacketDataEvent(iPacketType, iBuffer);
            }

        }/* end method */


        /// <summary>
        /// Synch updates to the packet data with the parent UI thread
        /// </summary>
        /// <param name="iPacketType">current packet</param>
        /// <param name="iBuffer">xml packet data</param>
        private void ReturnPacketDataUpdates(PacketType iPacketType, byte[] iBuffer)
        {
            if (mParent.InvokeRequired)
            {
                SynchUIPacketData theDelegate = new SynchUIPacketData(ReturnPacketDataUpdates);
                mParent.Invoke(theDelegate, new object[] { iPacketType, iBuffer });
            }
            else
            {
                RaisePacketDataEvent(iPacketType, iBuffer);
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Process events for raised exceptions

        /// <summary>
        /// Raise event on any exceptions thrown during connection to server
        /// </summary>
        /// <param name="ex">exception object</param>
        private void RaiseServerExceptionEvent(Exception ex)
        {
            // disconnect from the server
            Disconnect();

            // raise exception event
            if (ServerExceptionEvent != null)
            {
                ServerExceptionEvent(ex);
            }

            // notify connections that we are no longer busy
            ReturnClientStatusEvent(false);

        }/* end method */


        /// <summary>
        /// Synch exception raised event with the parent UI thread
        /// </summary>
        /// <param name="ex">exception object</param>
        protected void ReturnServerException(Exception ex)
        {
            // reset the timeout option
            receiveDone.Set();

            if (mParent.InvokeRequired)
            {
                SynchUIServerException theDelegate = new SynchUIServerException(ReturnServerException);
                mParent.Invoke(theDelegate, new object[] { ex });
            }
            else
            {
                // notify connections that we are no longer busy
                if (!ex.ToString().Contains("NullReferenceException"))
                {
                    RaiseServerExceptionEvent(ex);
                }
                else
                {
                    ConnectionEventRaised(false, ConnectionCode.ERR_TCP_DROPPED);
                }
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Process events for DataConnect errors

        /// <summary>
        /// Raise event on error returned from DataConnection layer  
        /// </summary>
        /// <param name="iError">Error definition object</param>
        protected void RaiseClientDataException(DataConnectError iError)
        {
            if (ClientDataExceptionEvent != null)
            {
                ClientDataExceptionEvent(iError);
            }

        }/* end method */


        /// <summary>
        /// Raise event on error returned from DataConnection layer and
        /// sync to the UI thread
        /// </summary>
        /// <param name="iError">Error definition object</param>
        protected void RaiseClientDataExceptionAsync(DataConnectError iError)
        {
            if ((mParent != null) && (mParent.InvokeRequired))
            {
                SynchUIClientDataException theDelegate = new SynchUIClientDataException(RaiseClientDataExceptionAsync);
                mParent.Invoke(theDelegate, new object[] { iError });
            }
            else
            {
                RaiseClientDataException(iError);
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Process events for the Progress value

        /// <summary>
        /// Raise event on update to cuurent progress position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="iValue"></param>
        private void RaiseProgressUpdateEvent(IDataProgress iDataProgress)
        {
            if (ProgressValueEvent != null)
            {
                ProgressValueEvent(iDataProgress);
            }

        }/* end method */


        /// <summary>
        /// Synch progress updates with the parent thread
        /// </summary>
        /// <param name="iValue"></param>
        private void ReturnProgressPosition(IDataProgress iDataProgress)
        {
            if (mParent.InvokeRequired)
            {
                SynchUIProgressValue theDelegate = new SynchUIProgressValue(ReturnProgressPosition);
                mParent.Invoke(theDelegate, new object[] { iDataProgress });
            }
            else
            {
                RaiseProgressUpdateEvent(iDataProgress);
            }

        }/* end method */


        /// <summary>
        /// Raise a change in progress position event
        /// </summary>
        /// <param name="Sender"></param>
        private void OnCurrentProgressUpdate(object Sender)
        {
            IDataProgress progress = (IDataProgress)Sender;
            ReturnProgressPosition(progress);

        }/* end constructor */


        /// <summary>
        /// Update the overall progress position
        /// </summary>
        /// <param name="Sender"></param>
        private void OnOverallProgressUpdate(object Sender)
        {
            ReturnProgressPosition((IDataProgress)Sender);
        }

        #endregion

        //------------------------------------------------------------------------

        #region Process events for DataConnection Errors

        /// <summary>
        /// Check for fatal comms error
        /// </summary>
        /// <param name="iErrorCode"></param>
        private void OnSqlErrorEvent(DataConnectError iError)
        {
            // return any exception raised
            RaiseClientDataExceptionAsync(iError);

            // if error was fatal then abort here
            if (iError.IOErrorType == ErrorType.Connection)
            {
                Disconnect();
                ReturnClientStatusEvent(false);
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        /// <summary>
        /// Construct the upload packet and return as an array of compressed bytes
        /// </summary>
        /// <returns>null on error raised</returns>
        protected PacketUploadCompressed BuildUploadPacket()
        {
            try
            {
                Console.WriteLine("Building upload packet!");
                // allow saved media files (redo with property)
                mSqlConnect.Upload.AddSavedMediaRecords = mAllowSavedMedia;

                // get the timezone info data for this device
                TimeZoneRecord timeZoneInfo = GetTimeZoneRecord();

                string lastOperator = GetLastOperator();

                // build a compressed upload file
                PacketUploadCompressed upload =
                   mSqlConnect.Upload.BuildCompressedUpload(mUID, lastOperator, timeZoneInfo);

                // and return!
                return upload;
            }
            catch
            {
                return null;
            }

        }/* end method */

        //------------------------------------------------------------------------

        #region Manage encryption

        /// <summary>
        /// Convert the Device UID into a hashed UID. The purpose of this 
        /// method is to ensure that the actual Device UID is never actually
        /// transmitted in readable format
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <returns>Hashed UID</returns>
        private string HashUID(string iUID)
        {
            XmlEncryption encrypt = new XmlEncryption();
            encrypt.SecurePassword = "20" +
                                     PacketBase.UUID_HASH1 +
                                     PacketBase.UUID_HASH2;
            return encrypt.EncryptStringToHex(iUID);

            //return "B1BE8A65B3E5EF559681F83DE1FE5CA571E2AE9DB0F359F6EA7BDC116EB33A4B46AA2B51B504A6C6C10D05372A7E3979";

        }/* end method */


        /// <summary>
        /// Decrypt an encryped UID
        /// </summary>
        /// <param name="iUID">encrypted UID</param>
        /// <returns>original (device) UID</returns>
        private string DecryptedUID(string iUID)
        {
            XmlEncryption encrypt = new XmlEncryption();
            encrypt.SecurePassword = "20" + PacketBase.UUID_HASH1 + PacketBase.UUID_HASH2;
            return encrypt.DecryptHexToString(iUID);
        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Socket Communications

        /// <summary>
        /// Connect to the Middleware Device Server
        /// </summary>
        /// <param name="iUseIPAddress">null</param>
        protected override void ConnectToServer(object result)
        {
            // Build the Upload file and reference
            mUploadPacket = BuildUploadPacket();

            try
            {
                // Create the socket instance
                mClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // reset the state object disposing flag
                mStateObjectDisposing = false;

                //
                // if use IP address then get address directly, otherwise 
                // use the hostname instead
                //
                if (UseIpAddress)
                {
                    mIpAddress = IPAddress.Parse(SocketHost);
                }
                else
                {
                    mIpAddress = GetIPAddress(SocketHost, AddressFamily.InterNetwork);
                }

                // Create the end point 
                IPEndPoint ipEnd = new IPEndPoint(mIpAddress, TcpPortNumber);

                // reference method to manage connection events
                AsyncCallback onconnect = new AsyncCallback(OnConnect);

                // initiate the new connection event
                mClientSocket.BeginConnect(ipEnd, onconnect, mClientSocket);
            }

            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        /// <summary>
        /// Process management for on Connection event 
        /// </summary>
        /// <param name="ar">pointer to interface ar</param>
        protected override void OnConnect(IAsyncResult ar)
        {
            try
            {
                if (mClientSocket.Connected)
                {
                    // return status
                    ConnectionEventRaised(true, ConnectionCode.CONNECTED);

                    // create a new client state object
                    ClientStateObject stateObject = new ClientStateObject(mClientSocket);

                    // reset the current packet type
                    stateObject.CurrentPacket = PacketType.UNKNOWN;

                    // reference any media files that are also to be uploaded
                    stateObject.MediaReady = mUploadPacket.HasMedia;
                    stateObject.SavedMedia = mSqlConnect.GetUploadMediaFiles(true);

                    // make reference to this object global
                    mStateObject = stateObject;

                    // wait for data over the socket
                    WaitForNextData(stateObject);
                }
                else
                {
                    ConnectionEventRaised(false, ConnectionCode.ERR_GENERAL);
                }
            }

            catch (SocketException ex)
            {
                HandleSocketException(ex);
            }

            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        private ManualResetEvent receiveDone = new ManualResetEvent(false);

        /// <summary>
        /// Point receive data event to 
        /// </summary>
        protected override void WaitForNextData(ClientStateObject iStateObject)
        {
            try
            {
                if (mPfnCallBack == null)
                {
                    mPfnCallBack = new AsyncCallback(OnDataReceived);
                }

                receiveDone.Reset();

                // Start listening to the data asynchronously
                mAsyncResult = mClientSocket.BeginReceive(iStateObject.DataBuffer,
                                                        0, iStateObject.DataBuffer.Length,
                                                        SocketFlags.None,
                                                        mPfnCallBack,
                                                        iStateObject);

                //
                // if we're sending a media packet then we need to check 
                // that we're getting a response from the Service
                //
                if (iStateObject.CurrentPacket == PacketType.MEDIA_READY)
                {
                    if (!receiveDone.WaitOne(MEDIA_UPLOAD_TIMEOUT, false))
                    {
                        throw new SocketException(SocketErrorCodes.ConnectionTimedOut);
                    }
                }

                // reset the timeout option
                receiveDone.Set();
            }
            //
            // these exception will typically be thrown on a
            // forced disconnection from the client itself
            //
            catch (SocketException ex)
            {
                HandleCommsException(ex, iStateObject);
            }

            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        /// <summary>
        /// Action on data received
        /// </summary>
        /// <param name="asyn"></param>
        protected override void OnDataReceived(IAsyncResult asyn)
        {
            // make sure this is well protected - many exceptions from here!
            try
            {
                // reset the timeout option
                receiveDone.Set();

                // Retrieve the state object from the AsyncResult pointer
                ClientStateObject stateObject = (ClientStateObject)asyn.AsyncState;

                // make sure we're still connected
                if (stateObject.WorkSocket.Connected)
                {
                    // ok, stop receiving data (for the moment)
                    int iBytesReceived = stateObject.WorkSocket.EndReceive(asyn);

                    // how much data did we receive there?
                    if (iBytesReceived > 0)
                    {
                        // append the cache buffer contents to the main read buffer
                        if (!stateObject.AppendDataToReadBuffer(iBytesReceived))
                        {
                            throw new SocketException(SocketErrorCodes.NoBufferSpaceAvailable);
                        }

                        // process the received data packet
                        ProcessReceiveBytes(stateObject);

                        // wait for the next dataset to arrive
                        WaitForNextData(stateObject);
                    }
                    // if nothing, then clear the platform!
                    else
                    {
                        stateObject.WorkSocket.Shutdown(SocketShutdown.Both);
                        stateObject.WorkSocket.Close();
                    }
                }
            }

            // handle socket exceptions
            catch (SocketException ex)
            {
                HandleSocketException(ex);
            }

            // handle all other exception types
            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        /// <summary>
        /// Send a byte array message to the Device Server
        /// </summary>
        /// <param name="iBuffer">Formatted byte array</param>
        protected override void SendMessage(byte[] iBuffer)
        {
            // update the overall progress
            if (!mMuteOverallProgress)
                mProgressPosition.Update(true);

            try
            {
                if (mClientSocket != null)
                {
                    if (mClientSocket.Connected)
                    {
                        mClientSocket.Send(iBuffer);
                    }
                    else
                    {
                        ConnectionEventRaised(false, ConnectionCode.ERR_GENERAL);
                    }
                }
            }

            // handle socket exceptions
            catch (SocketException ex)
            {
                HandleSocketException(ex);
            }

            // handle all other exception types
            catch (Exception ex)
            {
                ReturnServerException(ex);
            }

        }/* end method */


        /// <summary>
        /// Handle all Socket Exceptions (fatal and non-fatal)
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="iStateObject"></param>
        protected override void HandleCommsException(SocketException ex,
           ClientStateObject iStateObject)
        {
            if (ex.ErrorCode == SocketErrorCodes.ConnectionTimedOut)
            {
                if (iStateObject.CurrentPacket == PacketType.MEDIA_READY)
                {
                    // set the media upload flag to failed
                    mMediaUploadFailed = true;
                    //
                    // as the server is actually unable to receive media
                    // files, then reset the MediaReady flag, and have another
                    // attempt at finalizing the comms
                    //
                    iStateObject.MediaReady = false;
                    FinalizeCommsOrSendMedia(iStateObject);
                }
            }
            else
            {
                HandleSocketException(ex);
            }

        }/* end method */


        /// <summary>
        /// Handle all fatal Socket Exceptions
        /// </summary>
        /// <param name="ex"></param>
        protected override void HandleSocketException(SocketException ex)
        {
            // notify connections that we are no longer busy
            ReturnClientStatusEvent(false);

            //
            // If the error was related to an unexpected disconnection 
            // (i.e. The operator pulled the device from the cradle), then
            // we need to return an exception
            //
            if ((ex.ErrorCode == SocketErrorCodes.InterruptedFunctionCall)
            | (ex.ErrorCode == SocketErrorCodes.SocketOperationOnNonSocket))
            {
                // close the open socket
                if (mClientSocket != null && mClientSocket.Connected)
                {
                    mClientSocket.Close();
                }

                ConnectionEventRaised(false, ConnectionCode.ERR_UNCRADLED);
            }

            //
            // if the problem was memory related then flag as Data Quality
            //
            else if (ex.ErrorCode == SocketErrorCodes.NoBufferSpaceAvailable)
            {
                // close the open socket
                if (mClientSocket != null && mClientSocket.Connected)
                {
                    // disable all socket send and end receives
                    mClientSocket.Shutdown(SocketShutdown.Both);

                    // take a break!
                    System.Threading.Thread.Sleep(100);

                    // close and dispose!
                    mClientSocket.Close();
                    mClientSocket = null;
                }

                ConnectionEventRaised(false, ConnectionCode.ERR_DATA_QUALITY);
            }

            //
            // if the Device Profile table contained something then
            // rollback the contents, otherwise (assuming the table
            // was originally empty), delete the contents
            //
            if (mProfileRollback != null & mSqlConnect != null)
            {
                if ((mProfileRollback.ProfileID != string.Empty) &
                   (mProfileRollback.ProfileName != string.Empty) &
                   (mProfileRollback.DeviceName != string.Empty))
                {
                    ProfileSettings pfs = new ProfileSettings();

                    pfs.DeviceName = mProfileRollback.DeviceName;
                    pfs.ProfileName = mProfileRollback.ProfileName;
                    pfs.ProfileId = mProfileRollback.ProfileID;

                    mSqlConnect.ProfileWasUpdated(pfs);
                }
                else
                {
                    mSqlConnect.DeleteCurrentProfile();
                }
            }

            //
            // set the tables updated flag. Note this does not signify
            // that the tables were updated without error, but rather
            // that the process actually reached this point!
            //
            mTablesWereUpdated = true;

            //we're done here, lets dispose of the state object
            if (!mStateObjectDisposing)
            {
                mStateObjectDisposing = true;
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Process Received Byte Data

        /// <summary>
        /// This method process all receives and deserializes all received
        /// byte array data. Given that all packets are braced between the
        /// fields "ACK" and "/ACK", datagrams not correctly terminated are
        /// simply rejected, and the buffer re-read when more bytes have been
        /// revived from the server.
        /// Note: This method operates within the COMMS thread - and so has
        /// no connectivity with the user interface!
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        protected override void ProcessReceiveBytes(ClientStateObject iStateObject)
        {
            // get the server packet type
            PacketType serverPacketType = PacketBase.GetPacketType(iStateObject.ReadBuffer);
            
            // ensure packet type is known
            if (serverPacketType != PacketType.UNKNOWN)
            {
                Console.WriteLine("Packet: " + serverPacketType.ToString());

                switch (serverPacketType)
                {
                    // for packet type 'UR' (on connection)
                    case PacketType.UR:
                        ProcessPacketUR(iStateObject);
                        break;

                    // for packet type 'UR' (on connection)
                    case PacketType.NULL_BUSY:
                        ProcessPacketBUSY(iStateObject);
                        break;

                    // for packet type 'Null Reply' (response to Null Query)
                    case PacketType.NULL_REPLY:
                        ProcessPacketNullReply(iStateObject);
                        break;

                    // for packet type 'Unknown Device' (response to IAM)
                    case PacketType.UNKNOWN_DEVICE:
                        ProcessPacketUnknownDevice(iStateObject);
                        break;

                    // for packet 'Synchronization Failed' (any time event)
                    case PacketType.SYNCH_FAILED:
                        ProcessPacketSynchFailure(iStateObject);
                        break;

                    // for packet type 'No Valid License' (response to IAM)
                    case PacketType.NO_VALID_LICENSE:
                        ProcessPacketNoValidLicense(iStateObject);
                        break;

                    // for packet type 'SOFTWARE_LICENSE_EXPIRED' (on connection)
                    case PacketType.SOFTWARE_LICENSE_EXPIRED:
                        ProcessPacketServerUnlicensedLicense(iStateObject);
                        break;

                    // for packet type 'FIRMWARE_LICENSE_REFUSED' (on connection)
                    case PacketType.FIRMWARE_LICENSE_REFUSED:
                        ProcessPacketFirmwareLicenseRefused(iStateObject);
                        break;

                    // for packet type 'Profile'
                    case PacketType.PROFILE:
                        ProcessPacketProfile(iStateObject);
                        break;

                    // for packet type 'Profile Pending'
                    case PacketType.PROFILE_PENDING:
                        ProcessProfilePending(iStateObject);
                        break;

                    // for packet type 'Compressed' (table object)
                    case PacketType.COMPRESSED:
                        ProcessPacketCompressed(iStateObject);
                        break;

                    // for packet type Session (end of authentication process)
                    case PacketType.SESSION:
                        ProcessPacketSession(iStateObject);
                        break;

                    // for packet type Session (end of authentication process)
                    case PacketType.UPLOAD_RECEIVED:
                        ProcessPacketUploadRecieved(iStateObject);
                        break;

                    case PacketType.GET_MEDIA:
                        ProcessPacketGetMedia(iStateObject);
                        break;

                    case PacketType.MEDIA_RECEIVED:
                        ProcessPacketMediaReceived(iStateObject);
                        break;

                    // if all else fails
                    default:
                        break;
                }
                // clear all internal state object buffers
                iStateObject.FlushBuffers();

                if (mServerError != NO_ERRORS)
                {
                    RollBackProfile(iStateObject);
                }
            }

        }/* end method */


        /// <summary>
        /// Process the received GET_MEDIA Packet. This packet is 
        /// issued by the server to query the device for each saved
        /// media file. As these files are transferred as fixed size
        /// blocks, the server must query for each individual array
        /// of bytes until the file can be reconstructed.
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketGetMedia(ClientStateObject iStateObject)
        {
            // set the current packet type
            iStateObject.CurrentPacket = PacketType.MEDIA_BLOCK;

            // force update on the progress bar
            mMediaUploadProgress.Update(true);

            // deserialize the received packet
            PacketGetMedia packet = PacketGetMedia.DeserializeObject<PacketGetMedia>(iStateObject.ReadBuffer);

            // construct the new media block packet
            PacketMediaBlock mediaBlock = new PacketMediaBlock();

            // extract the target byte array from the media file
            mediaBlock.Block = GetFileBlock(iStateObject, packet);

            // if for any reason the block length is less than the
            // target media block size then we can safely assume that
            // there is nothing left to read. That done ..
            if (mediaBlock.Block.Length < iStateObject.MediaBlockSize)
            {
                // .. set the End of Record flag
                mediaBlock.EOR = true;

                // .. and return EOR feedback
                FeedbackMediaFileUpload(iStateObject, packet.UID);
            }

            // reference the session ID and UID of the media file
            mediaBlock.SessionID = iStateObject.SessionID;
            mediaBlock.UID = packet.UID;

            // transmit media block packet to the server
            SendMessage(mediaBlock.XmlToByteArray(mediaBlock));

            // clear the existing media block
            mediaBlock.ClearBlock();

            // and release..
            mediaBlock = null;

        }/* end method */


        /// <summary>
        /// As each file is uploaded to the server, return feedback to notify
        /// user of the actual file as well as its size in bytes
        /// </summary>
        /// <param name="iStateObject">populated state object</param>
        /// <param name="iFileID">lookup file ID</param>
        private void FeedbackMediaFileUpload(ClientStateObject iStateObject, Guid iFileID)
        {
            iStateObject.SavedMedia.Reset();
            foreach (SavedMediaRecord smr in iStateObject.SavedMedia)
            {
                if (iFileID == smr.Uid)
                {
                    PacketFeedbackMediaBlock pfmb = new PacketFeedbackMediaBlock();
                    pfmb.FileName = smr.FileName;
                    pfmb.FileBytes = (int)smr.Size;

                    ReturnPacketDataUpdates(PacketType.MEDIA_BLOCK,
                       pfmb.XmlToByteArray(pfmb));

                    break;
                }
            }

        }/* end method */


        /// <summary>
        /// All media was received by the server so disconnect here
        /// and finish off updating any tables that were received
        /// </summary>
        /// <param name="iStateObject">Populated state object</param>
        private void ProcessPacketMediaReceived(ClientStateObject iStateObject)
        {
            // flush the media buffer
            iStateObject.FlushMediaBuffer();

            // update the overall progress position
            mProgressPosition.Update(true);

            //
            // only return the DONE packet and update the database 
            // if there were no server errors
            //
            if (mServerError == NO_ERRORS)
            {
                SendDonePacket(true);
                UpdateDownloadTables(iStateObject);
            }
            else
            {
                TerminateWithoutUpdate(iStateObject);
            }

        }/* end method */


        /// <summary>
        /// Gets an indexed array of bytes from a specified media file 
        /// </summary>
        /// <param name="iStateObject">Client state object</param>
        /// <param name="iPacket">Media query block</param>
        /// <returns>Array of media bytes</returns>
        public byte[] GetFileBlock(ClientStateObject iStateObject, PacketGetMedia iPacket)
        {
            // get the associated media record object
            SavedMediaRecord mediaRecord =
               MediaIO.GetSavedMediaInfo(iPacket.UID, iStateObject);

            // get the media file block
            if (mediaRecord != null)
            {
                return MediaIO.GetMediaBlock(
                   mediaRecord.FilePath + "\\" + mediaRecord.FileName,
                   iPacket.BlockID,
                   iStateObject.MediaBlockSize);
            }

            // ok, we failed
            return null;

        }/* end method */


        /// <summary>
        /// This method processes all Profile based tables which are passed 
        /// from the server as compressed objects
        /// </summary>
        /// <param name="iStateObject">current state object</param>
        /// <param name="iPacketCx">compressed packet</param>
        private void ProcessReceiveTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // get the host packet (table) Type
            PacketType packetType = (PacketType)iPacketCx.HostingPacket;

            // ensure table type is known
            if (packetType != PacketType.UNKNOWN)
            {
                switch (packetType)
                {
                    // for packet type 'OPERATOR SET'
                    case PacketType.OPERATOR_SET:
                        ProcessPacketOperatorSet(iStateObject, iPacketCx);
                        break;

                    // for packet type 'CODED NOTES TABLE'
                    case PacketType.TABLE_CNOTE:
                        ProcessPacketCodedNotes(iStateObject, iPacketCx);
                        break;

                    // for packet type 'CONDITIONAL POINTS TABLE'
                    case PacketType.TABLE_CONDITIONAL_POINT:
                        ProcessPacketConditionalPointTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'DERIVED ITEMS TABLE'
                    case PacketType.TABLE_DERIVED_ITEMS:
                        ProcessPacketDerivedItemsTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'DERIVED POINTS TABLE'
                    case PacketType.TABLE_DERIVED_POINT:
                        ProcessPacketDerivedPointsTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'NODE TABLE'
                    case PacketType.TABLE_NODE:
                        ProcessPacketNodeTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'POINTS TABLE'
                    case PacketType.TABLE_POINTS:
                        ProcessPacketPointsTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'PROCESS TABLE'
                    case PacketType.TABLE_PROCESS:
                        ProcessPacketProcessTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'TEXT DATA TABLE'
                    case PacketType.TABLE_TEXT_DATA:
                        ProcessPacketTextDataTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'INSPECTION TABLE'
                    case PacketType.TABLE_INSPECTION:
                        ProcessPacketInspectionTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'INSTRUCTIONS TABLE'
                    case PacketType.TABLE_INSTRUCTIONS:
                        ProcessPacketInstructionTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'MCC TABLE'
                    case PacketType.TABLE_MCC:
                        ProcessPacketMCCTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'MESSAGE TABLE'
                    case PacketType.TABLE_MESSAGE:
                        ProcessPacketMessageTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'STRUCTURED TABLE'
                    case PacketType.TABLE_STRUCTURED:
                        ProcessPacketStructuredTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'STRUCTURED TABLE'
                    case PacketType.TABLE_FFT_POINTS:
                        ProcessPacketFFTPointsTable(iStateObject, iPacketCx);
                        break;

                    // for packet type 'CMMS OPTIONS TABLE'
                    case PacketType.TABLE_CMMS_SETTINGS:
                        ProcessPacketCmmsOptions(iStateObject, iPacketCx);
                        break;

                    case PacketType.TABLE_REFERENCEMEDIA:
                        ProcessPacketReferenceMedia(iStateObject, iPacketCx);
                        break;

                    //
                    // assuming that the compressed packet is not a profile table,
                    // then uncompress the packet, resave to the state object and
                    // process as per a normal handshake packet
                    //
                    default:
                        iStateObject.ReadBuffer = iPacketCx.Decompress(iPacketCx.ByteData);
                        ProcessReceiveBytes(iStateObject);
                        break;
                }
                // clear all internal state object buffers
                iStateObject.FlushBuffers();
            }

        }/* end method */


        /// <summary>
        /// Process the received UR Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketUR(ClientStateObject iStateObject)
        {
            ReturnPacketDataUpdates(PacketType.UR, iStateObject.ReadBuffer);

            // deserialize profile packet
            PacketUR packet = PacketUR.DeserializeObject<PacketUR>(iStateObject.ReadBuffer);

            // reference the devices list
            SupportedDevices devices = packet.UR.Devices;

            // reset the device status
            Boolean deviceOK = false;

            // validate the current device type and firmware version
            for (int i = 0; i < devices.Count; ++i)
            {
                DeviceType device = devices.Supported[i];

                if (device.Device == (byte)FieldTypes.Microlog.Inspector)
                {
                    Version minVersion = device.MinVersion.GetVersion();
                    if (minVersion.Major == mPacketsVersion.GetVersion().Major)
                    {
                        deviceOK = true;
                        break;
                    }
                }
            }

            // if device validated then send IAM - else disconnect and close
            if (deviceOK)
            {
                SendIAM();
            }
            else
            {
                ConnectionEventRaised(false, ConnectionCode.ERR_FIRMWARE);
                SendFirmwareError();
                Disconnect();
            }

        }/* end method */


        /// <summary>
        /// Process the received BUSY Packet - i.e. just disconnect!
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketBUSY(ClientStateObject iStateObject)
        {
            // return feedback
            ReturnPacketDataUpdates(PacketType.NULL_BUSY, iStateObject.ReadBuffer);

            // raise error and disconnect!
            ConnectionEventRaised(false, ConnectionCode.ERR_SERVER_BUSY);
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the received 'No Valid License Found' Error Packet
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void ProcessPacketNoValidLicense(ClientStateObject iStateObject)
        {
            // return feedback
            ReturnPacketDataUpdates(PacketType.NO_VALID_LICENSE, iStateObject.ReadBuffer);

            // raise the flag and fail!
            ConnectionEventRaised(false, ConnectionCode.ERR_UNLICENSED);

            // disconnect from server
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the received '@ptitude License Expired' Error Packet
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void ProcessPacketServerUnlicensedLicense(ClientStateObject iStateObject)
        {
            // raise the flag and fail!
            ConnectionEventRaised(true, ConnectionCode.ERR_SERVER_UNLICENSED);

            // disconnect from server
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the received 'Firmware License Count Exceeded' Error Packet
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void ProcessPacketFirmwareLicenseRefused(ClientStateObject iStateObject)
        {
            // raise the flag and fail!
            ConnectionEventRaised(true, ConnectionCode.ERR_FIRMWARE_UNLICENSED);

            // disconnect from server
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the received 'Unknown Device' Error Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketUnknownDevice(ClientStateObject iStateObject)
        {
            // return feedback
            ReturnPacketDataUpdates(PacketType.UNKNOWN_DEVICE, iStateObject.ReadBuffer);

            // raise the flag and fail!
            ConnectionEventRaised(false, ConnectionCode.ERR_UNKNOWN_DEVICE);

            // disconnect from server
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the received 'Synch Failure' Error Packet and
        /// make the error code available
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketSynchFailure(ClientStateObject iStateObject)
        {
            // deserialize the synch error packet
            PacketSynchFailed packet =
               PacketBase.DeserializeObject<PacketSynchFailed>(iStateObject.ReadBuffer);

            // reference the returned error code
            mServerError = packet.ErrorCode;

            // return feedback
            ReturnPacketDataUpdates(PacketType.SYNCH_FAILED, iStateObject.ReadBuffer);

            // raise the flag and fail!
            ConnectionEventRaised(false, ConnectionCode.ERR_SYNCH_FAILED);

            // disconnect from server
            Disconnect();

        }/* end method */


        /// <summary>
        /// Process the returned session packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketSession(ClientStateObject iStateObject)
        {
            // deserialize the received profile packet
            PacketSession packet = PacketBase.DeserializeObject<PacketSession>(iStateObject.ReadBuffer);

            // synchronize device to server UTC time 
            SynchDeviceToServerTime(packet);

            // return feedback
            ReturnPacketDataUpdates(PacketType.SESSION, iStateObject.ReadBuffer);

            // reference the session ID in the state object
            iStateObject.SessionID = packet.Session.ID;

            // send the upload packet
            SendUploadPacket(iStateObject);

        }/* end method */
        

        /// <summary>
        /// Process the returned Upload Received packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketUploadRecieved(ClientStateObject iStateObject)
        {
            Console.WriteLine("Upload packet received by server!");
            // given the the upload was received by the server set flag true
            iStateObject.UploadReceivedByServer = true;

            // get the current profile to allow for a rollback
            mProfileRollback = mSqlConnect.GetCurrentProfile();

            if (!mUpdateDB.ClearCurrentProfileFlags())
            {
                ReturnClientDataError(TableName.ALL, ErrorType.Clear);
            }

            //
            // Pass the current Profile object over to the database and see if
            // the database had to be updated. Updates will be forced if any 
            // profile field has changed - or if the profile 'dirty' flag is true
            //
            Boolean profileUpdated =
               mSqlConnect.ProfileWasUpdated(iStateObject.ProfileData);

            // are there any media files to upload?
            Boolean hasMedia =
               iStateObject.SavedMedia != null && iStateObject.SavedMedia.Count > 0;

            //
            // OK, does the profile need updating? If it does then request
            // a list of updated tables from the Server, but do not request
            // an updated list of the Operator, Coded Notes or Work Notification 
            // settings - these will be issued once all other profile tables 
            // have been successfully downloaded from the server
            //
            if (profileUpdated)
            {
                SetTotalProgressSteps(true, hasMedia);
                SendUpdateProfilesPacket(iStateObject);
                iStateObject.ProfileUpdateRequired = true;
            }
            //
            // if the profile is still current, then simply request an update
            // to the operator and CMMS settings (which should be refreshed on 
            // each and every synchronization
            //
            else
            {
                SetTotalProgressSteps(false, hasMedia);
                SendUpdateOperatorSetPacket(iStateObject);
                iStateObject.ProfileUpdateRequired = false;
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        /// <summary>
        /// Send the compressed upload packet to the Server
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        protected virtual void SendUploadPacket(ClientStateObject iStateObject)
        {
            Console.WriteLine("Sending upload packet!");
            // reference the session ID
            mUploadPacket.SessionID = iStateObject.SessionID;
            
            // send packet to server
            SendMessage(mUploadPacket.XmlToByteArray(mUploadPacket));

            var str = mUploadPacket.XmlToString(mUploadPacket);

            Console.WriteLine("Sent upload packet: xml\n\n" + str + "\n\n");

        }/* end method */


        /// <summary>
        /// Process the next pending table in the device profile
        /// </summary>
        /// <param name="iStateObject"></param>
        private void SendForNextProfileTable(ClientStateObject iStateObject)
        {
            // are there any profile tables pending
            if (iStateObject.PendingTables.Count > 0)
            {
                // if yes, grab the one from the top of the pending stack
                PacketType tableType = (PacketType)iStateObject.PendingTables.Tables[0].TablePacketType;

                // now remove that table from the pending stack 
                iStateObject.PendingTables.DropPendingTable(0);

                // create a new GET TABLE packet
                PacketGetTable getTablePacket = new PacketGetTable(tableType);

                // make sure it has the correct session ID!
                getTablePacket.SessionID = iStateObject.SessionID;

                // send the GET TABLE packet to the server
                SendMessage(getTablePacket.XmlToByteArray(getTablePacket));
            }
            else
            {
                // send an "update operator set" packet to the server
                SendUpdateOperatorSetPacket(iStateObject);
            }

        }/* end method */


        /// <summary>
        /// Send a packet to the server requesting a new operator set (if available)
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void SendUpdateOperatorSetPacket(ClientStateObject iStateObject)
        {
            // construct an new "update operator set" packet
            PacketUpdateOperators updateOperatorsPackets = new PacketUpdateOperators();

            // populate the Session ID and Operator Set ID properties
            updateOperatorsPackets.SessionID = iStateObject.SessionID;
            updateOperatorsPackets.OperatorSetId = iStateObject.ProfileData.OperatorSetId;

            // send packet to server
            SendMessage(updateOperatorsPackets.XmlToByteArray(updateOperatorsPackets));

        }/* end method */


        /// <summary>
        /// Send a packet to the server requesting new CMMS settings and Work 
        /// Notifications set - assuming any are actually available!
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void SendUpdateCmmsSettingsPacket(ClientStateObject iStateObject)
        {
            // construct an new "update operator set" packet
            PacketUpdateCMMS updateCmmsPacket = new PacketUpdateCMMS();

            // populate the Session ID and Operator Set ID properties
            updateCmmsPacket.SessionID = iStateObject.SessionID;

            // send packet to server
            SendMessage(updateCmmsPacket.XmlToByteArray(updateCmmsPacket));

        }/* end method */


        /// <summary>
        /// Send a packet to the server requesting new Coded Notes
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void SendUpdateCodedNotesPacket(ClientStateObject iStateObject)
        {
            // construct an new "update coded notes" packet
            PacketUpdateCodedNotes updateCodedNotesPacket = new PacketUpdateCodedNotes();

            // populate the Session ID and Operator Set ID properties
            updateCodedNotesPacket.SessionID = iStateObject.SessionID;

            // send packet to server
            SendMessage(updateCodedNotesPacket.XmlToByteArray(updateCodedNotesPacket));

        }/* end method */


        /// <summary>
        /// Send a packet to the server requesting a new profile
        /// </summary>
        /// <param name="iStateObject">client state object</param>
        private void SendUpdateProfilesPacket(ClientStateObject iStateObject)
        {
            // construct a new "update profile" packet
            PacketUpdateProfile updateProfilePacket = new PacketUpdateProfile();

            // reference the session ID
            updateProfilePacket.SessionID = iStateObject.SessionID;

            // send packet to server
            SendMessage(updateProfilePacket.XmlToByteArray(updateProfilePacket));

        }/* end method */


        /// <summary>
        /// Create XML Serialized NULL object and return
        /// </summary>
        private void SendNULL()
        {
            PacketNullTx packet = new PacketNullTx();
            SendMessage(packet.XmlToByteArray(packet));
            packet = null;

        }/* end method */


        /// <summary>
        /// Create XML Serialized MEDIAREADY object and return
        /// </summary>
        private void SendMediaReadyPacket(ClientStateObject iStateObject)
        {
            PacketMediaReady packet = new PacketMediaReady();
            iStateObject.CurrentPacket = (PacketType)packet.Packet;
            packet.UID = HashUID(DeviceUID);
            packet.SessionID = iStateObject.SessionID;
            SendMessage(packet.XmlToByteArray(packet));
            packet = null;
        }/* end method */


        /// <summary>
        /// Create XML Serialized DONE object and return
        /// </summary>
        private void SendDonePacket(Boolean iOK)
        {
            // reset the mute flag
            mMuteOverallProgress = false;

            PacketDONE packet = new PacketDONE();
            packet.OK = iOK;
            packet.UID = HashUID(DeviceUID);
            SendMessage(packet.XmlToByteArray(packet));
            packet = null;
        }/* end method */


        /// <summary>
        /// Create XML Serialized FIRMWARE_ERROR object and return
        /// </summary>
        private void SendFirmwareError()
        {
            PacketFirmwareError packet = new PacketFirmwareError();
            packet.UID = HashUID(DeviceUID);
            packet.DevicePacketsVersion = mPacketsVersion.GetVersion().ToString();
            SendMessage(packet.XmlToByteArray(packet));
            packet = null;

        }/* end method */


        /// <summary>
        /// Create XML Serialized IAM object and return as
        /// </summary>
        private void SendIAM()
        {
            // create a new IAM packet
            PacketIAM packet = new PacketIAM();

            // add the hashed device UID
            packet.IAM.UID = HashUID(DeviceUID);

            // add the current firmware version
            packet.IAM.Firmware = FirmwareVersion;

            // add the device friendly name
            packet.IAM.DeviceName = GetDeviceFriendlyName();

            // add the license type
            packet.IAM.DeviceKeyLicensed = DeviceKeyLicensed;

            // send this packet
            SendMessage(packet.XmlToByteArray(packet));

            // and trash!
            packet = null;

        }/* end method */


        /// <summary>
        /// Create XML Serialized IAM object and return as
        /// </summary>
        private void SendAuthentication(string iAuthenticationKey)
        {
            PacketAuthenticateMe packet = new PacketAuthenticateMe();
            packet.Authenticate.Key = iAuthenticationKey;
            SendMessage(packet.XmlToByteArray(packet));
            packet = null;

        }/* end method */


        //------------------------------------------------------------------------

        #region Process Received Tables

        /// <summary>
        /// Process the received OPERATOR SET Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketOperatorSet(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.OperatorSetsCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.OPERATOR_SET, iStateObject.ReadBuffer);

            // we now need to get the Coded Notes Packet
            SendUpdateCodedNotesPacket(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received CODED NOTES Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketCodedNotes(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.CodedNotesCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_CNOTE, iStateObject.ReadBuffer);

            // Are we using CMMS via @ptitude or via a direct WS connection
            if (UseAnalystCMMS)
            {
                //
                // If we are in fact performing all CMMS tasks via @ptitude,
                // then update the CMMS Settings and Work Notifications through
                // the Transaction Server client connection
                //
                SendUpdateCmmsSettingsPacket(iStateObject);
            }
            else
            {
                // either return the DONE packet or get server to request media
                FinalizeCommsOrSendMedia(iStateObject);
            }

        }/* end method */


        /// <summary>
        /// Process the received CMMS Options Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketCmmsOptions(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.CmmsOptionsTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_CMMS_SETTINGS, iStateObject.ReadBuffer);

            // either return the DONE packet or get server to request media
            FinalizeCommsOrSendMedia(iStateObject);

        }/* end method */


        /// <summary>
        /// Check to see if there are any media files to upload. If there are
        /// then notify the server and start streaming them out, otherwise just
        /// return a DONE packet
        /// </summary>
        /// <param name="iStateObject">Does the upload contain media files?</param>
        private void FinalizeCommsOrSendMedia(ClientStateObject iStateObject)
        {
            // reset the current packet type
            iStateObject.CurrentPacket = PacketType.UNKNOWN;

            if (iStateObject.MediaReady)
            {
                // configure the "current" progress bar to show 
                SetMediaProgressSteps(iStateObject);

                // notify the server that we're good to go!
                SendMediaReadyPacket(iStateObject);

                // mute the overall progress bar while we're uploading media
                mMuteOverallProgress = true;
            }
            else
            {
                //
                // only return the DONE packet and update the database 
                // if there were no server errors
                //
                if (mServerError == NO_ERRORS)
                {
                    SendDonePacket(true);
                    UpdateDownloadTables(iStateObject);
                }
                else
                {
                    TerminateWithoutUpdate(iStateObject);
                }
            }

        }/* end method */


        /// <summary>
        /// Process the received REFERENCE MEDIA TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketReferenceMedia(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.ReferenceMediaTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_REFERENCEMEDIA, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received NODE TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketNodeTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.NodeTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_NODE, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received CONDITIONAL POINT TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketConditionalPointTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.ConditionalPointsCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_CONDITIONAL_POINT, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received DERIVED ITEMS TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketDerivedItemsTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.DerivedItemsTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_DERIVED_ITEMS, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received DERIVED POINTS TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketDerivedPointsTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.DerivedPointTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_DERIVED_POINT, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received POINTS TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketPointsTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.PointsTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_POINTS, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received TEXT DATA TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketTextDataTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.TextDataTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_TEXT_DATA, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received PROCESS TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketProcessTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.ProcessTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_PROCESS, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received INSPECTION TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketInspectionTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.InspectionTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_INSPECTION, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received INSTRUCTION TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketInstructionTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.InstructionTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_INSTRUCTIONS, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received MCC TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketMCCTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.MCCTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_MCC, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received MESSAGE TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketMessageTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.MessageTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_MESSAGE, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received FFTPOINT TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketFFTPointsTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.FFTPointsTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_FFT_POINTS, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// Process the received STRUCTURED ROUTE TABLE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketStructuredTable(ClientStateObject iStateObject, PacketCompressed iPacketCx)
        {
            // add compressed data to state object
            iStateObject.StructuredRouteTableCompressed = iPacketCx;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.TABLE_STRUCTURED, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */


        /// <summary>
        /// This method decompresses a Compressed packet object back into the
        /// state object, and then recursively call the ProcessReceivedBytes()
        /// method to actually process the decompressed backet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketCompressed(ClientStateObject iStateObject)
        {
            // deserialize the compressed packet
            PacketCompressed compressedPacket = PacketBase.DeserializeObject<PacketCompressed>(iStateObject.ReadBuffer);

            // now have another shot at processing the packet
            ProcessReceiveTable(iStateObject, compressedPacket);

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        /// <summary>
        /// Process a received NULL Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketNullReply(ClientStateObject iStateObject)
        {
            ReturnPacketDataUpdates(PacketType.NULL_REPLY, iStateObject.ReadBuffer);

            //
            // if connection was run as a test, then reset the test flag and
            // disconnect from the server
            //
            Disconnect();

            // deserialize profile packet
            PacketNullRx packet = PacketNullRx.DeserializeObject<PacketNullRx>(iStateObject.ReadBuffer);

        }/* end method */


        /// <summary>
        /// Process the received PROFILE Packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessPacketProfile(ClientStateObject iStateObject)
        {
            ReturnPacketDataUpdates(PacketType.PROFILE, iStateObject.ReadBuffer);

            // deserialize profile packet
            PacketProfile packet = PacketProfile.DeserializeObject<PacketProfile>(iStateObject.ReadBuffer);

            // validate (and if necessary reset) the device timezone
            SetDeviceTimeZone(packet);

            // update the deice friendly name
            SetDeviceFriendlyName(packet.Profile.DeviceName);

            // move all profile data into the current state object
            iStateObject.ProfileData = packet.Profile;

            //reference the operator set
            iStateObject.OperatorSetId = packet.Profile.OperatorSetId;

            //generate the authentication key and pass to the "authenticate me" packet
            SendAuthentication(packet.GetAuthenticationKey(DeviceUID));

        }/* end method */


        /// <summary>
        /// Process the PROFILE PENDING packet
        /// </summary>
        /// <param name="iStateObject">Current State Object</param>
        private void ProcessProfilePending(ClientStateObject iStateObject)
        {
            // deserialize the pending profile packet
            PacketProfilePending packet = PacketBase.DeserializeObject<PacketProfilePending>(iStateObject.ReadBuffer);

            // add the list of pending tables to the state object
            iStateObject.PendingTables = packet.Tables;

            // send progress back to main form
            ReturnPacketDataUpdates(PacketType.PROFILE_PENDING, iStateObject.ReadBuffer);

            // get the first pending profile table
            SendForNextProfileTable(iStateObject);

        }/* end method */

        //------------------------------------------------------------------------

        #region Private support methods

        /// <summary>
        /// If something went wrong then we always want to rollback the
        /// device profile back to what it originally was.
        /// Fixes defect #1363
        /// </summary>
        /// <param name="iStateObject"></param>
        private void RollBackProfile(ClientStateObject iStateObject)
        {
            // reference the rollback object
            iStateObject.ProfileData.DeviceName = mProfileRollback.DeviceName;
            iStateObject.ProfileData.ProfileName = mProfileRollback.ProfileName;
            iStateObject.ProfileData.ProfileId = mProfileRollback.ProfileID;

            //
            // if the Device Profile table contained something then
            // rollback the contents, otherwise (assuming the table
            // was originally empty), delete the contents
            //
            if ((mProfileRollback.ProfileID != string.Empty) &
               (mProfileRollback.ProfileName != string.Empty) &
               (mProfileRollback.DeviceName != string.Empty))
            {
                mSqlConnect.ProfileWasUpdated(iStateObject.ProfileData);
            }
            else
            {
                mSqlConnect.DeleteCurrentProfile();
            }

        }/* end method */


        /// <summary>
        /// Safely close process without updating any tables.
        /// This is primarily used when there has been a server
        /// error involving data corruption
        /// </summary>
        /// <param name="iStateObject"></param>
        private void TerminateWithoutUpdate(ClientStateObject iStateObject)
        {
            // send progress notifying continued activity
            ReturnClientStatusEvent(true);

            // make sure we roll back the device profile
            RollBackProfile(iStateObject);

            //
            // set the tables updated flag. Note this does not signify
            // that the tables were updated without error, but rather
            // that the process actually reached this point!
            //
            mTablesWereUpdated = true;

            // dereference all internal state object compressed records
            iStateObject.FlushCompressedPacketBuffers();

            // flush the internal state object buffers
            iStateObject.FlushBuffers();

            // send progress notifying end of process
            ReturnClientStatusEvent(false);

            //we're done here, lets dispose of the state object
            if ((!mStateObjectDisposing) && (iStateObject != null))
            {
                iStateObject.Dispose();
                iStateObject = null;
                mStateObjectDisposing = true;
            }

        }/* end method */


        /// <summary>
        /// Update all tables following a download from the server
        /// </summary>
        /// <param name="iStateObject"></param>
        private void UpdateDownloadTables(ClientStateObject iStateObject)
        {
            try
            {
                UpdateDownloadTables(iStateObject, false);
            }
            catch (Exception)
            {
                
            }
            finally
            {
                Disconnect();
            }
        }

        /// <summary>
        /// Update all tables following a download from the server
        /// </summary>
        /// <param name="iStateObject"></param>
        private void UpdateDownloadTables(ClientStateObject iStateObject, bool disconnect)
        {
            // disconnect from the server
            if(disconnect)
                Disconnect();

            // send progress notifying continued activity
            ReturnClientStatusEvent(true);

            // reset the database update fuse
            Boolean updateOK = true;

            //
            // if the profile needs updating then "blitz" the database and
            // rebuild everything from the ground up!
            //
            if (iStateObject.ProfileUpdateRequired)
            {
                // get the update fuse state
                updateOK = UpdateProfileTables(iStateObject);
            }

            // only go beyond this point if no error were raised ..
            if (updateOK)
            {
                //decompress and add the OPERATORS table
                if (!mUpdateDB.UpdateOperatorsTable(iStateObject))
                    ReturnClientDataError(TableName.OPERATORS, ErrorType.Update);

                // decompress and add the CODEDNOTES table
                if (!mUpdateDB.UpdateCodedNotesTable(iStateObject))
                    ReturnClientDataError(TableName.C_NOTES, ErrorType.Update);

                //
                // assuming we're getting our CMMS data from @ptitude then clear all
                // of the CMMS tables and reload from the state object
                //
                if (UseAnalystCMMS)
                {
                    if (!mUpdateDB.UpdateCMMSTables(iStateObject))
                    {
                        ReturnClientDataError(
                            TableName.ALL_CMMS,
                            ErrorType.Update);
                    }
                }

                // reset the Uploaded Flag in each of the SavedMedia records
                if (iStateObject.MediaReady)
                    mUpdateDB.UpdateSavedMedia();

                //
                // set the tables updated flag. Note this does not signify
                // that the tables were updated without error, but rather
                // that the process actually reached this point!
                //
                mTablesWereUpdated = true;

                // send progress notifying end of process
                ReturnClientStatusEvent(false);
            }

            //
            // if errors were raised then roll-back the profile and allow
            // for another synchronization ..
            //
            else
            {
                // make sure we roll back the device profile
                RollBackProfile(iStateObject);
            }

            // dereference all internal state object compressed records
            iStateObject.FlushCompressedPacketBuffers();

            // flush the internal state object buffers
            iStateObject.FlushBuffers();

            // force a second refresh of the device time
            SetDeviceTime();

            //we're done here, lets dispose of the state object
            if ((!mStateObjectDisposing) && (iStateObject != null))
            {
                iStateObject.Dispose();
                iStateObject = null;
                mStateObjectDisposing = true;
            }

        }/* end method */


        /// <summary>
        /// Update the fifteen profile tables only - note this is done in a
        /// fixed order and should not be changed
        /// </summary>
        /// <param name="iStateObject"></param>
        /// <returns>True if OK, else false on fail</returns>
        private Boolean UpdateProfileTables(ClientStateObject iStateObject)
        {
            // reset the progress fuse
            Boolean ok = true;

            // clear all existing 'download profile' data
            if (!mUpdateDB.ClearDownloadProfileTables())
            {
                ReturnClientDataError(TableName.ALL_PROFILE, ErrorType.Clear);
                ok = false;
            }

            // update the NODE table
            if (ok && !mUpdateDB.UpdateNodeTable(iStateObject))
            {
                ReturnClientDataError(TableName.NODE, ErrorType.Update);
                ok = false;
            }

            // update the POINTS table
            if (ok && !mUpdateDB.UpdatePointsTable(iStateObject))
            {
                ReturnClientDataError(TableName.POINTS, ErrorType.Update);
                ok = false;
            }

            // update the FFTPOINTS table
            if (ok && !mUpdateDB.UpdateFFTPointsTable(iStateObject))
            {
                ReturnClientDataError(TableName.FFTPOINT, ErrorType.Update);
                ok = false;
            }

            // update the REFERENCEMEDIA table
            if (ok && !mUpdateDB.UpdateReferenceMediaTable(iStateObject))
            {
                ReturnClientDataError(TableName.REFERENCEMEDIA, ErrorType.Update);
                ok = false;
            }

            // update the PROCESS table
            if (ok && !mUpdateDB.UpdateProcessTable(iStateObject))
            {
                ReturnClientDataError(TableName.PROCESS, ErrorType.Update);
                ok = false;
            }

            // update the TEXTDATA table
            if (ok && !mUpdateDB.UpdateTextDataTable(iStateObject))
            {
                ReturnClientDataError(TableName.TEXTDATA, ErrorType.Update);
                ok = false;
            }

            // update the INSPECTION table
            if (ok && !mUpdateDB.UpdateInspectionTable(iStateObject))
            {
                ReturnClientDataError(TableName.INSPECTION, ErrorType.Update);
                ok = false;
            }

            // update the MCC table
            if (ok && !mUpdateDB.UpdateMCCTable(iStateObject))
            {
                ReturnClientDataError(TableName.MCC, ErrorType.Update);
                ok = false;
            }

            // update the MESSAGE table
            if (ok && !mUpdateDB.UpdateMessageTable(iStateObject))
            {
                ReturnClientDataError(TableName.MESSAGE, ErrorType.Update);
                ok = false;
            }

            // update the ROUTE table
            if (ok && !mUpdateDB.UpdateStructuredRouteTable(iStateObject))
            {
                ReturnClientDataError(TableName.STRUCTURED, ErrorType.Update);
                ok = false;
            }

            // update the INSTRUCTIONS table
            if (ok && !mUpdateDB.UpdateInstructionsTable(iStateObject))
            {
                ReturnClientDataError(TableName.INSTRUCTIONS, ErrorType.Update);
                ok = false;
            }

            // update the DERIVEDITEMS table
            if (ok && !mUpdateDB.UpdateDerivedItemsTable(iStateObject))
            {
                ReturnClientDataError(TableName.DERIVEDITEMS, ErrorType.Update);
                ok = false;
            }

            // update the DERIVEDPOINTS table
            if (ok && !mUpdateDB.UpdateDerivedPointTable(iStateObject))
            {
                ReturnClientDataError(TableName.DERIVEDPOINT, ErrorType.Update);
                ok = false;
            }

            // update the CONDITIONALPOINT table
            if (ok && !mUpdateDB.UpdateConditionalPointTable(iStateObject))
            {
                ReturnClientDataError(TableName.CONDITIONALPOINT, ErrorType.Update);
                ok = false;
            }

            return ok;

        }/* end method */


        /// <summary>
        /// On table update error, raise an error event. This is a very
        /// simple method and should be extended (along with the class
        /// "ClientUpdateError", as and when required)
        /// </summary>
        private void ReturnClientDataError(TableName iTableName, ErrorType iError)
        {
            RaiseClientDataExceptionAsync(new DataConnectError(iTableName, iError));

        }/* end method */


        /// <summary>
        /// Set the total number of media steps 
        /// </summary>
        /// <param name="iStateObject"></param>
        private void SetMediaProgressSteps(ClientStateObject iStateObject)
        {
            // reset the byte count
            int bytes = 0;

            // how many bytes are there awaiting upload?
            iStateObject.SavedMedia.Reset();
            foreach (SavedMediaRecord smr in iStateObject.SavedMedia)
            {
                bytes = bytes + (int)smr.Size;
            }

            // how many discreet steps is that?
            int steps = bytes / iStateObject.MediaBlockSize;

            // configure the progress bar
            mMediaUploadProgress.NoOfItemsToProcess = steps;
            mMediaUploadProgress.DisplayAt = DisplayPosition.FullSweep;
            mMediaUploadProgress.FeedbackSteps = DataProgress.PROGRESS_UPDATE_STEPS;
            mMediaUploadProgress.Reset();

        }/* end method */


        /// <summary>
        /// Set the total number of progress steps. 
        /// </summary>
        /// <param name="iUpdateProfile"></param>
        private void SetTotalProgressSteps(Boolean iUpdateProfile, Boolean iMediaUpload)
        {
            // set the default count (operators table and basic comms)
            int progressSteps = PROGRESS_CONNECTION_STEPS;

            if (iUpdateProfile)
            {
                progressSteps = progressSteps + PROGRESS_UPDATE_PROFILE_STEPS;
            }

            if (UseAnalystCMMS)
            {
                progressSteps = progressSteps + PROGRESS_UPDATE_CMMS_STEPS;
            }

            if (iMediaUpload)
            {
                ++progressSteps;
            }

            mProgressPosition.NoOfItemsToProcess = progressSteps;
            mProgressPosition.FeedbackSteps = DataProgress.PROGRESS_UPDATE_STEPS;

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

    }/* end class */

}/* end namespace */

//------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 5.0 APinkerton 11th September 2012
//  Implement support for TextData record downloads
//
//  Revision 4.0 APinkerton 12th October 2011
//  Implement support for media file uploads
//
//  Revision 3.0 APinkerton 11th March 2010
//  Implement remote setting of Device TimeZone via the Profile Packet
// 
//  Revision 2.0 APinkerton 12th January 2010
//  Block upload packet building when operating as test
//
//  Revision 1.0 APinkerton 21st November 2009
//  Implements support for device to server-time synchronization
//
//  Revision 0.0 APinkerton 8th March 2009
//  Add to project
//------------------------------------------------------------------------------

