﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
    /// <summary>
    /// Interface for the Core Tcp based Microlog Inspector Device Client
    /// </summary>
    public interface ISynchSocketClient
    {
        /// <summary>
        /// Connect to the Middleware Device Server
        /// </summary>
        /// <param name="iUseIPAddress">Use IP Address or Hostname</param>
        void Connect();

        /// <summary>
        /// Disconnect from the Device Server. This is a somewhat "messy"
        /// way of doing things, with a number of read\write exceptions
        /// being raised. It is however, the method that Microsoft actually
        /// stipulate in their documentation!
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Simple Dispose method that closes any open sockets as well as 
        /// connections to the SQL ResultSet data layer 
        /// </summary>
        void Dispose();

        /// <summary>
        /// Send a single ACK query to the Device Server
        /// </summary>
        void SendACK();

        /// <summary>
        /// Property: Does the device have a manual license key installed?
        /// </summary>
        bool DeviceKeyLicensed { get; set; }

        /// <summary>
        /// Gets whether any tables were actually updated
        /// </summary>
        bool TablesWereUpdated { get; }

        /// <summary>
        /// Gets any synch error code (no error = -1)
        /// </summary>
        int SynchErrorCode{get;}

        /// <summary>
        /// Property: Get or set the Device UID as an encrypted 
        /// </summary>
        string DeviceUID { get; set; }

        /// <summary>
        /// Property: Get or set the device Firmware Version
        /// </summary>
        HwVersion FirmwareVersion { get; set; }

        /// <summary>
        /// Property: Get or set the connection host name
        /// </summary>
        string SocketHost { get; set; }

        /// <summary>
        /// Property: Get or set the Work Notifications soure type
        /// </summary>
       Boolean UseAnalystCMMS { get; set; }

        /// <summary>
        /// Property: Get or set the connection port number
        /// </summary>
        short TcpPortNumber { get; set; }

        /// <summary>
        /// Property: Use either the IP address or Hostname
        /// </summary>
        bool UseIpAddress { get; set; }

        /// <summary>
        /// Event raised on any change in connection status
        /// </summary>
        event SynchSocketClient.ConnectionState ConnectionStateEvent;

        /// <summary>
        /// Event raised whenever an XML data packet is received from the server
        /// </summary>
        event SynchSocketClient.ReturnPacketData ReturnPacketDataEvent;

        /// <summary>
        /// Event raised on occurance of any general exception
        /// </summary>
        event SynchSocketClient.ServerException ServerExceptionEvent;

        /// <summary>
        /// Event raised when client "busy" status changes
        /// </summary>
        event SynchSocketClient.ClientStatus ClientStatusEvent;

        /// <summary>
        /// Event raised on changes to the progressbar value
        /// </summary>
        event SynchSocketClient.ProgressValue ProgressValueEvent;

        /// <summary>
        /// Event raised on client data exceptions (SQL)
        /// </summary>
        event SynchSocketClient.ClientDataException ClientDataExceptionEvent;

    }/* end interface */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th March 2009 (14:20)
//  Add to project
//----------------------------------------------------------------------------
