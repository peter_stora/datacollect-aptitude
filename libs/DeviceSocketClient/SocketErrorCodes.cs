﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SKF.RS.MicrologInspector.DeviceSocketClient
{
   /// <summary>
   /// Static class that defines potential TCP connection errors.
   /// </summary>
   public static class SocketErrorCodes
   {
      public static int InterruptedFunctionCall
      {
         get
         {
            return 10004;
         }
      }
      public static int PermissionDenied
      {
         get
         {
            return 10013;
         }
      }
      public static int BadAddress
      {
         get
         {
            return 10014;
         }
      }
      public static int InvalidArgument
      {
         get
         {
            return 10022;
         }
      }
      public static int TooManyOpenFiles
      {
         get
         {
            return 10024;
         }
      }
      public static int ResourceTemporarilyUnavailable
      {
         get
         {
            return 10035;
         }
      }
      public static int OperationNowInProgress
      {
         get
         {
            return 10036;
         }
      }
      public static int OperationAlreadyInProgress
      {
         get
         {
            return 10037;
         }
      }
      public static int SocketOperationOnNonSocket
      {
         get
         {
            return 10038;
         }
      }
      public static int DestinationAddressRequired
      {
         get
         {
            return 10039;
         }
      }
      public static int MessgeTooLong
      {
         get
         {
            return 10040;
         }
      }
      public static int WrongProtocolType
      {
         get
         {
            return 10041;
         }
      }
      public static int BadProtocolOption
      {
         get
         {
            return 10042;
         }
      }
      public static int ProtocolNotSupported
      {
         get
         {
            return 10043;
         }
      }
      public static int SocketTypeNotSupported
      {
         get
         {
            return 10044;
         }
      }
      public static int OperationNotSupported
      {
         get
         {
            return 10045;
         }
      }
      public static int ProtocolFamilyNotSupported
      {
         get
         {
            return 10046;
         }
      }
      public static int AddressFamilyNotSupported
      {
         get
         {
            return 10047;
         }
      }
      public static int AddressInUse
      {
         get
         {
            return 10048;
         }
      }
      public static int AddressNotAvailable
      {
         get
         {
            return 10049;
         }
      }
      public static int NetworkIsDown
      {
         get
         {
            return 10050;
         }
      }
      public static int NetworkIsUnreachable
      {
         get
         {
            return 10051;
         }
      }
      public static int NetworkReset
      {
         get
         {
            return 10052;
         }
      }
      public static int ConnectionAborted
      {
         get
         {
            return 10053;
         }
      }
      public static int ConnectionResetByPeer
      {
         get
         {
            return 10054;
         }
      }
      public static int NoBufferSpaceAvailable
      {
         get
         {
            return 10055;
         }
      }
      public static int AlreadyConnected
      {
         get
         {
            return 10056;
         }
      }
      public static int NotConnected
      {
         get
         {
            return 10057;
         }
      }
      public static int CannotSendAfterShutdown
      {
         get
         {
            return 10058;
         }
      }
      public static int ConnectionTimedOut
      {
         get
         {
            return 10060;
         }
      }
      public static int ConnectionRefused
      {
         get
         {
            return 10061;
         }
      }
      public static int HostIsDown
      {
         get
         {
            return 10064;
         }
      }
      public static int HostUnreachable
      {
         get
         {
            return 10065;
         }
      }
      public static int TooManyProcesses
      {
         get
         {
            return 10067;
         }
      }
      public static int NetworkSubsystemIsUnavailable
      {
         get
         {
            return 10091;
         }
      }
      public static int UnsupportedVersion
      {
         get
         {
            return 10092;
         }
      }
      public static int NotInitialized
      {
         get
         {
            return 10093;
         }
      }
      public static int ShutdownInProgress
      {
         get
         {
            return 10101;
         }
      }
      public static int ClassTypeNotFound
      {
         get
         {
            return 10109;
         }
      }
      public static int HostNotFound
      {
         get
         {
            return 11001;
         }
      }
      public static int HostNotFoundTryAgain
      {
         get
         {
            return 11002;
         }
      }
      public static int NonRecoverableError
      {
         get
         {
            return 11003;
         }
      }
      public static int NoDataOfRequestedType
      {
         get
         {
            return 11004;
         }
      }

   }/*end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------