﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Enumerted list of Upload Container objects
   /// </summary>
   public class EnumUploadContainerRecords
   {
      protected List<UploadContainerRecord> mUploadContainerRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumUploadContainerRecords()
      {
         mUploadContainerRecord = new List<UploadContainerRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw PointRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "Container" )]
      public List<UploadContainerRecord> Container
      {
         get
         {
            return mUploadContainerRecord;
         }
         set
         {
            mUploadContainerRecord = value;
         }
      }


      /// <summary>
      /// Add a new Point record to the list
      /// </summary>
      /// <param name="item">populated Node record object</param>
      /// <returns>number of Node objects available</returns>
      public int AddContainerRecord( UploadContainerRecord item )
      {
         mUploadContainerRecord.Add( item );
         return mUploadContainerRecord.Count;
      }/* end method */


      /// <summary>
      /// How many Point records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mUploadContainerRecord.Count;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 24th July 2010
//  Add to project
//----------------------------------------------------------------------------
