﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// UPLOAD_RECEIVED Packet Type - returned to Client on receipt of Upload
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketUploadReceived : PacketBase
    {
        /// <summary>
        /// Packet Identifier
        /// </summary>
        public byte Packet = (byte)PacketType.UPLOAD_RECEIVED;

        /// <summary>
        /// Session Identifier
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// Base Constructor
        /// </summary>
        public PacketUploadReceived()
        {
        }/* base constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------
