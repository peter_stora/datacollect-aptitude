﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// The FFT Channel object is used to store the actual FFT measurement 
   /// for a single channel along with the measurement parameters (i.e. 
   /// running speed, filter type, lines, etc) that need to be uploaded 
   /// along with the FFT data.  A record is stored for velocity and for 
   /// enveloped acceleration. This object is populated after an FFT 
   /// measurement is taken and saved.  
   /// </summary>
   public class FFTChannelBase
   {
      private FFTDataLines mFFTDataLines = new FFTDataLines();

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public FFTChannelBase()
      {
      }/* end constructor */

      /// <summary>
      /// Get or set the channel type 
      /// (0: None, 1: Envelope Acceleration, 2: Velocity, 3: Accel)
      /// </summary>
      [XmlElement( "ChannelId" )]
      public byte ChannelId
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the spectrum number 
      /// (-1: None, 0: Zero, 1: One)
      /// </summary>
      [XmlElement( "SpecNum" )]
      public byte SpecNum
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the timestamp of the measurement
      /// </summary>
      [XmlElement( "TimeStamp" )]
      public DateTime Timestamp
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the number of sample lines to collect
      /// </summary>
      [XmlElement( "NumberOfLines" )]
      public int NumberOfLines
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the datatype
      /// (0: Magnitude, 2: Phase, 3: Time, 20: Motor Current)
      /// </summary>
      [XmlElement( "DataType" )]
      public byte DataType
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the number of averages per FFT
      /// </summary>
      [XmlElement( "NumberOfAverages" )]
      public int NumberOfAverages
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the windowing function  
      /// (1: Hanning, 2: Flat top, 3: Uniform)
      /// </summary>
      [XmlElement( "Windowing" )]
      public byte Windowing
      {
         get;
         set;
      }

      /// <summary>
      /// Running speed (or pole pass frequency) of the machine
      /// </summary>
      [XmlElement( "SpeedOrPPF" )]
      public double SpeedOrPPF
      {
         get;
         set;
      }

      /// <summary>
      /// Decided by the WMCD, value = line[i]*factor + offset
      /// </summary>
      [XmlElement( "Factor" )]
      public double Factor
      {
         get;
         set;
      }

      /// <summary>
      /// Decided by the WMCD, value = line[i]*factor + offset
      /// </summary>
      [XmlElement( "Offset" )]
      public double Offset
      {
         get;
         set;
      }

      /// <summary>
      /// Upper limit of the passband collection frequency
      /// </summary>
      [XmlElement( "HiFreq" )]
      public double HiFreq
      {
         get;
         set;
      }

      /// <summary>
      /// Lower limit of the passband collection frequency
      /// </summary>
      [XmlElement( "LowFreq" )]
      public double LowFreq
      {
         get;
         set;
      }

      /// <summary>
      /// Default to zero
      /// </summary>
      [XmlElement( "Load" )]
      public int Load
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the compression type 
      /// (0: Not Compressed, 1: B255 compression: 2: 1800 compression)
      /// </summary>
      [XmlElement( "CompressionType" )]
      public byte CompressionType
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the user index
      /// </summary>
      [XmlElement( "UserIndex" )]
      public int UserIndex
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the units type
      /// </summary>
      [XmlElement( "Units" )]
      public int Units
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set: Was the measurement in overload?
      /// </summary>
      [XmlElement( "IsOverloaded" )]
      public Boolean IsOverloaded
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set collected FFT data as a formatted byte array
      /// </summary>
      [XmlElement( "DataLines" )]
      public byte[] DataLines
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Serial Number of the WMCD
      /// </summary>
      [XmlElement("SerialNo")]
      public string SerialNo
      {
         get;
         set;
      }


      /// <summary>
      /// Populate the base object from a full channel record
      /// </summary>
      /// <param name="iChannel">Populated FFTChannel Record</param>
      public void PopulateBaseFields( FFTChannelRecord iChannel )
      {
         this.ChannelId = iChannel.ChannelId;
         this.CompressionType = iChannel.CompressionType;
         this.AddDataLines( iChannel.DataLines );
         this.DataType = iChannel.DataType;
         this.Factor = iChannel.Factor;
         this.HiFreq = iChannel.HiFreq;
         this.IsOverloaded = iChannel.IsOverloaded;
         this.Load = iChannel.Load;
         this.LowFreq = iChannel.LowFreq;
         this.NumberOfAverages = iChannel.NumberOfAverages;
         this.NumberOfLines = iChannel.NumberOfLines;
         this.Offset = iChannel.Offset;
         this.SpecNum = iChannel.SpecNum;
         this.SpeedOrPPF = iChannel.SpeedOrPPF;
         this.Timestamp = iChannel.Timestamp;
         this.Units = iChannel.Units;
         this.UserIndex = iChannel.UserIndex;
         this.Windowing = iChannel.Windowing;
         this.SerialNo = iChannel.SerialNo;

      }/* end method */


      /// <summary>
      /// Converts an array of integers into an array of bytes, with each
      /// integer values saved as a two byte value. Note: If any integer 
      /// value exceeds 2^16, then the excess values will be ignored!
      /// </summary>
      /// <param name="iFFTDataArray">array if FFT integer values</param>
      public void AddDataLines( int[] iFFTDataArray )
      {
         DataLines = mFFTDataLines.ConvertToByteArray( iFFTDataArray );

      }/* end method */


      /// <summary>
      /// Copy the contents of an external FFT channel byte array to the
      /// local class "DataLines" destination array.
      /// </summary>
      /// <param name="iFFTDataArray">source binary array</param>
      public void AddDataLines( byte[] iFFTDataArray )
      {
         DataLines = mFFTDataLines.CopyArray( iFFTDataArray );

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------

