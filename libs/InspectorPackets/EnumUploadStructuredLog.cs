﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of UploadStructuredLog records 
    /// </summary>
    public class EnumUploadStructuredLogRecords
    {
        protected List<UploadStructuredLogRecord> mUploadStructuredLog;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public EnumUploadStructuredLogRecords()
        {
            mUploadStructuredLog = new List<UploadStructuredLogRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw NodeRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("Route")]
        public List<UploadStructuredLogRecord> Route
        {
            get { return mUploadStructuredLog; }
            set { mUploadStructuredLog = value; }
        }/* end property */


        /// <summary>
        /// Add a new StructuredLog upload record to the list
        /// </summary>
        /// <param name="item">populated UploadStucturedLog object</param>
        /// <returns>number of Structured Log objects now available</returns>
        public int AddStructuredLog(UploadStructuredLogRecord item)
        {
            mUploadStructuredLog.Add(item);
            return mUploadStructuredLog.Count;
        }/* end method */


    }/* end class */


    /// <summary>
    /// Class EnumUploadStructuredLogRecords with support for IEnumerable. 
    /// This extended functionality enables not only foreach support - but 
    /// also (and much more importantly) LINQ support!
    /// </summary>
    public class EnumUploadStructuredLogs : EnumUploadStructuredLogRecords, 
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mUploadStructuredLog.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mUploadStructuredLog[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009
//  Add to project
//----------------------------------------------------------------------------


