﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Connection Error: Client authentication either failed or was refused
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketConnectionRefused : PacketBase
    {
        /// <summary>
        /// Packet Type
        /// </summary>
        public byte Packet = (byte)PacketType.CONNECT_REFUSED;

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th February 2009
//  Add to project
//----------------------------------------------------------------------------