﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Serializable Archive Data
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketArchive : PacketBase
   {
      //----------------------------------------------------------------------

      #region Private members

      private ArchiveRecord mCodedNote = null;
      private ArchiveRecord mCodedNotes = null;
      private ArchiveRecord mConditionalPoint = null;
      private ArchiveRecord mCorrectiveAction = null;
      private ArchiveRecord mDerivedItems = null;
      private ArchiveRecord mDerivedPoint = null;
      private ArchiveRecord mDeviceProfile = null;
      private ArchiveRecord mFFTChannel = null;
      private ArchiveRecord mFFTPoint = null;
      private ArchiveRecord mEnvData = null;
      private ArchiveRecord mInspectionTable = null;
      private ArchiveRecord mInstructions = null;
      private ArchiveRecord mMachineNopSkips = null;
      private ArchiveRecord mMachineOkSkips = null;
      private ArchiveRecord mMccTable = null;
      private ArchiveRecord mMessageTable = null;
      private ArchiveRecord mMessagingPrefs = null;
      private ArchiveRecord mNodeTable = null;
      private ArchiveRecord mNotes = null;
      private ArchiveRecord mOperators = null;
      private ArchiveRecord mPointsTable = null;
      private ArchiveRecord mPriority = null;
      private ArchiveRecord mProblem = null;
      private ArchiveRecord mProcessTable = null;
      private ArchiveRecord mSavedMedia = null;
      private ArchiveRecord mTempData = null;
      private ArchiveRecord mTextData = null;
      private ArchiveRecord mVelData = null;
      private ArchiveRecord mUserPreferences = null;
      private ArchiveRecord mWorkNotification = null;
      private ArchiveRecord mWorkType = null;

      #endregion

      //----------------------------------------------------------------------

      /// <summary>
      /// The actual Packet type (Database Archive)
      /// </summary>
      public byte Packet = (byte) PacketType.ARCHIVE;

      /// <summary>
      /// Constructor
      /// </summary>
      public PacketArchive( ):
         this(string.Empty)
      {
      }/* end constructor */

      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iVersion">Current Database Version</param>
      public PacketArchive(string iVersion)
      {
         ArchiveVersion = iVersion;
         ArchiveDate = DateTime.Now;

      }/* end constructor */

      //----------------------------------------------------------------------

      #region Public Properties

      /// <summary>
      /// Gets or sets the Date and Time when this Archive was created
      /// </summary>
      [XmlElement( "ArchiveDate" )]
      public DateTime ArchiveDate
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current archive file version
      /// </summary>
      [XmlElement( "ArchiveVersion" )]
      public string ArchiveVersion
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the contents of the C_NOTE table
      /// </summary>
      [XmlElement( "C_NOTE" )]
      public ArchiveRecord C_NOTE
      {
         get
         {
            return mCodedNote;
         }
         set
         {
            mCodedNote = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the C_NOTES table
      /// </summary>
      [XmlElement( "C_NOTES" )]
      public ArchiveRecord C_NOTES
      {
         get
         {
            return mCodedNotes;
         }
         set
         {
            mCodedNotes = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the CONDITIONALPOINT table
      /// </summary>
      [XmlElement( "CONDITIONALPOINT" )]
      public ArchiveRecord CONDITIONALPOINT
      {
         get
         {
            return mConditionalPoint;
         }
         set
         {
            mConditionalPoint = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the CORRECTIVEACTION table
      /// </summary>
      [XmlElement( "CORRECTIVEACTION" )]
      public ArchiveRecord CORRECTIVEACTION
      {
         get
         {
            return mCorrectiveAction;
         }
         set
         {
            mCorrectiveAction = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the DERIVEDITEMS table
      /// </summary>
      [XmlElement( "DERIVEDITEMS" )]
      public ArchiveRecord DERIVEDITEMS
      {
         get
         {
            return mDerivedItems;
         }
         set
         {
            mDerivedItems = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the DERIVEDPOINT table
      /// </summary>
      [XmlElement( "DERIVEDPOINT" )]
      public ArchiveRecord DERIVEDPOINT
      {
         get
         {
            return mDerivedPoint;
         }
         set
         {
            mDerivedPoint = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the DEVICEPROFILE table
      /// </summary>
      [XmlElement( "DEVICEPROFILE" )]
      public ArchiveRecord DEVICEPROFILE
      {
         get
         {
            return mDeviceProfile;
         }
         set
         {
            mDeviceProfile = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the ENVDATA table
      /// </summary>
      [XmlElement( "ENVDATA" )]
      public ArchiveRecord ENVDATA
      {
         get
         {
            return mEnvData;
         }
         set
         {
            mEnvData = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the FFTCHANNEL table
      /// </summary>
      [XmlElement( "FFTCHANNEL" )]
      public ArchiveRecord FFTCHANNEL
      {
         get
         {
            return mFFTChannel;
         }
         set
         {
            mFFTChannel = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the FFTPOINT table
      /// </summary>
      [XmlElement( "FFTPOINT" )]
      public ArchiveRecord FFTPOINT
      {
         get
         {
            return mFFTPoint;
         }
         set
         {
            mFFTPoint = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the INSPECTION table
      /// </summary>
      [XmlElement( "INSPECTION" )]
      public ArchiveRecord INSPECTION
      {
         get
         {
            return mInspectionTable;
         }
         set
         {
            mInspectionTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the INSTRUCTIONS table
      /// </summary>
      [XmlElement( "INSTRUCTIONS" )]
      public ArchiveRecord INSTRUCTIONS
      {
         get
         {
            return mInstructions;
         }
         set
         {
            mInstructions = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the MESSAGE table
      /// </summary>
      [XmlElement( "MESSAGE" )]
      public ArchiveRecord MESSAGE
      {
         get
         {
            return mMessageTable;
         }
         set
         {
            mMessageTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the MCC table
      /// </summary>
      [XmlElement( "MCC" )]
      public ArchiveRecord MCC
      {
         get
         {
            return mMccTable;
         }
         set
         {
            mMccTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the MACHINEOKSKIPS table
      /// </summary>
      [XmlElement( "MACHINEOKSKIPS" )]
      public ArchiveRecord MACHINEOKSKIPS
      {
         get
         {
            return mMachineOkSkips;
         }
         set
         {
            mMachineOkSkips = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the MACHINENOPSKIPS table
      /// </summary>
      [XmlElement( "MACHINENOPSKIPS" )]
      public ArchiveRecord MACHINENOPSKIPS
      {
         get
         {
            return mMachineNopSkips;
         }
         set
         {
            mMachineNopSkips = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the MESSAGINGPREFS table
      /// </summary>
      [XmlElement( "MESSAGINGPREFS" )]
      public ArchiveRecord MESSAGINGPREFS
      {
         get
         {
            return mMessagingPrefs;
         }
         set
         {
            mMessagingPrefs = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the NODE table
      /// </summary>
      [XmlElement( "NODE" )]
      public ArchiveRecord NODE
      {
         get
         {
            return mNodeTable;
         }
         set
         {
            mNodeTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the NOTES table
      /// </summary>
      [XmlElement( "NOTES" )]
      public ArchiveRecord NOTES
      {
         get
         {
            return mNotes;
         }
         set
         {
            mNotes = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the OPERATORS table
      /// </summary>
      [XmlElement( "OPERATORS" )]
      public ArchiveRecord OPERATORS
      {
         get
         {
            return mOperators;
         }
         set
         {
            mOperators = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the PRIORITY table
      /// </summary>
      [XmlElement( "PRIORITY" )]
      public ArchiveRecord PRIORITY
      {
         get
         {
            return mPriority;
         }
         set
         {
            mPriority = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the POINTS table
      /// </summary>
      [XmlElement( "POINTS" )]
      public ArchiveRecord POINTS
      {
         get
         {
            return mPointsTable;
         }
         set
         {
            mPointsTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the PROBLEM table
      /// </summary>
      [XmlElement( "PROBLEM" )]
      public ArchiveRecord PROBLEM
      {
         get
         {
            return mProblem;
         }
         set
         {
            mProblem = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the PROCESS table
      /// </summary>
      [XmlElement( "PROCESS" )]
      public ArchiveRecord PROCESS
      {
         get
         {
            return mProcessTable;
         }
         set
         {
            mProcessTable = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the SAVEDMEDIA table
      /// </summary>
      [XmlElement( "SAVEDMEDIA" )]
      public ArchiveRecord SAVEDMEDIA
      {
         get
         {
            return mSavedMedia;
         }
         set
         {
            mSavedMedia = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the TEMPDATA table
      /// </summary>
      [XmlElement( "TEMPDATA" )]
      public ArchiveRecord TEMPDATA
      {
         get
         {
            return mTempData;
         }
         set
         {
            mTempData = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the TEXTDATA table
      /// </summary>
      [XmlElement( "TEXTDATA" )]
      public ArchiveRecord TEXTDATA
      {
         get
         {
            return mTextData;
         }
         set
         {
            mTextData = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the USERPREFERENCES table
      /// </summary>
      [XmlElement( "USERPREFERENCES" )]
      public ArchiveRecord USERPREFERENCES
      {
         get
         {
            return mUserPreferences;
         }
         set
         {
            mUserPreferences = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the VELDATA table
      /// </summary>
      [XmlElement( "VELDATA" )]
      public ArchiveRecord VELDATA
      {
         get
         {
            return mVelData;
         }
         set
         {
            mVelData = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the WORKNOTIFICATION table
      /// </summary>
      [XmlElement( "WORKNOTIFICATION" )]
      public ArchiveRecord WORKNOTIFICATION
      {
         get
         {
            return mWorkNotification;
         }
         set
         {
            mWorkNotification = value;
         }
      }

      /// <summary>
      /// Gets or sets the contents of the WORKTYPE table
      /// </summary>
      [XmlElement( "WORKTYPE" )]
      public ArchiveRecord WORKTYPE
      {
         get
         {
            return mWorkType;
         }
         set
         {
            mWorkType = value;
         }
      }

      #endregion

      //----------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// Archive the NODE table
      /// </summary>
      /// <param name="iNodes">Enumerated list of Node objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumNodes iNodes )
      {
         // set the default result
         Boolean result = true;

         if ( iNodes != null )
         {
            // create a new NODE packet object
            ArchiveNodeTable archiveTable= new ArchiveNodeTable();

            try
            {
               // populate the NODE packet object
               foreach ( NodeRecord node in iNodes )
               {
                  if ( node != null )
                  {
                     archiveTable.NODE.AddNodeRecord( node );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.NODE.Count > 0 )
               {
                  // create a NODE Table object
                  if ( mNodeTable == null )
                  {
                     mNodeTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mNodeTable.ByteData = mNodeTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the POINTS table 
      /// </summary>
      /// <param name="iPoints">Enumerated list of Point objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumPoints iPoints )
      {
         // set the default result
         Boolean result = true;

         if ( iPoints != null )
         {
            // create a new POINT packet object
            ArchivePointTable archiveTable= new ArchivePointTable();

            try
            {
               // populate the POINT packet object
               foreach ( PointRecord point in iPoints )
               {
                  if ( point != null )
                  {
                     archiveTable.POINTS.AddPointsRecord( point );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.POINTS.Count > 0 )
               {
                  // Create a POINTS table object
                  if ( mPointsTable == null )
                  {
                     mPointsTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mPointsTable.ByteData = mPointsTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the PROCESS table
      /// </summary>
      /// <param name="iProcess">Enumerated list of Process objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumProcess iProcess )
      {
         // set the default result
         Boolean result = true;

         if ( iProcess != null )
         {
            // create a new PROCESS packet object
            ArchiveProcessTable archiveTable= new ArchiveProcessTable();

            try
            {
               // populate the PROCESS packet object
               foreach ( ProcessRecord process in iProcess )
               {
                  if ( process != null )
                  {
                     archiveTable.PROCESS.AddProcessRecord( process );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.PROCESS.Count > 0 )
               {
                  // Create a PROCESS table object
                  if ( mProcessTable == null )
                  {
                     mProcessTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mProcessTable.ByteData = mProcessTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the INSPECTIONS table
      /// </summary>
      /// <param name="iInspections">Enumerated list of Inspection objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumInspection iInspections )
      {
         // set the default result
         Boolean result = true;

         if ( iInspections != null )
         {
            // create a new INSPECTION packet object
            ArchiveInspectionTable archiveTable= new ArchiveInspectionTable();

            try
            {
               // populate the INSPECTION packet object
               foreach ( InspectionRecord inspection in iInspections )
               {
                  if ( inspection != null )
                  {
                     archiveTable.INSPECTION.AddInspectionRecord( inspection );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.INSPECTION.Count > 0 )
               {
                  // Create a INSPECTION table object
                  if ( mInspectionTable == null )
                  {
                     mInspectionTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mInspectionTable.ByteData = mInspectionTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the TEXTDATA table
      /// </summary>
      /// <param name="iTextData">Enumerated list of TextData objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumTextData iTextData )
      {
         // set the default result
         Boolean result = true;

         if ( iTextData != null )
         {
            // create a new TEXTDATA packet object
            ArchiveTextDataTable archiveTable= new ArchiveTextDataTable();

            try
            {
               // populate the TEXTDATA packet object
               foreach ( TextDataRecord textRecord in iTextData )
               {
                  if ( textRecord != null )
                  {
                     archiveTable.TEXTDATA.AddTextDataRecord( textRecord );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.TEXTDATA.Count > 0 )
               {
                  // Create a TEXTDATA table object
                  if ( mTextData == null )
                  {
                     mTextData = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mTextData.ByteData = mTextData.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the MCC table
      /// </summary>
      /// <param name="iMccs">Enumerated list of MCC objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumMCC iMccs )
      {
         // set the default result
         Boolean result = true;

         if ( iMccs != null )
         {
            // create a new MCC packet object
            ArchiveMCCTable archiveTable = new ArchiveMCCTable();

            try
            {
               // populate the MCC packet object
               foreach ( MCCRecord Mcc in iMccs )
               {
                  if ( Mcc != null )
                  {
                     archiveTable.MCC.AddMCCRecord( Mcc );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.MCC.Count > 0 )
               {
                  // Create a MCC table object
                  if ( mMccTable == null )
                  {
                     mMccTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mMccTable.ByteData = mMccTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the FFTCHANNEL table
      /// </summary>
      /// <param name="iFFTChannels">Enumerated list of FFT channel objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumFFTChannelRecords iFFTChannels )
      {
         // set the default result
         Boolean result = true;

         if ( iFFTChannels != null )
         {
            // create a new FFTCHANNEL packet object
            ArchiveFFTChannelTable archiveTable= new ArchiveFFTChannelTable();

            try
            {
               // add each channel record
               foreach ( FFTChannelRecord channel in iFFTChannels )
               {
                  archiveTable.FFTCHANNEL.AddFFTChannelRecord( channel );
               }

               // only continue if we actually returned something
               if ( archiveTable.FFTCHANNEL.Count > 0 )
               {
                  // Create a FFTCHANNEL table object
                  if ( mFFTChannel == null )
                  {
                     mFFTChannel = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mFFTChannel.ByteData = mFFTChannel.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the FFTPOINTS table
      /// </summary>
      /// <param name="iFFTPoints">Enumerated list of FFT point objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumFFTPoints iFFTPoints )
      {
         // set the default result
         Boolean result = true;

         if ( iFFTPoints != null )
         {
            // create a new FFTPoint packet object
            ArchiveFFTPointTable archiveTable= new ArchiveFFTPointTable();

            try
            {
               // populate the FFTPoint packet object
               foreach ( FFTPointRecord fftPoint in iFFTPoints )
               {
                  if ( fftPoint != null )
                  {
                     archiveTable.FFTPOINT.AddFFTPointRecord( fftPoint );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.FFTPOINT.Count > 0 )
               {
                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // Create a FFTPoint table object
                  if ( mFFTPoint == null )
                  {
                     mFFTPoint = new ArchiveRecord();
                  }

                  // create a compressed data packet and populate
                  mFFTPoint.ByteData = mFFTPoint.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the C_NOTE table
      /// </summary>
      /// <param name="iCodedNotes">Enumerated list of @ptitude Coded Notes</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCodedNotes iCodedNotes )
      {
         // set the default result
         Boolean result = true;

         if ( iCodedNotes != null )
         {

            // create a new C_NOTE packet object
            ArchiveCNoteTable archiveTable= new ArchiveCNoteTable();

            try
            {
               // populate the C_NOTE packet object
               foreach ( CodedNoteRecord codedNote in iCodedNotes )
               {
                  if ( codedNote != null )
                  {
                     archiveTable.C_NOTE.AddCodedNoteRecord( codedNote );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.C_NOTE.Count > 0 )
               {
                  // Create a C_NOTE table object
                  if ( mCodedNote == null )
                  {
                     mCodedNote = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mCodedNote.ByteData = mCodedNote.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }

         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the C_NOTES table
      /// </summary>
      /// <param name="iCodedNotes">Enumarated list of saved Coded Notes</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumSelectedCodedNotes iCodedNotes )
      {
         // set the default result
         Boolean result = true;

         if ( iCodedNotes != null )
         {
            // create a new C_NOTES packet object
            ArchiveCNotesTable archiveTable= new ArchiveCNotesTable();

            try
            {
               // add all coded notes to the list
               for ( int i=0; i < iCodedNotes.Count; ++i )
               {
                  if ( iCodedNotes.CodedNote[ 0 ] != null )
                  {
                     archiveTable.C_NOTES.AddCodedNoteRecord( iCodedNotes.CodedNote[ i ] );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.C_NOTES.Count > 0 )
               {
                  // Create a C_NOTES table object
                  if ( mCodedNotes == null )
                  {
                     mCodedNotes = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mCodedNotes.ByteData = mCodedNotes.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the OPERATORS table
      /// </summary>
      /// <param name="iOperators">Enumerated list of Operator objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumOperators iOperators )
      {
         // set the default result
         Boolean result = true;

         if ( iOperators != null )
         {
            // create a new OPERATORS packet object
            ArchiveOperatorsTable archiveTable= new ArchiveOperatorsTable();

            try
            {
               foreach ( OperatorRecord oper in iOperators )
               {
                  archiveTable.OPERATORS.AddRecord( oper );
               }

               // only continue if we actually returned something
               if ( archiveTable.OPERATORS.Count > 0 )
               {
                  // Create a OPERATORS table object
                  if ( mOperators == null )
                  {
                     mOperators = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mOperators.ByteData = mOperators.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the MESSAGINGPREFS table
      /// </summary>
      /// <param name="iMessagingPrefs">Enumerated list of Message Preferences</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumMessagingPrefs iMessagingPrefs )
      {
         // set the default result
         Boolean result = true;

         if ( iMessagingPrefs != null )
         {
            // create a new MESSAGINGPREFS packet object
            ArchiveMessagingPrefsTable archiveTable= new ArchiveMessagingPrefsTable();

            try
            {
               foreach ( MessagingPrefsRecord prefs in iMessagingPrefs )
               {
                  archiveTable.MESSAGINGPREFS.AddRecord( prefs );
               }

               // only continue if we actually returned something
               if ( archiveTable.MESSAGINGPREFS.Count > 0 )
               {
                  // Create a MESSAGINGPREFS table object
                  if ( mMessagingPrefs == null )
                  {
                     mMessagingPrefs = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mMessagingPrefs.ByteData = mMessagingPrefs.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive TEMP, VEL or ENV measurement Tables
      /// </summary>
      /// <param name="iMeasurements">Enumerated list of Measurement objects</param>
      /// <param name="iTable">Measurement table type</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumSavedMeasurements iMeasurements, ArchiveTable iTable )
      {
         if ( iTable == ArchiveTable.TEMPDATA )
         {
            return Add_TEMPDATA_To_Archive( iMeasurements );
         }
         else if ( iTable == ArchiveTable.VELDATA )
         {
            return Add_VELDATA_To_Archive( iMeasurements );
         }
         else if ( iTable == ArchiveTable.ENVDATA )
         {
            return Add_ENVDATA_To_Archive( iMeasurements );
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Archive either "Machine OK" or "Machine Not Operating" Skip tables
      /// </summary>
      /// <param name="iMachineSkips">Enumerated list of Operator Preferences</param>
      /// <param name="iTable">Skip table type</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumMachineSkips iMachineSkips, ArchiveTable iTable )
      {
         if ( iTable == ArchiveTable.MACHINENOPSKIPS )
         {
            return Add_MACHINENOPSKIPS_To_Archive( iMachineSkips );
         }
         else if ( iTable == ArchiveTable.MACHINEOKSKIPS )
         {
            return Add_MACHINEOKSKIPS_To_Archive( iMachineSkips );
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Archive the USERPREFERENCES table
      /// </summary>
      /// <param name="iUserPreferences">Enumerated list of User Preferences</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumUserPreferences iUserPreferences )
      {
         // set the default result
         Boolean result = true;

         // create a new USERPREFERENCES packet object
         ArchiveUserPreferencesTable archiveTable= new ArchiveUserPreferencesTable();

         try
         {
            foreach ( UserPreferencesRecord prefs in iUserPreferences )
            {
               archiveTable.USERPREFERENCES.AddRecord( prefs );
            }

            // only continue if we actually returned something
            if ( archiveTable.USERPREFERENCES.Count > 0 )
            {
               // Create a USERPREFERENCES table object
               if ( mUserPreferences == null )
               {
                  mUserPreferences = new ArchiveRecord();
               }

               // convert to an array of bytes
               byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

               // create a compressed data packet and populate
               mUserPreferences.ByteData = mUserPreferences.Compress( myarray );
            }
         }
         catch
         {
            result = false;
         }

         // release what's left
         archiveTable = null;

         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the CORRECTIVEACTIONS table
      /// </summary>
      /// <param name="iCorrectiveActions">Enumerated list fo Corrective Actions</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCmmsCorrectiveActions iCorrectiveActions )
      {
         // set the default result
         Boolean result = true;

         if ( iCorrectiveActions != null )
         {
            // create a new CORRECTIVEACTION packet object
            ArchiveCorrectiveActionTable archiveTable= new ArchiveCorrectiveActionTable();

            try
            {
               foreach ( CmmsCorrectiveActionRecord prefs in iCorrectiveActions )
               {
                  archiveTable.CORRECTIVEACTION.AddCorrectiveActionRecord( prefs );
               }

               // only continue if we actually returned something
               if ( archiveTable.CORRECTIVEACTION.Count > 0 )
               {
                  // Create a CORRECTIVEACTION table object
                  if ( mCorrectiveAction == null )
                  {
                     mCorrectiveAction = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mCorrectiveAction.ByteData = mCorrectiveAction.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the WORKTYPES table
      /// </summary>
      /// <param name="iWorkTypes">Enumerate dlist of Work Types</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCmmsWorkTypes iWorkTypes )
      {
         // set the default result
         Boolean result = true;

         if ( iWorkTypes != null )
         {
            // create a new WORKTYPE packet object
            ArchiveWorkTypeTable archiveTable= new ArchiveWorkTypeTable();

            try
            {
               foreach ( CmmsWorkTypeRecord prefs in iWorkTypes )
               {
                  archiveTable.WORKTYPE.AddWorkTypeRecord( prefs );
               }

               // only continue if we actually returned something
               if ( archiveTable.WORKTYPE.Count > 0 )
               {
                  // Create a WORKTYPE table object
                  if ( mWorkType == null )
                  {
                     mWorkType = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mWorkType.ByteData = mWorkType.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the PRIORITIES table
      /// </summary>
      /// <param name="iPriorities">Enumerated list of Notification Priorities</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCmmsPriorities iPriorities )
      {
         // set the default result
         Boolean result = true;

         if ( iPriorities != null )
         {
            // create a new PRIORITY packet object
            ArchivePriorityTable archiveTable= new ArchivePriorityTable();
            try
            {
               foreach ( CmmsPriorityRecord priority in iPriorities )
               {
                  archiveTable.PRIORITY.AddPriorityRecord( priority );
               }

               // only continue if we actually returned something
               if ( archiveTable.PRIORITY.Count > 0 )
               {
                  // Create a PRIORITY table object
                  if ( mPriority == null )
                  {
                     mPriority = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mPriority.ByteData = mPriority.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }

         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the PROBLEMS table
      /// </summary>
      /// <param name="iProblems">Enumerated list of NotificationProblems</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCmmsProblems iProblems )
      {
         // set the default result
         Boolean result = true;

         if ( iProblems != null )
         {
            // create a new PROBLEM packet object
            ArchiveProblemTable archiveTable= new ArchiveProblemTable();
            try
            {
               foreach ( CmmsProblemRecord priority in iProblems )
               {
                  archiveTable.PROBLEM.AddProblemRecord( priority );
               }

               // only continue if we actually returned something
               if ( archiveTable.PROBLEM.Count > 0 )
               {
                  // Create a PROBLEM table object
                  if ( mProblem == null )
                  {
                     mProblem = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mProblem.ByteData = mProblem.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the WORKNOTIFICATIONS table
      /// </summary>
      /// <param name="iWorkNotifications">Enumerate dlist of Work Notifications</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumCmmsWorkNotifications iWorkNotifications )
      {
         // set the default result
         Boolean result = true;

         if ( iWorkNotifications != null )
         {

            // create a new WORKNOTIFICATION packet object
            ArchiveWorkNotificationTable archiveTable= new ArchiveWorkNotificationTable();

            try
            {
               foreach ( CmmsWorkNotification priority in iWorkNotifications )
               {
                  archiveTable.WORKNOTIFICATION.AddWorkNotificationRecord( priority );
               }

               // only continue if we actually returned something
               if ( archiveTable.WORKNOTIFICATION.Count > 0 )
               {
                  // Create a WORKNOTIFICATION table object
                  if ( mWorkNotification == null )
                  {
                     mWorkNotification = new ArchiveRecord();
                  }

                  // if there's nothing there - then don't archive!
                  if ( archiveTable.WORKNOTIFICATION.Count > 0 )
                  {
                     // convert to an array of bytes
                     byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                     // create a compressed data packet and populate
                     mWorkNotification.ByteData = mWorkNotification.Compress( myarray );
                  }
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
          // how did that go?
          return result;
      }


      /// <summary>
      /// Archive the NOTES table
      /// </summary>
      /// <param name="iNotes">Enumerated list of saved notes</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumNotes iNotes )
      {
         // set the default result
         Boolean result = true;

         if ( iNotes != null )
         {
            // create a new NOTES packet object
            ArchiveNotesTable archiveTable= new ArchiveNotesTable();
            try
            {
               foreach ( NotesRecord note in iNotes )
               {
                  archiveTable.NOTES.AddNoteRecord( note );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.NOTES.Count > 0 )
               {
                  // Create a NOTES table object
                  if ( mNotes == null )
                  {
                     mNotes = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mNotes.ByteData = mNotes.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the MESSAGES table
      /// </summary>
      /// <param name="iMessages">Enumerate dlist of Message objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumMessages iMessages )
      {
         // set the default result
         Boolean result = true;

         if ( iMessages != null )
         {
            // create a new MESSAGE packet object
            ArchiveMessageTable archiveTable= new ArchiveMessageTable();

            try
            {
               foreach ( MessageRecord message in iMessages )
               {
                  archiveTable.MESSAGE.AddMessageRecord( message );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.MESSAGE.Count > 0 )
               {
                  // Create a MESSAGE table object
                  if ( mMessageTable == null )
                  {
                     mMessageTable = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mMessageTable.ByteData = mMessageTable.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the INSTRUCTIONS table
      /// </summary>
      /// <param name="iInstructions">Enumerated list of Instructions</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumInstructions iInstructions )
      {
         // set the default result
         Boolean result = true;

         if ( iInstructions != null )
         {
            // create a new INSTRUCTIONS packet object
            ArchiveInstructionsTable archiveTable= new ArchiveInstructionsTable();

            try
            {
               foreach ( InstructionsRecord message in iInstructions )
               {
                  archiveTable.INSTRUCTIONS.AddInstructionsRecord( message );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.INSTRUCTIONS.Count > 0 )
               {
                  // Create a INSTRUCTIONS table object
                  if ( mInstructions == null )
                  {
                     mInstructions = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mInstructions.ByteData = mInstructions.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the DEVICEPROFILE table
      /// </summary>
      /// <param name="iProfile">Single Device Profile object</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( DeviceProfile iProfile )
      {
         // set the default result
         Boolean result = true;

         if ( iProfile != null )
         {
            // create a new DEVICEPROFILE packet object
            ArchiveDeviceProfileTable archiveTable= new ArchiveDeviceProfileTable();

            try
            {
               // populate the profile object
               archiveTable.DEVICEPROFILE.DeviceName = iProfile.DeviceName;
               archiveTable.DEVICEPROFILE.LastModified = iProfile.LastModified;
               archiveTable.DEVICEPROFILE.ProfileID = iProfile.ProfileID;
               archiveTable.DEVICEPROFILE.ProfileName = iProfile.ProfileName;

               // Create a DEVICEPROFILE table object
               if ( mDeviceProfile == null )
               {
                  mDeviceProfile = new ArchiveRecord();
               }

               // convert to an array of bytes
               byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

               // create a compressed data packet and populate
               mDeviceProfile.ByteData = mDeviceProfile.Compress( myarray );
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the CONDITIONALPOINT table
      /// </summary>
      /// <param name="iConditionals">Enumerated list of Conditionals</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumConditionalPoints iConditionals )
      {
         // set the default result
         Boolean result = true;

         if ( iConditionals != null )
         {
            // create a new CONDITIONALPOINT packet object
            ArchiveConditinalPointTable archiveTable= new ArchiveConditinalPointTable();

            try
            {
               foreach ( ConditionalPointRecord conditional in iConditionals )
               {
                  archiveTable.CONDITIONALPOINT.AddConditionalPointRecord( conditional );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.CONDITIONALPOINT.Count > 0 )
               {
                  // Create a CONDITIONALPOINT table object
                  if ( mConditionalPoint == null )
                  {
                     mConditionalPoint = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mConditionalPoint.ByteData = mConditionalPoint.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the SAVEDMEDIA table
      /// </summary>
      /// <param name="iMediaRecords">Enumerated list of Saved Media objects</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumSavedMedia iMediaRecords )
      {
         // set the default result
         Boolean result = true;

         if ( iMediaRecords != null )
         {
            // create a new SAVEDMEDIA packet object
            ArchiveSavedMediaTable archiveTable= new ArchiveSavedMediaTable();

            try
            {
               foreach ( SavedMediaRecord record in iMediaRecords )
               {
                  archiveTable.SAVEDMEDIA.AddMediaRecord( record );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.SAVEDMEDIA.Count > 0 )
               {
                  // Create a SAVEDMEDIA table object
                  if ( mSavedMedia == null )
                  {
                     mSavedMedia = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mSavedMedia.ByteData = mSavedMedia.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the DERIVEDITEMS table
      /// </summary>
      /// <param name="iDerivedItems">Enumerated list of Derived Items</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumDerivedItems iDerivedItems )
      {
         // set the default result
         Boolean result = true;

         if ( iDerivedItems != null )
         {
            // create a new DERIVEDITEMS packet object
            ArchiveDerivedItemsTable archiveTable= new ArchiveDerivedItemsTable();

            try
            {
               foreach ( DerivedItemsRecord record in iDerivedItems )
               {
                  archiveTable.DERIVEDITEMS.AddDerivedItemsRecord( record );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.DERIVEDITEMS.Count > 0 )
               {
                  // Create a DERIVEDITEMS table object
                  if ( mDerivedItems == null )
                  {
                     mDerivedItems = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mDerivedItems.ByteData = mDerivedItems.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the DERIVEDPOINT table
      /// </summary>
      /// <param name="iDerivedPoints">Enumerated list of Derived Items</param>
      /// <returns>True on success, else false</returns>
      public Boolean Add_Table_To_Archive( EnumDerivedPoints iDerivedPoints )
      {
         // set the default result
         Boolean result = true;

         if ( iDerivedPoints != null )
         {
            // create a new DERIVEDPOINT packet object
            ArchiveDerivedPointTable archiveTable= new ArchiveDerivedPointTable();

            try
            {
               foreach ( DerivedPointRecord record in iDerivedPoints )
               {
                  archiveTable.DERIVEDPOINT.AddDerivedPointRecord( record );
               }

               // if there's nothing there - then don't archive!
               if ( archiveTable.DERIVEDPOINT.Count > 0 )
               {
                  // Create a DERIVEDPOINT table object
                  if ( mDerivedPoint == null )
                  {
                     mDerivedPoint = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mDerivedPoint.ByteData = mDerivedPoint.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }

      #endregion

      //----------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// Archive the MACHINENOPSKIPS table
      /// </summary>
      /// <param name="iMachineSkips"></param>
      /// <returns>True on success, else false</returns>
      private Boolean Add_MACHINENOPSKIPS_To_Archive( EnumMachineSkips iMachineSkips )
      {
         // set the default result
         Boolean result = true;

         if ( iMachineSkips != null )
         {
            // create a new MACHINENOPSKIPS packet object
            ArchiveMachineNopSkipsTable archiveTable= new ArchiveMachineNopSkipsTable();

            try
            {
               foreach ( MachineSkipRecord prefs in iMachineSkips )
               {
                  archiveTable.MACHINENOPSKIPS.AddRecord( prefs );
               }

               // only continue if we actually returned something
               if ( archiveTable.MACHINENOPSKIPS.Count > 0 )
               {
                  // Create a MACHINENOPSKIPS table object
                  if ( mMachineNopSkips == null )
                  {
                     mMachineNopSkips = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mMachineNopSkips.ByteData = mMachineNopSkips.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the MACHINEOKSKIPS table
      /// </summary>
      /// <param name="iMachineSkips"></param>
      /// <returns>True on success, else false</returns>
      private Boolean Add_MACHINEOKSKIPS_To_Archive( EnumMachineSkips iMachineSkips )
      {
         // set the default result
         Boolean result = true;

         if ( iMachineSkips != null )
         {
            // create a new MACHINEOKSKIPS packet object
            ArchiveMachineOkSkipsTable archiveTable= new ArchiveMachineOkSkipsTable();

            try
            {
               foreach ( MachineSkipRecord prefs in iMachineSkips )
               {
                  archiveTable.MACHINEOKSKIPS.AddRecord( prefs );
               }

               // only continue if we actually returned something
               if ( archiveTable.MACHINEOKSKIPS.Count > 0 )
               {
                  // Create a MACHINEOKSKIPS table object
                  if ( mMachineOkSkips == null )
                  {
                     mMachineOkSkips = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mMachineOkSkips.ByteData = mMachineOkSkips.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the VELDATA table
      /// </summary>
      /// <param name="iVelMeasurements"></param>
      /// <returns>True on success, else false</returns>
      private Boolean Add_VELDATA_To_Archive( EnumSavedMeasurements iVelMeasurements )
      {
         // set the default result
         Boolean result = true;

         if ( iVelMeasurements != null )
         {
            // create a new Velocity Data packet object
            ArchiveVelDataTable archiveTable= new ArchiveVelDataTable();

            try
            {
               // populate the Velocity Data packet object
               foreach ( SavedMeasurement measurement in iVelMeasurements )
               {
                  if ( measurement != null )
                  {
                     archiveTable.VELDATA.AddMeasurement( measurement );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.VELDATA.Count > 0 )
               {
                  // Create a VELDATA table object
                  if ( mVelData == null )
                  {
                     mVelData = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mVelData.ByteData = mVelData.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the TEMPDATA table
      /// </summary>
      /// <param name="iTempMeasurements"></param>
      /// <returns>True on success, else false</returns>
      private Boolean Add_TEMPDATA_To_Archive( EnumSavedMeasurements iTempMeasurements )
      {
         // set the default result
         Boolean result = true;

         if ( iTempMeasurements != null )
         {
            // create a new TEMPDATA packet object
            ArchiveTempDataTable archiveTable= new ArchiveTempDataTable();

            try
            {
               // populate the TEMPDATA packet object
               foreach ( SavedMeasurement measurement in iTempMeasurements )
               {
                  if ( measurement != null )
                  {
                     archiveTable.TEMPDATA.AddMeasurement( measurement );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.TEMPDATA.Count > 0 )
               {
                  // Create a TEMPDATA table object
                  if ( mTempData == null )
                  {
                     mTempData = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mTempData.ByteData = mTempData.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }


      /// <summary>
      /// Archive the ENVDATA table
      /// </summary>
      /// <param name="iEnvMeasurements"></param>
      /// <returns>True on success, else false</returns>
      private Boolean Add_ENVDATA_To_Archive( EnumSavedMeasurements iEnvMeasurements )
      {
         // set the default result
         Boolean result = true;

         if ( iEnvMeasurements != null )
         {
            // create a new ENVDATA packet object
            ArchiveEnvDataTable archiveTable= new ArchiveEnvDataTable();

            try
            {
               // populate the ENVDATA packet object
               foreach ( SavedMeasurement measurement in iEnvMeasurements )
               {
                  if ( measurement != null )
                  {
                     archiveTable.ENVDATA.AddMeasurement( measurement );
                  }
               }

               // only continue if we actually returned something
               if ( archiveTable.ENVDATA.Count > 0 )
               {
                  // Create a ENVDATA table object
                  if ( mEnvData == null )
                  {
                     mEnvData = new ArchiveRecord();
                  }

                  // convert to an array of bytes
                  byte[] myarray = archiveTable.XmlToByteArray( archiveTable );

                  // create a compressed data packet and populate
                  mEnvData.ByteData = mEnvData.Compress( myarray );
               }
            }
            catch
            {
               result = false;
            }
            finally
            {
               archiveTable = null;
            }
         }
         // how did that go?
         return result;
      }

      #endregion

      //----------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2012
//  Add to project
//----------------------------------------------------------------------------
