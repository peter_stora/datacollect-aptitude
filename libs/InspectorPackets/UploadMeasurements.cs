﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// This class creates a series of enumerated measurement objects, with one
    /// object for each measurement type. This class will also hold the FFT type
    /// when it is eventually implemented
    /// </summary>
    public class UploadMeasurements
    {
        private UploadPointMeasurements mVelData = null;
        private UploadPointMeasurements mEnvData = null;
        private UploadPointMeasurements mTempData = null;
        private BaseFFTChannels mFFTChannels = null;

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public UploadMeasurements()
        {
        }/* end constructor */


        /// <summary>
        /// Add a single measurement to the Velocity enumerations list
        /// </summary>
        /// <param name="iMeasurement">Measurement object with set alarm state</param>
        public void AddVelMeasurement(UploadMeasurement iMeasurement)
        {
            // if Vel data is null then instantiate
            if (mVelData == null)
                mVelData = new UploadPointMeasurements();

            // add the measurement
            mVelData.AddMeasurement(iMeasurement);

        }/* end method */


        /// <summary>
        /// Add a single measurement to the Envelope enumerations list
        /// </summary>
        /// <param name="iMeasurement">Measurement object with set alarm state</param>
        public void AddEnvMeasurement(UploadMeasurement iMeasurement)
        {
            // if Env data is null then instantiate
            if (mEnvData == null)
                mEnvData = new UploadPointMeasurements();
            
            // add the measurement
            mEnvData.AddMeasurement(iMeasurement);

        }/* end method */


        /// <summary>
        /// Add a single measurement to the Temperature enumerations list
        /// </summary>
        /// <param name="iMeasurement">Measurement object with set alarm state</param>
        public void AddTempMeasurement(UploadMeasurement iMeasurement)
        {
            // if temp data is null then instantiate
            if (mTempData == null)
                mTempData = new UploadPointMeasurements();
            
            // add the measurement
            mTempData.AddMeasurement(iMeasurement);

        }/* end method */


        /// <summary>
        /// Add a single FFT Channel to the FFT enumerations list
        /// </summary>
        /// <param name="iMeasurement">Measurement object with set alarm state</param>
        public void AddFFTChannel(FFTChannelBase iFFTChannelRecord)
        {
            // if temp data is null then instantiate
            if (mFFTChannels == null)
                mFFTChannels = new BaseFFTChannels();

            // add the measurement
            mFFTChannels.AddFFTChannel(iFFTChannelRecord);

        }/* end method */



        /// <summary>
        /// Binary data list for MCD velocity data
        /// </summary>
        [XmlElement("TempData")]
        public UploadPointMeasurements TempData
        {
            get { return mTempData; }
            set { mTempData = value; }
        }/* end property */


        /// <summary>
        /// Binary data list for MCD velocity data
        /// </summary>
        [XmlElement("VelData")]
        public UploadPointMeasurements VelData
        {
            get { return mVelData; }
            set { mVelData = value; }
        }/* end property */


        /// <summary>
        /// Binary data list for MCD envelope data
        /// </summary>
        [XmlElement("EnvData")]
        public UploadPointMeasurements EnvData
        {
            get { return mEnvData; }
            set { mEnvData = value; }
        }/* end property */


        /// <summary>
        /// Binary data list for MCD Channel (FFT) data
        /// </summary>
        [XmlElement("FFT")]
        public BaseFFTChannels FFTChannel
        {
            get { return mFFTChannels; }
            set { mFFTChannels = value; }
        }/* end property */

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 30th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------

