﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage an enumerated list of Coded Notes
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumCodedNoteRecords : PacketBase
   {
      protected List<CodedNoteRecord> mCodedNoteRecords;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumCodedNoteRecords()
      {
         mCodedNoteRecords = new List<CodedNoteRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw CodedNoteRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "C_NOTE" )]
      public List<CodedNoteRecord> CodedNote
      {
         get
         {
            return mCodedNoteRecords;
         }
         set
         {
            mCodedNoteRecords = value;
         }
      }

      /// <summary>
      /// Add a new Coded Note record to the list
      /// </summary>
      /// <param name="item">populated Coded Note record object</param>
      /// <returns>number of Coded Note objects available</returns>
      public int AddCodedNoteRecord( CodedNoteRecord item )
      {
         mCodedNoteRecords.Add( item );
         return mCodedNoteRecords.Count;
      }

      /// <summary>
      /// How many Coded Note records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mCodedNoteRecords.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Class EnumCodedNoteRecords with support for IEnumerable. This extended
   /// functionality enables not only foreach support - but also (and much
   /// more importantly) LINQ support!
   /// </summary>
   public class EnumCodedNotes : EnumCodedNoteRecords, IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mCodedNoteRecords.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mCodedNoteRecords[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------