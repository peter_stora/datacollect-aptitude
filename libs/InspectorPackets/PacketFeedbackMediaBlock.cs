﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Dummy block for feedback use only
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketFeedbackMediaBlock : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.MEDIA_BLOCK;

      /// <summary>
      /// Gets or sets the Media File Block Index
      /// </summary>
      public int BlockID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Media File UID
      /// </summary>
      public Guid FileUID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Media File Name
      /// </summary>
      public string FileName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the total number of file bytes
      /// </summary>
      public int FileBytes
      {
         get;
         set;
      }

      /// <summary>
      /// Session Identifier
      /// </summary>
      public string SessionID
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 04th October 2011
//  Add to project
//----------------------------------------------------------------------------
