﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simple class to pass data errors to the UI layer
   /// </summary>
   public class DataConnectError
   {
      /// <summary>
      /// Base constructor - defaults to connection error
      /// </summary>
      public DataConnectError() :
         this( TableName.ALL, ErrorType.Connection )
      {
      }

      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iAffected"></param>
      /// <param name="iType"></param>
      public DataConnectError( TableName iAffected, ErrorType iType )
      {
         this.TableAffected = iAffected;
         this.IOErrorType = iType;
      }

      /// <summary>
      /// Gets or sets the affected table
      /// </summary>
      public TableName TableAffected
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the error type
      /// </summary>
      public ErrorType IOErrorType
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Enumeration of Data Error Types
   /// </summary>
   public enum ErrorType
   {
      Connection,
      Read,
      Write,
      Clear,
      Update
   }

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 22nd July 2010
//  Add to project
//----------------------------------------------------------------------------