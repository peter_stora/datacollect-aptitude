﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single ConditionalPoint table record entry
    /// </summary>
    [XmlRoot(ElementName = "CONDITIONALPOINT")]
    public class ConditionalPointRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public ConditionalPointRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Always set to zero
        /// </summary>
        public int ID {get; set;}
        
        /// <summary>
        /// Any Point referenced by this field is considered as being conditional. 
        /// The reference is via the indexed field “POINTS.Number” in the POINTS 
        /// table. This field was called NodeUid in the original Marlin database
        /// </summary>
        public int ConditionalPointNumber {get; set;}
        
        /// <summary>
        /// This represents the actual point to reference the condition against 
        /// (via the field “Number” in the “POINTS” table). This field was called 
        /// PointUid in the original Marlin database.
        /// </summary>
        public int ReferencePointNumber {get; set;}
        
        /// <summary>
        /// 21500 (Type 1): IN ALARM
        /// 21501 (Type 2): OUT OF ALARM
        /// 21502 (Type 3): ABOVE SET VALUE
        /// 21503 (Type 4): BELOW SET VALUE
        /// 21504 (Type 5): IN RANGE
        /// 21505 (Type 6): OUT OF RANGE
        /// 21506 (Type 7): IS EQUAL TO
        /// 21507 (Type 8): IS NOT EQUAL TO
        /// </summary>
        public int CriteriaType {get; set;}
        
        /// <summary>
        /// The minimum valid range for the reference point
        /// </summary>
        public Double MinRange {get; set;}
        
        /// <summary>
        /// The maximum valid range for the reference point
        /// </summary>
        public Double MaxRange {get; set;}
        
        /// <summary>
        /// When Conditional upon an Inspection POINT result, this represents
        /// the choice (1-5) used to determine equality/unequality
        /// </summary>
        public int ResultIdx { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------