﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Derived Items table record entry
    /// </summary>
    [XmlRoot(ElementName = "DERIVEDITEMS")]
    public class DerivedItemsRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public DerivedItemsRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Table Primary key (not used)
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Foreign key, Uid of Table "Derived Point"
        /// </summary>
        public Guid DerivedUid { get; set; }

        /// <summary>
        /// Index in equation order
        /// </summary>
        public int FormulaItemPosition { get; set; }

        /// <summary>
        /// 1-4 =Var (ov, mcd.e, mcd.v, mcd.t); See VarKey
        /// 1004 = Const; see ConstValue
        /// 200X = Operation, X  = Operation Type
        /// 30YY = Function;  YY = Function Type
        /// </summary>
        public int FormulaItemType { get; set; }

        /// <summary>
        /// Link to subling machine point via the field "POINTS.Number"
        /// </summary>
        public int VarKey { get; set; }

        /// <summary>
        /// Value of constant
        /// </summary>
        public Double ConstValue { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------