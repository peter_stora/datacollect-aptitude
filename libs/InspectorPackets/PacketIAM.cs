﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// IAM Packet Type - returned to Server on receipt of UR packet
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketIAM : PacketBase
    {
        /// <summary>
        /// Packet Identifier
        /// </summary>
        public byte Packet = (byte)PacketType.IAM;

        /// <summary>
        /// IAM Packet Object
        /// </summary>
        public DeviceIAM IAM;


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <param name="iVersion">Current version of device firmware</param>
        public PacketIAM()
            : this(DEFAULT_UID, null)
        {
        }/* base constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <param name="iVersion">Current version of device firmware</param>
        public PacketIAM(String iUID, HwVersion iVersion)
            : this(iUID, iVersion, DEFAULT_PRE_LICENSED)
        {
        }/* overload constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <param name="iVersion"></param>
        /// <param name="iDeviceLicensed"></param>
        public PacketIAM(String iUID, HwVersion iVersion, Boolean iDeviceLicensed)
        {
            IAM = new DeviceIAM(iUID, iVersion, iDeviceLicensed);
        }/* overload constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------