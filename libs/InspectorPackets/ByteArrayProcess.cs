﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Small helper class to manipulate arrays of byte data
   /// </summary>
   public static class ByteArrayProcess
   {
      /// <summary>
      /// Maximum page size bytes (512KB)
      /// </summary>
      private static int MAX_PAGE_SIZE_BYTES = 0x80000;
      
      /// <summary>
      /// Clear the read buffer and reset its internal size to zero
      /// </summary>
      /// <param name="refBuffer">call by reference array to be cleared</param>
      /// <returns>false on exception raised</returns>
      public static Boolean ClearByteBuffer( ref byte[] refBuffer )
      {
         try
         {
            Array.Clear( refBuffer, 0, refBuffer.Length );
            Array.Resize<byte>( ref refBuffer, 0 );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Are two given byte arrays identical?
      /// </summary>
      /// <param name="iArrayA">source array A</param>
      /// <param name="iArrayB">source array B</param>
      /// <returns>true if arrays matchm, else false</returns>
      public static Boolean ByteArraysMatch( byte[] iArrayA, byte[] iArrayB )
      {
         // if either array is undefined then exit here
         if ( ( iArrayA == null ) | ( iArrayB == null ) )
         {
            return false;
         }

         // set a new default result
         Boolean arraysMatch = true;

         // chec k the array lengths
         if ( iArrayA.Length == iArrayB.Length )
         {
            // compare the actual array bytes
            for ( int i=0; i < iArrayA.Length; ++i )
            {
               // if any byte pair do not match then fail
               if ( iArrayA[ i ] != iArrayB[ i ] )
               {
                  arraysMatch = false;
                  break;
               }
            }
         }

         // if address lengths do not match then fail
         else
         {
            arraysMatch = false;
         }

         // return the result
         return arraysMatch;

      }/* end method */


      /// <summary>
      /// Write the contents of a byte array to a named file
      /// </summary>
      /// <param name="fileName">filename with path</param>
      /// <param name="buff">byte array to save </param>
      public static void WriteArrayToFile(string fileName, byte[] iBuffer)
      {
         // create a managed filestream object
         using ( FileStream fs = new FileStream( fileName, 
            FileMode.Create, FileAccess.ReadWrite ) )
         {
            // reference to a binary writer
            BinaryWriter bw = new BinaryWriter( fs );

            // write the array
            bw.Write( iBuffer );

            // close the binary writer
            bw.Close();

            // close the file stream
            fs.Close();
         }

      }/* end method */


      /// <summary>
      /// Write a "type safe" list of bytes to a named file
      /// </summary>
      /// <param name="fileName">filename with path</param>
      /// <param name="buff">list of bytes </param>
      public static void WriteArrayToFile( string iFileName, List<byte> iBufferList )
      {
         // create a managed filestream object
         using ( FileStream fs = new FileStream( iFileName,
            FileMode.Create, FileAccess.ReadWrite ) )
         {
            // reference to a binary writer
            BinaryWriter bw = new BinaryWriter( fs );
            
            // reset the byte index and set the page size
            int listIndex = 0;
            int incrementBy = MAX_PAGE_SIZE_BYTES;

            // build the initial page buffer
            byte[] buffer = new byte[ incrementBy ];
            
            // write all full page size byte arrays to file
            while ( ( iBufferList.Count - listIndex ) >= incrementBy )
            {
               iBufferList.CopyTo(listIndex, buffer, 0, incrementBy);
               bw.Write(buffer);
               listIndex = listIndex + incrementBy;
            }

            // write the last (incomplete) byte array to file
            if ( ( iBufferList.Count - listIndex ) > 0 )
            {
               ResizeByteBuffer( ref buffer, iBufferList.Count - listIndex ); 
               iBufferList.CopyTo( listIndex, buffer, 0, iBufferList.Count - listIndex );
               bw.Write( buffer );
               ClearByteBuffer( ref buffer );
            }

            // close the binary writer
            bw.Close();

            // close the file stream
            fs.Close();
         }

      }/* end method */


      /// <summary>
      /// Returns the number of occurences of a byte pattern within
      /// a given array of bytes
      /// </summary>
      /// <param name="iPattern">byte pattern</param>
      /// <param name="iBuffer">reference array</param>
      /// <returns>total number of occurences</returns>
      public static int CountBytePattern( byte[] iPattern, byte[] iBuffer )
      {
         int matches = 0;
         for ( int i = 0; i < iBuffer.Length; i++ )
         {
            if ( iPattern[ 0 ] == iBuffer[ i ] && iBuffer.Length - i >= iPattern.Length )
            {
               bool matched = true;
               for ( int j = 1; j < iPattern.Length && matched == true; j++ )
               {
                  if ( iBuffer[ i + j ] != iPattern[ j ] )
                     matched = false;
               }
               if ( matched )
               {
                  matches++;
                  i += iPattern.Length - 1;
               }
            }
         }
         return matches;

      }/* end method */


      /// <summary>
      /// Returns the number of occurences of a byte pattern within
      /// a given type-safe list of bytes
      /// </summary>
      /// <param name="iPattern">byte pattern</param>
      /// <param name="iBuffer">reference bytes list</param>
      /// <returns>total number of pattern occurences</returns>
      public static int CountBytePattern( byte[] iPattern, List<byte> iBuffer )
      {
         int matches = 0;
         for ( int i = 0; i < iBuffer.Count; i++ )
         {
            if ( iPattern[ 0 ] == iBuffer[ i ] && iBuffer.Count - i >= iPattern.Length )
            {
               bool matched = true;
               for ( int j = 1; j < iPattern.Length && matched == true; j++ )
               {
                  if ( iBuffer[ i + j ] != iPattern[ j ] )
                     matched = false;
               }
               if ( matched )
               {
                  matches++;
                  i += iPattern.Length - 1;
               }
            }
         }
         return matches;

      }/* end method */


      /// <summary>
      /// Given an initial starting index, return the byte index of the 
      /// next available pattern within the array list
      /// </summary>
      /// <param name="iPattern">byte pattern to reference</param>
      /// <param name="iBuffer">source list</param>
      /// <param name="iIndex">search after this byte</param>
      /// <returns>next index</returns>
      public static int IndexFirstBytePattern( byte[] iPattern, byte[] iBuffer, int iIndex )
      {
         int index = -1;

         for ( int i = iIndex; i < iBuffer.Length - iIndex; i++ )
         {
            if ( iPattern[ 0 ] == iBuffer[ i ] && iBuffer.Length - i >= iPattern.Length )
            {
               bool matched = true;
               for ( int j = 1; j < iPattern.Length && matched == true; j++ )
               {
                  if ( iBuffer[ i + j ] != iPattern[ j ] )
                     matched = false;
               }
               if ( matched )
               {
                  index = i;
                  break;
               }
            }
         }

         return index;

      }/* end method */


      /// <summary>
      /// Given an initial starting index, return the byte index of the 
      /// next available pattern within the array list
      /// </summary>
      /// <param name="iPattern">byte pattern to reference</param>
      /// <param name="iBuffer">source list</param>
      /// <param name="iIndex">search after this byte</param>
      /// <returns>next index</returns>
      public static int IndexFirstBytePattern( byte[] iPattern, List<byte> iBuffer, int iIndex )
      {
         int index = -1;
         
         for ( int i = iIndex; i < iBuffer.Count - iIndex; i++ )
         {
            if ( iPattern[ 0 ] == iBuffer[ i ] && iBuffer.Count - i >= iPattern.Length )
            {
               bool matched = true;
               for ( int j = 1; j < iPattern.Length && matched == true; j++ )
               {
                  if ( iBuffer[ i + j ] != iPattern[ j ] )
                     matched = false;
               }
               if ( matched )
               {
                  index = i;
                  break;
               }
            }
         }

         return index;

      }/* end method */


      /// <summary>
      /// Simple method that resizes an array of bytes
      /// </summary>
      /// <param name="iBuffer">Reference byte array</param>
      /// <param name="iNewByteSize">new size in bytes</param>
      /// <returns>false on exception raised</returns>
      public static Boolean ResizeByteBuffer( ref byte[] iBuffer, int iNewByteSize )
      {
         try
         {
            ClearByteBuffer( ref iBuffer );
            Array.Resize<byte>( ref iBuffer, iNewByteSize  );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Append source byte array to destination byte array
      /// </summary>
      /// <param name="iSourceArray">Source data to be appended to end destination array</param>
      /// <param name="iDestinationArray">Destination array (returned as reg argument)</param>
      /// <returns>false on exception raised</returns>
      public static Boolean AppendToByteBuffer( byte[] iSourceArray, ref byte[] iDestinationArray )
      {
         try
         {
            int index = iDestinationArray.Length;
            Array.Resize<byte>( ref iDestinationArray, ( iDestinationArray.Length + iSourceArray.Length ) );
            Array.Copy( iSourceArray, 0, iDestinationArray, index, iSourceArray.Length );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Appends one array of bytes (iSourceArray) to the end of another
      /// byte array (iDestinationArray), given both the length and starting
      /// index of the data within the source array
      /// </summary>
      /// <param name="iSourceArray">source byte array</param>
      /// <param name="iDestinationArray">destination byte array</param>
      /// <param name="iSourceIndex">byte array start</param>
      /// <param name="iLength">number of bytes to write</param>
      /// <returns>false on error</returns>
      public static Boolean AppendToByteBuffer( byte[] iSourceArray,
                                         byte[] iDestinationArray,
                                         int iSourceIndex, 
                                         int iLength )
      {
         try
         {
            Array.Copy( 
               iSourceArray, 
               iSourceIndex, 
               iDestinationArray, 
               iDestinationArray.Length - 1, 
               iLength );

            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Extract the contents of one array into another
      /// </summary>
      /// <param name="iSourceArray">Array containing all data</param>
      /// <param name="iDestinationArray">Array that will contain extracted data</param>
      /// <param name="iStart">Source array start point index</param>
      /// <param name="iBytes">Number of bytes to copy</param>
      /// <returns>false on error</returns>
      public static Boolean ExtractArray( byte[] iSourceArray,
                                         ref byte[] iDestinationArray,
                                         int iStart, int iBytes )
      {
         try
         {
            Array.Resize<byte>( ref iDestinationArray, iBytes );
            Array.Copy( iSourceArray, iStart, iDestinationArray, 0, iBytes );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */



      /// <summary>
      /// Trim away n elements from the start of a byte array
      /// </summary>
      /// <param name="refBuffer"></param>
      /// <param name="iBytes"></param>
      /// <returns></returns>
      public static byte[] TrimStart( byte[] refBuffer, int iBytes )
      {
         // set the last byte to the last element in the read buffer
         int lastByte = refBuffer.Length;

         try
         {
            // create a new cache
            byte[]cache = new byte[ refBuffer.Length - iBytes ];

            // skip the start of the input buffer
            Array.Copy(refBuffer, iBytes, cache, 0, refBuffer.Length - iBytes);

            // resize the input buffer to remove the nulls
            Array.Resize<byte>( ref refBuffer, refBuffer.Length - iBytes );

            // reinstate the buffer from the cache
            Array.Copy( cache, refBuffer, cache.Length );

            // clear the cache (ready for GC)
            ClearByteBuffer( ref cache );

            // return the reprocessd array
            return refBuffer;
         }
         catch
         {
            return null;
         }

      }/* end method */



      /// <summary>
      /// Trim away all null (0x00) elements from a byte array
      /// </summary>
      /// <param name="refBuffer">call by reference byte array</param>
      /// <returns>the new array size</returns>
      /// <returns>false on exception raised</returns>
      public static byte[] TrimNulls( byte[] refBuffer )
      {
         // set the last byte to the last element in the read buffer
         int lastByte = refBuffer.Length;

         try
         {
            //
            // scan back through the input buffer looking for the last
            // non zero element. When found set the last byte value
            //
            for ( int i = refBuffer.Length - 1; i > 0; --i )
            {
               if ( refBuffer[ i ] != 0x00 )
               {
                  lastByte = i;
                  break;
               }
            }

            // resize the input buffer to remove the nulls
            Array.Resize<byte>( ref refBuffer, lastByte + 1 );

            // return the reprocessd array
            return refBuffer;
         }
         catch
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Converts a Byte Array into a single HEX string without
      /// any delimiting characters. On error this method will 
      /// return the characters "00"
      /// </summary>
      /// <param name="iByteBuffer">Byte Array to process</param>
      /// <returns>String of Bytes</returns>
      public static String ByteArrayToHexString( byte[] iByteBuffer )
      {
         try
         {
            StringBuilder hexString = new StringBuilder();
            foreach ( byte i in iByteBuffer )
            {
               hexString.Append( i.ToString( "X2" ) );
            }
            return hexString.ToString();
         }
         catch
         {
            return "00";
         }

      }/* end method */


      /// <summary>
      /// Converts a single HEX string to an array of bytes. On error
      /// this method will return a single byte (0x00)
      /// </summary>
      /// <param name="iHexString">Hex string</param>
      /// <returns>byte array</returns>
      public static byte[] HexStringToByteArray( string iHexString )
      {
         try
         {
            int byteLength = iHexString.Length / 2;
            byte[] bytes = new byte[ byteLength ];
            string hex;
            int j = 0;
            for ( int i = 0; i < bytes.Length; i++ )
            {
               hex = new String( new Char[] { iHexString[ j ], iHexString[ j + 1 ] } );
               bytes[ i ] = byte.Parse( hex, System.Globalization.NumberStyles.HexNumber );
               j = j + 2;
            }
            return bytes;
         }
         catch
         {
            byte[] fail = new byte[] { 0x00 };
            return fail;
         }

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------


