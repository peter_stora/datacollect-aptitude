﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// "Authenticate Me" Packet Type
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketAuthenticateMe : PacketBase
    {
        /// <summary>
        /// The actual Packet type (AUTHENTICATE ME)
        /// </summary>
        public byte Packet = (byte)PacketType.AUTHENTICATE;


        /// <summary>
        /// Authentication object
        /// </summary>
        public Authentication Authenticate;


        /// <summary>
        /// Base Constructor
        /// </summary>
        public PacketAuthenticateMe()
            : this(NULL_KEY)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <param name="iKey">Server Authentication Key</param>
        public PacketAuthenticateMe(string iKey)
        {
            Authenticate = new Authentication(iKey);
        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------
