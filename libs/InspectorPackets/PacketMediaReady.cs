﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Notify Server that there are media files to be uploaded
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketMediaReady : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.MEDIA_READY;


      /// <summary>
      /// Session Identifier
      /// </summary>
      public string SessionID
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Device UID
      /// </summary>
      public string UID
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 04th October 2011
//  Add to project
//----------------------------------------------------------------------------