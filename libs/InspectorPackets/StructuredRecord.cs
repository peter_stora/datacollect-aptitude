﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Structured Route table record entry
    /// </summary>
    [XmlRoot(ElementName = "STRUCTURED")]
    public class StructuredRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public StructuredRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Primary Key of the table
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// RouteId of the Structured Route
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// Time the User first entered the ROUTE.
        /// (Was seconds since 01/01/1980 12:00am)
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Time the User collected last point in ROUTE
        /// (Was seconds since 01/01/1980 12:00am)
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Duration in seconds 
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// The User who initiated Structured ROUTE collection
        /// </summary>
        public int OperId { get; set; }

        /// <summary>
        /// ID of the next point to be collected. References the
        /// field “Number” from the table “POINTS”
        /// </summary>
        public int NextPoint { get; set; }

        /// <summary>
        /// Current Start time
        /// (Was seconds since 01/01/1980 12:00am)
        /// </summary>
        public DateTime InternalStart { get; set; }

        /// <summary>
        /// Percentage Complete = points collected / number of points
        /// </summary>
        public double PercentComplete { get; set; }

        /// <summary>
        /// A= Active , S= suspended I=inactive
        /// </summary>
        public byte Status { get; set; }

        /// <summary>
        /// Last point signifying end of Route
        /// </summary>
        public int LastPoint { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------
