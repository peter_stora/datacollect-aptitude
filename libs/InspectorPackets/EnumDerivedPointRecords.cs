﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Derived Point Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumDerivedPointRecords : PacketBase
    {
        protected List<DerivedPointRecord> mDerivedPointRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumDerivedPointRecords()
        {
            mDerivedPointRecord = new List<DerivedPointRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw DerivedPointRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("DERIVEDPOINT")]
        public List<DerivedPointRecord> DerivedPoint
        {
            get { return mDerivedPointRecord; }
            set { mDerivedPointRecord = value; }
        }


        /// <summary>
        /// Add a new Derived Point record to the list
        /// </summary>
        /// <param name="item">populated Derived Point record object</param>
        /// <returns>number of Derived Point objects available</returns>
        public int AddDerivedPointRecord(DerivedPointRecord item)
        {
            mDerivedPointRecord.Add(item);
            return mDerivedPointRecord.Count;
        }

        /// <summary>
        /// How many Derived Point records are now available
        /// </summary>
        public int Count { get { return mDerivedPointRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumDerivedPointRecords with support for IEnumerable. This 
    /// extended functionality enables not only foreach support - but also 
    /// (and much more importantly) LINQ support!
    /// </summary>
    public class EnumDerivedPoints : EnumDerivedPointRecords,
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mDerivedPointRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mDerivedPointRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------