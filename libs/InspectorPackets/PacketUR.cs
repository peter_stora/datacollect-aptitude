﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Packet UR issued by the Middleware Server on each client connection 
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketUR : PacketBase
    {
        public byte Packet = (byte)PacketType.UR;
        public ServerConnect UR;

        /// <summary>
        /// Base Constructor
        /// </summary>
        public PacketUR()
            : this(DEFAULT_SERVER, DEFAULT_PROFILE)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iServerType">Server Type (Desktop/Enterprise)</param>
        /// <param name="iProfileType">Profile Type (User/Desktop)</param>
        public PacketUR(MiddlewareType iServerType,
                        ProfileType iProfileType)
        {
           UR = new ServerConnect( iServerType, iProfileType );

        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------
