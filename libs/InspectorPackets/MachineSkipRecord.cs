﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class for Table Archive purposes only. 
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   public class MachineSkipRecord : MachineSkips
   {
      /// <summary>
      /// Gets or sets the Operators ID
      /// </summary>
      public int OperId
      {
         get;
         set;
      }
   }


   /// <summary>
   /// Class to manage an enumerated list of Archived Operator Table records 
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumMachineSkipRecords : PacketBase
   {
      protected List<MachineSkipRecord> mMachineSkips;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumMachineSkipRecords()
      {
         mMachineSkips = new List<MachineSkipRecord>();
      }


      /// <summary>
      /// Get or set elements in the raw Machine Skips Record list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "MACHINESKIPS" )]
      public List<MachineSkipRecord> MACHINESKIPS
      {
         get
         {
            return mMachineSkips;
         }
         set
         {
            mMachineSkips = value;
         }
      }


      /// <summary>
      /// Add a new Machine Skips Record to the list
      /// </summary>
      /// <param name="item">populated Machine Skips Record</param>
      /// <returns>number of Machine Skips Records available</returns>
      public int AddRecord( MachineSkipRecord item )
      {
         mMachineSkips.Add( item );
         return mMachineSkips.Count;
      }


      /// <summary>
      /// How many Machine Skips records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mMachineSkips.Count;
         }
      }
   }


   /// <summary>
   /// Class EnumMachineSkipRecords with support for IEnumerable.
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   public class EnumMachineSkips : EnumMachineSkipRecords,
       IEnumerable, IEnumerator
   {
      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mMachineSkips.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mMachineSkips[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */
   
   }

}
//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 22nd June 2012
//  Add to project
//----------------------------------------------------------------------------