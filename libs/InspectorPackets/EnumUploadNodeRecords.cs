﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Upload Node Table records 
    /// </summary>
    public class EnumUploadNodeRecords
    {
        protected List<UploadNodeRecord> mUploadNodeRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumUploadNodeRecords()
        {
            mUploadNodeRecord = new List<UploadNodeRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw NodeRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("NODE")]
        public List<UploadNodeRecord> NODE
        {
            get { return mUploadNodeRecord; }
            set { mUploadNodeRecord = value; }
        }

        /// <summary>
        /// Add a new Node record to the list
        /// </summary>
        /// <param name="item">populated Node record object</param>
        /// <returns>number of Node objects available</returns>
        public int AddNodeRecord(UploadNodeRecord item)
        {
            mUploadNodeRecord.Add(item);
            return mUploadNodeRecord.Count;
        }/* end method */


        /// <summary>
        /// How many Node records are now available
        /// </summary>
        public int Count { get { return mUploadNodeRecord.Count; } }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
