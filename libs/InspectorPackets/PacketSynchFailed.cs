﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Synchronization Failed Packet - returned if server spits dummy!
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketSynchFailed : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.SYNCH_FAILED;

      /// <summary>
      /// Date and time of failure event
      /// </summary>
      public DateTime AT = PacketBase.DateTimeNow;

      /// <summary>
      /// Gets or sets the device UID
      /// </summary>
      public string DeviceUid
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets an internal error code
      /// </summary>
      public int ErrorCode
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 8th July 2010
//  Added Device Uid to packet
//
//  Revision 0.0 APinkerton 30th June 2010
//  Add to project
//----------------------------------------------------------------------------