﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2008 - 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   public static class Alarms
   {
      //
      // Note: These alarms are mapped to the bit flags
      // used internally within @ptitude - and not the 
      // original MDM alarms which required additional 
      // bit-field logic. 
      //

      /// <summary>
      /// Bit flag for no alarms set
      /// </summary>
      public static byte ALARM_NONE       = 0x00;

      /// <summary>
      /// Bit flag - use the Danger High alarm
      /// </summary>
      public static byte ALARM_DANGERHI   = 0x01;

      /// <summary>
      /// Bit flag - use the Danger Low alarm
      /// </summary>
      public static byte ALARM_DANGERLO   = 0x02;

      /// <summary>
      /// Bit flag - use the Alert High alarm
      /// </summary>
      public static byte ALARM_ALERTHI    = 0x04;

      /// <summary>
      /// Bit flag - use the Alert High alarm
      /// </summary>
      public static byte ALARM_ALERTLO    = 0x08;

      /// <summary>
      /// Bit flag - map generic Alert flag to Alert High
      /// </summary>
      public static byte ALARM_ALERT     = ALARM_ALERTHI;

      /// <summary>
      /// Bit flag - map generic Alarm flag to Danger High
      /// </summary>
      public static byte ALARM_DANGER    = ALARM_DANGERHI;

      /// <summary>
      /// How many choices for Inspection data
      /// </summary>
      public static int NO_OF_INSPECTION_CHOICES = 5;


      #region WMCD range limit constants

      // Velocity upper and lower ranges in ips
      public static Double WMCD_FULL_SCALE_IPS  = 3.0;
      public static Double WMCD_MIN_SCALE_IPS   = 0.0;

      // Velocity upper and lower ranges in mm/s
      public static Double WMCD_FULL_SCALE_MMS  = 55.0;
      public static Double WMCD_MIN_SCALE_MMS   = 0.0;

      // Envelope upper and lower ranges in gE
      public static Double WMCD_FULL_SCALE_GE   = 20.0;
      public static Double WMCD_MIN_SCALE_GE    = 0.0;

      // Temperature upper and lower ranges in DegC
      public static Double WMCD_FULL_SCALE_CEL  = 100.0;
      public static Double WMCD_MIN_SCALE_CEL   = -30.0;

      // Temperature upper and lower ranges in DegF
      public static Double WMCD_FULL_SCALE_FRH  = 212.0;
      public static Double WMCD_MIN_SCALE_FRH   = -22.0;

      /// <summary>
      /// amount by which a rescaled range is multiplied
      /// </summary>
      private static Double RANGE_RESCALE_FACTOR = 0.1;

      #endregion


      #region WMCD range set point constants

      // Velocity range set points in ips and mm/s
      public static Double WMCD_ALERT_IPS       = 0.2;
      public static Double WMCD_DANGER_IPS      = 0.35;
      public static Double WMCD_ALERT_MMS       = 4.0;
      public static Double WMCD_DANGER_MMS      = 7.0;

      // Acceleration range set points in gE
      public static Double WMCD_ALERT_GE        = 2.0;
      public static Double WMCD_DANGER_GE       = 5.0;

      // Temperature rane set points in DegC and DegF
      public static Double WMCD_ALERT_CEL       = 62.0;
      public static Double WMCD_DANGER_CEL      = 73.0;
      public static Double WMCD_ALERT_FRH       = 170.0;
      public static Double WMCD_DANGER_FRH      = 190.0;

      #endregion


      #region Alarm Mapping and validation

      /// <summary>
      /// Converts a full process type alarm into a simplified Point
      /// or Node level alarm (i.e. it ignores upper and lower levels)
      /// </summary>
      /// <param name="alarmState">process alarm state</param>
      /// <returns>node alarm state</returns>
      public static FieldTypes.NodeAlarmState MapProcessAlarmToNodeAlarm(
         FieldTypes.AlarmState alarmState )
      {
         if ( ( alarmState == FieldTypes.AlarmState.Alert ) |
            ( alarmState == FieldTypes.AlarmState.AlertHi ) |
            ( alarmState == FieldTypes.AlarmState.AlertLo ) )
         {
            return FieldTypes.NodeAlarmState.Alert;
         }

         else if ( ( alarmState == FieldTypes.AlarmState.Danger ) |
            ( alarmState == FieldTypes.AlarmState.DangerHi ) |
            ( alarmState == FieldTypes.AlarmState.DangerLo ) )
         {
            return FieldTypes.NodeAlarmState.Danger;
         }

         else if ( ( alarmState == FieldTypes.AlarmState.Clear ) |
            ( alarmState == FieldTypes.AlarmState.ClearHi ) |
            ( alarmState == FieldTypes.AlarmState.ClearLo ) )
         {
            return FieldTypes.NodeAlarmState.Clear;
         }

         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */


      /// <summary>
      /// Validates a process record's alarm levels against the point's
      /// range limits. In the event of any alarm exceeding a range, then
      /// that range will be reset above (or below) that alarm, and the
      /// process field InvalidRangeLimits will be set true.
      /// This method added primarily to address OTD #2199
      /// </summary>
      /// <param name="iProcess">Refrenced process record</param>
      public static void ValidateRanges( ref ProcessRecord iProcess )
      {
         // do nothing when the process record is null
         if ( iProcess != null )
         {
            Double rangeMax = iProcess.RangeMax;
            Double rangeMin = iProcess.RangeMin;

            //
            // does this point support High Alarm? If yes, then confirm that
            // it does not fall outside of the point's range limits
            //
            if ( ( iProcess.AlarmState & ALARM_ALERTHI ) == ALARM_ALERTHI )
            {
               SetAutoRangeLimits( iProcess.UpperAlert, ref rangeMax, ref rangeMin );
            }

            //
            // does this point support High Danger? If yes, then confirm that
            // it does not fall outside of the point's range limits
            //
            if ( ( iProcess.AlarmState & ALARM_DANGERHI ) == ALARM_DANGERHI )
            {
               SetAutoRangeLimits( iProcess.UpperDanger, ref rangeMax, ref rangeMin );
            }

            //
            // does this point support Low Alarm? If yes, then confirm that
            // it does not fall outside of the point's range limits
            //
            if ( ( iProcess.AlarmState & ALARM_ALERTLO ) == ALARM_ALERTLO )
            {
               SetAutoRangeLimits( iProcess.LowerAlert, ref rangeMax, ref rangeMin );
            }

            //
            // does this point support Low Alarm? If yes, then confirm that
            // it does not fall outside of the point's range limits
            //
            if ( ( iProcess.AlarmState & ALARM_DANGERLO ) == ALARM_DANGERLO )
            {
               SetAutoRangeLimits( iProcess.LowerDanger, ref rangeMax, ref rangeMin );
            }

            if ( ( rangeMin != iProcess.RangeMin ) | ( rangeMax != iProcess.RangeMax ) )
            {
               iProcess.RangeMin = rangeMin;
               iProcess.RangeMax = rangeMax;
               iProcess.AutoRange = true;
            }
         }

      }/* end method */


      /// <summary>
      /// Set the auto-range limits based on a reference value
      /// </summary>
      /// <param name="iReference"></param>
      /// <param name="rangeMax"></param>
      /// <param name="rangeMin"></param>
      private static void SetAutoRangeLimits( Double iReference, 
         ref Double rangeMax, ref Double rangeMin )
      {
         Boolean autoRangeMax = false;
         Boolean autoRangeMin = false;

         // set an initial full range value
         Double fullRange = rangeMax - rangeMin;

         //
         // if the reference exceeds the current maximum
         // range then reset the maximum range to reference
         //
         if ( iReference > rangeMax )
         {
            rangeMax = iReference;
            autoRangeMax = true;
         }

         //
         // if the reference is below the current minimum
         // range then reset the minimum range to reference
         //
         else if ( iReference < rangeMin )
         {
            rangeMin = iReference;
            autoRangeMin = true;
         }

         //
         // if the maximum range was reset to the reference,
         // then add an upper alarm area of ten percent
         //
         if ( autoRangeMax )
         {
            fullRange = rangeMax - rangeMin;
            rangeMax = Math.Ceiling( rangeMax +
               ( fullRange * RANGE_RESCALE_FACTOR ) );
         }

         //
         // if the minimum range was reset to the reference,
         // then add an lower alarm area of ten percent
         //
         if ( autoRangeMin )
         {
            fullRange = rangeMax - rangeMin;
            rangeMin = Math.Floor( rangeMin -
               ( fullRange * RANGE_RESCALE_FACTOR ) );
         }

      }/* end method */

      #endregion


      #region Calculate Inspection Alarm state

      /// <summary>
      /// Returns the alarm flags for both single or multiple-select Inspections
      /// </summary>
      /// <param name="iProcessType">single or multiple select inspections</param>
      /// <param name="iMeasurementValue">current measurement value</param>
      /// <param name="iInspectionRecord">populated inspection object</param>
      /// <returns>Point and Node level alarm state</returns>
      public static FieldTypes.NodeAlarmState GetInspectionAlarmFlag(
         FieldTypes.ProcessType iProcessType,
         Double iMeasurementValue,
         InspectionRecord iInspectionRecord )
      {
         if ( iInspectionRecord != null )
         {
            //
            // The DCE needs all unitialized values to be double.NaN
            // so if we haven't collected data, return None
            //
            if ( Double.IsNaN( iMeasurementValue ) )
            {
               return FieldTypes.NodeAlarmState.None;
            }

            // express the measurement value as an integer
            int selection = Convert.ToInt16( iMeasurementValue );

            // for single selection inspections 
            if ( iProcessType == FieldTypes.ProcessType.SingleSelectInspection )
            {
               return GetSingleInspectionAlarmFlag( selection, iInspectionRecord );
            }
            // for multiple selection inspections 
            else if ( iProcessType == FieldTypes.ProcessType.MultiSelectInspection )
            {
               return GetMultipleInspectionAlarmFlag( selection, iInspectionRecord );
            }
            // ignore for anything else
            else
            {
               return FieldTypes.NodeAlarmState.None;
            }
         }
         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */


      /// <summary>
      /// Get the overall alarm state for a Multiple-Inspection value
      /// </summary>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">populated point inspection</param>
      private static FieldTypes.NodeAlarmState GetSingleInspectionAlarmFlag(
         int iSelection, InspectionRecord settings )
      {
         // reset the the alarm and choice states
         FieldTypes.InspectionAlarms alarm = FieldTypes.InspectionAlarms.None;
         FieldTypes.InspectionChoices setChoice = FieldTypes.InspectionChoices.NoneSet;

         // set the default mask state to choices made
         ushort mask = (ushort) FieldTypes.InspectionChoices.NoneSet;

         // go through each of the choices and get the selected alarm state
         for ( int choice=0; choice < NO_OF_INSPECTION_CHOICES; ++choice )
         {
            switch ( choice )
            {
               case 0:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice1;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType1;
                  }
                  break;
               case 1:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice2;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType2;
                  }
                  break;
               case 2:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice3;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType3;
                  }
                  break;
               case 3:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice4;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType4;
                  }
                  break;
               case 4:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice5;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType5;
                  }
                  break;
               case 5:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice6;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType6;
                  }
                  break;
               case 6:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice7;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType7;
                  }
                  break;
               case 7:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice8;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType8;
                  }
                  break;
               case 8:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice9;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType9;
                  }
                  break;
               case 9:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice10;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType10;
                  }
                  break;
               case 10:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice11;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType11;
                  }
                  break;
               case 11:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice12;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType12;
                  }
                  break;
               case 12:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice13;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType13;
                  }
                  break;
               case 13:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice14;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType14;
                  }
                  break;
               case 14:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice15;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     alarm = (FieldTypes.InspectionAlarms) settings.AlarmType15;
                  }
                  break;      
               default:
                  alarm = FieldTypes.InspectionAlarms.None;
                  break;
            }

            // if we have a choice set then break from the check loop
            if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
            {
               break;
            }
         }

         // lastly, set the points alarm value
         if ( alarm == FieldTypes.InspectionAlarms.Alert )
         {
            return FieldTypes.NodeAlarmState.Alert;
         }
         else if ( alarm == FieldTypes.InspectionAlarms.Danger )
         {
            return FieldTypes.NodeAlarmState.Danger;
         }
         else
         {
            return FieldTypes.NodeAlarmState.Clear;
         }

      }/* end method */


      /// <summary>
      /// Get the overall alarm state for a Multiple-Inspection value
      /// </summary>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">populated point inspection</param>
      private static FieldTypes.NodeAlarmState GetMultipleInspectionAlarmFlag(
         int iSelection, InspectionRecord settings )
      {
         // reset the the alarm and choice states
         FieldTypes.InspectionAlarms alarm = FieldTypes.InspectionAlarms.None;
         FieldTypes.InspectionChoices setChoice = FieldTypes.InspectionChoices.NoneSet;

         // set the default mask state to choices made
         ushort mask = (ushort) FieldTypes.InspectionChoices.NoneSet;

         // go through each of the choices and get the selected alarm state
         for ( int choice=0; choice < NO_OF_INSPECTION_CHOICES; ++choice )
         {
            switch ( choice )
            {
               case 0:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice1;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType1 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType1;
                     }
                  }
                  break;
               case 1:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice2;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType2 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType2;
                     }
                  }
                  break;
               case 2:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice3;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType3 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType3;
                     }
                  }
                  break;
               case 3:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice4;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType4 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType4;
                     }
                  }
                  break;
               case 4:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice5;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType5 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType5;
                     }
                  }
                  break;
               case 5:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice6;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType6 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType6;
                     }
                  }
                  break;
               case 6:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice7;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType7 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType7;
                     }
                  }
                  break;
               case 7:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice8;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType8 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType8;
                     }
                  }
                  break;
               case 8:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice9;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType9 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType9;
                     }
                  }
                  break;
               case 9:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice10;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType10 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType10;
                     }
                  }
                  break;
               case 10:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice11;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType11 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType11;
                     }
                  }
                  break;
               case 11:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice12;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType12 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType12;
                     }
                  }
                  break;
               case 12:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice13;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType13 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType13;
                     }
                  }
                  break;
               case 13:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice14;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType14 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType14;
                     }
                  }
                  break;
               case 14:
                  mask = (ushort) FieldTypes.InspectionChoices.Choice15;
                  setChoice = (FieldTypes.InspectionChoices) ( iSelection & mask );
                  if ( setChoice != FieldTypes.InspectionChoices.NoneSet )
                  {
                     if ( (FieldTypes.InspectionAlarms) settings.AlarmType15 > alarm )
                     {
                        alarm = (FieldTypes.InspectionAlarms) settings.AlarmType15;
                     }
                  }
                  break; 
               default:
                  alarm = FieldTypes.InspectionAlarms.None;
                  break;
            }
         }

         // lastly, set the points alarm value
         if ( alarm == FieldTypes.InspectionAlarms.Alert )
         {
            return FieldTypes.NodeAlarmState.Alert;
         }
         else if ( alarm == FieldTypes.InspectionAlarms.Danger )
         {
            return FieldTypes.NodeAlarmState.Danger;
         }
         else
         {
            return FieldTypes.NodeAlarmState.Clear;
         }

      }/* end method */

      #endregion


      #region Calculate Process Alarm state

      /// <summary>
      /// Calculate the alarm state of the current process point. This
      /// method will process "Level", "In Window" and "Out of Window"
      /// alarm types.
      /// </summary>
      /// <param name="iPointRecord">reference point object</param>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">process record object</param>
      public static FieldTypes.AlarmState GetProcessAlarmFlag(
         Double iMeasurement, ProcessRecord iProcessRecord )
      {
         if ( iProcessRecord != null )
         {
            // if alarms disabled - then ignore!
            if ( iProcessRecord.AlarmType == (byte) FieldTypes.AlarmType.Off )
            {
               return FieldTypes.AlarmState.Off;
            }

            // for fixed "Level" alarms
            else if ( iProcessRecord.AlarmType == (byte) FieldTypes.AlarmType.Level )
            {
               return ProcessLevelAlarms( iMeasurement, iProcessRecord );
            }

            // for fixed "Out of Window" alarms
            else if ( iProcessRecord.AlarmType == (byte) FieldTypes.AlarmType.OutOfWindow )
            {
               return ProcessOutOfWindowAlarms( iMeasurement, iProcessRecord );
            }

            // for fixed "In Window" alarms
            else if ( iProcessRecord.AlarmType == (byte) FieldTypes.AlarmType.InWindow )
            {
               return ProcessInWindowAlarms( iMeasurement, iProcessRecord );
            }

            // for anything else - just disable the alarm state
            else
            {
               return FieldTypes.AlarmState.Off;
            }
         }
         else
         {
            return FieldTypes.AlarmState.Off;
         }

      }/* end method */


      /// <summary>
      /// Process the alarm condition for an "In Window" alarm type
      /// </summary>
      /// <param name="iMeasurement">Reference measurement</param>
      /// <param name="iProcessRecord">Process record</param>
      /// <returns>full alarm type</returns>
      private static FieldTypes.AlarmState ProcessLevelAlarms(
         double iMeasurement, ProcessRecord iProcessRecord )
      {
         // create a window alarm validation object
         ValidateAlarmLevels windowAlarm = new ValidateAlarmLevels( iProcessRecord );

         // lets make sure we actually have a range to work with
         if ( windowAlarm.FullRange != 0 )
         {
            // express measurement value as a percentage
            double valuePercent = windowAlarm.ValueAsPercentage( iMeasurement );

            if ( ( valuePercent >= 0 ) & ( valuePercent <= 100 ) )
            {
               // for use Danger and value greater than danger
               if ( ( windowAlarm.UseHighDanger ) &
                  ( valuePercent >= windowAlarm.HighDanger ) )
               {
                  return FieldTypes.AlarmState.Danger;
               }

               // for use Alert and value greater than high alert
               if ( ( windowAlarm.UseHighAlert ) &
                  ( valuePercent >= windowAlarm.HighAlert ) )
               {
                  return FieldTypes.AlarmState.Alert;
               }

               // making the last card in the pack Clear!
               return FieldTypes.AlarmState.Clear;
            }
            // fix for defect #1365
            else
            {
               if ( valuePercent < 0 )
                  return FieldTypes.AlarmState.DangerLo;
               else
                  return FieldTypes.AlarmState.DangerHi;
            }

         }

         // if the range was zero - then alarms are disabled
         return FieldTypes.AlarmState.Off;

      }/* end method */


      /// <summary>
      /// Process the alarm condition for an "In Window" alarm type
      /// </summary>
      /// <param name="iMeasurement">Reference measurement</param>
      /// <param name="iProcessRecord">Process record</param>
      /// <returns>full alarm type</returns>
      private static FieldTypes.AlarmState ProcessInWindowAlarms(
         double iMeasurement, ProcessRecord iProcessRecord )
      {
         // create a window alarm validation object
         ValidateAlarmLevels windowAlarm = new ValidateAlarmLevels( iProcessRecord );

         // lets make sure we actually have a range to work with
         if ( windowAlarm.FullRange != 0 )
         {
            // express measurement value as a percentage
            double valuePercent = windowAlarm.ValueAsPercentage( iMeasurement );

            if ( ( valuePercent >= 0 ) & ( valuePercent <= 100 ) )
            {
               // for use high alert and value greater than high alert
               if ( ( windowAlarm.UseHighAlert ) &
                  ( valuePercent > windowAlarm.HighAlert ) )
               {
                  return FieldTypes.AlarmState.ClearHi;
               }

               // for use low alert and value less than high alert
               if ( ( windowAlarm.UseLowAlert ) &
                  ( valuePercent < windowAlarm.LowAlert ) )
               {
                  return FieldTypes.AlarmState.ClearLo;
               }

               // for use high danger and value greater than high danger
               if ( ( windowAlarm.UseHighDanger ) &
                  ( windowAlarm.UseHighAlert ) &
                  ( valuePercent > windowAlarm.HighDanger ) &
                  ( valuePercent <= windowAlarm.HighAlert ) )
               {
                  return FieldTypes.AlarmState.AlertHi;
               }

               // for use high danger and value greater than high danger
               if ( ( windowAlarm.UseHighDanger ) &
                  ( !windowAlarm.UseHighAlert ) &
                  ( valuePercent > windowAlarm.HighDanger ) )
               {
                  return FieldTypes.AlarmState.ClearHi;
               }


               // for use high danger and value greater than high danger
               if ( ( windowAlarm.UseLowDanger ) &
                  ( windowAlarm.UseLowAlert ) &
                  ( valuePercent >= windowAlarm.LowAlert ) &
                  ( valuePercent < windowAlarm.LowDanger ) )
               {
                  return FieldTypes.AlarmState.AlertLo;
               }

               // for use high danger and value greater than high danger
               if ( ( windowAlarm.UseLowDanger ) &
                  ( !windowAlarm.UseLowAlert ) &
                  ( valuePercent < windowAlarm.LowDanger ) )
               {
                  return FieldTypes.AlarmState.ClearLo;
               }

               // making the last card in the pack danger!
               return FieldTypes.AlarmState.Danger;
            }

            // fix for defect 1365
            else
            {
               if ( valuePercent < 0 )
                  return FieldTypes.AlarmState.DangerLo;
               else
                  return FieldTypes.AlarmState.DangerHi;
            }
         }

         // if the range was zero - then alarms are disabled
         return FieldTypes.AlarmState.Off;

      }/* end method */


      /// <summary>
      /// Process the alarm condition for an "Out of Window" alarm type
      /// </summary>
      /// <param name="iMeasurement">Reference measurement</param>
      /// <param name="iProcessRecord">Process record</param>
      /// <returns>full alarm type</returns>
      private static FieldTypes.AlarmState ProcessOutOfWindowAlarms(
         double iMeasurement, ProcessRecord iProcessRecord )
      {
         // create a window alarm validation object
         ValidateAlarmLevels windowAlarm = new ValidateAlarmLevels( iProcessRecord );

         // lets make sure we actually have a range to work with
         if ( windowAlarm.FullRange != 0 )
         {
            // express current value as a percentage
            double valuePercent = windowAlarm.ValueAsPercentage( iMeasurement );

            if ( ( valuePercent >= 0 ) & ( valuePercent <= 100 ) )
            {
               // if we're in the High Danger band then set alarm and exit here
               if ( ( windowAlarm.UseHighDanger ) &
                  ( valuePercent >= windowAlarm.HighDanger ) )
               {
                  return FieldTypes.AlarmState.DangerHi;
               }

               // if we're in the Low Danger band then set alarm and exit here
               if ( ( windowAlarm.UseLowDanger ) &
                  ( valuePercent <= windowAlarm.LowDanger ) )
               {
                  return FieldTypes.AlarmState.DangerLo;
               }

               // if we're in the High Alert band then set alarm and exit here
               if ( ( windowAlarm.UseHighAlert ) &
                  ( valuePercent >= windowAlarm.HighAlert ) )
               {
                  return FieldTypes.AlarmState.AlertHi;
               }

               // if we're in the Low Alert band then set alarm and exit here
               if ( ( windowAlarm.UseLowAlert ) &
                  ( valuePercent <= windowAlarm.LowAlert ) )
               {
                  return FieldTypes.AlarmState.AlertLo;
               }

               // ok, all that's left is clear!
               return FieldTypes.AlarmState.Clear;
            }

            // for range errors - just set as either danger HI or LO
            else
            {
               if ( valuePercent < 0 )
                  return FieldTypes.AlarmState.DangerLo;
               else
                  return FieldTypes.AlarmState.DangerHi;
            }
         }

         // if the range was zero - then alarms are disabled
         return FieldTypes.AlarmState.Off;

      }/* end method */

      #endregion


      #region Calculate MCD Alarm state

      /// <summary>
      /// Calculate the alarm state of the current process point. This
      /// method will process "Level", "In Window" and "Out of Window"
      /// alarm types.
      /// </summary>
      /// <param name="iPointRecord">reference point object</param>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">process record object</param>
      public static FieldTypes.NodeAlarmState GetMCDAlarmFlag(
         PointRecord iPointRecord, MCCRecord iMCCRecord )
      {
         // alarm flags
         FieldTypes.NodeAlarmState tempAlarmState = FieldTypes.NodeAlarmState.None;
         FieldTypes.NodeAlarmState velAlarmState = FieldTypes.NodeAlarmState.None;
         FieldTypes.NodeAlarmState envAlarmState = FieldTypes.NodeAlarmState.None;

         // measurements
         PointMeasurement tempData;
         PointMeasurement envData;
         PointMeasurement velData;

         if ( ( iPointRecord != null ) & ( iMCCRecord != null ) )
         {
            // MCD data should always have Envelope and Velocity readings in pairs
            int lastMeasIndex = iPointRecord.VelData.Count - 1;

            if ( lastMeasIndex >= 0 )
            {
               // get the measurement
               envData = iPointRecord.EnvData.Measurement[ lastMeasIndex ];

               // get the measurement
               velData = iPointRecord.VelData.Measurement[ lastMeasIndex ];

               // get the VELDATA alarm state
               velAlarmState = GetMCDVelocityAlarmState( iMCCRecord, velData.Value );

               // get the ENVDATA alarm state
               envAlarmState = GetMCDEnvelopeAlarmState( iMCCRecord, envData.Value );
            }

            //
            // MCD POINTs don't have to contain a Temperature reading.
            // Check the DTS on the 'latest' Temperature reading to see
            // if it belongs to this set of data
            //
            int lastTempMeasIndex = iPointRecord.TempData.Count - 1;

            if ( lastTempMeasIndex >= 0 )
            {
               // if the DTS of the temperature matches the vib...
               if ( iPointRecord.TempData.Measurement[ lastTempMeasIndex ].LocalTime ==
                   iPointRecord.VelData.Measurement[ lastMeasIndex ].LocalTime )
               {
                  // get the measurement
                  tempData = iPointRecord.TempData.Measurement[ lastTempMeasIndex ];

                  // get the TEMPDATA alarm state
                  tempAlarmState = GetMCDTempAlarmState( iMCCRecord, tempData.Value );
               }
            }

            // Are any of the three values in Danger?
            if ( ( tempAlarmState == FieldTypes.NodeAlarmState.Danger ) |
               ( envAlarmState == FieldTypes.NodeAlarmState.Danger ) |
               ( velAlarmState == FieldTypes.NodeAlarmState.Danger ) )
            {
               return FieldTypes.NodeAlarmState.Danger;
            }

            // nothing in danger? OK are any of the three value in alert?
            if ( ( tempAlarmState == FieldTypes.NodeAlarmState.Alert ) |
            ( envAlarmState == FieldTypes.NodeAlarmState.Alert ) |
            ( velAlarmState == FieldTypes.NodeAlarmState.Alert ) )
            {
               return FieldTypes.NodeAlarmState.Alert;
            }

            // nothing in danger or alert? OK are any of the three values clear?
            if ( ( tempAlarmState == FieldTypes.NodeAlarmState.Clear ) |
            ( envAlarmState == FieldTypes.NodeAlarmState.Clear ) |
            ( velAlarmState == FieldTypes.NodeAlarmState.Clear ) )
            {
               return FieldTypes.NodeAlarmState.Clear;
            }

            // if all else fails - return now!
            return FieldTypes.NodeAlarmState.None;
         }
         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */


      /// <summary>
      /// Calculate the Alarm status of the MCD temperature element. In the
      /// event of no alarm flags being set by @ptitude, the default (internal)
      /// MCD alarm levels will referenced instead.
      /// </summary>
      /// <param name="iMCCRecord">Populated MCD object</param>
      /// <param name="iValue">Value to reference</param>
      /// <returns>Calculated Alarm State</returns>
      public static FieldTypes.NodeAlarmState GetMCDTempAlarmState(
         MCCRecord iMCCRecord, double iValue )
      {
         // reset the alarm use conditions
         Boolean useAlarm = false;
         Boolean useDanger = false;

         if ( iMCCRecord != null )
         {
            // is the Danger High bit set? If yes then enable Danger High property
            if ( ( iMCCRecord.TmpAlarmState & Alarms.ALARM_DANGER ) == Alarms.ALARM_DANGER )
            {
               useDanger = true;
            }

            // is the Danger Low bit set? If yes then enable Danger Low property
            if ( ( iMCCRecord.TmpAlarmState & Alarms.ALARM_ALERT ) == Alarms.ALARM_ALERT )
            {
               useAlarm = true;
            }

            // are we using alarms here?
            if ( useAlarm | useDanger )
            {
               // if Danger flag is set then check for Danger state
               if ( useDanger )
               {
                  if ( iValue >= iMCCRecord.TmpAlarm1 )
                     return FieldTypes.NodeAlarmState.Danger;
               }
               // if Alert flag is set then check for Alert state
               if ( useAlarm )
               {
                  if ( iValue >= iMCCRecord.TmpAlarm2 )
                     return FieldTypes.NodeAlarmState.Alert;
               }
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }

            // if not then simply return as clear (OTD #2634)
            else
            {
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }
         }
         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */


      /// <summary>
      /// Calculate the Alarm status of the MCD velocity element. In the
      /// event of no alarm flags being set by @ptitude, the default (internal)
      /// MCD alarm levels will referenced instead.
      /// </summary>
      /// <param name="iMCCRecord">Populated MCD object</param>
      /// <param name="iValue">Value to reference</param>
      /// <returns>Calculated Alarm State</returns>
      public static FieldTypes.NodeAlarmState GetMCDVelocityAlarmState(
         MCCRecord iMCCRecord, double iValue )
      {
         // reset the alarm use conditions
         Boolean useAlarm = false;
         Boolean useDanger = false;

         if ( iMCCRecord != null )
         {
            // is the Danger High bit set? If yes then enable Danger High property
            if ( ( iMCCRecord.VelAlarmState & Alarms.ALARM_DANGER ) == Alarms.ALARM_DANGER )
            {
               useDanger = true;
            }

            // is the Danger Low bit set? If yes then enable Danger Low property
            if ( ( iMCCRecord.VelAlarmState & Alarms.ALARM_ALERT ) == Alarms.ALARM_ALERT )
            {
               useAlarm = true;
            }

            // are we using alarms here?
            if ( useAlarm | useDanger )
            {
               // if Danger flag is set then check for Danger state
               if ( useDanger )
               {
                  if ( iValue >= iMCCRecord.VelAlarm1 )
                     return FieldTypes.NodeAlarmState.Danger;
               }
               // if Alert flag is set then check for Alert state
               if ( useAlarm )
               {
                  if ( iValue >= iMCCRecord.VelAlarm2 )
                     return FieldTypes.NodeAlarmState.Alert;
               }
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }

            // if not then simply return as clear (OTD #2634)
            else
            {
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }
         }
         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */


      /// <summary>
      /// Calculate the Alarm status of the MCD enveloping element. In the
      /// event of no alarm flags being set by @ptitude, the default (internal)
      /// MCD alarm levels will referenced instead.
      /// </summary>
      /// <param name="iMCCRecord">Populated MCD object</param>
      /// <param name="iValue">Value to reference</param>
      /// <returns>Calculated Alarm State</returns>
      public static FieldTypes.NodeAlarmState GetMCDEnvelopeAlarmState(
         MCCRecord iMCCRecord, double iValue )
      {
         // reset the alarm use conditions
         Boolean useAlarm = false;
         Boolean useDanger = false;

         if ( iMCCRecord != null )
         {
            // is the Danger High bit set? If yes then enable Danger High property
            if ( ( iMCCRecord.EnvAlarmState & Alarms.ALARM_DANGER ) == Alarms.ALARM_DANGER )
            {
               useDanger = true;
            }

            // is the Danger Low bit set? If yes then enable Danger Low property
            if ( ( iMCCRecord.EnvAlarmState & Alarms.ALARM_ALERT ) == Alarms.ALARM_ALERT )
            {
               useAlarm = true;
            }

            // are we using alarms here?
            if ( useAlarm | useDanger )
            {
               // if Danger flag is set then check for Danger state
               if ( useDanger )
               {
                  if ( iValue >= iMCCRecord.EnvAlarm1 )
                     return FieldTypes.NodeAlarmState.Danger;
               }
               // if Alert flag is set then check for Alert state
               if ( useAlarm )
               {
                  if ( iValue >= iMCCRecord.EnvAlarm2 )
                     return FieldTypes.NodeAlarmState.Alert;
               }
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }

            // if not then simply return as clear (OTD #2634)
            else
            {
               // otherwise just return as Clear
               return FieldTypes.NodeAlarmState.Clear;
            }
         }
         else
         {
            return FieldTypes.NodeAlarmState.None;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 25th April 2012
//  Add support for fifteen inspections results
//
//  Revision 0.0 APinkerton 6th November 2009
//  Add to project
//----------------------------------------------------------------------------