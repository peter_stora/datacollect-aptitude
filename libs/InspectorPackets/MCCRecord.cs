﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single MCC table record entry
    /// </summary>
    public class MCCRecord : MCCRecordBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public MCCRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Current UID (GUID) for this node. Note: This value is
        /// derived and therefore independant of @ptitude
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Hierarchy position of the current node. Note: This value
        /// is derived and therefore independant of @ptitude
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Associated Point Uid (from Marlin POINT table)
        /// </summary>
        public Guid PointUid { get; set; }

        /// <summary>
        /// Get or set the Envelope alarm
        /// </summary>
        public byte EnvAlarmState { get; set; }

        /// <summary>
        /// Envelope Baseline Data time (unused)
        /// </summary>
        public DateTime EnvBaselineDataTime { get; set; }

        /// <summary>
        /// Envelope Baseline Data value (unused)
        /// </summary>
        public double EnvBaselineDataValue { get; set; }

        /// <summary>
        /// Get or set the Velocity alarm
        /// </summary>
        public byte VelAlarmState { get; set; }

        /// <summary>
        /// Velocity Baseline Data time (unused)
        /// </summary>
        public DateTime VelBaselineDataTime { get; set; }

        /// <summary>
        /// Velocity Baseline Data value (unused)
        /// </summary>
        public double VelBaselineDataValue { get; set; }

        /// <summary>
        /// Get or set the Temperature alarm state
        /// </summary>
        public byte TmpAlarmState { get; set; }

        /// <summary>
        /// Velocity Baseline Data time (unused)
        /// </summary>
        public DateTime TmpBaselineDataTime { get; set; }

        /// <summary>
        /// Velocity Baseline Data value (unused)
        /// </summary>
        public double TmpBaselineDataValue { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------