﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Instruction Table Packet
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketInstructionsTable : PacketBase
    {
        /// <summary>
        /// The actual Packet type (Instruction TABLE)
        /// </summary>
        public byte Packet = (byte)PacketType.TABLE_INSTRUCTIONS;

        /// <summary>
        /// Instruction Table Element
        /// </summary>
        [XmlElement("InstructionTable")]
        public EnumInstructionRecords InstructionTable = new EnumInstructionRecords();

        /// <summary>
        /// Constructor
        /// </summary>
        public PacketInstructionsTable()
        {
        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------