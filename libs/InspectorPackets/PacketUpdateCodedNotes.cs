﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Request a new set of Coded Notes from the Server
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketUpdateCodedNotes : PacketBase
    {
        /// <summary>
        /// Packet Type "Update Coded Notes"
        /// </summary>
        public byte Packet = (byte)PacketType.UPDATE_CODED_NOTES;

        /// <summary>
        /// Session Identifier
        /// </summary>
        public string SessionID { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2010
//  Add to project
//----------------------------------------------------------------------------