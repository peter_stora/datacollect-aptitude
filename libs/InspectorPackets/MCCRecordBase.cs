﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Base class for MCC properties object. The only fields in this class are
    /// those that can be edited from within the application itself, and then 
    /// returned to @ptitude as part of the core XML upload file.
    /// </summary>
    public class MCCRecordBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public MCCRecordBase()
        {
        }/* end constructor */

        // Revised Envelope alarm values 
        public double EnvAlarm1 { get; set; }
        public double EnvAlarm2 { get; set; }

        // Revised velocity alarm values 
        public double VelAlarm1 { get; set; }
        public double VelAlarm2 { get; set; }

        // Revised Temperature alarm values 
        public double TmpAlarm1 { get; set; }
        public double TmpAlarm2 { get; set; }

        // Revised SI values 
        public byte SystemUnits { get; set; }
        public byte TempUnits { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------