﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Operator Set Class that manages what messages are to be 
    /// displayed on the handheld during data collection
    /// </summary>
    public class MessagePreferences: PacketBase
    {
        /// <summary>
        /// Base Constructor
        /// </summary>
        public MessagePreferences()
            : this(PacketBase.DEFAULT_MSG_END_OF_ROUTE,
                    PacketBase.DEFAULT_MSG_ZERO_VELACC,
                    PacketBase.DEFAULT_MSG_SAVE_AS_CURRENT,
                    PacketBase.DEFAULT_MSG_FOUND_IN_OTHER_ROUTE,
                    PacketBase.DEFAULT_MSG_SHOW_SETS)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iEndOfRoute"></param>
        /// <param name="iZeroAcceleration"></param>
        /// <param name="iSaveDataAsCurrent"></param>
        /// <param name="iFoundInOtherRoute"></param>
        /// <param name="iShowSetsWhileCollecting"></param>
        /// <param name="?"></param>
        public MessagePreferences(Boolean iEndOfRoute,
                                  Boolean iZeroAcceleration,
                                  Boolean iSaveDataAsCurrent,
                                  Boolean iFoundInOtherRoute,
                                  Boolean iShowSetsWhileCollecting)
        {
            this.EndOfRoute = iEndOfRoute;
            this.ZeroAcceleration = iZeroAcceleration;
            this.SaveDataAsCurrent = iSaveDataAsCurrent;
            this.FoundInOtherRoute = iFoundInOtherRoute;
            this.ShowSetsWhileCollecting = iShowSetsWhileCollecting;
        }/* end constructor */


        /// <summary>
        /// Get or set: Show the end of route message
        /// </summary>
        public Boolean EndOfRoute { get; set; }

        /// <summary>
        /// Get or set: Allow zero acceleration value from the WMCD
        /// </summary>
        public Boolean ZeroAcceleration { get; set; }

        /// <summary>
        /// When using “show previous data”, the data for a POINT shows up when 
        /// the POINT opens, when the operators hits OK to save, we want to make 
        /// sure it’s ok to save the data as current.  If plants use the “show 
        /// previous data” functionality, they have to answer “yes” to EVERY POINT 
        /// they collect on, which is very annoying.
        /// </summary>
        public Boolean SaveDataAsCurrent { get; set; }

        /// <summary>
        /// Get or set: Alert user that point already collected in other route
        /// </summary>
        public Boolean FoundInOtherRoute { get; set; }

        /// <summary>
        /// Get or Set: Allow operator to see sets within hierarchy
        /// </summary>
        public Boolean ShowSetsWhileCollecting { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th February 2009 (10:45)
//  Add to project
//----------------------------------------------------------------------------
