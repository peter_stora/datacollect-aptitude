﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single PROCESS table record entry
   /// </summary>
   public class ProcessRecord : ProcessRecordBase
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public ProcessRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Gets or sets the current UID (GUID) for this node. 
      /// Note: This value is derived and therefore independant of @ptitude
      /// </summary>
      public Guid Uid
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the hierarchy position of the current node. 
      /// Note: This value is derived and therefore independant of @ptitude
      /// </summary>
      public int Number
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the associated Point Uid (from Marlin POINT table)
      /// </summary>
      public Guid PointUid
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the state of the point based on entered value
      /// </summary>
      public byte AlarmState
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Baseline Data time (unused)
      /// </summary>
      public DateTime BaselineDataTime
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Baseline Data value (unused)
      /// </summary>
      public double BaselineDataValue
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------