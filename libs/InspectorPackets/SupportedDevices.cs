﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simple list of minumum supported firmware versions
   /// </summary>
   [XmlRoot( ElementName = "Devices" )] 
   public class SupportedDevices: PacketBase
   {
      protected List<DeviceType> mDevices;

      /// <summary>
      /// Constructor
      /// </summary>
      public SupportedDevices()
      {
         mDevices = new List<DeviceType>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the Supported Devices object list.
      /// </summary>
      [XmlElement( "Supported" )]
      public List<DeviceType> Supported
      {
         get
         {
            return mDevices;
         }
         set
         {
            mDevices = value;
         }
      }


      /// <summary>
      /// Add a new Supported Device record to the list
      /// </summary>
      /// <param name="item">populated Supported Device record object</param>
      /// <returns>number of Supported Device objects available</returns>
      public int AddSupportedDevice( DeviceType item )
      {
         mDevices.Add( item );
         return mDevices.Count;
      }


      /// <summary>
      /// How many Supported Device records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mDevices.Count;
         }
      }

   }/* end class */



   /// <summary>
   /// Class to define the minimum supported Hardware version for
   /// any particular Microlog Device
   /// </summary>
   public class DeviceType: PacketBase
   {
      HwVersion mMinimumVersion;

      /// <summary>
      /// Base Constructor - set the default version number
      /// </summary>
      public DeviceType()
      {
         this.Device = (byte) FieldTypes.Microlog.Inspector;
         mMinimumVersion = new HwVersion( 
            VERSION_MAJOR, 
            VERSION_MINOR, 
            VERSION_BUILD,
            VERSION_REVISION );
      }

      /// <summary>
      /// Gets or sets the Devive ID
      /// </summary>
      [XmlElement( "Device" )]
      public byte Device
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the minimum firware version
      /// </summary>
      [XmlElement( "MinVersion" )]
      public HwVersion MinVersion
      {
         get
         {
            return mMinimumVersion;
         }
         set
         {
            mMinimumVersion.Major = value.Major;
            mMinimumVersion.Minor = value.Minor;
            mMinimumVersion.Build = value.Build;
            mMinimumVersion.Revision = value.Revision;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th March 2010
//  Add to project
//----------------------------------------------------------------------------