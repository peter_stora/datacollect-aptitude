﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2008 - 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class validates all @ptitude alarm levels. Illegal alarm states 
   /// or levels will be corrected to ensure that impossible states will not 
   /// be processed (i.e. Having an alarm level that exceeds a danger level 
   /// in an "out of window" condition)
   /// </summary>
   public class ValidateAlarmLevels
   {
      # region Private fields

      // alarm fields (in radians)- override with properties
      private Double mLowAlert = 0.0;
      private Double mHighAlert = 0.0;
      private Double mLowDanger = 0.0;
      private Double mHighDanger = 0.0;
      private Double mRangeMax = 0.0;
      private Double mRangeMin = 0.0;

      // alarm enable fields
      private Boolean mUseLowAlert = false;
      private Boolean mUseHighAlert = false;
      private Boolean mUseLowDanger = false;
      private Boolean mUseHighDanger = false;

      // Alarm type field - default as 'Off'
      private FieldTypes.AlarmType mAlarmType = FieldTypes.AlarmType.Off;

      #endregion


      /// <summary>
      /// Base Constructor
      /// </summary>
      /// <param name="iProcess">populated process object</param>
      public ValidateAlarmLevels( ):
         this(null)
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iProcess">populated process object</param>
      public ValidateAlarmLevels( ProcessRecord iProcess )
      {
         if ( iProcess != null )
         {
            SetAlarmStates( iProcess );
         }
      }/* end constructor */


      #region Public Properties

      /// <summary>
      /// Gets the current window alarm type
      /// </summary>
      public FieldTypes.AlarmType AlarmType
      {
         get
         {
            return mAlarmType;
         }
      }

      /// <summary>
      /// Gets the Low Alert enable status
      /// </summary>
      public Boolean UseLowAlert
      {
         get
         {
            return mUseLowAlert;
         }
      }

      /// <summary>
      /// Gets the High Alert enable status
      /// </summary>
      public Boolean UseHighAlert
      {
         get
         {
            return mUseHighAlert;
         }
      }

      /// <summary>
      /// Gets the Low Danger enable status
      /// </summary>
      public Boolean UseLowDanger
      {
         get
         {
            return mUseLowDanger;
         }
      }

      /// <summary>
      /// Gets the High Danger enable status
      /// </summary>
      public Boolean UseHighDanger
      {
         get
         {
            return mUseHighDanger;
         }
      }

      /// <summary>
      /// Gets the Low Alert Value
      /// </summary>
      public Double LowAlert
      {
         get
         {
            return mLowAlert;
         }
      }

      /// <summary>
      /// Gets the High Alert Value
      /// </summary>
      public Double HighAlert
      {
         get
         {
            return mHighAlert;
         }
      }

      /// <summary>
      /// Gets the Low Alarm Value
      /// </summary>
      public Double LowDanger
      {
         get
         {
            return mLowDanger;
         }
      }

      /// <summary>
      /// Gets the High Alarm Value
      /// </summary>
      public Double HighDanger
      {
         get
         {
            return mHighDanger;
         }
      }

      /// <summary>
      /// Gets the full range as a real number
      /// </summary>
      public Double FullRange
      {
         get
         {
            return mRangeMax - mRangeMin;
         }
      }

      #endregion


      #region Public Methods

      /// <summary>
      /// Use the ProcessRecord.AlarmState field to set each of the 
      /// individual alarm state flags as well as reference each alarm
      /// level as a percentage of the overall range
      /// </summary>
      /// <param name="iProcess">Populated process record</param>
      public void SetAlarmStates( ProcessRecord iProcess )
      {
         // reset the alarm states
         mUseHighDanger = false;
         mUseLowDanger = false;
         mUseLowAlert = false;
         mUseHighAlert = false;

         // get the actual alarm type
         mAlarmType = (FieldTypes.AlarmType)iProcess.AlarmType;
         
         // make sure we're only looking at Window alarms
         if ( AlarmType != FieldTypes.AlarmType.Off )
         {
            // is the Danger High bit set? If yes then enable Danger High property
            if( ( iProcess.AlarmState & Alarms.ALARM_DANGERHI ) == Alarms.ALARM_DANGERHI )
            {
               mUseHighDanger = true;
            }

            // is the Danger Low bit set? If yes then enable Danger Low property
            if( ( iProcess.AlarmState & Alarms.ALARM_DANGERLO ) == Alarms.ALARM_DANGERLO )
            {
               mUseLowDanger = true;
            }

            // is the Alarm High bit set? If yes then enable Alarm High property
            if( ( iProcess.AlarmState & Alarms.ALARM_ALERTHI ) == Alarms.ALARM_ALERTHI )
            {
               mUseHighAlert = true;
            }

            // is the Alarm Low bit set? If yes then enable Alarm Low property
            if( ( iProcess.AlarmState & Alarms.ALARM_ALERTLO ) == Alarms.ALARM_ALERTLO )
            {
               mUseLowAlert = true;
            }

            // reference the range values
            mRangeMax = iProcess.RangeMax;
            mRangeMin = iProcess.RangeMin;

            // get the total range
            var range = mRangeMax - mRangeMin;

            // lets not have any divisions by zero
            if ( range != 0 )
            {
               // express High Danger as a percentage of the range
               mHighDanger = SetValidationLimits(
                  100 * ( ( iProcess.UpperDanger - mRangeMin ) / range ) );

               // express Upper Alert as a percentage of the range
               mHighAlert = SetValidationLimits(
                  100 * ( ( iProcess.UpperAlert - mRangeMin ) / range ) );

               // express Low Alert as a percentage of the range
               mLowAlert = SetValidationLimits(
                  100 * ( ( iProcess.LowerAlert - mRangeMin ) / range ) );

               // express Low Danger as a percentage of the range
               mLowDanger = SetValidationLimits(
                  100 * ( ( iProcess.LowerDanger - mRangeMin ) / range ) );
            }

            // now just double-check that everything is kosher
            ValidateAlarmStates();
         }

      }/* end method */


      /// <summary>
      /// Run a final check on all alarm states to ensure that there aren't
      /// any alarm level errors that break the set alarm conditions
      /// </summary>
      private void ValidateAlarmStates()
      {
         // validate all 'Out of Window' alarm levels
         if ( mAlarmType == FieldTypes.AlarmType.OutOfWindow )
         {
            ValidateAllOutOfWindowAlarms();
         }

         // validate all 'In Window' alarm levels
         else if ( mAlarmType == FieldTypes.AlarmType.InWindow )
         {
            ValidateAllInWindowAlarms();
         }

         // validate all 'Level only' alarm levels 
         else if ( mAlarmType == FieldTypes.AlarmType.Level )
         {
            ValidateAllLevelAlarms();
         }

         // failing that - disable everything!
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Express a measurement as a percentage of the full process
      /// ranges (range max - range min)
      /// </summary>
      /// <param name="iMeasurement">normal measurement value</param>
      /// <returns>value as percentage</returns>
      public double ValueAsPercentage( double iMeasurement )
      {
         // get the total range
         double range = mRangeMax - mRangeMin;
         double valuePercent = 0.0;

         // lets not have any divisions by zero
         if ( range != 0 )
         {
            double descale = 100.0 / range;
            valuePercent = descale * (iMeasurement - mRangeMin);
         }

         // return the value as a percentage
         return valuePercent;

      }/* end method */

      #endregion


      #region Validation Methods for 'Out Of Window' Alarms

      /// <summary>
      /// This method validates that each of the set alert and alarm values will
      /// allow the bands to be enabled as per the fifteen distinct "out of window"
      /// potential validation states. These are: 
      ///     High Alarm, Low Alert
      ///     Low Alert, High Alarm
      ///     High Alarm, Low Alarm
      ///     High Alert, Low Alert
      ///     Low Alert only
      ///     Low Alarm only
      ///     Low Alert, Low Alarm
      ///     High Alert, Low Alert, Low Alarm
      ///     High Alarm, Low Alert, Low Alarm
      ///     High Alarm, High Alert, Low Alert, Low Alarm
      ///     High Alarm, High Alert, Low Alarm
      ///     High Alarm, High Alert, Low Alert
      ///     High Alarm, High Alert
      ///     High Alarm only
      ///     High Alert only
      /// where each value must be in the correct ordinal position. In the event 
      /// of a non-ordinal value either or both of the two alerts or alarms will be 
      /// sacrificed. 
      /// 
      /// Each condition will be queried as a binary condition using the order
      /// High Alarm, Low Alarm, High Alert, Low Alert - with each comment shown
      /// as a four digit binary. i.e. 0010 = High Alert only. 
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void ValidateAllOutOfWindowAlarms()
      {
         // for condition [1111]
         if ( mUseHighDanger & mUseLowDanger & mUseHighAlert & mUseLowAlert )
         {
            SetAllOutOfWindowAlarmAndAlertBands();
         }

         // for condition [1110]
         else if ( mUseHighDanger & mUseLowDanger & mUseHighAlert & !mUseLowAlert )
         {
            SetOutOfWindowUpperAlertAndAlarms();
         }

         // for condition [1101]
         else if ( mUseHighDanger & mUseLowDanger & !mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowLowerAlertAndAlarms();
         }

         // for condition [1100]
         else if ( mUseHighDanger & mUseLowDanger & !mUseHighAlert & !mUseLowAlert )
         {
            SetOutOfWindowAlarms();
         }

         // for condition [1011]
         else if ( mUseHighDanger & !mUseLowDanger & mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowUpperAlarmAndAlerts();
         }

         // for condition [1010]
         else if ( mUseHighDanger & !mUseLowDanger & mUseHighAlert & !mUseLowAlert )
         {
            SetOutOfWindowUpperAlarmAndUpperAlert();
         }

         // for condition [1001]
         else if ( mUseHighDanger & !mUseLowDanger & !mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowUpperAlarmAndLowerAlert();
         }

         // for condition [1000]
         else if ( mUseHighDanger & !mUseLowDanger & !mUseHighAlert & !mUseLowAlert )
         {
            SetAlertStatus( true, false, false, false );
         }

         // for condition [0111]
         else if ( !mUseHighDanger & mUseLowDanger & mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowLowerAlarmAndAlerts();
         }

         // for condition [0110]
         else if ( !mUseHighDanger & mUseLowDanger & mUseHighAlert & !mUseLowAlert )
         {
            SetOutOfWindowLowerAlarmAndUpperAlert();
         }

         // for condition [0101]
         else if ( !mUseHighDanger & mUseLowDanger & !mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowLowerAlarmAndLowerAlert();
         }

         // for condition [0100]
         else if ( !mUseHighDanger & mUseLowDanger & !mUseHighAlert & !mUseLowAlert )
         {
            SetAlertStatus( false, true, false, false );
         }

         // for condition [0011]
         else if ( !mUseHighDanger & !mUseLowDanger & mUseHighAlert & mUseLowAlert )
         {
            SetOutOfWindowAlerts();
         }

         // for condition [0010]
         else if ( !mUseHighDanger & !mUseLowDanger & mUseHighAlert & !mUseLowAlert )
         {
            SetAlertStatus( false, false, true, false );
         }

         // for condition [0001]
         else if ( !mUseHighDanger & !mUseLowDanger & !mUseHighAlert & mUseLowAlert )
         {
            SetAlertStatus( false, false, false, true );
         }

         // for condition [0000]
         else if ( !mUseHighDanger & !mUseLowDanger & !mUseHighAlert & !mUseLowAlert )
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate all alarm and alert states
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetAllOutOfWindowAlarmAndAlertBands()
      {
         // if all alarm and alert levels valid
         if ( ( HighDanger > HighAlert ) &
             ( HighAlert > LowAlert ) &
             ( LowAlert > LowDanger ) &
             ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, true, true );
         }

         // if low alert <= low alarm, then disable low alert
         else if ( ( HighDanger > HighAlert ) &
                  ( HighAlert > LowAlert ) &
                  !( LowAlert > LowDanger ) &
                  ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, true, false );
         }

         //
         // if low alert >= high alert, then disable both
         //
         else if ( ( HighDanger > HighAlert ) &
                  !( HighAlert > LowAlert ) &
                  ( LowAlert > LowDanger ) &
                  ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         //
         // if low alert >= high alert, and
         // low alert <= low alarm then disable both
         // low and high alerts
         //
         else if ( ( HighDanger > HighAlert ) &
                  !( HighAlert > LowAlert ) &
                  !( LowAlert > LowDanger ) &
                  ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         // if high alert >= high alarm, then disable high alert
         else if ( !( HighDanger > HighAlert ) &
                  ( HighAlert > LowAlert ) &
                  ( LowAlert > LowDanger ) &
                  ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, true );
         }

         //
         // if high alert >= high alarm and 
         // low alert <= low alarm, then disable both
         // low and high alerts
         //
         else if ( !( HighDanger > HighAlert ) &
                 ( HighAlert > LowAlert ) &
                 !( LowAlert > LowDanger ) &
                 ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         //
         // if high alert >= high alarm and 
         // high alert <= low alert, then disable both
         // low and high alerts
         //
         else if ( !( HighDanger > HighAlert ) &
                  !( HighAlert > LowAlert ) &
                  ( LowAlert > LowDanger ) &
                  ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         //
         // if the low danger >= high danger then disable everything!
         //
         else if ( !( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, false, false, false );
         }

         // for all other conditions, disable everything
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate all alarms and upper alert
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowUpperAlertAndAlarms()
      {
         if ( ( HighDanger > HighAlert ) & ( HighAlert > LowDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, true, false );
         }

         else if ( !( HighDanger > HighAlert ) & ( HighAlert > LowDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         else if ( ( HighDanger > HighAlert ) & !( HighAlert > LowDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         else if ( !( HighDanger > LowDanger ) )
         {
            SetAlertStatus( false, false, false, false );
         }

         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate all alarms and lower alert
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowLowerAlertAndAlarms()
      {
         if ( ( HighDanger > LowAlert ) & ( LowAlert > LowDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, true );
         }

         else if ( ( HighDanger > LowAlert ) & !( LowAlert > LowDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         else if ( !( HighDanger > LowAlert ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         else if ( !( HighDanger > LowDanger ) )
         {
            SetAlertStatus( false, false, false, false );
         }

         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate upper alarm and all alerts
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowUpperAlarmAndAlerts()
      {
         if ( ( HighDanger > HighAlert ) & ( HighAlert > LowAlert ) & ( HighDanger > LowAlert ) )
         {
            SetAlertStatus( true, false, true, true );
         }

         else if ( ( HighDanger > HighAlert ) & !( HighAlert > LowAlert ) & ( HighDanger > LowAlert ) )
         {
            SetAlertStatus( true, false, false, false );
         }

         else
         {
            SetAlertStatus( true, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate upper alarm and upper alert
      /// </summary>
      private void SetOutOfWindowUpperAlarmAndUpperAlert()
      {
         if ( HighDanger > HighAlert )
         {
            SetAlertStatus( true, false, true, false );
         }
         else
         {
            SetAlertStatus( true, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate upper alarm and lower alert
      /// </summary>
      private void SetOutOfWindowUpperAlarmAndLowerAlert()
      {
         if ( HighDanger > LowAlert )
         {
            SetAlertStatus( true, false, false, true );
         }
         else
         {
            SetAlertStatus( true, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate lower alarm and upper alert
      /// </summary>
      private void SetOutOfWindowLowerAlarmAndUpperAlert()
      {
         if ( HighAlert > LowDanger )
         {
            SetAlertStatus( false, true, true, false );
         }
         else
         {
            SetAlertStatus( false, true, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate lower alarm and lower alert
      /// </summary>
      private void SetOutOfWindowLowerAlarmAndLowerAlert()
      {
         if ( LowAlert > LowDanger )
         {
            SetAlertStatus( false, true, false, true );
         }
         else
         {
            SetAlertStatus( false, true, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate only alerts
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowAlerts()
      {
         if ( HighAlert > LowAlert )
         {
            SetAlertStatus( false, false, true, true );
         }
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Validate only alarms
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowAlarms()
      {
         if ( HighDanger > LowDanger )
         {
            SetAlertStatus( true, true, false, false );
         }
      }/* end method */


      /// <summary>
      /// Validate lower alarm and lower alert
      /// </summary>
      /// <param name="iSentAs">currently active property</param>
      private void SetOutOfWindowLowerAlarmAndAlerts()
      {
         if ( ( HighAlert > LowAlert ) & ( HighAlert > LowDanger ) & ( LowAlert > LowDanger ) )
         {
            SetAlertStatus( false, true, true, true );
         }

         else if ( ( HighDanger > HighAlert ) & !( HighAlert > LowAlert ) & ( HighDanger > LowAlert ) )
         {
            SetAlertStatus( false, true, false, false );
         }

         else
         {
            SetAlertStatus( false, true, false, false );
         }

      }/*end method */

      #endregion


      #region Validation Methods for 'In Window' Alarms

      /// <summary>
      /// This method validates that each of the set alert and alarm values
      /// will allow the bands to be enabled as per the four distinct "in window"
      /// states: 
      ///     High Alert, High Danger, Low Danger, Low Alert
      ///     High Alert, High Danger, Low Danger
      ///     High Alert, High Danger, Low Alert
      ///     High Alert, High Danger
      /// where each value must be ordinarily correct. In the event of a 
      /// non-ordinal value either or both of the two alerts will be 
      /// sacrificed. In the event of any alarm being invalid - all alerts
      /// and alarms will be diabled.
      /// </summary>
      private void ValidateAllInWindowAlarms()
      {
         // check for "use all"
         if ( mUseHighAlert & mUseHighDanger & mUseLowDanger & mUseLowAlert )
         {
            SetAllInWindowAlarmAndAlertBands();
         }

         // check for "skip low alert"
         else if ( mUseHighDanger & mUseLowDanger & mUseHighAlert )
         {
            SetInWindowUpperAlertAndAlarms();
         }

         // check for "skip high alert"
         else if ( mUseHighDanger & mUseLowDanger & mUseLowAlert )
         {
            SetLowerAlertAndAlarms();
         }

         // check for use alarms only
         else if ( mUseHighDanger & mUseLowDanger )
         {
            SetAlarmsOnly();
         }

         // .. and if its not one of those, then diable everything anyway
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// This method validates the three 'Level' Alarm state, namely:
      ///     Alert
      ///     Danger
      ///     Alert, Danger
      /// In the event of an Alert value exceeding a Danger, the Alert
      /// condition will be disabled
      /// </summary>
      private void ValidateAllLevelAlarms()
      {
         if ( mUseHighDanger & mUseHighAlert )
         {
            SetLevelAlarmsOnly();
         }
         else if ( mUseHighDanger )
         {
            SetAlertStatus( true, mUseLowDanger, mUseHighAlert, mUseLowAlert );
         }
         else if ( mUseHighAlert )
         {
            SetAlertStatus( mUseHighDanger, mUseLowDanger, true, mUseLowAlert );
         }
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Set the alarm status fields for when all alarm and alert 
      /// fields have been assigned a value
      /// </summary>
      private void SetAllInWindowAlarmAndAlertBands()
      {
         if ( ( HighAlert > HighDanger ) &
             ( HighDanger > LowDanger ) &
             ( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, true, true );
         }

         else if ( ( HighAlert > HighDanger ) &
                  ( HighDanger > LowDanger ) &
                 !( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, true, false );
         }

         else if ( !( HighAlert > HighDanger ) &
                  ( HighDanger > LowDanger ) &
                  ( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, false, true );
         }

         else if ( !( HighAlert > HighDanger ) &
                  ( HighDanger > LowDanger ) &
                  !( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, false, false );
         }

         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Set the alarm status fields for when all alarm and upper
      /// alert fields have been assigned a value
      /// </summary>
      private void SetInWindowUpperAlertAndAlarms()
      {
         if ( ( HighAlert > HighDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, true, mUseLowAlert );
         }

         else if ( !( HighAlert > HighDanger ) & ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, false, mUseLowAlert );
         }

         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Set the alarm status fields for when all alarm and lower
      /// alert fields have been assigned a value
      /// </summary>
      private void SetLowerAlertAndAlarms()
      {
         if ( ( HighDanger > LowDanger ) & ( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, mUseHighAlert, true );
         }

         else if ( ( HighDanger > LowDanger ) & !( LowDanger > LowAlert ) )
         {
            SetAlertStatus( true, true, mUseHighAlert, false );
         }

         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Set the alarm status fields for when only the alarm fields 
      /// have been assigned a value
      /// </summary>
      private void SetAlarmsOnly()
      {
         if ( ( HighDanger > LowDanger ) )
         {
            SetAlertStatus( true, true, mUseHighAlert, mUseLowAlert );
         }
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      /// <summary>
      /// Set the alarm status fields for when only the alarm fields 
      /// have been assigned a value
      /// </summary>
      private void SetLevelAlarmsOnly()
      {
         if ( ( HighDanger > HighAlert ) )
         {
            SetAlertStatus( true, mUseLowDanger, true, mUseLowAlert );
         }
         else
         {
            SetAlertStatus( false, false, false, false );
         }

      }/* end method */


      #endregion


      /// <summary>
      /// Limit the target validation level between 0 and 100, any value
      /// exceeding this limits will reset to zero
      /// </summary>
      /// <param name="iValue">target level</param>
      /// <returns>validated level</returns>
      private Double SetValidationLimits( Double iValue )
      {
         Double result = 0.0;

         if ( ( iValue > 100.00 ) | ( iValue < 0.0 ) )
         {
            result = 0.0;
         }
         else
         {
            result = iValue;
         }

         return result;

      }/* end method */


      /// <summary>
      /// override the current alert or alarm "in use" flag
      /// </summary>
      /// <param name="iHighAlarm">override the current High Alarm flag</param>
      /// <param name="iLowAlarm">override the current Low Alarm flag</param>
      /// <param name="iHighAlert">override the current High Alert flag</param>
      /// <param name="iLowAlert">override the current Low Alert flag</param>
      private void SetAlertStatus( Boolean iHighDanger, Boolean iLowDanger,
                                  Boolean iHighAlert, Boolean iLowAlert )
      {
         mUseHighDanger = iHighDanger;
         mUseLowDanger = iLowDanger;
         mUseHighAlert = iHighAlert;
         mUseLowAlert = iLowAlert;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 13th November 2009
//  Add to project
//----------------------------------------------------------------------------
