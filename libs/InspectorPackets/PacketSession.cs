﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Packet SESSION: Issued by the Middleware Server on valid authentication
    /// and available license for the connected device
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketSession : PacketBase
    {
        /// <summary>
        /// Packet Type
        /// </summary>
        public byte Packet = (byte)PacketType.SESSION;

        /// <summary>
        /// Session Object
        /// </summary>
        public SessionRecord Session;

        /// <summary>
        /// Base Constructor
        /// </summary>
        public PacketSession()
            : this(DEFAULT_SESSION_ID, DEFAULT_SESSION_TIMEOUT)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iSessionID">Current Session ID</param>
        public PacketSession(String iSessionID)
            : this(iSessionID, DEFAULT_SESSION_TIMEOUT)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iSessionID">Current Session ID</param>
        /// <param name="iSessionLife">Session Life in Seconds</param>
        public PacketSession(String iSessionID, Double iSessionLife)
        {
            Session = new SessionRecord(iSessionID, iSessionLife);

        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------
