//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright � 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage basic encryption of Microlog Inspector Web Services
    /// Password data. This class uses Rijndael (or AES) secure methods  
    /// </summary>
    public class XmlEncryption
    {
        #region Private Fields

        private Rijndael RijndaelAlg;
        private byte[] mKey;
        private byte[] mIV;
        private byte[] mSecurePW;

        #endregion

        /// <summary>
        /// Class Constructor
        /// </summary>
        public XmlEncryption()
        {
            RijndaelAlg = Rijndael.Create();
            mKey = RijndaelAlg.Key;
            mIV = RijndaelAlg.IV;
            MakeSecureKey();
        }/* end constructor */


        /// <summary>
        /// Public Property: Get or set the Secure Password
        /// </summary>
        public string SecurePassword
        {
            get { return ByteArrayProcess.ByteArrayToHexString(mSecurePW); }
            set
            {
                mSecurePW = ByteArrayProcess.HexStringToByteArray(value);
                ExtractSecureKeysFromPW();
            }
        }/* end property */


        /// <summary>
        /// This simple method extracts both the secure key and 
        /// the initialization variable from the secure password
        /// </summary>
        private void ExtractSecureKeysFromPW()
        {
            int KeyLength = (int)mSecurePW[0];
            ByteArrayProcess.ExtractArray(mSecurePW, ref mKey, 1, KeyLength);
            ByteArrayProcess.ExtractArray(mSecurePW, ref mIV, KeyLength + 1, (mSecurePW.Length - (KeyLength + 1)));
        }/* end method */


        /// <summary>
        /// This method concatenates the both the secure key and the
        /// initialization variable to produce the secure password
        /// </summary>
        private void MakeSecureKey()
        {
            mSecurePW = new byte[1];
            mSecurePW[0] = (byte)mKey.Length;
            ByteArrayProcess.AppendToByteBuffer(mKey, ref mSecurePW);
            ByteArrayProcess.AppendToByteBuffer(mIV, ref mSecurePW);
        }/* end method */


        /// <summary>
        /// Encrypt the source string using AES
        /// </summary>
        /// <param name="iSourceString">String to be encrypted</param>
        /// <returns>encrypted string64</returns>
        public String EncryptData(string iSourceString)
        {
            try
            {
                byte[] encBytes = EncryptData(Encoding.UTF8.GetBytes(iSourceString), mKey, mIV);
                return Convert.ToBase64String(encBytes);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }/* end method */


        /// <summary>
        /// Decrypt an encrypted String64
        /// </summary>
        /// <param name="iEncryptedString">String to be decrypted</param>
        /// <returns>Original source string64</returns>
        public String DecryptData(string iEncryptedString)
        {
            try
            {
                byte[] encBytes = Convert.FromBase64String(iEncryptedString);
                byte[] decBytes = DecryptData(encBytes, mKey, mIV);
                return Encoding.UTF8.GetString(decBytes, 0, decBytes.Length);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }/* end method */


        /// <summary>
        /// Encrypt the source string to Hex string using AES
        /// </summary>
        /// <param name="iSourceString">String to be encrypted</param>
        /// <returns>encrypted Hex string</returns>
        public String EncryptStringToHex(string iSourceString)
        {
            try
            {
                byte[] encBytes = EncryptData(Encoding.UTF8.GetBytes(iSourceString), mKey, mIV);
                return ByteArrayProcess.ByteArrayToHexString(encBytes);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }/* end method */


        /// <summary>
        /// Decrypt an encrypted Hex String
        /// </summary>
        /// <param name="iEncryptedString">String to be decrypted</param>
        /// <returns>Original source string64</returns>
        public String DecryptHexToString(string iHexString)
        {
            try
            {
                byte[] encBytes = ByteArrayProcess.HexStringToByteArray(iHexString);
                byte[] decBytes = DecryptData(encBytes, mKey, mIV);
                return Encoding.UTF8.GetString(decBytes, 0, decBytes.Length);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }/* end method */


        /// <summary>
        /// Encrypt one byte array into another byte array using Rijndael AES
        /// </summary>
        /// <param name="iByteData">Un-encrypted data</param>
        /// <param name="iKey">Secure Key</param>
        /// <param name="iIV">Initialized vector</param>
        /// <returns></returns>
        private byte[] EncryptData(byte[] iByteData, byte[] iKey, byte[] iIV)
        {
            try
            {

                Rijndael RijndaelAlg = Rijndael.Create();

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream encStream = new CryptoStream(msEncrypt, RijndaelAlg.CreateEncryptor(iKey, iIV), CryptoStreamMode.Write))
                    {
                        encStream.Write(iByteData, 0, iByteData.Length);
                        encStream.FlushFinalBlock();
                        return msEncrypt.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                return Encoding.UTF8.GetBytes("ERROR: " + ex.Message);
            }

        }/* end method */


        /// <summary>
        /// Decrypt one byte array into another byte array using Rijndael AES
        /// </summary>
        /// <param name="iByteData">Encrypted data</param>
        /// <param name="iKey">Secure Key</param>
        /// <param name="iIV">Initialized vector</param>
        /// <returns></returns>
        private byte[] DecryptData(byte[] iByteData, byte[] iKey, byte[] iIV)
        {
            try
            {
                Rijndael RijndaelAlg = Rijndael.Create();
                using (MemoryStream msDecrypt = new MemoryStream(iByteData))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, RijndaelAlg.CreateDecryptor(iKey, iIV), CryptoStreamMode.Read))
                    {
                        //
                        // Decrypted bytes will always be less than encrypted bytes, so
                        // length of the encrypted data will be big enouph for buffer.
                        //
                        byte[] fromEncrypt = new byte[iByteData.Length];

                        // Read as many bytes as possible.
                        int read = csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                        if (read < fromEncrypt.Length)
                        {
                            // Return a byte array of proper size.
                            byte[] clearBytes = new byte[read];
                            Buffer.BlockCopy(fromEncrypt, 0, clearBytes, 0, read);
                            return clearBytes;
                        }
                        return fromEncrypt;
                    }
                }
            }
            catch (Exception ex)
            {
                return Encoding.UTF8.GetBytes("ERROR: " + ex.Message);
            }

        }/* end method */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 3rd February 2009
//  Add to project
//----------------------------------------------------------------------------


