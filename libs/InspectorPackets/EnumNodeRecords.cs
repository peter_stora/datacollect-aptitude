﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Node Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumNodeRecords : PacketBase
    {
        protected List<NodeRecord> mNodeRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumNodeRecords()
        {
            mNodeRecord = new List<NodeRecord>();
        }/* end constructor */

        /// <summary>
        /// Get or set elements in the raw NodeRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("NODE")]
        public List<NodeRecord> NODE
        {
            get { return mNodeRecord; }
            set { mNodeRecord = value; }
        }

        /// <summary>
        /// Add a new Node record to the list
        /// </summary>
        /// <param name="item">populated Node record object</param>
        /// <returns>number of Node objects available</returns>
        public int AddNodeRecord(NodeRecord item)
        {
            mNodeRecord.Add(item);
            return mNodeRecord.Count;
        }

        /// <summary>
        /// How many Node records are now available
        /// </summary>
        public int Count { get { return mNodeRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumNodeRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumNodes : EnumNodeRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mNodeRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mNodeRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------