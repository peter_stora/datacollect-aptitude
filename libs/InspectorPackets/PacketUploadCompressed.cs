﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Serializable Compressed Upload Packet
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketUploadCompressed : PacketBase
   {
      /// <summary>
      /// The actual Packet type (OPERATOR SET)
      /// </summary>
      public byte Packet = (byte) PacketType.UPLOAD_COMPRESSED;
      public byte HostingPacket
      {
         get;
         set;
      }

      /// <summary>
      /// Operator Settings Element
      /// </summary>
      [XmlElement( "ByteData" )]
      public byte[] ByteData
      {
         get;
         set;
      }

      /// <summary>
      /// Session Identifier
      /// </summary>
      [XmlElement( "SessionID" )]
      public string SessionID
      {
         get;
         set;
      }

      /// <summary>
      /// Does this upload have media files to be processed
      /// </summary>
      [XmlElement( "HasMedia" )]
      public Boolean HasMedia
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      public PacketUploadCompressed()
      {
      }/* end constructor */


      /// <summary>
      /// Compress the serialized byte array 
      /// </summary>
      /// <param name="buffer">raw byte array</param>
      /// <returns>decompressed byte array</returns>
      public byte[] Compress( byte[] buffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Compress, true ) )
            {
               zip.Write( buffer, 0, buffer.Length );
            }

            ms.Position = 0;

            byte[] compressed = new byte[ ms.Length ];
            ms.Read( compressed, 0, compressed.Length );

            byte[] gzBuffer = new byte[ compressed.Length + 4 ];

            System.Buffer.BlockCopy( compressed, 0, gzBuffer, 4, compressed.Length );
            System.Buffer.BlockCopy( BitConverter.GetBytes( buffer.Length ), 0, gzBuffer, 0, 4 );

            return gzBuffer;
         }

      }/* end method */


      /// <summary>
      /// Decompress the serialized byte array
      /// </summary>
      /// <param name="gzBuffer">compressed byte array</param>
      /// <returns>decompressed byte array</returns>
      public byte[] Decompress( byte[] gzBuffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            int msgLength = BitConverter.ToInt32( gzBuffer, 0 );
            ms.Write( gzBuffer, 4, gzBuffer.Length - 4 );

            byte[] buffer = new byte[ msgLength ];

            ms.Position = 0;
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Decompress ) )
            {
               zip.Read( buffer, 0, buffer.Length );
            }

            return buffer;
         }
      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------