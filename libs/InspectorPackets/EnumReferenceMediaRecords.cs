﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Enumeration of Reference Media object records. These are the 
    /// image and audio files downloaded to the device to assist the 
    /// user in collecting route data. In addition to audio and image
    /// files, this format will also support both WORD, EXCEL and PDF
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumReferenceMediaRecords
    {
        protected List<ReferenceMediaRecord> mMediaRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumReferenceMediaRecords()
        {
            mMediaRecord = new List<ReferenceMediaRecord>();
        }/* end constructor */

        /// <summary>
        /// Get or set elements in the raw Media Record object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("ReferenceMedia")]
        public List<ReferenceMediaRecord> ReferenceMedia
        {
            get { return mMediaRecord; }
            set { mMediaRecord = value; }
        }

        /// <summary>
        /// Add a new Media record to the list
        /// </summary>
        /// <param name="item">populated Media record object</param>
        /// <returns>number of Media objects available</returns>
        public int AddMediaRecord(ReferenceMediaRecord item)
        {
            mMediaRecord.Add(item);
            return mMediaRecord.Count;
        }

        /// <summary>
        /// How many Media records are now available
        /// </summary>
        public int Count { get { return mMediaRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumNodeRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumReferenceMedia : EnumReferenceMediaRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mMediaRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mMediaRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th June 2009
//  Add to project
//----------------------------------------------------------------------------
