﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Server notifies client that all media has been received and processed
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketMediaReceived : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.MEDIA_RECEIVED;

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 04th October 2011
//  Add to project
//----------------------------------------------------------------------------