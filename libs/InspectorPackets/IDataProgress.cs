﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Interface that defines how Progress result data is returned to
   /// the parent object. 
   /// </summary>
   public interface IDataProgress
   {
      /// <summary>
      /// Gets or sets whether progress data is current or overall
      /// </summary>
      ProgressTarget Target
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the task type
      /// </summary>
      DisplayPosition DisplayAt
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current "raw" progress position 
      /// </summary>
      int RawValue
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the offset corrected progress position
      /// </summary>
      int CorrectedValue
      {
         get;
      }

      /// <summary>
      /// Gets or sets the total number of events to raise during this task 
      /// </summary>
      int FeedbackSteps
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the total number of items to process
      /// </summary>
      int NoOfItemsToProcess
      {
         get;
         set;
      }

      /// <summary>
      /// Raise event on change in the progress position
      /// </summary>
      event DataProgress.ProgressPosition OnUpdate;

      /// <summary>
      /// Reset all progress values to initial state
      /// </summary>
      void Reset();

      /// <summary>
      /// Jump to a specific position
      /// </summary>
      /// <param name="iValue"></param>
      void JumpToValue( int iValue );

      /// <summary>
      /// Raise a change in progress value event
      /// </summary>
      /// <param name="iRaiseEvent">Do we also wish to raise an event</param>
      void Update( Boolean iRaiseEvent );

   }/* end Interface */


   /// <summary>
   /// Enumerated list of Progress style
   /// </summary>
   public enum ProgressTarget
   {
      Overall,
      Current,
   }


   /// <summary>
   /// Enumerated list of display
   /// </summary>
   public enum DisplayPosition
   {
      FullSweep,
      FirstHalf,
      SecondHalf,
   }

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th October 2009
//  Add to project
//----------------------------------------------------------------------------