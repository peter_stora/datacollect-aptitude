﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single field to return POINTS.Skipped to the Server
    /// </summary>
    public class SkippedPointRecord
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SkippedPointRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Collection status
        /// </summary>
        [XmlElement("Skipped")]
        public byte Skipped { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
