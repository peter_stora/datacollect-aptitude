﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simple Class to manage Version Numbers 
   /// </summary>
   public class HwVersion
   {
      private const int VER_MAJOR = 1;
      private const int VER_MINOR = 0;
      private const int VER_BUILD = 0;
      private const int VER_REVISION = 0;

      public int Major
      {
         get;
         set;
      }
      public int Minor
      {
         get;
         set;
      }
      public int Build
      {
         get;
         set;
      }

      public int Revision
      {
         get;
         set;
      }

      /// <summary>
      /// Base Constructor
      /// </summary>
      public HwVersion()
         : this( VER_MAJOR, VER_MINOR, VER_BUILD, VER_REVISION )
      {
      }

      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iMajor">Major Version</param>
      /// <param name="iMinor">Minor Version</param>
      /// <param name="iBuild">Build Number</param>
      public HwVersion( int iMajor, int iMinor, int iBuild, int iRevision )
      {
         this.Major = iMajor;
         this.Minor = iMinor;
         this.Build = iBuild;
         this.Revision = iRevision;

      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iVersion"></param>
      public HwVersion( string iVersion )
      {
         try
         {
            Version sysVersion = new Version( iVersion );
            this.Major = sysVersion.Major;
            this.Minor = sysVersion.Minor;
            this.Build = sysVersion.Build;
            this.Revision = sysVersion.Revision;
         }
         catch
         {
            this.Major = 0;
            this.Minor = 0;
            this.Build = 0;
            this.Revision = 0;
         }

      }/* end method */


      /// <summary>
      /// Convert a string based version ID to a firmware version object
      /// </summary>
      /// <param name="iVersion"></param>
      public void StringToVersion( string iVersion )
      {
         try
         {
            Version sysVersion = new Version( iVersion );
            this.Major = sysVersion.Major;
            this.Minor = sysVersion.Minor;
            this.Build = sysVersion.Build;
            this.Revision = sysVersion.Revision;
         }
         catch
         {
            this.Major = 0;
            this.Minor = 0;
            this.Build = 0;
            this.Revision = 0;
         }

      }/* end method */


      /// <summary>
      /// Return the version number as a version object
      /// </summary>
      /// <returns></returns>
      public Version GetVersion()
      {
         Version ver = new Version( Major, Minor, Build, Revision );
         return ver;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15th January 2010
//  Added new overload constructor to support string based version numbers
//
//  Revision 0.0 APinkerton 8th March 2009
//  Add to project
//----------------------------------------------------------------------------