﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single Point Record object
   /// </summary>
   public class PointRecord
   {
      //-------------------------------------------------------------------------

      #region Private fields

      private EnumMeasurementRecords mVelData;
      private EnumMeasurementRecords mEnvData;
      private EnumMeasurementRecords mTempData;
      private byte mTagChanged = (byte) FieldTypes.TagChanged.Unchanged;

      #endregion

      //-------------------------------------------------------------------------

      /// <summary>
      /// Constructor
      /// </summary>
      public PointRecord()
      {
         mVelData = new EnumMeasurementRecords();
         mEnvData = new EnumMeasurementRecords();
         mTempData = new EnumMeasurementRecords();
      }/* end constructor */

      //-------------------------------------------------------------------------

      #region Packet properties

      /// <summary>
      /// Current UID (GUID) for this node. Note: This value is
      /// derived and therefore independant of @ptitude
      /// </summary>
      public Guid Uid
      {
         get;
         set;
      }

      /// <summary>
      /// Node Descriptor field (i.e. Hierarchies, NONROUTE, Routes etc) 
      /// From Analyst [Point Properties]: Name
      /// </summary>
      public string Id
      {
         get;
         set;
      }

      /// <summary>
      /// Parent Node Uid (from Marlin NODE table)
      /// </summary>
      public Guid NodeUid
      {
         get;
         set;
      }

      /// <summary>
      /// The Point friendly name (i.e. "My Route")
      /// </summary>
      public string Description
      {
         get;
         set;
      }

      /// <summary>
      /// Actual Point Type (i.e. MCD, Temperature, Pressure etc)
      /// </summary>
      public byte ProcessType
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the number of days between inspections
      /// </summary>
      public double ScheduleDays
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Calendar Compliance field
      /// Note: This property should not be directly accessed!
      /// </summary>
      public int Compliance
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Compliance start date
      /// </summary>
      public DateTime StartFrom
      {
         get;
         set;
      }

      /// <summary>
      /// The actual means by which the Node is located (i.e Barcode)
      /// </summary>
      public byte LocationMethod
      {
         get;
         set;
      }

      /// <summary>                                         
      /// The actual Location Tag value (i.e. Barcode value)
      /// </summary>                                        
      public string LocationTag
      {
         get;
         set;
      }

      /// <summary>
      /// Point Modify Time Stamp
      /// </summary>
      public DateTime LastModified
      {
         get;
         set;
      }

      /// <summary>
      /// Has a value been collected and saved for this point?
      /// </summary>
      public Boolean DataSaved
      {
         get;
         set;
      }

      /// <summary>
      /// Current overall alarm state of this point (for display purposes only)
      /// </summary>
      public byte AlarmState
      {
         get;
         set;
      }

      /// <summary>
      /// Unknown field
      /// </summary>
      public int PointIndex
      {
         get;
         set;
      }

      /// <summary>
      /// This is the actual link from the Marlin to Analyst Points 
      /// Analyst Table Link: TREEELEM 
      /// Field Reference: TREEELEMID 
      /// It is identical to the field “PRISM” in the NODE table
      /// </summary>
      public int PointHostIndex
      {
         get;
         set;
      }

      /// <summary>
      /// Hierarchy position of the current node. Note: This value
      /// is derived and therefore independant of @ptitude
      /// </summary>
      public int Number
      {
         get;
         set;
      }

      /// <summary>
      /// Flag status of this point - similar to the NODE table field “Flags”
      /// 0: UNCHANGED
      /// 1: POINT HAS NOTES
      /// 2: POINT TAG CHANGED
      /// </summary>
      public byte TagChanged
      {
         get
         {
            return mTagChanged;
         }
         set
         {
            mTagChanged = value;
         }
      }

      /// <summary>
      /// Binary data list for MCD velocity data
      /// </summary>
      public EnumMeasurementRecords VelData
      {
         get
         {
            return mVelData;
         }
         set
         {
            mVelData = value;
         }
      }

      /// <summary>
      /// Binary data list for MCD envelope data
      /// </summary>
      public EnumMeasurementRecords EnvData
      {
         get
         {
            return mEnvData;
         }
         set
         {
            mEnvData = value;
         }
      }

      /// <summary>
      /// Binary data list for MCD velocity data
      /// </summary>
      public EnumMeasurementRecords TempData
      {
         get
         {
            return mTempData;
         }
         set
         {
            mTempData = value;
         }
      }

      /// <summary>
      /// Analyst parent route ID, or 0 for hierarchy. Note: If the field
      /// ParentUid is null for a record containing a valid RouteId, then
      /// that field represents the actual name of the route
      /// </summary>
      public int RouteId
      {
         get;
         set;
      }

      /// <summary>
      /// Was the point skipped? If so why
      /// </summary>
      public byte Skipped
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the saved note count. Note: This property is purely 
      /// used for referencing saved table "NoteCount" field, and should not 
      /// be used to get the note count value!
      /// </summary>
      public int RefNoteCount
      {
         get;
         set;
      }

      #endregion

      //-------------------------------------------------------------------------

      #region Note count support * Cannot be Properties! *

      /// <summary>
      /// Increment the current note count by one
      /// </summary>
      public void IncrementNoteCount()
      {
         ++this.RefNoteCount;
      }


      /// <summary>
      /// Decrement the current note count by one
      /// </summary>
      public void DecrementNoteCount()
      {
         --this.RefNoteCount;
         if ( this.RefNoteCount < 0 )
            this.RefNoteCount = 0;
      }


      /// <summary>
      /// Set the note count to a specific value
      /// </summary>
      /// <param name="iValue"></param>
      public void SetNoteCount( int iValue )
      {
         this.RefNoteCount = iValue;
      }


      /// <summary>
      /// Get the current note count
      /// </summary>
      /// <returns></returns>
      public int GetNoteCount()
      {
         return this.RefNoteCount;
      }

      #endregion

      //-------------------------------------------------------------------------

      #region Calendar and interval based Ccmpliance support methods

      /// <summary>
      /// Set the compliance property 
      /// </summary>
      /// <param name="iCompliance">Calendar compliance type</param>
      /// <param name="iWindow">Compliance window</param>
      public void SetCalendarCompliance( FieldTypes.Calendar iCompliance, UInt16 iWindow )
      {
         this.Compliance = (byte) iCompliance * 65536 + (int) iWindow;

      }/* end method */


      /// <summary>
      /// Gets the Calendar compliance period (hours, days, weeks, months)
      /// </summary>
      /// <returns></returns>
      public FieldTypes.Calendar GetCalendarCompliance()
      {
         try
         {
            int value = this.Compliance / 65536;
            return (FieldTypes.Calendar) value;
         }
         catch
         {
            return FieldTypes.Calendar.Second;
         }

      }/* end method */


      /// <summary>
      /// Gets the Calendar compliance window
      /// </summary>
      /// <returns></returns>
      public UInt16 GetCalendarComplianceWindow()
      {
         try
         {
            int value = this.Compliance & 65535;
            return (UInt16) value;
         }
         catch
         {
            return 0x0000;
         }

      }/* end method */


      /// <summary>
      /// Resets the compliance start date to the most recent date that still
      /// upholds the set conditions.
      /// </summary>
      /// <param name="iCalendar">Calendar base (month, week etc.)</param>
      /// <param name="iWindow">Base increments</param>
      public void RefreshStartDate( FieldTypes.Calendar iCalendar, UInt16 iWindow )
      {
         // skip if we're using interval compliance
         if ( iCalendar != FieldTypes.Calendar.Second )
         {
            long s = 0;
            long w = 0;

            //
            // To calculate the most recent start date we need to consider
            // the actual calendar interval. For hours, days and weeks we
            // can just add an offset, however for months and years we need
            // to consider both monthly and annual calendar boundries, so 
            // an incremental loop is the only real option
            //
            if ( ( iCalendar == FieldTypes.Calendar.Hour ) |
            ( iCalendar == FieldTypes.Calendar.Day ) |
            ( iCalendar == FieldTypes.Calendar.Week ) )
            {
               // calculate the calendar window in ticks
               switch ( iCalendar )
               {
                  case FieldTypes.Calendar.Hour:
                     w = iWindow * TimeSpan.TicksPerHour;
                     break;
                  case FieldTypes.Calendar.Day:
                     w = iWindow * TimeSpan.TicksPerDay;
                     break;
                  case FieldTypes.Calendar.Week:
                     w = iWindow * TimeSpan.TicksPerDay * 7;
                     break;
               }

               // determine the most recent start date
               s = w * ( ( PacketBase.DateTimeNow.Ticks - this.StartFrom.Ticks ) / w );
               this.StartFrom = this.StartFrom.AddTicks( s );

            }
            else if ( ( iCalendar == FieldTypes.Calendar.Month ) |
            ( iCalendar == FieldTypes.Calendar.Year ) )
            {
               // copy the reference start date
               DateTime _StartFrom = new DateTime( this.StartFrom.Ticks );

               //
               // increment the start date until it is no longer behind the 
               // current date and time
               //
               while ( _StartFrom < PacketBase.DateTimeNow )
               {
                  switch ( iCalendar )
                  {
                     case FieldTypes.Calendar.Month:
                        _StartFrom = _StartFrom.AddMonths( iWindow );
                        break;
                     case FieldTypes.Calendar.Year:
                        _StartFrom = _StartFrom.AddYears( iWindow );
                        break;
                  }
                  //
                  // if the new start is still behind the current date and
                  // time, then update the start date field
                  //
                  if ( _StartFrom < PacketBase.DateTimeNow )
                  {
                     this.StartFrom = _StartFrom;
                  }
               }
            }
         }

      }/* end method */


      /// <summary>
      /// This method will return true if the current method is overdue 
      /// for collecton. This will work for both Calendar and Interval
      /// based compliance windows
      /// </summary>
      /// <returns>true if overdue</returns>
      public Boolean IsOverdue()
      {
         // for interval compliance
         if ( GetCalendarCompliance() == FieldTypes.Calendar.Second )
         {
            return Is_Overdue_Interval( );
         }
         // for calendar based compliance
         else
         {
            return Is_Overdue_Calendar( );
         }

      }/* end method */


      /// <summary>
      /// This method returns the actual collection status based on the 
      /// state of the Skipped (or status) flag.
      /// </summary>
      /// <returns></returns>
      public Boolean ValidCollectionStatus()
      {
         Boolean result;

         // set the method result based on the skipped type 
         switch ( (FieldTypes.SkippedType) this.Skipped )
         {
            case FieldTypes.SkippedType.AutoCollectedMachineOk:
               result = true;
               break;
            case FieldTypes.SkippedType.Collected:
               result = true;
               break;
            case FieldTypes.SkippedType.ScanDataCollected:
               result = true;
               break;
            case FieldTypes.SkippedType.CollectionFailed:
               result = false;
               break;
            case FieldTypes.SkippedType.ConditionFailed:
               result = true;
               break;
            case FieldTypes.SkippedType.Corrected:
               result = false;
               break;
            case FieldTypes.SkippedType.DpDivideByZero:
               result = true;
               break;
            case FieldTypes.SkippedType.DpInvalidFormula:
               result = true;
               break;
            case FieldTypes.SkippedType.DpReferenceDataMissing:
               result = true;
               break;
            case FieldTypes.SkippedType.DpReferencePointMissing:
               result = true;
               break;
            case FieldTypes.SkippedType.MachineNotOperating:
               result = true;
               break;
            case FieldTypes.SkippedType.MachineOk:
               result = true;
               break;
            case FieldTypes.SkippedType.None:
               result = true;
               break;
            case FieldTypes.SkippedType.NotCollected:
               result = true;
               break;
            default:
               result = false;
               break;
         }

         // and return the result
         return result;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// Gets the next 'start date due'. Note: if the start date is not reset
      /// by a point being updated, then the 'next start date due' will not be
      /// updated.
      /// </summary>
      /// <returns></returns>
      private DateTime GetNextStartDateDue()
      {
         FieldTypes.Calendar calendar = this.GetCalendarCompliance();
         UInt16 calendarWindow = this.GetCalendarComplianceWindow();
         DateTime nextDue = this.StartFrom;

         switch ( calendar )
         {
            case FieldTypes.Calendar.Hour:
               nextDue = nextDue.AddHours( calendarWindow );
               break;
            case FieldTypes.Calendar.Day:
               nextDue = nextDue.AddDays( calendarWindow );
               break;
            case FieldTypes.Calendar.Week:
               nextDue = nextDue.AddDays( calendarWindow * 7 );
               break;
            case FieldTypes.Calendar.Month:
               nextDue = nextDue.AddMonths( calendarWindow );
               break;
            case FieldTypes.Calendar.Year:
               nextDue = nextDue.AddYears( calendarWindow );
               break;
         }

         // return the new due date
         return nextDue;

      }/* end method */


      /// <summary>
      /// This method will return true if the current method is overdue 
      /// for collecton. This will work for Interval based compliance 
      /// windows only
      /// </summary>
      /// <returns>true if overdue</returns>
      private Boolean Is_Overdue_Interval()
      {
         bool result;

         //
         // if the time to the last collection does not exceed the 
         // current interval, then return the collection Status. 
         // Note: we have to invert the collection status, since it
         // will return true if the collection was OK, and what we
         // want to know is whether the point is overdue
         //
         if ( !Data_Within_Interval( ) )
         {
            // data hasn't been collected in the interval
            result = true;
         }
         else
         {
            //
            // 'data' has been collected in the interval, but it could be
            // from skipping, which may make it still overdue
            //
            result = !ValidCollectionStatus( );
         }

         return result;

      }/* end method */


      /// <summary>
      /// This method will return true if the current method is overdue 
      /// for collecton. This will work for Calendar based compliance 
      /// windows only
      /// </summary>
      /// <returns>true if overdue</returns>
      private Boolean Is_Overdue_Calendar()
      {
         bool result;

         //
         // if the current date and time is ahead of the next scheduled
         // compliance start date, then the current point has not been 
         // updated since then. Consequently, we can safely assume that 
         // it is in fact overdue.
         //
         if ( PacketBase.DateTimeNow > GetNextStartDateDue( ) )
         {
            result = true;
         }
         //
         // if however the current date and time is behind the next
         // scheduled start date, we will need to confirm the the last
         // collection date is still in compliance
         //
         else
         {
            //
            // Check if we have data in the window, checking the LastModified
            // flag if available, and if not checking for previous measuremnts
            //
            if ( Data_Within_Calendar_Window( ) )
            {
               // we have data, but is it valid?
               result = !ValidCollectionStatus( );
            }
            //
            // The POINT hasn't been modified and there's no previous data in
            // the window, this is overdue
            //
            else
            {
               result = true;
            }
         }

         return result;

      }/* end method */


      /// <summary>
      /// Determines if we have data collected or if the POINT has been
      /// modified by something like a Conditional POINT skip for an 
      /// Interval Based POINT
      /// </summary>
      /// <returns>True if there's "data" within the interval</returns>
      private Boolean Data_Within_Interval()
      {
         bool result = false;

         // if this POINT has been modified in less time than the Interval
         if( Time_Within_Interval( this.LastModified ) )
         {
            result = true;
         }
         else
         {
            //
            // although the POINT hasn't been 'modified' within the interval,
            // it might be a 'fresh' POINT from a download.  We should check
            // for previous measurements its DTS
            //

            // try temp data first
            PointMeasurement meas = mTempData.GetLatestMeasurement( );

            if ( meas != null )
            {
               if ( Time_Within_Interval( meas.LocalTime ) )
               {
                  result = true;
               }
            }
            else
            {
               // no temp data, could be MCD point without temperature reading
               meas = mVelData.GetLatestMeasurement( );

               if ( meas != null )
               {
                  if ( Time_Within_Interval( meas.LocalTime ) )
                  {
                     result = true;
                  }
               }
            }
         }

         return result;

      }/* end method */


      /// <summary>
      /// Determines if we have data collected or if the POINT has been
      /// modified by something like a Conditional POINT skip for an 
      /// Calendar Based POINT
      /// </summary>
      /// <returns>True if there's "data" within the calendar window</returns>
      private Boolean Data_Within_Calendar_Window()
      {
         bool result = false;

         if ( this.LastModified >= this.StartFrom )
         {
            result = true;
         }
         else
         {
            //
            // although the POINT hasn't been 'modified' within the interval,
            // it might be a 'fresh' POINT from a download.  We should check
            // for previous measurements its DTS
            //

            // try temp data first
            PointMeasurement meas = mTempData.GetLatestMeasurement( );

            if ( meas != null )
            {
               if ( meas.LocalTime >= this.StartFrom )
               {
                  result = true;
               }
            }
            else
            {
               // no temp data, could be MCD point without temperature reading
               meas = mVelData.GetLatestMeasurement( );

               if ( meas != null )
               {
                  if ( meas.LocalTime >= this.StartFrom )
                  {
                     result = true;
                  }
               }
            }
         }

         return result;

      }/* end method */
      

      /// <summary>
      /// Determines if the passed in time is within the Scheduled interval
      /// for an Interval compliance
      /// </summary>
      /// <param name="iDTS">The time to check</param>
      /// <returns>True if the passed in time is within the collection interval</returns>
      private Boolean Time_Within_Interval( DateTime iDTS )
      {
         bool result = false;

         if ( ( PacketBase.DateTimeNow - iDTS ) < TimeSpan.FromDays( this.ScheduleDays ) )
         {
            result = true;
         }

         return result;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      /// <summary>
      /// Replace all current fields with the fields from another point object
      /// </summary>
      /// <param name="iPointRecord">reference object</param>
      public void CloneObject( PointRecord iPointRecord )
      {
         this.AlarmState = iPointRecord.AlarmState;
         this.DataSaved = iPointRecord.DataSaved;
         this.SetNoteCount( iPointRecord.GetNoteCount() );
         this.TagChanged = iPointRecord.TagChanged;
         this.TempData = iPointRecord.TempData;
         this.EnvData = iPointRecord.EnvData;
         this.VelData = iPointRecord.VelData;
         this.Skipped = iPointRecord.Skipped;
         this.LastModified = iPointRecord.LastModified;
         this.LocationMethod = iPointRecord.LocationMethod;
         this.LocationTag = iPointRecord.LocationTag;
         this.Id = iPointRecord.Id;
         this.NodeUid = iPointRecord.NodeUid;
         this.Number = iPointRecord.Number;
         this.PointHostIndex = iPointRecord.PointHostIndex;
         this.PointIndex = iPointRecord.PointIndex;
         this.ProcessType = iPointRecord.ProcessType;
         this.RouteId = iPointRecord.RouteId;
         this.ScheduleDays = iPointRecord.ScheduleDays;
         this.Compliance = iPointRecord.Compliance;
         this.StartFrom = iPointRecord.StartFrom;
         this.Uid = iPointRecord.Uid;
      }/* end method */

      //-------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 3.0 TBottalico 25th March 2011
//  Modified overdue reporting to also check for measurements of OTD# 2535
//
//  Revision 2.0 APinkerton 27th April 2011
//  Added support for Calendar based overdue reporting
//
//  Revision 1.0 APinkerton 20th April 2010
//  Add methods to support note count arithmatic
//
//  Revision 0.0 APinkerton 4th March 2009
//  Add to project
//-------------------------------------------------------------------------------