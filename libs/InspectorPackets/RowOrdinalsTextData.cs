﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the TEXTDATA table. This structure should be revised following any
    /// changes made to this table.
    /// </summary>
    public class RowOrdinalsTextData
    {
        public int Uid { get; set; }
        public int Number { get; set; }
        public int PointUid { get; set; }
        public int MaxLength { get; set; }
        public int AllowSpaces { get; set; }
        public int AllowSpecial { get; set; }
        public int UpperCase { get; set; }
        public int ScannerInput { get; set; }
        public int TextType { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsTextData( SqlCeResultSet iCeResultSet )
        {
           GetTextDataTableColumns( iCeResultSet );
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// TEXTDATA table. This table will need to be revised in the event of any changes
        /// being made to the TEXTDATA table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTextDataTableColumns( SqlCeResultSet iCeResultSet )
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.Number = iCeResultSet.GetOrdinal("Number");
            this.PointUid = iCeResultSet.GetOrdinal("PointUid");
            this.MaxLength = iCeResultSet.GetOrdinal( "MaxLength" );
            this.AllowSpaces = iCeResultSet.GetOrdinal( "AllowSpaces" );
            this.AllowSpecial = iCeResultSet.GetOrdinal( "AllowSpecial" );
            this.UpperCase = iCeResultSet.GetOrdinal( "UpperCase" );
            this.ScannerInput = iCeResultSet.GetOrdinal( "ScannerInput" );
            this.TextType = iCeResultSet.GetOrdinal( "TextType" );
        }/* end method */

    }/* end class */

}/* end namespace*/

//----------------------------------------------------------------------------
//  Revision 0.0 APinkerton 11th September 2012
//  Add to project
//----------------------------------------------------------------------------