﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Unknown Device Packet - returned if the UUID is not known to @ptitude
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketUnknownDevice : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.UNKNOWN_DEVICE;

      /// <summary>
      /// Gets the local DateTime this packet was returned at
      /// </summary>
      public DateTime AT
      {
         get
         {
            return PacketBase.DateTimeNow;
         }
      }

      /// <summary>
      /// Gets or Sets any name that @ptitude may have assigned.
      /// This is used purely for feedback display purposes
      /// </summary>
      public string DeviceName
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 8th July 2010
//  Add Device Name field
//
//  Revision 0.0 APinkerton 5th February 2009
//  Add to project
//----------------------------------------------------------------------------