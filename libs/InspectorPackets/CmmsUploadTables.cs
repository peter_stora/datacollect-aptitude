﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable CMMS Tables for inclusion with the Upload packet
    /// This class addresses OTDs #2719 and #2551
    /// </summary>
   [XmlRoot( ElementName = "Settings" )]
    public class CmmsUploadTables : PacketBase
    {
        [XmlElement("Problems")]
        public EnumCmmsProblemRecords ProblemsTable = new EnumCmmsProblemRecords();


        [XmlElement("Priorities")]
        public EnumCmmsPriorityRecords PrioritiesTable = new EnumCmmsPriorityRecords();


        [XmlElement("CorrectiveActions")]
        public EnumCmmsCorrectiveActionRecords CorrectiveActionsTable = new EnumCmmsCorrectiveActionRecords();


        [XmlElement("WorkTypes")]
        public EnumCmmsWorkTypeRecords WorkTypesTable = new EnumCmmsWorkTypeRecords();


        /// <summary>
        /// Constructor
        /// </summary>
        public CmmsUploadTables()
        {
        }/* end constructor */


        /// <summary>
        /// Add a single cmms problem record to the problems table
        /// </summary>
        /// <param name="iProblem">Problem record</param>
        public void AddCmmsProblem( CmmsProblemRecord iProblem )
        {
            ProblemsTable.AddProblemRecord( iProblem );
        }/* end method */


        /// <summary>
        /// Add a single cmms priority record to the priorities table
        /// </summary>
        /// <param name="iPriority">Priority record</param>
        public void AddCmmsPriority( CmmsPriorityRecord iPriority )
        {
            PrioritiesTable.AddPriorityRecord( iPriority );
        }/* end method */


        /// <summary>
        /// Add a single cmms corrective action record to the problems table
        /// </summary>
        /// <param name="iCorrAction">Corrective action record</param>
        public void AddCmmsCorrectiveAction( CmmsCorrectiveActionRecord iCorrAction )
        {
            CorrectiveActionsTable.AddCorrectiveActionRecord(iCorrAction);
        }/* end method */


        /// <summary>
        /// Add a single cmms work type record to the problems table
        /// </summary>
        /// <param name="iWorkType">WorkType record</param>
        public void AddCmmsWorkType( CmmsWorkTypeRecord iWorkType )
        {
           WorkTypesTable.AddWorkTypeRecord( iWorkType );
        }/* end method */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 27th April 2011
//  Add to project
//----------------------------------------------------------------------------

