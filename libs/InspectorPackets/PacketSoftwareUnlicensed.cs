﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Packet returned by Server if @ptitude license has expired
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketSoftwareUnlicensed : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.SOFTWARE_LICENSE_EXPIRED;

      /// <summary>
      /// Gets the local DateTime this packet was returned at
      /// </summary>
      public DateTime AT
      {
         get
         {
            return PacketBase.DateTimeNow;
         }
      }

      /// <summary>
      /// Gets or Sets a message from the @ptitude service
      /// </summary>
      public string Message
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 7th October 2010
//  Add to project
//----------------------------------------------------------------------------
