﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Message Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumMessageRecords : PacketBase
    {
        protected List<MessageRecord> mMessageRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumMessageRecords()
        {
            mMessageRecord = new List<MessageRecord>();
        }/* end constructor */

        /// <summary>
        /// Message Table record (type ::Message)
        /// </summary>
        [XmlElement("MESSAGE")]
        public List<MessageRecord> Message
        {
            get { return mMessageRecord; }
            set { mMessageRecord = value; }
        }

        /// <summary>
        /// Add a new Message record to the list
        /// </summary>
        /// <param name="item">populated Message record object</param>
        /// <returns>number of Message objects available</returns>
        public int AddMessageRecord(MessageRecord item)
        {
            mMessageRecord.Add(item);
            return mMessageRecord.Count;
        }

        /// <summary>
        /// How many Message records are now available
        /// </summary>
        public int Count { get { return mMessageRecord.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumMessageRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumMessages : EnumMessageRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mMessageRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mMessageRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */



}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------