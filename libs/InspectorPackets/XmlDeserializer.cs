﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class provides a secondary set of deserialization methods
   /// which are quicker than using the native XmlSerializer. However 
   /// the main advantage of these methods are that they allow feedback
   /// on the deserialization process
   /// </summary>
   public class XmlDeserializer : PacketBase
   {
      #region Private fields

      /// <summary>
      /// progress update value object
      /// </summary>
      private IDataProgress mProgressPosition = null;

      /// <summary>
      /// Unicode Carriage Return 
      /// </summary>
      private const char CR = (char) 0x000d;

      /// <summary>
      /// Unicode Line Feed
      /// </summary>
      private const char LF = (char) 0x000a;

      #endregion

      //------------------------------------------------------------------------

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public XmlDeserializer()
      {
         //
         // instantiate a new progress update object and override
         // the default display position to the first half of the
         // progress bar only
         //
         mProgressPosition = new DataProgress( DisplayPosition.FirstHalf );

      }/* end constructor */

      //------------------------------------------------------------------------

      /// <summary>
      /// Gets or sets the progress update manager
      /// </summary>
      public IDataProgress ProgressPosition
      {
         get
         {
            return mProgressPosition;
         }
         set
         {
            mProgressPosition = value;
         }
      }

      //------------------------------------------------------------------------

      #region Deserialize XML for the CORRECTIVEACTION table

      /// <summary>
      /// Manually deserialize the contents of the received DeviceProfile
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public DeviceProfile DeserializeDeviceProfile( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.DEVICEPROFILE );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         DeviceProfile profile = null;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<DEVICEPROFILE_TABLE>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</DEVICEPROFILE_TABLE>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  profile = PopulateDeviceProfileObject( xmlElement );
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return profile;

      }/* end method */


      /// <summary>
      /// Populate a single Device Profile object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private DeviceProfile PopulateDeviceProfileObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         DeviceProfile profile = new DeviceProfile();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode ProfileID = xmlRecord.SelectSingleNode( "ProfileID" );
         if ( ProfileID != null )
            profile.ProfileID = Convert.ToString( ProfileID.InnerText );

         XmlNode ProfileName = xmlRecord.SelectSingleNode( "ProfileName" );
         if ( ProfileName != null )
            profile.ProfileName = Convert.ToString( ProfileName.InnerText );

         XmlNode DeviceName = xmlRecord.SelectSingleNode( "DeviceName" );
         if ( DeviceName != null )
            profile.DeviceName = Convert.ToString( DeviceName.InnerText );

         XmlNode LastModified = xmlRecord.SelectSingleNode( "LastModified" );
         if ( LastModified != null )
            profile.LastModified = ParseInvariantDate( LastModified );

         // return the point object
         return profile;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the NODE table

      /// <summary>
      /// Manually deserialize the contents of the received NODE object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of nodes</returns>
      public EnumNodes DeserializeNodeList( int iTotalRecords, Boolean iResetState )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.NODE );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumNodes enumPoints = new EnumNodes();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<NODE>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</NODE>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  NodeRecord point = PopulateNodeObject( xmlElement, iResetState );

                  // and update the enumerated points list
                  if ( point != null )
                  {
                     enumPoints.AddNodeRecord( point );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumPoints;

      }/* end method */


      /// <summary>
      /// Populate a single Node object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private NodeRecord PopulateNodeObject( StringBuilder iXmlElement, Boolean iResetState )
      {
         // create a new point record object
         NodeRecord nodeRecord = new NodeRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            nodeRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Id = xmlRecord.SelectSingleNode( "Id" );
         if ( Id != null )
            nodeRecord.Id = Id.InnerText;

         XmlNode Description = xmlRecord.SelectSingleNode( "Description" );
         if ( Description != null )
            nodeRecord.Description = Description.InnerText;

         XmlNode LastModified = xmlRecord.SelectSingleNode( "LastModified" );
         if ( LastModified != null )
            nodeRecord.LastModified = ParseInvariantDate( LastModified );

         XmlNode LocationMethod = xmlRecord.SelectSingleNode( "LocationMethod" );
         if ( LocationMethod != null )
            nodeRecord.LocationMethod = Convert.ToByte( LocationMethod.InnerText );

         XmlNode LocationTag = xmlRecord.SelectSingleNode( "LocationTag" );
         if ( LocationTag != null )
            nodeRecord.LocationTag = LocationTag.InnerText;

         XmlNode ParentUid = xmlRecord.SelectSingleNode( "ParentUid" );
         if ( ParentUid != null )
            nodeRecord.ParentUid = new Guid( ParentUid.InnerText );

         XmlNode Prism = xmlRecord.SelectSingleNode( "PRISM" );
         if ( Prism != null )
            nodeRecord.PRISM = Convert.ToInt32( Prism.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            nodeRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode HierarchyDepth = xmlRecord.SelectSingleNode( "HierarchyDepth" );
         if ( HierarchyDepth != null )
            nodeRecord.HierarchyDepth = Convert.ToInt32( HierarchyDepth.InnerText );

         XmlNode Flags = xmlRecord.SelectSingleNode( "Flags" );
         if ( Flags != null )
            nodeRecord.Flags = Convert.ToByte( Flags.InnerText );

         XmlNode Structured = xmlRecord.SelectSingleNode( "Structured" );
         if ( Structured != null )
            nodeRecord.Structured = Convert.ToByte( Structured.InnerText );

         XmlNode RouteId = xmlRecord.SelectSingleNode( "RouteId" );
         if ( RouteId != null )
            nodeRecord.RouteId = Convert.ToInt32( RouteId.InnerText );

         XmlNode FunctionalLocation = xmlRecord.SelectSingleNode( "FunctionalLocation" );
         if ( FunctionalLocation != null )
            nodeRecord.FunctionalLocation = FunctionalLocation.InnerText;

         XmlNode DefWorkType = xmlRecord.SelectSingleNode( "DefWorkType" );
         if ( DefWorkType != null )
            nodeRecord.DefWorkType = Convert.ToInt32( DefWorkType.InnerText );

         XmlNode LastInspResult = xmlRecord.SelectSingleNode( "LastInspResult" );
         if ( LastInspResult != null )
            nodeRecord.LastInspResult = Convert.ToByte( LastInspResult.InnerText );

         XmlNode NoLastMeas = xmlRecord.SelectSingleNode( "NoLastMeas" );
         if ( NoLastMeas != null )
            nodeRecord.NoLastMeas = Convert.ToByte( NoLastMeas.InnerText );

         XmlNode ElementType = xmlRecord.SelectSingleNode( "ElementType" );
         if ( ElementType != null )
            nodeRecord.ElementType = Convert.ToByte( ElementType.InnerText );

         XmlNode CollectionCheck = xmlRecord.SelectSingleNode( "CollectionCheck" );
         if ( CollectionCheck != null && !iResetState )
            nodeRecord.CollectionCheck = Convert.ToByte( CollectionCheck.InnerText );
         else
            nodeRecord.CollectionCheck = Convert.ToByte( FieldTypes.CollectionCheck.None );

         XmlNode AlarmState = xmlRecord.SelectSingleNode( "AlarmState" );
         if ( AlarmState != null && !iResetState )
            nodeRecord.AlarmState = Convert.ToByte( AlarmState.InnerText );
         else
            nodeRecord.AlarmState = Convert.ToByte( FieldTypes.AlarmState.Off );

         // return the point object
         return nodeRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the POINTS table

      /// <summary>
      /// Manualy deserialize the contents of the received POINTS object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <param name="iResetState">resets all status when set true</param>
      /// <returns>enumerated list of points</returns>
      public EnumPoints DeserializePointsList( int iTotalRecords, Boolean iResetState )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.POINTS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumPoints enumPoints = new EnumPoints();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<POINTS>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</POINTS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  PointRecord point = PopulatePointObject( xmlElement, iResetState );

                  // and update the enumerated points list
                  if ( point != null )
                  {
                     enumPoints.AddPointsRecord( point );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumPoints;

      }/* end method */


      /// <summary>
      /// Populate a single Point object
      /// </summary>
      /// <param name="iXmlElement">raw xml point element</param>
      /// <param name="iResetState">resets all status when set true</param>
      /// <returns></returns>
      private PointRecord PopulatePointObject( StringBuilder iXmlElement, Boolean iResetStates )
      {
         // create a new point record object
         PointRecord pointRecord = new PointRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            pointRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Id = xmlRecord.SelectSingleNode( "Id" );
         if ( Id != null )
            pointRecord.Id = Id.InnerText;

         XmlNode NodeUid = xmlRecord.SelectSingleNode( "NodeUid" );
         if ( NodeUid != null )
            pointRecord.NodeUid = new Guid( NodeUid.InnerText );

         XmlNode Description = xmlRecord.SelectSingleNode( "Description" );
         if ( Description != null )
            pointRecord.Description = Description.InnerText;

         XmlNode ProcessType = xmlRecord.SelectSingleNode( "ProcessType" );
         if ( ProcessType != null )
            pointRecord.ProcessType = Convert.ToByte( ProcessType.InnerText );

         XmlNode ScheduleDays = xmlRecord.SelectSingleNode( "ScheduleDays" );
         if ( ScheduleDays != null )
            pointRecord.ScheduleDays = ParseInvariantDouble( ScheduleDays );

         XmlNode compliance = xmlRecord.SelectSingleNode( "Compliance" );
         if ( compliance != null )
            pointRecord.Compliance = Convert.ToInt32( compliance.InnerText );

         XmlNode startFrom = xmlRecord.SelectSingleNode( "StartFrom" );
         if ( startFrom != null )
            pointRecord.StartFrom = ParseInvariantDate( startFrom );

         XmlNode LocationMethod = xmlRecord.SelectSingleNode( "LocationMethod" );
         if ( LocationMethod != null )
            pointRecord.LocationMethod = Convert.ToByte( LocationMethod.InnerText );

         XmlNode LocationTag = xmlRecord.SelectSingleNode( "LocationTag" );
         if ( LocationTag != null )
            pointRecord.LocationTag = LocationTag.InnerText;

         XmlNode LastModified = xmlRecord.SelectSingleNode( "LastModified" );
         if ( LastModified != null )
            pointRecord.LastModified = ParseInvariantDate( LastModified );

         XmlNode DataSaved = xmlRecord.SelectSingleNode( "DataSaved" );
         if ( DataSaved != null )
            pointRecord.DataSaved = Convert.ToBoolean( DataSaved.InnerText );

         XmlNode PointIndex = xmlRecord.SelectSingleNode( "PointIndex" );
         if ( PointIndex != null )
            pointRecord.PointIndex = Convert.ToInt32( PointIndex.InnerText );

         XmlNode PointHostIndex = xmlRecord.SelectSingleNode( "PointHostIndex" );
         if ( PointHostIndex != null )
            pointRecord.PointHostIndex = Convert.ToInt32( PointHostIndex.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            pointRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode TagChanged = xmlRecord.SelectSingleNode( "TagChanged" );
         if ( TagChanged != null )
            pointRecord.TagChanged = Convert.ToByte( TagChanged.InnerText );

         XmlNode RouteId = xmlRecord.SelectSingleNode( "RouteId" );
         if ( RouteId != null )
            pointRecord.RouteId = Convert.ToInt32( RouteId.InnerText );

         XmlNode Skipped = xmlRecord.SelectSingleNode( "Skipped" );
         if ( Skipped != null )
            pointRecord.Skipped = Convert.ToByte( Skipped.InnerText );

         XmlNode TempData = xmlRecord.SelectSingleNode( "TempData" );
         if ( TempData != null )
         {
            foreach ( XmlNode meas in TempData )
            {
               // get the current measurement
               PointMeasurement measurement = GetMeasurement( meas );

               // add measurement to current record
               if ( measurement != null )
                  pointRecord.TempData.AddMeasurement( measurement );
            }
         }

         XmlNode EnvData = xmlRecord.SelectSingleNode( "EnvData" );
         if ( EnvData != null )
         {
            foreach ( XmlNode meas in EnvData )
            {
               // get the current measurement
               PointMeasurement measurement = GetMeasurement( meas );

               // add measurement to current record
               if ( measurement != null )
                  pointRecord.EnvData.AddMeasurement( measurement );
            }
         }

         XmlNode VelData = xmlRecord.SelectSingleNode( "VelData" );
         if ( VelData != null )
         {
            foreach ( XmlNode meas in VelData )
            {
               // get the current measurement
               PointMeasurement measurement = GetMeasurement( meas );

               // add measurement to current record
               if ( measurement != null )
                  pointRecord.VelData.AddMeasurement( measurement );
            }
         }


         XmlNode AlarmState = xmlRecord.SelectSingleNode( "AlarmState" );
         if ( AlarmState != null && !iResetStates )
            pointRecord.AlarmState = Convert.ToByte( AlarmState.InnerText );
         else
            pointRecord.AlarmState = Convert.ToByte( FieldTypes.AlarmState.Off );

         XmlNode RefNoteCount = xmlRecord.SelectSingleNode( "RefNoteCount" );
         if ( RefNoteCount != null && !iResetStates )
            pointRecord.RefNoteCount = Convert.ToInt32( RefNoteCount.InnerText );
         else
            pointRecord.RefNoteCount = 0;

         // return the point object
         return pointRecord;

      }/* end method */


      /// <summary>
      /// Return the raw buffer data as a populated XmlDocument object
      /// </summary>
      /// <param name="iXmlByteBuffer">raw byte data</param>
      /// <returns>populated Xml document</returns>
      private static XmlDocument WriteBufferToXmlDocument( byte[] iXmlByteBuffer )
      {
         XmlDocument xmlRef = new XmlDocument();

         // load the raw byte data into an Xml Document object
         using ( MemoryStream ms = new MemoryStream() )
         {
            ms.Write( iXmlByteBuffer, 0, iXmlByteBuffer.Length );
            ms.Position = 0;
            xmlRef.Load( ms );
         }
         return xmlRef;

      }/* end method */


      /// <summary>
      /// Return the raw buffer data as a populated XmlDocument object
      /// </summary>
      /// <param name="iXmlByteBuffer">raw byte data</param>
      /// <returns>populated Xml document</returns>
      private static XmlDocument WriteBufferToXmlDocument( byte[] iXmlByteBuffer, TableName iTableName )
      {
         XmlDocument xmlRef = new XmlDocument();

         // load the raw byte data into an Xml Document object
         using ( MemoryStream ms = new MemoryStream() )
         {
            ms.Write( iXmlByteBuffer, 0, iXmlByteBuffer.Length );
            ms.Position = 0;

            try
            {
               string fileName = iTableName.ToString() + ".xml";
               ByteArrayProcess.WriteArrayToFile( @fileName, iXmlByteBuffer );
               xmlRef.Load( ms );
            }
            catch
            {
               xmlRef = null;
            }
         }

         return xmlRef;

      }/* end method */


      /// <summary>
      /// Parse the Xml DateNode to a culture invariant DateTime
      /// </summary>
      /// <param name="iXmlDate">Xml DateTime Node</param>
      /// <returns>Invariant DateTime</returns>
      private static DateTime ParseInvariantDate( XmlNode iXmlDate )
      {
         return DateTime.Parse(
                  iXmlDate.InnerText,
                  CultureInfo.InvariantCulture );
      }/* end method */


      /// <summary>
      /// Parse the Xml DoubleNode to a culture invariant Double value
      /// </summary>
      /// <param name="iXmlDouble">Xml float value Node</param>
      /// <returns>Invariant Double</returns>
      private static Double ParseInvariantDouble( XmlNode iXmlDouble )
      {
         return Double.Parse(
                  iXmlDouble.InnerText,
                  CultureInfo.InvariantCulture );
      }/* end method */


      /// <summary>
      /// Get the point measurement from the XML Node as a culture invariant value
      /// </summary>
      /// <param name="iMeasurementNode">Xml Measurement node</param>
      /// <returns>PointMeasurement object, or null on failure</returns>
      private static PointMeasurement GetMeasurement( XmlNode iMeasurementNode )
      {
         // create a new measurement
         PointMeasurement measurement = new PointMeasurement();

         try
         {
            // extract both the timestamp and the process value
            XmlNode localTime = iMeasurementNode.Attributes.GetNamedItem( "LocalTime" );
            XmlNode TextValue = iMeasurementNode.Attributes.GetNamedItem( "TextValue" );
            XmlNode Value = iMeasurementNode.Attributes.GetNamedItem( "Value" );

            // parse as culture invariant and add to the measurement
            measurement.Value = ParseInvariantDouble( Value );

            if ( Double.IsNaN( measurement.Value ) )
            {
               measurement.TextValue = TextValue.InnerText;
            }

            measurement.LocalTime = ParseInvariantDate( localTime );
         }
         catch
         {
            measurement = null;
         }
         return measurement;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the INSPECTION table

      /// <summary>
      /// Manually deserialize the contents of the received INSPECTION object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of inspection records</returns>
      public EnumInspection DeserializeInspectionList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.INSPECTION );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumInspection enumInspection = new EnumInspection();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<INSPECTION>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</INSPECTION>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  InspectionRecord inspection = PopulateInspectionObject( xmlElement );

                  // and update the enumerated points list
                  if ( inspection != null )
                  {
                     enumInspection.AddInspectionRecord( inspection );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumInspection;

      }/* end method */


      /// <summary>
      /// Populate a single Inspection object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private InspectionRecord PopulateInspectionObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         InspectionRecord inspectionRecord = new InspectionRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            inspectionRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            inspectionRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            inspectionRecord.PointUid = new Guid( PointUid.InnerText );

         XmlNode Question = xmlRecord.SelectSingleNode( "Question" );
         if ( Question != null )
            inspectionRecord.Question = Question.InnerText;

         XmlNode AlertText = xmlRecord.SelectSingleNode( "AlertText" );
         if ( AlertText != null )
            inspectionRecord.AlertText = AlertText.InnerText;

         XmlNode DangerText = xmlRecord.SelectSingleNode( "DangerText" );
         if ( DangerText != null )
            inspectionRecord.DangerText = DangerText.InnerText;

         XmlNode NumberOfChoices = xmlRecord.SelectSingleNode( "NumberOfChoices" );
         if ( NumberOfChoices != null )
            inspectionRecord.NumberOfChoices = Convert.ToByte( NumberOfChoices.InnerText );

         XmlNode Choice1 = xmlRecord.SelectSingleNode( "Choice1" );
         if ( Choice1 != null )
            inspectionRecord.Choice1 = Choice1.InnerText;

         XmlNode Choice2 = xmlRecord.SelectSingleNode( "Choice2" );
         if ( Choice2 != null )
            inspectionRecord.Choice2 = Choice2.InnerText;

         XmlNode Choice3 = xmlRecord.SelectSingleNode( "Choice3" );
         if ( Choice3 != null )
            inspectionRecord.Choice3 = Choice3.InnerText;

         XmlNode Choice4 = xmlRecord.SelectSingleNode( "Choice4" );
         if ( Choice4 != null )
            inspectionRecord.Choice4 = Choice4.InnerText;

         XmlNode Choice5 = xmlRecord.SelectSingleNode( "Choice5" );
         if ( Choice5 != null )
            inspectionRecord.Choice5 = Choice5.InnerText;

         XmlNode Choice6 = xmlRecord.SelectSingleNode( "Choice6" );
         if ( Choice6 != null )
            inspectionRecord.Choice6 = Choice6.InnerText;

         XmlNode Choice7 = xmlRecord.SelectSingleNode( "Choice7" );
         if ( Choice7 != null )
            inspectionRecord.Choice7 = Choice7.InnerText;

         XmlNode Choice8 = xmlRecord.SelectSingleNode( "Choice8" );
         if ( Choice8 != null )
            inspectionRecord.Choice8 = Choice8.InnerText;

         XmlNode Choice9 = xmlRecord.SelectSingleNode( "Choice9" );
         if ( Choice9 != null )
            inspectionRecord.Choice9 = Choice9.InnerText;

         XmlNode Choice10 = xmlRecord.SelectSingleNode( "Choice10" );
         if ( Choice10 != null )
            inspectionRecord.Choice10 = Choice10.InnerText;

         XmlNode Choice11 = xmlRecord.SelectSingleNode( "Choice11" );
         if ( Choice11 != null )
            inspectionRecord.Choice11 = Choice11.InnerText;

         XmlNode Choice12 = xmlRecord.SelectSingleNode( "Choice12" );
         if ( Choice12 != null )
            inspectionRecord.Choice12 = Choice12.InnerText;

         XmlNode Choice13 = xmlRecord.SelectSingleNode( "Choice13" );
         if ( Choice13 != null )
            inspectionRecord.Choice13 = Choice13.InnerText;

         XmlNode Choice14 = xmlRecord.SelectSingleNode( "Choice14" );
         if ( Choice14 != null )
            inspectionRecord.Choice14 = Choice14.InnerText;

         XmlNode Choice15 = xmlRecord.SelectSingleNode( "Choice15" );
         if ( Choice15 != null )
            inspectionRecord.Choice15 = Choice15.InnerText;

         XmlNode AlarmType1 = xmlRecord.SelectSingleNode( "AlarmType1" );
         if ( AlarmType1 != null )
            inspectionRecord.AlarmType1 = Convert.ToByte( AlarmType1.InnerText );

         XmlNode AlarmType2 = xmlRecord.SelectSingleNode( "AlarmType2" );
         if ( AlarmType2 != null )
            inspectionRecord.AlarmType2 = Convert.ToByte( AlarmType2.InnerText );

         XmlNode AlarmType3 = xmlRecord.SelectSingleNode( "AlarmType3" );
         if ( AlarmType3 != null )
            inspectionRecord.AlarmType3 = Convert.ToByte( AlarmType3.InnerText );

         XmlNode AlarmType4 = xmlRecord.SelectSingleNode( "AlarmType4" );
         if ( AlarmType4 != null )
            inspectionRecord.AlarmType4 = Convert.ToByte( AlarmType4.InnerText );

         XmlNode AlarmType5 = xmlRecord.SelectSingleNode( "AlarmType5" );
         if ( AlarmType5 != null )
            inspectionRecord.AlarmType5 = Convert.ToByte( AlarmType5.InnerText );

         XmlNode AlarmType6 = xmlRecord.SelectSingleNode( "AlarmType6" );
         if ( AlarmType6 != null )
            inspectionRecord.AlarmType6 = Convert.ToByte( AlarmType6.InnerText );

         XmlNode AlarmType7 = xmlRecord.SelectSingleNode( "AlarmType7" );
         if ( AlarmType7 != null )
            inspectionRecord.AlarmType7 = Convert.ToByte( AlarmType7.InnerText );

         XmlNode AlarmType8 = xmlRecord.SelectSingleNode( "AlarmType8" );
         if ( AlarmType8 != null )
            inspectionRecord.AlarmType8 = Convert.ToByte( AlarmType8.InnerText );

         XmlNode AlarmType9 = xmlRecord.SelectSingleNode( "AlarmType9" );
         if ( AlarmType9 != null )
            inspectionRecord.AlarmType9 = Convert.ToByte( AlarmType9.InnerText );

         XmlNode AlarmType10 = xmlRecord.SelectSingleNode( "AlarmType10" );
         if ( AlarmType10 != null )
            inspectionRecord.AlarmType10 = Convert.ToByte( AlarmType10.InnerText );

         XmlNode AlarmType11 = xmlRecord.SelectSingleNode( "AlarmType11" );
         if ( AlarmType11 != null )
            inspectionRecord.AlarmType11 = Convert.ToByte( AlarmType11.InnerText );

         XmlNode AlarmType12 = xmlRecord.SelectSingleNode( "AlarmType12" );
         if ( AlarmType12 != null )
            inspectionRecord.AlarmType12 = Convert.ToByte( AlarmType12.InnerText );

         XmlNode AlarmType13 = xmlRecord.SelectSingleNode( "AlarmType13" );
         if ( AlarmType13 != null )
            inspectionRecord.AlarmType13 = Convert.ToByte( AlarmType13.InnerText );

         XmlNode AlarmType14 = xmlRecord.SelectSingleNode( "AlarmType14" );
         if ( AlarmType14 != null )
            inspectionRecord.AlarmType14 = Convert.ToByte( AlarmType14.InnerText );

         XmlNode AlarmType15 = xmlRecord.SelectSingleNode( "AlarmType15" );
         if ( AlarmType15 != null )
            inspectionRecord.AlarmType15 = Convert.ToByte( AlarmType15.InnerText );

         XmlNode BaselineDataTime = xmlRecord.SelectSingleNode( "BaselineDataTime" );
         if ( BaselineDataTime != null )
            inspectionRecord.BaselineDataTime = ParseInvariantDate( BaselineDataTime );

         XmlNode BaselineDataValue = xmlRecord.SelectSingleNode( "BaselineDataValue" );
         if ( BaselineDataValue != null )
            inspectionRecord.BaselineDataValue = Convert.ToByte( BaselineDataValue.InnerText );

         // return the inspection object
         return inspectionRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the PROCESS table

      /// <summary>
      /// Manually deserialize the contents of the received PROCESS object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of Process records</returns>
      public EnumProcess DeserializeProcessList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.PROCESS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumProcess enumProcess = new EnumProcess();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<PROCESS>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</PROCESS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  ProcessRecord process = PopulateProcessObject( xmlElement );

                  // and update the enumerated points list
                  if ( process != null )
                  {
                     enumProcess.AddProcessRecord( process );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumProcess;

      }/* end method */


      /// <summary>
      /// Populate a single Process object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private ProcessRecord PopulateProcessObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         ProcessRecord processRecord = new ProcessRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            processRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            processRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            processRecord.PointUid = new Guid( PointUid.InnerText );

         XmlNode Form = xmlRecord.SelectSingleNode( "Form" );
         if ( Form != null )
            processRecord.Form = Convert.ToByte( Form.InnerText );

         XmlNode RangeMin = xmlRecord.SelectSingleNode( "RangeMin" );
         if ( RangeMin != null )
            processRecord.RangeMin = ParseInvariantDouble( RangeMin );

         XmlNode RangeMax = xmlRecord.SelectSingleNode( "RangeMax" );
         if ( RangeMax != null )
            processRecord.RangeMax = ParseInvariantDouble( RangeMax );

         XmlNode Units = xmlRecord.SelectSingleNode( "Units" );
         if ( Units != null )
            processRecord.Units = Units.InnerText;

         XmlNode AutoRange = xmlRecord.SelectSingleNode( "AutoRange" );
         if ( AutoRange != null )
            processRecord.AutoRange = Convert.ToBoolean( AutoRange.InnerText );

         XmlNode AlarmType = xmlRecord.SelectSingleNode( "AlarmType" );
         if ( AlarmType != null )
            processRecord.AlarmType = Convert.ToByte( AlarmType.InnerText );

         XmlNode AlarmState = xmlRecord.SelectSingleNode( "AlarmState" );
         if ( AlarmState != null )
            processRecord.AlarmState = Convert.ToByte( AlarmState.InnerText );

         XmlNode UpperAlert = xmlRecord.SelectSingleNode( "UpperAlert" );
         if ( UpperAlert != null )
            processRecord.UpperAlert = ParseInvariantDouble( UpperAlert );

         XmlNode LowerAlert = xmlRecord.SelectSingleNode( "LowerAlert" );
         if ( LowerAlert != null )
            processRecord.LowerAlert = ParseInvariantDouble( LowerAlert );

         XmlNode UpperDanger = xmlRecord.SelectSingleNode( "UpperDanger" );
         if ( UpperDanger != null )
            processRecord.UpperDanger = ParseInvariantDouble( UpperDanger );

         XmlNode LowerDanger = xmlRecord.SelectSingleNode( "LowerDanger" );
         if ( LowerDanger != null )
            processRecord.LowerDanger = ParseInvariantDouble( LowerDanger );

         XmlNode BaselineDataTime = xmlRecord.SelectSingleNode( "BaselineDataTime" );
         if ( BaselineDataTime != null )
            processRecord.BaselineDataTime = ParseInvariantDate( BaselineDataTime );

         XmlNode BaselineDataValue = xmlRecord.SelectSingleNode( "BaselineDataValue" );
         if ( BaselineDataValue != null )
            processRecord.BaselineDataValue = ParseInvariantDouble( BaselineDataValue );

         // return the point object
         return processRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the TEXTDATA table

      /// <summary>
      /// Manually deserialize the contents of the received TEXTDATA object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of TextData records</returns>
      public EnumTextData DeserializeTextDataList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.TEXTDATA );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumTextData enumTextData = new EnumTextData();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<TEXTDATA>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</TEXTDATA>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a TextData object using this element
                  TextDataRecord textData = PopulateTextDataObject( xmlElement );

                  // and update the enumerated TextData list
                  if ( textData != null )
                  {
                     enumTextData.AddTextDataRecord( textData );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumTextData;

      }/* end method */


      /// <summary>
      /// Populate a single TextData object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private TextDataRecord PopulateTextDataObject( StringBuilder iXmlElement )
      {
         // create a new TextData record object
         TextDataRecord textDataRecord = new TextDataRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode TextLength = xmlRecord.SelectSingleNode( "MaxLength" );
         if ( TextLength != null )
            textDataRecord.MaxLength = Convert.ToByte( TextLength.InnerText );

         XmlNode TextType = xmlRecord.SelectSingleNode( "TextType" );
         if ( TextType != null )
            textDataRecord.TextType = Convert.ToByte( TextType.InnerText );

         XmlNode AllowSpaces = xmlRecord.SelectSingleNode( "AllowSpaces" );
         if ( AllowSpaces != null )
            textDataRecord.AllowSpaces = Convert.ToBoolean( AllowSpaces.InnerText );

         XmlNode AllowSpecial = xmlRecord.SelectSingleNode( "AllowSpecial" );
         if ( AllowSpecial != null )
            textDataRecord.AllowSpecial = Convert.ToBoolean( AllowSpecial.InnerText );

         XmlNode UpperCase = xmlRecord.SelectSingleNode( "UpperCase" );
         if ( UpperCase != null )
            textDataRecord.UpperCase = Convert.ToBoolean( UpperCase.InnerText );

         XmlNode ScannerInput = xmlRecord.SelectSingleNode( "ScannerInput" );
         if ( ScannerInput != null )
            textDataRecord.ScannerInput = Convert.ToBoolean( ScannerInput.InnerText );

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            textDataRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            textDataRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            textDataRecord.PointUid = new Guid( PointUid.InnerText );

         // return the point object
         return textDataRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the MCC table

      /// <summary>
      /// Manually deserialize the contents of the received MCC object.
      /// Note and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of MCC records</returns>
      public EnumMCC DeserializeMCCList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.MCC );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumMCC enumProcess = new EnumMCC();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<MCC>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</MCC>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  MCCRecord mcc = PopulateMCCObject( xmlElement );

                  // and update the enumerated points list
                  if ( mcc != null )
                  {
                     enumProcess.AddMCCRecord( mcc );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumProcess;

      }/* end method */


      /// <summary>
      /// Populate a single MCC object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private MCCRecord PopulateMCCObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         MCCRecord MCCRecord = new MCCRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            MCCRecord.Uid = new Guid( Uid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            MCCRecord.Number = Convert.ToInt32( Number.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            MCCRecord.PointUid = new Guid( PointUid.InnerText );

         XmlNode EnvAlarmState = xmlRecord.SelectSingleNode( "EnvAlarmState" );
         if ( EnvAlarmState != null )
            MCCRecord.EnvAlarmState = Convert.ToByte( EnvAlarmState.InnerText );

         XmlNode EnvAlarm1 = xmlRecord.SelectSingleNode( "EnvAlarm1" );
         if ( EnvAlarm1 != null )
            MCCRecord.EnvAlarm1 = ParseInvariantDouble( EnvAlarm1 );

         XmlNode EnvAlarm2 = xmlRecord.SelectSingleNode( "EnvAlarm2" );
         if ( EnvAlarm2 != null )
            MCCRecord.EnvAlarm2 = ParseInvariantDouble( EnvAlarm2 );

         XmlNode EnvBaselineDataTime = xmlRecord.SelectSingleNode( "EnvBaselineDataTime" );
         if ( EnvBaselineDataTime != null )
            MCCRecord.EnvBaselineDataTime = ParseInvariantDate( EnvBaselineDataTime );

         XmlNode EnvBaselineDataValue = xmlRecord.SelectSingleNode( "EnvBaselineDataValue" );
         if ( EnvBaselineDataValue != null )
            MCCRecord.EnvBaselineDataValue = ParseInvariantDouble( EnvBaselineDataValue );

         XmlNode TmpAlarmState = xmlRecord.SelectSingleNode( "TmpAlarmState" );
         if ( TmpAlarmState != null )
            MCCRecord.TmpAlarmState = Convert.ToByte( TmpAlarmState.InnerText );

         XmlNode TmpAlarm1 = xmlRecord.SelectSingleNode( "TmpAlarm1" );
         if ( TmpAlarm1 != null )
            MCCRecord.TmpAlarm1 = ParseInvariantDouble( TmpAlarm1 );

         XmlNode TmpAlarm2 = xmlRecord.SelectSingleNode( "TmpAlarm2" );
         if ( TmpAlarm2 != null )
            MCCRecord.TmpAlarm2 = ParseInvariantDouble( TmpAlarm2 );

         XmlNode TmpBaselineDataTime = xmlRecord.SelectSingleNode( "TmpBaselineDataTime" );
         if ( TmpBaselineDataTime != null )
            MCCRecord.TmpBaselineDataTime = ParseInvariantDate( TmpBaselineDataTime );

         XmlNode TmpBaselineDataValue = xmlRecord.SelectSingleNode( "TmpBaselineDataValue" );
         if ( TmpBaselineDataValue != null )
            MCCRecord.TmpBaselineDataValue = ParseInvariantDouble( TmpBaselineDataValue );

         XmlNode VelAlarmState = xmlRecord.SelectSingleNode( "VelAlarmState" );
         if ( VelAlarmState != null )
            MCCRecord.VelAlarmState = Convert.ToByte( VelAlarmState.InnerText );

         XmlNode VelAlarm1 = xmlRecord.SelectSingleNode( "VelAlarm1" );
         if ( VelAlarm1 != null )
            MCCRecord.VelAlarm1 = ParseInvariantDouble( VelAlarm1 );

         XmlNode VelAlarm2 = xmlRecord.SelectSingleNode( "VelAlarm2" );
         if ( VelAlarm2 != null )
            MCCRecord.VelAlarm2 = ParseInvariantDouble( VelAlarm2 );

         XmlNode VelBaselineDataTime = xmlRecord.SelectSingleNode( "VelBaselineDataTime" );
         if ( VelBaselineDataTime != null )
            MCCRecord.VelBaselineDataTime = ParseInvariantDate( VelBaselineDataTime );

         XmlNode VelBaselineDataValue = xmlRecord.SelectSingleNode( "VelBaselineDataValue" );
         if ( VelBaselineDataValue != null )
            MCCRecord.VelBaselineDataValue = ParseInvariantDouble( VelBaselineDataValue );

         XmlNode SystemUnits = xmlRecord.SelectSingleNode( "SystemUnits" );
         if ( SystemUnits != null )
            MCCRecord.SystemUnits = Convert.ToByte( SystemUnits.InnerText );

         XmlNode TempUnits = xmlRecord.SelectSingleNode( "TempUnits" );
         if ( TempUnits != null )
            MCCRecord.TempUnits = Convert.ToByte( TempUnits.InnerText );

         // return the point object
         return MCCRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the FFTPoint table

      /// <summary>
      /// Manually deserialize the contents of the received EnumFFTPoint 
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of FFTPoint records</returns>
      public EnumFFTPoints DeserializeFFTPointsList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.FFTPOINT );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumFFTPoints enumProcess = new EnumFFTPoints();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<FFTPoint>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</FFTPoint>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  FFTPointRecord fftPoint = PopulateFFTPointObject( xmlElement );

                  // and update the enumerated points list
                  if ( fftPoint != null )
                  {
                     enumProcess.AddFFTPointRecord( fftPoint );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumProcess;

      }/* end method */


      /// <summary>
      /// Populate a single FFTPoint object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private FFTPointRecord PopulateFFTPointObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         FFTPointRecord FFTPoint = new FFTPointRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            FFTPoint.Uid = new Guid( Uid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            FFTPoint.Number = Convert.ToInt32( Number.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            FFTPoint.PointUid = new Guid( PointUid.InnerText );

         XmlNode TakeData = xmlRecord.SelectSingleNode( "TakeData" );
         if ( TakeData != null )
            FFTPoint.TakeData = Convert.ToByte( TakeData.InnerText );

         XmlNode UseConfiguration = xmlRecord.SelectSingleNode( "UseConfiguration" );
         if ( UseConfiguration != null )
            FFTPoint.UseConfiguration = Convert.ToByte( UseConfiguration.InnerText );

         XmlNode DefaultRunningSpeed = xmlRecord.SelectSingleNode( "DefaultRunningSpeed" );
         if ( DefaultRunningSpeed != null )
            FFTPoint.DefaultRunningSpeed = ParseInvariantDouble( DefaultRunningSpeed );

         XmlNode EnvType = xmlRecord.SelectSingleNode( "EnvType" );
         if ( EnvType != null )
            FFTPoint.EnvType = Convert.ToByte( EnvType.InnerText );

         XmlNode EnvDetection = xmlRecord.SelectSingleNode( "EnvDetection" );
         if ( EnvDetection != null )
            FFTPoint.EnvDetection = Convert.ToByte( EnvDetection.InnerText );

         XmlNode EnvNumberOfLines = xmlRecord.SelectSingleNode( "EnvNumberOfLines" );
         if ( EnvNumberOfLines != null )
            FFTPoint.EnvNumberOfLines = Convert.ToInt32( EnvNumberOfLines.InnerText );

         XmlNode EnvAverages = xmlRecord.SelectSingleNode( "EnvAverages" );
         if ( EnvAverages != null )
            FFTPoint.EnvAverages = Convert.ToInt32( EnvAverages.InnerText );

         XmlNode EnvWindow = xmlRecord.SelectSingleNode( "EnvWindow" );
         if ( EnvWindow != null )
            FFTPoint.EnvWindow = Convert.ToByte( EnvWindow.InnerText );

         XmlNode EnvFreqMax = xmlRecord.SelectSingleNode( "EnvFreqMax" );
         if ( EnvFreqMax != null )
            FFTPoint.EnvFreqMax = ParseInvariantDouble( EnvFreqMax );

         XmlNode VelType = xmlRecord.SelectSingleNode( "VelType" );
         if ( VelType != null )
            FFTPoint.VelType = Convert.ToByte( VelType.InnerText );

         XmlNode VelDetection = xmlRecord.SelectSingleNode( "VelDetection" );
         if ( VelDetection != null )
            FFTPoint.VelDetection = Convert.ToByte( VelDetection.InnerText );

         XmlNode VelNumberOfLines = xmlRecord.SelectSingleNode( "VelNumberOfLines" );
         if ( VelNumberOfLines != null )
            FFTPoint.VelNumberOfLines = Convert.ToInt32( VelNumberOfLines.InnerText );

         XmlNode VelAverages = xmlRecord.SelectSingleNode( "VelAverages" );
         if ( VelAverages != null )
            FFTPoint.VelAverages = Convert.ToInt32( VelAverages.InnerText );

         XmlNode VelWindow = xmlRecord.SelectSingleNode( "VelWindow" );
         if ( VelWindow != null )
            FFTPoint.VelWindow = Convert.ToByte( VelWindow.InnerText );

         XmlNode VelFreqMax = xmlRecord.SelectSingleNode( "VelFreqMax" );
         if ( VelFreqMax != null )
            FFTPoint.VelFreqMax = ParseInvariantDouble( VelFreqMax );

         XmlNode AcclType = xmlRecord.SelectSingleNode( "AcclType" );
         if ( AcclType != null )
            FFTPoint.AcclType = Convert.ToByte( AcclType.InnerText );

         XmlNode AcclDetection = xmlRecord.SelectSingleNode( "AcclDetection" );
         if ( AcclDetection != null )
            FFTPoint.AcclDetection = Convert.ToByte( AcclDetection.InnerText );

         XmlNode AcclNumberOfLines = xmlRecord.SelectSingleNode( "AcclNumberOfLines" );
         if ( AcclNumberOfLines != null )
            FFTPoint.AcclNumberOfLines = Convert.ToInt32( AcclNumberOfLines.InnerText );

         XmlNode AcclAverages = xmlRecord.SelectSingleNode( "AcclAverages" );
         if ( AcclAverages != null )
            FFTPoint.AcclAverages = Convert.ToInt32( AcclAverages.InnerText );

         XmlNode AcclWindow = xmlRecord.SelectSingleNode( "AcclWindow" );
         if ( AcclWindow != null )
            FFTPoint.AcclWindow = Convert.ToByte( AcclWindow.InnerText );

         XmlNode AcclFreqMax = xmlRecord.SelectSingleNode( "AcclFreqMax" );
         if ( AcclFreqMax != null )
            FFTPoint.AcclFreqMax = ParseInvariantDouble( AcclFreqMax );


         // return the point object
         return FFTPoint;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the FFTChannel table

      /// <summary>
      /// Manually deserialize the contents of the received EnumFFTChannel 
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of FFTPoint records</returns>
      public EnumFFTChannelRecords DeserializeFFTChannelList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.FFTCHANNEL );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of objects
         EnumFFTChannelRecords enumFFTChannels = new EnumFFTChannelRecords();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<FFTChannel>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</FFTChannel>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text
                  xmlElement.Append( streamXml );

                  // populate a point object using this element
                  FFTChannelRecord fftChannel = 
                     PacketArchive.DeserializeObject<FFTChannelRecord>( xmlElement.ToString() );

                  // and update the enumerated points list
                  if ( fftChannel != null )
                  {
                     enumFFTChannels.AddFFTChannelRecord( fftChannel );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumFFTChannels;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the Reference Media table

      /// <summary>
      /// Manually deserialize the contents of the received Media enumerations 
      /// object and return feedback events on each record processed.
      /// NOTE: This method differs from the others in this unit in that
      /// it processes the data in raw binary format.
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of ReferenceMedia records</returns>
      public EnumReferenceMedia DeserializeReferenceMediaList( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.REFERENCEMEDIA );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         // create an empty list of point objects
         EnumReferenceMedia enumReferenceMedia = new EnumReferenceMedia();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( FileStream stream = new FileStream( @fileName,
               FileMode.Open, FileAccess.Read ) )
         {
            // set stream through binary reader
            using ( BinaryReader br = new BinaryReader( stream ) )
            {
               // skip past the XML header
               byte[] buffer = br.ReadBytes( 255 );

               // find the first media element
               int index = ByteArrayProcess.IndexFirstBytePattern( XML_ELEMENT_MEDIA_START, buffer, 0 );

               // reset file pointer to start of the first XML element
               br.BaseStream.Seek( index, SeekOrigin.Begin );

               // clear the buffer
               ByteArrayProcess.ClearByteBuffer( ref buffer );

               // create a cache array
               byte[] cache = new byte[ 0 ];
               byte[] loader = new byte[ 1 ];

               // repeat while loader cache is not empty
               while ( loader.Length > 0 )
               {
                  // read the next block of bytes to the cache
                  loader = br.ReadBytes( 0x80000 );

                  // did we read anything?
                  if ( loader.Length > 0 )
                  {
                     // append to the receive buffer
                     ByteArrayProcess.AppendToByteBuffer( loader, ref buffer );

                     // reset the search index
                     index = 0;

                     // repeat until there's no valid index
                     while ( index != -1 )
                     {
                        // do we have an end element?
                        index = ByteArrayProcess.IndexFirstBytePattern( XML_ELEMENT_MEDIA_END, buffer, 0 );

                        if ( index > -1 )
                        {
                           // copy the buffer back to the cache
                           ByteArrayProcess.ExtractArray( buffer,
                              ref cache, 0, index + XML_ELEMENT_MEDIA_END.Length );

                           // remove the processed section from the buffer
                           buffer = ByteArrayProcess.TrimStart(
                              buffer, index + XML_ELEMENT_MEDIA_END.Length );

                           // deserialize the xml
                           ReferenceMediaRecord media = 
                           PacketBase.DeserializeObject<ReferenceMediaRecord>( cache );

                           if ( media != null )
                           {
                              enumReferenceMedia.AddMediaRecord( media );
                              mProgressPosition.Update( true );
                           }
                        }
                     }
                     // clear the byte array
                     ByteArrayProcess.ClearByteBuffer( ref cache );
                  }
               }

               ByteArrayProcess.ClearByteBuffer( ref buffer );
               ByteArrayProcess.ClearByteBuffer( ref cache );
               ByteArrayProcess.ClearByteBuffer( ref loader );

               // close the stream - we're done here!
               br.Close();
               stream.Close();
            }
         }

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumReferenceMedia;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the MESSAGE table

      /// <summary>
      /// Manualy deserialize the contents of the received MESSAGES object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of messages</returns>
      public EnumMessages DeserializeMessages( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.MESSAGE );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of message objects
         EnumMessages enumMessages = new EnumMessages();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            bool isTextBody = false;

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               //
               // reinstate any CR or LF from the body text that
               // will have been stripped away by the trim command
               // This addresses OTD #3866
               //
               ReinstateUnicodeCRLF( ref streamXml, ref isTextBody,
                  "<Text>", "</Text>" );

               // is this the start of a Message element
               if ( streamXml == "<MESSAGE>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</MESSAGE>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the message object
                  MessageRecord messageRecord =
                        PacketBase.DeserializeObject<MessageRecord>( xmlElement.ToString() );

                  // fix for OTD# 4515, handle nulls from @A2010MR1 with Oracle...
                  if ( messageRecord != null && messageRecord.Text == null )
                     messageRecord.Text = string.Empty;

                  //
                  // re-entetize line feed handling as the .Net CF deserializer
                  // strips away the '/r' from any string containing '/r/n'
                  // This addresses OTD #3866
                  //
                  messageRecord.Text =
                     EntitizeLineHandling( messageRecord.Text );

                  // add to the enumerated list
                  if ( messageRecord != null )
                  {
                     enumMessages.AddMessageRecord( messageRecord );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumMessages;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the INSTRUCTIONS table

      /// <summary>
      /// Manualy deserialize the contents of the received INSTRUCTIONS 
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of instructions</returns>
      public EnumInstructions DeserializeInstructions( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.INSTRUCTIONS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of message objects
         EnumInstructions enumInstructions = new EnumInstructions();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            bool isTextBody = false;

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               //
               // reinstate any CR or LF from the body text that
               // will have been stripped away by the trim command
               // This addresses OTD #3866
               //
               ReinstateUnicodeCRLF( ref streamXml, ref isTextBody,
                  "<TextBody>", "</TextBody>" );

               // is this the start of a Instruction element
               if ( streamXml == "<INSTRUCTIONS>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</INSTRUCTIONS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the instructions object
                  InstructionsRecord instructionsRecord =
                        PacketBase.DeserializeObject<InstructionsRecord>( xmlElement.ToString() );

                  // fix for OTD# 4515, handle nulls from @A2010MR1 with Oracle...
                  if ( instructionsRecord != null && instructionsRecord.TextBody == null )
                     instructionsRecord.TextBody = string.Empty;

                  //
                  // re-entetize line feed handling as the .Net CF deserializer
                  // strips away the '/r' from any string containing '/r/n'
                  // This addresses OTD #3866
                  //
                  instructionsRecord.TextBody =
                     EntitizeLineHandling( instructionsRecord.TextBody );

                  // add to the enumerated list
                  if ( instructionsRecord != null )
                  {
                     enumInstructions.AddInstructionsRecord( instructionsRecord );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumInstructions;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the DERIVEDPOINT table

      /// <summary>
      /// Manualy deserialize the contents of the received DERIVEDPOINTs 
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of Derived Points</returns>
      public EnumDerivedPoints DeserializeDerivedPoints( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.DERIVEDPOINT );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumDerivedPoints enumDerivedPoints = new EnumDerivedPoints();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Instruction element
               if ( streamXml == "<DERIVEDPOINT>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</DERIVEDPOINT>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the Derived Point object
                  DerivedPointRecord derivedPoint =
                        PacketBase.DeserializeObject<DerivedPointRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( derivedPoint != null )
                  {
                     enumDerivedPoints.AddDerivedPointRecord( derivedPoint );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumDerivedPoints;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the DERIVEDITEMS table

      /// <summary>
      /// Manualy deserialize the contents of the received DERIVEDITEMS
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of Derived Items</returns>
      public EnumDerivedItems DeserializeDerivedItems( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.DERIVEDITEMS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumDerivedItems enumDerivedItems = new EnumDerivedItems();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Instruction element
               if ( streamXml == "<DERIVEDITEMS>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</DERIVEDITEMS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the Derived Item object
                  DerivedItemsRecord derivedItem =
                        PacketBase.DeserializeObject<DerivedItemsRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( derivedItem != null )
                  {
                     enumDerivedItems.AddDerivedItemsRecord( derivedItem );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumDerivedItems;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the CONDITIONALPOINT table

      /// <summary>
      /// Manualy deserialize the contents of the received CONDITIONALPOINTs
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of Conditional Points</returns>
      public EnumConditionalPoints DeserializeConditionalPoints( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.CONDITIONALPOINT );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Conditional Point objects
         EnumConditionalPoints enumConditionalPoints = new EnumConditionalPoints();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the conditional points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Instruction element
               if ( streamXml == "<CONDITIONALPOINT>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</CONDITIONALPOINT>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the Conditional Point object
                  ConditionalPointRecord conditionalPoint =
                        PacketBase.DeserializeObject<ConditionalPointRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( conditionalPoint != null )
                  {
                     enumConditionalPoints.AddConditionalPointRecord( conditionalPoint );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumConditionalPoints;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the C_NOTE table

      /// <summary>
      /// Manualy deserialize the contents of the received EnumCodedNotes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received C_NOTE objects</returns>
      public EnumCodedNotes DeserializeCodedNotes( byte[] iXmlByteBuffer )
      {
         EnumCodedNotes enumCodedNotes = new EnumCodedNotes();

         // convert the byte buffer to a populated Xml document object
         XmlDocument xmlRef = WriteBufferToXmlDocument( iXmlByteBuffer );

         // reference the Node Table
         XmlNode tableData = xmlRef.ChildNodes[ 1 ].ChildNodes[ 1 ];

         // how many records in total?
         int totalRecords = tableData.ChildNodes.Count;

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = totalRecords;

         // process all records
         foreach ( XmlNode xmlRecord in tableData )
         {
            // deserialize the media object
            CodedNoteRecord codedNote =
                        PacketBase.DeserializeObject<CodedNoteRecord>( xmlRecord.OuterXml );

            // add to the enumerated list
            if ( codedNote != null )
               enumCodedNotes.AddCodedNoteRecord( codedNote );

            // feedback please!
            mProgressPosition.Update( true );
         }

         return enumCodedNotes;

      }/* end method */


      /// <summary>
      /// Manually deserialize the contents of the received EnumCodedNotes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>An enumeration of received C_NOTE objects</returns>
      public EnumCodedNotes DeserializeCodedNotes( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.C_NOTE );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCodedNotes enumNoteCodes = new EnumCodedNotes();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<C_NOTE>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</C_NOTE>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the media object
                  CodedNoteRecord codedNote =
                        PacketBase.DeserializeObject<CodedNoteRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( codedNote != null )
                  {
                     enumNoteCodes.AddCodedNoteRecord( codedNote );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumNoteCodes;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the C_NOTES table

      /// <summary>
      /// Manually deserialize the contents of the received EnumCodedNotes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>An enumeration of received C_NOTE objects</returns>
      public EnumSelectedCodedNotes DeserializeSelectedCodedNotes( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.C_NOTES );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumSelectedCodedNotes enumNoteCodes = new EnumSelectedCodedNotes();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<SelectedCodedNote>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</SelectedCodedNote>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the media object
                  SelectedCodedNote codedNote =
                        PacketBase.DeserializeObject<SelectedCodedNote>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( codedNote != null )
                  {
                     enumNoteCodes.AddCodedNoteRecord( codedNote );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumNoteCodes;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the NOTES table

      /// <summary>
      /// Manually deserialize the contents of the received EnumNotes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>An enumeration of received NOTES objects</returns>
      public EnumNotes DeserializeNotes( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.NOTES );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumNotes enumNoteCodes = new EnumNotes();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<NOTE>" )
               {
                  // flag as being an element
                  isElement = true;

                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );
               }
               // .. or the end of the element
               else if ( streamXml == "</NOTE>" )
               {
                  // flag as not an element
                  isElement = false;

                  // complete the current node
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  NotesRecord note = PopulateNoteObject( xmlElement );

                  // add to the enumerated list
                  if ( note != null )
                  {
                     enumNoteCodes.AddNoteRecord( note );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumNoteCodes;

      }/* end method */


      /// <summary>
      /// Populate a single NOTE object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private NotesRecord PopulateNoteObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         NotesRecord note = new NotesRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            note.Uid = new Guid( Uid.InnerText );

         XmlNode NoteType = xmlRecord.SelectSingleNode( "NoteType" );
         if ( NoteType != null )
            note.NoteType = Convert.ToByte( NoteType.InnerText );

         XmlNode TextNote = xmlRecord.SelectSingleNode( "TextNote" );
         if ( TextNote != null )
            note.TextNote = Convert.ToString( TextNote.InnerText );

         XmlNode PointUid = xmlRecord.SelectSingleNode( "PointUid" );
         if ( PointUid != null )
            note.PointUid = new Guid( PointUid.InnerText );

         XmlNode Number = xmlRecord.SelectSingleNode( "Number" );
         if ( Number != null )
            note.Number = Convert.ToInt32( Number.InnerText );

         XmlNode CollectionStamp = xmlRecord.SelectSingleNode( "CollectionStamp" );
         if ( CollectionStamp != null )
            note.CollectionStamp = ParseInvariantDate( CollectionStamp );

         XmlNode LastModified = xmlRecord.SelectSingleNode( "LastModified" );
         if ( LastModified != null )
            note.LastModified = ParseInvariantDate( LastModified );

         XmlNode OperId = xmlRecord.SelectSingleNode( "OperId" );
         if ( OperId != null )
            note.OperId = Convert.ToInt32( OperId.InnerText );

         // return the point object
         return note;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the OPERATORS table

      /// <summary>
      /// Manually deserialize the contents of the received EnumOperators
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>An enumeration of received Operator objects</returns>
      public EnumOperators DeserializeOperators( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.OPERATORS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumOperators enumOperators = new EnumOperators();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<OPERATOR>" )
               {
                  // flag as being an element
                  isElement = true;

                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );
               }
               // .. or the end of the element
               else if ( streamXml == "</OPERATOR>" )
               {
                  // flag as not an element
                  isElement = false;

                  // complete the current node
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  OperatorRecord miOperator = PopulateOperatorObject( xmlElement );

                  // add to the enumerated list
                  if ( miOperator != null )
                  {
                     enumOperators.AddRecord( miOperator );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumOperators;

      }/* end method */


      /// <summary>
      /// Populate a single OPERATOR object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private OperatorRecord PopulateOperatorObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         OperatorRecord oper = new OperatorRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode OperId = xmlRecord.SelectSingleNode( "OperId" );
         if ( OperId != null )
            oper.OperId = Convert.ToInt32( OperId.InnerText );

         XmlNode OperName = xmlRecord.SelectSingleNode( "OperName" );
         if ( OperName != null )
            oper.OperName = Convert.ToString( OperName.InnerText );

         XmlNode DadType = xmlRecord.SelectSingleNode( "DadType" );
         if ( DadType != null )
            oper.DadType = Convert.ToInt32( DadType.InnerText );

         XmlNode AccessLevel = xmlRecord.SelectSingleNode( "AccessLevel" );
         if ( AccessLevel != null )
            oper.AccessLevel = Get_Access_Level( AccessLevel );

         XmlNode CanChangePassword = xmlRecord.SelectSingleNode( "CanChangePassword" );
         if ( CanChangePassword != null )
            oper.CanChangePassword = Convert.ToBoolean( CanChangePassword.InnerText );

         XmlNode PasswordChanged = xmlRecord.SelectSingleNode( "PasswordChanged" );
         if ( PasswordChanged != null )
            oper.PasswordChanged = Convert.ToBoolean( PasswordChanged.InnerText );

         XmlNode CreateWorkNotification = xmlRecord.SelectSingleNode( "CreateWorkNotification" );
         if ( CreateWorkNotification != null )
            oper.CreateWorkNotification = Convert.ToBoolean( CreateWorkNotification.InnerText );

         XmlNode DataCollectionTime = xmlRecord.SelectSingleNode( "DataCollectionTime" );
         if ( DataCollectionTime != null )
            oper.DataCollectionTime = Convert.ToInt32( DataCollectionTime.InnerText );

         XmlNode Password = xmlRecord.SelectSingleNode( "Password" );
         if ( Password != null )
            oper.Password = Convert.ToString( Password.InnerText );

         XmlNode ResetPassword = xmlRecord.SelectSingleNode( "ResetPassword" );
         if ( ResetPassword != null )
            oper.ResetPassword = Get_Reset_Password( ResetPassword );

         XmlNode ScanRequiredToCollect = xmlRecord.SelectSingleNode( "ScanRequiredToCollect" );
         if ( ScanRequiredToCollect != null )
            oper.ScanRequiredToCollect = Convert.ToBoolean( ScanRequiredToCollect.InnerText );

         XmlNode ShowPreData = xmlRecord.SelectSingleNode( "ShowPreData" );
         if ( ShowPreData != null )
            oper.ShowPreData = Convert.ToBoolean( ShowPreData.InnerText );

         XmlNode VerifyMode = xmlRecord.SelectSingleNode( "VerifyMode" );
         if ( VerifyMode != null )
            oper.VerifyMode = Get_Verification_Mode( VerifyMode );

         XmlNode ViewOverdueOnly = xmlRecord.SelectSingleNode( "ViewOverdueOnly" );
         if ( ViewOverdueOnly != null )
            oper.ViewOverdueOnly = Get_Overdue_Filter( ViewOverdueOnly );

         XmlNode ViewTreeElement = xmlRecord.SelectSingleNode( "ViewTreeElement" );
         if ( ViewTreeElement != null )
            oper.ViewTreeElement = Get_View_Tree_As( ViewTreeElement );

         XmlNode ViewCMMSWorkHistory = xmlRecord.SelectSingleNode( "ViewCMMSWorkHistory" );
         if ( ViewCMMSWorkHistory != null )
            oper.ViewCMMSWorkHistory = Convert.ToBoolean( ViewCMMSWorkHistory.InnerText );

         XmlNode NumericRangeProtection = xmlRecord.SelectSingleNode( "NumericRangeProtection" );
         if ( NumericRangeProtection != null )
            oper.NumericRangeProtection = Convert.ToBoolean( NumericRangeProtection.InnerText );

         XmlNode ScanAndGoToFirstPoint = xmlRecord.SelectSingleNode( "ScanAndGoToFirstPoint" );
         if ( ScanAndGoToFirstPoint != null )
            oper.ScanAndGoToFirstPoint = Convert.ToBoolean( ScanAndGoToFirstPoint.InnerText );

         XmlNode ViewSpectralFFT = xmlRecord.SelectSingleNode( "ViewSpectralFFT" );
         if ( ViewSpectralFFT != null )
            oper.ViewSpectralFFT = Convert.ToBoolean( ViewSpectralFFT.InnerText );

         XmlNode EnableCamera = xmlRecord.SelectSingleNode( "EnableCamera" );
         if( EnableCamera != null )
            oper.EnableCamera = Convert.ToBoolean( EnableCamera.InnerText );

         XmlNode BarcodeLogin = xmlRecord.SelectSingleNode( "BarcodeLogin" );
         if( EnableCamera != null )
            oper.BarcodeLogin = Convert.ToString( BarcodeLogin.InnerText ).Trim( );

         XmlNode BarcodePassword = xmlRecord.SelectSingleNode( "BarcodePassword" );
         if( BarcodePassword != null )
            oper.BarcodePassword = Convert.ToBoolean( BarcodePassword.InnerText );

         // return the point object
         return oper;

      }/* end method */


      /// <summary>
      /// Get the ResetPassword enumerated type from the raw Xml Node
      /// </summary>
      /// <param name="iNode">Xml Note to reference</param>
      /// <returns>enumerated type</returns>
      private ResetPassword Get_Reset_Password( XmlNode iNode )
      {
         string mode = Convert.ToString( iNode.InnerText ).Trim();
         if ( mode == ResetPassword.Set.ToString() )
         {
            return ResetPassword.Set;
         }
         else if ( mode == ResetPassword.Reset.ToString() )
         {
            return ResetPassword.Reset;
         }
         else
         {
            return ResetPassword.None;
         }
      }


      /// <summary>
      /// Get the ViewTreeAs enumerated type from the raw Xml Node
      /// </summary>
      /// <param name="iNode">Xml Note to reference</param>
      /// <returns>enumerated type</returns>
      private ViewTreeAs Get_View_Tree_As( XmlNode iNode )
      {
         string viewAs = Convert.ToString( iNode.InnerText ).Trim();
         if ( viewAs == ViewTreeAs.UserDefined.ToString() )
         {
            return ViewTreeAs.UserDefined;
         }
         else if ( viewAs == ViewTreeAs.SetAndPoint.ToString() )
         {
            return ViewTreeAs.SetAndPoint;
         }
         else if ( viewAs == ViewTreeAs.PointOnly.ToString() )
         {
            return ViewTreeAs.PointOnly;
         }
         else if ( viewAs == ViewTreeAs.MachineAndPoint.ToString() )
         {
            return ViewTreeAs.MachineAndPoint;
         }
         else
         {
            return ViewTreeAs.All;
         }
      }


      /// <summary>
      /// Get the OverdueFilter enumerated type from the raw Xml Node
      /// </summary>
      /// <param name="iNode">Xml Note to reference</param>
      /// <returns>enumerated type</returns>
      private OverdueFilter Get_Overdue_Filter( XmlNode iNode )
      {
         string mode = Convert.ToString( iNode.InnerText ).Trim();
         if ( mode == OverdueFilter.UserDefined.ToString() )
         {
            return OverdueFilter.UserDefined;
         }
         else if ( mode == OverdueFilter.On.ToString() )
         {
            return OverdueFilter.On;
         }
         else
         {
            return OverdueFilter.Off;
         }
      }


      /// <summary>
      /// Get the VerificationMode enumerated type from the raw Xml Node
      /// </summary>
      /// <param name="iNode">Xml Note to reference</param>
      /// <returns>enumerated type</returns>
      private VerificationMode Get_Verification_Mode( XmlNode iNode )
      {
         string mode = Convert.ToString( iNode.InnerText ).Trim();
         if ( mode == VerificationMode.OnAlarm.ToString() )
         {
            return VerificationMode.OnAlarm;
         }
         else if ( mode == VerificationMode.Never.ToString() )
         {
            return VerificationMode.Never;
         }
         else
         {
            return VerificationMode.Always;
         }
      }


      /// <summary>
      /// Get the OperatorRights enumerated type from the raw Xml Node
      /// </summary>
      /// <param name="iNode">Xml Note to reference</param>
      /// <returns>enumerated type</returns>
      private OperatorRights Get_Access_Level( XmlNode iNode )
      {
         string oprights = Convert.ToString( iNode.InnerText ).Trim();
         if ( oprights == OperatorRights.Admin.ToString() )
         {
            return OperatorRights.Admin;
         }
         else if ( oprights == OperatorRights.Full.ToString() )
         {
            return OperatorRights.Full;
         }
         else if ( oprights == OperatorRights.Limited.ToString() )
         {
            return OperatorRights.Limited;
         }
         else if ( oprights == OperatorRights.Operator.ToString() )
         {
            return OperatorRights.Operator;
         }
         else
         {
            return OperatorRights.Review;
         }
      }

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the MEASUREMENT tables

      /// <summary>
      /// Manually deserialize the contents of the received EnumCodedNotes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <param name="iTableName">what measurement table</param>
      /// <returns>An enumeration of received C_NOTE objects</returns>
      public EnumSavedMeasurements DeserializeSavedMeasurements( int iTotalRecords,
         TableName iTableName )
      {
         if ( iTableName != TableName.TEMPDATA &
              iTableName != TableName.VELDATA &
              iTableName != TableName.ENVDATA )
         {
            return null;
         }

         // get the cache file name
         string fileName = CacheFile( iTableName );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of objects
         EnumSavedMeasurements enumMeasurements = new EnumSavedMeasurements();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               if ( streamXml.StartsWith( "<Measurement" ) )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  SavedMeasurement measurement = PopulateMeasurementObject( xmlElement );

                  // add to the enumerated list
                  if ( measurement != null )
                  {
                     enumMeasurements.AddMeasurement( measurement );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumMeasurements;

      }/* end method */


      /// <summary>
      /// Populate a single saved measurement object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private SavedMeasurement PopulateMeasurementObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         SavedMeasurement measurement = new SavedMeasurement();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode New = xmlRecord.Attributes.GetNamedItem( "New" );
         if ( New != null )
            measurement.New = Convert.ToBoolean( New.InnerText );

         XmlNode PointUid = xmlRecord.Attributes.GetNamedItem( "PointUid" );
         if ( PointUid != null )
            measurement.PointUid = new Guid( PointUid.InnerText );

         XmlNode PointHostIndex = xmlRecord.Attributes.GetNamedItem( "PointHostIndex" );
         if ( PointHostIndex != null )
            measurement.PointHostIndex = Convert.ToInt32( PointHostIndex.InnerText );

         XmlNode AlarmState = xmlRecord.Attributes.GetNamedItem( "AlarmState" );
         if ( AlarmState != null )
            measurement.AlarmState = Convert.ToByte( AlarmState.InnerText );

         XmlNode Status = xmlRecord.Attributes.GetNamedItem( "Status" );
         if ( Status != null )
            measurement.Status = Convert.ToByte( Status.InnerText );

         XmlNode OperId = xmlRecord.Attributes.GetNamedItem( "OperId" );
         if ( OperId != null )
            measurement.OperId = Convert.ToInt32( OperId.InnerText );

         XmlNode LocalTime = xmlRecord.Attributes.GetNamedItem( "LocalTime" );
         if ( LocalTime != null )
            measurement.LocalTime = ParseInvariantDate( LocalTime );

         XmlNode TextValue = xmlRecord.Attributes.GetNamedItem( "TextValue" );
         if ( TextValue != null && TextValue.InnerText != string.Empty )
            measurement.TextValue = TextValue.InnerText;

         XmlNode Value = xmlRecord.Attributes.GetNamedItem( "Value" );
         if ( Value != null && measurement.TextValue == string.Empty )
            measurement.Value = Convert.ToDouble( Value.InnerText );

         // return the point object
         return measurement;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the STRUCTURED table

      /// <summary>
      /// Manually deserialize the contents of the received EnumStructureds
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received STRUCTURED objects</returns>
      public EnumStructuredRoutes DeserializeStructuredRoutes( byte[] iXmlByteBuffer )
      {
         EnumStructuredRoutes structuredRoutes = new EnumStructuredRoutes();

         // convert the byte buffer to a populated Xml document object
         XmlDocument xmlRef = WriteBufferToXmlDocument( iXmlByteBuffer );

         // reference the Node Table
         XmlNode tableData = xmlRef.ChildNodes[ 1 ].ChildNodes[ 1 ];

         // how many records in total?
         int totalRecords = tableData.ChildNodes.Count;

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = totalRecords;

         // process all records
         foreach ( XmlNode xmlRecord in tableData )
         {
            // deserialize the media object
            StructuredRecord structuredRoute =
                        PacketBase.DeserializeObject<StructuredRecord>( xmlRecord.OuterXml );

            // add to the enumerated list
            if ( structuredRoute != null )
               structuredRoutes.AddStructuredRecord( structuredRoute );

            // feedback please!
            mProgressPosition.Update( true );
         }

         return structuredRoutes;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the MESSAGINGPREFS table

      /// <summary>
      /// Manually deserialize the contents of the received EnumMessagingPrefs
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumMessagingPrefs DeserializeMessagingPrefs( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.MESSAGINGPREFS );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumMessagingPrefs enumMessagePrefs = new EnumMessagingPrefs();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<MESSAGINGPREFS>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</MESSAGINGPREFS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );

                  // deserialize the media object
                  MessagingPrefsRecord messagePref =
                        PacketBase.DeserializeObject<MessagingPrefsRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( messagePref != null )
                  {
                     enumMessagePrefs.AddRecord( messagePref );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumMessagePrefs;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the MACHINESKIPS tables

      /// <summary>
      /// Manually deserialize the contents of the received EnumMessagingPrefs
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumMachineSkips DeserializeMachineSkips( int iTotalRecords, TableName iTableName )
      {
         // make sure we're referencing the correct tables
         if ( iTableName != TableName.MACHINENOPSKIPS &
              iTableName != TableName.MACHINEOKSKIPS )
         {
            return null;
         }

         // get the cache file name
         string fileName = CacheFile( iTableName );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumMachineSkips enumMachineSkips = new EnumMachineSkips();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<MACHINESKIPS>" )
               {
                  // flag as being an element
                  isElement = true;

                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );
               }
               // .. or the end of the element
               else if ( streamXml == "</MACHINESKIPS>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  MachineSkipRecord machineSkip = PopulateMachineSkipObject( xmlElement );

                  // add to the enumerated list
                  if ( machineSkip != null )
                  {
                     enumMachineSkips.AddRecord( machineSkip );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumMachineSkips;

      }/* end method */


      /// <summary>
      /// Populate a single MACHINESKIP object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private MachineSkipRecord PopulateMachineSkipObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         MachineSkipRecord skip = new MachineSkipRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode MCD = xmlRecord.SelectSingleNode( "MCD" );
         if ( MCD != null )
            skip.MCD = Convert.ToBoolean( MCD.InnerText );

         XmlNode Process = xmlRecord.SelectSingleNode( "Process" );
         if ( Process != null )
            skip.Process = Convert.ToBoolean( Process.InnerText );

         XmlNode Inspection = xmlRecord.SelectSingleNode( "Inspection" );
         if ( Inspection != null )
            skip.Inspection = Convert.ToBoolean( Inspection.InnerText );

         XmlNode OperId = xmlRecord.SelectSingleNode( "OperId" );
         if ( OperId != null )
            skip.OperId = Convert.ToInt32( OperId.InnerText );

         // return the point object
         return skip;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the USERPREFERENCES tables

      /// <summary>
      /// Manually deserialize the contents of the received EnumMessagingPrefs
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumUserPreferences DeserializeUserPreferences( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.USERPREFERENCES );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of objects
         EnumUserPreferences enumUserPreferences = new EnumUserPreferences();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<UserPreferencesRecord>" )
               {
                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</UserPreferencesRecord>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text
                  xmlElement.Append( streamXml );

                  // deserialize the media object
                  UserPreferencesRecord userPreference = 
                     PacketArchive.DeserializeObject<UserPreferencesRecord>( xmlElement.ToString() );

                  // add to the enumerated list
                  if ( userPreference != null )
                  {
                     enumUserPreferences.AddRecord( userPreference );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumUserPreferences;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the WORKOTIFICATIONS table

      /// <summary>
      /// Manually deserialize the contents of the received WorkNotifications
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumCmmsWorkNotifications DeserializeWorkNotifications( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.WORKNOTIFICATION );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCmmsWorkNotifications workNotifications = new EnumCmmsWorkNotifications();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of an element
               if ( streamXml == "<WorkNotification>" )
               {
                  // flag as being an element
                  isElement = true;

                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );
               }
               // .. or the end of the element
               else if ( streamXml == "</WorkNotification>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  CmmsWorkNotification workNotification = PopulateWorkNotification( xmlElement );

                  // add to the enumerated list
                  if ( workNotification != null )
                  {
                     workNotifications.AddWorkNotificationRecord( workNotification );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return workNotifications;

      }/* end method */


      /// <summary>
      /// Populate a single WorkNotification object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private CmmsWorkNotification PopulateWorkNotification( StringBuilder iXmlElement )
      {
         // create a new point record object
         CmmsWorkNotification notification = new CmmsWorkNotification();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode PRISMNumber = xmlRecord.SelectSingleNode( "PRISMNumber" );
         if ( PRISMNumber != null )
            notification.PRISM = Convert.ToInt32( PRISMNumber.InnerText );

         XmlNode WorkNotificationID = xmlRecord.SelectSingleNode( "WorkNotificationID" );
         if ( WorkNotificationID != null )
            notification.WorkNotificationID = Convert.ToInt32( WorkNotificationID.InnerText );

         XmlNode LocationText = xmlRecord.SelectSingleNode( "LocationText" );
         if ( LocationText != null )
            notification.LocationText = Convert.ToString( LocationText.InnerText );

         XmlNode WNTimeStamp = xmlRecord.SelectSingleNode( "WNTimeStamp" );
         if ( WNTimeStamp != null )
            notification.WNTimeStamp = ParseInvariantDate( WNTimeStamp );

         XmlNode OperatorID = xmlRecord.SelectSingleNode( "OperatorID" );
         if ( OperatorID != null )
            notification.OperatorID = Convert.ToInt32( OperatorID.InnerText );

         XmlNode CompletionDate = xmlRecord.SelectSingleNode( "CompletionDate" );
         if ( CompletionDate != null )
            notification.CompletionDate = ParseInvariantDate( CompletionDate );

         XmlNode WorkTypeIDs = xmlRecord.SelectSingleNode( "WorkTypeIDs" );
         if ( WorkTypeIDs != null )
            notification.WorkTypeIDs = Convert.ToString( WorkTypeIDs.InnerText );

         XmlNode PriorityID = xmlRecord.SelectSingleNode( "PriorityID" );
         if ( PriorityID != null )
            notification.PriorityID = Convert.ToInt32( PriorityID.InnerText );

         XmlNode ProblemID = xmlRecord.SelectSingleNode( "ProblemID" );
         if ( ProblemID != null )
            notification.ProblemID = Convert.ToInt32( ProblemID.InnerText );

         XmlNode ProblemDescription = xmlRecord.SelectSingleNode( "ProblemDescription" );
         if ( ProblemDescription != null )
            notification.ProblemDescription = Convert.ToString( ProblemDescription.InnerText );

         XmlNode CorrectiveActionID = xmlRecord.SelectSingleNode( "CorrectiveActionID" );
         if ( CorrectiveActionID != null )
            notification.CorrectiveActionID = Convert.ToInt32( CorrectiveActionID.InnerText );

         XmlNode CorrectiveDescription = xmlRecord.SelectSingleNode( "CorrectiveDescription" );
         if ( CorrectiveDescription != null )
            notification.CorrectiveDescription = Convert.ToString( CorrectiveDescription.InnerText );

         XmlNode KeyField = xmlRecord.SelectSingleNode( "KeyField" );
         if ( KeyField != null )
            notification.KeyField = Convert.ToByte( KeyField.InnerText );

         XmlNode NodeUid = xmlRecord.SelectSingleNode( "NodeUid" );
         if ( NodeUid != null )
            notification.NodeUid = new Guid( NodeUid.InnerText );

         XmlNode RouteID = xmlRecord.SelectSingleNode( "RouteID" );
         if ( RouteID != null )
            notification.RouteID = Convert.ToInt32( RouteID.InnerText );

         XmlNode Status = xmlRecord.SelectSingleNode( "Status" );
         if ( Status != null )
            notification.Status = Convert.ToByte( Status.InnerText );

         XmlNode MachineID = xmlRecord.SelectSingleNode( "MachineID" );
         if ( MachineID != null )
            notification.MachineID = Convert.ToInt32( MachineID.InnerText );

         XmlNode FixByDateSet = xmlRecord.SelectSingleNode( "FixByDateSet" );
         if ( FixByDateSet != null )
            notification.FixByDateSet = Convert.ToBoolean( FixByDateSet.InnerText );

         // return the point object
         return notification;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the PROBLEMS table

      /// <summary>
      /// Manually deserialize the contents of the received EnumCmmsProblems
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumCmmsProblems DeserializeProblems( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.PROBLEM );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCmmsProblems enumProblems = new EnumCmmsProblems();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               if ( streamXml.StartsWith( "<Problem" ) )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  CmmsProblemRecord problem = PopulateCmmsProblemsObject( xmlElement );

                  // add to the enumerated list
                  if ( problem != null )
                  {
                     enumProblems.AddProblemRecord( problem );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumProblems;

      }/* end method */


      /// <summary>
      /// Populate a single Cmms Problems object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private CmmsProblemRecord PopulateCmmsProblemsObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         CmmsProblemRecord measurement = new CmmsProblemRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode MAProbDescKey = xmlRecord.Attributes.GetNamedItem( "MAProbDescKey" );
         if ( MAProbDescKey != null )
            measurement.MAProblemDescriptionKey = Convert.ToInt32( MAProbDescKey.InnerText );

         XmlNode ProbText = xmlRecord.Attributes.GetNamedItem( "ProbText" );
         if ( ProbText != null )
            measurement.ProblemText = Convert.ToString( ProbText.InnerText );

         // return the point object
         return measurement;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the PRIORITIES table

      /// <summary>
      /// Manually deserialize the contents of the received EnumCmmsPriorities
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumCmmsPriorities DeserializePriorities( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.PRIORITY );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCmmsPriorities enumProblems = new EnumCmmsPriorities();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               if ( streamXml.StartsWith( "<Priority" ) )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  CmmsPriorityRecord problem = PopulateCmmsPriorityObject( xmlElement );

                  // add to the enumerated list
                  if ( problem != null )
                  {
                     enumProblems.AddPriorityRecord( problem );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumProblems;

      }/* end method */


      /// <summary>
      /// Populate a single Cmms Priority object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private CmmsPriorityRecord PopulateCmmsPriorityObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         CmmsPriorityRecord priority = new CmmsPriorityRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode MAPriorityKey = xmlRecord.Attributes.GetNamedItem( "MAPriorityKey" );
         if ( MAPriorityKey != null )
            priority.MAPriorityKey = Convert.ToInt32( MAPriorityKey.InnerText );

         XmlNode MAPriorityCode = xmlRecord.Attributes.GetNamedItem( "MAPriorityCode" );
         if ( MAPriorityKey != null )
            priority.MAPriorityCode = Convert.ToString( MAPriorityCode.InnerText );

         XmlNode PriorityText = xmlRecord.Attributes.GetNamedItem( "PriorityText" );
         if ( PriorityText != null )
            priority.PriorityText = Convert.ToString( PriorityText.InnerText );

         // return the point object
         return priority;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the WORKTYPE table

      /// <summary>
      /// Manually deserialize the contents of the received EnumCmmsWorkTypes
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumCmmsWorkTypes DeserializeWorkTypes( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.WORKTYPE );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCmmsWorkTypes enumWorkTypes = new EnumCmmsWorkTypes();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               if ( streamXml.StartsWith( "<WorkType" ) )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  CmmsWorkTypeRecord workType = PopulateCmmsWorkTypeObject( xmlElement );

                  // add to the enumerated list
                  if ( workType != null )
                  {
                     enumWorkTypes.AddWorkTypeRecord( workType );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumWorkTypes;

      }/* end method */


      /// <summary>
      /// Populate a single Cmms Work Type object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private CmmsWorkTypeRecord PopulateCmmsWorkTypeObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         CmmsWorkTypeRecord workType = new CmmsWorkTypeRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode MAWorkTypeKey = xmlRecord.Attributes.GetNamedItem( "MAWorkTypeKey" );
         if ( MAWorkTypeKey != null )
            workType.MAWorkTypeKey = Convert.ToInt32( MAWorkTypeKey.InnerText );

         XmlNode WorkTypeText = xmlRecord.Attributes.GetNamedItem( "WorkTypeText" );
         if ( WorkTypeText != null )
            workType.WorkTypeText = Convert.ToString( WorkTypeText.InnerText );

         // return the point object
         return workType;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the CORRECTIVEACTION table

      /// <summary>
      /// Manually deserialize the contents of the received EnumCmmsCorrectiveActions
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumCmmsCorrectiveActions DeserializeCorrectiveActions( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.CORRECTIVEACTION );

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of Derived Point objects
         EnumCmmsCorrectiveActions enumCorrectiveActions = new EnumCmmsCorrectiveActions();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               if ( streamXml.StartsWith( "<CorrectiveAction" ) )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // deserialize the media object
                  CmmsCorrectiveActionRecord correctiveAction = 
                     PopulateCmmsCorrectiveActionObject( xmlElement );

                  // add to the enumerated list
                  if ( correctiveAction != null )
                  {
                     enumCorrectiveActions.AddCorrectiveActionRecord( correctiveAction );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated points list
         return enumCorrectiveActions;

      }/* end method */


      /// <summary>
      /// Populate a single Cmms Corrective Action object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private CmmsCorrectiveActionRecord PopulateCmmsCorrectiveActionObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         CmmsCorrectiveActionRecord correctiveAction = new CmmsCorrectiveActionRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode MACorrActnKey = xmlRecord.Attributes.GetNamedItem( "MACorrActnKey" );
         if ( MACorrActnKey != null )
            correctiveAction.MACorrectiveActionKey = Convert.ToInt32( MACorrActnKey.InnerText );

         XmlNode CorrActText = xmlRecord.Attributes.GetNamedItem( "CorrActText" );
         if ( CorrActText != null )
            correctiveAction.CorrectiveActionText = Convert.ToString( CorrActText.InnerText );

         // return the point object
         return correctiveAction;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Deserialize XML for the SAVEDMEDIA tables

      /// <summary>
      /// Manually deserialize the contents of the received EnumCmmsCorrectiveActions
      /// object and return feedback events on each record processed
      /// </summary>
      /// <param name="iXmlByteBuffer">Decompressed Xml Data</param>
      /// <returns>An enumeration of received objects</returns>
      public EnumSavedMedia DeserializeSavedMedia( int iTotalRecords )
      {
         // get the cache file name
         string fileName = CacheFile( TableName.SAVEDMEDIA);

         // if no cachee file exists then abort here
         if ( !File.Exists( @fileName ) )
         {
            return null;
         }

         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         EnumSavedMedia enumSavedMedia = new EnumSavedMedia();

         // set the progress states for this action
         mProgressPosition.Reset();
         mProgressPosition.NoOfItemsToProcess = iTotalRecords;

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( @fileName ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // is this the start of a Point element
               if ( streamXml == "<SavedMedia>" )
               {
                  // add the header elements
                  xmlElement.Append( hdr1 );
                  xmlElement.Append( hdr2 );

                  // flag as being an element
                  isElement = true;
               }
               // .. or the end of the element
               else if ( streamXml == "</SavedMedia>" )
               {
                  // flag as not an element
                  isElement = false;

                  // append the element text and terminate with ACK
                  xmlElement.Append( streamXml );
                  xmlElement.Append( "</ACK>" );

                  // populate a point object using this element
                  SavedMediaRecord savedMedia = PopulateSavedMediaObject( xmlElement );

                  // and update the enumerated points list
                  if ( savedMedia != null )
                  {
                     enumSavedMedia.AddMediaRecord( savedMedia );
                     mProgressPosition.Update( true );
                  }
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;

         // delete the cache file
         File.Delete( @fileName );

         // return the penumerated process list
         return enumSavedMedia;

      }/* end method */


      /// <summary>
      /// Populate a single Saved Media object
      /// </summary>
      /// <param name="iXmlElement"></param>
      /// <returns></returns>
      private SavedMediaRecord PopulateSavedMediaObject( StringBuilder iXmlElement )
      {
         // create a new point record object
         SavedMediaRecord mediaRecord = new SavedMediaRecord();

         // create an xml document object
         XmlDocument xmlRef = new XmlDocument();

         // load the xml object into it
         xmlRef.LoadXml( iXmlElement.ToString() );

         // extract the inner xml
         XmlNode xmlRecord = xmlRef.LastChild.LastChild;

         XmlNode Uid = xmlRecord.SelectSingleNode( "Uid" );
         if ( Uid != null )
            mediaRecord.Uid = new Guid( Uid.InnerText );

         XmlNode PointHostIndex = xmlRecord.SelectSingleNode( "PointHostIndex" );
         if ( PointHostIndex != null )
            mediaRecord.PointHostIndex = Convert.ToInt32( PointHostIndex.InnerText );

         XmlNode TimeStamp = xmlRecord.SelectSingleNode( "TimeStamp" );
         if ( TimeStamp != null )
            mediaRecord.TimeStamp = ParseInvariantDate( TimeStamp );

         XmlNode MediaType = xmlRecord.SelectSingleNode( "MediaType" );
         if ( MediaType != null )
            mediaRecord.MediaType = Convert.ToByte( MediaType.InnerText );

         XmlNode MediaOrigin = xmlRecord.SelectSingleNode( "MediaOrigin" );
         if ( MediaOrigin != null )
            mediaRecord.MediaOrigin = Convert.ToByte( MediaOrigin.InnerText );

         XmlNode WNID = xmlRecord.SelectSingleNode( "WNID" );
         if ( WNID != null )
            mediaRecord.WNID = Convert.ToInt32( WNID.InnerText );

         XmlNode FileName = xmlRecord.SelectSingleNode( "FileName" );
         if ( FileName != null )
            mediaRecord.FileName = FileName.InnerText;

         XmlNode UTM = xmlRecord.SelectSingleNode( "UTM" );
         if ( UTM != null )
            mediaRecord.UTM = UTM.InnerText;

         XmlNode Note = xmlRecord.SelectSingleNode( "Note" );
         if ( Note != null )
            mediaRecord.Note = Note.InnerText;

         XmlNode OperId = xmlRecord.SelectSingleNode( "OperId" );
         if ( OperId != null )
            mediaRecord.OperId = Convert.ToInt32( OperId.InnerText );

         XmlNode Size = xmlRecord.SelectSingleNode( "Size" );
         if ( Size != null )
            mediaRecord.Size = Convert.ToInt32( Size.InnerText );

         XmlNode CRC32 = xmlRecord.SelectSingleNode( "CRC32" );
         if ( CRC32 != null )
            mediaRecord.CRC32 = CRC32.InnerText;

         XmlNode FilePath = xmlRecord.SelectSingleNode( "FilePath" );
         if ( FilePath != null )
            mediaRecord.FilePath = FilePath.InnerText;

         XmlNode Uploaded = xmlRecord.SelectSingleNode( "Uploaded" );
         if ( Uploaded != null )
            mediaRecord.Uploaded = Convert.ToBoolean( Uploaded.InnerText );

         // return the point object
         return mediaRecord;

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region Private text field support methods

      /// <summary>
      /// Re-entetize line feed handling as the .Net CF deserializer
      /// strips away the '/r' from any string containing '/r/n'
      /// </summary>
      /// <param name="iRawString">raw string missing \r char</param>
      /// <returns>string with \r char reinstated</returns>
      private string EntitizeLineHandling( string iRawString )
      {
         StringBuilder sbFixed = new StringBuilder();

         foreach ( char c in iRawString )
         {
            if ( c == LF )
            {
               sbFixed.Append( CR );
               sbFixed.Append( LF );
            }
            else
            {
               sbFixed.Append( c );
            }
         }

         return sbFixed.ToString();

      }/* end method */


      /// <summary>
      /// Reinstate any CR or LF from the body text that
      /// will have been stripped away by the trim command
      /// This addresses OTD #3866
      /// </summary>
      /// <param name="iStreamXml">unfiltered string</param>
      /// <param name="iIsTextBody">is this a textbody element?</param>
      /// <param name="iElementStart">element brace start</param>
      /// <param name="iElementEnd">element brace end</param>
      private static void ReinstateUnicodeCRLF( ref string iStreamXml,
         ref bool iIsTextBody, string iElementStart, string iElementEnd )
      {
         // does this line contain the start of the TextBody block?
         if ( iStreamXml.Contains( iElementStart ) )
         {
            iIsTextBody = true;
         }

         // does this line contain the end of the TextBody block?
         if ( iStreamXml.Contains( iElementEnd ) )
         {
            iIsTextBody = false;
         }

         //
         // only reinstate the CR and LF if we have body text in 
         // this line and not just the XML element block title
         //
         if ( iIsTextBody && iStreamXml != iElementStart )
         {
            iStreamXml = iStreamXml + CR + LF;
         }

      }/* end method */

      #endregion

      //------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  XmlDeserializer.cs
//
//  Revision 6.0 APinkerton 8th November 2012
//  Add support for SAVEDMEDIA type
//
//  Revision 5.0 APinkerton 17th September 2012
//  Add support for TextData POINT type
//
//  Revision 4.0 APinkerton 15th July 2012
//  Add deserialization support for archived tables
//
//  Revision 3.0 APinkerton 16th March 2012
//  Fix for OTD #3866 which stripped away the /r character from /r/n
//
//  Revision 2.0 APinkerton 15th October 2010
//  Re-engineer "large data" methods to allow for 10,000 point downloads
//
//  Revision 1.0 APinkerton 24th November 2009
//  Add support for invariant culture
//
//  Revision 0.0 APinkerton 23rd March 2009
//  Add to project
//----------------------------------------------------------------------------
