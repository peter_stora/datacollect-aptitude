﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class for the enumeration of Cmms Work Notification
    /// upload objects (i.e. notifications created by operators
    /// on the device itself)
    /// </summary>
    public class EnumCmmsUploadNotificationRecords
    {
        protected List<CmmsWorkNotificationBase> mWorkNotificationUploads;
        protected int mIndex = -1;


        /// <summary>
        /// Constructor
        /// </summary>
        public EnumCmmsUploadNotificationRecords()
        {
            mWorkNotificationUploads = new List<CmmsWorkNotificationBase>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw CMMS Work Notification object list.
        /// </summary>
        [XmlElement("WorkNotification")]
        public List<CmmsWorkNotificationBase> WorkNotification
        {
            get { return mWorkNotificationUploads; }
            set { mWorkNotificationUploads = value; }
        }


        /// <summary>
        /// Add a new CMMS Work Notification record to the list
        /// </summary>
        /// <param name="item">populated CMMS Priority record object</param>
        /// <returns>number of CMMS Priority objects available</returns>
        public int AddRecord(CmmsWorkNotificationBase item)
        {
            mWorkNotificationUploads.Add(item);
            return mWorkNotificationUploads.Count;
        }/* end method */


        /// <summary>
        /// How many CMMS Work Notification records are now available
        /// </summary>
        public int Count { get { return mWorkNotificationUploads.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumNodeRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumCmmsUploadNotifications : EnumCmmsUploadNotificationRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mWorkNotificationUploads.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mWorkNotificationUploads[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (10:15)
//  Add to project
//----------------------------------------------------------------------------