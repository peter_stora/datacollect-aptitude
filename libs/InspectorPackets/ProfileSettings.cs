﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Device Profile object from Middleware Device Server - minus the
   /// device authorization key!
   /// </summary>
   public class ProfileSettings : PacketBase
   {
      /// <summary>
      /// private firmware version field
      /// </summary>
      private HwVersion mFirmware = new HwVersion();
      
      /// <summary>
      /// Private Device TimeZone local settings
      /// </summary>
      private TimeZoneRecord mDeviceTimeZoneInfo = null;

      /// <summary>
      /// The actual ID of the target Device Profile
      /// </summary>
      public String ProfileId
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the the "Profile Friendly Name" (i.e. Mezanine Deck Crew)
      /// </summary>
      public String ProfileName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the "Device Friendly Name" (i.e. Unit23-045)
      /// </summary>
      public String DeviceName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets what Operator set assigned to the current profile
      /// </summary>
      public int OperatorSetId
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the profile associated with this 
      /// device need to be updated
      /// </summary>
      public Boolean ProfileChanged
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the device's authentication sub-key
      /// </summary>
      public String SubKey
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the latest Microlog Inspector Firmware 
      /// release that is available for download
      /// </summary>
      public HwVersion FirmwareAvailable
      {
         get
         {
            return mFirmware;
         }
         set
         {
            mFirmware = value;
         }
      }

      /// <summary>
      /// Gets or sets the last upload event (i.e. "was the upload received and processed")
      /// </summary>
      public String LastUploadEvent
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the device's time-zone information (location)
      /// </summary>
      public TimeZoneRecord DeviceTimeZoneInfo
      {
         get
         {
            return mDeviceTimeZoneInfo;
         }
         set
         {
            mDeviceTimeZoneInfo = value;
         }
      }

      /// <summary>
      /// Base constructor
      /// </summary>
      public ProfileSettings()
         : this( null )
      {
      }/* end constructor */

      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iVersion">Most recent version of MI available</param>
      public ProfileSettings( HwVersion iVersion )
      {
         this.SubKey = NULL_KEY;
         this.ProfileName = DEFAULT_PROFILE_NAME;
         this.ProfileId = DEFAULT_PROFILE_ID;

         if ( iVersion != null )
         {
            this.FirmwareAvailable.Major = iVersion.Major;
            this.FirmwareAvailable.Minor = iVersion.Minor;
            this.FirmwareAvailable.Build = iVersion.Build;
            this.FirmwareAvailable.Revision = iVersion.Revision;
         }
         else
         {
            this.FirmwareAvailable.Major = VERSION_MAJOR;
            this.FirmwareAvailable.Minor = VERSION_MINOR;
            this.FirmwareAvailable.Build = VERSION_BUILD;
         }

      }/* end constructor */

   }/* end class */


   /// <summary>
   /// Device Profile object from Middleware Device Server - with the required
   /// authorization key and DeviceUID. This class exists primarily to allow for
   /// "disconnected" device replication, and so consequently allows for the
   /// public visibility of both the Device UID and the Authorization key!
   /// </summary>
   public class DownloadProfile : ProfileSettings
   {
      /// <summary>
      /// Gets or sets the Authorization key
      /// </summary>
      public string AuthorizationKey
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Device UID
      /// </summary>
      public string DeviceUID
      {
         get;
         set;
      }

      /// <summary>
      /// Base constructor
      /// </summary>
      public DownloadProfile()
         : this( null )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iVersion">Most recent version of MI available</param>
      public DownloadProfile( string iMinimumFirmwareVersion )
      {
         this.AuthorizationKey = NULL_AUTHORIZATION;

         if ( iMinimumFirmwareVersion != null )
         {
            HwVersion version = new HwVersion( iMinimumFirmwareVersion );
            
            this.FirmwareAvailable.Major = version.Major;
            this.FirmwareAvailable.Minor = version.Minor;
            this.FirmwareAvailable.Build = version.Build;
            this.FirmwareAvailable.Revision = version.Revision;
         }
         else
         {
            this.FirmwareAvailable.Major = VERSION_MAJOR;
            this.FirmwareAvailable.Minor = VERSION_MINOR;
            this.FirmwareAvailable.Build = VERSION_BUILD;
            this.FirmwareAvailable.Revision = VERSION_REVISION;
         }

      }/* end constructor */

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.1 APinkerton 25th Feb 2010
//  Added the DeviceName and DeviceTimeZone properties. Also Changed the 
//  LastModified DateTime field to the Boolean ProfileChanged flag. 
//
//  Revision 0.0 APinkerton 2nd March 2009
//  Add to project
//----------------------------------------------------------------------------

