﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single TEXTDATASETUP table record entry
   /// </summary>
   public class TextDataRecord
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public TextDataRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Gets or sets the current UID (GUID) for this node. 
      /// Note: This value is derived and therefore independant of @ptitude
      /// </summary>
      public Guid Uid
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the hierarchy position of the current node. 
      /// Note: This value is derived and therefore independant of @ptitude
      /// </summary>
      public int Number
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the associated Point Uid (from Marlin POINT table)
      /// </summary>
      public Guid PointUid
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the maximum text length
      /// </summary>
      public byte MaxLength
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the stated text type
      /// </summary>
      public byte TextType
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether spaces are allowed
      /// </summary>
      public Boolean AllowSpaces
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether special characters are allowed
      /// </summary>
      public Boolean AllowSpecial
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether whether all text should be UPPER case only
      /// </summary>
      public Boolean UpperCase
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether whether Scan data should be used as text data
      /// </summary>
      public Boolean ScannerInput
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 10th September 2012
//  Add to project
//----------------------------------------------------------------------------