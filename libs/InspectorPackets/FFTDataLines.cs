﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Simple class that manages the processing of an integer array to an 
    /// array of double bytes. Note integer array values should not exceed
    /// the value 2^16 or be negative.
    /// </summary>
    public class FFTDataLines
    {
        private const int BYTES_INT32 = 4;
        private const int BYTES_INT16 = 2;

        private byte[] mInt32Bytes = new byte[BYTES_INT32];
        private byte[] mInt16Bytes = new byte[BYTES_INT16];
        private byte[] mResultArray = new byte[0];


        /// <summary>
        /// Converts an array of integers into an array of bytes, with each
        /// integer values saved as a two byte value. Note: If any integer 
        /// value exceeds 2^16, then the excess values will be ignored.
        /// </summary>
        /// <param name="iFFTValues">array if FFT integer values</param>
        /// <returns>array of processed bytes</returns>
        public byte[] ConvertToByteArray(int[] iFFTValues)
        {
            // clear the results array
            ByteArrayProcess.ClearByteBuffer(ref mResultArray);

            // process each of the integer value into two byte arrays
            foreach (int fftIntValue in iFFTValues)
            {
                mInt32Bytes = System.BitConverter.GetBytes(fftIntValue);
                ByteArrayProcess.ExtractArray(mInt32Bytes, ref mInt16Bytes, 0, BYTES_INT16);
                ByteArrayProcess.AppendToByteBuffer(mInt16Bytes, ref mResultArray);
            }

            // return the results array
            return mResultArray;

        }/* end method */




        public byte[] CopyArray(byte[] iFFTValues)
        {
            // clear the results array
            ByteArrayProcess.ClearByteBuffer(ref mResultArray);

            // append the new array values
            ByteArrayProcess.AppendToByteBuffer(iFFTValues, ref mResultArray);
       
            // return the results array
            return mResultArray;

        }/* end method */


    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------

