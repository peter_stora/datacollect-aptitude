﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Client returns a block of byte data to the Server
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketMediaBlock : PacketBase
   {
      byte[] mBlock = null;
      
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.MEDIA_BLOCK;

      /// <summary>
      /// Gets or sets the raw byte Block
      /// </summary>
      public byte[] Block
      {
         get
         {
            return mBlock;
         }
         set
         {
            mBlock = value;
         }
      }

      /// <summary>
      /// Gets or sets the End Of Record (EOR) state
      /// </summary>
      public Boolean EOR
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Media File UID
      /// </summary>
      public Guid UID
      {
         get;
         set;
      }

      /// <summary>
      /// Session Identifier
      /// </summary>
      public string SessionID
      {
         get;
         set;
      }

      /// <summary>
      /// Clear the read buffer and reset its internal size to zero
      /// </summary>
      /// <param name="refBuffer">call by reference array to be cleared</param>
      /// <returns>false on exception raised</returns>
      public void ClearBlock( )
      {
         try
         {
            if ( mBlock != null )
            {
               Array.Clear( mBlock, 0, mBlock.Length );
               Array.Resize<byte>( ref  mBlock, 0 );
            }
         }
         catch
         {
         }

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 04th October 2011
//  Add to project
//----------------------------------------------------------------------------