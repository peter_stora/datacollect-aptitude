﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    public class EnumCmmsPriorityRecords
    {
        protected List<CmmsPriorityRecord> mPriorityRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumCmmsPriorityRecords()
        {
            mPriorityRecord = new List<CmmsPriorityRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw CMMS Priority object list.
        /// </summary>
        [XmlElement("Priority")]
        public List<CmmsPriorityRecord> Priority
        {
            get { return mPriorityRecord; }
            set { mPriorityRecord = value; }
        }


        /// <summary>
        /// Add a new CMMS Priority record to the list
        /// </summary>
        /// <param name="item">populated CMMS Priority record object</param>
        /// <returns>number of CMMS Priority objects available</returns>
        public int AddPriorityRecord(CmmsPriorityRecord item)
        {
            mPriorityRecord.Add(item);
            return mPriorityRecord.Count;
        }


        /// <summary>
        /// How many CMMS Priority records are now available
        /// </summary>
        public int Count { get { return mPriorityRecord.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumCmmsPriorityRecords with support for IEnumerable. This 
    /// extended functionality enables not only foreach support - but also 
    /// (and much more importantly) LINQ support!
    /// </summary>
    public class EnumCmmsPriorities : EnumCmmsPriorityRecords, 
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mPriorityRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mPriorityRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (10:15)
//  Add to project
//----------------------------------------------------------------------------