﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Container class for Route or Workspace point collections
   /// </summary>
   public class UploadContainerRecord
   {
      /// <summary>
      /// Base constructor
      /// </summary>
      public UploadContainerRecord():
         this(null)
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iNodeRecord">Populated container node object</param>
      public UploadContainerRecord(NodeRecord iNodeRecord)
      {
         if ( iNodeRecord != null )
         {
            this.RouteID = iNodeRecord.RouteId;
            this.ID = iNodeRecord.Id;
            this.Structured = iNodeRecord.Structured;
         }

      }/* end constructor */


      /// <summary>
      /// Gets or sets the container name (NODE:Id)
      /// </summary>
      [XmlElement( "ID" )]
      public string ID
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the container ID (Route ID)
      /// </summary>
      [XmlElement( "RouteID" )]
      public int RouteID
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets whether container is a structured route
      /// </summary>
      [XmlElement( "Structured" )]
      public byte Structured
      {
         get;
         set;
      }


      /// <summary>
      /// Enumerated list of Point Elements for this collection
      /// </summary>
      [XmlElement( "Points" )]
      public EnumUploadPointRecords PointsTable = 
                new EnumUploadPointRecords();

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 24th July 2010
//  Add to project
//----------------------------------------------------------------------------

