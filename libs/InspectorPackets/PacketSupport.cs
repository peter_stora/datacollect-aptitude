﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class provides a library of packet data support methods
   /// </summary>
   public static class PacketSupport
   {
      /// <summary>
      /// Identify and return the generic hierarchy node type (including
      /// Point, Machine, Set and Route). On failure return type "unknown"
      /// </summary>
      /// <param name="iTreeNode">Generic hierarchy Node object</param>
      /// <returns>Hierarchy Node type (Machine, Set, etc)</returns>
      public static FieldTypes.NodeType GetNodeType( object iNode )
      {
         //  When the object type is a Node .. 
         if ( iNode.GetType() == typeof( NodeRecord ) )
         {
            // then cast as a node
            NodeRecord node = (NodeRecord) iNode;

            // Is this a node a route?
            if ( NodeIsRoute( node ) )
            {
               return FieldTypes.NodeType.Route;
            }
            // Is this a node a Workspace?
            else if ( NodeIsWorkspace( node ) )
            {
               return FieldTypes.NodeType.Workspace;
            }
            else
            {
               // or is it a machine or set?
               if ( NodeIsMachine( node ) )
               {
                  return FieldTypes.NodeType.Machine;
               }
               else
               {
                  return FieldTypes.NodeType.Set;
               }
            }
         }

         // When object type is a Point
         else if ( iNode.GetType() == typeof( PointRecord ) )
         {
            return FieldTypes.NodeType.Point;
         }
         else
         {
            return FieldTypes.NodeType.Unknown;
         }

      }/* end method */


      /// <summary>
      /// Is the current node a MACHINE or a SET? We use the FLAGS field
      /// to determine this by bit masking LSB+1
      /// </summary>
      /// <param name="iNode">node object</param>
      /// <returns>true if node is machine</returns>
      public static bool NodeIsMachine( NodeRecord iNode )
      {
         // get the machine falg mask
         byte machineFlag = (byte) FieldTypes.NodeFlagType.Machine;

         // ok, is this bit set in the current node
         if ( ( iNode.Flags & machineFlag ) == machineFlag )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// Is the current node a ROUTE?
      /// </summary>
      /// <param name="node">Populated Node object</param>
      /// <returns>true if Node is ROUTE, else false</returns>
      public static bool NodeIsRoute( NodeRecord iNode )
      {
         if ( iNode.ElementType == (byte) FieldTypes.NodeElementType.Route )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// Is the current node a WORKSPACE?
      /// </summary>
      /// <param name="node">Populated Node object</param>
      /// <returns>true if Node is SET, else false</returns>
      public static bool NodeIsWorkspace( NodeRecord iNode )
      {
         //
         // we can determine that a NODE is a WORKSPACE by inference. If the parent
         // UID is is the ROOT NODE, while at the same time the NODE is not a ROUTE
         // then the only other thing it can be is a WORKSPACE
         //
         if (( iNode.ElementType == (byte) FieldTypes.NodeElementType.Other ) &&
             ( iNode.ParentUid == PacketBase.ROOT_NODE_GUID ) )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th November 2009
//  Add to project
//----------------------------------------------------------------------------