﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single CodedNote table record entry (that is the record created when
   /// an operator actually adds a note to a point)
   /// </summary>
   public class SelectedNotesRecord
   {
      private EnumSelectedCodedNotes mCodedNotes;

      /// <summary>
      /// Base Constructor
      /// </summary>
      public SelectedNotesRecord()
      {
         mCodedNotes = new EnumSelectedCodedNotes();
      }/* end constructor */


      /// <summary>
      /// Oveload Constructor
      /// </summary>
      /// <param name="iNoteRecord">Populated note record</param>
      public SelectedNotesRecord( NotesRecord iNoteRecord )
      {
         mCodedNotes = new EnumSelectedCodedNotes();
         this.LastModified = iNoteRecord.LastModified;
         this.Number = iNoteRecord.Number;
         this.PointUid = iNoteRecord.PointUid;
         this.TextNote = iNoteRecord.TextNote;
         this.Uid = iNoteRecord.Uid;
         this.NoteType = iNoteRecord.NoteType;
         this.OperId = iNoteRecord.OperId;
         this.CollectionStamp = iNoteRecord.CollectionStamp;

      }/* end constructor */


      /// <summary>
      /// Current UID (GUID) for this node. Note: This value is
      /// derived and therefore independant of @ptitude
      /// </summary>
      public Guid Uid
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Note Type
      /// </summary>
      public byte NoteType
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set any coded notes 
      /// </summary>
      public EnumSelectedCodedNotes CodedNotes
      {
         get
         {
            return mCodedNotes;
         }
         set
         {
            mCodedNotes = value;
         }
      }


      /// <summary>
      /// Node Descriptor field (i.e. Hierarchies, NONROUTE, Routes etc) 
      /// From Analyst [Point Properties]: Name
      /// </summary>
      public string TextNote
      {
         get;
         set;
      }


      /// <summary>
      /// Parent Point Uid
      /// </summary>
      public Guid PointUid
      {
         get;
         set;
      }


      /// <summary>
      /// Hierarchy position of the current node. Note: This value
      /// is derived and therefore independant of @ptitude
      /// </summary>
      public int Number
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the measurement Time Stamp
      /// </summary>
      public DateTime CollectionStamp
      {
         get;
         set;
      }


      /// <summary>
      /// Note "added at" Time Stamp
      /// </summary>
      public DateTime LastModified
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Operator Id
      /// </summary>
      public int OperId
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Single "selected" Coded Note entry. This object represents that actual fields
   /// that are saved when an operator enters a coded note from the device.
   /// </summary>
   public class SelectedCodedNote : PacketBase
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public SelectedCodedNote()
      {
      }/* end constructor */

      /// <summary>
      /// Reference to the parent Note record
      /// </summary>
      public Guid NoteUid
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the coded note code value
      /// </summary>
      public int Code
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the actual note text
      /// </summary>
      public string Text
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Class to manage an enumerated list of "saved" Coded Notes
   /// </summary>
   public class EnumSelectedCodedNotes : PacketBase
   {
      private List<SelectedCodedNote> mCodedNote;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumSelectedCodedNotes()
      {
         mCodedNote = new List<SelectedCodedNote>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw CodedNoteRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      public List<SelectedCodedNote> CodedNote
      {
         get
         {
            return mCodedNote;
         }
         set
         {
            mCodedNote = value;
         }
      }

      /// <summary>
      /// Add a new Coded Note record to the list
      /// </summary>
      /// <param name="item">populated Coded Note record object</param>
      /// <returns>number of Coded Note objects available</returns>
      public int AddCodedNoteRecord( SelectedCodedNote item )
      {
         mCodedNote.Add( item );
         return mCodedNote.Count;
      }

      /// <summary>
      /// How many Coded Note records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mCodedNote.Count;
         }
      }

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th March 2009 (13:15)
//  Add to project
//----------------------------------------------------------------------------