﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{

   /// <summary>
   /// Class for Table Archive purposes only. 
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   [XmlRoot( ElementName = "MESSAGINGPREFS" )]
   public class MessagingPrefsRecord : MessagePreferences
   {
      /// <summary>
      /// Gets or sets the Operators ID
      /// </summary>
      public int OperId
      {
         get;
         set;
      }
   }


   /// <summary>
   /// Class to manage an enumerated list of Archived Operator Table records 
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumMessagingPrefRecords: PacketBase
   {
      protected List<MessagingPrefsRecord> mMessagingPrefs;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumMessagingPrefRecords()
      {
         mMessagingPrefs = new List<MessagingPrefsRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw Messaging Preferences Record list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "MESSAGINGPREFS" )]
      public List<MessagingPrefsRecord> MESSAGINGPREFS
      {
         get
         {
            return mMessagingPrefs;
         }
         set
         {
            mMessagingPrefs = value;
         }
      }


      /// <summary>
      /// Add a new Messaging Preferences Record to the list
      /// </summary>
      /// <param name="item">populated Messaging Preferences Record</param>
      /// <returns>number of Messaging Preferences Records available</returns>
      public int AddRecord( MessagingPrefsRecord item )
      {
         mMessagingPrefs.Add( item );
         return mMessagingPrefs.Count;
      }


      /// <summary>
      /// How many Messaging Prefs records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mMessagingPrefs.Count;
         }
      }
   }


   /// <summary>
   /// Class EnumMessagingPrefRecords with support for IEnumerable.
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   public class EnumMessagingPrefs : EnumMessagingPrefRecords,
       IEnumerable, IEnumerator
   {
      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mMessagingPrefs.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mMessagingPrefs[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */
   }

}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 22nd June 2012
//  Add to project
//----------------------------------------------------------------------------