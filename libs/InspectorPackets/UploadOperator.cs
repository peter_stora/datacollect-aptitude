﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Container class for the simple Upload Operator Packet
   /// </summary>
   public class UploadOperator
   {
      /// <summary>
      /// Base constructor
      /// </summary>
      public UploadOperator():
         this(null)
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iOperID">Operator ID</param>
      /// <param name="iOperName">Operator Name</param>
      public UploadOperator( OperatorName iOperator)
      {
         if ( iOperator != null )
         {
            this.OperID = iOperator.OperatorId;
            this.OperName = iOperator.Name;
         }

      }/* end constructor */


      /// <summary>
      /// Gets or sets the Operator ID
      /// </summary>
      [XmlElement( "OperID" )]
      public int OperID
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Operator Name
      /// </summary>
      [XmlElement( "OperName" )]
      public string OperName
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th July 2010
//  Add to project
//----------------------------------------------------------------------------