﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Core IAM Packet class (with constructor defaults)
   /// </summary>
   public class DeviceIAM : PacketBase
   {
      private HwVersion mFirmware = new HwVersion();
      private FieldTypes.ConnectionType mConnectionType = FieldTypes.ConnectionType.SynchDevice;

      /// <summary>
      /// Gets or sets the Devive UID
      /// </summary>
      public String UID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Device Friendly Name
      /// </summary>
      public String DeviceName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Device type (always Microlog Inspector)
      /// </summary>
      public byte DeviceType
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current licensing type
      /// </summary>
      public Boolean DeviceKeyLicensed
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current device firmware version
      /// </summary>
      public HwVersion Firmware
      {
         get
         {
            return mFirmware;
         }
         set
         {
            mFirmware = value;
         }
      }


      /// <summary>
      /// Gets or sets the requested connection type (Synch, LiveSAP, LiveMaximo etc)
      /// </summary>
      public byte ConnectionType
      {
         get
         {
            return (byte)mConnectionType;
         }
         set
         {
            mConnectionType = (FieldTypes.ConnectionType) value;
         }
      }


      /// <summary>
      /// Base Constructor
      /// </summary>
      public DeviceIAM()
         : this( DEFAULT_UID, null, DEFAULT_PRE_LICENSED )
      {
      }/* end base constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iUID">Device UID</param>
      /// <param name="iVersion">Current version of device firmware</param>
      public DeviceIAM( String iUID, HwVersion iVersion, Boolean iDeviceLicensed )
      {
         this.UID = iUID;
         this.DeviceKeyLicensed = iDeviceLicensed;
         if ( iVersion != null )
         {
            this.Firmware.Major = iVersion.Major;
            this.Firmware.Minor = iVersion.Minor;
            this.Firmware.Build = iVersion.Build;
            this.Firmware.Revision = iVersion.Revision;
         }
         else
         {
            this.Firmware.Major = VERSION_MAJOR;
            this.Firmware.Minor = VERSION_MINOR;
            this.Firmware.Build = VERSION_BUILD;
            this.Firmware.Revision = VERSION_REVISION;
         }

         // set the device type to Microlog Inspector
         this.DeviceType = (byte) FieldTypes.Microlog.Inspector;

      }/* end overload constructor */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------