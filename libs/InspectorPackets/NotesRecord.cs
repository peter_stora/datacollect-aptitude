﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Basic NOTES Record object for Table I/O
   /// </summary>
   public class NotesRecord
   {
      /// <summary>
      /// Base Constructor
      /// </summary>
      public NotesRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iCreateNewGuid">true to create new Guid</param>
      public NotesRecord( Boolean iCreateNewGuid )
      {
         if ( iCreateNewGuid )
         {
            this.Uid = System.Guid.NewGuid();
         }
      }/* end constructor */
      

      /// <summary>
      /// Gets or sets the Unique identifier for this note
      /// </summary>
      public Guid Uid
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the enumerated NoteType
      /// </summary>
      public byte NoteType
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Freehand text note
      /// </summary>
      public string TextNote
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Associated Parent Point UID
      /// </summary>
      public Guid PointUid
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Hierarchy Position
      /// </summary>
      public Int32 Number
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Collection Time Stamp (null if 
      /// not assigned to a particular measurement)
      /// </summary>
      public DateTime CollectionStamp
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Note Modified Time Stamp
      /// </summary>
      public DateTime LastModified
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the associated Operator ID
      /// </summary>
      public Int32 OperId
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 8th April 2010
//  Add overload constructor
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------
