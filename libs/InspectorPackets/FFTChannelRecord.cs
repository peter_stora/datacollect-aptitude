﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// The FFT Channel Record is an FFT Channel object with some extra
    /// index fields to allow easier linkage to the the FFTCHANNEL table
    /// </summary>
   [XmlRoot( ElementName = "FFTChannel" )] 
   public class FFTChannelRecord: FFTChannelBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public FFTChannelRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Current UID (GUID) for this CHANNEL FFT. Note: This value is
        /// derived and therefore independant of @ptitude
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Associated FFT Point Uid (from FFTPointRecord: Uid) 
        /// </summary>
        public Guid FFTPointUid { get; set; }

        /// <summary>
        /// Associated Point Uid (from FFTPointRecord: Uid) 
        /// </summary>
        public Guid PointUid { get; set; }

        /// <summary>
        /// This is the actual link from the Marlin to Analyst Points 
        /// Analyst Table Link: TREEELEM 
        /// Field Reference: TREEELEMID 
        /// It is identical to the field “PRISM” in the NODE table
        /// </summary>
        public int PointHostIndex { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------
