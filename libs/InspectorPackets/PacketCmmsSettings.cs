﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable CMMS Options Table Packet 
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketCmmsSettings : PacketBase
    {
        /// <summary>
        /// The actual Packet type (@ptitude CMMS Options Table)
        /// </summary>
        public byte Packet = (byte)PacketType.TABLE_CMMS_SETTINGS;

        /// <summary>
        /// CMMS Options table
        /// </summary>
        [XmlElement("CMMS")]
        public CmmsSettingsRecords CMMS = new CmmsSettingsRecords();

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
