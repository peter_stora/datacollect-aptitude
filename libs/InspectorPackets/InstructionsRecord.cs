﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single Instruction table record entry
   /// </summary>
   [XmlRoot( ElementName = "INSTRUCTIONS" )]
   public class InstructionsRecord : PacketBase
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public InstructionsRecord()
      {
      }/* end constructor */

      /// <summary>
      /// Associated Point Uid (from Marlin POINT or NODE tables)
      /// </summary>
      public Guid PointUid
      {
         get;
         set;
      }

      /// <summary>
      /// Text format - free or hyperlinked (default type = UNKNOWN)
      /// </summary>
      public byte TextFormat
      {
         get;
         set;
      }

      /// <summary>
      /// general instruction type (default type = UNKNOWN)
      /// </summary>
      public byte TextType
      {
         get;
         set;
      }

      /// <summary>
      /// Instruction Heading
      /// </summary>
      public string TextTitle
      {
         get;
         set;
      }

      /// <summary>
      /// Instruction body text
      /// </summary>
      public string TextBody
      {
         get;
         set;
      }

      /// <summary>
      // References the PRISM field in the NODE and POINTS table
      /// </summary>
      public int PointHostIndex
      {
         get;
         set;
      }

      /// <summary>
      /// As Message and Instruction Records are effectively identical
      /// use this method to populate an Instruction with a message
      /// </summary>
      /// <param name="iMessage"></param>
      public void Load_From_Message( MessageRecord iMessage )
      {
         // Point UID is identical to Node UID
         this.PointUid = iMessage.NodeUid;
         
         // for Routes Instructions the Point Inded will reference the Hierarchy location
         this.PointHostIndex = iMessage.Number;

         // set the TextFormat to freeform (we do not support Hyperlinks (yet)
         this.TextFormat = (byte)FieldTypes.InstructionTextFormat.FreeForm;

         // This is an override to the existing Instruction types
         this.TextType = (byte) FieldTypes.InstructionTextType.Route;

         // Although this is in the Message object, @ptitude does not export this
         this.TextTitle = iMessage.ID;

         // bulk of the body text
         this.TextBody = iMessage.Text;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th March 2009 (13:15)
//  Add to project
//----------------------------------------------------------------------------
