﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simplified version of the current Points class for 
   /// revisions being returned to @ptitude Analyst
   /// </summary>
   public class RevisedPointRecord
   {
      /// <summary>
      /// Constructor
      /// </summary>
      public RevisedPointRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Node Descriptor field (i.e. Hierarchies, NONROUTE, Routes etc) 
      /// From Analyst [Point Properties]: Name
      /// </summary>
      public string Id
      {
         get;
         set;
      }


      /// <summary>
      /// The Point friendly name (i.e. "My Route")
      /// </summary>
      public string Description
      {
         get;
         set;
      }


      /// <summary>
      /// Number of days between inspections
      /// </summary>
      public double ScheduleDays
      {
         get;
         set;
      }


      /// <summary>
      /// The actual means by which the Node is located (i.e Barcode)
      /// </summary>
      public byte LocationMethod
      {
         get;
         set;
      }


      /// <summary>                                         
      /// The actual Location Tag value (i.e. Barcode value)
      /// </summary>                                        
      public string LocationTag
      {
         get;
         set;
      }


      /// <summary>
      /// Point Modify Time Stamp
      /// </summary>
      public DateTime LastModified
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
