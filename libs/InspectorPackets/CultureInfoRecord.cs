﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class is used to pass culture specific data between
   /// the client and server. This data includes decimal and group
   /// seperator characters.
   /// </summary>
   public class CultureInfoRecord : PacketBase
   {
      /// <summary>
      /// Base Constructor
      /// </summary>
      public CultureInfoRecord()
      {
      }/* end constructor */

  
      /// <summary>
      /// Gets or sets the culture name
      /// </summary>
      public string Name
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the culture ID
      /// </summary>
      public int LCID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the ISO language name
      /// </summary>
      public string ISOCode
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd November 2009
//  Add to project
//----------------------------------------------------------------------------