﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Node Revisions Class, for serialized return to @ptitude
    /// </summary>
    public class RevisedNodeRecord
    {      
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public RevisedNodeRecord()
        {
        }

        /// <summary>
        /// The Node descriptor Field (i.e. "Hierarchies", "Routes")
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Node friendly name (i.e. "My Route")
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Date and Time that this node was last modified!
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// The actual means by which the Node is located (i.e Barcode)
        /// </summary>
        public byte LocationMethod { get; set; }

        /// <summary>
        /// The actual Location Tag value (i.e. Barcode value)
        /// </summary>
        public string LocationTag { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------

