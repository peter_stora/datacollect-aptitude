﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Derived Items Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumDerivedItemRecords : PacketBase
    {
        protected List<DerivedItemsRecord> mDerivedItemsRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumDerivedItemRecords()
        {
            mDerivedItemsRecord = new List<DerivedItemsRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw DerivedItemRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("DERIVEDITEMS")]
        public List<DerivedItemsRecord> DerivedItems
        {
            get { return mDerivedItemsRecord; }
            set { mDerivedItemsRecord = value; }
        }


        /// <summary>
        /// Add a new DerivedItems record to the list
        /// </summary>
        /// <param name="item">populated Derived Items record object</param>
        /// <returns>number of Derived Items objects available</returns>
        public int AddDerivedItemsRecord(DerivedItemsRecord item)
        {
            mDerivedItemsRecord.Add(item);
            return mDerivedItemsRecord.Count;
        }

        /// <summary>
        /// How many Derived Items records are now available
        /// </summary>
        public int Count { get { return mDerivedItemsRecord.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumDerivedItemRecords with support for IEnumerable. This 
    /// extended functionality enables not only foreach support - but also 
    /// (and much more importantly) LINQ support!
    /// </summary>
    public class EnumDerivedItems : EnumDerivedItemRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mDerivedItemsRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mDerivedItemsRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------