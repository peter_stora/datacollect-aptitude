﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    public class EnumCmmsWorkTypeRecords 
    {
        protected List<CmmsWorkTypeRecord> mWorkTypeRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumCmmsWorkTypeRecords()
        {
            mWorkTypeRecord = new List<CmmsWorkTypeRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw CMMS WorkType object list.
        /// </summary>
        [XmlElement("WorkType")]
        public List<CmmsWorkTypeRecord> WorkType
        {
            get { return mWorkTypeRecord; }
            set { mWorkTypeRecord = value; }
        }


        /// <summary>
        /// Add a new CMMS WorkType record to the list
        /// </summary>
        /// <param name="item">populated CMMS WorkType record object</param>
        /// <returns>number of CMMS WorkType objects available</returns>
        public int AddWorkTypeRecord(CmmsWorkTypeRecord item)
        {
            mWorkTypeRecord.Add(item);
            return mWorkTypeRecord.Count;
        }


        /// <summary>
        /// How many CMMS WorkType records are now available
        /// </summary>
        public int Count { get { return mWorkTypeRecord.Count; } }


    }/* end class */


    /// <summary>
    /// Class EnumCmmsWorkTypeRecords with support for IEnumerable. 
    /// This extended functionality enables not only foreach support 
    /// but also (and much more importantly) LINQ support!
    /// </summary>
    public class EnumCmmsWorkTypes : EnumCmmsWorkTypeRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mWorkTypeRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mWorkTypeRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (11:15)
//  Add to project
//----------------------------------------------------------------------------


