﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Base class for Process properties object. The only fields in this 
   /// class are those that can be edited from within the application itself, 
   /// and then returned to @ptitude as part of the core XML upload file.
   /// </summary>
   public class ProcessRecordBase
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public ProcessRecordBase()
      {
         this.AutoRange = false;
      }/* end constructor */


      /// <summary>
      /// Process Table input display types (i.e. Bar, Slider, Gauge)
      /// </summary>
      public byte Form
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the minimum acceptable range value
      /// </summary>
      public double RangeMin
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the maximum acceptable range value
      /// </summary>
      public double RangeMax
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Process scale units
      /// </summary>
      public string Units
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the set range limits are to be set
      /// internally for this point (overrides RangeMax and RangeMin)
      /// </summary>
      public Boolean AutoRange
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the actual Alarm Type (Window, level etc)
      /// </summary>
      public byte AlarmType
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the upper alert value
      /// </summary>
      public double UpperAlert
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the lower Alert Value
      /// </summary>
      public double LowerAlert
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the upper Danger Value
      /// </summary>
      public double UpperDanger
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the lower Danger Value
      /// </summary>
      public double LowerDanger
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------