﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2008 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simple Bluetooth Connection object
   /// </summary>
   [XmlRoot( ElementName = "BTDEVICE" )]
   public class BluetoothDevice: PacketBase
   {
      /// <summary>
      /// Is this device currently visible?
      /// </summary>
      private Boolean mVisible = false;
      
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public BluetoothDevice( )
      {
      }
      
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public BluetoothDevice( FieldTypes.BluetoothStack iStackType )
      {
         this.BTStackType = (byte) iStackType;
      }

      /// <summary>
      /// BT Stack type that discovered this device
      /// </summary>
      public byte BTStackType
      {
         get;
         set;
      }

      /// <summary>
      /// Name of the bluetooth device
      /// </summary>
      public String DeviceName
      {
         get;
         set;
      }

      /// <summary>
      /// Address of the device as an array of bytes
      /// </summary>
      public byte[] DeviceAddress
      {
         get;
         set;
      }

      
      /// <summary>
      /// Set the visible state for this device
      /// </summary>
      /// <param name="iAvailable"></param>
      public void SetVisibleState( Boolean iAvailable )
      {
         mVisible = iAvailable;
      }/* end method */


      /// <summary>
      /// Get the visible state for this device
      /// </summary>
      /// <returns>true if visible</returns>
      public Boolean GetVisibleState()
      {
         return mVisible;

      }/* end method */


   }/* end class */


   /// <summary>
   /// Class to manage a list of Bluetooth Connection Records
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumBluetoothDevices : PacketBase
   {
      protected List<BluetoothDevice> mMCCDevices;
      protected int mIndex = -1;


      /// <summary>
      /// Constructor
      /// </summary>
      public EnumBluetoothDevices()
      {
         mMCCDevices = new List<BluetoothDevice>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw Bluetooth object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "BTDEVICE" )]
      public List<BluetoothDevice> BTDEVICE
      {
         get
         {
            return mMCCDevices;
         }
         set
         {
            mMCCDevices = value;
         }
      }


      /// <summary>
      /// Add a new Bluetooth Device record to the list
      /// </summary>
      /// <param name="item">populated Bluetooth Device object</param>
      /// <returns>number of MCCDevice objects available</returns>
      public int AddDevice( BluetoothDevice item )
      {
         mMCCDevices.Add( item );
         return mMCCDevices.Count;
      }


      /// <summary>
      /// Get the index position of a reference Bluetooth device
      /// </summary>
      /// <param name="iBTAddress">reference address</param>
      /// <returns>0 or greater, -1 on not found or error</returns>
      public int IndexOf( byte[] iBTAddress )
      {
         // set initial result to nothing found
         int index = -1;
         Boolean addressMatch = true;

         // check all available devices
         foreach ( BluetoothDevice device in mMCCDevices )
         {
            // increment the index
            index++;

            // set the address match
            addressMatch = true;

            // reference the current device address
            byte[] reference = device.DeviceAddress;

            // are the addresses of the same length?
            if ( iBTAddress.Length == reference.Length )
            {
               // compare the actual array bytes
               for ( int i=0; i < reference.Length; ++i )
               {
                  // if any byte pair do not match then fail
                  if ( reference[ i ] != iBTAddress[ i ] )
                  {
                     addressMatch = false;
                     break;
                  }
               }
               
               // if a match was found then we're done here
               if ( addressMatch )
               {
                  break;
               }
            }
            // if address lengths do noth match then fail
            else
            {
               addressMatch = false;
            }
         }

         // if we never found anything then reset the index
         if (( !addressMatch ) & (index >= 0))
         {
            index = -1;
         }

         // return result
         return index;

      }/* end method */


      /// <summary>
      /// Clear all available devices
      /// </summary>
      public void Clear()
      {
         mMCCDevices.Clear();
      }/* end method */


      /// <summary>
      /// How many Bluetooth Devicess are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mMCCDevices.Count;
         }
      }

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 13th May 2010
//  Add to project
//----------------------------------------------------------------------------