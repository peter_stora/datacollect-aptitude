﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This download object is used to store the defined measurement 
   /// configuration for an FFT enabled POINT.  Some of this information 
   /// is copied to the upload FFT Channel object prior the measurement 
   /// being uploaded to the Device Server
   /// </summary>
   public class FFTPointRecord
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public FFTPointRecord()
      {
      }/* end constructor */

      /// <summary>
      /// Get or set the current UID (GUID) for this POINT FFT. 
      /// Note: This value is derived and therefore independant 
      /// of @ptitude
      /// </summary>
      public Guid Uid { get; set; }

      /// <summary>
      /// Hierarchy position of the current node. Note: This value
      /// is derived and therefore independant of @ptitude
      /// </summary>
      public int Number { get; set; }

      /// <summary>
      /// Get or set the associated Point Uid (from POINTS table)
      /// </summary>
      public Guid PointUid { get; set; }

      /// <summary>
      /// Get or set: When is the POINT to take FFT data
      /// (0: Never, 1: On Alarm, 2: Always)
      /// </summary>
      public byte TakeData { get; set; }

      /// <summary>
      /// Get or Set: Use WMCD’s default or @A custom configurations. 
      /// (0: Use WMCD default, 1: Use custom settings from @Aptiude)
      /// </summary>
      public byte UseConfiguration { get; set; }

      /// <summary>
      /// Machine running speed (or pole pass frequency)
      /// </summary>
      public double DefaultRunningSpeed { get; set; }

      /// <summary>
      /// Get or Set the type for data being saved
      /// (0: Magnitude, 2: Phase, 3: Time, 20: Motor Current)
      /// </summary>
      public byte EnvType { get; set; }

      /// <summary>
      /// Get or Set the Detection Type
      /// (0: Peak, 1: Peak-to-Peak, 2: RMS)
      /// </summary>
      public byte EnvDetection { get; set; }

      /// <summary>
      /// Get or set the number of lines for the measurement
      /// </summary>
      public int EnvNumberOfLines { get; set; }

      /// <summary>
      /// Get or set the number of averages for the measurement
      /// </summary>
      public int EnvAverages { get; set; }

      /// <summary>
      /// Get or set the type of windowing function to use on the data
      /// (1: Hanning, 2: Flat top, 3: Uniform)
      /// </summary>
      public byte EnvWindow { get; set; }

      /// <summary>
      /// Get or set the maximum envelope frequency
      /// </summary>
      public double EnvFreqMax { get; set; }

      /// <summary>
      /// Get or Set the type for data being saved
      /// (0: Magnitude, 2: Phase, 3: Time, 20: Motor Current)
      /// </summary>
      public byte VelType { get; set; }

      /// <summary>
      /// Get or Set the Detection Type
      /// (0: Peak, 1: Peak-to-Peak, 2: RMS)
      /// </summary>
      public byte VelDetection { get; set; }

      /// <summary>
      /// Get or set the number of lines for the measurement
      /// </summary>
      public int VelNumberOfLines { get; set; }

      /// <summary>
      /// Get or set the number of averages for the measurement
      /// </summary>
      public int VelAverages { get; set; }

      /// <summary>
      /// Get or set the type of windowing function to use on the data
      /// (1: Hanning, 2: Flat top, 3: Uniform)
      /// </summary>
      public byte VelWindow { get; set; }

      /// <summary>
      /// Get or set the maximum envelope frequency
      /// </summary>
      public double VelFreqMax { get; set; }

      /// <summary>
      /// Get or Set the type for data being saved
      /// (0: Magnitude, 2: Phase, 3: Time, 20: Motor Current)
      /// </summary>
      public byte AcclType { get; set; }

      /// <summary>
      /// Get or Set the Detection Type
      /// (0: Peak, 1: Peak-to-Peak, 2: RMS)
      /// </summary>
      public byte AcclDetection { get; set; }

      /// <summary>
      /// Get or set the number of lines for the measurement
      /// </summary>
      public int AcclNumberOfLines { get; set; }

      /// <summary>
      /// Get or set the number of averages for the measurement
      /// </summary>
      public int AcclAverages { get; set; }

      /// <summary>
      /// Get or set the type of windowing function to use on the data
      /// (1: Hanning, 2: Flat top, 3: Uniform)
      /// </summary>
      public byte AcclWindow { get; set; }

      /// <summary>
      /// Get or set the maximum envelope frequency
      /// </summary>
      public double AcclFreqMax { get; set; }


      /// <summary>
      /// Use the default settings
      /// </summary>
      public void Use_Defaults()
      {
         EnvType           = (byte)FFTRecordInfo.DEFAULT_DATA_TYPE_ENVELOPE;
         EnvDetection      = (byte)FFTRecordInfo.DEFAULT_DETECTION_ENVELOPE;
         EnvNumberOfLines  = FFTRecordInfo.DEFAULT_LINES_ENVELOPE;
         EnvAverages       = FFTRecordInfo.DEFAULT_AVG_ENVELOPE;
         EnvWindow         = (byte)FFTRecordInfo.DEFAULT_WINDOW_ENVELOPE;
         EnvFreqMax        = FFTRecordInfo.DEFAULT_FMAX_ENVELOPE;

         VelType           = (byte)FFTRecordInfo.DEFAULT_DATA_TYPE_VELOCITY;
         VelDetection      = (byte)FFTRecordInfo.DEFAULT_DETECTION_VELOCITY;
         VelNumberOfLines  = FFTRecordInfo.DEFAULT_LINES_VELOCITY;
         VelAverages       = FFTRecordInfo.DEFAULT_AVG_VELOCITY;
         VelWindow         = (byte)FFTRecordInfo.DEFAULT_WINDOW_VELOCITY;
         VelFreqMax        = FFTRecordInfo.DEFAULT_FMAX_VELOCITY;

         AcclType          = (byte)FFTRecordInfo.DEFAULT_DATA_TYPE_ACCEL;
         AcclDetection     = (byte)FFTRecordInfo.DEFAULT_DETECTION_ACCEL;
         AcclNumberOfLines = FFTRecordInfo.DEFAULT_LINES_ACCEL;
         AcclAverages      = FFTRecordInfo.DEFAULT_AVG_ACCEL;
         AcclWindow        = (byte)FFTRecordInfo.DEFAULT_WINDOW_ACCEL;
         AcclFreqMax       = FFTRecordInfo.DEFAULT_FMAX_ACCEL;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------