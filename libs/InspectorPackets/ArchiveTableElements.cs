﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
// This file contains a collection of serilizable classes which are required
// for the archive and recovery of the Inspector database. Although as classes 
// each should be in its own *.cs file, adding an additional thirty files to
// the Packets namespace would not be desirable.
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Enumeration of all Tables that can be archived
   /// </summary>
   public enum ArchiveTable
   {
      C_NOTE,
      C_NOTES,
      CONDITIONALPOINT,
      CORRECTIVEACTION,
      DERIVEDITEMS,
      DERIVEDPOINT,
      DEVICEPROFILE,
      ENVDATA,
      FFTCHANNEL,
      FFTPOINT,
      INSPECTION,
      INSTRUCTIONS,
      LOG,
      MACHINENOPSKIPS,
      MACHINEOKSKIPS,
      MCC,
      MESSAGE,
      MESSAGINGPREFS,
      NODE,
      NOTES,
      OPERATORS,
      POINTS,
      PRIORITY,
      PROBLEM,
      PROCESS,
      REFERENCEMEDIA,
      SAVEDMEDIA,
      STRUCTURED,
      STRUCTUREDLOG,
      TEMPDATA,
      TEXTDATA,
      USERPREFERENCES,
      VELDATA,
      VERSION,
      WORKNOTIFICATION,
      WORKTYPE
   }

   /// <summary>
   /// C_NOTE Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveCNoteTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (C_NOTE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.C_NOTE;

      /// <summary>
      /// Coded Note Table Element
      /// </summary>
      [XmlElement( "C_NOTE_TABLE" )]
      public EnumCodedNoteRecords C_NOTE = new EnumCodedNoteRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveCNoteTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// C_NOTES Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveCNotesTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (C_NOTE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.C_NOTES;

      /// <summary>
      /// Saved Note Codes Table Element
      /// </summary>
      [XmlElement( "C_NOTES_TABLE" )]
      public EnumSelectedCodedNotes C_NOTES = new EnumSelectedCodedNotes();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveCNotesTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// CONDITIONALPOINT Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveConditinalPointTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (CONDITIONALPOINT TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.CONDITIONALPOINT;

      /// <summary>
      /// CONDITIONALPOINT Table Element
      /// </summary>
      [XmlElement( "CONDITIONALPOINT_TABLE" )]
      public EnumConditionalPointRecords CONDITIONALPOINT = new EnumConditionalPointRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveConditinalPointTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// CORRECTIVEACTION Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveCorrectiveActionTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (CORRECTIVEACTION TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.CORRECTIVEACTION;

      /// <summary>
      /// CORRECTIVEACTION Table Element
      /// </summary>
      [XmlElement( "CORRECTIVEACTION_TABLE" )]
      public EnumCmmsCorrectiveActionRecords CORRECTIVEACTION = new EnumCmmsCorrectiveActionRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveCorrectiveActionTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// DERIVEDITEMS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveDerivedItemsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (DERIVEDITEMS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.DERIVEDITEMS;

      /// <summary>
      /// Single DERIVEDITEMS Record Element
      /// </summary>
      [XmlElement( "DERIVEDITEMS_TABLE" )]
      public EnumDerivedItemRecords DERIVEDITEMS = new EnumDerivedItemRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveDerivedItemsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// DERIVEDPOINT Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveDerivedPointTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (DERIVEDPOINTS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.DERIVEDPOINT;

      /// <summary>
      /// Single DERIVEDPOINTS Record Element
      /// </summary>
      [XmlElement( "DERIVEDPOINT_TABLE" )]
      public EnumDerivedPointRecords DERIVEDPOINT = new EnumDerivedPointRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveDerivedPointTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// DEVICEPROFILE Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveDeviceProfileTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (DEVICEPROFILE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.DEVICEPROFILE;

      /// <summary>
      /// Single DEVICEPROFILE Record Element
      /// </summary>
      [XmlElement( "DEVICEPROFILE_TABLE" )]
      public DeviceProfile DEVICEPROFILE = new DeviceProfile();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveDeviceProfileTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// ENVDATA Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveEnvDataTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (ENVDATA TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.ENVDATA;

      /// <summary>
      /// Coded Note Table Element
      /// </summary>
      [XmlElement( "ENVDATA_TABLE" )]
      public EnumSavedMeasurementRecords ENVDATA = new EnumSavedMeasurementRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveEnvDataTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// FFTCHANNEL Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveFFTChannelTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (FFTCHANNEL TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.FFTCHANNEL;

      /// <summary>
      /// FFTCHANNEL Table Element
      /// </summary>
      [XmlElement( "FFTCHANNEL_TABLE" )]
      public FFTChannelRecords FFTCHANNEL = new FFTChannelRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveFFTChannelTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// FFTPOINT Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveFFTPointTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (FFTPOINT TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.FFTPOINT;

      /// <summary>
      /// FFTPOINT Table Element
      /// </summary>
      [XmlElement( "FFTPOINT_TABLE" )]
      public EnumFFTPointRecords FFTPOINT = new EnumFFTPointRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveFFTPointTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// INSPECTION Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveInspectionTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MCC TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.INSPECTION;

      /// <summary>
      /// Inspection Table Element
      /// </summary>
      [XmlElement( "INSPECTION_TABLE" )]
      public EnumInspectionRecords INSPECTION = new EnumInspectionRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveInspectionTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// INSTRUCTIONS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveInstructionsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (INSTRUCTIONS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.INSTRUCTIONS;

      /// <summary>
      /// INSTRUCTIONS Table Element
      /// </summary>
      [XmlElement( "INSTRUCTIONS_TABLE" )]
      public EnumInstructionRecords INSTRUCTIONS = new EnumInstructionRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveInstructionsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// MACHINENOPSKIPS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveMachineNopSkipsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MACHINENOPSKIPS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.MACHINENOPSKIPS;

      /// <summary>
      /// MACHINENOPSKIPS Table Element
      /// </summary>
      [XmlElement( "MACHINENOPSKIPS_TABLE" )]
      public EnumMachineSkipRecords MACHINENOPSKIPS = new EnumMachineSkipRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveMachineNopSkipsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// MACHINEOKSKIPS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveMachineOkSkipsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MACHINEOKSKIPS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.MACHINEOKSKIPS;

      /// <summary>
      /// MACHINEOKSKIPS Table Element
      /// </summary>
      [XmlElement( "MACHINEOKSKIPS_TABLE" )]
      public EnumMachineSkipRecords MACHINEOKSKIPS = new EnumMachineSkipRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveMachineOkSkipsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// MCC Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveMCCTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MCC TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.MCC;

      /// <summary>
      /// MCC Table Archive
      /// </summary>
      [XmlElement( "MCC_TABLE" )]
      public EnumMCCRecords MCC = new EnumMCCRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveMCCTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// MESSAGINGPREFS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveMessagingPrefsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MESSAGINGPREFS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.MESSAGINGPREFS;

      /// <summary>
      /// MESSAGINGPREFS Table Element
      /// </summary>
      [XmlElement( "MESSAGINGPREFS_TABLE" )]
      public EnumMessagingPrefRecords MESSAGINGPREFS = new EnumMessagingPrefRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveMessagingPrefsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// MESSAGE Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveMessageTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (MESSAGE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.MESSAGE;

      /// <summary>
      /// NOTES Table Element
      /// </summary>
      [XmlElement( "MESSAGE_TABLE" )]
      public EnumMessageRecords MESSAGE = new EnumMessageRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveMessageTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// NODE Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveNodeTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (NODE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.NODE;
      
      /// <summary>
      /// Node Table Element
      /// </summary>
      [XmlElement( "NODE_TABLE" )]
      public EnumNodeRecords NODE = new EnumNodeRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveNodeTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// NOTES Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveNotesTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (NOTES  TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.NOTES;

      /// <summary>
      /// NOTES Table Element
      /// </summary>
      [XmlElement( "NOTES_TABLE" )]
      public EnumNoteRecords NOTES  = new EnumNoteRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveNotesTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// OPERATORS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveOperatorsTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (OPERATORS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.OPERATORS;

      /// <summary>
      /// OPERATORS Table Element
      /// </summary>
      [XmlElement( "OPERATORS_TABLE" )]
      public EnumOperatorsRecords OPERATORS = new EnumOperatorsRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveOperatorsTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// POINTS Table Archive  
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchivePointTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (POINTS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.POINTS;
      
      /// <summary>
      /// Points Table Elemenent
      /// </summary>
      [XmlElement( "POINTS_TABLE" )]
      public EnumPointRecords POINTS = new EnumPointRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchivePointTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// PRIORITY Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchivePriorityTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (PRIORITY TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.PRIORITY;

      /// <summary>
      /// PRIORITY Table Element
      /// </summary>
      [XmlElement( "PRIORITY_TABLE" )]
      public EnumCmmsPriorityRecords PRIORITY = new EnumCmmsPriorityRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchivePriorityTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// PROBLEM Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveProblemTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (PROBLEM TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.PROBLEM;

      /// <summary>
      /// PROBLEM Table Element
      /// </summary>
      [XmlElement( "PROBLEM_TABLE" )]
      public EnumCmmsProblemRecords PROBLEM = new EnumCmmsProblemRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveProblemTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// PROCESS Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveProcessTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (PROCESS TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.PROCESS;
      
      /// <summary>
      /// Process Table Element
      /// </summary>
      [XmlElement( "PROCESS_TABLE" )]
      public EnumProcessRecords PROCESS = new EnumProcessRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveProcessTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// SAVEDMEDIA Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveSavedMediaTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (SAVEDMEDIA TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.SAVEDMEDIA;

      /// <summary>
      /// SAVEDMEDIA Table Element
      /// </summary>
      [XmlElement( "SAVEDMEDIA_TABLE" )]
      public EnumSavedMediaRecords SAVEDMEDIA = new EnumSavedMediaRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveSavedMediaTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// TEMPDATA Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveTempDataTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (TEMPDATA TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.TEMPDATA;

      /// <summary>
      /// Coded Note Table Element
      /// </summary>
      [XmlElement( "TEMPDATA_TABLE" )]
      public EnumSavedMeasurementRecords TEMPDATA = new EnumSavedMeasurementRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveTempDataTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// TEXTDATA Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveTextDataTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (TEXTDATA TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.TEXTDATA;

      /// <summary>
      /// TextData Table Element
      /// </summary>
      [XmlElement( "TEXTDATA_TABLE" )]
      public EnumTextDataRecords TEXTDATA = new EnumTextDataRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveTextDataTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// USERPREFERENCES Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveUserPreferencesTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (USERPREFERENCES TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.USERPREFERENCES;

      /// <summary>
      /// USERPREFERENCES Table Element
      /// </summary>
      [XmlElement( "USERPREFERENCES_TABLE" )]
      public EnumUserPreferenceRecords USERPREFERENCES = new EnumUserPreferenceRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveUserPreferencesTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// VELDATA Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveVelDataTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (VELDATA TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.VELDATA;

      /// <summary>
      /// Coded Note Table Element
      /// </summary>
      [XmlElement( "VELDATA_TABLE" )]
      public EnumSavedMeasurementRecords VELDATA = new EnumSavedMeasurementRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveVelDataTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// WORKNOTIFICATION Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveWorkNotificationTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (WORKNOTIFICATION  TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.WORKNOTIFICATION;

      /// <summary>
      /// WORKNOTIFICATION  Table Element
      /// </summary>
      [XmlElement( "WORKNOTIFICATION_TABLE" )]
      public EnumCmmsWorkNotificationRecords WORKNOTIFICATION  = new EnumCmmsWorkNotificationRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveWorkNotificationTable()
      {
      }/* end constructor */
   }


   /// <summary>
   /// WORKTYPE Table Archive
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ArchiveWorkTypeTable : PacketBase
   {
      /// <summary>
      /// The actual Table type (WORKTYPE TABLE)
      /// </summary>
      public byte Table = (byte) ArchiveTable.WORKTYPE;

      /// <summary>
      /// WORKTYPE Table Element
      /// </summary>
      [XmlElement( "WORKTYPE_TABLE" )]
      public EnumCmmsWorkTypeRecords WORKTYPE = new EnumCmmsWorkTypeRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public ArchiveWorkTypeTable()
      {
      }/* end constructor */
   }
}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2012
//  Add to project
//----------------------------------------------------------------------------