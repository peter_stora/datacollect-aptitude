﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Inspection table record entry
    /// </summary>
    public class InspectionRecord: InspectionRecordBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public InspectionRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Current UID (GUID) for this node. Note: This value is
        /// derived and therefore independant of @ptitude
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Hierarchy position of the current node. Note: This value
        /// is derived and therefore independant of @ptitude
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Associated Point Uid (from Marlin POINT table)
        /// </summary>
        public Guid PointUid { get; set; }

        /// <summary>
        /// Baseline Data time (unused)
        /// </summary>
        public DateTime BaselineDataTime { get; set; }

        /// <summary>
        /// Baseline Data value (unused)
        /// </summary>
        public byte BaselineDataValue { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th March 2009 (13:15)
//  Add to project
//----------------------------------------------------------------------------
