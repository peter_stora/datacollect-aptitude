﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Enumerted list of Upload Operator objects
   /// </summary>
   public class EnumUploadOperators
   {
      protected List<UploadOperator> mUploadOperatorRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumUploadOperators()
      {
         mUploadOperatorRecord = new List<UploadOperator>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw PointRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "Operator" )]
      public List<UploadOperator> Container
      {
         get
         {
            return mUploadOperatorRecord;
         }
         set
         {
            mUploadOperatorRecord = value;
         }
      }


      /// <summary>
      /// Add a new Point record to the list
      /// </summary>
      /// <param name="item">populated Node record object</param>
      /// <returns>number of Node objects available</returns>
      public int AddOperator( UploadOperator item )
      {
         mUploadOperatorRecord.Add( item );
         return mUploadOperatorRecord.Count;
      }/* end method */


      /// <summary>
      /// How many Point records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mUploadOperatorRecord.Count;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th July 2010
//  Add to project
//----------------------------------------------------------------------------
