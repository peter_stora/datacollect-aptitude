﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// PROFILE PACKET: Transmitted from the Server to the connected client,
    /// this packet contains all off the data elements required to allow the
    /// client to generate its authentication key.
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketProfile : PacketBase
    {
        /// <summary>
        /// The actual Packet type (PROFILE)
        /// </summary>
        public byte Packet = (byte)PacketType.PROFILE;
        public ProfileSettings Profile = new ProfileSettings();


        /// <summary>
        /// Return the authentication key
        /// </summary>
        /// <param name="iUID"></param>
        /// <returns></returns>
        public string GetAuthenticationKey(string iUID)
        {
            XmlEncryption encrypt = new XmlEncryption();
            encrypt.SecurePassword = Profile.SubKey;
            return encrypt.EncryptStringToHex(iUID);

        }/* end method */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 2nd March 2009
//  Add to project
//----------------------------------------------------------------------------

