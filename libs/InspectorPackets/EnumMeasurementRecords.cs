﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage an enumerated list of Measurments
   /// </summary>
   public class EnumMeasurementRecords : PacketBase
   {
      protected List<PointMeasurement> mPointMeasurements;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumMeasurementRecords()
      {
         mPointMeasurements = new List<PointMeasurement>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw MeasurementRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "Measurement" )]
      public List<PointMeasurement> Measurement
      {
         get
         {
            return mPointMeasurements;
         }
         set
         {
            mPointMeasurements = value;
         }

      }/* end method */

      /// <summary>
      /// Add a new measurement to the measurements list
      /// </summary>
      /// <param name="item">measurement item</param>
      /// <returns>number or measurements</returns>
      public int AddMeasurement( PointMeasurement item )
      {
         mPointMeasurements.Add( item );
         return mPointMeasurements.Count;

      }/* end method */


      /// <summary>
      /// Clear all existing measurements
      /// </summary>
      public void Clear()
      {
         for ( int i=0; i < mPointMeasurements.Count; ++i )
         {
            mPointMeasurements[ i ] = null;
         }
         mPointMeasurements.Clear();

      }/* end method */


      /// <summary>
      /// This method returns the most recent measurement - or null
      /// </summary>
      /// <returns>Last collected PointMeasurement object</returns>
      public PointMeasurement GetLatestMeasurement()
      {
         // get the index of the last collected TEMP measurement
         int index = mPointMeasurements.Count - 1;

         // now get the measurement
         if ( index >= 0 )
         {
            return mPointMeasurements[ index ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// This method returns the second most recent measurement - or null
      /// </summary>
      /// <returns>Last collected PointMeasurement object</returns>
      public PointMeasurement GetSecondLatestMeasurement()
      {
         // get the index of the last collected TEMP measurement
         int index = mPointMeasurements.Count - 2;

         // now get the measurement
         if ( index >= 0 )
         {
            return mPointMeasurements[ index ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// How many measurements are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mPointMeasurements.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Class EnumMeasurementRecords with support for IEnumerable. This 
   /// extended functionality enables not only foreach support - but 
   /// also (and much more importantly) LINQ support!
   /// </summary>
   public class EnumPointMeasurements : EnumMeasurementRecords,
       IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mPointMeasurements.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mPointMeasurements[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------