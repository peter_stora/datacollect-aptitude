﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Base class for Inspection properties object. The only fields in this 
   /// class are those that can be edited from within the application itself, 
   /// and then returned to @ptitude as part of the core XML upload file.
   /// </summary>
   public class InspectionRecordBase
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public InspectionRecordBase()
      {
      }/* end constructor */

      /// <summary>
      /// Question associated with the Inspection Point
      /// </summary>
      public string Question
      {
         get;
         set;
      }

      /// <summary>
      /// Text displayed on alert condition raised
      /// </summary>
      public string AlertText
      {
         get;
         set;
      }

      /// <summary>
      /// Text displayed on danger condition raised
      /// </summary>
      public string DangerText
      {
         get;
         set;
      }

      /// <summary>
      /// Number of choices to be displayed (between 1 and 15)
      /// </summary>
      public byte NumberOfChoices
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #1
      /// </summary>
      public string Choice1
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #2
      /// </summary>
      public string Choice2
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #3
      /// </summary>
      public string Choice3
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #4
      /// </summary>
      public string Choice4
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #5
      /// </summary>
      public string Choice5
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #6
      /// </summary>
      public string Choice6
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #7
      /// </summary>
      public string Choice7
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #8
      /// </summary>
      public string Choice8
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #9
      /// </summary>
      public string Choice9
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #10
      /// </summary>
      public string Choice10
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #11
      /// </summary>
      public string Choice11
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #12
      /// </summary>
      public string Choice12
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #13
      /// </summary>
      public string Choice13
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #14
      /// </summary>
      public string Choice14
      {
         get;
         set;
      }

      /// <summary>
      /// Choice text field #15
      /// </summary>
      public string Choice15
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType1
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType2
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType3
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType4
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType5
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType6
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType7
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType8
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType9
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType10
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType11
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType12
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType13
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType14
      {
         get;
         set;
      }

      /// <summary>
      /// What type of alarm is associated with the question (alert\danger)
      /// </summary>
      public byte AlarmType15
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 24th April 2012
//  Increase choices count to fifteen
//
//  Revision 0.0 APinkerton 28th April 2009
//  Add to project
//----------------------------------------------------------------------------