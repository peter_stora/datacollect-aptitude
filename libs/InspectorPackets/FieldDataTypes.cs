﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class holds a list of enumerated types that represent their
   /// field equivalents within the database
   /// </summary>
   public static class FieldTypes
   {
      public static int NULL_OPERID = -1;

      /// <summary>
      /// Process Table Alarm State enumerators
      /// </summary>
      public enum AlarmState
      {
         Off = 0x00,
         Alert = 0x01,
         Danger = 0x02,
         Clear = 0x03,
         AlertHi = 0x04,
         DangerHi = 0x05,
         ClearHi = 0x06,
         AlertLo = 0x07,
         DangerLo = 0x08,
         ClearLo = 0x09
      }


      /// <summary>
      /// Process Table Alarm State Types
      /// </summary>
      public enum AlarmType
      {
         Off = 0x00,
         Level = 0x01,
         InWindow = 0x02,
         OutOfWindow = 0x03
      }


      /// <summary>
      /// Supported Bluetooth stack types
      /// </summary>
      public enum BluetoothStack
      {
         Microsoft,
         Widcomm,
         StoneStreet
      }


      /// <summary>
      /// Calendar Compliance window type
      /// </summary>
      public enum Calendar
      {
         Second = 0x00,
         Hour = 0x01,
         Day = 0x02,
         Week = 0x03,
         Month = 0x04,
         Year = 0x05
      }


      /// <summary>
      /// CMMS Work Notification Key Field
      /// </summary>
      public enum CmmsKeyField
      {
         TreeElem = 0x01,
         OffRouteFunctLocation = 0x02,
         OffRouteBarCode = 0x03,
         OffRouteRFID = 0x04
      }


      /// <summary>
      /// Cmms Work Notification Status Field (new or uploaded)
      /// </summary>
      public enum CmmsStatus
      {
         Uploaded = 0x00,
         New = 0x01
      }


      /// <summary>
      /// Collection status type for tree check flags
      /// </summary>
      public enum CollectionCheck
      {
         None = 0x00,
         Some = 0x01,
         All = 0x02
      }


      /// <summary>
      /// What is our connection type? This type specifies how the
      /// Middleware will actually manage the device connection - with
      /// the default being "SynchDevice"
      /// </summary>
      public enum ConnectionType
      {
         SynchDevice = 0x00,
         LiveAptitude = 0x01,
         LiveMaximo = 0xA1,
         LiveSAP = 0xA2,
         LivePI = 0xB1,
         LivePHD = 0xB2
      }


      /// <summary>
      /// Conditional POINT Criteria Types
      /// </summary>
      public enum CriteriaType
      {
         InAlarm = 21500,
         OutOfAlarm = 21501,
         Above = 21502,
         Below = 21503,
         InRange = 21504,
         OutOfRange = 21505,
         IsEqual = 21506,
         InNotEqual = 21507,
      }


      /// <summary>
      /// Derived POINT Variable types.  The variables are the Source POINT's
      /// actual data.  This list the types of data that can be used
      /// </summary>
      public enum DPVariableType
      {
         None = 0,
         Overall = 1,
         MCDTemp = 2,
         MCDVel = 3,
         MCDEnv = 4,
         InspResult1 = 5,
         InspResult2 = 6,
         InspResult3 = 7,
         InspResult4 = 8,
         InspResult5 = 9
      }


      /// <summary>
      /// Derived POINT Formula Item Types.  These represent the types for the 
      /// Derived POINT Computation Engines Formula Buffer
      /// </summary>
      public enum DPFormulaItemType
      {
         None = 0,
         Constant = 1004,
         Space = 1005,
         Operator = 1006,
         Variable = 1007,
         Function = 1009
      }


      /// <summary>
      /// Derived POINT Operator types.  These are the types of operations 
      /// Derived POINTs can handle
      /// </summary>
      public enum DPOperatorType
      {
         None = 0,
         Plus = 2001,
         Minus = 2002,
         Multiply = 2003,
         Divide = 2004,
         Negate = 2005,
         OpenParen = 2006,
         CloseParen = 2007,
         Comma = 2008
      }


      /// <summary>
      /// Derived POINT Function types.  These are the types of functions the 
      /// Derived POINT Computation Engine can preform
      /// </summary>
      public enum DPFunctionType
      {
         None = 0,
         LogBase10 = 3001,
         NaturalLog = 3002,
         Sin = 3003,
         ArcSin = 3004,
         Cos = 3005,
         ArcCos = 3006,
         Tan = 3007,
         ArcTan = 3008,
         PercentChange = 3009,
         Power = 3010,
         DeltaTime = 3011,
         DeltaValue = 3012,
         DegToRad = 3013
      }


      /// <summary>
      /// FFT Channel Identifier
      /// </summary>
      public enum FFTChannelId
      {
         None = 0x00,
         EnvAccl = 0x01,
         Velocity = 0x02,
         Acceleration = 0x03
      }


      /// <summary>
      /// Process Table input display types
      /// </summary>
      public enum FormType
      {
         Gauge = 0x00,
         Bar = 0x01,
         Slider = 0x02,
         Keypad = 0x03
      }


      /// <summary>
      /// Inspection Alarm settings
      /// </summary>
      public enum InspectionAlarms
      {
         None = 0x00,
         Alert = 0x01,
         Danger = 0x02
      }


      /// <summary>
      /// Fifteen Inspection choices
      /// </summary>
      public enum InspectionChoices : ushort
      {
         NoneSet = 0x0000,
         Choice1 = 0x0001,
         Choice2 = 0x0002,
         Choice3 = 0x0004,
         Choice4 = 0x0008,
         Choice5 = 0x0010,
         Choice6 = 0x0020,
         Choice7 = 0x0040,
         Choice8 = 0x0080,
         Choice9 = 0x0100,
         Choice10 = 0x0200,
         Choice11 = 0x0400,
         Choice12 = 0x0800,
         Choice13 = 0x1000,
         Choice14 = 0x2000,
         Choice15 = 0x4000,
      }


      /// <summary>
      /// Instruction Text Format
      /// </summary>
      public enum InstructionTextFormat
      {
         Unknown = 0x00,
         FreeForm = 0x01,
         HyperLink = 0x02
      }


      /// <summary>
      /// Instruction Text Type
      /// </summary>
      public enum InstructionTextType
      {
         Unknown = 0x00,
         Machine = 0x01,
         PrePoint = 0x02,
         PostPoint = 0x03,
         InspectionAlert = 0x04,
         InspectionDanger = 0x05,
         OverallAlertHigh = 0x06,
         OverallDangerHigh = 0x07,
         OverallAlertLow = 0x08,
         OverallDangerLow = 0x09,
         MCDEnvelopeDanger = 0x0A,
         MCDEnvelopeAlert = 0x0B,
         MCDVelocityDanger = 0x0C,
         MCDVelocityAlert = 0x0D,
         MCDTempDanger = 0x0E,
         MCDTempAlert = 0x0F,
         Route = 0xF0,
      }


      /// <summary>
      /// Process Node Data Type
      /// NOTE: @A does not support RFID for an upload!
      /// </summary>
      public enum LocationType
      {
         None = 0x00,
         Barcode = 0x01,
         MQC = 0x02,
         RFID = 0x03
      }


      /// <summary>
      /// MCD Channel Identifier
      /// </summary>
      public enum MCDChannelId
      {
         None = 0x00,
         Velocity = 0x01,
         EnvAccl = 0x02,
         Temperature = 0x03
      }


      /// <summary>
      /// Device Type enumerator
      /// </summary>
      public enum Microlog
      {
         Generic = 0x00,
         Inspector = 0x01,
         AX = 0x02,
         GX = 0x03,
         MX = 0x04
      }


      /// <summary>
      /// Is the current node a route
      /// </summary>
      public enum NodeElementType
      {
         Other = 0x00,
         Route = 0x01
      }


      /// <summary>
      /// Flag status of the current Node object (fields sum as required)
      /// NOTE: @ptitude 5.0 does not support Node based notes!! 
      /// </summary>
      public enum NodeFlagType
      {
         Other = 0x00,
         Machine = 0x01,
         NodeTagChanged = 0x02,
         NodeHasNotes = 0x04
      }


      /// <summary>
      /// Generic type enumerator covering both NODE and POINT
      /// </summary>
      public enum NodeType
      {
         Point,
         Machine,
         Set,
         Route,
         Workspace,
         Unknown
      }


      /// <summary>
      /// Type of note being created and saved.
      /// Note:  For initial release of MI, the Measurement and Compliance
      /// types will not be supported. Default note type will be "Standard"
      /// </summary>
      public enum NoteType
      {
         AdHoc = 0x00,
         Measurement = 0x01,
         Compliance = 0x02,
         Unknown = 0xFF
      }


      /// <summary>
      /// Simplified Alarm State for hierarchy elements.
      /// Note: This type is for internel reference only!
      /// </summary>
      public enum NodeAlarmState
      {
         Reset = 0x00,
         None = 0x01,
         Clear = 0x02,
         Alert = 0x03,
         Danger = 0x04
      }


      /// <summary>
      /// Node collection Type
      /// </summary>
      public enum ProcessType
      {
         MCD = 0x00,
         Temperature = 0x01,
         Current = 0x02,
         Pressure = 0x03,
         Flow = 0x04,
         RPM = 0x05,
         VoltsDC = 0x06,
         Counts = 0x07,
         CountRate = 0x08,
         OperatingHours = 0x09,
         WildCard = 0x0A,
         SingleSelectInspection = 0x0B,
         MultiSelectInspection = 0x0C,
         Derived = 0x0D,
         TextData = 0x0E
      }


      /// <summary>
      /// Route Type: structured or standard
      /// </summary>
      public enum RouteType
      {
         Standard = 0x00,
         Structured = 0x01
      }


      /// <summary>
      /// Points Table "skipped" type
      /// </summary>
      public enum SkippedType
      {
         None = 0x00,
         Collected = 0x01,
         CollectionFailed = 0x02,
         Corrected = 0x03,
         MachineNotOperating = 0x04,
         ConditionFailed = 0x05,
         MachineOk = 0x06,
         AutoCollectedMachineOk = 0x07,
         NotCollected = 0x08,
         DpReferenceDataMissing = 0x09,
         DpReferencePointMissing = 0x0A,
         DpInvalidFormula = 0x0B,
         DpDivideByZero = 0x0C,
         ScanDataCollected = 0x11,
      }

      /// <summary>
      /// MCC table System units
      /// </summary>
      public enum SystemUnits
      {
         English = 0x00,
         Metric = 0x01
      }


      /// <summary>
      /// Flag status of the current Point object (fields sum as required)
      /// </summary>
      public enum TagChanged
      {
         Unchanged = 0x00,
         PointHasNotes = 0x01,
         PointTagChanged = 0x02
      }


      /// <summary>
      /// Flag status for FFT Point Data collection
      /// </summary>
      public enum TakeData
      {
         Never = 0x00,
         OnAlarm = 0x01,
         Always = 0x02
      }


      /// <summary>
      /// MCC Temperature units
      /// </summary>
      public enum TempUnits
      {
         Farenheit = 0x00,
         Celcius = 0x01
      }


      /// <summary>
      /// Allowable chracters for TextData POINT type
      /// </summary>
      public enum TextType
      {
         /// <summary>
         /// Text can be both alpha and numeric
         /// </summary>
         AlphaNumeric = 0x00,

         /// <summary>
         /// Text can be alpha only (numeric will be blocked)
         /// </summary>
         AlphaOnly = 0x01,

         /// <summary>
         /// Text can be numeric only (alpha will be blocked)
         /// </summary>
         NumericOnly = 0x02
      }


      /// <summary>
      /// External vibration sensor types
      /// </summary>
      public enum VibrationSensor
      {
         None = 0x00,
         MCD = 0x01,
         WMCD = 0x02,
      }

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  FieldDataTypes.cs
//
//  Revision 4.1 APinkerton 16th October 2012
//  Add enumerator 'ScanDataCollected' to enumerator 'SkipperType'
//
//  Revision 4.0 APinkerton 3rd October 2012
//  Add TextData type enumerator
//
//  Revision 3.0 APinkerton 25th April 2012
//  Increased Inspection choices to fifteen
//
//  Revision 2.0 APinkerton 13th June 2010
//  Added new enumeror type TakeData
//
//  Revision 1.0 APinkerton 4th January 2010
//  Added additional enumeror fields to "SkippedType"
//
//  Revision 0.0 APinkerton 16th March 2009
//  Add to project
//----------------------------------------------------------------------------