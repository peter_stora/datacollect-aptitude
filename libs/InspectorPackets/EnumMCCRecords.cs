﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of MCC Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumMCCRecords : PacketBase
    {
        protected List<MCCRecord> mMCCRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumMCCRecords()
        {
            mMCCRecord = new List<MCCRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw MCCRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("MCC")]
        public List<MCCRecord> MCC
        {
            get { return mMCCRecord; }
            set { mMCCRecord = value; }
        }


        /// <summary>
        /// Add a new MCC record to the list
        /// </summary>
        /// <param name="item">populated MCC record object</param>
        /// <returns>number of MCC objects available</returns>
        public int AddMCCRecord(MCCRecord item)
        {
            mMCCRecord.Add(item);
            return mMCCRecord.Count;
        }

        /// <summary>
        /// How many MCC records are now available
        /// </summary>
        public int Count { get { return mMCCRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumMCCRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumMCC : EnumMCCRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mMCCRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mMCCRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------