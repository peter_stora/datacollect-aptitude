﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Inspection Table Packet
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketInspectionTable : PacketBase
    {
        /// <summary>
        /// The actual Packet type (OPERATOR SET)
        /// </summary>
        public byte Packet = (byte)PacketType.TABLE_INSPECTION;

        /// <summary>
        /// Operator Settings Element
        /// </summary>
        [XmlElement("InspectionTable")]
        public EnumInspectionRecords InspectionTable = new EnumInspectionRecords();

        /// <summary>
        /// Constructor
        /// </summary>
        public PacketInspectionTable()
        {
        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th March 2009 (13:15)
//  Add to project
//----------------------------------------------------------------------------