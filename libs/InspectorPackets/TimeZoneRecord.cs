﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class is used to pass TimeZone information (both ways) 
   /// between both server and client
   /// </summary>
   public class TimeZoneRecord : PacketBase
   {
      /// <summary>
      /// Base Constructor
      /// </summary>
      public TimeZoneRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Gets or sets the current year 
      /// </summary>
      public int CurrentYear
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of daylight saving changeover hour
      /// </summary>
      public int DSTHour
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of daylight saving changeover Day
      /// </summary>
      public int DSTDay
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of daylight saving changeover Month
      /// </summary>
      public int DSTMonth
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of standard time changeover Hour
      /// </summary>
      public int STHour
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of standard time changeover Day
      /// </summary>
      public int STDay
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the start of standard time changeover Month
      /// </summary>
      public int STMonth
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the overall zone offset in minutes
      /// </summary>
      public int Bias
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the standard time name
      /// </summary>
      public string StandardName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the bias for standard time
      /// </summary>
      public int StandardBias
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the daylight saving name
      /// </summary>
      public string DaylightName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the bias for daylight saving time
      /// </summary>
      public int DaylightBias
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 21st November 2009
//  Add to project
//----------------------------------------------------------------------------
