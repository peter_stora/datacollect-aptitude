﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    public class EnumCmmsProblemRecords 
    {
        protected List<CmmsProblemRecord> mProblemRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumCmmsProblemRecords()
        {
            mProblemRecord = new List<CmmsProblemRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw CMMS Problem object list.
        /// </summary>
        [XmlElement("Problem")]
        public List<CmmsProblemRecord> Problem
        {
            get { return mProblemRecord; }
            set { mProblemRecord = value; }
        }


        /// <summary>
        /// Add a new CMMS Problem record to the list
        /// </summary>
        /// <param name="item">populated CMMS Problem record object</param>
        /// <returns>number of CMMS Problem objects available</returns>
        public int AddProblemRecord(CmmsProblemRecord item)
        {
            mProblemRecord.Add(item);
            return mProblemRecord.Count;
        }


        /// <summary>
        /// How many CMMS Problem records are now available
        /// </summary>
        public int Count { get { return mProblemRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumCmmsProblemRecords with support for IEnumerable. This 
    /// extended functionality enables not only foreach support - but also 
    /// (and much more importantly) LINQ support!
    /// </summary>
    public class EnumCmmsProblems : EnumCmmsProblemRecords,
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mProblemRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mProblemRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (11:15)
//  Add to project
//----------------------------------------------------------------------------

