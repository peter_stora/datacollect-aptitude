﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    public class UploadNodeRecord
    {
        private const int PRISM_INDEX = -1;

        private RevisedNodeRecord mRevisedNode = null;
        
        /// <summary>
        /// Base constructor (Prism index defaults to -1)
        /// </summary>
        public UploadNodeRecord()
        {
            PRISM = PRISM_INDEX;
        }/* end constructor */


        /// <summary>
        /// This is the actual link from the Marlin to Analyst Points 
        /// Analyst Table Link: TREEELEM. Note, the reason this is not
        /// directly included in the "Revised Node" section, is that 
        /// keeping it apart will allow for addition for further data
        /// structures (such as pictures etc).
        /// </summary>
        [XmlElement("TreeElemID")]
        public int PRISM { get; set; }


        /// <summary>
        /// Add a new "revised" Node Record
        /// </summary>
        /// <param name="iNodeRecord"></param>
        public void AddNodeRecord(NodeRecord iNodeRecord)
        {
            // create object if not already created
            if (mRevisedNode == null)
                mRevisedNode = new RevisedNodeRecord();

            // populate revised node fields
            mRevisedNode.Description = iNodeRecord.Description;
            mRevisedNode.Id = iNodeRecord.Id;
            mRevisedNode.LastModified = PacketBase.DateTimeNow;
            mRevisedNode.LocationMethod = iNodeRecord.LocationMethod;
            mRevisedNode.LocationTag = iNodeRecord.LocationTag;

            // override any other TreeElemID
            PRISM = iNodeRecord.PRISM;

        }/* end method */


        /// <summary>
        /// Get the revised process record. This property will not be
        /// serialized if the local member is null!
        /// </summary>
        [XmlElement("RevisedNode")]
        public RevisedNodeRecord NodeRecord
        {
            get { return mRevisedNode; }
            set { mRevisedNode = value; }
        }/* end property */


    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 30th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
