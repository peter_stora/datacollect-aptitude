﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable CMMS Options Table
    /// </summary>
   [XmlRoot( ElementName = "CmmsDownloadSettings" )]
    public class CmmsSettingsRecords : PacketBase
    {
        [XmlElement("Problems")]
        public EnumCmmsProblemRecords ProblemsTable = new EnumCmmsProblemRecords();


        [XmlElement("Priorities")]
        public EnumCmmsPriorityRecords PrioritiesTable = new EnumCmmsPriorityRecords();


        [XmlElement("CorrectiveActions")]
        public EnumCmmsCorrectiveActionRecords CorrectiveActionsTable = new EnumCmmsCorrectiveActionRecords();


        [XmlElement("WorkTypes")]
        public EnumCmmsWorkTypeRecords WorkTypesTable = new EnumCmmsWorkTypeRecords();


        [XmlElement("WorkNotifications")]
        public EnumCmmsWorkNotificationRecords WorkNotifications = new EnumCmmsWorkNotificationRecords();


        /// <summary>
        /// Constructor
        /// </summary>
        public CmmsSettingsRecords()
        {
        }/* end constructor */


        /// <summary>
        /// Add a single cmms problem record to the problems table
        /// </summary>
        /// <param name="iKey">@ptitude supplied reference key</param>
        /// <param name="iProblemText">Problem Text</param>
        public void AddCmmsProblem(int iKey, string iProblemText)
        {
            CmmsProblemRecord problem = new CmmsProblemRecord();
            problem.MAProblemDescriptionKey = iKey;
            problem.ProblemText = iProblemText;
            ProblemsTable.AddProblemRecord(problem);
        }/* end method */


        /// <summary>
        /// Add a single cmms priority record to the priorities table
        /// </summary>
        /// <param name="iKey">@ptitude supplied reference key</param>
        /// <param name="iCode">@ptitude supplied reference code</param>
        /// <param name="iProblemText">Priority Text</param>
        public void AddCmmsPriority(int iKey, string iCode, string iPriorityText)
        {
            CmmsPriorityRecord priority = new CmmsPriorityRecord();
            priority.MAPriorityKey = iKey;
            priority.MAPriorityCode = iCode;
            priority.PriorityText = iPriorityText;
            PrioritiesTable.AddPriorityRecord(priority);
        }/* end method */


        /// <summary>
        /// Add a single cmms corrective action record to the problems table
        /// </summary>
        /// <param name="iKey">@ptitude supplied reference key</param>
        /// <param name="iProblemText">Corrective action Text</param>
        public void AddCmmsCorrectiveAction(int iKey, string iActionText)
        {
            CmmsCorrectiveActionRecord corrAction = new CmmsCorrectiveActionRecord();
            corrAction.MACorrectiveActionKey = iKey;
            corrAction.CorrectiveActionText = iActionText;
            CorrectiveActionsTable.AddCorrectiveActionRecord(corrAction);
        }/* end method */


        /// <summary>
        /// Add a single cmms work type record to the problems table
        /// </summary>
        /// <param name="iKey">@ptitude supplied reference key</param>
        /// <param name="iProblemText">Work Type Text</param>
        public void AddCmmsWorkType(int iKey, string iWorkTypeText)
        {
            CmmsWorkTypeRecord workType = new CmmsWorkTypeRecord();
            workType.MAWorkTypeKey = iKey;
            workType.WorkTypeText = iWorkTypeText;
            WorkTypesTable.AddWorkTypeRecord(workType);
        }/* end method */


        /// <summary>
        /// Add a single cmms work notification record to the notifications table
        /// </summary>
        /// <param name="iWorkNotification">Populated object from @ptitude</param>
        public void AddCmmsWorkNotification(CmmsWorkNotification iWorkNotification)
        {
            WorkNotifications.AddWorkNotificationRecord(iWorkNotification);
        }/* end method */


    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
