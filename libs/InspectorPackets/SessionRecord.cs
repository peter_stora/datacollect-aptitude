﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Session container with session ID, UTC synchronization time
    /// and UTC synchronized expiry time
    /// </summary>
    public class SessionRecord : PacketBase
    {
        /// <summary>
        /// Session ID property
        /// </summary>
        public String ID { get; set; }
        public DateTime Begins;
        public DateTime Expires;


        /// <summary>
        /// Set the session expiry time in seconds
        /// </summary>
        public void SessionLife(Double iSessionLife)
        {
           this.Expires = PacketBase.DateTimeNow.AddSeconds( iSessionLife );

        }/* end method */


        /// <summary>
        /// Base Constructor
        /// </summary>
        public SessionRecord()
            : this(DEFAULT_SESSION_ID, DEFAULT_SESSION_TIMEOUT)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iSessionID">Current Session ID</param>
        public SessionRecord(String iSessionID)
            : this(iSessionID, DEFAULT_SESSION_TIMEOUT)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iSessionID">Current Session ID</param>
        /// <param name="iSessionLife">Session Life in Seconds</param>
        public SessionRecord(String iSessionID, Double iSessionLife)
        {
            this.ID = iSessionID;
            this.Begins = PacketBase.DateTimeNow;
            this.Expires = PacketBase.DateTimeNow.AddSeconds( iSessionLife );
        }/* end constructor */

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------

