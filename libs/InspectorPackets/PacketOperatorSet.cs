﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable list of operator objects (perator set)
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketOperatorSet : PacketBase
    {
        /// <summary>
        /// The actual Packet type (OPERATOR SET)
        /// </summary>
        public byte Packet = (byte)PacketType.OPERATOR_SET;

        /// <summary>
        /// Operator Settings Element
        /// </summary>
        [XmlElement("OperatorSet")]
        public OperatorSet OperatorSet = new OperatorSet();

        /// <summary>
        /// Constructor
        /// </summary>
        public PacketOperatorSet()
        {
        }/* end constructor */

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Wrapper for OperatorSettingsTS. This class simply braces the core
    /// object with the standard "ACK" elements, to allow it to be passed
    /// directly to the device if required.
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class BinaryOperatorSet : PacketBase
    {
        /// <summary>
        /// Operator Settings Element
        /// </summary>
        [XmlElement("Get_Operator_Names")]
        public TsOperatorSet GetOperatorNames = new TsOperatorSet();

        /// <summary>
        /// Constructor
        /// </summary>
        public BinaryOperatorSet()
        {
        }/* end constructor */

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// This class manages both the query generation and return data 
    /// processor for the Transaction Server method Get_Operator_Settings
    /// </summary>
    public class TsOperatorSet
    {
        private EnumOperatorNames enumOperName;
        private EnumOperatorBinarySettings enumOperSettings;

        /// <summary>
        /// Constructor
        /// </summary>
        public TsOperatorSet()
        {
            enumOperName = new EnumOperatorNames();
            enumOperSettings = new EnumOperatorBinarySettings();
        }/* end constructor */

        /// <summary>
        /// Operator Set Id number
        /// </summary>
        [XmlElement("iOperSetId")]
        public int iOperSetId { get; set; }

        /// <summary>
        /// Enumerated list of operator names
        /// </summary>
        [XmlElement("oEnumOperName")]
        public EnumOperatorNames OperatorNames
        {
            get { return enumOperName; }
            set { enumOperName = value; }
        }

        /// <summary>
        /// Enumerated list of operator settings
        /// </summary>
        [XmlElement("oEnumOperSettings")]
        public EnumOperatorBinarySettings OperatorSettings
        {
            get { return enumOperSettings; }
            set { enumOperSettings = value; }
        }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// This class manages both the query generation and return data 
    /// processor for the Transaction Server method Get_Operator_Settings
    /// </summary>
    public class OperatorSet
    {
        private EnumOperatorNames enumOperName;
        private EnumOperatorXmlSettings enumOperSettings;

        /// <summary>
        /// Constructor
        /// </summary>
        public OperatorSet()
        {
            enumOperName = new EnumOperatorNames();
            enumOperSettings = new EnumOperatorXmlSettings();
        }/* end constructor */

        /// <summary>
        /// Operator Set Id number
        /// </summary>
        [XmlElement("iOperSetId")]
        public int iOperSetId { get; set; }

        /// <summary>
        /// Enumerated list of operator names
        /// </summary>
        [XmlElement("oEnumOperName")]
        public EnumOperatorNames OperatorNames
        {
            get { return enumOperName; }
            set { enumOperName = value; }
        }

        /// <summary>
        /// Enumerated list of operator settings
        /// </summary>
        [XmlElement("oEnumOperSettings")]
        public EnumOperatorXmlSettings OperatorSettings
        {
            get { return enumOperSettings; }
            set { enumOperSettings = value; }
        }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Class to manage an enumerated list of operator settings 
    /// </summary>
    public class EnumOperatorBinarySettings
    {
        private List<OperatorSettingBinary> enumOperSettings;

        /// <summary>
        /// Constructor 
        /// </summary>
        public EnumOperatorBinarySettings()
        {
            enumOperSettings = new List<OperatorSettingBinary>();
        }/* end constructor */

        /// <summary>
        /// Operator settings object (type OperatorSetting)
        /// </summary>
        [XmlElement("ISKFDataOperSettings")]
        public OperatorSettingBinary[] SettingsData
        {
            get
            {
                OperatorSettingBinary[] items = new OperatorSettingBinary[enumOperSettings.Count];
                enumOperSettings.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                OperatorSettingBinary[] items = (OperatorSettingBinary[])value;
                enumOperSettings.Clear();
                foreach (OperatorSettingBinary item in items)
                    enumOperSettings.Add(item);
            }
        }

        /// <summary>
        /// Add a new operator setting object
        /// </summary>
        /// <param name="item">populated operator setting object</param>
        /// <returns>number of setting objects available</returns>
        public int AddEnumOperSettings(OperatorSettingBinary item)
        {
            enumOperSettings.Add(item);
            return enumOperSettings.Count;
        }

        /// <summary>
        /// How many operator settings are available
        /// </summary>
        public int Count { get { return enumOperSettings.Count; } }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Class to manage an enumerated list of operator settings 
    /// </summary>
    public class EnumOperatorXmlSettings
    {
        private List<OperatorSettingXML> mEnumOperSettings;

        /// <summary>
        /// Constructor 
        /// </summary>
        public EnumOperatorXmlSettings()
        {
            mEnumOperSettings = new List<OperatorSettingXML>();
        }/* end constructor */

        /// <summary>
        /// Operator settings object (type OperatorSetting)
        /// </summary>
        [XmlElement("ISKFDataOperSettings")]
        public List<OperatorSettingXML> SettingsData
        {
            get
            {
                return mEnumOperSettings;
            }
            set
            {
               mEnumOperSettings = value;
            }
        }

        /// <summary>
        /// Add a new operator setting object
        /// </summary>
        /// <param name="item">populated operator setting object</param>
        /// <returns>number of setting objects available</returns>
        public int AddEnumOperSettings(OperatorSettingXML item)
        {
            mEnumOperSettings.Add(item);
            return mEnumOperSettings.Count;
        }

        /// <summary>
        /// How many operator settings are available
        /// </summary>
        public int Count { get { return mEnumOperSettings.Count; } }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Class to manage an enumerated list of operator names 
    /// </summary>
    public class EnumOperatorNames
    {
        private List<OperatorName> enumOperName;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumOperatorNames()
        {
            enumOperName = new List<OperatorName>();
        }/* end constructor */

        /// <summary>
        /// Operator name object (type OperatorName)
        /// </summary>
        [XmlElement("ISKFDataOperNames")]
        public List<OperatorName> NameData
        {
            get
            {
               return enumOperName;
            }
            set
            {
               enumOperName = value;
            }
        }

        /// <summary>
        /// Add a new operator name object
        /// </summary>
        /// <param name="item">populated operator name object</param>
        /// <returns>number of name objects available</returns>
        public int AddEnumOperName(OperatorName item)
        {
            enumOperName.Add(item);
            return enumOperName.Count;
        }

        /// <summary>
        /// How many operator names are available
        /// </summary>
        public int Count { get { return enumOperName.Count; } }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Simple class to manage each operator name
    /// </summary>
    public class OperatorName
    {
        /// <summary>
        /// Operator ID
        /// </summary>
        [XmlElement("OperId")]
        public int OperatorId { get; set; }

        /// <summary>
        /// Operator name
        /// </summary>
        [XmlElement("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public OperatorName()
        {
        }/* end constructor */

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Simple class to manage settings associated with each operator
    /// </summary>
    public class OperatorSetting
    {
        /// <summary>
        /// Current settings ID
        /// </summary>
        [XmlElement("SettingsId")]
        public int SettingsId { get; set; }

        /// <summary>
        /// Operator ID
        /// </summary>
        [XmlElement("OperId")]
        public int OperatorId { get; set; }

        /// <summary>
        /// DAD type (i.e Marlin, Microlog, DMx etc)
        /// </summary>
        [XmlElement("DadType")]
        public int DadType { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public OperatorSetting()
        {
        }/* end constructor */

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Simple class to manage settings associated with each operator
    /// </summary>
    public class OperatorSettingBinary : OperatorSetting
    {
        /// <summary>
        /// Operator settings
        /// </summary>
        [XmlElement("Settings")]
        public byte[] Settings { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public OperatorSettingBinary()
        {
        }/* end constructor */

        /// <summary>
        /// Get how many bytes are in the settings block
        /// </summary>
        public int SettingBytes
        {
            get
            {
                int result = 0;
                if (Settings != null)
                {
                    result = Settings.Count<byte>();
                }
                return result;
            }
        }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Class that defines all operator settings
    /// </summary>
    public class OperatorSettingXML : OperatorSetting
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OperatorSettingXML()
        {
        }/* end constructor */

        [XmlElement("Settings")]
        public XmlSettingsNode Settings = new XmlSettingsNode();

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th February 2009 (10:45)
//  Add to project
//----------------------------------------------------------------------------
