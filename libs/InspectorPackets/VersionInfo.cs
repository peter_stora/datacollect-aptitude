﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Net;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Static class that returns the Assembly FullName and Version Number
   /// </summary>
   public static class VersionInfo
   {
      /// <summary>
      /// Forced version (update on each published release for software)
      /// </summary>
      private const string ASSEMBLY_VERSION = "1.0.2.16";

      /// <summary>
      /// Full name of this assembly
      /// </summary>
      private static string mAssemblyName = string.Empty;
      
      /// <summary>
      /// Assembly version number Major.Minor.Build.Revision
      /// </summary>
      private static string mAssemblyVersion = string.Empty;


      /// <summary>
      /// Gets the current version of this assembly
      /// Note: This is specific to InspectorPackets
      /// </summary>
      public static HwVersion Current
      {
         get
         {
            GetAssemblyName();
            return new HwVersion(ASSEMBLY_VERSION);
         }
      }


      /// <summary>
      /// Gets the Assembly full name as a string
      /// </summary>
      public static string AssemblyFullName
      {
         get
         {
            GetAssemblyName();
            return mAssemblyName;
         }
      }


      /// <summary>
      /// Gets the Assembly version "major.minor.build.revision"
      /// </summary>
      public static string AssemblyVersion
      {
         get
         {
            GetAssemblyName();
            return mAssemblyVersion;
         }
      }


      /// <summary>
      /// Gets the FullName string for this Assembly
      /// </summary>
      private static void GetAssemblyName()
      {
         try
         {
            if ( mAssemblyName == string.Empty )
            {
               // split the Assembly "FullName" string into csv parts
               string[] assemblyCSV = 
                  Assembly.GetCallingAssembly().FullName.Split( new char[] { ',' } );

               // reference just the version name
               mAssemblyName = assemblyCSV[ 0 ];

               // reference just the version number
               mAssemblyVersion =
                  assemblyCSV[ 1 ].Remove(
                     0,
                     assemblyCSV[ 1 ].IndexOf( '=' ) + 1
                  );
            }
         }
         catch
         {
            mAssemblyName = string.Empty;
            mAssemblyVersion = string.Empty;
         }

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th July 2010
//  Add to project
//----------------------------------------------------------------------------