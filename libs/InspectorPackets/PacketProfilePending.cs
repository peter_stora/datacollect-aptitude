﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable list of pending profile tables.
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketProfilePending : PacketBase
    {
        private EnumPendingTableNames mTableNames;

        /// <summary>
        /// Constructor (no overloads) 
        /// </summary>
        public PacketProfilePending()
        {
            mTableNames = new EnumPendingTableNames();
        }/* end constructor */

        /// <summary>
        /// Packet Type
        /// </summary>
        public byte Packet = (byte)PacketType.PROFILE_PENDING;

        public EnumPendingTableNames Tables
        {
            get { return mTableNames; }
            set { mTableNames = value; }
        }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Class to manage an enumerated list of pending tables
    /// </summary>
    public class EnumPendingTableNames : PacketBase
    {
        private List<PendingTableSet> mTableNames;

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public EnumPendingTableNames()
        {
            mTableNames = new List<PendingTableSet>();
        }/* end constructor */

        /// <summary>
        /// Pending Table Name
        /// </summary>
        [XmlElement("Tables")]
        public PendingTableSet[] Tables
        {
            get
            {
                PendingTableSet[] items = new PendingTableSet[mTableNames.Count];
                mTableNames.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                PendingTableSet[] items = (PendingTableSet[])value;
                mTableNames.Clear();
                foreach (PendingTableSet item in items)
                    mTableNames.Add(item);
            }
        }

        /// <summary>
        /// Add a new table name to the tables list
        /// </summary>
        /// <param name="item">table name item</param>
        /// <returns>number of pending tables</returns>
        public int AddTableName(PendingTableSet item)
        {
            mTableNames.Add(item);
            return mTableNames.Count;
        }/* end method */


        /// <summary>
        /// Drop one of the pending tables
        /// </summary>
        /// <param name="index">index of table name to drop</param>
        /// <returns>number of table names remaining</returns>
        public int DropPendingTable(int index)
        {
            mTableNames.RemoveAt(index);
            return mTableNames.Count;
        }/* end method */


        /// <summary>
        /// How many pending tables are now available
        /// </summary>
        public int Count { get { return mTableNames.Count; } }

    }/* end class */


    //***********************************************************************
    //***********************************************************************
    //***********************************************************************


    /// <summary>
    /// Very simple string class (with XML Attribute) to allow for
    /// serialization of each pending table name
    /// </summary>
    public class PendingTableSet : PacketBase
    {
        //private string mTableName = "";
        private PacketType mTableType = PacketType.TABLE_CNOTE;

        /// <summary>
        /// Base Constructor
        /// </summary>
        public PendingTableSet()
        {
        }/* end cosntructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iTableName">name of pending table</param>
        public PendingTableSet(PacketType iTableType)
        {
            mTableType = iTableType;
        }/* end constructor */


        [XmlAttribute("Type")]
        public byte TablePacketType
        {
            get { return (byte)mTableType; }
            set { mTableType = (PacketType)value; }
        }

    }/* end class */



}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 2nd March 2009
//  Add to project
//----------------------------------------------------------------------------
