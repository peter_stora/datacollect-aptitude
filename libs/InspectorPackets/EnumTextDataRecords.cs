﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage an enumerated list of Text Data Setup Table records 
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumTextDataRecords : PacketBase
   {
      protected List<TextDataRecord> mTextDataRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumTextDataRecords()
      {
         mTextDataRecord = new List<TextDataRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw TextDataRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "TEXTDATA" )]
      public List<TextDataRecord> TextData
      {
         get
         {
            return mTextDataRecord;
         }
         set
         {
            mTextDataRecord = value;
         }
      }


      /// <summary>
      /// Add a new TextData record to the list
      /// </summary>
      /// <param name="item">populated TextData record object</param>
      /// <returns>number of TextData objects available</returns>
      public int AddTextDataRecord( TextDataRecord item )
      {
         mTextDataRecord.Add( item );
         return mTextDataRecord.Count;
      }

      /// <summary>
      /// How many TextData records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mTextDataRecord.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Class EnumTextDataRecords with support for IEnumerable. This extended
   /// functionality enables not only foreach support - but also (and much
   /// more importantly) LINQ support!
   /// </summary>
   public class EnumTextData : EnumTextDataRecords, IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mTextDataRecord.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mTextDataRecord[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 10th September 2012
//  Add to project
//----------------------------------------------------------------------------

