﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Conditional Point records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumConditionalPointRecords : PacketBase
    {
        protected List<ConditionalPointRecord> mConditionalPointRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumConditionalPointRecords()
        {
            mConditionalPointRecord = new List<ConditionalPointRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw ConditionalPointRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("CONDITIONALPOINT")]
        public List<ConditionalPointRecord> ConditionalPoint
        {
            get { return mConditionalPointRecord; }
            set { mConditionalPointRecord = value; }
        }


        /// <summary>
        /// Add a new Conditional Point record to the list
        /// </summary>
        /// <param name="item">populated Conditional Point record object</param>
        /// <returns>number of Conditional Point objects available</returns>
        public int AddConditionalPointRecord(ConditionalPointRecord item)
        {
            mConditionalPointRecord.Add(item);
            return mConditionalPointRecord.Count;
        }

        /// <summary>
        /// How many Conditional Point records are now available
        /// </summary>
        public int Count { get { return mConditionalPointRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumConditionalPointRecords with support for IEnumerable. 
    /// This extended functionality enables not only foreach support but 
    /// also (and much more importantly) LINQ support!
    /// </summary>
    public class EnumConditionalPoints : EnumConditionalPointRecords, 
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mConditionalPointRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mConditionalPointRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------