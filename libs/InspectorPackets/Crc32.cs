﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Security.Cryptography;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// CRC32 Validation for upload of large media files split
   /// over multiple packets
   /// </summary>
   public class Crc32 : HashAlgorithm
   {
      public const UInt32 DEFAULT_POLYNOMIAL = 0xedb88320;
      public const UInt32 DEFAULT_SEED = 0xffffffff;

      private UInt32 mHash;
      private UInt32 mSeed;
      private UInt32[] mTable;
      private static UInt32[] mDefaultTable;

      public Crc32()
      {
         mTable = InitializeTable( DEFAULT_POLYNOMIAL );
         mSeed = DEFAULT_SEED;
         Initialize();
      }

      public Crc32( UInt32 polynomial, UInt32 seed )
      {
         mTable = InitializeTable( polynomial );
         this.mSeed = seed;
         Initialize();
      }

      public override void Initialize()
      {
         mHash = mSeed;
      }

      protected override void HashCore( byte[] buffer, int start, int length )
      {
         mHash = CalculateHash( mTable, mHash, buffer, start, length );
      }

      protected override byte[] HashFinal()
      {
         byte[] hashBuffer = UInt32ToBigEndianBytes( ~mHash );
         this.HashValue = hashBuffer;
         return hashBuffer;
      }

      public override int HashSize
      {
         get
         {
            return 32;
         }
      }

      public static UInt32 Compute( byte[] buffer )
      {
         return ~CalculateHash( InitializeTable( DEFAULT_POLYNOMIAL ), DEFAULT_SEED, buffer, 0, buffer.Length );
      }

      public static UInt32 Compute( UInt32 seed, byte[] buffer )
      {
         return ~CalculateHash( InitializeTable( DEFAULT_POLYNOMIAL ), seed, buffer, 0, buffer.Length );
      }

      public static UInt32 Compute( UInt32 polynomial, UInt32 seed, byte[] buffer )
      {
         return ~CalculateHash( InitializeTable( polynomial ), seed, buffer, 0, buffer.Length );
      }

      private static UInt32[] InitializeTable( UInt32 polynomial )
      {
         if ( polynomial == DEFAULT_POLYNOMIAL && mDefaultTable != null )
            return mDefaultTable;

         UInt32[] createTable = new UInt32[ 256 ];
         for ( int i = 0; i < 256; i++ )
         {
            UInt32 entry = (UInt32) i;
            for ( int j = 0; j < 8; j++ )
               if ( ( entry & 1 ) == 1 )
                  entry = ( entry >> 1 ) ^ polynomial;
               else
                  entry = entry >> 1;
            createTable[ i ] = entry;
         }

         if ( polynomial == DEFAULT_POLYNOMIAL )
            mDefaultTable = createTable;

         return createTable;
      }

      private static UInt32 CalculateHash( UInt32[] table, UInt32 seed, byte[] buffer, int start, int size )
      {
         UInt32 crc = seed;
         for ( int i = start; i < size; i++ )
            unchecked
            {
               crc = ( crc >> 8 ) ^ table[ buffer[ i ] ^ crc & 0xff ];
            }
         return crc;
      }

      private byte[] UInt32ToBigEndianBytes( UInt32 x )
      {
         return new byte[] {
			(byte)((x >> 24) & 0xff),
			(byte)((x >> 16) & 0xff),
			(byte)((x >> 8) & 0xff),
			(byte)(x & 0xff)
		};
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 2nd October 2011
//  Add to project
//----------------------------------------------------------------------------