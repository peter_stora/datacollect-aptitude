﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage a list of UserPreference records 
   /// </summary>
   public class EnumUserPreferenceRecords : PacketBase
   {
      protected List<UserPreferencesRecord> mPreferencesRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumUserPreferenceRecords()
      {
         mPreferencesRecord = new List<UserPreferencesRecord>();
      }/* end constructor */


      /// <summary>
      /// UserPreferences Table record ( type ::LocalSettingsRecord )
      /// </summary>
      public List<UserPreferencesRecord> UserPreferences
      {
         get
         {
            return mPreferencesRecord;
         }
         set
         {
            mPreferencesRecord = value;
         }
      }


      /// <summary>
      /// Add a new UserPreferences record to the list
      /// </summary>
      /// <param name="item">populated UserPreferences record object</param>
      /// <returns>number of UserPreferences objects available</returns>
      public int AddRecord( UserPreferencesRecord item )
      {
         mPreferencesRecord.Add( item );
         return mPreferencesRecord.Count;
      }


      /// <summary>
      /// How many UserPreferences records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mPreferencesRecord.Count;
         }
      }

   }/* end class */



   /// <summary>
   /// Class EnumUserPreferenceRecords with support for IEnumerable. This 
   /// extended functionality enables not only foreach support - but also 
   /// (and much more importantly) LINQ support!
   /// </summary>
   public class EnumUserPreferences : EnumUserPreferenceRecords, IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mPreferencesRecord.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mPreferencesRecord[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th March 2010
//  Add to project
//----------------------------------------------------------------------------
