﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to represent the CMMS Corrective Action record 
    /// downloaded from @ptitude
    /// </summary>
    public class CmmsCorrectiveActionRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsCorrectiveActionRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Get or set the @ptitude corrctive action key
        /// </summary>
        [XmlAttribute("MACorrActnKey")]
        public int MACorrectiveActionKey { get; set; }

        /// <summary>
        /// Get or set the corrective action text
        /// </summary>
        [XmlAttribute("CorrActText")]
        public string CorrectiveActionText { get; set; }

    }/* end class */


    /// <summary>
    /// Class to represent the CMMS Priority record downloaded 
    /// from @ptitude
    /// </summary>
    public class CmmsPriorityRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsPriorityRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Get or set the @ptitude Priority key
        /// </summary>
        [XmlAttribute("MAPriorityKey")]
        public int MAPriorityKey { get; set; }

        /// <summary>
        /// Get or set the @ptitude Priority code
        /// </summary>
        [XmlAttribute("MAPriorityCode")]
        public string MAPriorityCode { get; set; }

        /// <summary>
        /// Get or set the Priority text field
        /// </summary>
        [XmlAttribute("PriorityText")]
        public string PriorityText { get; set; }

    }/* end class */


    /// <summary>
    /// Class to represent the CMMS Problem record downloaded 
    /// from @ptitude
    /// </summary>
    public class CmmsProblemRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsProblemRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Get or set the @ptitude problem description key
        /// </summary>
        [XmlAttribute("MAProbDescKey")]
        public int MAProblemDescriptionKey { get; set; }
        
        /// <summary>
        /// Get or set the problem text field
        /// </summary>
        [XmlAttribute("ProbText")]
        public string ProblemText { get; set; }

    }/* end class */


    /// <summary>
    /// Class to represent the CMMS Work type record downloaded 
    /// from @ptitude
    /// </summary>
    public class CmmsWorkTypeRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsWorkTypeRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Get or set the @ptitude work type key
        /// </summary>
        [XmlAttribute("MAWorkTypeKey")]
        public int MAWorkTypeKey { get; set; }

        /// <summary>
        /// Get or set the work type text
        /// </summary>
        [XmlAttribute("WorkTypeText")]
        public string WorkTypeText { get; set; }

    }/* end class */ 

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------
