﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single Upload Notes record, that is added to the XML upload file
   /// whenever notes have been added to a measurement point
   /// </summary>
   public class UploadNoteRecord
   {
      #region Private fields

      /// <summary>
      /// List of selected note codes
      /// </summary>
      private EnumUploadCodedNotes mCodedNotes = new EnumUploadCodedNotes( );
      
      /// <summary>
      /// Has this not been touched?
      /// </summary>
      private Boolean mTouchedFlag;

      /// <summary>
      /// Freehand note text
      /// </summary>
      private string mNoteText;

      /// <summary>
      /// Current note type (measurement, Adhoc, Compliance)
      /// </summary>
      private byte mNoteType;

      /// <summary>
      /// Current Operator ID
      /// </summary>
      private int mOperatorId;
      
      /// <summary>
      /// Collection DateTime for a measurement note
      /// </summary>
      private DateTime mCollectionStamp;

      /// <summary>
      /// When was this note last modified?
      /// </summary>
      private DateTime mLastModified;
      
      /// <summary>
      /// Note Uid field (not to be exposed via property)
      /// </summary>
      private Guid mNoteUid = Guid.Empty;

      #endregion


      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public UploadNoteRecord()
      {
         ClearNote();
      }/* end constructor */


      /// <summary>
      /// Gets or sets the Note Type
      /// </summary>
      [XmlElement( "NoteType" )]
      public byte NoteType
      {
         get
         {
            return mNoteType;
         }
         set
         {
            mTouchedFlag = true;
            mNoteType = value;
         }
      }


      /// <summary>
      /// Node Descriptor field (i.e. Hierarchies, NONROUTE, Routes etc) 
      /// From Analyst [Point Properties]: Name
      /// </summary>
      [XmlElement( "TextNote" )]
      public string TextNote
      {
         get
         {
            return mNoteText;
         }
         set
         {
            mTouchedFlag = true;
            mNoteText = value;
         }
      }


      /// <summary>
      /// Get or set any coded notes 
      /// </summary>
      [XmlElement( "CodedNotes" )]
      public EnumUploadCodedNotes CodedNotes
      {
         get
         {
            return mCodedNotes;
         }
         set
         {
            mTouchedFlag = true;
            mCodedNotes = value;
         }
      }


      /// <summary>
      /// Gets or sets the measurement Time Stamp
      /// </summary>
      [XmlElement( "CollectionStamp" )]
      public DateTime CollectionStamp
      {
         get
         {
            return mCollectionStamp;
         }
         set
         {
            mTouchedFlag = true;
            mCollectionStamp = value;
         }
      }


      /// <summary>
      /// Gets or sets the Point Modify Time Stamp
      /// </summary>
      [XmlElement( "LastModified" )]
      public DateTime LastModified
      {
         get
         {
            return mLastModified;
         }
         set
         {
            mTouchedFlag = true;
            mLastModified = value;
         }
      }


      /// <summary>
      /// Gets or sets the Operator Id
      /// </summary>
      [XmlElement( "OperId" )]
      public int OperId
      {
         get
         {
            return mOperatorId;
         }
         set
         {
            mTouchedFlag = true;
            mOperatorId = value;
         }
      }


      /// <summary>
      /// Add a new Coded Note string to the list
      /// </summary>
      /// <param name="item">populated Coded Note record object</param>
      /// <returns>number of Coded Note objects available</returns>
      public int AddCodedNote( UploadCodedNote item )
      {
         mTouchedFlag = true;

         if ( mCodedNotes == null )
            mCodedNotes = new EnumUploadCodedNotes();

         mCodedNotes.AddUploadCodedNote( item );
         return mCodedNotes.Count;

      }/* end method */


      /// <summary>
      /// Add a new Coded Note string to the list
      /// </summary>
      /// <param name="item">populated Coded Note record object</param>
      /// <returns>number of Coded Note objects available</returns>
      public int AddCodedNote( int iCode, string iText )
      {
         mTouchedFlag = true;

         if ( mCodedNotes == null )
            mCodedNotes = new EnumUploadCodedNotes();

         UploadCodedNote codedNote = new UploadCodedNote();
         codedNote.Text = iText;
         codedNote.Code = iCode;

         mCodedNotes.AddUploadCodedNote( codedNote );
         return mCodedNotes.Count;

      }/* end method */


      /// <summary>
      /// Add a single coded note record to the upload codes
      /// </summary>
      /// <param name="iCodedNote">populated code record</param>
      public void AddCodedNote( CodedNoteRecord iCodedNote )
      {
         mTouchedFlag = true;

         if ( iCodedNote != null )
         {
            AddCodedNote( iCodedNote.Code, iCodedNote.Text );
         }

      }/* end method */


      /// <summary>
      /// Clear all existing note codes
      /// </summary>
      public void ClearCodedNotes()
      {
         if ( mCodedNotes != null )
         {
            for ( int i=0; i < mCodedNotes.Count; ++i )
            {
               mCodedNotes.CodedNote[ i ] = null;
            }
            mCodedNotes.CodedNote.Clear();
         }

      }/* end method */


      /// <summary>
      /// Reset all object fields
      /// </summary>
      public void ClearNote()
      {
         // set all fields to their default values
         mNoteType = (byte) FieldTypes.NoteType.AdHoc;
         mCollectionStamp = DateTime.MinValue;
         mLastModified = DateTime.MinValue;
         mNoteText = string.Empty;
         mOperatorId = 0;

         // clear the coded notes
         ClearCodedNotes();

         // reset the dirty flag
         mTouchedFlag = false;

      }/* end method */


      /// <summary>
      /// Copy the contents of an existing note object into this note
      /// object. Note: This method will always reset the fields of
      /// the current note object
      /// </summary>
      /// <param name="iNoteRecord">object to copy from</param>
      public void CopyFrom( UploadNoteRecord iNoteRecord )
      {
         // empty the current note object contents (reset to default)
         this.ClearNote();

         // if the note parameter is not null then copy to local
         if ( iNoteRecord != null )
         {
            // reference the note text
            this.TextNote = iNoteRecord.TextNote;

            // reference all note codes
            if ( iNoteRecord.CodedNotes != null )
            {
               for ( int i=0; i < iNoteRecord.CodedNotes.Count; ++i )
               {
                  this.AddCodedNote( iNoteRecord.CodedNotes.CodedNote[ i ] );
               }
            }

            // reference the remaining fields object fields
            this.OperId = iNoteRecord.OperId;
            this.NoteType = iNoteRecord.NoteType;
            this.SetNoteUid( iNoteRecord.GetNoteUid() );
            this.LastModified = iNoteRecord.LastModified;
            this.CollectionStamp = iNoteRecord.CollectionStamp;
            mTouchedFlag = iNoteRecord.NoteTouched();
         }

      }/* end method */


      /// <summary>
      /// Has the current notes object been edited?
      /// Note: The edit field is returned as a method (not a property) 
      /// since all properties of this object are serializable.
      /// </summary>
      /// <returns>true if yes, else false</returns>
      public Boolean NoteTouched()
      {
         return mTouchedFlag;

      }/* end method */


      /// <summary>
      /// Set the internal Note Uid field
      /// </summary>
      /// <param name="iNoteUid">local note Uid</param>
      public void SetNoteUid( Guid iNoteUid )
      {
         mNoteUid = iNoteUid;

      }/* end method */


      /// <summary>
      /// Get the internal Note Uid field
      /// </summary>
      /// <returns>local note UId</returns>
      public Guid GetNoteUid()
      {
         return mNoteUid;

      }/* end method */

   }/* end class */


   /// <summary>
   /// This class is a simple enumeration of the UploadCodedNote object
   /// </summary>
   public class EnumUploadCodedNotes
   {
      private List<UploadCodedNote> mUploadCodedNotes;

      /// <summary>
      /// Constructor (no oveloads)
      /// </summary>
      public EnumUploadCodedNotes()
      {
         mUploadCodedNotes = new List<UploadCodedNote>();
      }/* end constructor */


      /// <summary>
      /// Get or set any coded notes 
      /// </summary>
      [XmlElement( "CodedNote" )]
      public List<UploadCodedNote> CodedNote
      {
         get
         {
            return mUploadCodedNotes;
         }
         set
         {
            mUploadCodedNotes = value;
         }
      }


      /// <summary>
      /// Add a new Coded Note string to the list
      /// </summary>
      /// <param name="item">populated Coded Note record object</param>
      /// <returns>number of Coded Note objects available</returns>
      public int AddUploadCodedNote( UploadCodedNote item )
      {
         mUploadCodedNotes.Add( item );
         return mUploadCodedNotes.Count;
      }

      /// <summary>
      /// How many Coded Note strings are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mUploadCodedNotes.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Single Coded Note object entry that forms the lowest element in the
   /// XML Notes record that is returned to the Middleware Device Server as
   /// part of the XML Upload file. This object should not be referenced 
   /// by any class other than EnumUploadCodedNotes
   /// </summary>
   public class UploadCodedNote
   {
      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public UploadCodedNote()
      {
      }/* end constructor */

      /// <summary>
      /// Get or set the actual note code
      /// </summary>
      [XmlAttribute( "Code" )]
      public int Code
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the actual note text
      /// </summary>
      [XmlAttribute( "Text" )]
      public string Text
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 23rd July 2010
//  Add support for Note edits by exposing the Note Uid
//
//  Revision 0.0 APinkerton 30th April 2009
//  Add to project
//----------------------------------------------------------------------------
