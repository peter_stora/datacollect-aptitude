﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Authenticate me reference class
    /// </summary>
    public class Authentication : PacketBase
    {
        public string Key { get; set; }

        /// <summary>
        /// Base Constructor
        /// </summary>
        public Authentication()
            : this(NULL_KEY)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iUID">Device UID</param>
        /// <param name="iKey">Server Authentication Key</param>
        public Authentication(string iKey)
        {
            this.Key = iKey;
        }/* end constructor */

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------