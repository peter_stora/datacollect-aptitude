﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to both compress and decompress large binary and text objects
   /// using the standard .Net compression libraries
   /// </summary>
   public class Compression
   {
      /// <summary>
      /// Compress contents of the input byte array buffer using the native
      /// .Net GZipStream compression resources
      /// </summary>
      /// <param name="iBuffer">uncompressed text buffer</param>
      /// <returns>compressed buffer as string</returns>
      public static string Compress( string iBuffer )
      {
         byte[] buffer = Encoding.UTF8.GetBytes( iBuffer );
         MemoryStream ms = new MemoryStream();
         using ( GZipStream zip = new GZipStream( ms, CompressionMode.Compress, true ) )
         {
            zip.Write( buffer, 0, buffer.Length );
         }

         ms.Position = 0;
         MemoryStream outStream = new MemoryStream();

         byte[] compressed = new byte[ ms.Length ];
         ms.Read( compressed, 0, compressed.Length );

         byte[] gzBuffer = new byte[ compressed.Length + 4 ];

         System.Buffer.BlockCopy( compressed, 0, gzBuffer, 4, compressed.Length );
         System.Buffer.BlockCopy( BitConverter.GetBytes( buffer.Length ), 0, gzBuffer, 0, 4 );

         return Convert.ToBase64String( gzBuffer );
      }/* end method */


      /// <summary>
      /// Uncompress contents of the input byte array buffer using the native
      /// .Net GZipStream decompression resources
      /// </summary>
      /// <param name="iGzBuffer">Compressed text buffer</param>
      /// <returns>Uncompressed buffer as string</returns>
      public static string Decompress( string iGzBuffer )
      {
         byte[] gzBuffer = Convert.FromBase64String( iGzBuffer );
         using ( MemoryStream ms = new MemoryStream() )
         {
            int msgLength = BitConverter.ToInt32( gzBuffer, 0 );
            ms.Write( gzBuffer, 4, gzBuffer.Length - 4 );

            byte[] buffer = new byte[ msgLength ];

            ms.Position = 0;
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Decompress ) )
            {
               zip.Read( buffer, 0, buffer.Length );
            }

            return Encoding.UTF8.GetString( buffer, 0, buffer.Length );
         }
      }/* end method */


      /// <summary>
      /// Compresses the contents of an XML cache file and returns the
      /// contents as an array of bytes.
      /// </summary>
      /// <param name="iFileName">name and path to cache file</param>
      /// <returns>cache file compressed as byte array</returns>
      public static byte[] CompressFile( string iFileName )
      {
         // create managed memory stream buffer
         using ( FileStream fs = new FileStream( iFileName,
               FileMode.Open, FileAccess.ReadWrite ) )
         {
            // Set the buffer size to the size of the file
            byte[] bufferWrite = new byte[ fs.Length ];

            // Read the data from the stream into the buffer
            fs.Read( bufferWrite, 0, bufferWrite.Length );

            // Open the FileStream to write to
            using ( MemoryStream cacheStream = new MemoryStream() )
            {
               // Will hold the compressed stream created from the destination stream
               using ( GZipStream gzCompressed = new GZipStream( 
                  cacheStream, CompressionMode.Compress, true ) )
               {
                  // Write the compressed stream from the bytes array to a file
                  gzCompressed.Write( bufferWrite, 0, bufferWrite.Length );

                  // close the compression
                  gzCompressed.Close();
               }

               // reset the memory stream pointer
               cacheStream.Position = 0;

               // create output output buffer for the compressed memory stream
               byte[] compressed = new byte[ cacheStream.Length ];
               cacheStream.Read( compressed, 0, compressed.Length );

               // convert to a formatted output byte array
               byte[] gzBuffer = new byte[ compressed.Length + 4 ];
               System.Buffer.BlockCopy( 
                  compressed, 0, gzBuffer, 4, compressed.Length );
               System.Buffer.BlockCopy( 
                  BitConverter.GetBytes( bufferWrite.Length ), 0, gzBuffer, 0, 4 );

               // reset and close the file stream
               fs.Flush();
               fs.Close();

               // reset and close the cache stream
               cacheStream.Flush();
               cacheStream.Close();

               // clear the unwanted buffers
               ByteArrayProcess.ClearByteBuffer( ref compressed );
               ByteArrayProcess.ClearByteBuffer( ref bufferWrite );

               // and return!
               return gzBuffer;
            }
         }

      }/* end method */


      /// <summary>
      /// Compress contents of the input byte array buffer using the native
      /// .Net GZipStream compression resources
      /// </summary>
      /// <param name="iBuffer">Uncompressed byte array</param>
      /// <returns>Compressed byte array</returns>
      public static byte[] Compress( byte[] iBuffer )
      {
         // create managed memory stream buffer
         using ( MemoryStream ms = new MemoryStream() )
         {
            //
            // create managed .Net ZIP stream object and write the input
            // byte buffer to the memory stream
            //
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Compress, true ) )
            {
               zip.Write( iBuffer, 0, iBuffer.Length );
            }

            // reset the memory stream pointer
            ms.Position = 0;

            // create output output buffer for the compressed memory stream
            byte[] compressed = new byte[ ms.Length ];
            ms.Read( compressed, 0, compressed.Length );

            // convert to a formatted output byte array
            byte[] gzBuffer = new byte[ compressed.Length + 4 ];
            System.Buffer.BlockCopy( compressed, 0, gzBuffer, 4, compressed.Length );
            System.Buffer.BlockCopy( BitConverter.GetBytes( iBuffer.Length ), 0, gzBuffer, 0, 4 );

            // and return!
            return gzBuffer;
         }

      }/* end method */


      /// <summary>
      /// Uncompress contents of the input byte array buffer using the native
      /// .Net GZipStream decompression resources
      /// </summary>
      /// <param name="iGzBuffer">Array of GZipped bytes</param>
      /// <returns>uncompressed array of bytes</returns>
      public static byte[] Decompress( byte[] iGzBuffer )
      {
         // create managed memory stream buffer 
         using ( MemoryStream ms = new MemoryStream() )
         {
            // get the uncompressed length of the input buffer
            int msgLength = BitConverter.ToInt32( iGzBuffer, 0 );

            // write input buffer to managed memory stream
            ms.Write( iGzBuffer, 4, iGzBuffer.Length - 4 );

            // allocate new array of bytes for uncompressed array
            byte[] buffer = new byte[ msgLength ];

            // reset the memory pointer
            ms.Position = 0;

            // create managed GZipStream object and uncompress input buffer
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Decompress ) )
            {
               zip.Read( buffer, 0, buffer.Length );
            }

            // return uncompressed data
            return buffer;
         }
      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------
