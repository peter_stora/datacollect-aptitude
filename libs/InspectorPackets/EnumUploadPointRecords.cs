﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Upload Points Table records 
    /// </summary>
    public class EnumUploadPointRecords
    {
        protected List<UploadPointRecord> mUploadPointsRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumUploadPointRecords()
        {
            mUploadPointsRecord = new List<UploadPointRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw PointRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("POINT")]
        public List<UploadPointRecord> POINT
        {
            get { return mUploadPointsRecord; }
            set { mUploadPointsRecord = value; }
        }


        /// <summary>
        /// Add a new Point record to the list
        /// </summary>
        /// <param name="item">populated Node record object</param>
        /// <returns>number of Node objects available</returns>
        public int AddPointsRecord(UploadPointRecord item)
        {
            mUploadPointsRecord.Add(item);
            return mUploadPointsRecord.Count;
        }/* end method */


        /// <summary>
        /// How many Point records are now available
        /// </summary>
        public int Count { get { return mUploadPointsRecord.Count; } }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
