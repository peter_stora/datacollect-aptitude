﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class provides a pair of static methods that allow both
   /// serialization and deserialization of binary streams into and
   /// out of C# data structures (struct)
   /// </summary>
   public class BinaryIO
   {
      /// <summary>
      /// Parse any struct into an array of bytes
      /// </summary>
      /// <param name="anything">populated data structure</param>
      /// <returns>array of bytes</returns>
      public static byte[] RawSerialize( object iAnything )
      {
         int rawsize =
                Marshal.SizeOf( iAnything );
         byte[] rawdata = new byte[ rawsize ];
         GCHandle handle =
                GCHandle.Alloc( rawdata,
             GCHandleType.Pinned );
         Marshal.StructureToPtr( iAnything,
             handle.AddrOfPinnedObject(),
             false );
         handle.Free();
         return rawdata;

      }/* end method */


      /// <summary>
      /// Parse an array of bytes into a specified data struct
      /// </summary>
      /// <param name="buffer">byte array</param>
      /// <param name="t">type of struct</param>
      /// <returns>populated struct</returns>
      public static object ReadStructFromBuffer( byte[] iBuffer, Type t )
      {
         GCHandle handle =
                GCHandle.Alloc( iBuffer,
             GCHandleType.Pinned );
         Object temp =
                Marshal.PtrToStructure(
             handle.AddrOfPinnedObject(),
             t );
         handle.Free();
         return temp;

      } /* end method */


      /// <summary>
      /// Return the number of bytes associated with the struct
      /// </summary>
      /// <returns></returns>
      public static int GetBytes( object iAnything )
      {
         return Marshal.SizeOf( iAnything );
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2010
//  Add to project
//----------------------------------------------------------------------------
