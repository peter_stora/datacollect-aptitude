﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Request a new Operator Set for the Server
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketUpdateOperators : PacketBase
    {
        /// <summary>
        /// Packet Type "Update Profile"
        /// </summary>
        public byte Packet = (byte)PacketType.UPDATE_OPERATORS;

        /// <summary>
        /// Session Identifier
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// Operator Set associated with the current device profile
        /// </summary>
        public int OperatorSetId { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd February 2009
//  Add to project
//----------------------------------------------------------------------------