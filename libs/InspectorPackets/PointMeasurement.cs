﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2008 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage a single measurement value
   /// </summary>
   public class PointMeasurement
   {
      /// <summary>
      /// Numeric measurement value
      /// </summary>
      private double mValue = double.NaN;
      
      /// <summary>
      /// Text measurement value
      /// </summary>
      private string mTextValue = string.Empty;
      
      /// <summary>
      /// Is the current meausurement Numeric or Text?
      /// </summary>
      private Boolean mNumeric = true;

      /// <summary>
      /// Constructor
      /// </summary>
      public PointMeasurement()
      {
      }/* end constructor */

      /// <summary>
      /// Get or set the collected value timestamp
      /// </summary>
      [XmlAttribute( "LocalTime" )]
      public DateTime LocalTime
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the collected Value
      /// </summary>
      [XmlAttribute( "Value" )]
      public Double Value
      {
         get
         {
            return mValue;
         }
         set
         {
            mNumeric = true;
            mValue = value;
         }
      }

      /// <summary>
      /// Get or set the collected Value
      /// </summary>
      [XmlAttribute( "TextValue" )]
      public string TextValue
      {
         get
         {
            return mTextValue;
         }
         set
         {
            mNumeric = false;
            mTextValue = value;
         }
      }

      /// <summary>
      /// Is this a numeric or a text measurement?
      /// </summary>
      /// <returns></returns>
      public Boolean Is_Numeric_Measurement()
      {
         return mNumeric;
      }

   }/* end class */


   /// <summary>
   /// Extend PointMeasuremnt to include alarm status and operator Id
   /// </summary>
   public class UploadMeasurement : PointMeasurement
   {
      /// <summary>
      /// Get or set the associated alarm state
      /// </summary>
      [XmlAttribute( "AlarmState" )]
      public byte AlarmState
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the collection status
      /// </summary>
      [XmlAttribute( "Status" )]
      public byte Status
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Operator ID
      /// </summary>
      [XmlAttribute( "OperId" )]
      public int OperId
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Extend PointMeasurement Class to include Point Uid and Index
   /// </summary>
   public class DownloadMeasurement : UploadMeasurement
   {
      /// <summary>
      /// Get or set the Point Uid
      /// </summary>
      [XmlAttribute( "PointUid" )]
      public Guid PointUid
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Point Host (or PRISM) index
      /// </summary>
      [XmlAttribute( "PointHostIndex" )]
      public int PointHostIndex
      {
         get;
         set;
      }

      /// <summary>
      /// Return the base upload object
      /// </summary>
      /// <returns>Populated UploadMeasurement object</returns>
      public UploadMeasurement GetUploadMeasurent( byte iStatusMask )
      {
         UploadMeasurement measurement = new UploadMeasurement();
         measurement.AlarmState = base.AlarmState;
         measurement.OperId = base.OperId;
         measurement.LocalTime = base.LocalTime;
         measurement.Value = base.Value;
         measurement.TextValue = base.TextValue;
         measurement.Status = (byte) ( iStatusMask & base.Status );
         return measurement;
      }

   }/* end class */


   /// <summary>
   /// Extend PointMeasurement Class to include Point Uid and Index
   /// </summary>
   public class SavedMeasurement : DownloadMeasurement
   {
      /// <summary>
      /// Get or set the New Flag
      /// </summary>
      [XmlAttribute( "New" )]
      public Boolean New
      {
         get;
         set;
      }

      /// <summary>
      /// Return the base download object
      /// </summary>
      /// <returns>Populated DownloadMeasurement object</returns>
      public DownloadMeasurement GetDownloadMeasurent()
      {
         DownloadMeasurement measurement = new DownloadMeasurement();
         measurement.AlarmState = base.AlarmState;
         measurement.OperId = base.OperId;
         measurement.LocalTime = base.LocalTime;
         measurement.Value = base.Value;
         measurement.TextValue = base.TextValue;
         measurement.Status = base.Status;
         measurement.PointUid = base.PointUid;
         measurement.PointHostIndex = base.PointHostIndex;
         return measurement;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 17th March 2010
//  Add support for Operator Id
//
//  Revision 1.0 APinkerton 3rd February 2010
//  Add support for packet DownloadMeasurement
//
//  Revision 0.0 APinkerton 4th March 2009
//  Add to project
//----------------------------------------------------------------------------