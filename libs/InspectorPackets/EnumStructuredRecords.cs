﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of STRUCTURED Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumStructuredRecords : PacketBase
    {
        protected List<StructuredRecord> mStructuredRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumStructuredRecords()
        {
            mStructuredRecord = new List<StructuredRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw StructuredRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("STRUCTURED")]
        public List<StructuredRecord> Structured
        {
            get { return mStructuredRecord; }
            set { mStructuredRecord = value; }
        }


        /// <summary>
        /// Add a new Structured record to the list
        /// </summary>
        /// <param name="item">populated Structured record object</param>
        /// <returns>number of Structured objects available</returns>
        public int AddStructuredRecord(StructuredRecord item)
        {
            mStructuredRecord.Add(item);
            return mStructuredRecord.Count;
        }

        /// <summary>
        /// How many Structured records are now available
        /// </summary>
        public int Count { get { return mStructuredRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumStructuredRecords with support for IEnumerable. 
    /// This extended functionality enables not only foreach support but 
    /// also (and much more importantly) LINQ support!
    /// </summary>
    public class EnumStructuredRoutes : EnumStructuredRecords,
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mStructuredRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mStructuredRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------
