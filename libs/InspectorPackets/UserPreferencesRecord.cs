﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;


namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This simple class manages each operators local settings. On synch, a 
   /// series of images will be uploaded to the device along with the rest
   /// of the standard operator settings. Then, on logging in, this image 
   /// will be deserialised and used to populate that operators settings.
   /// Any updates made by the operator, will be saved to this table and 
   /// the check flag (Changed) will be set true
   /// </summary>
   [XmlRoot( ElementName = "UserPreferencesRecord" )] 
   public class UserPreferencesRecord : PacketBase
   {
      private int mOperatorID = 0;
      private Boolean mChanged = false;
      private ResetPassword mResetPassword = ResetPassword.None;
      private byte[] mSettings;

      /// <summary>
      /// Gets or sets the current Operator ID
      /// </summary>
      public int OperatorId
      {
         get{
            return mOperatorID;
         }
         set
         {
            mOperatorID = value;
         }
      }

      /// <summary>
      /// Gets or sets whether the user has changed their HKCU settings
      /// </summary>
      public Boolean Changed
      {
         get
         {
            return mChanged;
         }
         set
         {
            mChanged = value;
         }
      }

      //<summary>
      //Gets or sets a binary image of the HKCU settings
      //</summary>
      public byte[] BinaryPreferencesBlob
      {
         get
         {
            return mSettings;
         }
         set
         {
            mSettings = value;
         }
      }


      /// <summary>
      /// Gets or sets the "Password Reset" flag
      /// </summary>
      public ResetPassword ResetPassword
      {
         get
         {
            return mResetPassword;
         }
         set
         {
            mResetPassword = value;
         }
      }


      /// <summary>
      /// Base Constructor
      /// </summary>
      public UserPreferencesRecord()
      {
         mSettings = new byte[ PacketBase.DEFAULT_USER_SETTING_BYTES ];
      
      }/* end constructor */


      /// <summary>
      /// Returns the current user preferences object 
      /// </summary>
      /// <returns></returns>
      public UserPreferencesObject GetPreferences()
      {
         object settings = BinaryIO.ReadStructFromBuffer( 
            mSettings, 
            typeof( UserPreferencesStruct ) );

         return new UserPreferencesObject( (UserPreferencesStruct) settings );

      }/* end method */


      /// <summary>
      /// Set the preferences object
      /// </summary>
      /// <param name="iSettings"></param>
      public void SetPreferences( UserPreferencesObject iSettings )
      {
         UserPreferencesStruct settings = iSettings.GetSettingsStruct();
         mSettings = BinaryIO.RawSerialize( settings );

      }/* end method */


      /// <summary>
      /// Return the number of bytes associated with the settings structure
      /// </summary>
      /// <returns></returns>
      public static int GetSettingsBytes()
      {
         return PacketBase.DEFAULT_USER_SETTING_BYTES;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2010
//  Add to project
//----------------------------------------------------------------------------

