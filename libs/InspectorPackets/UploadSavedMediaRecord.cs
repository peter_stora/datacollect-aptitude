﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{

   /// <summary>
   /// Base media class for SavedMedia record that only includes those
   /// fields to be uploaded to @ptitude
   /// </summary>
   public class UploadSavedMediaRecord
   {
      /// <summary>
      /// Default constructor for this class (required for serialization).
      /// </summary>
      public UploadSavedMediaRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Get or set the record Identifier
      /// </summary>
      [XmlElement( "Uid" )]
      public Guid Uid
      {
         get;
         set;
      }


      /// <summary>
      /// Get or Set the @ptitude TreeElemID
      /// </summary>
      [XmlElement( "PointHostIndex" )]
      public int PointHostIndex
      {
         get;
         set;
      }


      /// <summary>
      /// Get or Set the Image Date and Time Stamp
      /// </summary>
      [XmlElement( "TimeStamp" )]
      public DateTime TimeStamp
      {
         get;
         set;
      }


      /// <summary>
      /// Get or Set any GPS based Universal Transverse Mercator 
      /// </summary>
      [XmlElement( "UTM" )]
      public string UTM
      {
         get;
         set;
      }


      /// <summary>
      /// Media type: 0x00: IMAGE
      ///             0x01: AUDIO
      ///             0x02: BINARY
      /// </summary>
      [XmlElement( "MediaType" )]
      public byte MediaType
      {
         get;
         set;
      }


      /// <summary>
      /// Media origin: 0x00: Off Route
      ///               0x01: Point
      ///               0x02: Machine
      ///               0x03: Work Notification
      /// </summary>
      [XmlElement( "MediaOrigin" )]
      public byte MediaOrigin
      {
         get;
         set;
      }


      /// <summary>
      /// Get or Set the Work Notification ID
      /// </summary>
      [XmlElement( "WNID" )]
      public int WNID
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the source file name
      /// </summary>
      [XmlElement( "FileName" )]
      public string FileName
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the note text associated with the source file
      /// </summary>
      [XmlElement( "Note" )]
      public string Note
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the Operator ID
      /// </summary>
      [XmlElement( "OperId" )]
      public int OperId
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the size in bytes of the source file
      /// </summary>
      [XmlElement( "Size" )]
      public long Size
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the CRC32 value associated with this image
      /// </summary>
      [XmlElement( "CRC32" )]
      public string CRC32
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15 September 2011
//  Update for Inspector Version 2.0
//
//  Revision 0.0 APinkerton 8th June 2009
//  Add to project
//-------------------------------------------------------------------------------