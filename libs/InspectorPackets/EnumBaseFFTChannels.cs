﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Upload FFT Channels
    /// </summary>
    public class BaseFFTChannels
    {
        protected List<FFTChannelBase> mFFTChannels;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseFFTChannels()
        {
            mFFTChannels = new List<FFTChannelBase>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw FFTChannelBase object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("Channel")]
        public List<FFTChannelBase> FFTChannel
        {
            get { return mFFTChannels; }
            set { mFFTChannels = value; }
        }

        /// <summary>
        /// Add a new FFTChannel to the FFTChannels list
        /// </summary>
        /// <param name="item">FFT Channel item</param>
        /// <returns>number or FFT Channels</returns>
        public int AddFFTChannel(FFTChannelBase item)
        {
            mFFTChannels.Add(item);
            return mFFTChannels.Count;
        }

        /// <summary>
        /// How many FFT Channels are now available
        /// </summary>
        public int Count { get { return mFFTChannels.Count; } }

    }/* end class */


    /// <summary>
    /// Class BaseFFTChannels with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumBaseFFTChannels : BaseFFTChannels, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mFFTChannels.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mFFTChannels[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 27th May 2009
//  Add to project
//----------------------------------------------------------------------------

