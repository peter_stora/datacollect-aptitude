﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{

    /// <summary>
    /// Enumeration of FFT Point object records
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class FFTChannelRecords
    {
        protected List<FFTChannelRecord> mFFTChannelRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public FFTChannelRecords()
        {
            mFFTChannelRecord = new List<FFTChannelRecord>();
        }/* end constructor */

        /// <summary>
        /// Get or set elements in the raw FFT Channel Record object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("FFTChannel")]
        public List<FFTChannelRecord> FFTChannel
        {
            get { return mFFTChannelRecord; }
            set { mFFTChannelRecord = value; }
        }

        /// <summary>
        /// Add a new FFT Point record to the list
        /// </summary>
        /// <param name="item">populated FFT Channel record object</param>
        /// <returns>number of FFT Channel objects available</returns>
        public int AddFFTChannelRecord(FFTChannelRecord item)
        {
            mFFTChannelRecord.Add(item);
            return mFFTChannelRecord.Count;
        }

        /// <summary>
        /// How many FFT Channel records are now available
        /// </summary>
        public int Count { get { return mFFTChannelRecord.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumNodeRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumFFTChannelRecords : FFTChannelRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mFFTChannelRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mFFTChannelRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------
