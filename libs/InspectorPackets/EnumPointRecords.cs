﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage an enumerated list of Points Table records 
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumPointRecords : PacketBase
   {
      protected List<PointRecord> mPointsRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumPointRecords()
      {
         mPointsRecord = new List<PointRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw PointRecord object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "POINTS" )]
      public List<PointRecord> POINTS
      {
         get
         {
            return mPointsRecord;
         }
         set
         {
            mPointsRecord = value;
         }
      }


      /// <summary>
      /// Add a new Point record to the list
      /// </summary>
      /// <param name="item">populated Node record object</param>
      /// <returns>number of Node objects available</returns>
      public int AddPointsRecord( PointRecord item )
      {
         mPointsRecord.Add( item );
         return mPointsRecord.Count;
      }/* end method */

      /// <summary>
      /// How many Point records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mPointsRecord.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Class EnumPointRecords with support for IEnumerable. This extended
   /// functionality enables not only foreach support - but also (and much
   /// more importantly) LINQ support!
   /// </summary>
   public class EnumPoints : EnumPointRecords, IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mPointsRecord.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mPointsRecord[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------