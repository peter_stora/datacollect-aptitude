﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Enumeration of upload setting records (does not support
   /// the IEnumerated interface)
   /// </summary>
   public class EnumUploadSettingRecords
   {
      protected List<UploadSettings> mSettings;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumUploadSettingRecords()
      {
         mSettings = new List<UploadSettings>();
      }/* end constructor */

      /// <summary>
      /// Get or set elements in the raw Media Record object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "Settings" )]
      public List<UploadSettings> Settings
      {
         get
         {
            return mSettings;
         }
         set
         {
            mSettings = value;
         }
      }

      /// <summary>
      /// Add a new Media record to the list
      /// </summary>
      /// <param name="item">populated Media record object</param>
      /// <returns>number of Media objects available</returns>
      public int AddRecord( UploadSettings item )
      {
         mSettings.Add( item );
         return mSettings.Count;
      }

      /// <summary>
      /// How many Media records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mSettings.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Enumeration of UploadSetting records that fully supports
   /// the IEnumerable interface
   /// </summary>
   public class EnumUploadSettings :
       EnumUploadSettingRecords, IEnumerable, IEnumerator
   {
      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mSettings.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mSettings[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

   }/* end class */


   /// <summary>
   /// Very simple upload class for Operator Settings. 
   /// </summary>
   public class UploadSettings
   {
      /// <summary>
      /// Default constructor for this class (required for serialization).
      /// </summary>
      public UploadSettings()
      {
      }/* end constructor */

      [XmlElement( "OperId" )]
      public int OperId
      {
         get;
         set;
      }

      [XmlElement( "UserPreferences" )]
      public byte[] UserPreferences
      {
         get;
         set;
      }

      [XmlElement( "Password" )]
      public string Password
      {
         get;
         set;
      }

      [XmlElement( "ResetPassword" )]
      public byte ResetPassword
      {
         get;
         set;
      }


   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th July 2009
//  Add to project
//----------------------------------------------------------------------------