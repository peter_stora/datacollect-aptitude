﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Text Data Table Packet
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketTextDataTable : PacketBase
   {
      /// <summary>
      /// The actual Packet type (TEXT DATA)
      /// </summary>
      public byte Packet = (byte) PacketType.TABLE_TEXT_DATA;

      /// <summary>
      /// Single Text Data Element
      /// </summary>
      [XmlElement( "TextDataTable" )]
      public EnumTextDataRecords TextDataTable = new EnumTextDataRecords();

      /// <summary>
      /// Constructor
      /// </summary>
      public PacketTextDataTable()
      {
      }/* end constructor */

   }/* end class */
}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 10th September 2012
//  Add to project
//----------------------------------------------------------------------------