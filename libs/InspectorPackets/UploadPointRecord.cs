﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class to manage Point records being returned to @ptitude Analyst
   /// </summary>
   public class UploadPointRecord
   {
      #region Constants and Private Fields

      private const int POINT_INDEX = -1;
      private InspectionRecordBase mRevisedInspection = null;
      private ProcessRecordBase mRevisedProcess = null;
      private MCCRecordBase mRevisedMCC = null;

      private EnumUploadNoteRecords mUploadNotes = null;
      private UploadMeasurements mMeasurements = null;
      private RevisedPointRecord mRevisedPoint = null;
      private SkippedPointRecord mSkippedPoint = null;

      #endregion

      /// <summary>
      /// Base constructor
      /// </summary>
      public UploadPointRecord()
         : this( POINT_INDEX )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iPointHostIndex">Point host (or PRISM) index</param>
      public UploadPointRecord( int iPointHostIndex )
      {
         PointHostIndex = iPointHostIndex;
      }/* end constructor */


      /// <summary>
      /// This is the actual link from the Marlin to Analyst Points 
      /// Analyst Table Link: TREEELEM 
      /// Field Reference: TREEELEMID 
      /// It is identical to the field “PRISM” in the NODE table
      /// </summary>
      [XmlElement( "TreeElemID" )]
      public int PointHostIndex
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Point Process Type
      /// </summary>
      [XmlElement( "ProcessType" )]
      public byte ProcessType
      {
         get;
         set;
      }


      /// <summary>
      /// Collection status
      /// </summary>
      //[XmlElement("Skipped")]
      //public byte Skipped { get; set; }


      /// <summary>
      /// Add a Note Record to the current Upload Point object. This object will contain
      /// not only freehand, but also coded notes as entered by the operator against the
      /// current Point Record object
      /// </summary>
      /// <param name="iNotesRecord">Populated Notes Record object</param>
      /// <param name="iPointHostIndex">The Point index it is to be assigned to</param>
      public void AddPointNotes( SelectedNotesRecord iNotesRecord, int iPointHostIndex )
      {
         if ( mUploadNotes == null )
            mUploadNotes = new EnumUploadNoteRecords();

         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointHostIndex;

         //
         // create a new "upload note record" object. This is a greatly simplified 
         // version of the full note record, and contains only those fields actually
         // required by the Mobile Device Server
         //
         UploadNoteRecord uploadNoteRecord = new UploadNoteRecord();

         // set time that the notes were added
         uploadNoteRecord.LastModified = iNotesRecord.LastModified;

         // add the freehand note text
         uploadNoteRecord.TextNote = iNotesRecord.TextNote;

         // add the operator Id
         uploadNoteRecord.OperId = iNotesRecord.OperId;
         
         // add the note type
         uploadNoteRecord.NoteType = iNotesRecord.NoteType;

         // add the collection stamp
         uploadNoteRecord.CollectionStamp = iNotesRecord.CollectionStamp;

         // add the coded notes assigned to the current point
         for ( int i = 0; i < iNotesRecord.CodedNotes.Count; ++i )
         {
            UploadCodedNote uploadNote = new UploadCodedNote();
            uploadNote.Code = iNotesRecord.CodedNotes.CodedNote[ i ].Code;
            uploadNote.Text = iNotesRecord.CodedNotes.CodedNote[ i ].Text;
            
            uploadNoteRecord.AddCodedNote( uploadNote );
         }

         // add this new upload note to the enumerated list of upload notes
         mUploadNotes.AddUploadNotesRecord( uploadNoteRecord );

      }/* end method */


      /// <summary>
      /// Add a revised Inspection record
      /// </summary>
      /// <param name="iInspectionRecord"></param>
      /// <param name="iPointHostIndex">The Point index it is to be assigned to</param>
      public void AddInspectionRecord( InspectionRecord iInspectionRecord, int iPointHostIndex )
      {
         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointHostIndex;

         // instantiate the object
         mRevisedInspection = new InspectionRecordBase();

         // populate the fields
         mRevisedInspection.AlarmType1 = iInspectionRecord.AlarmType1;
         mRevisedInspection.AlarmType2 = iInspectionRecord.AlarmType2;
         mRevisedInspection.AlarmType3 = iInspectionRecord.AlarmType3;
         mRevisedInspection.AlarmType4 = iInspectionRecord.AlarmType4;
         mRevisedInspection.AlarmType5 = iInspectionRecord.AlarmType5;
         mRevisedInspection.AlarmType6 = iInspectionRecord.AlarmType6;
         mRevisedInspection.AlarmType7 = iInspectionRecord.AlarmType7;
         mRevisedInspection.AlarmType8 = iInspectionRecord.AlarmType8;
         mRevisedInspection.AlarmType9 = iInspectionRecord.AlarmType9;
         mRevisedInspection.AlarmType10 = iInspectionRecord.AlarmType10;
         mRevisedInspection.AlarmType11 = iInspectionRecord.AlarmType11;
         mRevisedInspection.AlarmType12 = iInspectionRecord.AlarmType12;
         mRevisedInspection.AlarmType13 = iInspectionRecord.AlarmType13;
         mRevisedInspection.AlarmType14 = iInspectionRecord.AlarmType14;
         mRevisedInspection.AlarmType15 = iInspectionRecord.AlarmType15;
         mRevisedInspection.AlertText = iInspectionRecord.AlertText;
         mRevisedInspection.Choice1 = iInspectionRecord.Choice1;
         mRevisedInspection.Choice2 = iInspectionRecord.Choice2;
         mRevisedInspection.Choice3 = iInspectionRecord.Choice3;
         mRevisedInspection.Choice4 = iInspectionRecord.Choice4;
         mRevisedInspection.Choice5 = iInspectionRecord.Choice5;
         mRevisedInspection.Choice6 = iInspectionRecord.Choice6;
         mRevisedInspection.Choice7 = iInspectionRecord.Choice7;
         mRevisedInspection.Choice8 = iInspectionRecord.Choice8;
         mRevisedInspection.Choice9 = iInspectionRecord.Choice9;
         mRevisedInspection.Choice10 = iInspectionRecord.Choice10;
         mRevisedInspection.Choice11 = iInspectionRecord.Choice11;
         mRevisedInspection.Choice12 = iInspectionRecord.Choice12;
         mRevisedInspection.Choice13 = iInspectionRecord.Choice13;
         mRevisedInspection.Choice14 = iInspectionRecord.Choice14;
         mRevisedInspection.Choice15 = iInspectionRecord.Choice15;
         mRevisedInspection.DangerText = iInspectionRecord.DangerText;
         mRevisedInspection.NumberOfChoices = iInspectionRecord.NumberOfChoices;
         mRevisedInspection.Question = iInspectionRecord.Question;

      }/* end method */


      /// <summary>
      /// Add a revised Process record
      /// </summary>
      /// <param name="iProcessRecord"></param>
      /// <param name="iPointHostIndex">The Point index it is to be assigned to</param>
      public void AddProcessRecord( ProcessRecord iProcessRecord, int iPointHostIndex )
      {
         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointHostIndex;

         // instantiate the object
         mRevisedProcess = new ProcessRecordBase();

         // populate the fields
         mRevisedProcess.AlarmType = iProcessRecord.AlarmType;
         mRevisedProcess.AutoRange = iProcessRecord.AutoRange;
         mRevisedProcess.Form = iProcessRecord.Form;
         mRevisedProcess.LowerAlert = iProcessRecord.LowerAlert;
         mRevisedProcess.LowerDanger = iProcessRecord.LowerDanger;
         mRevisedProcess.RangeMax = iProcessRecord.RangeMax;
         mRevisedProcess.RangeMin = iProcessRecord.RangeMin;
         mRevisedProcess.Units = iProcessRecord.Units;
         mRevisedProcess.UpperAlert = iProcessRecord.UpperAlert;
         mRevisedProcess.UpperDanger = iProcessRecord.UpperDanger;

      }/* end method */


      /// <summary>
      /// Add a revised MCC record
      /// </summary>
      /// <param name="iMCCRecord"></param>
      /// <param name="iPointHostIndex">The Point index it is to be assigned to</param>
      public void AddMCCRecord( MCCRecord iMCCRecord, int iPointHostIndex )
      {
         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointHostIndex;

         // instantiate the object
         mRevisedMCC = new MCCRecordBase();

         // populate the fields
         mRevisedMCC.EnvAlarm1 = iMCCRecord.EnvAlarm1;
         mRevisedMCC.EnvAlarm2 = iMCCRecord.EnvAlarm2;
         mRevisedMCC.TmpAlarm1 = iMCCRecord.TmpAlarm1;
         mRevisedMCC.TmpAlarm2 = iMCCRecord.TmpAlarm2;
         mRevisedMCC.VelAlarm1 = iMCCRecord.VelAlarm1;
         mRevisedMCC.VelAlarm2 = iMCCRecord.VelAlarm2;
         mRevisedMCC.SystemUnits = iMCCRecord.SystemUnits;
         mRevisedMCC.TempUnits = iMCCRecord.TempUnits;

      }/* end method */


      /// <summary>
      /// Add a revised Point Record (i.e. Alarms, location Tag etc)
      /// </summary>
      /// <param name="iPointRecord">populated point record</param>
      public void AddPointRevisions( PointRecord iPointRecord )
      {
         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointRecord.PointHostIndex;

         mRevisedPoint = new RevisedPointRecord();
         
         mRevisedPoint.Description = iPointRecord.Description;
         mRevisedPoint.Id = iPointRecord.Id;
         mRevisedPoint.LastModified = iPointRecord.LastModified;
         mRevisedPoint.LocationMethod = iPointRecord.LocationMethod;
         mRevisedPoint.LocationTag = iPointRecord.LocationTag;
         mRevisedPoint.ScheduleDays = iPointRecord.ScheduleDays;

      }/* end method */


      /// <summary>
      /// Add a skipped field Point record
      /// </summary>
      /// <param name="iPointRecord">populated point record</param>
      /// <param name="iIgnoreSkippedNone">when true, also include non-collected records</param>
      public void AddPointSkippedField( PointRecord iPointRecord, Boolean iIgnoreSkippedNone )
      {
         // ensure we have a reference for this point
         if ( PointHostIndex < 0 )
            PointHostIndex = iPointRecord.PointHostIndex;

         if ( ( iPointRecord.Skipped > (byte) FieldTypes.SkippedType.None ) || iIgnoreSkippedNone )
         {
            mSkippedPoint = new SkippedPointRecord();
            mSkippedPoint.Skipped = iPointRecord.Skipped;
         }

      }/* end method */


      #region Add new measurement objects

      /// <summary>
      /// Add a generic process measurement to the upload file.  Note although 
      /// the measurement is saved against the "Temp" field type, the additional 
      /// "Vel" and "Env" types are not included in the upload file
      /// </summary>
      /// <param name="iMeasurement">Single upload measurement value</param>
      public void AddMeasurement( UploadMeasurement iMeasurement )
      {
         // if not already instantiated create a new measurement object
         if ( mMeasurements == null )
            mMeasurements = new UploadMeasurements();

         // add the new measurement
         mMeasurements.AddTempMeasurement( iMeasurement );

      }/* end method */


      /// <summary>
      /// Add a temperature measurement as collected by the MCD. Note when a
      /// measurement is added to this field, the "Vel" and "Env" fields are
      /// also instantiated
      /// </summary>
      /// <param name="iMeasurement">Single upload measurement value</param>
      public void AddTempMeasurement( UploadMeasurement iMeasurement )
      {
         // if not already instantiated create a new measurement object
         if ( mMeasurements == null )
            mMeasurements = new UploadMeasurements();

         // add the new measurement
         mMeasurements.AddTempMeasurement( iMeasurement );

      }/* end method */


      /// <summary>
      /// Add a velocity measurement as collected by the MCD. Note when a
      /// measurement is added to this field, the "Temp" and "Env" fields are
      /// also instantiated
      /// </summary>
      /// <param name="iMeasurement">Single upload measurement value</param>
      public void AddVelMeasurement( UploadMeasurement iMeasurement )
      {
         // if not already instantiated create a new measurement object
         if ( mMeasurements == null )
            mMeasurements = new UploadMeasurements();

         // add the new measurement
         mMeasurements.AddVelMeasurement( iMeasurement );

      }/* end method */


      /// <summary>
      /// Add an Envelope measurement as collected by the MCD. Note when a
      /// measurement is added to this field, the "Temp" and "Vel" fields are
      /// also instantiated
      /// </summary>
      /// <param name="iMeasurement">Single upload measurement value</param>
      public void AddEnvMeasurement( UploadMeasurement iMeasurement )
      {
         // if not already instantiated create a new measurement object
         if ( mMeasurements == null )
            mMeasurements = new UploadMeasurements();

         // add the new measurement
         mMeasurements.AddEnvMeasurement( iMeasurement );

      }/* end method */

      #endregion


      #region Dynamic XML elements

      /// <summary>
      /// Get or set the revised Notes record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "Measurements" )]
      public UploadMeasurements Measurements
      {
         get
         {
            return mMeasurements;
         }
         set
         {
            mMeasurements = value;
         }

      }/* end property */


      /// <summary>
      /// Get the revised Notes record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "Notes" )]
      public EnumUploadNoteRecords PointNotes
      {
         get
         {
            return mUploadNotes;
         }
         set
         {
            mUploadNotes = value;
         }
      }/* end property */


      /// <summary>
      /// Get or set the revised Notes record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "RevisedPoint" )]
      public RevisedPointRecord Points
      {
         get
         {
            return mRevisedPoint;
         }
         set
         {
            mRevisedPoint = value;
         }
      }/* end property */


      /// <summary>
      /// Get or set the revised inspection record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "RevisedInspection" )]
      public InspectionRecordBase InspectionRecord
      {
         get
         {
            return mRevisedInspection;
         }
         set
         {
            mRevisedInspection = value;
         }
      }/* end property */


      /// <summary>
      /// Get or set the revised process record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "RevisedProcess" )]
      public ProcessRecordBase ProcessRecord
      {
         get
         {
            return mRevisedProcess;
         }
         set
         {
            mRevisedProcess = value;
         }
      }/* end property */


      /// <summary>
      /// Get or set the revised MCC record. This property will not be
      /// serialized if the local member is null!
      /// </summary>
      [XmlElement( "RevisedMCC" )]
      public MCCRecordBase MCCRecord
      {
         get
         {
            return mRevisedMCC;
         }
         set
         {
            mRevisedMCC = value;
         }
      }/* end property */


      /// <summary>
      /// Get or set the POINTS.Skipped field. This property will not
      /// be serialized if the local member is null!
      /// </summary>
      [XmlElement( "Skipped" )]
      public SkippedPointRecord Skipped
      {
         get
         {
            return mSkippedPoint;
         }
         set
         {
            mSkippedPoint = value;
         }
      }/* end property */


      #endregion


   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 25th April 2012
//  Add support for fifteen inspection choices
//
//  Revision 0.0 APinkerton 28th April 2009
//  Add to project
//----------------------------------------------------------------------------

