﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010-2013 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// This class is the object representation of the structure
   /// "UserPreferencesStruct". Any field can be added, provided
   /// it already exists in its cousin structure.
   /// As with the structure, the field "Version" must not be 
   /// removed, edited or have its byte position moved!
   /// </summary>
   public class UserPreferencesObject
   {
      #region Class Members
      /// <summary>
      /// The user settings struct
      /// </summary>
      private UserPreferencesStruct mSettings;
      #endregion

      #region Properties
      /// <summary>
      /// Gets or sets the current settings version
      /// </summary>
      public int Version
      {
         get{ return mSettings.Version; }
         set{ mSettings.Version = value; }
      }

      /// <summary>
      /// Gets or sets the Font Size
      /// </summary>
      public byte FontSize
      {
         get{ return mSettings.FontSize; }
         set{ mSettings.FontSize = value; }
      }

      /// <summary>
      /// Gets or sets the Alarm Filter
      /// </summary>
      public byte AlarmFilterType
      {
         get{ return mSettings.AlarmFilterType; }
         set{ mSettings.AlarmFilterType = value; }
      }

      /// <summary>
      /// The View Type being used by the operator
      /// </summary>
      public byte ViewType
      {
         get{ return mSettings.ViewType; }
         set{ mSettings.ViewType = value; }
      }
 
      /// <summary>
      /// The Overdue filter type being used by the operator
      /// </summary>
      public byte OverdueFilterType
      {
         get{ return mSettings.OverdueFilterType; }
         set{ mSettings.OverdueFilterType = value; }
      }

      /// <summary>
      /// The Node filter type being used by the operator
      /// </summary>
      public byte NodeFilterType
      {
         get{ return mSettings.NodeFilterType; }
         set{ mSettings.NodeFilterType = value; }
      }

      /// <summary>
      /// Gets or sets the third Byte field 
      /// </summary>
      public byte ByteField3
      {
         get{ return mSettings.ByteField3; }
         set{ mSettings.ByteField3 = value; }
      }

      /// <summary>
      /// Gets or sets the first Boolean flag 
      /// </summary>
      public Boolean HighContrastMode
      {
         get{ return mSettings.HighContrastMode; }
         set{ mSettings.HighContrastMode = value; }
      }
      #endregion

      #region Constructor
      /// <summary>
      /// Base Constructor
      /// </summary>
      public UserPreferencesObject()
      {
         mSettings = new UserPreferencesStruct( 0 );
      }

      /// <summary>
      /// Overload Constructor 
      /// </summary>
      /// <param name="iVersion">Settings Version Number</param>
      public UserPreferencesObject(int iVersion)
      {
         mSettings = new UserPreferencesStruct(iVersion);
         mSettings.Version = iVersion;
      }

      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iSettings">Populated settings object</param>
      public UserPreferencesObject( UserPreferencesStruct iSettings )
      {
         mSettings = new UserPreferencesStruct(iSettings.Version);
         mSettings.FontSize = iSettings.FontSize;
         mSettings.AlarmFilterType = iSettings.AlarmFilterType;
         mSettings.ViewType = iSettings.ViewType;
         mSettings.OverdueFilterType = iSettings.OverdueFilterType;
         mSettings.NodeFilterType = iSettings.NodeFilterType;
         mSettings.ByteField3 = iSettings.ByteField3;
         mSettings.HighContrastMode = iSettings.HighContrastMode;
      }
      #endregion

      #region Methods
      /// <summary>
      /// Return the current settings as a Struct
      /// </summary>
      /// <returns></returns>
      public UserPreferencesStruct GetSettingsStruct()
      {
         return mSettings;
      }
      #endregion
   }


   /// <summary>
   /// Customizable User Preferences Structure.
   /// 
   /// Note 1: 
   /// Although any field combination can be added to this
   /// structure, the first 4 bytes MUST always be allocated 
   /// to the Version number!!
   /// 
   /// Note 2:
   /// Due to integration with XML serialization, all fields
   /// MUST be word aligned and explicitly identified using
   /// the FieldOffset directive.
   /// 
   /// Note 3:
   /// The use of strings is NOT permitted - although an array 
   /// of word aligned chars is OK.
   /// 
   /// Note 4:
   /// To initialize a setting, use:
   /// PacketBase.Create_Default_User_Preferences
   /// 
   /// Note:
   /// The total number of bytes used MUST be referenced in the
   /// PacketBase.cs class under "DEFAULT_USER_SETTING_BYTES"
   /// 
   /// </summary>
   [StructLayout( LayoutKind.Explicit)]
   public struct UserPreferencesStruct
   {
      [FieldOffset( 0 )]
      public int Version;

      [FieldOffset( 4 )]
      public byte FontSize;

      [FieldOffset( 8 )]
      public byte AlarmFilterType;

      [FieldOffset( 12 )]
      public byte ViewType;

      [FieldOffset( 16 )]
      public byte OverdueFilterType;

      [FieldOffset( 20 )]
      public byte NodeFilterType;

      [FieldOffset( 24 )]
      public Boolean HighContrastMode;

      [FieldOffset( 28 )]
      public byte ByteField3;

      public UserPreferencesStruct(int iVersion)
      {
         Version = iVersion;

         // font size (not in @A)
         FontSize = (byte)FontSizes.Large;

         // alarm filter type (not in @A)
         AlarmFilterType = (byte)AlarmFilter.Off;

         // view type (not in @A)
         ViewType = (byte)DCEViewType.SplitView;
         
         // user defined overdue filter and "Off" by default
         OverdueFilterType = (byte) OverdueFilter.UserDefined + (byte)OverdueFilter.Off;

         // user defined node filter and "All" by default
         NodeFilterType = (byte) ViewTreeAs.UserDefined + (byte) ViewTreeAs.All;

         // user defined contrast mode
         HighContrastMode = false;

         // example byte
         ByteField3 = 0x00;
      }
   }
}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2010
//  Add to project
//----------------------------------------------------------------------------