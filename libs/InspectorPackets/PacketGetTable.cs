﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Get a single Profile Table from the Server
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketGetTable : PacketBase
    {
        private PendingTableSet mPendingTable;

        /// <summary>
        /// Base Constructor
        /// </summary>
        public PacketGetTable()
        {
            mPendingTable = new PendingTableSet();
        }/* end constructor */


        /// <summary>
        /// Overload Constructor
        /// </summary>
        /// <param name="iTableName">reference table name</param>
        public PacketGetTable(PacketType iTableType)
        {
            mPendingTable = new PendingTableSet(iTableType);
        }/* end constructor */

         
        /// <summary>
        /// Packet Type "Update Profile"
        /// </summary>
        public byte Packet = (byte)PacketType.GET_TABLE;

        /// <summary>
        /// Session Identifier
        /// </summary>
        public string SessionID { get; set; }

        /// <summary>
        /// Name of the profile table to return
        /// </summary>
        public PendingTableSet Table
        {
            get { return mPendingTable; }
            set { mPendingTable = value; }
        }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th March 2009
//  Add to project
//----------------------------------------------------------------------------
