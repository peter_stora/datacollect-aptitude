﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Inspection Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumInspectionRecords : PacketBase
    {
        protected List<InspectionRecord> mInspectionRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumInspectionRecords()
        {
            mInspectionRecord = new List<InspectionRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw InspectionRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("INSPECTION")]
        public List<InspectionRecord> Inspection
        {
            get { return mInspectionRecord; }
            set { mInspectionRecord = value; }
        }


        /// <summary>
        /// Add a new Inspection record to the list
        /// </summary>
        /// <param name="item">populated Inspection record object</param>
        /// <returns>number of Inspection objects available</returns>
        public int AddInspectionRecord(InspectionRecord item)
        {
            mInspectionRecord.Add(item);
            return mInspectionRecord.Count;
        }

        /// <summary>
        /// How many Inspection records are now available
        /// </summary>
        public int Count { get { return mInspectionRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumInspectionRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumInspection : EnumInspectionRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mInspectionRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mInspectionRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th March 2009 (13:15)
//  Add to project
//----------------------------------------------------------------------------