﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Single CodedNote table record entry
   /// </summary>
   [XmlRoot( ElementName = "C_NOTE" )]
   public class CodedNoteRecord : PacketBase
   {
      /// <summary>
      /// Base Constructor
      /// </summary>
      public CodedNoteRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      public CodedNoteRecord( int iCode, string iNoteText )
      {
         this.Code = iCode;
         this.Text = iNoteText;

      }/* end constructor */


      /// <summary>
      /// Get or set the actual note code
      /// </summary>
      public int Code
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the actual note text
      /// </summary>
      public string Text
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 1st April 2010
//  Added overload constructor
//
//  Revision 0.0 APinkerton 9th March 2009
//  Add to project
//----------------------------------------------------------------------------
