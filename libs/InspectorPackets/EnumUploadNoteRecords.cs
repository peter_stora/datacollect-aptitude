﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// An enumerated class of Upload Notes. The reason we require an enumeration 
    /// of note records is because the same point can be collected more than once.
    /// For example, point A could appear in any number of routes, with the user
    /// being able to assign notes to each point individually.
    /// </summary>
    public class EnumUploadNoteRecords
    {
        protected List<UploadNoteRecord> mEnumUploadNotes;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public EnumUploadNoteRecords()
        {
            mEnumUploadNotes = new List<UploadNoteRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set any specific Upload Note
        /// </summary>
        [XmlElement("NOTE")]
        public List<UploadNoteRecord> UploadNotes
        {
            get { return mEnumUploadNotes; }
            set { mEnumUploadNotes = value; }
        }/* end property */


        /// <summary>
        /// Add a new Upload Note to the list
        /// </summary>
        /// <param name="item">populated Coded Note record object</param>
        /// <returns>number of Coded Note objects available</returns>
        public int AddUploadNotesRecord(UploadNoteRecord item)
        {
            mEnumUploadNotes.Add(item);
            return mEnumUploadNotes.Count;
        }

        /// <summary>
        /// How many Coded Note strings are now available
        /// </summary>
        public int Count { get { return mEnumUploadNotes.Count; } }

    }/* end class */


        /// <summary>
    /// Class EnumNoteRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumUploadNotes : EnumUploadNoteRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mEnumUploadNotes.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mEnumUploadNotes[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 30th April 2009
//  Add to project
//----------------------------------------------------------------------------

