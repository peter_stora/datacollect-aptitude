﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Instructions Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumInstructionRecords : PacketBase
    {
        protected List<InstructionsRecord> mInstructionsRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumInstructionRecords()
        {
            mInstructionsRecord = new List<InstructionsRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw InstructionsRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("INSTRUCTIONS")]
        public List<InstructionsRecord> Instructions
        {
            get { return mInstructionsRecord; }
            set { mInstructionsRecord = value; }
        }


        /// <summary>
        /// Add a new Instructions record to the list
        /// </summary>
        /// <param name="item">populated Instructions record object</param>
        /// <returns>number of Instructions objects available</returns>
        public int AddInstructionsRecord(InstructionsRecord item)
        {
            mInstructionsRecord.Add(item);
            return mInstructionsRecord.Count;
        }

        /// <summary>
        /// How many Instructions records are now available
        /// </summary>
        public int Count { get { return mInstructionsRecord.Count; } }

    }/* end class */



    /// <summary>
    /// Class EnumInstructionRecords with support for IEnumerable. This 
    /// extended functionality enables not only foreach support - but also 
    /// (and much more importantly) LINQ support!
    /// </summary>
    public class EnumInstructions : EnumInstructionRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mInstructionsRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mInstructionsRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------