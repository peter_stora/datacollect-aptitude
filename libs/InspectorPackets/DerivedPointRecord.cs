﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Derived Items table record entry
    /// </summary>
    [XmlRoot(ElementName = "DERIVEDPOINT")]
    public class DerivedPointRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public DerivedPointRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Table Primary key (not used)
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Foreign Key, point Id of the derived point from Points table
        /// </summary>
        public Guid PointUid { get; set; }

        /// <summary>
        /// Evaluation time
        /// </summary>
        public int EvaluationTime { get; set; }

        /// <summary>
        /// Number of items in table Derived Items
        /// </summary>
        public int NumberOfItems { get; set; }

        /// <summary>
        /// Last point signifying end of route
        /// </summary>
        public int RouteId { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------