﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Class for Table Archive purposes only. 
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   public class OperatorRecord
   {
      /// <summary>
      /// Base Constructor
      /// </summary>
      public OperatorRecord()
      {
      }/* end constructor */

      /// <summary>
      /// Gets or sets the Operators ID
      /// </summary>
      public int OperId
      {
         get;
         set;
      }
      
      /// <summary>
      /// Gets or sets the Operators Name
      /// </summary>
      public string OperName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current DAD type
      /// </summary>
      public int DadType
      {
         get;
         set;
      }

      /// <summary>
      /// Set the operators access level
      /// </summary>
      public OperatorRights AccessLevel
      {
         get;
         set;
      }

      /// <summary>
      /// Can the operator change the password
      /// </summary>
      public Boolean CanChangePassword
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the Operator has changed the password
      /// </summary>
      public Boolean PasswordChanged
      {
         get;
         set;
      }

      /// <summary>
      /// Can the operator create a Work Notification
      /// </summary>
      public Boolean CreateWorkNotification
      {
         get;
         set;
      }

      /// <summary>
      /// Get or Set the time interval at which MARLIN considers data to be
      /// new (used to warn if MARLIN thinks data is being collected twice)
      /// </summary>
      public int DataCollectionTime
      {
         get;
         set;
      }

      /// <summary>
      /// The operators Password
      /// </summary>
      public String Password
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set: If on will reset the user's password to "skf"
      /// </summary>
      public ResetPassword ResetPassword
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the operator will have to scan the asset Tag 
      /// before collecting data
      /// </summary>
      public Boolean ScanRequiredToCollect
      {
         get;
         set;
      }

      /// <summary>
      /// Enable to allow the operator to view the previous data prior 
      /// to collecting new data 
      /// </summary>
      public Boolean ShowPreData
      {
         get;
         set;
      }

      /// <summary>
      /// Specify whether to display the Feedback review display during 
      /// data collection. Choices are, Always, On Alarm, or Never
      /// </summary>
      public VerificationMode VerifyMode
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the tree filtering view. if true all POINTs 
      /// that have data collected against them will be hidden
      /// </summary>
      public OverdueFilter ViewOverdueOnly
      {
         get;
         set;
      }

      /// <summary>
      /// Specify the hierarchy items you would like to show on the 
      /// hierarchy display. Choices are POINT only , SET+POINT, 
      /// Machine+POINT, or All.
      /// </summary>
      public ViewTreeAs ViewTreeElement
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the operator can view the work notification history
      /// </summary>
      public Boolean ViewCMMSWorkHistory
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the system accept 'out of range' values from
      /// the numeric keypad
      /// </summary>
      public Boolean NumericRangeProtection
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets: When true, after an operator scans a Machine, they 
      /// will go to the first collection POINT - not the Machine dialog!
      /// </summary>
      public Boolean ScanAndGoToFirstPoint
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets whether an operator view Spectral FFT data from the WMCD
      /// </summary>
      public Boolean ViewSpectralFFT
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether or not the camera option is available for the operator
      /// </summary>
      public Boolean EnableCamera
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the barcode used for the operator to login
      /// </summary>
      public string BarcodeLogin
      {
         get;
         set;
      }

      /// <summary>
      /// Gtes or sets whether or not a password is required for a barcode login
      /// </summary>
      public Boolean BarcodePassword
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Class to manage an enumerated list of Archived Operator Table records
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   [XmlRoot( ElementName = "DocumentElement" )]
   public class EnumOperatorsRecords : PacketBase
   {
      protected List<OperatorRecord> mOperatorRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumOperatorsRecords()
      {
         mOperatorRecord = new List<OperatorRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw Operator Record object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "OPERATOR" )]
      public List<OperatorRecord> Operator
      {
         get
         {
            return mOperatorRecord;
         }
         set
         {
            mOperatorRecord = value;
         }
      }


      /// <summary>
      /// Add a new Operator record to the list
      /// </summary>
      /// <param name="item">populated OPerator record object</param>
      /// <returns>number of Operator objects available</returns>
      public int AddRecord( OperatorRecord item )
      {
         mOperatorRecord.Add( item );
         return mOperatorRecord.Count;
      }


      /// <summary>
      /// How many Operator records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mOperatorRecord.Count;
         }
      }
   }


   /// <summary>
   /// Class EnumOperatorsRecords with support for IEnumerable.
   /// NOTE: DO NOT USE THIS CLASS FOR READING OPERATOR SETTINGS!
   /// </summary>
   public class EnumOperators : EnumOperatorsRecords,
       IEnumerable, IEnumerator
   {
      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mOperatorRecord.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mOperatorRecord[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */
   
   }

}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 22nd June 2012
//  Add to project
//----------------------------------------------------------------------------