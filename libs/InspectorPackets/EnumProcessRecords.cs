﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Process Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumProcessRecords : PacketBase
    {
        protected List<ProcessRecord> mProcessRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumProcessRecords()
        {
            mProcessRecord = new List<ProcessRecord>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw ProcessRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("PROCESS")]
        public List<ProcessRecord> Process
        {
            get { return mProcessRecord; }
            set { mProcessRecord = value; }
        }


        /// <summary>
        /// Add a new Process record to the list
        /// </summary>
        /// <param name="item">populated Process record object</param>
        /// <returns>number of Process objects available</returns>
        public int AddProcessRecord(ProcessRecord item)
        {
            mProcessRecord.Add(item);
            return mProcessRecord.Count;
        }

        /// <summary>
        /// How many Process records are now available
        /// </summary>
        public int Count { get { return mProcessRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumProcessRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumProcess : EnumProcessRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mProcessRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mProcessRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------