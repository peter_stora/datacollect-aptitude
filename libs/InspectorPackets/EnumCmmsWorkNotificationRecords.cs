﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    public class EnumCmmsWorkNotificationRecords
    {
        protected List<CmmsWorkNotification> mWorkNotification;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumCmmsWorkNotificationRecords()
        {
            mWorkNotification = new List<CmmsWorkNotification>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw CMMS Work Notification object list.
        /// </summary>
        [XmlElement("WorkNotification")]
        public List<CmmsWorkNotification> WorkNotification
        {
            get { return mWorkNotification; }
            set { mWorkNotification = value; }
        }


        /// <summary>
        /// Add a new CMMS Work Notification record to the list
        /// </summary>
        /// <param name="item">populated CMMS Priority record object</param>
        /// <returns>number of CMMS Priority objects available</returns>
        public int AddWorkNotificationRecord(CmmsWorkNotification item)
        {
            mWorkNotification.Add(item);
            return mWorkNotification.Count;
        }/* end method */

        
        /// <summary>
        /// How many CMMS Work Notification records are now available
        /// </summary>
        public int Count { get { return mWorkNotification.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumCmmsWorkNotificationRecords with support for IEnumerable. 
    /// This extended functionality enables not only foreach support - but 
    /// also (and much more importantly) LINQ support!
    /// </summary>
    public class EnumCmmsWorkNotifications : EnumCmmsWorkNotificationRecords, 
        IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mWorkNotification.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mWorkNotification[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009 (10:15)
//  Add to project
//----------------------------------------------------------------------------