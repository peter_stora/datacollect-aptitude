﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// CMMS Work Notifications class for download from Middleware
    /// </summary>
    public class CmmsWorkNotification : CmmsWorkNotificationBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsWorkNotification()
        {
        }/* end constructor */
        

        /// <summary>
        /// Parent Node Uid (from Marlin NODE table)
        /// </summary>
        [XmlElement("NodeUid")]
        public Guid NodeUid { get; set; }


        /// <summary>
        /// Route ID parent lookup
        /// </summary>
        [XmlElement("RouteID")]
        public int RouteID { get; set; }
        

        /// <summary>
        /// Get or set the status of this notification (FieldTypes.CmmsStatus)
        /// 00: UPLOADED (raised within @ptitude)
        /// 01: NEW (raised on device)
        /// </summary>
        [XmlElement("Status")]
        public byte Status { get; set; }
        

        ///// <summary>
        ///// Get or set the key field type (FieldTypes.CmmsKeyField)
        ///// 01: Use the MA Machine Key - TreeElemID
        ///// 02: User typed in Functional Location – Off Route
        ///// 03: User typed in Bar Code – Off Route
        ///// 04: User typed in RFID – Off Route
        ///// </summary>
        //[XmlElement("KeyField")]
        //public byte KeyField { get; set; }
        

        /// <summary>
        /// Machine ID parent lookup – similar to field RouteID
        /// </summary>
        [XmlElement("MachineID")]
        public int MachineID { get; set; }
        

        /// <summary>
        /// Set true if the user enters a value in field CompletionDate
        /// </summary>
        [XmlElement("FixByDateSet")]
        public Boolean FixByDateSet { get; set; }

    }/* end class */


    /// <summary>
    /// Base CMMS Work Notifications class for upload to Middleware
    /// </summary>
    public class CmmsWorkNotificationBase
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public CmmsWorkNotificationBase()
        {
        }/* end constructor */


        /// <summary>
        /// Get or set the actual TreeElemID link from @ptitude
        /// </summary>
        [XmlElement("PRISMNumber")]
        public int PRISM { get; set; }


        /// <summary>
        /// Get or set the incremental work notification number against 
        /// the current NodeUid. Note: Work notifications are raised against 
        /// machines only – and not specific Points!
        /// </summary>
        [XmlElement("WorkNotificationID")]
        public int WorkNotificationID { get; set; }


        /// <summary>
        /// Get or set the manually entered Location Text
        /// </summary>
        [XmlElement("LocationText")]
        public string LocationText { get; set; }
        

        /// <summary>
        /// Get or set the device Date\Time when notification was raised
        /// </summary>
        [XmlElement("WNTimeStamp")]
        public DateTime WNTimeStamp { get; set; }
        

        /// <summary>
        /// Get or set the Id of operator requesting the notificatipn
        /// </summary>
        [XmlElement("OperatorID")]
        public int OperatorID { get; set; }
        

        /// <summary>
        /// Get or set the target date for completion of work request
        /// </summary>
        [XmlElement("CompletionDate")]
        public DateTime CompletionDate { get; set; }
        

        /// <summary>
        /// Get or set a list of Work types as CSV string (i.e. 1,2,3,4)
        /// </summary>
        [XmlElement("WorkTypeIDs")]
        public string WorkTypeIDs { get; set; }
        

        /// <summary>
        /// Get or set the selected @ptitude Priority ID
        /// </summary>
        [XmlElement("PriorityID")]
        public int PriorityID { get; set; }
        

        /// <summary>
        /// Get or set the selected @ptitude Problem ID
        /// </summary>
        [XmlElement("ProblemID")]
        public int ProblemID { get; set; }
        

        /// <summary>
        /// Get or set and additional problem description field entered 
        /// as freehand text by the device operator
        /// </summary>
        [XmlElement("ProblemDescription")]
        public string ProblemDescription { get; set; }
        

        /// <summary>
        /// Get or set the selected MA Corrective Action ID
        /// </summary>
        [XmlElement("CorrectiveActionID")]
        public int CorrectiveActionID { get; set; }
        

        /// <summary>
        /// Get or set the additional corrective action description 
        /// (entered as freehand text by the device operator)
        /// </summary>
        [XmlElement("CorrectiveDescription")]
        public string CorrectiveDescription { get; set; }


        /// <summary>
        /// Get or set the key field type (FieldTypes.CmmsKeyField)
        /// 01: Use the MA Machine Key - TreeElemID
        /// 02: User typed in Functional Location – Off Route
        /// 03: User typed in Bar Code – Off Route
        /// 04: User typed in RFID – Off Route
        /// </summary>
        [XmlElement( "KeyField" )]
        public byte KeyField
        {
           get;
           set;
        }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 6th May 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------

