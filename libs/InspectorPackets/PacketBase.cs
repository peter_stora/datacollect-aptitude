﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2008 to 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using SKF.RS.MicrologInspector.HardwareConnect;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Middleware Server type: Enterprise (Web Services) or Desktop (Sockets)
   /// </summary>
   public enum MiddlewareType
   {
      ENTERPRISE,
      DESKTOP
   }

   /// <summary>
   /// Middleware Profile type: User or device driven
   /// </summary>
   public enum ProfileType
   {
      USER,
      DEVICE
   }

   /// <summary>
   /// Collection DAD type
   /// </summary>
   public enum DadType
   {
      a,
      b,
      c,
      d
   }

   /// <summary>
   /// Enumerated client and server packet identifier codes
   /// </summary>
   public enum PacketType
   {
      UNKNOWN = 0x00,
      IAM = 0x01,
      RETRY = 0x02,
      AUTHENTICATE = 0x03,
      CLOSE_SESSION = 0x04,
      UPDATE_PROFILE = 0x05,
      UPDATE_OPERATORS = 0x06,
      GET_TABLE = 0x07,
      UPDATE_CMMS = 0x08,
      UPLOAD_RECEIVED = 0x09,
      UPDATE_CODED_NOTES = 0x0A,
      HELLO = 0x10,
      ARCHIVE = 0x20,
      UR = 0x81,
      PROFILE = 0x82,
      UNKNOWN_DEVICE = 0x83,
      NO_VALID_LICENSE = 0x84,
      SESSION = 0x85,
      CONNECT_REFUSED = 0x86,
      PROFILE_PENDING = 0x87,
      SYNCH_FAILED = 0x88,
      DONE = 0x89,
      FIRMWARE_ERROR = 0x8A,
      SOFTWARE_LICENSE_EXPIRED = 0x8B,
      FIRMWARE_LICENSE_REFUSED = 0x8C,
      MEDIA_READY = 0x90,
      GET_MEDIA = 0x91,
      MEDIA_BLOCK = 0x92,
      MEDIA_RECEIVED = 0x93,
      
      TABLE_NODE = 0xA0,
      TABLE_POINTS = 0xA1,
      TABLE_PROCESS = 0xA2,
      TABLE_INSPECTION = 0xA3,
      TABLE_MCC = 0xA4,
      TABLE_DERIVED_ITEMS = 0xA5,
      TABLE_DERIVED_POINT = 0xA6,
      TABLE_STRUCTURED = 0xA7,
      TABLE_STRUCTURED_LOG = 0xA8,
      TABLE_NOTES = 0xA9,
      TABLE_CNOTE = 0xAA,
      TABLE_CONDITIONAL_POINT = 0xAB,
      TABLE_INSTRUCTIONS = 0xAC,
      TABLE_MESSAGE = 0xAD,
      TABLE_CMMS_SETTINGS = 0xAE,
      TABLE_FFT_POINTS = 0xAF,
      TABLE_REFERENCEMEDIA = 0xB0,
      TABLE_TEXT_DATA = 0xB1,

      UPLOAD_FILE = 0xC1,
      QUERY_OPC = 0xD0,
      OPC_VALUE=0xD1,
      OPC_ITEM = 0xD2,
      OPERATOR_SET = 0xE0,
      COMPRESSED = 0xE1,
      UPLOAD_COMPRESSED = 0xE2,
      NULL_QUERY = 0xE3,
      NULL_REPLY = 0xE4,
      NULL_BUSY = 0xE5,
      NULL_CALLBACK = 0xE6,
      NULL_END_OF_PROCESS = 0xE7,
   }

   /// <summary>
   /// Enumerated list of Table Names
   /// </summary>
   public enum TableName
   {
      ALL,
      ALL_CMMS,
      ALL_OPERATORS,
      ALL_PROFILE,
      ALL_READINGS,
      C_NOTE,
      C_NOTES,
      CONDITIONALPOINT,
      CORRECTIVEACTION,
      DERIVEDITEMS,
      DERIVEDPOINT,
      DEVICEPROFILE,
      ENVDATA,
      FFTCHANNEL,
      FFTPOINT,
      INSPECTION,
      INSTRUCTIONS,
      LOG,
      MACHINENOPSKIPS,
      MACHINEOKSKIPS,
      MCC,
      MESSAGE,
      MESSAGINGPREFS,
      NODE,
      NOTES,
      OPERATORS,
      POINTS,
      PRIORITY,
      PROBLEM,
      PROCESS,
      REFERENCEMEDIA,
      SAVEDMEDIA,
      STRUCTURED,
      STRUCTUREDLOG,
      TEMPDATA,
      TEXTDATA,
      USERPREFERENCES,
      VELDATA,
      VERSION,
      WORKNOTIFICATION,
      WORKTYPE
   }

   /// <summary>
   /// Connect state enumerators
   /// </summary>
   public enum ConnectionCode
   {
      SERVER_NOT_AVAILABLE = 0x00,
      CONNECTED = 0x01,
      DISCONNECTED = 0x02,
      ERR_NO_SERVER = 0x03,
      ERR_FIRMWARE = 0x04,
      ERR_UNKNOWN_DEVICE = 0x05,
      ERR_UNLICENSED = 0x06,
      ERR_GENERAL = 0x07,
      ERR_SERVER_BUSY = 0x08,
      ERR_SYNCH_FAILED = 0x09,
      ERR_TCP_DROPPED = 0x0A,
      ERR_DATA_QUALITY = 0x0B,
      ERR_UNCRADLED = 0x0C,
      ERR_SERVER_UNLICENSED = 0x0D,
      ERR_FIRMWARE_UNLICENSED = 0x0E,
      ERR_INVALID_SERVICE_PORT = 0x0F,
      ERR_SQL_PROCESSING = 0x10,
      ERR_MEDIA_UPLOAD_FAILURE = 0x11,
   }

   /// <summary>
   /// Operator Levels
   /// </summary>
   public enum OperatorRights
   {
      Admin = 0x00,
      Full = 0x01,
      Operator = 0x02,
      Limited = 0x03,
      Review = 0x04
   }

   /// <summary>
   /// Tree filter verification
   /// </summary>
   public enum VerificationMode
   {
      Never = 0x00,
      OnAlarm = 0x01,
      Always = 0x02
   }

   /// <summary>
   /// Connected device status:
   /// </summary>
   public enum DeviceStatus
   {
      GoodToGo,
      Unknown,
      Unlicensed,
      ServerUnlicensed
   }

   /// <summary>
   /// Microlog Inspector saved media types.
   /// Notes: 
   /// * IMAGE type covers BMP, GIF, IMG, JPG, JPEG, TIF and TIFF
   /// * AUDIO type covers AIF, IFF, MP3, M4A, MPA, WAV and WMA
   /// * VIDEO type covers WAV only
   /// * RAWBINARY covers all other types including DOC, XLS, PDF etc.
   /// </summary>
   public enum BinaryMediaType
   {
      Image = 0x00,
      Audio = 0x01,
      Video = 0x02,
      RawBinary = 0x03
   }

   /// <summary>
   /// Where did the current Media upload originate from
   /// </summary>
   public enum BinaryMediaOrigin
   {
      /// <summary>
      /// Media (photo/video/audio) recorded from Home Page
      /// </summary>
      OffRoute = 0x00,

      /// <summary>
      /// Media recorded while collecting Measurement (POINT)
      /// </summary>
      Point = 0x01,

      /// <summary>
      /// Media recorded at the MACHINE Page
      /// </summary>
      Machine = 0x02,

      /// <summary>
      /// Media recorded during input of Work Notification
      /// </summary>
      WorkNotification = 0x03
   }

   /// <summary>
   /// Specify hierarchy items to show on the hierarchy display.  If specified
   /// by @ptitude, MI will enforce the filter, otherwise (if user defined) the
   /// user can choose themselves.  When user defined, the value will be the
   /// bitwise or-ing of UserDefined and the value
   /// </summary>
   public enum ViewTreeAs
   {
      UserDefined = 0x01,
      PointOnly = 0x02,
      SetAndPoint = 0x04,
      MachineAndPoint = 0x08,
      All = 0x10
   }

   /// <summary>
   /// The type of View the operator is using
   /// </summary>
   public enum DCEViewType
   {
      Unkown = 0x00,
      SplitView = 0x01,
      Search = 0x02
   }

   /// <summary>
   /// The alarm filter type the operator is using
   /// </summary>
   public enum AlarmFilter
   {
      Off = 0x00,
      InDanger = 0x01,
      InAlert = 0x02,
      InAny = 0x03
   }

   /// <summary>
   /// Font size to be used (i.e. in the Views)
   /// </summary>
   public enum FontSizes
   {
      Small = 0x00,
      Large = 0x01
   }

   /// <summary>
   /// Reset password flag for @A.  When Reset, @A is telling us that the 
   /// operator needs reset their password.  When Set, we are telling @A 
   /// that the operator has set their password.  When None, we proceed 
   /// with normal logins.
   /// </summary>
   public enum ResetPassword
   {
      None = 0x00,
      Reset = 0x01,
      Set = 0x02
   }

   /// <summary>
   /// Overdue filter.  If speficifed by @ptitude, MI will enforce the filter,
   /// otherwise (if user defined) the user can choose themselves.  When user 
   /// defined, the value will be the bitwise or-ing of UserDefined and the 
   /// value.
   /// </summary>
   public enum OverdueFilter
   {
      UserDefined = 0x01,
      Off = 0x02,
      On = 0x04
   }

   /// <summary>
   /// FFT Frequency Unit types to use when viewing FFTs
   /// </summary>
   public enum FFTFrequencyUnits
   {
      Hz = 0x00,
      CPM = 0x01
   }


   /// <summary>
   /// Microlog Inspector Xml Base Packet
   /// </summary>
   public class PacketBase
   {
      /// <summary>
      /// Null Packet
      /// </summary>
      public static String NULL_ACK_PACKET = "<ACK></ACK>";

      /// <summary>
      /// XML Header String (with special characters)
      /// </summary>
      public static String XML_HEADER_LINE = "<?xml version=" + (char) 0x22 + "1.0" + (char) 0x22 + "?>";

      /// <summary>
      /// UUID Hash encrypter #1
      /// </summary>
      public static String UUID_HASH1 = "4FDBD2EA4909870153638891B6FC8A03C369F83CCE5C2B5D";

      /// <summary>
      /// UUID Hash encrypter #2
      /// </summary>
      public static String UUID_HASH2 = "C2D1834BADDFE48D583455B8FA2331A9F874B1F8C9D367CC";

      /// <summary>
      /// General fixed Hash Key (for double encryption)
      /// </summary>
      public static string OPSLOCAL_HASH = "200DF0A5A0E8856AAFBB3E71684446" +
          "C2688EA2D431470CB67378E49E481B456A585C3B10C9C778B96165" +
          "0A0F322AE0BAE1";

      /// <summary>
      /// Hierarchy ROOT node guid
      /// </summary>
      public static Guid ROOT_NODE_GUID = new Guid( "00000000-0000-0000-0000-000000000000" );

      /// <summary>
      /// Hierarchy NULL guid for default and fail states
      /// </summary>
      public static Guid NODE_NULL_GUID = new Guid( "00000001-0001-0001-0001-000000000001" );

      /// <summary>
      /// Value for null session within state objects
      /// </summary>
      public static string NULL_SESSION = "0000000000";

      /// <summary>
      /// XML UID Element 
      /// </summary>
      public static byte[] XML_ELEMENT_UID = new byte[] { 0x3c, 0x55, 0x69, 0x64, 0x3e };

      /// <summary>
      /// XML ID Element 
      /// </summary>
      public static byte[] XML_ELEMENT_ID = new byte[] { 0x3c, 0x49, 0x44, 0x3e };

      /// <summary>
      /// XML PointUid Element
      /// </summary>
      public static byte[] XML_ELEMENT_POINT_UID = new byte[] { 0x3c, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x55, 0x69, 0x64, 0x3e };

      /// <summary>
      /// XML ReferenceMedia Element Start byte array
      /// </summary>
      public static byte[] XML_ELEMENT_MEDIA_START = new byte[] { 0x3c, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x4d, 0x65, 0x64, 0x69, 0x61, 0x3e };

      /// <summary>
      /// XML ReferenceMedia Element End byte array
      /// </summary>
      public static byte[] XML_ELEMENT_MEDIA_END = new byte[] { 0x3c, 0x2f, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x4d, 0x65, 0x64, 0x69, 0x61, 0x3e };

      /// <summary>
      /// XML ACK Element as Unicode Hex
      /// </summary>
      protected static Byte[] ACK_UNICODE = new byte[] { 0x3C, 0x00, 0x2F, 0x00, 0x41, 0x00, 0x43, 0x00, 0x4B, 0x00, 0x3E, 0x00 };
      
      /// <summary>
      /// XML ACK Element as ASCII Hex
      /// </summary>
      protected static Byte[] ACK_ASCII = new byte[] { 0x3C, 0x2F, 0x41, 0x43, 0x4B, 0x3E };


      #region Protected and Static Constants

      #region General Packet Constants and Defaults

      protected const ProfileType DEFAULT_PROFILE = ProfileType.DEVICE;
      protected const MiddlewareType DEFAULT_SERVER = MiddlewareType.DESKTOP;
      protected const String UNKNOWN_PACKET = "0x00";
      protected const String ACK_ELEMENT_NAME = "ACK";
      protected const String PACKET_NAME = "Packet";
      protected const String DEFAULT_UID = "SKFMICROLOGINSPECTOR";
      protected const String NULL_KEY = "KEY00000000000000000001";
      protected const String NULL_AUTHORIZATION = "KEY00000000000000000002";
      protected const String DEFAULT_PROFILE_NAME = "PENDING";
      protected const String DEFAULT_PROFILE_ID = "PROFILEID0000000000001";
      protected const String DEFAULT_SESSION_ID = "SESSION000000000000000";
      protected const Double DEFAULT_SESSION_TIMEOUT = 600.0;
      protected const Boolean DEFAULT_PRE_LICENSED = false;

      #endregion


      #region Default Firmware Version

      protected const int VERSION_MAJOR = 1;
      protected const int VERSION_MINOR = 0;
      protected const int VERSION_BUILD = 0;
      protected const int VERSION_REVISION = 0;

      #endregion

      /// <summary>
      /// Maximum uncompressed packet size
      /// </summary>
      public static int MAX_UNCOMPRESSED_BYTE_SIZE = 4096;

      #region Default Operator Setting

      public static readonly OperatorRights     DEFAULT_USER_LEVEL = OperatorRights.Limited;
      public static readonly Boolean            DEFAULT_CAN_CHANGE_PASSWORD = true;
      public static readonly Boolean            DEFAULT_CAN_CREATE_WORK_ORDER = true;
      public static readonly int                DEFAULT_DATA_COLLECTION_TIME = 45;
      public static readonly Boolean            DEFAULT_ENABLE_MACHINE_OK = false;
      public static readonly string             DEFAULT_PASSWORD = "skf";
      public static readonly ResetPassword      DEFAULT_RESET_PASSWORD = ResetPassword.None;
      public static readonly Boolean            DEFAULT_SCAN_REQUIRED_TO_COLLECT = false;
      public static readonly Boolean            DEFAULT_SHOW_PREVIOUS_DATE = false;
      public static readonly VerificationMode   DEFAULT_VERIFY_MODE = VerificationMode.OnAlarm;
      public static readonly OverdueFilter      DEFAULT_VIEW_OVERDUE_ONLY = OverdueFilter.Off;
      public static readonly ViewTreeAs         DEFAULT_VIEW_TREE_ELEMS = ViewTreeAs.All;
      public static readonly Boolean            DEFAULT_VIEW_WORK_ORDER_HISTORY = false;
      public static readonly Boolean            DEFAULT_RANGE_PROTECTION = true;
      public static readonly Boolean            DEFAULT_SCAN_AND_GO_TO_FIRST_POINT = false;
      public static readonly Boolean            DEFAULT_VIEW_FFT_SPECTRUMS = false;
      public static readonly DCEViewType        DEFAULT_VIEW_TYPE = DCEViewType.SplitView;
      public static readonly AlarmFilter        DEFAULT_ALARM_FILTER = AlarmFilter.Off;
      public static readonly FontSizes          DEFAULT_FONT_SIZE = FontSizes.Large;
      public static readonly Boolean            DEFAULT_ENABLE_CAMERA = true;
      public static readonly string             DEFAULT_BARCODE_LOGIN = string.Empty;
      public static readonly Boolean            DEFAULT_BARCODE_PASSWORD = false;

      public static readonly Boolean            DEFAULT_MSG_END_OF_ROUTE = true;
      public static readonly Boolean            DEFAULT_MSG_ZERO_VELACC = true;
      public static readonly Boolean            DEFAULT_MSG_SAVE_AS_CURRENT = true;
      public static readonly Boolean            DEFAULT_MSG_FOUND_IN_OTHER_ROUTE = true;
      public static readonly  Boolean           DEFAULT_MSG_SHOW_SETS = true;

      public static readonly Boolean            DEFAULT_SKIP_ON_MCD = true;
      public static readonly Boolean            DEFAULT_SKIP_ON_PROCESS = true;
      public static readonly Boolean            DEFAULT_SKIP_ON_INSPECTION = true;

      public static readonly Boolean            DEFAULT_LOGIC = false;
      public static readonly int                DEFAULT_USER_SETTING_BYTES = 32;

      /// <summary>
      /// Initializes the user preference blob with the default settings
      /// </summary>
      /// <returns>The user preference blob bytes</returns>
      public static byte[] Create_Default_User_Preferences()
      {
         // create the bytes, this size has to be DEFAULT_USER_SETTING_BYTES
         byte[] prefence = new byte[DEFAULT_USER_SETTING_BYTES];

         //
         // Set defaults where needed.  Please see UserPreferencesStruct for
         // the byte order
         //
         prefence[4]    = (byte) DEFAULT_FONT_SIZE;
         prefence[8]    = (byte) DEFAULT_ALARM_FILTER;
         prefence[12]   = (byte) DEFAULT_VIEW_TYPE;
         prefence[16]   = (byte) DEFAULT_VIEW_OVERDUE_ONLY;
         prefence[20]   = (byte) DEFAULT_VIEW_TREE_ELEMS;

         return prefence;

      }/* end method */

      #endregion

      #endregion

      /// <summary>
      /// Gets the device's local time (replaces DateTime.Now)
      /// This method addresses OTD #2095
      /// </summary>
      public static DateTime DateTimeNow
      {
         get
         {
            return DeviceTimeManager.GetLocalTime();
         }
      }


      /// <summary>
      /// Gets the My Documents directory
      /// </summary>
      public static string MyDocumentsPath
      {
         get
         {
            return DeviceInfo.Get_System_Folder_Path( DeviceInfo.Folders.Personal );
         }
      }


      /// <summary>
      /// Gets the My Pictures directory
      /// </summary>
      public static string MyPicturesPath
      {
         get
         {
            return DeviceInfo.Get_System_Folder_Path( DeviceInfo.Folders.MyPictures );
         }
      }


      /// <summary>
      /// Gets the My Music directory
      /// </summary>
      public static string MyMusicPath
      {
         get
         {
            return DeviceInfo.Get_System_Folder_Path( DeviceInfo.Folders.MyMusic );
         }
      }


      /// <summary>
      /// Gets the My Videos directory
      /// </summary>
      public static string MyVideosPath
      {
         get
         {
            return DeviceInfo.Get_System_Folder_Path( DeviceInfo.Folders.MyVideos );
         }
      }


      /// <summary>
      /// Serialize the Xml UR Packet to a file
      /// </summary>
      /// <param name="filename">file to serialize objects to</param>
      public void Save( string filename, object PacketType )
      {
         Stream _stream = null;
         try
         {
            _stream = File.Create( filename );
            XmlSerializer serializer = new XmlSerializer( PacketType.GetType() );
            serializer.Serialize( _stream, this );
         }
         finally
         {
            if ( _stream != null )
               _stream.Close();
         }

      }/* end method */


      /// <summary>
      /// Serialize the Xml Packet as a string  
      /// </summary>
      /// <returns>Xml string</returns>
      public String XmlToString( object PacketType )
      {
         // create managed memory stream
         using ( MemoryStream ms = new MemoryStream() )
         {
            // serialize the packet object to the memory stream
            XmlSerializer serializer = new XmlSerializer( PacketType.GetType() );
            serializer.Serialize( ms, this );

            // reset the memory stream pointer
            ms.Position = 0;

            // parse to byte array ..
            byte[] XmlPacket = new byte[ ms.Length ];
            ms.Read( XmlPacket, 0, XmlPacket.Length );

            // and return as a formatted string
            return Encoding.UTF8.GetString( XmlPacket, 0, XmlPacket.Length );
         }

      }/* end method */


      /// <summary>
      /// Serializes the Xml Packet to a cache file
      /// </summary>
      /// <returns>Xml byte array</returns>
      public Boolean XmlToCacheFile( object PacketType, string iFileName )
      {
         // if cache file already exists then delete it
         PacketBase.DeleteFile( iFileName );

         // set the default result
         Boolean result = true;

         // create managed memory stream
         using ( FileStream fs = new FileStream( iFileName,
               FileMode.Create, FileAccess.ReadWrite ) )
         {
            try
            {
               // serialize the packet object to the file stream
               XmlSerializer serializer = new XmlSerializer( PacketType.GetType() );
               serializer.Serialize( fs, this );

               // reset the file stream pointer
               fs.Position = 0;
            }
            catch
            {
               // on error, fail method result
               result = false;
            }
            finally
            {
               // close the file stream
               fs.Close();
            }
            return result;
         }

      }/* end method */


      /// <summary>
      /// Serialize the Xml Packet as an array of bytes 
      /// </summary>
      /// <returns>Xml byte array</returns>
      public byte[] XmlToByteArray( object PacketType )
      {
         // create managed memory stream
         using ( MemoryStream ms = new MemoryStream() )
         {
            // serialize the packet object to the memory stream
            XmlSerializer serializer = new XmlSerializer( PacketType.GetType() );
            serializer.Serialize( ms, this );

            // reset the memory stream pointer
            ms.Position = 0;

            // parse to byte array ..
            byte[] XmlPacket = new byte[ ms.Length ];
            ms.Read( XmlPacket, 0, XmlPacket.Length );

            // and return that byte array
            return XmlPacket;
         }

      }/* end method */


      /// <summary>
      /// Serialize the Packet object to an array of bytes 
      /// </summary>
      /// <returns>Xml byte array</returns>
      public static byte[] ObjectToByteArray( object PacketType )
      {
         // create managed memory stream
         using ( MemoryStream ms = new MemoryStream() )
         {
            // serialize the packet object to the memory stream
            XmlSerializer serializer = new XmlSerializer( PacketType.GetType() );
            serializer.Serialize( ms, null );

            // reset the memory stream pointer
            ms.Position = 0;

            // parse to byte array ..
            byte[] XmlPacket = new byte[ ms.Length ];
            ms.Read( XmlPacket, 0, XmlPacket.Length );

            // and return that byte array
            return XmlPacket;
         }

      }/* end method */


      /// <summary>
      /// Given a Microlog Inspector xml packet, return the packet type
      /// </summary>
      /// <param name="xml">reference xml packet</param>
      /// <returns>Inspector packet type</returns>
      public static PacketType GetPacketType( string iXmlBuffer )
      {
         // set default and fail values
         string xmlValue = UNKNOWN_PACKET;
         PacketType result;

         // safely convert packet number into packet type
         try
         {
            // create managed xml reader object
            using ( XmlTextReader reader = new XmlTextReader( iXmlBuffer, XmlNodeType.Document, null ) )
            {
               // read xml and extract the packet number
               while ( reader.Read() )
               {
                  if ( reader.NodeType == XmlNodeType.Element )
                  {
                     if ( reader.LocalName == PACKET_NAME )
                     {
                        xmlValue = reader.ReadString();
                        break;
                     }
                  }
               }
            }
            try
            {
               result = (PacketType) Convert.ToByte( xmlValue );
            }
            catch
            {
               result = PacketType.UNKNOWN;
            }
         }
         catch
         {
            result = PacketType.UNKNOWN;
         }

         // return packet type
         return result;

      }/* end method */


      /// <summary>
      /// Given a Microlog Inspector xml packet, return the packet type
      /// </summary>
      /// <param name="xml">reference xml packet as a binary array</param>
      /// <returns>Inspector packet type</returns>
      public static PacketType GetPacketType( byte[] iXmlBuffer )
      {
         //
         // if buffer contents have no terminating ACK then
         // end here and return the packet type as UNKNOWN
         //
         if ( !HasTerminatingACK( iXmlBuffer ) )
         {
            return PacketType.UNKNOWN;
         }

         // set default and fail values
         string xmlValue = UNKNOWN_PACKET;
         PacketType result;

         // safely convert packet number into packet type
         try
         {
            // create managed xml reader object
            using ( XmlTextReader reader = new
                XmlTextReader( Encoding.UTF8.GetString( iXmlBuffer, 0, iXmlBuffer.Length ),
                    XmlNodeType.Document, null ) )
            {
               // read xml and extract the packet number
               while ( reader.Read() )
               {
                  if ( reader.NodeType == XmlNodeType.Element )
                  {
                     if ( reader.LocalName == PACKET_NAME )
                     {
                        xmlValue = reader.ReadString();
                        break;
                     }
                  }
               }
            }
            try
            {
               result = (PacketType) Convert.ToByte( xmlValue );
            }
            catch
            {
               result = PacketType.UNKNOWN;
            }
         }
         catch
         {
            result = PacketType.UNKNOWN;
         }

         // return packet type
         return result;

      }/* end method */


      /// <summary>
      /// Check for the end of packet "ACK"  
      /// </summary>
      /// <param name="iXmlBuffer">XML Packet in raw bytes</param>
      /// <returns>true if buffer contains "ACK"</returns>
      public static Boolean HasTerminatingACK( byte[] iXmlBuffer )
      {
         // set the initial fail result
         Boolean result = false;
         int flag = 0;

         // ensure that the buffer is larger than the reference array
         if ( iXmlBuffer.Length > ACK_ASCII.Length )
         {
            try
            {
               // check to see if </ACK> in last siz bytes
               for ( int k = 0; k < ACK_ASCII.Length; ++k )
               {
                  int j = k + ( iXmlBuffer.Length - ACK_ASCII.Length );

                  if ( ACK_ASCII[ k ] == iXmlBuffer[ j ] )
                  {
                     flag++;
                  }
                  if ( flag == ACK_ASCII.Length )
                  {
                     result = true;
                     break;
                  }
               }

               // if not the last six bytes, then scan the entire buffer
               if ( !result )
               {
                  // reset the initial fail result
                  result = false;
                  flag = 0;

                  for ( int i = 0; i < ( iXmlBuffer.Length - ( ACK_ASCII.Length - 1 ) ); ++i )
                  {
                     flag = 0;
                     for ( int j = 0; j < ACK_ASCII.Length; ++j )
                     {
                        if ( ACK_ASCII[ j ] == iXmlBuffer[ i + j ] )
                        {
                           flag++;
                        }
                        if ( flag == ACK_ASCII.Length )
                        {
                           result = true;
                           break;
                        }
                     }
                  }
               }
            }
            catch
            {
               result = false;
            }
         }

         return result;

      }/* end method */


      /// <summary>
      /// Generic xml deserializer to convert xml packet into object
      /// </summary>
      /// <typeparam name="T">type to reference</typeparam>
      /// <param name="iXmlBuffer">xml packet in bytes</param>
      /// <returns>field populated object</returns>
      public static T DeserializeObject<T>( string xml )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            byte[] buffer = Encoding.UTF8.GetBytes( xml );
            ms.Write( buffer, 0, buffer.Length );
            ms.Position = 0;

            XmlSerializer xs = new XmlSerializer( typeof( T ) );
            try
            {
               return (T) xs.Deserialize( ms );
            }
            catch
            {
               return default( T );
            }
         }

      }/* end method */


      /// <summary>
      /// Generic xml deserializer to convert xml packet into object
      /// </summary>
      /// <typeparam name="T">type to reference</typeparam>
      /// <param name="iXmlBuffer">xml packet in bytes</param>
      /// <returns>field populated object</returns>
      public static T DeserializeObject<T>( byte[] iXmlByteBuffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            ms.Write( iXmlByteBuffer, 0, iXmlByteBuffer.Length );
            ms.Position = 0;

            XmlSerializer xs = new XmlSerializer( typeof( T ) );
            try
            {
               return (T) xs.Deserialize( ms );
            }
            catch
            {
               return default( T );
            }
         }

      }/* end method */


      /// <summary>
      /// Simple password encryption
      /// </summary>
      /// <param name="iString">string to be encrypted</param>
      /// <returns>encrypted string</returns>
      public static string EncryptPassword( string iPassword )
      {
         XmlEncryption encryp = new XmlEncryption();
         encryp.SecurePassword = OPSLOCAL_HASH;
         return encryp.EncryptData( iPassword );
      }/* end method */


      /// <summary>
      /// Simple password decryption
      /// </summary>
      /// <param name="iString">string to be decrypted</param>
      /// <returns>encrypted string</returns>
      public static string DecryptPassword( string iPassword )
      {
         XmlEncryption encrypt = new XmlEncryption();
         encrypt.SecurePassword = OPSLOCAL_HASH;
         return encrypt.DecryptData( iPassword );
      }/* end method */


      /// <summary>
      /// Return the XML cache file name and path based on the actual 
      /// name of the table. This is primarily used to saving decompressed
      /// XML table files from the download, prior to deserialization
      /// </summary>
      /// <param name="iTableName">Table name reference</param>
      /// <returns>XML filename and path</returns>
      public static string CacheFile( TableName iTableName )
      {
         string fileName = iTableName.ToString();
         return fileName + ".xml";

      }/* end method */


      /// <summary>
      /// Deletes a named file
      /// </summary>
      /// <param name="iFileName"></param>
      public static void DeleteFile( string iFileName )
      {
         if ( File.Exists( iFileName ) )
         {
            File.Delete( iFileName );
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 3rd October 2010
//  Add password encryption
//
//  Revision 0.0 APinkerton 8th March 2009
//  Add to project
//----------------------------------------------------------------------------


