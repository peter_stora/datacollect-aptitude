﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Device profile business object (for Table connection only)
   /// </summary>
   public class DeviceProfile
   {
      /// <summary>
      /// Get or set the name of this device profile
      /// </summary>
      public string ProfileName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the device friendly name
      /// </summary>
      public string DeviceName
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Profile ID
      /// </summary>
      public string ProfileID
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the time Stamp associated with this profile
      /// </summary>
      public DateTime LastModified
      {
         get;
         set;
      }
      
      /// <summary>
      /// Base Constructor
      /// </summary>
      public DeviceProfile(): 
         this(null)
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iProfileSettings"></param>
      public DeviceProfile( ProfileSettings iProfileSettings )
      {
         if ( iProfileSettings != null )
         {
            this.ProfileName = iProfileSettings.ProfileName;
            this.ProfileID = iProfileSettings.ProfileId;
            this.DeviceName = iProfileSettings.DeviceName;
         }
         else
         {
            this.ProfileName = "";
            this.ProfileID = "";
            this.DeviceName = "";
         }
         
         this.LastModified = PacketBase.DateTimeNow;

      }/* end constructor */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------