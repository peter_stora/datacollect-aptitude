﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Class to manage an enumerated list of Note Table records 
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumNoteRecords : PacketBase
    {
        protected List<NotesRecord> mNoteRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumNoteRecords()
        {
            mNoteRecord = new List<NotesRecord>();
        }/* end constructor */

        /// <summary>
        /// Get or set elements in the raw NoteRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("NOTE")]
        public List<NotesRecord> NOTE
        {
            get { return mNoteRecord; }
            set { mNoteRecord = value; }
        }

        /// <summary>
        /// Add a new Note record to the list
        /// </summary>
        /// <param name="item">populated Note record object</param>
        /// <returns>number of Note objects available</returns>
        public int AddNoteRecord(NotesRecord item)
        {
            mNoteRecord.Add(item);
            return mNoteRecord.Count;
        }

        /// <summary>
        /// How many Note records are now available
        /// </summary>
        public int Count { get { return mNoteRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumNoteRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumNotes : EnumNoteRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mNoteRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mNoteRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------