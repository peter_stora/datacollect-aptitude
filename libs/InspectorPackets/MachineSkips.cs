﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// What type of points can the operator SKIP. If all properties
    /// are false, then Machine Skip will be disabled
    /// </summary>
    public class MachineSkips
    {
        /// <summary>
        /// Base Constructor
        /// </summary>
        public MachineSkips()
            : this(PacketBase.DEFAULT_SKIP_ON_MCD,
                    PacketBase.DEFAULT_SKIP_ON_PROCESS,
                    PacketBase.DEFAULT_SKIP_ON_INSPECTION)
        {
        }/* end constructor */


        /// <summary>
        /// Overload constructor
        /// </summary>
        /// <param name="iAll"></param>
        /// <param name="iMCD"></param>
        /// <param name="iProcess"></param>
        /// <param name="iInspection"></param>
        public MachineSkips(Boolean iMCD,
                            Boolean iProcess,
                            Boolean iInspection)
        {
            this.MCD = iMCD;
            this.Process = iProcess;
            this.Inspection = iInspection;

        }/* end constructor */

        /// <summary>
        /// Get or Set: MCD POINTs under a Machine would be skipped 
        /// if an operator uses “Machine OK” in the Machine Dialog
        /// </summary>
        public Boolean MCD { get; set; }

        /// <summary>
        /// Get or Set: Process POINTs under a Machine would be skipped 
        /// if an operator uses “Machine OK” in the Machine Dialog
        /// </summary>
        public Boolean Process { get; set; }

        /// <summary>
        /// Get or Set: Inspection POINTs under a Machine would be 
        /// if an operator uses “Machine OK” in the Machine Dialog
        /// </summary>
        public Boolean Inspection { get; set; }


        /// <summary>
        /// When true, this provides the operator a method to quickly
        /// describe the overall machine condition as "OK", without 
        /// collecting current data on the machine’s measurements. 
        /// </summary>
        /// <returns>True if any of the skip states are enabled</returns>
        public Boolean SkipEnabled()
        {
            if (this.Inspection || this.MCD || this.Process)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th February 2009 (10:45)
//  Add to project
//----------------------------------------------------------------------------

