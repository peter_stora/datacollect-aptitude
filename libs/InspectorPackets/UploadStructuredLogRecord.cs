﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Simple class to manage Upload Structured Log statistics
    /// </summary>
    public class UploadStructuredLogRecord
    {
        [XmlElement("RouteId")]
        public int RouteId { get; set; }

        [XmlElement("StartTime")]
        public DateTime StartTime { get; set; }

        [XmlElement("EndTime")]
        public DateTime EndTime { get; set; }

        [XmlElement("Duration")]
        public int Duration { get; set; }

        [XmlElement("OperId")]
        public int OperId { get; set; }

        [XmlElement("PercentComplete")]
        public double PercentComplete { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th May 2009
//  Add to project
//----------------------------------------------------------------------------

