﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 to 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Serializable Compressed Table Packet
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketCompressed : PacketBase
   {
      /// <summary>
      /// The actual Packet type (OPERATOR SET)
      /// </summary>
      public byte Packet = (byte) PacketType.COMPRESSED;
      public byte HostingPacket
      {
         get;
         set;
      }

      /// <summary>
      /// Operator Settings Element
      /// </summary>
      [XmlElement( "ByteData" )]
      public byte[] ByteData
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      public PacketCompressed()
      {
      }/* end constructor */


      /// <summary>
      /// Compress the serialized byte array 
      /// </summary>
      /// <param name="buffer">raw byte array</param>
      /// <returns>decompressed byte array</returns>
      public byte[] Compress( byte[] buffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Compress, true ) )
            {
               zip.Write( buffer, 0, buffer.Length );
            }

            ms.Position = 0;

            byte[] compressed = new byte[ ms.Length ];
            ms.Read( compressed, 0, compressed.Length );

            byte[] gzBuffer = new byte[ compressed.Length + 4 ];

            System.Buffer.BlockCopy( compressed, 0, gzBuffer, 4, compressed.Length );
            System.Buffer.BlockCopy( BitConverter.GetBytes( buffer.Length ), 0, gzBuffer, 0, 4 );

            return gzBuffer;
         }

      }/* end method */


      /// <summary>
      /// Decompress the serialized byte array
      /// </summary>
      /// <param name="gzBuffer">compressed byte array</param>
      /// <returns>decompressed byte array</returns>
      public byte[] Decompress( byte[] gzBuffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            int msgLength = BitConverter.ToInt32( gzBuffer, 0 );
            ms.Write( gzBuffer, 4, gzBuffer.Length - 4 );

            byte[] buffer = new byte[ msgLength ];

            ms.Position = 0;
            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Decompress ) )
            {
               zip.Read( buffer, 0, buffer.Length );
            }

            return buffer;
         }
      }/* end method */


      /// <summary>
      /// Decompress the serialized byte array into a list of bytes
      /// </summary>
      /// <param name="gzBuffer">compressed byte array</param>
      /// <returns>decompressed byte list</returns>
      public List<byte> ListDecompress( byte[] gzBuffer )
      {
         using ( MemoryStream ms = new MemoryStream() )
         {
            ms.Write( gzBuffer, 4, gzBuffer.Length - 4 );

            List<byte> buffer = new List<byte>();

            // set a generous limit!
            buffer.Capacity = 0x600000;

            int cacheSize = 0x40000;
            byte[] cache = new byte[ cacheSize ];
            byte[] tail = new byte[ 0 ];

            ms.Position = 0;

            using ( GZipStream zip = new GZipStream( ms, CompressionMode.Decompress, true ) )
            {
               try
               {
                  int read = -1;
                  read = zip.Read( cache, 0, cache.Length );

                  while ( read > 0 )
                  {
                     if ( read == cacheSize )
                     {
                        buffer.AddRange( cache );
                     }
                     else
                     {
                        ByteArrayProcess.ResizeByteBuffer( ref tail, read );
                        ByteArrayProcess.ExtractArray( cache, ref tail, 0, read );
                        buffer.AddRange( tail );
                     }
                     read = zip.Read( cache, 0, cache.Length );
                  }
               }
               catch
               {
                  buffer.Clear();
               }
               finally
               {
                  zip.Close();
               }
            }
            return buffer;
         }

      }/* end method */


      /// <summary>
      /// Decompresses the received byte array into a single Xml cache
      /// file and returns a count of the number of elements written
      /// </summary>
      /// <param name="gzBuffer">zipped byte array</param>
      /// <param name="iLookupElement">return count on this lookup</param>
      /// <param name="iFileName">name and path of file</param>
      /// <returns>number of Xml elements written</returns>
      public int DecompressToXmlFile( byte[] gzBuffer, 
         byte[] iLookupElement, 
         string iFileName )
      {
         int count = 0;
         int cacheSize = 0x40000;
         byte[] cache = new byte[ cacheSize ];
         byte[] tail = new byte[ 0 ];

         // create a safe and managed memory stream object
         using ( MemoryStream ms = new MemoryStream() )
         {
            // and initialize
            ms.Write( gzBuffer, 4, gzBuffer.Length - 4 );
            ms.Position = 0;

            // create a safe and managed filestream object
            using ( FileStream fs = new FileStream( iFileName,
               FileMode.Create, FileAccess.ReadWrite ) )
            {
               try
               {
                  // create a safe and managed binary writer
                  using ( BinaryWriter bw = new BinaryWriter( fs ) )
                  {
                     try
                     {
                        // create a safe and managed zip object
                        using ( GZipStream zip = new GZipStream( ms, 
                           CompressionMode.Decompress, true ) )
                        {
                           try
                           {
                              int read = -1;
                              read = zip.Read( cache, 0, cache.Length );

                              while ( read > 0 )
                              {
                                 if ( read == cacheSize )
                                 {
                                    bw.Write( cache );
                                    if ( iLookupElement != null )
                                    {
                                       count += ByteArrayProcess.CountBytePattern(
                                          iLookupElement, cache );
                                    }
                                 }
                                 else
                                 {
                                    ByteArrayProcess.ResizeByteBuffer( ref tail, read );
                                    ByteArrayProcess.ExtractArray( cache, ref tail, 0, read );
                                    bw.Write( tail );
                                    if ( iLookupElement != null )
                                    {
                                       count += ByteArrayProcess.CountBytePattern(
                                          iLookupElement, tail );
                                    }
                                 }
                                 read = zip.Read( cache, 0, cache.Length );
                              }
                           }
                           catch
                           {
                              count = -1;
                           }
                           finally
                           {
                              zip.Close();
                           }
                        }
                     }
                     finally
                     {
                        bw.Close();
                     }
                  }
               }
               finally
               {
                  fs.Close();
               }
            }
         }

         return count;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 16th October 2010
//  Major revision to support decompression to file instead of memory
//
//  Revision 0.0 APinkerton 20th February 2009
//  Add to project
//----------------------------------------------------------------------------