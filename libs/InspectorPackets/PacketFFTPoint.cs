﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// FFT Points Table Packet
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketFFTPoint : PacketBase
    {
        /// <summary>
        /// The actual Packet type FFTPOINT TABLE)
        /// </summary>
        public byte Packet = (byte)PacketType.TABLE_FFT_POINTS;

        /// <summary>
        /// FFTPOINT Table Element
        /// </summary>
        [XmlElement("FFTPointsTable")]
        public EnumFFTPointRecords FFTPointTable = new EnumFFTPointRecords();

        /// <summary>
        /// Constructor
        /// </summary>
        public PacketFFTPoint()
        {
        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th May 2009
//  Add to project
//----------------------------------------------------------------------------

