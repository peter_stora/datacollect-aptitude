﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// 
   /// </summary>
   public class XmlSettingsNode
   {
      #region Private fields

      private MessagePreferences mMessagePreference = new MessagePreferences();
      private MachineSkips mMachineOkSkips = new MachineSkips();
      private MachineSkips mMachineNotOperatingSkips = new MachineSkips();

      #endregion

      /// <summary>
      /// Base Constructor
      /// </summary>
      public XmlSettingsNode()
      {
         this.AccessLevel = PacketBase.DEFAULT_USER_LEVEL;
         this.CanChangePassword = PacketBase.DEFAULT_CAN_CHANGE_PASSWORD;
         this.CreateWorkNotification = PacketBase.DEFAULT_CAN_CREATE_WORK_ORDER;
         this.DataCollectionTime = PacketBase.DEFAULT_DATA_COLLECTION_TIME;
         this.Password = PacketBase.DEFAULT_PASSWORD;
         this.ResetPassword = PacketBase.DEFAULT_RESET_PASSWORD;
         this.ScanRequiredToCollect = PacketBase.DEFAULT_SCAN_REQUIRED_TO_COLLECT;
         this.ShowPreData = PacketBase.DEFAULT_SHOW_PREVIOUS_DATE;
         this.VerifyMode = PacketBase.DEFAULT_VERIFY_MODE;
         this.ViewOverdueOnly = PacketBase.DEFAULT_VIEW_OVERDUE_ONLY;
         this.ViewTreeElement = PacketBase.DEFAULT_VIEW_TREE_ELEMS;
         this.ViewCMMSWorkHistory = PacketBase.DEFAULT_VIEW_WORK_ORDER_HISTORY;
         this.NumericRangeProtection = PacketBase.DEFAULT_RANGE_PROTECTION;
         this.ScanAndGoToFirstPoint = PacketBase.DEFAULT_SCAN_AND_GO_TO_FIRST_POINT;
         this.ViewSpectralFFT = PacketBase.DEFAULT_VIEW_FFT_SPECTRUMS;
         this.EnableCamera = PacketBase.DEFAULT_ENABLE_CAMERA;
         this.BarcodeLogin = PacketBase.DEFAULT_BARCODE_LOGIN;
         this.BarcodePassword = PacketBase.DEFAULT_BARCODE_PASSWORD;
         this.UserPreferences = PacketBase.Create_Default_User_Preferences( );
      }/* end constructor */

      /// <summary>
      /// Set the operators access level
      /// </summary>
      public OperatorRights AccessLevel
      {
         get;
         set;
      }

      /// <summary>
      /// Can the operator change the password
      /// </summary>
      public Boolean CanChangePassword
      {
         get;
         set;
      }

      /// <summary>
      /// Can the operator create a Work Notification
      /// </summary>
      public Boolean CreateWorkNotification
      {
         get;
         set;
      }

      /// <summary>
      /// Get or Set the time interval at which MARLIN considers data to be
      /// new (used to warn if MARLIN thinks data is being collected twice)
      /// </summary>
      public int DataCollectionTime
      {
         get;
         set;
      }

      /// <summary>
      /// The operators Password
      /// </summary>
      public String Password
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set: If on will reset the user's password to "skf"
      /// </summary>
      public ResetPassword ResetPassword
      {
         get;
         set;
      }

      /// <summary>
      /// Will the operator have to scan asset Tag before collecting data
      /// </summary>
      public Boolean ScanRequiredToCollect
      {
         get;
         set;
      }

      /// <summary>
      /// Enable to allow the operator to view the previous data prior 
      /// to collecting new data 
      /// </summary>
      public Boolean ShowPreData
      {
         get;
         set;
      }

      /// <summary>
      /// Specify whether to display the Feedback review display during 
      /// data collection. Choices are, Always, On Alarm, or Never
      /// </summary>
      public VerificationMode VerifyMode
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the tree filtering view. if true all POINTs 
      /// that have data collected against them will be hidden
      /// </summary>
      public OverdueFilter ViewOverdueOnly
      {
         get;
         set;
      }

      /// <summary>
      /// Specify the hierarchy items you would like to show on the 
      /// hierarchy display. Choices are POINT only , SET+POINT, 
      /// Machine+POINT, or All.
      /// </summary>
      public ViewTreeAs ViewTreeElement
      {
         get;
         set;
      }

      /// <summary>
      /// Can the operator view the work notification history
      /// </summary>
      public Boolean ViewCMMSWorkHistory
      {
         get;
         set;
      }

      /// <summary>
      /// Property: Will the system accept 'out of range' values from
      /// the numeric keypad
      /// </summary>
      public Boolean NumericRangeProtection
      {
         get;
         set;
      }

      /// <summary>
      /// Get or Set: When true, after an operator scans a Machine, they 
      /// will go to the first collection POINT - not the Machine dialog!
      /// </summary>
      public Boolean ScanAndGoToFirstPoint
      {
         get;
         set;
      }

      /// <summary>
      /// Get or Set: Can the operator view Spectral FFT data from the WMCD
      /// </summary>
      public Boolean ViewSpectralFFT
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether or not the camera option is available for the operator
      /// </summary>
      public Boolean EnableCamera
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the barcode used for the operator to login
      /// </summary>
      public string BarcodeLogin
      {
         get;
         set;
      }

      /// <summary>
      /// Gtes or sets whether or not a password is required for a barcode login
      /// </summary>
      public Boolean BarcodePassword
      {
         get;
         set;
      }

      /// <summary>
      /// Get or Set: Current user preferences
      /// </summary>
      public byte[] UserPreferences
      {
         get;
         set;
      }

      /// <summary>
      /// Property: What messages are to be displayed during data 
      /// collection
      /// </summary>
      public MessagePreferences MessagingPreferences
      {
         get
         {
            return mMessagePreference;
         }
         set
         {
            mMessagePreference = value;
         }
      }

      /// <summary>
      /// Get or Set: What type of points should be skipped when the
      /// operator hits the "Machine not Operating" button. Note: when
      /// all properties are set false, the "Machine not Operating"
      /// button will be disabled on the device
      /// </summary>
      public MachineSkips MachineNotOperatingSkips
      {
         get
         {
            return mMachineNotOperatingSkips;
         }
         set
         {
            mMachineNotOperatingSkips = value;
         }
      }

      /// <summary>
      /// Get or Set: What type of points can the operator skip when
      /// the "Machine OK" button is pressed. 
      /// </summary>
      public MachineSkips MachineOkSkips
      {
         get
         {
            return mMachineOkSkips;
         }
         set
         {
            mMachineOkSkips = value;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 19th February 2009 (10:45)
//  Add to project
//----------------------------------------------------------------------------
