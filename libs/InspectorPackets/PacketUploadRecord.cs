﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Serializable table containing all upload records and fields. It is this
   /// class which will ultimetely generate the final object hierarchy that will
   /// be serialized to XML and passed to @ptitude via Transaction Server
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketUploadRecord : PacketBase
   {
      private TimeZoneRecord mDeviceTimeZoneInfo = null;

      /// <summary>
      /// The actual Packet type (Upload File)
      /// </summary>
      public byte Packet = (byte) PacketType.UPLOAD_FILE;


      /// <summary>
      /// Profile returned from device
      /// </summary>
      [XmlElement( "DeviceUID" )]
      public String DeviceUID
      {
         get;
         set;
      }


      /// <summary>
      /// Profile created at
      /// </summary>
      [XmlElement( "UploadDateTime" )]
      public DateTime UploadDateTime
      {
         get;
         set;
      }


      /// <summary>
      /// Last Connected Operator
      /// </summary>
      [XmlElement( "LastOperator" )]
      public String LastOperator
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the device time-zone information
      /// </summary>
      [XmlElement( "DeviceTimeZoneInfo" )]
      public TimeZoneRecord DeviceTimeZoneInfo
      {
         get
         {
            return mDeviceTimeZoneInfo;
         }
         set
         {
            mDeviceTimeZoneInfo = value;
         }
      }


      /// <summary>
      /// Enumerated list of Node (Machine) Elements
      /// </summary>
      [XmlElement( "Nodes" )]
      public EnumUploadNodeRecords NodeTable = 
                new EnumUploadNodeRecords();


      /// <summary>
      /// Enumerated list of Container objects. Note: As Containers effectively
      /// encapsulate Route and Workspace objects, it is possible that the same
      /// TreeElemID can appear more than once in the XML Upload File!
      /// </summary>
      [XmlElement( "Containers" )]
      public EnumUploadContainerRecords Containers = 
               new EnumUploadContainerRecords();


      /// <summary>
      /// Enumerated list of upload structured log records. These records are 
      /// returned to the Middleware for potential further upload to @ptitude.
      /// It is not currenly known if this element of the upload data is actually
      /// required by @ptitude.
      /// </summary>
      [XmlElement( "StructuredLogs" )]
      public EnumUploadStructuredLogRecords StructuredLogTable = 
                new EnumUploadStructuredLogRecords();


      /// <summary>
      /// Enumerated list of Upload CMMS Work Notification tables. These tables
      /// are returned to the middleware layer, to provide validation that the
      /// returned Work Notification fields are still valid.
      /// This addresses OTDs #2719 and #2551
      /// </summary>
      [XmlElement( "ReferenceWorkNotifications" )]
      public CmmsUploadTables ReferenceWorkNotifications =
                new CmmsUploadTables();

      /// <summary>
      /// Enumerated list of Upload CMMS Work Notification records. These records
      /// are returned to the middleware layer, effectively as standalone items
      /// in that they are not tightly bound to the hierarchy.
      /// </summary>
      [XmlElement( "CMMSWorkNotifications" )]
      public EnumCmmsUploadNotificationRecords CMMSWorkNotifications =
                new EnumCmmsUploadNotificationRecords();


      /// <summary>
      /// Enumerated list of updated Operators. 
      /// </summary>
      [XmlElement( "Operators" )]
      public EnumUploadOperators Operators =
                new EnumUploadOperators();


      /// <summary>
      /// Enumerated list of updated Operator Settings. Although this will 
      /// typically just be changed passwords, there is no reason why (ultimately)
      /// any other similar setting could not also be uploaded to the server
      /// </summary>
      [XmlElement( "OperatorSettings" )]
      public EnumUploadSettingRecords OperatorSettings =
                new EnumUploadSettingRecords();


      /// <summary>
      /// Enumerated list of Saved Media records. These will typically be
      /// photographs or audio recordings made by the operator during their
      /// route - though any file format is actually supported. 
      /// </summary>
      [XmlElement( "Media" )]
      public EnumUploadSavedMediaRecords Media =
                new EnumUploadSavedMediaRecords();


      /// <summary>
      /// Base constructor (no overloads)
      /// </summary>
      public PacketUploadRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Clear all elements in the current Upload record
      /// </summary>
      public void ClearElements()
      {
         // clear the container objects
         if ( Containers != null )
         {
            for ( int i=0; i < Containers.Count; ++i )
            {
               // reference the current container record
               UploadContainerRecord container = Containers.Container[ i ];

               if ( container.PointsTable != null )
               {
                  for ( int j=0; j < container.PointsTable.Count; ++j )
                  {
                     // get the current point
                     UploadPointRecord point = container.PointsTable.POINT[ j ];

                     // clear all measurements associated with this point
                     ClearMeasurements( point );

                     // clear all notes associated with this point
                     ClearNotes( point );

                     // now delete the point itself
                     point = null;
                  }

                  // clear all points
                  container.PointsTable.POINT.Clear();
               }
            }
            // clear all containers
            Containers.Container.Clear();
         }

         // delete all nodes and clear the table
         if ( NodeTable != null )
         {
            for ( int i = 0; i < NodeTable.Count; ++i )
            {
               NodeTable.NODE[ i ] = null;
            }
            NodeTable.NODE.Clear();
         }

      }/* end method */


      /// <summary>
      /// Clear all note elements in an Upload Point
      /// </summary>
      /// <param name="point">work point</param>
      private void ClearNotes( UploadPointRecord point )
      {
         // delete all Coded Note Data and clear the buffer
         if ( point.PointNotes != null )
         {
            for ( int k=0; k < point.PointNotes.Count; ++k )
            {
               point.PointNotes.UploadNotes[ k ] = null;
            }
            point.PointNotes.UploadNotes.Clear();
         }
      }/* end method */

      
      /// <summary>
      /// Clear all measurement elements in an Upload Point
      /// </summary>
      /// <param name="point">work point</param>
      private void ClearMeasurements( UploadPointRecord point )
      {
         // delete all temp measurements and clear the buffer
         if ( point.Measurements != null )
         {
            if ( point.Measurements.TempData != null )
            {
               for ( int k=0; k < point.Measurements.TempData.Count; ++k )
               {
                  point.Measurements.TempData.Measurement[ k ] = null;
               }
               point.Measurements.TempData.Measurement.Clear();
            }

            // delete all vel measurements and clear the buffer
            if ( point.Measurements.VelData != null )
            {
               for ( int k=0; k < point.Measurements.VelData.Count; ++k )
               {
                  point.Measurements.VelData.Measurement[ k ] = null;
               }
               point.Measurements.VelData.Measurement.Clear();
            }

            // delete all env measurements and clear the buffer
            if ( point.Measurements.EnvData != null )
            {
               for ( int k=0; k < point.Measurements.EnvData.Count; ++k )
               {
                  point.Measurements.EnvData.Measurement[ k ] = null;
               }
               point.Measurements.EnvData.Measurement.Clear();
            }

            // delete all FFT Channel data and clear the buffer
            if ( point.Measurements.FFTChannel != null )
            {
               for ( int k=0; k < point.Measurements.FFTChannel.Count; ++k )
               {
                  point.Measurements.FFTChannel.FFTChannel[ k ] = null;
               }
               point.Measurements.FFTChannel.FFTChannel.Clear();
            }
         }

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------