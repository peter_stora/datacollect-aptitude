﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Extension to the Saved Media object. This object will enable 
   /// image and audio files to be downloaded to the device.
   /// </summary>
   [XmlRoot( ElementName = "ReferenceMedia" )]
   public class ReferenceMediaRecord
   {
      /// <summary>
      /// Default constructor for this class (required for serialization).
      /// </summary>
      public ReferenceMediaRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Get or set the record Identifier
      /// </summary>
      [XmlElement( "Uid" )]
      public Guid Uid
      {
         get;
         set;
      }


      /// <summary>
      /// Get or Set the @ptitude TreeElemID
      /// </summary>
      [XmlElement( "PointHostIndex" )]
      public int PointHostIndex
      {
         get;
         set;
      }


      /// <summary>
      /// Media type: 0x00: IMAGE
      ///             0x01: AUDIO
      ///             0x02: BINARY
      /// </summary>
      [XmlElement( "MediaType" )]
      public byte MediaType
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the source file name
      /// </summary>
      [XmlElement( "FileName" )]
      public string FileName
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the source file path
      /// </summary>
      [XmlElement( "FilePath" )]
      public string FilePath
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the size in bytes of the source file
      /// </summary>
      [XmlElement( "Size" )]
      public int Size
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15 September 2011
//  Replace Uid field with PointUid field
//
//  Revision 0.0 APinkerton 3rd July 2009
//  Add to project
//-------------------------------------------------------------------------------