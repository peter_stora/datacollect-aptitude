﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Node Table record entry
    /// </summary>
    public class NodeRecord
    {

        private byte mNodeFlag = (byte)FieldTypes.NodeFlagType.Other; 
        
        /// <summary>
        /// Constructor
        /// </summary>
        public NodeRecord()
        {
        }

        /// <summary>
        /// Current UID (GUID) for this node. Note: This value is
        /// derived and therefore independant of @ptitude
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// The Node descriptor Field (i.e. "Hierarchies", "Routes")
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Node friendly name (i.e. "My Route")
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Date and Time that this node was last modified!
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// The actual means by which the Node is located (i.e Barcode)
        /// </summary>
        public byte LocationMethod { get; set; }

        /// <summary>
        /// The actual Location Tag value (i.e. Barcode value)
        /// </summary>
        public string LocationTag { get; set; }

        /// <summary>
        /// The ID of the current nodes parent. Note: This value
        /// is derived and therefore independant of @ptitude
        /// </summary>
        public Guid ParentUid { get; set; }

        /// <summary>
        /// @ptitude Hierarchy Element ID (tied to specific Hierarchy)
        /// </summary>
        public int PRISM { get; set; }

        /// <summary>
        /// Hierarchy position of the current node. Note: This value
        /// is derived and therefore independant of @ptitude
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Heirarchy depth of the current node. Note: This value
        /// is derived and therefore independant of @ptitude
        /// </summary>
        public int HierarchyDepth { get; set; }

        /// <summary>
        /// What is the current flag status of the NODE object
        /// 0: NODE (SET OR HIERARCHY), 
        /// 1: NODE MACHINE,
        /// 2: NODE TAG CHANGED
        /// </summary>
        public byte Flags 
        {
            get { return mNodeFlag; }
            set { mNodeFlag = value; } 
        }

        /// <summary>
        /// Is the current route type standard or structured
        /// </summary>
        public byte Structured { get; set; }

        /// <summary>
        /// Analyst parent route ID, or 0 for hierarchy. Note: If the field
        /// ParentUid is null for a record containing a valid RouteId, then
        /// that field represents the actual name of the route.
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// CMMS Location TAG
        /// </summary>
        public string FunctionalLocation { get; set; }

        /// <summary>
        /// CMMS Work Type
        /// </summary>
        public int DefWorkType { get; set; }

        /// <summary>
        /// Result of the last inspection
        /// </summary>
        public byte LastInspResult { get; set; }

        /// <summary>
        /// Number of previous measurements downloaded to the device
        /// </summary>
        public byte NoLastMeas { get; set; }

        /// <summary>
        /// Is this node a route?
        /// </summary>
        public byte ElementType { get; set; }

        /// <summary>
        /// Collection check flag assigned to this element
        /// </summary>
        public byte CollectionCheck { get; set; }

        /// <summary>
        /// Current overall alarm state of this element
        /// </summary>
        public byte AlarmState { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th February 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------