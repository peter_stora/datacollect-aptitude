﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using SKF.RS.MicrologInspector.Packets;


namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Derived media class which will allow users to save images,
   /// audio files or any other custom binary type that needs to be
   /// recorded.
   /// </summary>
   public class SavedMediaRecord : UploadSavedMediaRecord
   {
      /// <summary>
      /// Common Image file formats
      /// </summary>
      private static readonly string[] IMAGE_FILES_TYPES = new string[] 
      { ".BMP", ".GIF",".IMG", ".JPG", ".JPEG", ".TIF", ".TIFF" };
      
      /// <summary>
      /// Common Audio file formats
      /// </summary>
      private static readonly string[] AUDIO_FILES_TYPES = new string[] 
      { ".AIF", ".IFF", ".MP3", ".M4A", ".MPA", ".WAV", ".WMA" };


      /// <summary>
      /// Common Video file formats
      /// </summary>
      private static readonly string[] VIDEO_FILES_TYPES = new string[] 
      { ".AVI", ".MPG", ".MPEG", ".MMV", ".MP4", ".MOV" };


      //-------------------------------------------------------------------------

      #region Constructors

      /// <summary>
      /// Default constructor for this class (required for serialization).
      /// </summary>
      public SavedMediaRecord()
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor to create a new 'Off Route' saved media object.
      /// Notes: Although this constructor will determine the media type from the 
      /// file name extension, for uncommon formats, the field MediaType should be
      /// set manually.
      /// </summary>
      /// <param name="iFileName">file name of saved file with extension</param>
      /// <param name="iFilePath">File save location or empty for My Documents</param>
      /// <param name="iOperId">current Operator Id</param>
      public SavedMediaRecord( string iFileName, string iFilePath, int iOperId ) :
         this( iFileName, iFilePath, iOperId, null )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor to create a new 'Off Route' saved media object
      /// with additional note text.
      /// Notes: Although this constructor will determine the media type from the 
      /// file name extension, for uncommon formats, the field MediaType should be
      /// set manually.
      /// </summary>
      /// <param name="iFileName">file name of saved file with extension</param>
      /// <param name="iFilePath">File save location or empty for My Documents</param>
      /// <param name="iOperId">current Operator Id</param>
      /// <param name="iPointHostIndex">Point reference (or "null" for off-route)</param>
      /// <param name="iNoteText">Free hand note text</param>
      public SavedMediaRecord( string iFileName, string iFilePath, int iOperId, string iNoteText ):
         this(iFileName, iFilePath, iOperId, BinaryMediaOrigin.OffRoute, null, null, iNoteText)
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor to create a new saved media object with a defined
      /// origin (Point, Machine or Work Notification) as well as note text
      /// Notes: Although this constructor will determine the media type from the 
      /// file name extension, for uncommon formats, the field MediaType should be
      /// set manually.
      /// </summary>
      /// <param name="iFileName">file name of saved file with extension</param>
      /// <param name="iFilePath">File save location or empty for My Documents</param>
      /// <param name="iOperId">current Operator Id</param>
      /// <param name="iPointHostIndex">Point reference (or "null" for off-route)</param>
      /// <param name="iNoteText">Free hand note text</param>
      public SavedMediaRecord( string iFileName, string iFilePath, int iOperId,
         BinaryMediaOrigin iOrigin, int? iPointHostIndex, int? iWorkNotificationId, string iNoteText )
      {
         //
         // if this is 'off route' then ensure that both the Point Index 
         // and the Work Notification ID are set to null
         // 
         if ( iOrigin == BinaryMediaOrigin.OffRoute )
         {
            iPointHostIndex = null;
            iWorkNotificationId = null;
         }

         //
         // if not a Work Notification then ensure Work Notification ID is null
         //
         if ( iOrigin != BinaryMediaOrigin.WorkNotification )
         {
            iWorkNotificationId = null;
         }

         // reference the origin
         this.MediaOrigin = (byte) iOrigin;

         // reference any Work Notification ID, else null if none 
         if ( iWorkNotificationId.HasValue )
         {
            this.WNID = iWorkNotificationId.Value;
         }
         else
         {
            this.WNID = -1;
         }

         // create a new key field
         this.Uid = Guid.NewGuid();

         // if the point index is null, then assume this is an off-route image
         if ( iPointHostIndex.HasValue )
         {
            this.PointHostIndex = iPointHostIndex.Value;
         }
         else
         {
            this.PointHostIndex = -1;
         }

         // we must have a filename!
         this.FileName = iFileName;

         // if no valid file path, then save to My Documents
         if ( iFilePath != string.Empty )
         {
            this.FilePath = iFilePath;
         }
         else
         {
            this.FilePath = GetDefaultPath( iFileName );
         }

         // derive the media type
         this.MediaType = GetFileType( iFileName );

         // set the note text field
         this.Note = iNoteText;

         // set the operator ID
         this.OperId = iOperId;

         // set default the upload flag to false (upload still pending)
         this.Uploaded = false;

         // calculate the file size (assuming the file exists!
         this.Size = GetFileSize( this.FilePath, this.FileName );

         // get the CRC value
         this.CRC32 = GetFileCRC( this.FilePath, this.FileName );

      }/* end constructor */

      #endregion

      //-------------------------------------------------------------------------

      #region Serializable Properties

      /// <summary>
      /// Get or set the source file path
      /// </summary>
      [XmlElement( "FilePath" )]
      public string FilePath
      {
         get;
         set;
      }


      /// <summary>
      /// Get or set the source file has been uploaded to @ptitude
      /// </summary>
      [XmlElement( "Uploaded" )]
      public Boolean Uploaded
      {
         get;
         set;
      }

      #endregion

      //-------------------------------------------------------------------------

      /// <summary>
      /// Does this file exist?
      /// </summary>
      /// <returns></returns>
      public Boolean FileExists()
      {
         return File.Exists( Get_Safe_File_Name( FilePath, FileName ) );

      }/* end method */


      /// <summary>
      /// Gets the fully qualified path
      /// </summary>
      /// <returns></returns>
      public string GetFullyQualifiedPath()
      {
         return Get_Safe_File_Name( FilePath, FileName );
      }


      /// <summary>
      /// Gets the file type as enum type BinaryMediaType
      /// </summary>
      /// <param name="iFileName">raw file name</param>
      /// <returns>media type</returns>
      public static BinaryMediaType GetBinaryFileType( string iFileName )
      {
         // get the file extension (in upper case)
         string extension = Path.GetExtension( iFileName ).ToUpper();

         // is this an image file?
         foreach ( string media in IMAGE_FILES_TYPES )
         {
            if ( extension == media )
            {
               return BinaryMediaType.Image;
            }
         }

         // is this an audio file?
         foreach ( string media in AUDIO_FILES_TYPES )
         {
            if ( extension == media )
            {
               return BinaryMediaType.Audio;
            }
         }

         // is this a video file?
         foreach ( string media in VIDEO_FILES_TYPES )
         {
            if ( extension == media )
            {
               return BinaryMediaType.Video;
            }
         }

         // Ok, we give in..
         return BinaryMediaType.RawBinary;
      
      }/* end method */


      /// <summary>
      /// Get the media file type for a given file extension
      /// </summary>
      /// <param name="iFileName">raw file name</param>
      /// <returns>media type cast as byte</returns>
      public byte GetFileType( string iFileName )
      {
         // get the file extension (in upper case)
         string extension = Path.GetExtension( iFileName ).ToUpper();

         // is this an image file?
         foreach ( string media in IMAGE_FILES_TYPES )
         {
            if ( extension == media )
            {
               return (byte) BinaryMediaType.Image;
            }
         }

         // is this an audio file?
         foreach ( string media in AUDIO_FILES_TYPES )
         {
            if ( extension == media )
            {
               return (byte) BinaryMediaType.Audio;
            }
         }

         // is this a video file?
         foreach ( string media in VIDEO_FILES_TYPES )
         {
            if ( extension == media )
            {
               return (byte) BinaryMediaType.Video;
            }
         }

         // Ok, we give in..
         return (byte) BinaryMediaType.RawBinary;

      }/* end method */


      /// <summary>
      /// Get the byte size of the specified media file
      /// </summary>
      /// <param name="iFilePath">full path</param>
      /// <param name="iFileName">name with extension</param>
      /// <returns>size in bytes</returns>
      public long GetFileSize( string iFilePath, string iFileName )
      {
         try
         {
            var fi = new FileInfo( Get_Safe_File_Name( iFilePath, iFileName ) );
            return fi.Length;
         }
         catch
         {
            return -1;
         }
      }


      /// <summary>
      /// Get a CRC32 value for the specified media file
      /// </summary>
      /// <param name="iFilePath">full path</param>
      /// <param name="iFileName">name with extension</param>
      /// <returns>four byte CRC value as a string</returns>
      public string GetFileCRC( string iFilePath, string iFileName )
      {
         // create new CRC and string builder objects
         var crc32 = new Crc32();
         var hash = new StringBuilder();

         // process all bytes in the file stream 
         using ( var fs = File.Open( Get_Safe_File_Name( iFilePath, iFileName ), FileMode.Open ) )
         {
            foreach ( byte b in crc32.ComputeHash( fs ) )
               hash.Append( b.ToString( "x2" ).ToLower() );
         }

         // return the final CRC value
         return hash.ToString();
      }


      /// <summary>
      /// Gets the safe file name
      /// </summary>
      /// <param name="iFilePath">Path to file</param>
      /// <param name="iFileName">File name with extension</param>
      /// <returns></returns>
      private string Get_Safe_File_Name( string iFilePath, string iFileName )
      {
         var correctedFileName = string.Empty;

         if( iFilePath == "\\" )
            correctedFileName = "\\" + iFileName;
         else
            correctedFileName = @iFilePath + "\\" + @iFileName;

         return correctedFileName;
      }

      //-------------------------------------------------------------------------

      /// <summary>
      /// Returns a default path to the specified media file
      /// </summary>
      /// <param name="iFileName">Filename with extension</param>
      /// <returns>Path as returned by SHGetSpecialFolder P\Invoke</returns>
      private static string GetDefaultPath( string iFileName )
      {
         switch ( GetBinaryFileType( iFileName ) )
         {
            case BinaryMediaType.Audio:
               return PacketBase.MyMusicPath;
            case BinaryMediaType.Image:
               return PacketBase.MyPicturesPath;
            case BinaryMediaType.Video:
               return PacketBase.MyVideosPath;
            case BinaryMediaType.RawBinary:
               return PacketBase.MyDocumentsPath;
         }

         return PacketBase.MyDocumentsPath;

      }/* end method */

      //-------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15 September 2011
//  Update for Inspector Version 2.0
//
//  Revision 0.0 APinkerton 8th June 2009
//  Add to project
//-------------------------------------------------------------------------------