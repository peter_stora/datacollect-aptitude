﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Manage the Server driven UR Packet Type
   /// </summary>
   public class ServerConnect : PacketBase
   {
      private SupportedDevices mSupportedDevices = 
         new SupportedDevices();

      /// <summary>
      /// Gets or sets the server type
      /// </summary>
      [XmlElement( "ServerType" )]
      public MiddlewareType ServerType
      {
         get;
         set;
      }
      

      /// <summary>
      /// Gets or sets the profile type
      /// </summary>
      [XmlElement( "ProfileType" )]
      public ProfileType ProfileType
      {
         get;
         set;
      }
      

      /// <summary>
      /// Gets or sets a list of supported Microlog Devices
      /// </summary>
      [XmlElement( "Devices" )]
      public SupportedDevices Devices
      {
         get
         {
            return mSupportedDevices;
         }
         set
         {
            mSupportedDevices = value;
         }
      }


      /// <summary>
      /// Base Constructor
      /// </summary>
      public ServerConnect()
         : this( DEFAULT_SERVER, DEFAULT_PROFILE )
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iServerType">Server Type (Desktop/Enterprise)</param>
      /// <param name="iProfileType">Profile Type (User/Desktop)</param>
      public ServerConnect( MiddlewareType iServerType,
                           ProfileType iProfileType )
      {
         this.ServerType = iServerType;
         this.ProfileType = iProfileType;

      }/* end constructor */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 25th January 2009
//  Add to project
//----------------------------------------------------------------------------