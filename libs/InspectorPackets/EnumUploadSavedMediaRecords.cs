﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Enumeration of Upload Saved Media object records. These are the 
   /// image and audio files, taken (or recorded) by the operator during
   /// their route. The UploadSavedImage is almost identical to the
   /// SavedImage, except for the additional PointUid field in the Upload
   /// </summary>
   public class EnumUploadSavedMediaRecords
   {
      protected List<UploadSavedMediaRecord> mMediaRecord;
      protected int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumUploadSavedMediaRecords()
      {
         mMediaRecord = new List<UploadSavedMediaRecord>();
      }/* end constructor */

      /// <summary>
      /// Get or set elements in the raw Media Record object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      [XmlElement( "SavedMedia" )]
      public List<UploadSavedMediaRecord> SavedMedia
      {
         get
         {
            return mMediaRecord;
         }
         set
         {
            mMediaRecord = value;
         }
      }

      /// <summary>
      /// Add a new Media record to the list
      /// </summary>
      /// <param name="item">populated Media record object</param>
      /// <returns>number of Media objects available</returns>
      public int AddMediaRecord( UploadSavedMediaRecord item )
      {
         mMediaRecord.Add( item );
         return mMediaRecord.Count;
      }


      /// <summary>
      /// Delete a single media record
      /// </summary>
      /// <param name="item">object to remove</param>
      /// <returns>new record count</returns>
      public int DeleteMediaRecord( UploadSavedMediaRecord item )
      {
         mMediaRecord.Remove( item );
         return mMediaRecord.Count;
      }


      /// <summary>
      /// How many Media records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mMediaRecord.Count;
         }
      }

   }/* end class */


   /// <summary>
   /// Class EnumUploadSavedMediaRecords with support for IEnumerable. This extended
   /// functionality enables not only foreach support - but also (and much
   /// more importantly) LINQ support!
   /// </summary>
   public class EnumUploadSavedMedia : EnumUploadSavedMediaRecords, IEnumerable, IEnumerator
   {
      #region Support IEnumerable Interface

      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mMediaRecord.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mMediaRecord[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

      #endregion

   }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th June 2009
//  Add to project
//----------------------------------------------------------------------------
