﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{

    /// <summary>
    /// Class to manage an enumerated list of upload Upload Measurments
    /// </summary>
    public class UploadPointMeasurements
    {
        protected List<UploadMeasurement> mPointMeasurements;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public UploadPointMeasurements()
        {
            mPointMeasurements = new List<UploadMeasurement>();
        }/* end constructor */


        /// <summary>
        /// Get or set elements in the raw MeasurementRecord object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("Measurement")]
        public List<UploadMeasurement> Measurement
        {
            get { return mPointMeasurements; }
            set { mPointMeasurements = value; }
        }

        /// <summary>
        /// Add a new measurement to the measurements list
        /// </summary>
        /// <param name="item">measurement item</param>
        /// <returns>number or measurements</returns>
        public int AddMeasurement(UploadMeasurement item)
        {
            mPointMeasurements.Add(item);
            return mPointMeasurements.Count;
        }

        /// <summary>
        /// How many measurements are now available
        /// </summary>
        public int Count { get { return mPointMeasurements.Count; } }

    }/* end class */



    /// <summary>
    /// Class UploadMeasurementList with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumUploadMeasurements : UploadPointMeasurements, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mPointMeasurements.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mPointMeasurements[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 30th April 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------
