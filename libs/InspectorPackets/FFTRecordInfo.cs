﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// FFT Record defaults
   /// </summary>
   public static class FFTRecordInfo
   {
      //-----------------------------------------------------------------------

      #region Enumerations
      
      /// <summary>
      /// FFT Data types
      /// </summary>
      public enum DataType
      {
         None = -1,
         Magnitude = 0,
         Phase = 2,
         Time = 3,
         MotorCurrent = 20
      }

      /// <summary>
      /// FFT Detection types
      /// </summary>
      public enum DetectionType
      {
         Peak = 0,
         PeakToPeak = 1,
         RMS = 2
      }

      /// <summary>
      /// FFT Windowing types
      /// </summary>
      public enum WindowType
      {
         Hanning = 1,
         FlatTop = 2,
         Uniform = 3
      }

      // FFT channel identifiers
      public enum Channels
      {
         None = 0x00,
         Envelope = 0x01,
         Velocity = 0x02,
         Acceleration = 0x03
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Constants

      /// <summary>
      /// Maximum number of FFT lines
      /// </summary>
      public static readonly int MAX_FFT_LINES = 1024;

      /// <summary>
      /// FFT Buffer size
      /// </summary>
      public static readonly int FFT_DATA_LEN = 3072;

      /// <summary>
      /// Default overload
      /// </summary>
      public static readonly int DEFAULT_OVERLOAD = 0;

      /// <summary>
      /// Default running speed
      /// </summary>
      public static readonly int DEFAULT_RUNNING_SPEED = 3600;



      /// <summary>
      /// Default data type for envelope
      /// </summary>
      public static readonly int DEFAULT_DATA_TYPE_ENVELOPE = (int) DataType.Magnitude;

      /// <summary>
      /// Default detection type for envelope
      /// </summary>
      public static readonly int DEFAULT_DETECTION_ENVELOPE = (int) DetectionType.PeakToPeak;

      /// <summary>
      /// Default number of ENVELOPE FFT lines
      /// </summary>
      public static readonly int DEFAULT_LINES_ENVELOPE = 800;

      /// <summary>
      /// Default number of enveloped averages
      /// </summary>
      public static readonly int DEFAULT_AVG_ENVELOPE = 2;

      /// <summary>
      /// Default window for envelope
      /// </summary>
      public static readonly int DEFAULT_WINDOW_ENVELOPE = (int)WindowType.Hanning;

      /// <summary>
      /// Default factor for envelope
      /// </summary>
      public static readonly float DEFAULT_FACTOR_ENVELOPE = 1.0f;

      /// <summary>
      /// Default offset for envelope
      /// </summary>
      public static readonly float DEFAULT_OFFSET_ENVELOPE = 0.0f;

      /// <summary>
      /// Default Fmax for envelope
      /// </summary>
      public static readonly float DEFAULT_FMAX_ENVELOPE = 2000.0f;

      /// <summary>
      /// Default Fmin for envelope
      /// </summary>
      public static readonly float DEFAULT_FMIN_ENVELOPE = 0.0f;



      /// <summary>
      /// Default data type for velocity
      /// </summary>
      public static readonly int DEFAULT_DATA_TYPE_VELOCITY = (int) DataType.Magnitude;

      /// <summary>
      /// Default detection type for envelope
      /// </summary>
      public static readonly int DEFAULT_DETECTION_VELOCITY = (int) DetectionType.RMS;

      /// <summary>
      /// Maximum number of VELOCITY FFT lines
      /// </summary>
      public static readonly int DEFAULT_LINES_VELOCITY = 400;

      /// <summary>
      /// Default number of velocity averages
      /// </summary>
      public static readonly int DEFAULT_AVG_VELOCITY = 1;

      /// <summary>
      /// Default window for velocity
      /// </summary>
      public static readonly int DEFAULT_WINDOW_VELOCITY = (int) WindowType.Hanning;

      /// <summary>
      /// Default factor for velocity
      /// </summary>
      public static readonly float DEFAULT_FACTOR_VELOCITY = 1.0f;

      /// <summary>
      /// Default offset for velocity
      /// </summary>
      public static readonly float DEFAULT_OFFSET_VELOCITY = 0.0f;

      /// <summary>
      /// Default Fmax for velocity
      /// </summary>
      public static readonly float DEFAULT_FMAX_VELOCITY = 1000.0f;

      /// <summary>
      /// Default Fmin for velocity
      /// </summary>
      public static readonly float DEFAULT_FMIN_VELOCITY = 0.0f;




      /// <summary>
      /// Default data type for acceleration
      /// </summary>
      public static readonly int DEFAULT_DATA_TYPE_ACCEL = (int) DataType.Magnitude;

      /// <summary>
      /// Default detection type for acceleration
      /// </summary>
      public static readonly int DEFAULT_DETECTION_ACCEL = (int) DetectionType.PeakToPeak;

      /// <summary>
      /// Maximum number of acceleration FFT lines
      /// </summary>
      public static readonly int DEFAULT_LINES_ACCEL = 800;

      /// <summary>
      /// Default number of acceleration averages
      /// </summary>
      public static readonly int DEFAULT_AVG_ACCEL = 2;

      /// <summary>
      /// Default window for acceleration
      /// </summary>
      public static readonly int DEFAULT_WINDOW_ACCEL = (int) WindowType.Hanning;

      /// <summary>
      /// Default factor for acceleration
      /// </summary>
      public static readonly float DEFAULT_FACTOR_ACCEL = 1.0f;

      /// <summary>
      /// Default offset for acceleration
      /// </summary>
      public static readonly float DEFAULT_OFFSET_ACCEL = 0.0f;

      /// <summary>
      /// Default Fmax for acceleration
      /// </summary>
      public static readonly float DEFAULT_FMAX_ACCEL = 2000.0f;

      /// <summary>
      /// Default Fmin for acceleration
      /// </summary>
      public static readonly float DEFAULT_FMIN_ACCEL = 0.0f; 


      #endregion

      //-----------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico August 12th 2010
//  Add to project
//----------------------------------------------------------------------------