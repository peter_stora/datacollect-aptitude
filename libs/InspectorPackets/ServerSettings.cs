﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class ServerSettings : PacketBase
   {
      [XmlElement( "HostName" )]
      public string HostName
      {
         get;
         set;
      }
      
      [XmlElement( "IPAddress" )]
      public string IPAddress
      {
         get;
         set;
      }
      
      [XmlElement( "UseIPAddress" )]
      public Boolean UseIPAddress
      {
         get;
         set;
      }
   
      [XmlElement( "PortNumber" )]
      public UInt16 PortNumber
      {
         get;
         set;
      }

      [XmlElement( "FriendlyName" )]
      public string FriendlyName
      {
         get;
         set;
      }

      [XmlElement( "CradleDetect" )]
      public Boolean CradleDetect
      {
         get;
         set;
      }

      [XmlElement( "UseTestID" )]
      public Boolean UseTestID
      {
         get;
         set;
      }


      /// <summary>
      /// Constructor
      /// </summary>
      public ServerSettings()
      {
         this.HostName = "localhost";
         this.IPAddress = "127.0.0.1";
         this.UseIPAddress = false;
         this.PortNumber = 8086;
         this.FriendlyName = "SKFMI";
         this.CradleDetect = false;
         this.UseTestID = false;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th February 2010
//  Add to project
//----------------------------------------------------------------------------
