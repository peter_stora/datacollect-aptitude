﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Simple class to manage the return of progress bar information
   /// from the database engine. Events are only raised on a change in
   /// the current progress value, the overall value should be either
   /// read or set using the OverallProgress property.
   /// 
   /// Note: Two value types are returned:
   /// 
   /// a. RawValue - which is simply the current progress value 
   /// b. CorrectedValue - which corrects the value based on where the
   ///    current action is to be displayed on the ProgressBar
   /// </summary>
   public class DataProgress : IDataProgress
   {
      // default number of update steps
      public static int PROGRESS_UPDATE_STEPS = 50;

      // set the default progress style to current
      private ProgressTarget mShowProgressStyle = ProgressTarget.Current;

      // the current progress value
      private int mValue = 0;

      // how many feedback steps should be returned
      private int mFeedbackSteps = PROGRESS_UPDATE_STEPS;

      // how many items are there to actualy process
      private int mTotalItemsToProcess = 100;

      // current item number
      private int mItemNumber = 0;

      // how many items to receive before raing an event
      private int mItemsPerStep = 1;

      // last item number used to raise an event
      private int mLastItemPosition = 0;

      // how is this progress value to be calculated
      private DisplayPosition mDisplayAs = DisplayPosition.FullSweep;


      /// <summary>
      /// Base Constructor
      /// </summary>
      public DataProgress()
         : this( DisplayPosition.FullSweep )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iDisplayAs">Position on progress bar to show value</param>
      public DataProgress( DisplayPosition iDisplayAs )
      {
         mDisplayAs = iDisplayAs;
      }/* end constructor */


      /// <summary>
      /// Gets or sets the task type
      /// </summary>
      public DisplayPosition DisplayAt
      {
         get
         {
            return mDisplayAs;
         }
         set
         {
            mDisplayAs = value;
         }
      }


      /// <summary>
      /// Gets or sets whether the overall progress should be shown
      /// </summary>
      public ProgressTarget Target
      {
         get
         {
            return mShowProgressStyle;
         }
         set
         {
            mShowProgressStyle = value;
         }
      }


      /// <summary>
      /// Gets or sets the current progress position 
      /// </summary>
      public int RawValue
      {
         get
         {
            return mValue;
         }
         set
         {
            mValue = value;
         }
      }


      /// <summary>
      /// Gets the offset corrected progress position
      /// </summary>
      public int CorrectedValue
      {
         get
         {
            return GetCorrectedValue();
         }
      }


      /// <summary>
      /// Gets or sets the total number of events to raise during this task 
      /// </summary>
      public int FeedbackSteps
      {
         get
         {
            return mFeedbackSteps;
         }
         set
         {
            if ( ( value <= 100 ) & ( value >= 1 ) )
            {
               mFeedbackSteps = value;
               SetItemsPerStep();
            }
         }
      }


      /// <summary>
      /// Gets or sets the total number of items to process
      /// </summary>
      public int NoOfItemsToProcess
      {
         get
         {
            return mTotalItemsToProcess;
         }
         set
         {
            mTotalItemsToProcess = value;
            SetItemsPerStep();
         }
      }


      /// <summary>
      /// Progress position management
      /// </summary>
      /// <param name="iTable">source table</param>
      /// <param name="iProgressValue">value as percentage</param>
      public delegate void ProgressPosition( object Sender );
      public event ProgressPosition OnUpdate;
      public void RaiseEvent()
      {
         if ( OnUpdate != null )
         {
            OnUpdate( this );
         }

      }/* end method */


      /// <summary>
      /// Reset all progress values to initial state
      /// </summary>
      public void Reset()
      {
         mValue = 0;
         mItemNumber = 0;
         mLastItemPosition = 0;

      }/* end method */


      /// <summary>
      /// If current process item requires a refresh of the current
      /// progress value, then calculate new position and raise event
      /// </summary>
      public void Update( Boolean iRaiseEvent )
      {
         // increment the item number
         mItemNumber++;

         // do we want to raise an event here?
         if ( mItemNumber >= ( mLastItemPosition + mItemsPerStep ) )
         {
            // record this item position
            mLastItemPosition = mItemNumber;

            // get the current progress position as a percentage
            double percent = 100.00 * (double) mItemNumber / (double) mTotalItemsToProcess;

            // set the current progress position (and limit to a mximum of 100%)
            if ( percent < 100.00 )
            {
               mValue = (int) percent;
            }
            else
            {
               mValue = 100;
            }

            // raise an event if required
            if ( iRaiseEvent )
            {
               RaiseEvent();
            }
         }

      }/* end method */


      /// <summary>
      /// Jump to a specified value
      /// </summary>
      public void JumpToValue( int iValue )
      {
         if ( ( iValue >= 0 ) & ( iValue <= 100 ) )
         {
            mValue = iValue;
            RaiseEvent();
         }

      }/* end method */


      /// <summary>
      /// How many steps should we receive before raising a progress event
      /// </summary>
      private void SetItemsPerStep()
      {
         if ( mFeedbackSteps >= mTotalItemsToProcess )
         {
            mItemsPerStep = 1;
         }
         else
         {
            mItemsPerStep = mTotalItemsToProcess / mFeedbackSteps;
         }

      }/* end method */


      /// <summary>
      /// Calculates the corrected progress position based on the display 
      /// location (i.e. full, first or second half)
      /// </summary>
      /// <returns>value as integer</returns>
      private int GetCorrectedValue()
      {
         if ( this.DisplayAt == DisplayPosition.FirstHalf )
         {
            return mValue / 2;
         }
         else if ( this.DisplayAt == DisplayPosition.SecondHalf )
         {
            return 50 + ( mValue / 2 );
         }
         else
         {
            return mValue;
         }

      }/* end method */

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 5th October 2009
//  Add to project
//----------------------------------------------------------------------------
