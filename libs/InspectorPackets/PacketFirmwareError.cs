﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SKF.RS.MicrologInspector.Packets
{
   /// <summary>
   /// Notify Server that synchronization failed due to Firware Version Error
   /// </summary>
   [XmlRoot( ElementName = ACK_ELEMENT_NAME )]
   public class PacketFirmwareError : PacketBase
   {
      /// <summary>
      /// Packet Type
      /// </summary>
      public byte Packet = (byte) PacketType.FIRMWARE_ERROR;

      /// <summary>
      /// Date and time of failure event
      /// </summary>
      public DateTime AT = PacketBase.DateTimeNow;

      /// <summary>
      /// Gets or sets the Device UID of the disconnecting object
      /// </summary>
      public string UID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the device packets version
      /// </summary>
      public string DevicePacketsVersion
      {
         get;
         set;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 28th July 2010
//  Add to project
//----------------------------------------------------------------------------
