﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Single Message table record entry
    /// </summary>
    [XmlRoot(ElementName = "MESSAGE")]
    public class MessageRecord
    {
        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        public MessageRecord()
        {
        }/* end constructor */

        /// <summary>
        /// Current UID (GUID) for this node. Note: This value is
        /// derived and therefore independant of @ptitude
        /// </summary>
        public Guid Uid { get; set; }

        /// <summary>
        /// Associated Node Uid (from Marlin Node table)
        /// </summary>
        public Guid NodeUid { get; set; }

        /// <summary>
        /// Message Caption
        /// </summary>
        public string ID {get; set;}
        
        /// <summary>
        /// Hierachy position of the current point
        /// </summary>
        public int Number {get; set;}

        /// <summary>
        /// Freehand text message (Analyst originated)
        /// </summary>
        public string Text { get; set; }

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 8th March 2009 (19:15)
//  Add to project
//----------------------------------------------------------------------------