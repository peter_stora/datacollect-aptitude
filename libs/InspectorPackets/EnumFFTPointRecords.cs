﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Enumeration of FFT Point object records
    /// </summary>
    [XmlRoot(ElementName = "DocumentElement")]
    public class EnumFFTPointRecords
    {
        protected List<FFTPointRecord> mFFTPointRecord;
        protected int mIndex = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnumFFTPointRecords()
        {
            mFFTPointRecord = new List<FFTPointRecord>();
        }/* end constructor */

        /// <summary>
        /// Get or set elements in the raw FFT Point Record object list.
        /// This is primarily supplied for LINQ implementations
        /// </summary>
        [XmlElement("FFTPoint")]
        public List<FFTPointRecord> FFTPoint
        {
            get { return mFFTPointRecord; }
            set { mFFTPointRecord = value; }
        }

        /// <summary>
        /// Add a new FFT Point record to the list
        /// </summary>
        /// <param name="item">populated FFT Point record object</param>
        /// <returns>number of FFT Point objects available</returns>
        public int AddFFTPointRecord(FFTPointRecord item)
        {
            mFFTPointRecord.Add(item);
            return mFFTPointRecord.Count;
        }

        /// <summary>
        /// How many FFT Point records are now available
        /// </summary>
        public int Count { get { return mFFTPointRecord.Count; } }

    }/* end class */


    /// <summary>
    /// Class EnumNodeRecords with support for IEnumerable. This extended
    /// functionality enables not only foreach support - but also (and much
    /// more importantly) LINQ support!
    /// </summary>
    public class EnumFFTPoints : EnumFFTPointRecords, IEnumerable, IEnumerator
    {
        #region Support IEnumerable Interface

        /// <summary>
        /// Definition for IEnumerator.Reset() method.
        /// </summary>
        public void Reset()
        {
            mIndex = -1;
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.MoveNext() method.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return (++mIndex < mFFTPointRecord.Count);
        }/* end method */


        /// <summary>
        /// Definition for IEnumerator.Current Property.
        /// </summary>
        public object Current
        {
            get
            {
                return mFFTPointRecord[mIndex];
            }
        }/* end method */


        /// <summary>
        /// Definition for IEnumerable.GetEnumerator() method.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }/* end method */

        #endregion

    }/* end class */


}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 11th May 2009
//  Add to project
//----------------------------------------------------------------------------