﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SKF.RS.MicrologInspector.Packets
{
    /// <summary>
    /// Serializable Points Table Packet 
    /// </summary>
    [XmlRoot(ElementName = ACK_ELEMENT_NAME)]
    public class PacketPointTable : PacketBase
    {
        /// <summary>
        /// The actual Packet type (Points Table)
        /// </summary>
        public byte Packet = (byte)PacketType.TABLE_POINTS;

        /// <summary>
        /// Operator Settings Element
        /// </summary>
        [XmlElement("PointsTable")]
        public EnumPointRecords PointsTable = new EnumPointRecords();

        /// <summary>
        /// Constructor
        /// </summary>
        public PacketPointTable()
        {
        }/* end constructor */

    }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th March 2009 (11:55)
//  Add to project
//----------------------------------------------------------------------------