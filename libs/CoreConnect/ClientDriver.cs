﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Very simple class that returns the ActiveSync default client type.
   /// Note: this is a legacy class and should not be carried forward into
   /// the next WinRT version of Microlog Inspector
   /// </summary>
   public static class ClientDriver
   {
      /// <summary>
      /// Path for detecting the ActiveSync USB type
      /// </summary>
      private const string DEFAULT_ACTIVESYNCH_PATH = @"Drivers\USB";

      /// <summary>
      /// Key holding the value flag
      /// </summary>
      private const string FUNCTION_DRIVERS_KEY = @"FunctionDrivers";

      /// <summary>
      /// Value flag
      /// </summary>
      private const string DEFAULT_CLIENT_DRIVER_VALUE = "DefaultClientDriver";

      /// <summary>
      /// Network connection type
      /// </summary>
      private const string DRIVER_RNDIS = "RNDIS";

      /// <summary>
      /// Network connection type
      /// </summary>
      private const string DRIVER_SERIAL = "Serial_Class";


      /// <summary>
      /// Returns the raw string value from [HKLM\Drivers\USB\FunctionDrivers]
      /// </summary>
      /// <returns>"RNDIS", "Serial_Class" or empty on error</returns>
      public static string Get_Client_Driver_Value()
      {
         // default result
         string result = string.Empty;

         // base registry connection
         IRegistryIO reg = new RegistryIO();

         // get the key value
         try
         {
            reg.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                          DEFAULT_ACTIVESYNCH_PATH,
                          FUNCTION_DRIVERS_KEY,
                          DEFAULT_CLIENT_DRIVER_VALUE,
                          ref result );
         }
         catch
         {
            return string.Empty;
         }

         return result;
      }


      /// <summary>
      /// Returns the enum value from [HKLM\Drivers\USB\FunctionDrivers]
      /// </summary>
      /// <returns></returns>
      public static ActiveSyncDriverType Get_Client_Driver_Type()
      {
         // default result
         string result = string.Empty;

         // base registry connection
         IRegistryIO reg = new RegistryIO();

         // get the key value
         try
         {
            reg.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                          DEFAULT_ACTIVESYNCH_PATH,
                          FUNCTION_DRIVERS_KEY,
                          DEFAULT_CLIENT_DRIVER_VALUE,
                          ref result );
         }
         catch
         {
            return ActiveSyncDriverType.unknown;
         }

         // if we made it this far then return the actual type
         if ( result == DRIVER_RNDIS )
         {
            return ActiveSyncDriverType.rndis;
         }
         else if ( result == DRIVER_SERIAL )
         {
            return ActiveSyncDriverType.serial;
         }
         else
         {
            return ActiveSyncDriverType.unknown;
         }
      }
   
   }/* end class */


   /// <summary>
   /// Enumeration of ActiveSync default driver types
   /// </summary>
   public enum ActiveSyncDriverType
   {
      rndis,
      serial,
      unknown
   }

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton August 22nd 2012
//  Add to project
//----------------------------------------------------------------------------