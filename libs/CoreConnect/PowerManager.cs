﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;
using System.Threading;

namespace SKF.RS.MicrologInspector.HardwareConnect
{

   /// <summary>
   /// This class is used to provide change in power state notifications
   /// NOTE: USE THIS CLASS WITH EXTREME CAUTION!!
   /// </summary>
   public class PowerManager : IDisposable
   {
      //-----------------------------------------------------------------------

      #region Constants

      /// <summary>
      /// Indicates that an application would like to receive all types of
      /// power notifications.
      /// </summary>
      private const uint POWER_NOTIFY_ALL = 0xFFFFFFFF;

      /// <summary>
      /// Indicates an infinite wait period
      /// </summary>
      private const int INFINITE = -1;

      /// <summary>
      /// Allocate message buffers on demand and free the message buffers after they are read.
      /// </summary>
      private const int MSGQUEUE_NOPRECOMMIT = 1;

      /// <summary>
      /// How many messages can the queue hold
      /// </summary>
      private const int MAX_MESSAGES = 32;

      /// <summary>
      /// Maximum number of bytes in each message
      /// </summary>
      private const int MAX_MESSAGED_BYTES = 0x0200;

      /// <summary>
      /// We need a name for the Message Queue
      /// </summary>
      private const string NOTIFICATIONS_MSG_Q_NAME = "PowerNotifications";

      #endregion

      //-----------------------------------------------------------------------

      #region Private fields

      /// <summary>
      /// Event to wake up the worker thread so that it can close
      /// </summary>
      private AutoResetEvent mPowerThreadAbort;

      /// <summary>
      /// Flag requesting worker thread closure
      /// </summary>
      private bool mAbortPowerThread = false;

      /// <summary>
      /// Flag to indicate that the worker thread is running
      /// </summary>
      private bool mPowerThreadRunning = false;

      /// <summary>
      /// Thread interface queue
      /// </summary>
      private Queue mPowerQueue;

      /// <summary>
      /// Handle to the message queue
      /// </summary>
      private IntPtr mMessageQue = IntPtr.Zero;

      /// <summary>
      /// Handle returned from Request_Power_Notifications
      /// </summary>
      private IntPtr mRequestHandle = IntPtr.Zero;

      /// <summary>
      /// Boolean used to indicate if the object has been disposed
      /// </summary>
      private bool mDisposed = false;

      /// <summary>
      /// Occurs when there is some PowerNotify information available.
      /// </summary>
      public event EventHandler PowerNotifyEvent;

      #endregion

      //-----------------------------------------------------------------------

      #region enumerations

      /// <summary>
      /// Responses from <see cref="WaitForMultipleObjects"/> function.
      /// </summary>
      private enum Wait : uint
      {
         /// <summary>
         /// The state of the specified object is signaled.
         /// </summary>
         Object = 0x00000000,
         /// <summary>
         /// Wait abandoned.
         /// </summary>
         Abandoned = 0x00000080,
         /// <summary>
         /// Wait failed.
         /// </summary>
         Failed = 0xffffffff,
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Methods

      /// <summary>
      /// Ensures that resources are freed when the garbage collector reclaims the object.
      /// </summary>
      ~PowerManager()
      {
         Dispose();
      }


      /// <summary>
      /// Releases the resources used by the object.
      /// </summary>
      public void Dispose()
      {
         if ( !mDisposed )
         {
            // Try disabling notifications and ending the thread
            Disable_Notifications();
            mDisposed = true;

            // SupressFinalize to take this object off the finalization queue
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize( this );
         }
      }


      /// <summary>
      /// Activates notification events. An application can now register to PowerNotify and be
      /// notified when a power notification is received.
      /// </summary>
      public void Enable_Notifications()
      {
         // Set the message queue options
         WinCE.MessageQueueOptions Options = new WinCE.MessageQueueOptions();

         // Size in bytes ( 5 * 4)
         Options.Size = (uint) Marshal.SizeOf( Options );

         // Allocate message buffers on demand and to free the message buffers after they are read
         Options.Flags = MSGQUEUE_NOPRECOMMIT;

         // Number of messages in the queue.
         Options.MaxMessages = MAX_MESSAGES;

         // Number of bytes for each message, do not set to zero.
         Options.MaxMessage = MAX_MESSAGED_BYTES;

         // Set to true to request read access to the queue.
         Options.ReadAccess = 1;

         // Create the queue and request power notifications on it
         mMessageQue = WinCE.WinCECreateMsgQueue( NOTIFICATIONS_MSG_Q_NAME, ref Options );

         mRequestHandle = WinCE.WinCERequestPowerNotifications( mMessageQue, POWER_NOTIFY_ALL );

         // If the above succeed
         if ( mMessageQue != IntPtr.Zero && mRequestHandle != IntPtr.Zero )
         {
            mPowerQueue = new Queue();

            // Create an event so that we can kill the thread when we want
            mPowerThreadAbort = new AutoResetEvent( false );

            // Create the power notifications thread
            new Thread( new ThreadStart( Power_Notify_Thread ) ).Start();
         }
      }


      /// <summary>
      /// Disables power notification events.
      /// </summary>
      public void Disable_Notifications()
      {
         // If we are already closed just exit
         if ( !mPowerThreadRunning )
            return;

         // Stop receiving power notifications
         if ( mRequestHandle != IntPtr.Zero )
            WinCE.WinCEStopPowerNotifications( mRequestHandle );

         // Attempt to end the PowerNotifyThread
         mAbortPowerThread = true;
         mPowerThreadAbort.Set();

         // Wait for the thread to stop
         int count = 0;
         while ( mPowerThreadRunning )
         {
            Thread.Sleep( 100 );

            // If it did not stop it time record this and give up
            if ( count++ > 50 )
               break;
         }
      }


      /// <summary>
      /// Obtain the next PowerInfo structure
      /// </summary>
      public WinCE.PowerInfo Get_Next_Power_Info()
      {
         // Get the next item from the queue in a thread safe manner
         lock ( mPowerQueue.SyncRoot )
            return (WinCE.PowerInfo) mPowerQueue.Dequeue();
      }


      /// <summary>
      /// Worker thread that creates and reads a message queue for power notifications
      /// </summary>
      private void Power_Notify_Thread()
      {
         mPowerThreadRunning = true;

         // Keep going util we are asked to quit
         while ( !mAbortPowerThread )
         {
            IntPtr[] Handles = new IntPtr[ 2 ];

            Handles[ 0 ] = mMessageQue;
            Handles[ 1 ] = mPowerThreadAbort.Handle;

            // Wait on two handles because the message queue will never
            // return from a read unless messages are posted.
            Wait res = (Wait) WinCE.WinCEWaitForMultipleObjects(
            (uint) Handles.Length,
            Handles,
            false,
            INFINITE );

            // Exit the loop if an abort was requested
            if ( mAbortPowerThread )
               break;

            // Else
            switch ( res )
            {
               // This must be an error - Exit loop and thread
               case Wait.Abandoned:
                  mAbortPowerThread = true;
                  break;

               // Timeout - Continue after a brief sleep
               case Wait.Failed:
                  Thread.Sleep( 500 );
                  break;

               // Read the message from the queue
               case Wait.Object:
                  {
                     // Create a new structure to read into
                     WinCE.PowerInfo Power = new WinCE.PowerInfo();

                     uint PowerSize = (uint) Marshal.SizeOf( Power );
                     uint BytesRead = 0;
                     uint Flags = 0;

                     // Read the message
                     if ( WinCE.WinCEReadMsgQueue(
                           mMessageQue,
                           ref Power,
                           PowerSize,
                           ref BytesRead,
                           0,
                           ref Flags ) )
                     {
                        // Set value to zero if percentage is not known
                        if ( ( Power.BatteryLifePercent < 0 ) || ( Power.BatteryLifePercent > 100 ) )
                           Power.BatteryLifePercent = 0;

                        if ( ( Power.BackupBatteryLifePercent < 0 ) || ( Power.BackupBatteryLifePercent > 100 ) )
                           Power.BackupBatteryLifePercent = 0;

                        //if ( Power.Flags == WinCE.SystemPowerStates.Suspend )
                        //{
                        // Add the power structure to the queue so that the
                        // UI thread can get it
                        lock ( mPowerQueue.SyncRoot )
                           mPowerQueue.Enqueue( Power );

                        // Fire an event to notify the UI
                        if ( PowerNotifyEvent != null )
                           PowerNotifyEvent( this, null );
                        //}
                     }

                     break;
                  }
            }
         }

         // Close the message queue
         if ( mMessageQue != IntPtr.Zero )
            WinCE.WinCECloseMsgQueue( mMessageQue );

         mPowerThreadRunning = false;
      }

      #endregion

      //-----------------------------------------------------------------------
   }
}

//-----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 26th October 2011
//  Add to project
//-----------------------------------------------------------------------------