﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public interface IMediaReg
   {
      /// <summary>
      /// Gets or sets the camera type.  See Camera.CameraTypes
      /// </summary>
      int CameraType { get; set; }

      /// <summary>
      /// Gets or sets the camera resolution ID
      /// </summary>
      int CameraResolution { get; set; }

      /// <summary>
      /// Gets or sets the saved media location
      /// </summary>
      string SavedMedia { get; set; }

      /// <summary>
      /// Gets or sets the Imager type.  See Camera.ImagerTypes
      /// </summary>
      int ImagerType { get; set; }
      
      /// <summary>
      /// Gets or sets the flash type.  See Camera.FlashTypes
      /// </summary>
      int FlashType { get; set; }
   }
}
