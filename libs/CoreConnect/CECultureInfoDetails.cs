﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This Singleton class provides a series of public methods that 
   /// return the formatting details of the current local culture. 
   /// </summary>
   public class CECultureInfoDetails : ICultureInfoDetails
   {
      //------------------------------------------------------------------------

      /// <summary>
      ///  Internal reference to self
      /// </summary>
      private volatile static CECultureInfoDetails mUniqueInstance = null;

      /// <summary>
      /// Thread protection
      /// </summary>
      private static object syncRoot = new Object();

      /// <summary>
      /// Culture Info object
      /// </summary>
      public static CultureInfo mCultureInfo = null;

      //------------------------------------------------------------------------

      /// <summary>
      /// Private Constructor
      /// </summary>
      private CECultureInfoDetails()
      {
         mCultureInfo = new CultureInfo( CultureInfo.CurrentCulture.LCID );
      }/* end constructor */


      /// <summary>
      /// Returns a thread safe instance of the CultureInfoDetails Class. 
      /// </summary>
      /// <returns>Instantaited CultureInfoDetails object</returns>
      public static CECultureInfoDetails GetCultureInfo()
      {
         //
         // if a thread safe instance of this object does not
         // already exist, then creat one
         //
         if ( mUniqueInstance == null )
         {
            lock ( syncRoot )
            {
               if ( mUniqueInstance == null )
                  mUniqueInstance = new CECultureInfoDetails();
            }
         }

         // return reference to this object
         return mUniqueInstance;

      }/* end method */

      //------------------------------------------------------------------------

      #region Static Methods

      /// <summary>
      /// Gets the ISO 639-1 Culture Code
      /// </summary>
      /// <returns></returns>
      public static string GetLocal6391CultureName()
      {
         CultureInfo ci = CultureInfo.CurrentUICulture;
         return ci.TwoLetterISOLanguageName;
      }/* end method */


      /// <summary>
      /// Gets the ISO 639-2 Culture Code
      /// </summary>
      /// <returns></returns>
      public static string GetLocal6392CultureName()
      {
         CultureInfo ci = CultureInfo.CurrentUICulture;
         return ci.ThreeLetterISOLanguageName;
      }/* end method */


      /// <summary>
      /// Gets the Culture name
      /// </summary>
      /// <returns></returns>
      public static string GetLocalCultureName()
      {
         CultureInfo ci = CultureInfo.CurrentUICulture;
         return ci.EnglishName;
      }/* end method */


      /// <summary>
      /// Gets the Culture ID
      /// </summary>
      /// <returns></returns>
      public static int GetLocalCultureID()
      {
         CultureInfo ci = CultureInfo.CurrentUICulture;
         return ci.LCID;
      }/* end method */

      #endregion

      //------------------------------------------------------------------------

      #region public Methods

      /// <summary>
      /// Get the Decimal seperator
      /// </summary>
      /// <returns>decimal seperator character</returns>
      public char DecimalSeparator()
      {
         CultureInfo ci = CultureInfo.CurrentUICulture;
         NumberFormatInfo nfi = (NumberFormatInfo) mCultureInfo.NumberFormat.Clone();
         return ( nfi.NumberDecimalSeparator.ToCharArray() )[ 0 ];
      }/* end method */


      /// <summary>
      /// Get the Group seperator
      /// </summary>
      /// <returns>group seperator character</returns>
      public char GroupSeparator()
      {
         NumberFormatInfo nfi = (NumberFormatInfo) mCultureInfo.NumberFormat.Clone();
         return ( nfi.NumberGroupSeparator.ToCharArray() )[ 0 ];
      }/* end method */


      /// <summary>
      /// Get Negative Sign
      /// </summary>
      /// <returns>negative sign character</returns>
      public char NegativeSign()
      {
         NumberFormatInfo nfi = (NumberFormatInfo) mCultureInfo.NumberFormat.Clone();
         return ( nfi.NegativeSign.ToCharArray() )[ 0 ];
      }/* end method */


      /// <summary>
      /// Get the number of Decimal digits
      /// </summary>
      /// <returns>number of decimal digits set by OS</returns>
      public int NumberDecimalDigits()
      {
         NumberFormatInfo nfi = (NumberFormatInfo) mCultureInfo.NumberFormat.Clone();
         return nfi.NumberDecimalDigits;
      }/* end method */


      /// <summary>
      /// Gets the number of digits to the left of the decimal
      /// </summary>
      /// <returns>array of number group sizes</returns>
      public int[] NumberGroupSizes()
      {
         NumberFormatInfo nfi = (NumberFormatInfo) mCultureInfo.NumberFormat.Clone();
         return nfi.NumberGroupSizes;
      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th November 2010
//  Add to project
//-------------------------------------------------------------------------------