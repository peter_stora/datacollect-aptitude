﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Threading;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Class used to interact with the device's LEDs
   /// </summary>
   public static class LED
   {
      //-----------------------------------------------------------------------

      #region Class Members

      /// <summary>
      /// Intermec CN3e LED colors
      /// </summary>
      public enum CN3eLEDs
      {
         Green = 3,
         Blue = 6
      }

      /// <summary>
      /// Flags for turning the LEDs on or off
      /// </summary>
      private enum LEDState
      {
         Off = 0,
         On = 1
      }

      /// <summary>
      /// Default LED blink duration (in milliseconds)
      /// </summary>
      private static readonly int DEFAULT_BLINK_DURATION = 200;

      /// <summary>
      /// Current LED blink duration
      /// </summary>
      private static int sBlinkDuration = DEFAULT_BLINK_DURATION;

      /// <summary>
      /// The LED count
      /// </summary>
      private static int sLEDCount;

      /// <summary>
      /// Resuse of LED settings structure (really a class...)
      /// </summary>
      private static WinCE.NLED_SETTINGS_INFO sSettings = new WinCE.NLED_SETTINGS_INFO( );

      #endregion

      //-----------------------------------------------------------------------

      #region Properties

      /// <summary>
      /// Gets whether LEDs are supported on this device
      /// </summary>
      public static bool Enabled
      {
         get
         {
            return sLEDCount != 0;
         }
      }

      /// <summary>
      /// Gets the amount of LEDs on the device
      /// </summary>
      public static int Count
      {
         get
         {
            return sLEDCount;
         }
      }

      /// <summary>
      /// LED Blink Duration
      /// </summary>
      public static int BlinkDuration
      {
         get
         {
            return sBlinkDuration;
         }
         set
         {
            sBlinkDuration = value;
         }
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Constructor

      /// <summary>
      /// Static constructor, called on first use of class
      /// </summary>
      static LED()
      {
         // see if we have LEDs to use
         sLEDCount = Get_LED_Count( );
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// Blink a CN3e LED Synchronously
      /// </summary>
      /// <param name="iColor">The color to blink</param>
      public static void Blink_CN3e_LED_Sync( CN3eLEDs iColor )
      {
         Blink_CN3e_LED( iColor );

      }/* end method */


      /// <summary>
      /// Blink a CN3e LED asynchronously
      /// </summary>
      /// <param name="iColor">The color to blink</param>
      public static void Blink_CN3e_LED_Async( CN3eLEDs iColor )
      {
         if ( Enabled )
         {
            ThreadPool.QueueUserWorkItem( new WaitCallback( Blink_CN3e_LED ), iColor );
         }

      }/* end method */

      #endregion

      //-----------------------------------------------------------------------

      #region Private Methods

      /// <summary>
      /// Gets the amount of LEDs on the device
      /// </summary>
      /// <returns>The count of LEDs</returns>
      private static int Get_LED_Count()
      {
         int ledCount = 0;

         try
         {
            WinCE.NLED_COUNT_INFO cInfo = new WinCE.NLED_COUNT_INFO( );

            if ( WinCE.NLedGetDeviceInfo( WinCE.LED_INFO_TYPE.NLED_COUNT_INFO_ID, cInfo ) )
            {
               ledCount = (int) cInfo.cLeds;
            }
         }
         catch ( Exception )
         {
         }

         return ledCount;

      }/* end method */


      /// <summary>
      /// Blink a CN3e's LED
      /// </summary>
      /// <param name="iColor">The color to blink</param>
      private static void Blink_CN3e_LED( object state )
      {
         try
         {
            CN3eLEDs color = (CN3eLEDs) state;

            Set_CN3e_LED( color, LEDState.On );
            Thread.Sleep( sBlinkDuration );
            Set_CN3e_LED( color, LEDState.Off );
         }
         catch ( Exception )
         {
         }

      }/* end method */


      /// <summary>
      /// Set the state of a CN3e's LED
      /// </summary>
      /// <param name="iColor">The LED color</param>
      /// <param name="iState">The LED state</param>
      private static void Set_CN3e_LED( CN3eLEDs iColor, LEDState iState )
      {
         sSettings.LedNum = (uint) iColor;
         sSettings.OffOnBlink = (uint) iState;

         WinCE.NLedSetDevice( WinCE.LED_INFO_TYPE.NLED_SETTINGS_INFO_ID, sSettings );

      }/* end method */

      #endregion

      //-----------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico September 1st 2010
//  Add to project
//----------------------------------------------------------------------------