﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This interface manages the reading of the "Module" settings
   /// in the Registry (under Key Modules)
   /// </summary>
   public interface IModuleRegistryKey
   {
      /// <summary>
      /// The SubKey of the Module
      /// </summary>
      string ModuleSubKey { get; }

      /// <summary>
      /// The name of the Module
      /// </summary>
      string Name { get; }

      /// <summary>
      /// The name of the actual assembly for the Module
      /// </summary>
      string AssemblyName { get; }

      /// <summary>
      /// The launch arguments that might be needed
      /// </summary>
      int LaunchArgs { get; }

      /// <summary>
      /// The license for the Module
      /// </summary>
      string License { get; }

   } /* end interface */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico 4th December 2009
//  Add to project
//----------------------------------------------------------------------------
