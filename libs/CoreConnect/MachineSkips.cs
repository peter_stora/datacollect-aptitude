﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
    /// <summary>
    /// Simple base class for Machine SKIPS
    /// </summary>
    public class BaseMachineSkips : RegistryBase
    {
        protected const string VALUE_SKIP_MCD = "SkipMCD";
        protected const string VALUE_SKIP_PROCESS = "SkipProcess";
        protected const string VALUE_SKIP_INSPECTION = "SkipInspection";
        protected const string VALUE_SKIP_ENABLED = "SkipEnabled";
        protected string mBranch = KEY_ROOT_SKF + KEY_APP_NAME + "\\" + KEY_OPERATOR_SETTINGS;

    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 21st June 2009
//  Add to project
//----------------------------------------------------------------------------