﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public interface IDeviceTimeManager
   {
      DateTime GetDeviceLocalTime();
      bool SetDeviceLocalTime( DateTime iNewTime );

      DateTime GetDeviceSystemTime();
      bool SetDeviceSystemTime( DateTime iNewTime );

      IDeviceTimeZoneInformation GetTimeZoneInformation();
      bool SetTimeZoneInformation( IDeviceTimeZoneInformation iDevZoneInfo );
      bool IsDaylightSavingTime( IDeviceTimeZoneInformation iZoneInfo, DateTime iRef );

   }/* end interface */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th November 2009
//  Add to project
//----------------------------------------------------------------------------