﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Text;
using System.Threading;
using System.Reflection;
using System.IO;
using System.Collections.Generic;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Class used to get device specific information
   /// </summary>
   public static class DeviceInfo
   {
      //-----------------------------------------------------------------------

      #region Enums

      /// <summary>
      /// Device type
      /// </summary>
      public enum DeviceType
      {
         Professional,
         SmartPhone,
         Unknown
      }

      /// <summary>
      /// AC Line status
      /// </summary>
      public enum ACPower
      {
         On,
         Off,
         BackupBattery,
         Unknown
      }

      /// <summary>
      /// Error codes for consumers of this class
      /// </summary>
      public enum DeviceInfoErrorCode
      {
         GetPlatformType,
         GetPowerState
      }

      /// <summary>
      /// Native File System folder
      /// </summary>
      public enum Folders
      {
         Programs = 0x02,        // \Windows\Start Menu\Programs
         Personal = 0x05,        // \My Documents
         Startup = 0x07,         // \Windows\StartUp
         Startmenu = 0x0B,       // \Windows\Start Menu
         MyMusic = 0x0D,         // \My Documents\My Music
         MyVideos = 0x0E,        // \My Documents\My Videos           
         Fonts = 0x14,           // \Windows\Fonts
         Favorites = 0x16,       // \Windows\Favorites
         Windows = 0x24,         // \Windows
         Program_Files = 0x26,   // \Program Files
         MyPictures = 0x27       // \My Documents\My Pictures
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Class Members

      /// <summary>
      /// The power status
      /// </summary>
      private static WinCE.SYSTEM_POWER_STATUS_EX sPowerStatus;

      /// <summary>
      /// Value used to determine if we are running on a Pocket PC or a 
      /// Smart Phone
      /// </summary>
      private const string POCKET_PC = "PocketPC";

      /// <summary>
      /// More friendly power info structure
      /// </summary>
      public struct PowerInfo
      {
         public ACPower ChargeState;
         public uint BatteryPercent;
      };

      /// <summary>
      /// Update timer for Power Info status
      /// </summary>
      private static Timer sUpdateTimer;

      /// <summary>
      /// Default Power Info status update interval
      /// </summary>
      public const int DEFAULT_UPDATE_INTERVAL = 5000;

      /// <summary>
      /// The update interval for the power status info
      /// </summary>
      private static int sUpdateInterval;

      /// <summary>
      /// Whether or not the update timer is enabled
      /// </summary>
      private static bool sTimerEnabled;

      /// <summary>
      /// The power info structure
      /// </summary>
      private static PowerInfo sPowerInfo;

      /// <summary>
      /// The platform type string builder size
      /// </summary>
      private const int PLATFORM_TYPE_BUFFER_SIZE = 50;

      /// <summary>
      /// Windows Mobile 5 major version number 
      /// </summary>
      private const int WM5_VER_MAJOR = 5;

      /// <summary>
      /// Windows Mobile 6.5.3 major version number 
      /// </summary>
      private const int WM653_VER_MAJOR = 5;

      /// <summary>
      /// Windows Mobile 6.5.3 minor version number 
      /// </summary>
      private const int WM653_VER_MINOR = 2;

      /// <summary>
      /// Windows Mobile 6.5.3 revision number 
      /// </summary>
      private const int WM653_VER_REVISION = 23090;

      /// <summary>
      /// Windows Mobile 6.5.3 major build number 
      /// </summary>
      private const int WM653_VER_BUILD = 23090;

      /// <summary>
      /// Archive file
      /// </summary>
      private const string ARCHIVE_FILE = "ARCHIVE.XML";

      /// <summary>
      /// Registry connect object
      /// </summary>
      private static RegistryConnect sRegistryConnect;

      /// <summary>
      /// Flags for finding flash drives
      /// </summary>
      private const int INVALID_HANDLE_VALUE = -1;
      private const int WIN32_TRUE = 1;
      #endregion

      //-----------------------------------------------------------------------

      #region Properties

      /// <summary>
      /// Gets if the device is WM5 or newer
      /// </summary>
      public static bool IsWM5OrNewer
      {
         get
         {
            return ( Environment.OSVersion.Version.Major >= WM5_VER_MAJOR );
         }
      }

      /// <summary>
      /// Is this device running Windows Mobile 6.5.3 or newer?
      /// </summary>
      public static bool IsWindowsEmbeddedOrNewer
      {
         get
         {
            Version ver = new Version( WM653_VER_MAJOR,
               WM653_VER_MINOR, WM653_VER_BUILD );
            return ( Environment.OSVersion.Version >= ver );
         }
      }

      /// <summary>
      /// Gets or sets the update timer enabled state
      /// </summary>
      public static bool UpdateTimerEnabled
      {
         get
         {
            return sTimerEnabled;
         }
         set
         {
            Enable_Update_Timer( value );
         }
      }

      /// <summary>
      /// Gets whether or not the device supports Barcode
      /// </summary>
      public static bool SupportsBarcode
      {
         get
         {
            return Supports_Barcode();
         }
      }

      /// <summary>
      /// Gets whether or not the device is using Serial or RNDIS
      /// if comms is via ActiveSync (LEGACY ONLY - not for Win8)
      /// </summary>
      public static bool UsingSerialComms
      {
         get
         {
            return Using_Serial_Comms();
         }
      }

      /// <summary>
      /// Gets whether or not the device supports MQC
      /// </summary>
      public static bool SupportsMQC
      {
         get
         {
            return Supports_MQC();
         }
      }

      /// <summary>
      /// Gets whether or not the device supports RFID
      /// </summary>
      public static bool SupportsRFID
      {
         get
         {
            return Supports_RIFD();
         }
      }

      /// <summary>
      /// Gets whether or not the device supports a Camera
      /// </summary>
      public static bool SupportsCamera
      {
         get
         {
            return Supports_Camera( );
         }
      }

      /// <summary>
      /// Gets the name of the backup and restore XML archive file 
      /// </summary>
      public static string ArchiveFileName
      {
         get
         {
            return ARCHIVE_FILE;
         }
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Constructor

      /// <summary>
      /// Static constructor
      /// </summary>
      static DeviceInfo()
      {
         sRegistryConnect = new RegistryConnect(  );
         sPowerInfo = new PowerInfo();
         sPowerStatus = new WinCE.SYSTEM_POWER_STATUS_EX();

         // Get the timer ready to go
         sUpdateInterval = DEFAULT_UPDATE_INTERVAL;

         sUpdateTimer = new Timer( new TimerCallback( On_Update_Timer_Tick ),
                                                      null,
                                                      Timeout.Infinite,
                                                      Timeout.Infinite );
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// Determines if we are running on a Professional or Standard device.
      /// </summary>
      /// <returns>DeviceType enum</returns>
      public static DeviceType Get_Device_Type()
      {
         var platformType = new StringBuilder( PLATFORM_TYPE_BUFFER_SIZE );

         try
         {
            if ( WinCE.SystemParametersInfo4Strings( WinCE.SPI_GETPLATFORMTYPE, (uint) platformType.Capacity, platformType, 0 ) != 0 )
            {
               return platformType.ToString() == POCKET_PC ? DeviceType.Professional : DeviceType.SmartPhone;
            }
         }
         catch ( Exception )
         {
            On_Device_Info_Error( DeviceInfoErrorCode.GetPlatformType );
         }

         return DeviceType.Unknown;
      }


      /// <summary>
      /// Gets the power state of the device
      /// </summary>
      /// <param name="iUpdate">True if you want to get current status, false if you can
      /// afford to have data that can be several seconds old</param>
      /// <returns></returns>
      public static PowerInfo Get_Power_State( bool iUpdate )
      {
         // initialize the structure
         sPowerStatus.ACLineStatus = 0;
         sPowerStatus.BackupBatteryFlag = 0;
         sPowerStatus.BackupBatteryFullLifeTime = 0;
         sPowerStatus.BackupBatteryLifePercent = 0;
         sPowerStatus.BackupBatteryLifeTime = 0;
         sPowerStatus.BatteryFlag = 0;
         sPowerStatus.BatteryFullLifeTime = 0;
         sPowerStatus.BatteryLifePercent = 0;
         sPowerStatus.BatteryLifeTime = 0;

         // initialize the structure
         sPowerInfo.BatteryPercent = 0;
         sPowerInfo.ChargeState = ACPower.Unknown;

         try
         {
            if ( WinCE.GetSystemPowerStatusEx( sPowerStatus, iUpdate ) )
            {
               switch ( sPowerStatus.ACLineStatus )
               {
                  case WinCE.AC_LINE_OFFLINE:
                     sPowerInfo.ChargeState = ACPower.Off;
                     break;
                  case WinCE.AC_LINE_ONLINE:
                     sPowerInfo.ChargeState = ACPower.On;
                     break;
                  case WinCE.AC_LINE_BACKUP_POWER:
                     sPowerInfo.ChargeState = ACPower.BackupBattery;
                     break;
                  case WinCE.AC_LINE_UNKNOWN:
                     sPowerInfo.ChargeState = ACPower.Unknown;
                     break;
                  default:
                     sPowerInfo.ChargeState = ACPower.Unknown;
                     break;
               }

               sPowerInfo.BatteryPercent = sPowerStatus.BatteryLifePercent;
            }
         }
         catch ( Exception )
         {
            On_Device_Info_Error( DeviceInfoErrorCode.GetPowerState );
         }

         return sPowerInfo;
      }


      /// <summary>
      /// Gets the path to a system folder.  This is needed when using localized
      /// operating systems like Chinese.
      /// </summary>
      /// <param name="iFolder">The folder to get the path to</param>
      /// <returns>A path to the folder, if found</returns>
      public static string Get_System_Folder_Path( Folders iFolder )
      {
         var path = new StringBuilder( 255 );

         try
         {
            WinCE.SHGetSpecialFolderPath( IntPtr.Zero, path, (int) iFolder, 0 );
         }
         catch
         {
            path.Length = 0;
         }

         return path.ToString();
      }


      /// <summary>
      /// Gets the application's diectory
      /// </summary>
      /// <returns>The applications direcotory</returns>
      public static string Get_Application_Directory()
      {
         // get the current directory
         return Path.GetDirectoryName( Assembly.GetExecutingAssembly().GetName().CodeBase );
      }


      /// <summary>
      /// Gets the firmware version
      /// </summary>
      /// <returns>The firmware version</returns>
      public static string Get_Firmware_Version()
      {
         return Assembly.GetExecutingAssembly().GetName().Version.ToString();
      }


      /// <summary>
      /// Returns a list of all Flash (or SD) drive paths along
      /// with the resource details associated with each path 
      /// </summary>
      /// <param name="oDrives">Returned list of FlashDrive structures</param>
      /// <returns>True on success, else false</returns>
      public static Boolean Get_All_Flash_Paths( out List<WinCE.FlashDrive> oDrives )
      {
         // set the default result
         Boolean result = true;

         // create an empty list of Flash drive objects
         oDrives = new List<WinCE.FlashDrive>();

         // create the return data structure
         WinCE.LPWIN32_FIND_DATA findData;

         try
         {
            // get details on the first Flash card and reference its handle
            IntPtr lHandle = WinCE.FindFirstFlashCard( out findData );

            // if the handle is valid then add this drive
            if( lHandle.ToInt32( ) != INVALID_HANDLE_VALUE )
            {
               Set_Properties( oDrives, ref findData );

               // now look for other drives on the handheld
               while( Find_Next_Flash_Card( lHandle, ref findData ) )
               {
                  Set_Properties( oDrives, ref findData );
               }

               // close the open handle
               WinCE.FindClose( lHandle );
            }
            else
            {
               result = false;
            }
         }
         catch
         {
            result = false;
         }

         // return the result
         return result;
      }


      /// <summary>
      /// The FindNextFlashCard function finds the next mountable file system, 
      /// such as a flash card
      /// </summary>
      /// <param name="iPreviousFlashCardHandle">[in] Handle to the first flash card as returned by
      /// a previous call to the FindFirstFlashCard function</param>
      /// <param name="iFindDataPtr">[out] Long pointer to the WIN32_FIND_DATA structure
      /// that receives information about the found flash card</param>
      /// <returns>TRUE indicates success. FALSE indicates failure.</returns>
      private static bool Find_Next_Flash_Card( IntPtr iPreviousFlashCardHandle, ref WinCE.LPWIN32_FIND_DATA iFindDataPtr )
      {
         return ( WinCE.FindNextFlashCard( iPreviousFlashCardHandle, ref iFindDataPtr ) == WIN32_TRUE );

      }
      #endregion

      //-----------------------------------------------------------------------

      #region Private Methods

      /// <summary>
      /// Enables or disables the update timer
      /// </summary>
      /// <param name="iEnable"></param>
      private static void Enable_Update_Timer( bool iEnable )
      {
         if ( iEnable )
         {
            sUpdateTimer.Change( 0, sUpdateInterval );
            sTimerEnabled = true;
         }
         else
         {
            sUpdateTimer.Change( Timeout.Infinite, Timeout.Infinite );
            sTimerEnabled = false;
         }
      }


      /// <summary>
      /// Status timer click event handler
      /// </summary>
      /// <param name="iStateInfo"></param>
      private static void On_Update_Timer_Tick( object iStateInfo )
      {
         On_Power_Info_Update( Get_Power_State( true ) );
      }


      /// <summary>
      /// Determines if the devices supports Barcode
      /// </summary>
      /// <returns></returns>
      private static bool Supports_Barcode()
      {
         return ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.Intermec ) ||
                ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.Symbol ) ||
                ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.Unitech ) ||
                ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.EcomSE955 )||
                ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.EcomEA15 )||
                ( sRegistryConnect.Barcode == ScanningCtl.BarcodeEngine.EcomEX25 );
      }


      /// <summary>
      /// Determines if the devices is using Serial or RNDIS comms
      /// </summary>
      /// <returns>true if using Serial, else false</returns>
      private static bool Using_Serial_Comms()
      {
         ActiveSyncDriverType driver = ClientDriver.Get_Client_Driver_Type();

         if ( driver == ActiveSyncDriverType.serial )
         {
            return true;
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Determines if the devices supports MQC
      /// </summary>
      /// <returns></returns>
      private static bool Supports_MQC()
      {
         bool result = false;

         // using WMCD?
         if ( sRegistryConnect.ExtVibSensor == ExternalSensor.Vibration.WMCD )
         {
            // do we have a BT stack?
            if( sRegistryConnect.BTDeviceStack != BlueToothCtl.DeviceStack.Unknown )
            {
               result = true;
            }
         }
         else if( sRegistryConnect.ExtVibSensor == ExternalSensor.Vibration.MCD )
         {
            result = true;
         }

         return result;
      }


      /// <summary>
      /// Determines if the devices supports RFID
      /// </summary>
      /// <returns></returns>
      private static bool Supports_RIFD()
      {
         bool result = false;

         var engine = sRegistryConnect.RFID;

         if( engine == ScanningCtl.RFIDEngine.IntermecIP30 ||
             engine == ScanningCtl.RFIDEngine.IntermecIM11 ||
             engine == ScanningCtl.RFIDEngine.Unitech ||
             engine == ScanningCtl.RFIDEngine.EcomARE8 ||
             engine == ScanningCtl.RFIDEngine.EcomTLB30 ||
             engine == ScanningCtl.RFIDEngine.EcomUNI13 )
         {
            result = true;
         }

         return result;
      }


      /// <summary>
      /// Determines if the device supports the a camera
      /// </summary>
      /// <returns>True if camera supported</returns>
      private static bool Supports_Camera()
      {
         var result = false;

         var type = sRegistryConnect.Media.CameraType;

         // See SKF.RS.MicrologInspector.Camera.CameraTypes.  CN70 Color camera only
         if( type == 2 )
            result = true;

         return result;
      }


      /// <summary>
      /// Populate a new Flash Drive properties structure and add
      /// it to the existing list of structures
      /// </summary>
      /// <param name="oDrives">Available list of structures</param>
      /// <param name="findData">Flash drive data structure</param>
      private static void Set_Properties( List<WinCE.FlashDrive> oDrives,
         ref WinCE.LPWIN32_FIND_DATA findData )
      {
         WinCE.FlashDrive drive = new WinCE.FlashDrive();

         drive.Path = findData.cFileName;
         drive.FileSizeHigh = findData.nFileSizeHigh;
         drive.FileSizeLow = findData.nFileSizeLow;

         WinCE.GetDiskFreeSpaceEx( findData.cFileName,
            out drive.ByteAvailableToCaller,
            out drive.TotalNumOfBytes,
            out drive.TotalNumOfFreeBytes );

         oDrives.Add( drive );
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Events

      /// <summary>
      /// Update event used to get power info status updates.  Not thread
      /// safe.
      /// </summary>
      /// <param name="iError">PowerInfo structure</param>
      public delegate void PowerInfoUpdate( PowerInfo iPowerInfo );
      public static event PowerInfoUpdate PowerInfoUpdateEvent;

      private static void On_Power_Info_Update( PowerInfo iPowerInfo )
      {
         if ( PowerInfoUpdateEvent != null )
         {
            PowerInfoUpdateEvent( iPowerInfo );
         }
      }

      /// <summary>
      /// Event used to relay errors to consumers of this class
      /// </summary>
      /// <param name="iError">Error message</param>
      public delegate void DeviceInfoError( DeviceInfoErrorCode iErrorCode );
      public static event DeviceInfoError DeviceInfoErrorEvent;

      private static void On_Device_Info_Error( DeviceInfoErrorCode iErrorCode )
      {
         if ( DeviceInfoErrorEvent != null )
         {
            DeviceInfoErrorEvent( iErrorCode );
         }
      }

      #endregion

      //-----------------------------------------------------------------------

   } /* end class */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 18th October 2011
//  Extended system folders to include My Pictures, My Videos and My Music
//
//  Revision 0.0 TBottalico 18th December 2009
//  Add to project
//----------------------------------------------------------------------------