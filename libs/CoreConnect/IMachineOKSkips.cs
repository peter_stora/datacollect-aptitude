﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
    /// <summary>
    /// This interface manages the reading and writing of the "Machine OK" skip
    /// collection settings to the Registry (under the Key OperatorSettings)
    /// </summary>
    public interface IMachineOKSkips
    {
        /// <summary>
        /// Get or Set whether an operator on will have the "Machine OK" 
        /// (skip option) button enabled
        /// </summary>
        bool SkipEnabled { get; }

        /// <summary>
        /// Get or Set whether an operator pressing the "Machine OK" button
        /// would result in all Inspection only Points being skipped
        /// </summary>
        bool SkipInspectionPoints { get; set; }

        /// <summary>
        /// Get or Set whether an operator pressing the "Machine OK" button
        /// would result in all MCD only Points being skipped
        /// </summary>
        bool SkipMCDPoints { get; set; }

        /// <summary>
        /// Get or Set whether an operator pressing the "Machine OK" button
        /// would result in all Process only Points being skipped
        /// </summary>
        bool SkipProcessPoints { get; set; }

    }/* end interface */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2009
//  Add to project
//----------------------------------------------------------------------------