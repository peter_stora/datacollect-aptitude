﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class contains arrays of system strings that are just not
   /// otherwise available via the WinCE API or P\Invoke calls
   /// </summary>
   internal static class SystemString
   {
      /// <summary>
      /// Array of descriptions for the Unicode string "Network Card" 
      /// which covers all supported languages - and one or two others.
      /// </summary>
      private static string[] mNetworkCard = new string[]{
         "NETWORK CARD",         /* English */
         "네트워크 카드",        /* Korean */
         "СЕТЕВАЯ КАРТА",        /* Russian */
         "网路卡",               /* Chinese (Simplified) for Network Card */
         "网卡",                 /* Chinese (Simplified) for NIC */
         "網路卡",               /* Chinese (Traditional) */
         "NETWERKKAART",         /* Afrikaans, Dutch */
         "SÍŤOVÁ KARTA",         /* Czech */
         "NETVÆRKSKORT",         /* Danish */
         "CARTE RÉSEAU",         /* French */
         "VERKKOKORTTI",         /* Finnish */
         "NETZWERKKARTE",        /* German */
         "HÁLÓZATI KÁRTYA",      /* Hungarian */
         "NETTVERKSKORT",        /* Norwegian */
         "KARTA SIECIOWA",       /* Polish */
         "PLACA DE REDE",        /* Portuguese */
         "TARJETA DE RED",       /* Spanish */
         "NÄTVERKSKORT"          /* Swedish */
      };


      /// <summary>
      /// Gets an assay of definitions for the unicode string "Network Card"
      /// Note: This system string was originally part of the InTheHand.Net 
      /// WindowsMobile.Status.SystemNotifications assembly, which was later
      /// added to Windows Mobile by Microsoft.
      /// </summary>
      public static string[] NetworkCard
      {
         get
         {
            return mNetworkCard;
         }
      }
   }
}

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 26th September 2011
//  Add to project
//-------------------------------------------------------------------------------
