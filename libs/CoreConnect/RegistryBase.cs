﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using SKF.RS.MicrologInspector.HardwareConnect.Properties;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Registry IO Base Class
   /// </summary>
   public class RegistryBase
   {
      // default Root fields
      protected const string KEY_ROOT_DI = @"SOFTWARE\DiagnosticInstruments\";
      protected const string KEY_ROOT_SKF = @"SOFTWARE\SKF\";
      protected const string KEY_ROOT_OPERATORS = @"SOFTWARE\SKF\MicrologInspector\";

      // default Key name fields
      protected const string KEY_APP_NAME = "MicrologInspector";
      protected const string KEY_SW_REV = "SW_Revision";
      protected const string KEY_MSG_PREFERENCES = "MessagingPrefernces";
      protected const string KEY_MACHINE_OK_SKIPS = "MachineOKSkips";
      protected const string KEY_MACHINE_NOP_SKIPS = "MachineNotOperatingSkips";
      protected const string KEY_OPERATOR_SETTINGS = "OperatorSettings";
      protected const string KEY_MODULES = "Modules";

      // required char count for the device UID (32 chars + 4 dashes)
      protected const int TARGET_UID_LENGTH = 36;

      // Link to Registry IO Interface
      protected IRegistryIO mRegistryIO;


      /// <summary>
      /// Enumeration of primary Registry Hives
      /// </summary>
      public enum RegistryBranch
      {
         MicrologInspector,
         SW_Revision
      }


      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public RegistryBase()
      {
         mRegistryIO = new RegistryIO();
      }/* end constructor */


      /// <summary>
      /// Obtain the UUID of the device using the external IOCTL_HAL_GET_DEVICEID.
      /// Note: This call should work for both long and short format Device IDs
      /// </summary>
      /// <returns>UUID of the current device - or error strings</returns>
      protected static string GetDeviceID()
      {
         // Initialize the output buffer to the size of a 
         // Win32 DEVICE_ID structure.
         byte[] outbuff = new byte[ 256 ];
         Int32 dwOutBytes;
         bool done = false;

         Int32 nBuffSize = outbuff.Length;

         //
         // Set DEVICEID.dwSize to size of buffer.  Some platforms look at
         // this field rather than the nOutBufSize param of KernelIoControl
         // when determining if the buffer is large enough.
         //
         BitConverter.GetBytes( nBuffSize ).CopyTo( outbuff, 0 );
         dwOutBytes = 0;

         // create a string builder object for the UUID elements
         StringBuilder sb = new StringBuilder();

         try
         {
            // Loop until the device ID is retrieved or an error occurs.
            while ( !done )
            {
               if ( WinCE.KernelIoControl( WinCE.IOCTL_HAL_GET_DEVICEID, IntPtr.Zero,
                   0, outbuff, nBuffSize, ref dwOutBytes ) )
               {
                  done = true;
               }
               else
               {
                  int error = Marshal.GetLastWin32Error();
                  switch ( error )
                  {
                     // On UUID not supported, return error
                     case WinCE.ERROR_NOT_SUPPORTED:
                        return Resources.ERR_NO_UUID_SUPPORT;

                     case WinCE.ERROR_INSUFFICIENT_BUFFER:

                        //
                        // The buffer is not big enough for the data.  The
                        // required size is in the first 4 bytes of the output
                        // buffer (DEVICE_ID.dwSize).
                        //
                        nBuffSize = BitConverter.ToInt32( outbuff, 0 );
                        outbuff = new byte[ nBuffSize ];
                        //
                        // Set DEVICEID.dwSize to size of buffer.  Some
                        // platforms look at this field rather than the
                        // nOutBufSize param of KernelIoControl when
                        // determining if the buffer is large enough.
                        //
                        BitConverter.GetBytes( nBuffSize ).CopyTo( outbuff, 0 );
                        break;

                     // On general error return fail string 
                     default:
                        return Resources.ERR_UUID_ERROR;
                  }
               }
            }

            // Copy the elements of the DEVICE_ID structure.
            Int32 dwPresetIDOffset = BitConverter.ToInt32( outbuff, 0x4 );
            Int32 dwPresetIDSize = BitConverter.ToInt32( outbuff, 0x8 );
            Int32 dwPlatformIDOffset = BitConverter.ToInt32( outbuff, 0xc );
            Int32 dwPlatformIDSize = BitConverter.ToInt32( outbuff, 0x10 );

            byte[] idBytes = new byte[ dwPresetIDSize + dwPlatformIDSize ];

            Array.Copy( outbuff, dwPresetIDOffset, idBytes, 0, dwPresetIDSize );
            Array.Copy( outbuff, dwPlatformIDOffset, idBytes, dwPresetIDSize, dwPlatformIDSize );

            string idString = string.Empty;

            for ( int i = 0; i < idBytes.Length; i++ )
            {
               if ( i == 4 || i == 6 || i == 8 || i == 10 )
               {
                  sb.Append( String.Format( "{0}-{1}",idString, idBytes[ i ].ToString( "X2" ) ) );
               }
               else
               {
                  sb.Append( String.Format( "{0}{1}", idString, idBytes[ i ].ToString( "X2" ) ) );
               }
            }
            //
            // For OTD #4627 (UID is not long enough)
            // We need a minimum length of UID, so ensure that's what we have!
            // Note: assuming that the read UID is unique (irrespective of its
            // length, then adding zeros is a valid fix.
            //
            if ( sb.Length < TARGET_UID_LENGTH )
            {
               int pad = TARGET_UID_LENGTH - sb.Length;
               for ( int i=0; i < pad; ++i )
               {
                  sb.Append( '0' );
               }
            }
         }
         catch
         {
            sb.Append( Resources.ERR_UUID_ERROR );
         }

         // return (hopefully) the device UUID
         return sb.ToString();

      }/* end method */


      /// <summary>
      /// Very simple password encryption
      /// </summary>
      /// <param name="iString">string to be encrypted</param>
      /// <returns>encrypted string</returns>
      protected string EncrptPassword( string iPassword )
      {
         AESEncryption encryp = new AESEncryption();
         return encryp.EncryptData( iPassword );

      }/* end method */


      /// <summary>
      /// Very simple password decryption
      /// </summary>
      /// <param name="iString">string to be decrypted</param>
      /// <returns>encrypted string</returns>
      protected string DecryptPassword( string iPassword )
      {
         AESEncryption encrypt = new AESEncryption();
         return encrypt.DecryptData( iPassword );

      }/* end method */

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th June 2009
//  Add to project
//----------------------------------------------------------------------------
