﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Ensure CultureInfo support is consistent across Win32 and WinCE
   /// </summary>
   public interface ICultureInfoDetails
   {
      /// <summary>
      /// Get the Decimal seperator
      /// </summary>
      /// <returns>decimal seperator character</returns>
      char DecimalSeparator();

      /// <summary>
      /// Get the Group seperator
      /// </summary>
      /// <returns>group seperator character</returns>
      char GroupSeparator();

      /// <summary>
      /// Get Negative Sign
      /// </summary>
      /// <returns>negative sign character</returns>
      char NegativeSign();

      /// <summary>
      /// Get the number of Decimal digits
      /// </summary>
      /// <returns>number of decimal digits set by OS</returns>
      int NumberDecimalDigits();

      /// <summary>
      /// Gets the number of digits to the left of the decimal
      /// </summary>
      /// <returns>array of number group sizes</returns>
      int[] NumberGroupSizes();

   }/* end interface */

}/* end namespace */

//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th November 2010
//  Add to project
//-------------------------------------------------------------------------------