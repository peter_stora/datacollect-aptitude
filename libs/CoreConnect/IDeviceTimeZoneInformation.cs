﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public interface IDeviceTimeZoneInformation
   {
      /// <summary>
      /// Gets the start of daylight saving changeover hour
      /// </summary>
      int DSTHour
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the start of daylight saving changeover Day
      /// </summary>
      int DSTDay
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the start of daylight saving changeover Month
      /// </summary>
      int DSTMonth
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the start of standard time changeover Hour
      /// </summary>
      int STHour
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the start of standard time changeover Day
      /// </summary>
      int STDay
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the start of standard time changeover Month
      /// </summary>
      int STMonth
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current Year
      /// </summary>
      int CurrentYear
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the overall time bias from UTC = 0
      /// </summary>
      int Bias
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the daylight saving bias
      /// </summary>
      int DaylightBias
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the daylight name (i.e. British Summer Time)
      /// </summary>
      string DaylightName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the standard time bias
      /// </summary>
      int StandardBias
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the standard time name (i.e. GMT)
      /// </summary>
      string StandardName
      {
         get;
         set;
      }

      /// <summary>
      /// Returns all object property fields as a TimeZoneInfo structure 
      /// </summary>
      /// <returns>Populated TimeZoneInfo Structure</returns>
      WinCE.TimeZoneInformation GetTimeZoneStructure();

      /// <summary>
      /// Populate all fields in this object using a TimeZoneInformation struct
      /// </summary>
      /// <param name="iZoneInfo">Populated TimeZoneInformation struct</param>
      void PopulateObject( WinCE.TimeZoneInformation iZoneInfo );

   }/* end interface */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th November 2009
//  Add to project
//----------------------------------------------------------------------------