﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class exposes all required Interop properties 
   /// and method declarations as statics
   /// </summary>
   public class WinCE
   {
      //-----------------------------------------------------------------------

      #region Constants
      
      /// <summary>
      /// Native method call success
      /// </summary>
      public static int S_OK = 0;

      /// <summary>
      /// Native method call returns FALSE
      /// </summary>
      public static int S_FALSE = 1;

      /// <summary>
      /// Native method call failed
      /// </summary>
      public static int E_FAIL = 4;

      public static Int32 METHOD_BUFFERED = 0;
      public static Int32 FILE_ANY_ACCESS = 0;
      public static Int32 FILE_DEVICE_HAL = 0x00000101;

      public const Int32 ERROR_NOT_SUPPORTED = 0x32;
      public const Int32 ERROR_INSUFFICIENT_BUFFER = 0x7A;

      public static Int32 IOCTL_HAL_GET_DEVICEID =
            ( ( FILE_DEVICE_HAL ) << 16 ) | ( ( FILE_ANY_ACCESS ) << 14 )
          | ( ( 21 ) << 2 ) | ( METHOD_BUFFERED );

      /// <summary>
      /// Used with CeRunAppAtEvent
      /// </summary>
      public const int NOTIFICATION_EVENT_WAKEUP = 11;
      public const int NOTIFICATION_EVENT_NONE = 0;

      /// <summary>
      /// Maximum character size of File Path
      /// </summary>
      public const int MAX_PATH = 260;

      #endregion

      //-----------------------------------------------------------------------

      #region Public enumerators

      public enum HKEY : uint
      {
         HKEY_CLASSES_ROOT = 0x80000000,
         HKEY_CURRENT_USER = 0x80000001,
         HKEY_LOCAL_MACHINE = 0x80000002,
         HKEY_USERS = 0x80000003,
         HKEY_PERFORMANCE_DATA = 0x80000004,
         HKEY_PERFORMANCE_TEXT = 0x80000050,
         HKEY_PERFORMANCE_NLSTEXT = 0x80000060,
         HKEY_CURRENT_CONFIG = 0x80000005,
      }

      public enum REGDISP
      {
         REG_CREATED_NEW_KEY = 0x1,
         REG_OPENED_EXISTING_KEY = 0x2,
      }

      public enum REGTYPE : int
      {
         REG_NONE = 0,
         REG_SZ = 1,
         REG_EXPAND_SZ = 2,
         REG_BINARY = 3,
         REG_DWORD = 4,
         REG_DWORD_LITTLE_ENDIAN = 4,
         REG_DWORD_BIG_ENDIAN = 5,
         REG_LINK = 6,
         REG_MULTI_SZ = 7,
         REG_RESOURCE_LIST = 8,
      }

      /// <summary>
      /// Specifies the current status of the Bluetooth radio modes
      /// </summary>
      public enum BTH_RADIO_MODE
      {
         /// <summary>
         /// Bluetooth radio power down 
         /// </summary>
         BTH_POWER_OFF = 0x00,
         /// <summary>
         /// Bluetooth device is connectable only 
         /// </summary>
         BTH_CONNECTABLE = 0x01,
         /// <summary>
         /// Bluetooth device is discoverable only 
         /// </summary>
         BTH_DISCOVERABLE = 0x02,
         /// <summary>
         /// Bluetooth device is discoverable and connectable (StoneStreet only)
         /// </summary>
         BTH_DISCOVERABLE_AND_CONNECTABLE = 0x03,
         /// <summary>
         /// This is reserved and currently not been used (StoneStreet only)
         /// </summary>
         BTH_ERROR = 0x04
      }

      /// <summary>
      /// Specifies the current status of the Bluetooth hardware.
      /// </summary>
      public enum BTH_PI_STATUS
      {
         HCI_HARDWARE_UNKNOWN = 0x00,
         HCI_HARDWARE_RUNNING = 0x01,
         HCI_HARDWARE_INITIALIZING = 0x02,
         HCI_HARDWARE_ERROR = 0x03,
         HCI_HARDWARE_SHUTDOWN = 0x04,
         HCI_HARDWARE_NOT_PRESENT = 0x05,
      }
      /// <summary>
      /// Defines the Power Status message type.
      /// </summary>
      [FlagsAttribute()]
      public enum MessageTypes : uint
      {
         /// <summary>
         /// System power state transition.
         /// </summary>
         Transition = 0x00000001,

         /// <summary>
         /// Resume from previous state.
         /// </summary>
         Resume = 0x00000002,

         /// <summary>
         /// Power supply switched to/from AC/DC.
         /// </summary>
         Change = 0x00000004,

         /// <summary>
         /// A member of the POWER_BROADCAST_POWER_INFO structure has changed.
         /// </summary>
         Status = 0x00000008
      }

      /// <summary>
      /// Defines the System power states
      /// </summary>
      public enum SystemPowerStates : uint
      {
         /// <summary>
         /// On state
         /// </summary>
         On = 0x00010000,

         /// <summary>
         /// No power, full off
         /// </summary>
         Off = 0x00020000,

         /// <summary>
         /// Critical off
         /// </summary>
         Critical = 0x00040000,

         /// <summary>
         /// Boot state
         /// </summary>
         Boot = 0x00080000,

         /// <summary>
         /// Suspend state
         /// </summary>
         Suspend = 0x00200000,

         /// <summary>
         /// Unattended state (contradicts MSDN doumentation)
         /// </summary>
         Unattended = 0x00400000,

         /// <summary>
         /// Reset state
         /// </summary>
         Reset = 0x00800000,

         /// <summary>
         /// Idle state (contradicts MSDN doumentation)
         /// </summary>
         Idle = 0x01000000
      }

      /// <summary>
      /// Defines the AC power status flags.
      /// </summary>
      public enum ACLineStatus : byte
      {
         /// <summary>
         /// AC power is offline.
         /// </summary>
         Offline = 0x00,

         /// <summary>
         /// AC power is online.
         /// </summary>
         OnLine = 0x01,

         /// <summary>
         /// AC line status is unknown.
         /// </summary>
         Unknown = 0xff
      }

      /// <summary>
      /// Defines the Battery charge status flags.
      /// </summary>
      [FlagsAttribute()]
      public enum BatteryFlags : byte
      {
         /// <summary>
         /// High
         /// </summary>
         High = 0x01,

         /// <summary>
         /// Low
         /// </summary>
         Low = 0x02,

         /// <summary>
         /// Critical
         /// </summary>
         Critical = 0x04,

         /// <summary>
         /// Charging
         /// </summary>
         Charging = 0x08,

         /// <summary>
         /// Reserved1
         /// </summary>
         Reserved1 = 0x10,

         /// <summary>
         /// Reserved2
         /// </summary>
         Reserved2 = 0x20,

         /// <summary>
         /// Reserved3
         /// </summary>
         Reserved3 = 0x40,

         /// <summary>
         /// No system battery
         /// </summary>
         NoBattery = 0x80,

         /// <summary>
         /// Unknown status
         /// </summary>
         Unknown = High | Low | Critical | Charging | Reserved1 | Reserved2 | Reserved3 | NoBattery
      }

      /// <summary>
      /// Flags for playing sound
      /// </summary>
      public enum PlaySoundFlags : int
      {
         SND_SYNC = 0x0,         // play synchronously (default)
         SND_ASYNC = 0x1,        // play asynchronously
         SND_NODEFAULT = 0x2,    // silence (!default) if sound not found
         SND_MEMORY = 0x4,       // pszSound points to a memory file
         SND_LOOP = 0x8,         // loop the sound until next sndPlaySound
         SND_NOSTOP = 0x10,      // don't stop any currently playing sound
         SND_NOWAIT = 0x2000,    // don't wait if the driver is busy
         SND_ALIAS = 0x10000,    // name is a registry alias
         SND_ALIAS_ID = 0x110000,// alias is a predefined ID
         SND_FILENAME = 0x20000, // name is file name
         SND_RESOURCE = 0x40004, // name is resource name or atom
      };
      #endregion

      //-----------------------------------------------------------------------

      #region SystemTime and TimeZone Structs

      /// <summary>
      /// Specifies a date and time, using individual members for the 
      /// month, day, year, weekday, hour, minute, second, and millisecond. 
      /// The time is either in coordinated universal time (UTC) or local 
      /// time, depending on the function that is being called.
      /// </summary>
      public struct SystemTime
      {
         public ushort Year;
         public ushort Month;
         public ushort DayOfWeek;
         public ushort Day;
         public ushort Hour;
         public ushort Minute;
         public ushort Second;
         public ushort Millisecond;
      }


      /// <summary>
      /// This structure specifies information specific to the time zone. 
      /// </summary>
      [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
      public struct TimeZoneInformation
      {
         /// <summary>
         /// Specifies the current bias, in minutes, for local time translation 
         /// on this computer. The bias is the difference, in minutes, between 
         /// UTC and local time (so UTC = local time + bias)
         /// </summary>
         public int Bias;

         /// <summary>
         /// Specifies a null-terminated string associated with standard 
         /// time on this operating system. For example, this member could 
         /// contain EST to indicate Eastern Standard Time.
         /// This string is not used by the operating system, so anything 
         /// stored there by using the SetTimeZoneInformation function is 
         /// returned unchanged by the GetTimeZoneInformation function.
         /// This string can be empty. 
         /// </summary>
         [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 32 )]
         public string StandardName;

         /// <summary>
         /// Specifies a "SystemTime" structure that contains a date and local 
         /// time when the transition from daylight time to standard time occurs 
         /// on this operating system. If this date is not specified, the wMonth 
         /// member in the "SystemTime" structure must be zero. 
         /// Note: If this date is specified, the DaylightDate value in the this 
         /// structure must also be specified. 
         /// </summary>
         public SystemTime StandardDate;

         /// <summary>
         /// Specifies a bias value to be used during local time translations 
         /// that occur during standard time. This member is ignored if a value 
         /// for the StandardDate member is not supplied.
         /// This value is added to the value of the Bias member to form the 
         /// bias used during standard time. In most time zones, the value of 
         /// this member is zero. 
         /// </summary>
         public int StandardBias;

         /// <summary>
         /// Specifies a null-terminated string associated with daylight time 
         /// on this operating system. For example, this member could contain 
         /// PDT to indicate Pacific Daylight Time. This string is not used by 
         /// the operating system, so anything stored there by using the 
         /// "SetTimeZoneInformation" function is returned unchanged by the 
         /// "GetTimeZoneInformation" function. This string can be empty. 
         /// </summary>
         [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 32 )]
         public string DaylightName;

         /// <summary>
         /// Specifies a "SystemTime" structure that contains a date and local 
         /// time when the transition from standard time to daylight time 
         /// occurs on this operating system. If this date is not specified, 
         /// the wMonth member in the "SystemTime" structure must be zero. 
         /// If this date is specified, the StandardDate value in thid structure 
         /// must also be specified. This member supports the absolute and 
         /// day-in-month time formats described for the StandardDate member. 
         /// </summary>
         public SystemTime DaylightDate;

         /// <summary>
         /// Specifies a bias value to be used during local time translations 
         /// that occur during daylight time. This member is ignored if a value 
         /// for the DaylightDate member is not supplied. This value is added 
         /// to the value of the Bias member to form the bias used during daylight 
         /// time. In most time zones, the value of this member is –60. 
         /// </summary>
         public int DaylightBias;
      }

      #endregion

      //-----------------------------------------------------------------------

      #region Flash Drive and SD Card Structures

      /// <summary>
      /// This structure describes a file found by the FindFirstFile, the 
      /// FindFirstFileEx or the FindNextFile function
      /// </summary>
      [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
      public struct LPWIN32_FIND_DATA
      {
         /// <summary>
         /// File attributes of the file found
         /// </summary>
         public uint dwFileAttributes;

         /// <summary>
         /// Contains the time at which the file was created
         /// </summary>
         public FILETIME ftCreationTime;

         /// <summary>
         /// Contains the time at which the file was last accessed
         /// </summary>
         public FILETIME ftLastAccessTime;

         /// <summary>
         /// Contains the time at which the file was written to
         /// </summary>
         public FILETIME ftLastWriteTime;

         /// <summary>
         /// High-order DWORD value of the file size, in bytes
         /// </summary>
         public uint nFileSizeHigh;

         /// <summary>
         /// Low-order DWORD value of the file size, in bytes
         /// </summary>
         public uint nFileSizeLow;

         /// <summary>
         /// Object identifier (OID) of the file
         /// </summary>
         public uint dwOID;

         /// <summary>
         /// Null-terminated string that is the name of the file
         /// </summary>
         [MarshalAs( UnmanagedType.ByValTStr, SizeConst = MAX_PATH )]
         public string cFileName;
      }


      /// <summary>
      /// Simple FileDateTime structure (for return purposes only)
      /// </summary>
      [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
      public struct FILETIME
      {
         UInt32 dwLowDateTime;
         UInt32 dwHighDateTime;
      }


      /// <summary>
      /// Flash Drive Structure
      /// </summary>
      [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
      public struct FlashDrive
      {
         /// <summary>
         /// Returns structure name as Flash drive name
         /// </summary>
         /// <returns></returns>
         public override string ToString()
         {
            return this.Path;
         }

         /// <summary>
         /// Full name of this Flash Drive or SD Card
         /// </summary>
         public string Path;

         /// <summary>
         /// How many bytes are available to current user
         /// </summary>
         public ulong ByteAvailableToCaller;

         /// <summary>
         /// How many bytes are there in total
         /// </summary>
         public ulong TotalNumOfBytes;

         /// <summary>
         /// How many Free bytes remain on the drive
         /// </summary>
         public ulong TotalNumOfFreeBytes;

         /// <summary>
         /// What is the largest File Size on the drive
         /// </summary>
         public ulong FileSizeHigh;

         /// <summary>
         /// What is the smallest File Size on the drive
         /// </summary>
         public ulong FileSizeLow;
      }

      #endregion

      //-----------------------------------------------------------------------

      #region System Info Structs and Constants

      /// <summary>
      /// Constant used to get the platform type, used with
      /// SystemParametersInfo4Strings
      /// </summary>
      public const uint SPI_GETPLATFORMTYPE = 257;

      /// <summary>
      /// Structure for Power Status information
      /// </summary>
      [StructLayout( LayoutKind.Sequential )]
      public class SYSTEM_POWER_STATUS_EX
      {
         public byte ACLineStatus;
         public byte BatteryFlag;
         public byte BatteryLifePercent;
         public byte Reserved1;
         public uint BatteryLifeTime;
         public uint BatteryFullLifeTime;
         public byte Reserved2;
         public byte BackupBatteryFlag;
         public byte BackupBatteryLifePercent;
         public byte Reserved3;
         public uint BackupBatteryLifeTime;
         public uint BackupBatteryFullLifeTime;
      }

      /// <summary>
      /// Contains information about a message queue.
      /// </summary>
      [StructLayout( LayoutKind.Sequential )]
      public struct MessageQueueOptions
      {
         /// <summary>
         /// Size of the structure in bytes.
         /// </summary>
         public uint Size;

         /// <summary>
         /// Describes the behavior of the message queue. Set to MSGQUEUE_NOPRECOMMIT to
         /// allocate message buffers on demand and to free the message buffers after
         /// they are read, or set to MSGQUEUE_ALLOW_BROKEN to enable a read or write
         /// operation to complete even if there is no corresponding writer or reader present.
         /// </summary>
         public uint Flags;

         /// <summary>
         /// Number of messages in the queue.
         /// </summary>
         public uint MaxMessages;

         /// <summary>
         /// Number of bytes for each message, do not set to zero.
         /// </summary>
         public uint MaxMessage;

         /// <summary>
         /// Set to TRUE to request read access to the queue. Set to FALSE to request write
         /// access to the queue.
         /// </summary>
         public uint ReadAccess;
      };

      /// <summary>
      /// Contains information about the power status of the system 
      /// as received from the Power Status message queue.
      /// </summary>
      [StructLayout( LayoutKind.Sequential )]
      public struct PowerInfo
      {
         /// <summary>
         /// Defines the event type.
         /// </summary>
         /// <see cref="MessageTypes"/>
         public MessageTypes Message;

         /// <summary>
         /// One of the system power flags.
         /// </summary>
         /// <see cref="SystemPowerStates"/>
         public SystemPowerStates Flags;

         /// <summary>
         /// The byte count of SystemPowerState that follows.
         /// </summary>
         public uint Length;

         /// <summary>
         /// Levels available in battery flag fields
         /// </summary>
         public uint NumLevels;

         /// <summary>
         /// Number of seconds of battery life remaining,
         /// or 0xFFFFFFFF if remaining seconds are unknown.
         /// </summary>
         public uint BatteryLifeTime;

         /// <summary>
         /// Number of seconds of battery life when at full charge,
         /// or 0xFFFFFFFF if full battery lifetime is unknown.
         /// </summary>
         public uint BatteryFullLifeTime;

         /// <summary>
         /// Number of seconds of backup battery life remaining,
         /// or BATTERY_LIFE_UNKNOWN if remaining seconds are unknown.
         /// </summary>
         public uint BackupBatteryLifeTime;

         /// <summary>
         /// Number of seconds of backup battery life when at full charge,
         /// or BATTERY_LIFE_UNKNOWN if full battery lifetime is unknown.
         /// </summary>
         public uint BackupBatteryFullLifeTime;

         /// <summary>
         /// AC power status.
         /// </summary>
         /// <see cref="ACLineStatus"/>
         public ACLineStatus ACLineStatus;

         /// <summary>
         /// Battery charge status.
         /// </summary>
         /// <see cref="BatteryFlags"/>
         public BatteryFlags BatteryFlag;

         /// <summary>
         /// Percentage of full battery charge remaining.
         /// This member can be a value in the range 0 (zero) to 100, or 255
         /// if the status is unknown. All other values are reserved.
         /// </summary>
         public byte BatteryLifePercent;

         /// <summary>
         /// Backup battery charge status.
         /// </summary>
         public byte BackupBatteryFlag;

         /// <summary>
         /// Percentage of full backup battery charge remaining.
         /// This value must be in the range of 0 to 100, or BATTERY_PERCENTAGE_UNKNOWN.
         /// </summary>
         public byte BackupBatteryLifePercent;
      };

      /// <summary>
      /// Constants for AC line status
      /// </summary>
      public const byte AC_LINE_OFFLINE       = 0x00;
      public const byte AC_LINE_ONLINE        = 0x01;
      public const byte AC_LINE_BACKUP_POWER  = 0x02;
      public const byte AC_LINE_UNKNOWN       = 0xFF;

      /// <summary>
      /// Constants for battery flags
      /// </summary>
      public const byte BATTERY_FLAG_HIGH           = 0x01;
      public const byte BATTERY_FLAG_LOW            = 0x02;
      public const byte BATTERY_FLAG_CRITICAL       = 0x04;
      public const byte BATTERY_FLAG_CHARGING       = 0x08;
      public const byte BATTERY_FLAG_NO_BATTERY     = 0x80;
      public const byte BATTERY_FLAG_UNKNOWN        = 0xFF;
      public const byte BATTERY_PERCENTAGE_UNKNOWN  = 0xFF;
      public const uint BATTERY_LIFE_UNKNOWN        = 0xFFFFFFFF;

      #endregion 

      //-----------------------------------------------------------------------

      #region Memory Status Structs

      /// <summary>
      /// Memory status structure
      /// </summary>
      public struct MEMORYSTATUS
      {
         public int dwLength;
         public int dwMemoryLoad;
         public int dwTotalPhys;
         public int dwAvailPhys;
         public int dwTotatlPageFile;
         public int dwAvailPageFile;
         public int dwTotalVirtual;
         public int dwAvailVirtual;
      }
      
      #endregion

      //-----------------------------------------------------------------------

      #region MainMenu manipulation structs and enums

      public enum ShowState : int
      {
         SW_HIDE = 0,
         SW_SHOW = 5,
      }

      [Flags()]
      public enum SHFS
      {
         SHOWTASKBAR = 0x0001,
         HIDETASKBAR = 0x0002,
         SHOWSIPBUTTON = 0x0004,
         HIDESIPBUTTON = 0x0008,
         SHOWSTARTICON = 0x0010,
         HIDESTARTICON = 0x0020
      }

      #endregion MainMenu manipulation structs and enums

      //-----------------------------------------------------------------------

      #region SIP Structs

      /// <summary>
      /// The RECT structure defines the coordinates of the upper-left
      /// and lower-right corners of a rectangle.
      /// </summary>
      [StructLayout( LayoutKind.Sequential )]
      public struct RECT
      {
         public int left;
         public int top;
         public int right;
         public int bottom;
      }
      #endregion

      //-----------------------------------------------------------------------

      #region LED method declarations

      /// <summary>
      /// For use with NLedGetDeviceInfo and NLedSetDevice
      /// </summary>
      public enum LED_INFO_TYPE : int
      {
         NLED_COUNT_INFO_ID = 0,
         NLED_SUPPORTS_INFO_ID = 1,
         NLED_SETTINGS_INFO_ID = 2
      }

      /// <summary>
      /// This structure (class) contains information about the capabilities of the 
      /// specified LED
      /// </summary>
      public class NLED_SETTINGS_INFO
      {
         /// <summary>
         /// LED number. The first LED is zero (0)
         /// </summary>
         public uint  LedNum = 0;

         /// <summary>
         /// Current setting. 0 - off, 1 - on, 2 - blink
         /// </summary>
         public uint  OffOnBlink = 0;

         /// <summary>
         /// Total cycle time of a blink, in microseconds
         /// </summary>
         public int   TotalCycleTime = 0;

         /// <summary>
         /// On time of the cycle, in microseconds
         /// </summary>
         public int   OnTime = 0;

         /// <summary>
         /// Off time of the cycle, in microseconds
         /// </summary>
         public int   OffTime = 0;

         /// <summary>
         /// Number of on blink cycles
         /// </summary>
         public int   MetaCycleOn = 0;

         /// <summary>
         /// Number of off blink cycles
         /// </summary>
         public int   MetaCycleOff = 0;

      };

      /// <summary>
      /// This structure (class) contains information about the number of 
      /// notification LEDs for the system
      /// </summary>
      public class NLED_COUNT_INFO
      {
         /// <summary>
         /// Count of LEDs for the system
         /// </summary>
         public int cLeds = 0;

      };


      /// <summary>
      /// This function returns information about the notification LED device
      /// </summary>
      /// <param name="nInfoId">Integer specifying the information to return.
      /// The following table shows the defined values. </param>
      /// <param name="pOutput">Pointer to the buffer to return the information. 
      /// The buffer points to various structure types, depending on the value of nInfoId.
      /// The following table shows these structures.</param>
      /// <returns>TRUE indicates success. FALSE indicates failure</returns>
      [DllImport( "coredll.dll" )]
      public static extern bool NLedGetDeviceInfo( LED_INFO_TYPE nInfoId, NLED_COUNT_INFO pOutput );


      /// <summary>
      /// This function changes the setting of a notification LED.
      /// </summary>
      /// <param name="nInfoId">Indicates what kind of configuration information
      /// is being changed. The only supported value is NLED_SETTINGS_INFO_ID</param>
      /// <param name="pOutput">Pointer to the buffer that contains the 
      /// NLED_SETTINGS_INFO structure. This structure contains the new settings
      /// for the notification LED.</param>
      /// <returns>TRUE indicates success. FALSE indicates failure.</returns>
      [DllImport( "coredll.dll" )]
      public static extern bool NLedSetDevice( LED_INFO_TYPE nInfoId, NLED_SETTINGS_INFO pOutput );

      #endregion

      //-----------------------------------------------------------------------

      #region CoreDLL method declarations

      [DllImport( "coredll.dll" )]
      public extern static int GetDeviceUniqueID(
          [In, Out] byte[] appdata,
          int cbApplictionData,
          int dwDeviceIDVersion,
          [In, Out] byte[] deviceIDOuput,
          out uint pcbDeviceIDOutput );


      [DllImport( "coredll.dll", SetLastError = true )]
      public static extern bool KernelIoControl(
          Int32 dwIoControlCode,
          IntPtr lpInBuf,
          Int32 nInBufSize,
          byte[] lpOutBuf,
          Int32 nOutBufSize,
          ref Int32 lpBytesReturned );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegCloseKey(
          IntPtr hKey );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegOverridePredefKey(
          IntPtr hKey,
          IntPtr hNewHKey );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegOpenUserClassesRoot(
          IntPtr hToken,
          int dwOptions,
          int samDesired,
          IntPtr phkResult );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegOpenCurrentUser(
          int samDesired,
          IntPtr phkResult );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegDisablePredefinedCache();


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegConnectRegistry(
          string lpMachineName,
          IntPtr hKey,
          IntPtr phkResult );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegCreateKey(
          IntPtr hKey,
          string lpSubKey,
          IntPtr phkResult );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegCreateKeyEx(
          IntPtr hKey,
          string lpSubKey,
          int Reserved,
          string lpClass,
          int dwOptions,
          int samDesired,
          IntPtr lpSecurityAttributes,
          ref IntPtr phkResult,
          ref WinCE.REGDISP lpdwDisposition );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegDeleteKey(
          IntPtr hKey,
          string lpSubKey );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegDeleteValue(
          IntPtr hKey,
          string lpValueName );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegEnumKey(
          IntPtr hKey,
          int dwIndex,
          string lpName,
          int cbName );


      [DllImport( "CoreDll.DLL" )]
      public static extern int RegEnumKeyEx(
          IntPtr hKey,
          int dwIndex,
          string lpName,
          ref int lpcbName,
          int lpReserved,
          string lpClass,
          ref int lpcbClass,
          int lpftLastWriteTime );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegEnumValue(
          IntPtr hKey,
          int dwIndex,
          string lpValueName,
          ref int lpcbValueName,
          ref int lpReserved,
          ref int lpType,
          ref byte lpData,
          ref int lpcbData );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegLoadKey(
          IntPtr hKey,
          string lpSubKey,
          string lpFile );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegNotifyChangeKeyValue(
          IntPtr hKey,
          int bWatchSubtree,
          int dwNotifyFilter,
          IntPtr hEvent,
          int fAsynchronus );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegOpenKey(
          IntPtr hKey,
          string lpSubKey,
          IntPtr phkResult );


      [DllImport( "CoreDll.DLL" )]
      public static extern int RegOpenKeyEx(
          IntPtr hKey,
          string lpSubKey,
          int ulOptions,
          int samDesired,
          ref IntPtr phkResult );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegQueryValue(
          IntPtr hKey,
          string lpSubKey,
          string lpValue,
          ref int lpcbValue );


      //  System.IntPtr version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          IntPtr iptrData, ref int lpcbData );


      //  System.StringBuilder version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          StringBuilder strData,
          ref int lpcbData );


      //  System.int version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          ref int piData,
          ref int lpcbData );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegReplaceKey(
          IntPtr hKey,
          string lpSubKey,
          string lpNewFile,
          string lpOldFile );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegRestoreKey(
          IntPtr hKey,
          string lpFile,
          int dwFlags );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSaveKey(
          IntPtr hKey,
          string lpFile,
          IntPtr lpSecurityAttributes );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSetValue(
          IntPtr hKey,
          string lpSubKey,
          int dwType,
          string lpData,
          int cbData );


      //  System.IntPtr version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          ref IntPtr lpData,
          int cbData );


      //  System.int version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          ref int piData,
          int cbData );


      //  System.string version
      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          string strData,
          int cbData );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegUnLoadKey(
          IntPtr hKey,
          string lpSubKey );


      [DllImport( "CoreDLL.DLL" )]
      public static extern int RegSaveKeyEx(
          IntPtr hKey,
          string lpFile,
          IntPtr lpSecurityAttributes,
          int Flags );


      /// <summary>
      /// This function retrieves the power status of the system.  Not supported on emulators.
      /// </summary>
      /// <param name="pStatus">Pointer to the SYSTEM_POWER_STATUS_EX structure receiving the 
      /// power status information.</param>
      /// <param name="fUpdate">If this Boolean is set to TRUE, GetSystemPowerStatusEx gets the 
      /// latest information from the device driver, otherwise it retrieves cached information 
      /// that may be out-of-date by several seconds</param>
      /// <returns>This function returns TRUE if successful; otherwise, it returns FALSE.</returns>
      [DllImport( "coredll.dll" )]
      public static extern bool GetSystemPowerStatusEx(
         SYSTEM_POWER_STATUS_EX pStatus,
         bool fUpdate );


      /// <summary>
      /// Sets the specified window's show state
      /// </summary>
      /// <param name="hWnd">Handle to the window</param>
      /// <param name="nCmdShow"> Specifies how the window is to be shown.  See ShowState enum</param>
      /// <returns>If the window was previously visible, the return value is nonzero.
      /// If the window was previously hidden, the return value is zero</returns>
      [DllImport( "coredll.dll" )]
      public static extern bool ShowWindow(
         IntPtr hWnd,
         int nCmdShow );


      /// <summary>
      /// Gets the memory status
      /// </summary>
      /// <param name="lpBuffer"></param>
      [DllImport( "coredll.dll" )]
      public static extern void GlobalMemoryStatus( ref MEMORYSTATUS lpBuffer );


      /// <summary>
      /// This function starts running an application when a specified event 
      /// occurs.
      /// </summary>
      /// <param name="pwszAppName">Pointer to a null-terminated string that 
      /// specifies the name of the application to be started</param>
      /// <param name="lWhichEvent">Event at which the application is to be 
      /// started. It is one of the following values</param>
      /// <returns>TRUE indicates success. FALSE indicates failure.</returns>
      [DllImport( "coredll.dll", EntryPoint = "CeRunAppAtEvent", SetLastError = true )]
      public static extern bool CeRunAppAtEvent( string pwszAppName, int lWhichEvent );


      /// <summary>
      /// 
      /// </summary>
      /// <param name="hwndOwner">Reserved</param>
      /// <param name="lpszPath">A pointer to a null-terminated string that receives
      /// the drive and path of the specified folder. This buffer must be at least
      /// MAX_PATH characters in size</param>
      /// <param name="nFolder">A CSIDL that identifies the folder of interest. If 
      /// a virtual folder is specified, this function will fail</param>
      /// <param name="fCreate">Indicates whether the folder should be created if 
      /// it does not already exist. If this value is nonzero, the folder is 
      /// created. If this value is zero, the folder is not created</param>
      /// <returns>TRUE if successful; otherwise, FALSE.</returns>
      [DllImport("coredll.dll")]
      public static extern int SHGetSpecialFolderPath(
         IntPtr hwndOwner,
         StringBuilder lpszPath,
         int nFolder,
         int fCreate );


      /// <summary>
      /// This function creates a named or unnamed event object
      /// </summary>
      /// <param name="lpEventAttributes">Set to IntPtr.Zero</param>
      /// <param name="bManualReset">Boolean value that specifies whether a 
      /// manual-reset or auto-reset event object is created. If true, you 
      /// must use the ResetEvent function to manually reset the state to 
      /// nonsignaled. If false, the system automatically resets the state
      /// to nonsignaled after a single waiting thread is released</param>
      /// <param name="bIntialState">Boolean value that specifies the initial
      /// state of the event object. If true, the initial state is signaled;
      /// otherwise, it is nonsignaled.</param>
      /// <param name="lpName">Pointer to a null-terminated string that 
      /// specifies the name of the event object.</param>
      /// <returns>A handle to the event object indicates success. 
      /// NULL indicates failure. 
      /// To get extended error information, call GetLastError</returns>
      [DllImport( "coredll.dll", SetLastError = true )]
      public static extern IntPtr CreateEvent( 
         IntPtr lpEventAttributes, 
         bool bManualReset, 
         bool bIntialState, 
         string lpName );


      /// <summary>
      /// Closes an open object handle.
      /// </summary>
      /// <param name="lpHandle">A valid handle to an open object.</param>
      /// <returns>If the function succeeds, the return value is nonzero.</returns>
      [DllImport( "coredll.dll", SetLastError = true )]
      public static extern int CloseHandle( IntPtr lpHandle );

      /// <summary>
      /// Plays a sound
      /// </summary>
      /// <param name="szSound">The file to play</param>
      /// <param name="hModule"></param>
      /// <param name="flags">PlaySoundFlags enum</param>
      /// <returns>1 if played, 0 if not</returns>
      [DllImport( "coredll.dll", SetLastError = true )]
      public static extern int PlaySound( string szSound, IntPtr hModule, int flags );
      #endregion

      //-----------------------------------------------------------------------

      #region WinCE Entry Point Declarations

      /// <summary>
      /// Retrieves the current local date and time for a WinCE device. The system time 
      /// is expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      [DllImport( "coredll.dll", EntryPoint = "GetLocalTime", SetLastError = true )]
      public extern static void WinCEGetLocalTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Sets the current local time and date for a WinCE device. The system time is 
      /// expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      /// <returns>If the function succeeds, the return value is true</returns>
      [DllImport( "coredll.dll", EntryPoint = "SetLocalTime", SetLastError = true )]
      public extern static bool WinCESetLocalTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Retrieves the current system date and time for a WinCE device. The system time 
      /// is expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      [DllImport( "coredll.dll", EntryPoint = "GetSystemTime", SetLastError = true )]
      public extern static void WinCEGetSystemTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Sets the current system time and date for a WinCE device. The system time is 
      /// expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      /// <returns>If the function succeeds, the return value is true</returns>
      [DllImport( "coredll.dll", EntryPoint = "SetSystemTime", SetLastError = true )]
      public extern static bool WinCESetSystemTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Retrieves the current time zone settings for a WinCE device. These settings control 
      /// the translations between Coordinated Universal Time (UTC) and local time.
      /// </summary>
      /// <param name="lpTimeZoneInformation">TimeZoneInformation structure with current settings</param>
      /// <returns>If the function succeeds, the return value is nonzero</returns>
      [DllImport( "coredll.dll", EntryPoint = "GetTimeZoneInformation",
         CharSet = CharSet.Auto, SetLastError = true )]
      public static extern int WinCEGetTimeZoneInformation(
         out TimeZoneInformation lpTimeZoneInformation );


      /// <summary>
      /// Sets the current time zone settings for a WinCE device. These settings control 
      /// translations from Coordinated Universal Time (UTC) to local time.
      /// </summary>
      /// <param name="lpTimeZoneInformation">TimeZoneInformation structure with new settings</param>
      /// <returns>If the function succeeds, the return value is nonzero</returns>
      [DllImport( "coredll.dll", EntryPoint = "SetTimeZoneInformation",
         CharSet = CharSet.Auto, SetLastError = true )]
      public static extern int WinCESetTimeZoneInformation(
         ref TimeZoneInformation lpTimeZoneInformation );


      /// <summary>
      /// This function queries or sets system-wide parameters, and updates the user profile 
      /// during the process.
      /// </summary>
      /// <param name="uiAction">Specifies the system-wide parameter to query or set</param>
      /// <param name="uiParam">Depends on the system parameter being queried or set. For more 
      /// information about system-wide parameters, see the uiAction parameter. If not otherwise 
      /// indicated, you must specify zero for this parameter.</param>
      /// <param name="pvParam">Depends on the system parameter being queried or set. For more 
      /// information about system-wide parameters, see the uiAction parameter. If not otherwise
      /// indicated, you must specify NULL for this parameter.</param>
      /// <param name="fWinIni"> Boolean that, if a system parameter is being set, specifies whether
      /// the user profile is to be updated, and if so, whether the WM_SETTINGCHANGE message is to
      /// be broadcast to all top-level windows to notify them of the change. This parameter can be
      /// zero or can be one of the following values.</param>
      /// <returns>Nonzero indicates success. Zero indicates failure.</returns>
      [DllImport( "Coredll.dll", EntryPoint = "SystemParametersInfoW", CharSet = CharSet.Unicode )]
      public static extern int SystemParametersInfo4Strings(
         uint uiAction,
         uint uiParam,
         StringBuilder pvParam,
         uint fWinIni );


      /// <summary>
      /// Retrieves a handle to the top-level window whose class name and window name match the 
      /// specified strings
      /// </summary>
      /// <param name="lpClassName">Pointer to a null-terminated string that specifies the class name 
      /// or a class atom created by a previous call to the RegisterClass or RegisterClassEx function. 
      /// If lpClassName is NULL, it finds any window whose title matches the lpWindowName parameter</param>
      /// <param name="lpWindowName">Pointer to a null-terminated string that specifies the window name 
      /// (the window's title). If this parameter is NULL, all window names match</param>
      /// <returns>If the function succeeds, the return value is a handle to the window that has the 
      /// specified class name and window name.  If the function fails, the return value is NULL</returns>
      [DllImport( "coredll.dll", EntryPoint = "FindWindowW", SetLastError = true )]
      public static extern IntPtr FindWindowCE(
         string lpClassName,
         string lpWindowName );


      /// <summary>
      /// Retrieves the dimensions of the bounding rectangle of the specified window
      /// </summary>
      /// <param name="hwnd">A handle to the window</param>
      /// <param name="lpRect">A pointer to a RECT structure that receives the screen
      /// coordinates of the upper-left and lower-right corners of the window. </param>
      /// <returns>If the function succeeds, the return value is nonzero.</returns>
      [DllImport( "coredll.dll" )]
      [return: MarshalAs( UnmanagedType.Bool )]
      public static extern bool GetWindowRect( IntPtr hwnd, out RECT lpRect );


      /// <summary>
      /// Changes the position and dimensions of the specified window.
      /// </summary>
      /// <param name="hwnd">A handle to the window. </param>
      /// <param name="X">The new position of the left side of the window. </param>
      /// <param name="Y">The new position of the top of the window. </param>
      /// <param name="nWidth">The new width of the window. </param>
      /// <param name="nHeight">The new height of the window. </param>
      /// <param name="bRepaint">Indicates whether the window is to be repainted.</param>
      [DllImport( "coredll.dll" )]
      public static extern void MoveWindow( IntPtr hwnd, int X, int Y, int nWidth, int nHeight, bool bRepaint );


      /// <summary>
      /// This function shows or hides the currently active software-based input panel window.
      /// </summary>
      /// <param name="dwFlag">Specifies whether to show or hide the current software-based
      /// input panel window. </param>
      /// <returns>TRUE indicates success</returns>
      [DllImport( "coredll.dll", SetLastError = true )]
      public extern static int SipShowIM( int dwFlag );


      /// <summary>
      /// Retrieves information about the amount of space that is available on
      /// a disk volume, which is the total amount of space, the total amount of 
      /// free space, and the total amount of free space available to the user 
      /// that is associated with the calling thread
      /// </summary>
      /// <param name="lpDirectoryName">A directory on the disk</param>
      /// <param name="lpFreeBytesAvailableToCaller">Total number of free bytes available</param>
      /// <param name="lpTotalNumberOfBytes">Total number of free bytes available</param>
      /// <param name="lpTotalNumberOfFreeBytes">Total number of free bytes available</param>
      /// <returns></returns>
      [DllImport( "coredll.dll" )]
      public static extern bool GetDiskFreeSpaceEx(
         string lpDirectoryName,
         out ulong lpFreeBytesAvailableToCaller,
         out ulong lpTotalNumberOfBytes,
         out ulong lpTotalNumberOfFreeBytes
      );


      /// <summary>
      /// Closes a file search handle opened by the FindFirstFile
      /// </summary>
      /// <param name="hFindFile">The file search handle</param>
      /// <returns>If the function succeeds, the return value is nonzero</returns>
      [DllImport( "coredll.dll" )]
      public static extern bool FindClose( IntPtr hFindFile );

      #endregion

      //-----------------------------------------------------------------------

      #region Win32 Entry Point Declarations

      /// <summary>
      /// Retrieves the current local date and time for a WinCE device. The system time 
      /// is expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      [DllImport( "kernel32.dll", EntryPoint = "GetLocalTime", SetLastError = true )]
      public extern static void Win32GetLocalTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Sets the current local time and date for a WinCE device. The system time is 
      /// expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      /// <returns>If the function succeeds, the return value is true</returns>
      [DllImport( "kernel32.dll", EntryPoint = "SetLocalTime", SetLastError = true )]
      public extern static bool Win32SetLocalTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Retrieves the current system date and time for a Win32 device. The system time 
      /// is expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      [DllImport( "kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true )]
      public extern static void Win32GetSystemTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Sets the current system time and date for a Win32 device. The system time is 
      /// expressed in Coordinated Universal Time (UTC).
      /// </summary>
      /// <param name="lpSystemTimeInformation">SystemTime structure</param>
      /// <returns>If the function succeeds, the return value is true</returns>
      [DllImport( "kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true )]
      public extern static bool Win32SetSystemTime(
         ref SystemTime lpSystemTimeInformation );


      /// <summary>
      /// Retrieves the current time zone settings for a Win32 device. These settings control 
      /// the translations between Coordinated Universal Time (UTC) and local time.
      /// </summary>
      /// <param name="lpTimeZoneInformation"></param>
      /// <returns>If the function succeeds, the return value is nonzero</returns>
      [DllImport( "kernel32.dll", EntryPoint = "GetTimeZoneInformation",
         CharSet = CharSet.Auto, SetLastError = true )]
      public static extern int Win32GetTimeZoneInformation(
         out TimeZoneInformation lpTimeZoneInformation );


      /// <summary>
      /// Sets the current time zone settings for a WinCE device. These settings control 
      /// translations from Coordinated Universal Time (UTC) to local time.
      /// </summary>
      /// <param name="lpTimeZoneInformation">TimeZoneInformation structure with new settings</param>
      /// <returns>If the function succeeds, the return value is nonzero</returns>
      [DllImport( "kernel32.dll", EntryPoint = "SetTimeZoneInformation",
         CharSet = CharSet.Auto, SetLastError = true )]
      public static extern int Win32SetTimeZoneInformation(
         ref TimeZoneInformation lpTimeZoneInformation );

      #endregion

      //-----------------------------------------------------------------------

      #region Flash and SD Card Entry Points for "note_prj" library

      /// <summary>
      /// returns the handle to the first mountable file system, such as a flash card
      /// </summary>
      /// <param name="lpFindFlashData">[out] Long pointer to the CE WIN32_FIND_DATA 
      /// structure that represents the mountable file system</param>
      /// <returns>A handle to the first flash card indicates success. 
      /// INVALID_HANDLE_VALUE indicates failure</returns>
      [DllImport( "note_prj.dll", EntryPoint = "FindFirstFlashCard" )]
      public static extern IntPtr FindFirstFlashCard(
         out LPWIN32_FIND_DATA lpFindFlashData );


      /// <summary>
      /// This function finds the next mountable file system, such as a flash card
      /// </summary>
      /// <param name="hFlashCard">[in] Handle to the first flash card as returned 
      /// by a previous call to the FindFirstFlashCard function.</param>
      /// <param name="lpFindFlashData">[out] Long pointer to the WIN32_FIND_DATA 
      /// structure that receives information about the found flash card</param>
      /// <returns>TRUE indicates success. FALSE indicates failure. </returns>
      [DllImport( "note_prj.dll", EntryPoint = "FindNextFlashCard" )]
      public static extern int FindNextFlashCard(
         IntPtr hFlashCard,
         ref LPWIN32_FIND_DATA lpFindFlashData );

      #endregion

      //-----------------------------------------------------------------------

      #region WinCE Power Notifications

      /// <summary>
      /// This function allows applications and drivers to register for power notification events
      /// </summary>
      /// <param name="hMsgQ">[in] Handle to application's message queue</param>
      /// <param name="Flags">[in] Set to the logical-or of the desired PBT_XXX 
      /// notifications, or to POWER_NOTIFY_ALL to receive all notifications</param>
      /// <returns>Nonzero indicates success. Zero indicates failure</returns>
      [DllImport( "coredll.dll", EntryPoint = "RequestPowerNotifications" )]
      public static extern IntPtr WinCERequestPowerNotifications( IntPtr hMsgQ, uint Flags );


      /// <summary>
      /// This function allows applications and drivers to stop receiving power notification events
      /// </summary>
      /// <param name="hReq">[in] The handle returned from RequestPowerNotifications</param>
      /// <returns>TRUE indicates success. FALSE indicates failuree</returns>
      [DllImport( "coredll.dll", EntryPoint = "StopPowerNotifications" )]
      public static extern bool WinCEStopPowerNotifications( IntPtr hReq );

      #endregion

      //-----------------------------------------------------------------------

      #region WinCE Message Queue

      /// <summary>
      /// Creates or opens a user-defined message queue
      /// </summary>
      /// <param name="iName">[in] Name of the message queue. You can also set this 
      /// parameter to NULL or you must set it no larger than the MAX_PATH characters 
      /// including the NULL character. Each object type, such as memory maps, semaphores, 
      /// events, message queues, mutexes, and watchdog timers, has its own separate 
      /// namespace. Empty strings, "", are handled as named objects. On Windows desktop-based 
      /// platforms, synchronization objects all share the same namespace.</param>
      /// <param name="Options">[in] Pointer to an MSGQUEUEOPTIONS structure that sets the 
      /// properties of the message queue</param>
      /// <returns>Nonzero indicates success. Zero indicates failure</returns>
      [DllImport( "coredll.dll", EntryPoint = "CreateMsgQueue" )]
      public static extern IntPtr WinCECreateMsgQueue( string iName, ref MessageQueueOptions Options );


      /// <summary>
      /// Closes a currently open message queue
      /// </summary>
      /// <param name="hMsgQ">[in] Handle to an open message queue.</param>
      /// <returns>TRUE indicates success. FALSE indicates failure</returns>
      [DllImport( "coredll.dll", EntryPoint = "CloseMsgQueue" )]
      public static extern bool WinCECloseMsgQueue( IntPtr hMsgQ );


      /// <summary>
      /// Reads a single message from a message queue
      /// </summary>
      /// <param name="hMsgQ">[in] Handle to an open message queue</param>
      /// <param name="oBuffer">[out] Pointer to a buffer to store a read message. 
      /// This parameter cannot be NULL</param>
      /// <param name="iBufferSize">[in] Size of the buffer, in bytes, specified by lpBuffer. 
      /// This parameter cannot be 0 (zero)</param>
      /// <param name="oBytesRead">[out] Number of bytes stored in lpBuffer. 
      /// This parameter cannot be NULL</param>
      /// <param name="iTimeout">[in] Time in milliseconds (ms) before the read 
      /// operation times out. If set to zero, the read operation will not block if 
      /// there is no data to read. If set to INFINITE, the read operation will block 
      /// until data is available or the status of the queue changes</param>
      /// <param name="oFlags">[out] Pointer to a DWORD to indicate the properties of 
      /// the message. A value of MSGQUEUE_MSGALERT specifies that the message is an 
      /// alert message</param>
      /// <returns>Nonzero indicates success. Zero indicates failure</returns>
      [DllImport( "coredll.dll", EntryPoint = "ReadMsgQueue" )]
      public static extern bool WinCEReadMsgQueue( IntPtr hMsgQ, ref PowerInfo oBuffer,
         uint iBufferSize, ref uint oBytesRead, uint iTimeout, ref uint oFlags );


      /// <summary>
      /// Waits until one or all of the specified objects are in the signaled state 
      /// or the time-out interval elapses
      /// </summary>
      /// <param name="nCount">The number of object handles in the array 
      /// pointed to by lpHandles</param>
      /// <param name="lpHandles">An array of object handles. For a list of the object types 
      /// whose handles can be specified, see the following Remarks section. The array can 
      /// contain handles to objects of different types. It may not contain multiple copies 
      /// of the same handle</param>
      /// <param name="iWaitAll">If this parameter is TRUE, the function returns when the 
      /// state of all objects in the lpHandles array is signaled. If FALSE, the function 
      /// returns when the state of any one of the objects is set to signaled. In the latter 
      /// case, the return value indicates the object whose state caused the function to return</param>
      /// <param name="dwMilliseconds">The time-out interval, in millisecond</param>
      /// <returns>Nonzero indicates success. Zero indicates failure</returns>
      [DllImport( "coredll.dll", EntryPoint = "WaitForMultipleObjects", SetLastError = true )]
      public static extern int WinCEWaitForMultipleObjects( uint nCount, IntPtr[] lpHandles,
         bool iWaitAll, int dwMilliseconds );

      #endregion

      //-----------------------------------------------------------------------

      #region Microsoft.Win32.Registry Functions

      [DllImport( "advapi32.dll", EntryPoint = "RegCloseKey", SetLastError = true )]
      public static extern int Win32RegCloseKey(
          IntPtr hKey );


      [DllImport( "advapi32.dll", EntryPoint = "RegOverridePredefKey", SetLastError = true )]
      public static extern int Win32RegOverridePredefKey(
          IntPtr hKey,
          IntPtr hNewHKey );


      [DllImport( "advapi32.dll", EntryPoint = "RegOpenUserClassesRoot", SetLastError = true )]
      public static extern int Win32RegOpenUserClassesRoot(
          IntPtr hToken,
          int dwOptions,
          int samDesired,
          IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegOpenCurrentUser", SetLastError = true )]
      public static extern int Win32RegOpenCurrentUser(
          int samDesired,
          IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegDisablePredefinedCache", SetLastError = true )]
      public static extern int Win32RegDisablePredefinedCache();


      [DllImport( "advapi32.dll", EntryPoint = "RegConnectRegistry", SetLastError = true )]
      public static extern int Win32RegConnectRegistry(
          string lpMachineName,
          IntPtr hKey,
          IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegCreateKey", SetLastError = true )]
      public static extern int Win32RegCreateKey(
          IntPtr hKey,
          string lpSubKey,
          IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegCreateKeyEx", SetLastError = true )]
      public static extern int Win32RegCreateKeyEx(
          IntPtr hKey,
          string lpSubKey,
          int Reserved,
          string lpClass,
          int dwOptions,
          int samDesired,
          IntPtr lpSecurityAttributes,
          ref IntPtr phkResult,
          ref WinCE.REGDISP lpdwDisposition );


      [DllImport( "advapi32.dll", EntryPoint = "RegDeleteKey", SetLastError = true )]
      public static extern int Win32RegDeleteKey(
          IntPtr hKey,
          string lpSubKey );


      [DllImport( "advapi32.dll", EntryPoint = "RegDeleteValue", SetLastError = true )]
      public static extern int Win32RegDeleteValue(
          IntPtr hKey,
          string lpValueName );


      [DllImport( "advapi32.dll", EntryPoint = "RegEnumKey", SetLastError = true )]
      public static extern int Win32RegEnumKey(
          IntPtr hKey,
          int dwIndex,
          string lpName,
          int cbName );


      [DllImport( "advapi32.dll", EntryPoint = "RegEnumKeyEx", SetLastError = true )]
      public static extern int Win32RegEnumKeyEx(
          IntPtr hKey,
          int dwIndex,
          string lpName,
          ref int lpcbName,
          int lpReserved,
          string lpClass,
          ref int lpcbClass,
          int lpftLastWriteTime );


      [DllImport( "advapi32.dll", EntryPoint = "RegEnumValue", SetLastError = true )]
      public static extern int Win32RegEnumValue(
          IntPtr hKey,
          int dwIndex,
          string lpValueName,
          ref int lpcbValueName,
          ref int lpReserved,
          ref int lpType,
          ref byte lpData,
          ref int lpcbData );


      [DllImport( "advapi32.dll", EntryPoint = "RegLoadKey", SetLastError = true )]
      public static extern int Win32RegLoadKey(
          IntPtr hKey,
          string lpSubKey,
          string lpFile );


      [DllImport( "advapi32.dll", EntryPoint = "RegNotifyChangeKeyValue", SetLastError = true )]
      public static extern int Win32RegNotifyChangeKeyValue(
          IntPtr hKey,
          int bWatchSubtree,
          int dwNotifyFilter,
          IntPtr hEvent,
          int fAsynchronus );


      [DllImport( "advapi32.dll", EntryPoint = "RegOpenKey", SetLastError = true )]
      public static extern int Win32RegOpenKey(
          IntPtr hKey,
          string lpSubKey,
          IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegOpenKeyEx", SetLastError = true )]
      public static extern int Win32RegOpenKeyEx(
          IntPtr hKey,
          string lpSubKey,
          int ulOptions,
          int samDesired,
          ref IntPtr phkResult );


      [DllImport( "advapi32.dll", EntryPoint = "RegQueryValue", SetLastError = true )]
      public static extern int Win32RegQueryValue(
          IntPtr hKey,
          string lpSubKey,
          string lpValue,
          ref int lpcbValue );


      //  System.IntPtr version
      [DllImport( "advapi32.dll", EntryPoint = "RegQueryValueEx", SetLastError = true )]
      public static extern int Win32RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          IntPtr iptrData, ref int lpcbData );


      //  System.StringBuilder version
      [DllImport( "advapi32.dll", EntryPoint = "RegQueryValueEx", SetLastError = true )]
      public static extern int Win32RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          StringBuilder strData,
          ref int lpcbData );


      //  System.int version
      [DllImport( "advapi32.dll", EntryPoint = "RegQueryValueEx", SetLastError = true )]
      public static extern int Win32RegQueryValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          ref REGTYPE lpType,
          ref int piData,
          ref int lpcbData );


      [DllImport( "advapi32.dll", EntryPoint = "RegReplaceKey", SetLastError = true )]
      public static extern int Win32RegReplaceKey(
          IntPtr hKey,
          string lpSubKey,
          string lpNewFile,
          string lpOldFile );


      [DllImport( "advapi32.dll", EntryPoint = "RegRestoreKey", SetLastError = true )]
      public static extern int Win32RegRestoreKey(
          IntPtr hKey,
          string lpFile,
          int dwFlags );


      [DllImport( "advapi32.dll", EntryPoint = "RegSaveKey", SetLastError = true )]
      public static extern int Win32RegSaveKey(
          IntPtr hKey,
          string lpFile,
          IntPtr lpSecurityAttributes );


      [DllImport( "advapi32.dll", EntryPoint = "RegSetValue", SetLastError = true )]
      public static extern int Win32RegSetValue(
          IntPtr hKey,
          string lpSubKey,
          int dwType,
          string lpData,
          int cbData );


      //  System.IntPtr version
      [DllImport( "advapi32.dll", EntryPoint = "RegSetValueEx", SetLastError = true )]
      public static extern int Win32RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          ref IntPtr lpData,
          int cbData );


      //  System.int version
      [DllImport( "advapi32.dll", EntryPoint = "RegSetValueEx", SetLastError = true )]
      public static extern int Win32RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          ref int piData,
          int cbData );


      //  System.string version
      [DllImport( "advapi32.dll", EntryPoint = "RegSetValueEx", SetLastError = true )]
      public static extern int Win32RegSetValueEx(
          IntPtr hKey,
          string lpValueName,
          int Res1,
          REGTYPE dwType,
          string strData,
          int cbData );


      [DllImport( "advapi32.dll", EntryPoint = "RegUnLoadKey", SetLastError = true )]
      public static extern int Win32RegUnLoadKey(
          IntPtr hKey,
          string lpSubKey );


      [DllImport( "advapi32.dll", EntryPoint = "RegSaveKeyEx", SetLastError = true )]
      public static extern int Win32RegSaveKeyEx(
          IntPtr hKey,
          string lpFile,
          IntPtr lpSecurityAttributes,
          int Flags );

      #endregion Microsoft.Win32.Registry Functions

      //-----------------------------------------------------------------------

      #region Direct Registry calls (outside of the MI Hive)

      /// <summary>
      /// Retrieves subkeys for a given key in a string array
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iBranch">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="oSubKeys">Reference to an array to hold the subkeys</param>
      /// <returns>True on success</returns>
      public static bool GetSubKeyNames( WinCE.HKEY iHive, string iBranch, string iKeyName, 
         ref string[] oSubKeys )
      {
         RegistryKey hKey = null;
         string keyPath = iBranch + "\\" + iKeyName;

         try
         {
            // Open the subkey
            if ( iHive == HKEY.HKEY_LOCAL_MACHINE )
            {
               hKey = Registry.LocalMachine.OpenSubKey( keyPath );
            }
            else
            {
               hKey = Registry.CurrentUser.OpenSubKey( keyPath );
            }

            // If the key is there, get the subkeys
            if ( hKey != null )
            {
               oSubKeys = hKey.GetSubKeyNames();
               return true;
            }
         }
         finally
         {
            // clean up
            if ( hKey != null )
            {
               hKey.Close();
            }
         }

         return false;

      }/* end method */


      /// <summary>
      /// Simple method that checks for the presence of a specific key
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iBranch">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <returns>true if key exists, else false</returns>
      public static Boolean DoesSubKeyExist( WinCE.HKEY iHive, string iBranch, 
         string iKeyName )
      {
         RegistryKey hKey = null;
         string keyPath = iBranch + "\\" + iKeyName;
         Boolean result = false;

         try
         {
            switch ( iHive )
            {
               case HKEY.HKEY_CLASSES_ROOT:
                  {
                     hKey = Registry.ClassesRoot.OpenSubKey( keyPath );
                     break;
                  }
               case HKEY.HKEY_CURRENT_USER:
                  {
                     hKey = Registry.CurrentUser.OpenSubKey( keyPath );
                     break;
                  }
               case HKEY.HKEY_LOCAL_MACHINE:
                  {
                     hKey = Registry.LocalMachine.OpenSubKey( keyPath );
                     break;
                  }
               case HKEY.HKEY_USERS:
                  {
                     hKey = Registry.Users.OpenSubKey(keyPath);
                     break;
                  }
               default:
                  {
                     hKey = null;
                     break;
                  }
            }
         }
         finally
         {
            if ( hKey != null )
            {
               result = true;
               hKey.Close();
            }
         }

         return result;

      }/* end method */


      /// <summary>
      /// Execute a generic Registry read
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iBranch">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValue">The name of the value to query</param>
      /// <param name="oValue">Returned value as object</param>
      /// <returns>True on success, false on error</returns>
      public static bool GetKeyValue( WinCE.HKEY iHive, string iBranch, 
         string iKeyName, string iValue, ref object oValue )
      {
         RegistryKey hKey = null;
         string keyPath = iBranch + "\\" + iKeyName;

         try
         {
            // Open the subkey
            if ( iHive == HKEY.HKEY_LOCAL_MACHINE )
            {
               hKey = Registry.LocalMachine.OpenSubKey( keyPath );
            }
            else
            {
               hKey = Registry.CurrentUser.OpenSubKey( keyPath );
            }

            // If the key is there, get the subkeys
            if ( hKey != null )
            {
               oValue = hKey.GetValue( iValue );
               return true;
            }
         }
         finally
         {
            // clean up
            if ( hKey != null )
            {
               hKey.Close();
            }
         }

         return false;

      }/* end method */

      #endregion

      //-----------------------------------------------------------------------

      #region aygshell method declarations

      /// <summary>
      /// This function can be used to take over certain areas of the screen. It is used 
      /// to modify the taskbar, Input Panel button, or Start menu icon.
      /// </summary>
      /// <param name="hwndRequester">Handle to the top-level window requesting the full-
      /// screen state</param>
      /// <param name="dwState">Specifies the state of the window.  See the SHFS struct</param>
      /// <returns>This function returns TRUE if it is successful and FALSE if it fails</returns>
      [DllImport( "aygshell.dll", SetLastError = true )]
      [return: MarshalAs( UnmanagedType.Bool )]
      public static extern bool SHFullScreen(
         IntPtr hwndRequester,
         SHFS dwState );


      /// <summary>
      /// This function can be used to get a handle to a menu bar window created with 
      /// SHCreateMenuBar
      /// </summary>
      /// <param name="hwnd">Handle to the window that contains the menu bar to be 
      /// found</param>
      /// <returns>This function returns a handle to the menu bar window for the 
      /// specified window</returns>
      [DllImport( "aygshell.dll" )]
      public extern static IntPtr SHFindMenuBar( IntPtr hwnd );

      #endregion aygshell method declarations

      //-----------------------------------------------------------------------

      #region Microsoft specific Bluetooth calls

      /// <summary>
      /// Returns the current BlueTooth hardware Stastus (works for 
      /// all Microsoft stack devices, including Intermec units)
      /// </summary>
      /// <param name="dwMode">returned hardware status</param>
      /// <returns>HRESULT (0 = S_OK, 1 = S_FALSE, 4 = E_FAIL)</returns>
      [DllImport( "Btdrt.dll" )]
      public static extern int BthGetHardwareStatus( out BTH_PI_STATUS dwMode );


      /// <summary>
      /// Returns the current BlueTooth Radio Mode (works for all Microsoft
      /// stack devices, including Intermec units)
      /// </summary>
      /// <param name="dwMode">current radio mode</param>
      /// <returns>HRESULT (0 = S_OK, 1 = S_FALSE, 4 = E_FAIL)</returns>
      [DllImport( "BthUtil.dll" )]
      public static extern int BthGetMode( out BTH_RADIO_MODE dwMode );


      /// <summary>
      /// Sets the Microsoft Radio Mode (i.e. it switches the BlueTooth on or off)
      /// </summary>
      /// <param name="dwMode">target radio mode</param>
      /// <returns>HRESULT (0 = S_OK, 1 = S_FALSE, 4 = E_FAIL)</returns>
      [DllImport( "BthUtil.dll" )]
      public static extern int BthSetMode( BTH_RADIO_MODE dwMode );

      #endregion

      //-----------------------------------------------------------------------

      #region Intermec specific Bluetooth Calls

      /// <summary>
      /// Switch on Intermec BlueTooth (overrides Microsoft RadioMode command)
      /// </summary>
      /// <returns>HRESULT (0 = S_OK, 1 = S_FALSE, 4 = E_FAIL)</returns>
      [DllImport( "ibt.dll", EntryPoint = "IBT_On" )]
      public static extern int IntermecBT_On();


      /// <summary>
      /// Switch off Intermec Bluetooth (overrides Microsoft RadioMode command)
      /// </summary>
      /// <returns>HRESULT (0 = S_OK, 1 = S_FALSE, 4 = E_FAIL)</returns>
      [DllImport( "ibt.dll", EntryPoint = "IBT_Off" )]
      public static extern int IntermecBT_Off();

      #endregion

      //-----------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
// 
//  Revision 1.0 APinkerton 18th November 2009
//  Added support for SystemTime, LocalTime as TimeZone management
//
//  Revision 0.0 APinkerton 16th March 2009
//  Add to project
//----------------------------------------------------------------------------