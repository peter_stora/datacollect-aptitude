﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class manages a Module under the HIVE
   /// Key HKEY_LOCAL_MACHINE.
   /// </summary>
   public class ModuleRegistryKey : RegistryBase, IModuleRegistryKey
   {
      #region Class Members

      /// <summary>
      /// Value for the name of the Module
      /// </summary>
      private const string VALUE_NAME = "Name";

      /// <summary>
      /// Value for the name of the actual assembly for the Module
      /// </summary>
      private const string VALUE_ASSEMBLY = "Assembly";

      /// <summary>
      /// Value for the launch arguments that might be needed
      /// </summary>
      private const string VALUE_LAUNCH_ARGS = "LaunchArgs";

      /// <summary>
      /// Value for the license for the Module
      /// </summary>
      private const string VALUE_LICENSE = "License";

      /// <summary>
      /// The branch name
      /// </summary>
      private string mBranch = KEY_ROOT_SKF + KEY_APP_NAME + "\\" + KEY_MODULES; // SOFTWARE\SKF\MicrologInspector\Modules

      /// <summary>
      /// The Module's subkey
      /// </summary>
      private string mModuleSubKey;

      #endregion Class Members
      

      #region Properties

      /// <summary>
      /// The SubKey of the Module
      /// </summary>
      public string ModuleSubKey 
      {
         get { return mModuleSubKey; }
      }


      /// <summary>
      /// The name of the Module
      /// </summary>
      public string Name 
      {
         get
         {
            string _value = "";
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                mBranch, mModuleSubKey, VALUE_NAME, ref _value );
            return _value;
         }
      }


      /// <summary>
      /// The name of the actual assembly for the Module
      /// </summary>
      public string AssemblyName 
      {
         get
         {
            string _value = "";
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                mBranch, mModuleSubKey, VALUE_ASSEMBLY, ref _value );
            return _value;
         }
      }


      /// <summary>
      /// The launch arguments that might be needed (bit wise or)
      /// </summary>
      public int LaunchArgs
      {
         get
         {
            int _value = 0;
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                mBranch, mModuleSubKey, VALUE_LAUNCH_ARGS, ref _value );
            return _value;
         }
      }


      /// <summary>
      /// The license for the Module
      /// </summary>
      public string License 
      {
         get
         {
            string _value = "";
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                mBranch, mModuleSubKey, VALUE_LICENSE, ref _value );
            return _value;
         }
      }

      #endregion Properties


      #region Constructor

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iModuleSubKeyName">Name of the Module's subkey</param>
      public ModuleRegistryKey( string iModuleSubKeyName )
      {
         mModuleSubKey = iModuleSubKeyName;

      }/* end constructor */

      #endregion Constructor

   } /* end class */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico 4th December 2009
//  Add to project
//----------------------------------------------------------------------------
