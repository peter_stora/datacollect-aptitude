﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
    /// <summary>
    /// Interface for direct Registry IO connection
    /// </summary>
    public interface IRegistryIO
    {
        /// <summary>
        /// Delete a single Registry key
        /// </summary>
        /// <param name="strKeyName">name of the key to delete</param>
        /// <returns>true on success</returns>
        bool DeleteKey(RegistryIO.RegistryHive iHive, string iBranch, string strKeyName);

        /// <summary>
        /// Read a System.bool from the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="oValue">returns boolean value</param>
        /// <returns>true on success</returns>
        bool GetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, ref bool oValue);

        /// <summary>
        /// Read a System.string  from the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="oValue">returnes string value</param>
        /// <returns>true on success</returns>
        bool GetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, ref string oValue);

        /// <summary>
        /// Read a System.int from the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="oValue">returns integer value</param>
        /// <returns>true on success</returns>
        bool GetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, ref int oValue);

        /// <summary>
        /// Write a System.int to the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iKeyName">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="iValue">integer value to write to registry</param>
        /// <returns>true on success</returns>
        bool SetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, int iValue);

        /// <summary>
        /// Write a System.string  to the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="iValue">string value to write to registry</param>
        /// <returns>true on success</returns>
        bool SetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, string iValue);

        /// <summary>
        /// Write a System.int to the registry
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="iValueName">The actual value name</param>
        /// <param name="iValue">integer value to write to registry</param>
        /// <returns>true on success</returns>
        bool SetValue(RegistryIO.RegistryHive iHive, string iBranch, 
            string iKeyName, string iValueName, bool iValue);

        /// <summary>
        /// Retrieves subkeys for a given key in a string array
        /// </summary>
        /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
        /// <param name="iBranch">The full Registry branch path to the Key</param>
        /// <param name="iKeyName">The name of the target Key</param>
        /// <param name="oSubKeys">Reference to an array to hold the subkeys</param>
        /// <returns>True on success</returns>
        bool GetSubKeyNames( RegistryIO.RegistryHive iHive, string iBranch,
           string iKeyName, ref string[] oSubKeys );
    
    }/* end interface */


}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2009
//  Add to project
//----------------------------------------------------------------------------

