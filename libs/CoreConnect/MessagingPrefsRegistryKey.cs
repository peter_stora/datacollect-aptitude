﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This Class Manages the OperatorSettings\MessagingPreferences Registry Key
   /// </summary>
   public class MessagingPrefsRegistryKey : RegistryBase, IMessagingPrefsRegistryKey
   {
      private const string VALUE_EOR                        = "EndOfRoute";
      private const string VALUE_FOUND_IN_OTHER             = "FoundInOtherRoute";
      private const string VALUE_SAVE_DATA_AS_CURRENT       = "SaveDataAsCurrent";
      private const string VALUE_SHOW_SETS_WHILE_COLLECTING = "ShowSetsWhileCollecting";
      private const string VALUE_ZERO_ACCELERATION          = "ZeroAcceleration";

      private string mBranch = KEY_ROOT_SKF + KEY_APP_NAME + "\\" + KEY_OPERATOR_SETTINGS;


      /// <summary>
      /// Get or Set whether the operator should see the "End of Route" message
      /// </summary>
      public bool EndOfRoute
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MSG_PREFERENCES, VALUE_EOR, value );
         }
         get
         {
            try
            {
               bool _value = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MSG_PREFERENCES, VALUE_EOR, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return true;
            }
         }
      }


      /// <summary>
      /// Get or Set whether the operator should see the "Found in other Route" message
      /// </summary>
      public bool FoundInOtherRoute
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MSG_PREFERENCES, VALUE_FOUND_IN_OTHER, value );
         }
         get
         {
            try
            {
               bool _value = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MSG_PREFERENCES, VALUE_FOUND_IN_OTHER, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return true;
            }
         }
      }


      /// <summary>
      /// Get or Set whether the operator should see the "save as current" message
      /// </summary>
      public bool SaveDataAsCurrent
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MSG_PREFERENCES, VALUE_SAVE_DATA_AS_CURRENT, value );
         }
         get
         {
            try
            {
               bool _value = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MSG_PREFERENCES, VALUE_SAVE_DATA_AS_CURRENT, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return true;
            }            
         }
      }


      /// <summary>
      /// Get or Set whether the operator should see sets while collecting Route data
      /// </summary>
      public bool ShowSetsWhileCollecting
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MSG_PREFERENCES, VALUE_SHOW_SETS_WHILE_COLLECTING, value );
         }
         get
         {
            try
            {
               bool _value = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MSG_PREFERENCES, VALUE_SHOW_SETS_WHILE_COLLECTING, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return true;
            }            
         }
      }


      /// <summary>
      /// Get or Set whether to display message when WMCD returns zero Accl value
      /// </summary>
      public bool ShowZeroAcclWarning
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MSG_PREFERENCES, VALUE_ZERO_ACCELERATION, value );
         }
         get
         {
            try
            {
               bool _value = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MSG_PREFERENCES, VALUE_ZERO_ACCELERATION, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return true;
            }            
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 21st June 2009
//  Add to project
//----------------------------------------------------------------------------