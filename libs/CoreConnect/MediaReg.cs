﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Registry access for Media settings
   /// </summary>
   class MediaReg : RegistryBase, IMediaReg
   {
      private const string KEY_MEDIA            = "Media";
      private const string VALUE_CAMERA_TYPE    = "CameraType";
      private const string VALUE_CAMERA_RES_ID  = "CameraResolution";
      private const string VALUE_SAVED_MEDIA    = "SavedMedia";
      private const string VALUE_IMAGER_TYPE    = "ImagerType";
      private const string VALUE_FLASH_TYPE     = "FlashType";
      
      private const string BRANCH = KEY_ROOT_SKF + KEY_APP_NAME;

      /// <summary>
      /// Gets or sets the camera type.  See Camera.CameraTypes
      /// </summary>
      public int CameraType
      {
         get
         {
            try
            {
               // set to unknown, see Camera.CameraTypes
               var cameraType = 0;


               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   BRANCH, KEY_MEDIA, VALUE_CAMERA_TYPE, ref cameraType );

               // and return the value
               return cameraType;

            }
            catch( Exception )
            {
               // Camera.CameraTypes.Unknown
               return 0;
            }
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               BRANCH, KEY_MEDIA, VALUE_CAMERA_TYPE, value );
         }
      }


      /// <summary>
      /// Gets or sets the camera resolution ID
      /// </summary>
      public int CameraResolution
      {
         get
         {
            try
            {
               // set to unknown
               var resolutionId = 0;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   BRANCH, KEY_MEDIA, VALUE_CAMERA_RES_ID, ref resolutionId );

               // and return the value
               return resolutionId;

            }
            catch( Exception )
            {
               return 0;
            }
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               BRANCH, KEY_MEDIA, VALUE_CAMERA_RES_ID, value );
         }
      }


      /// <summary>
      /// Gets or sets the saved media location
      /// </summary>
      public string SavedMedia
      {
         get
         {
            var path = string.Empty;

            // lets see what's in the registry
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                BRANCH, KEY_MEDIA, VALUE_SAVED_MEDIA, ref path );

            // and return the value
            return path;
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               BRANCH, KEY_MEDIA, VALUE_SAVED_MEDIA, value );
         }
      }

      /// <summary>
      /// Gets or sets the Imager type.  See Camera.ImagerTypes
      /// </summary>
      public int ImagerType
      {
         get
         {
            try
            {
               // set to Unknown
               var imageType = 0;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   BRANCH, KEY_MEDIA, VALUE_IMAGER_TYPE, ref imageType );

               // and return the value
               return imageType;

            }
            catch( Exception )
            {
               return 0;
            }
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               BRANCH, KEY_MEDIA, VALUE_IMAGER_TYPE, value );
         }
      }

      /// <summary>
      /// Gets or sets the flash type.  See Camera.FlashTypes
      /// </summary>
      public int FlashType
      {
         get
         {
            try
            {
               var result = 0;
               
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   BRANCH, KEY_MEDIA, VALUE_FLASH_TYPE, ref result );

               return result;
            }
            catch( Exception )
            {
               return 0;
            }
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               BRANCH, KEY_MEDIA, VALUE_FLASH_TYPE, value );
         }
      }
   }
}
