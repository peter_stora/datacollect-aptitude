﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class is used to manipulate the device screen (i.e. going fullscreen,
   /// hiding the Start icon, and hiding the MainMenu
   /// </summary>
   public static class DeviceScreen
   {
      #region Public Methods

      /// <summary>
      /// Hides the start menu.  See Show_Start_Menu() to show the start menu
      /// </summary>
      /// <param name="iWindowName">Use the Form's Text memeber (this.Text)</param>
      /// <returns>True if successfull, else false</returns>
      public static bool Hide_Start_Menu( string iWindowName )
      {
         return Do_SHFullScreen( WinCE.SHFS.HIDESTARTICON, iWindowName );

      }/* end method */

      
      /// <summary>
      /// Shows the start menu.  See Hide_Start_Menu() to hide the start menu
      /// </summary>
      /// <param name="iWindowName">Use the Form's Text memeber (this.Text)</param>
      /// <returns>True if successfull, else false</returns>
      public static bool Show_Start_Menu( string iWindowName )
      {
         return Do_SHFullScreen( WinCE.SHFS.SHOWSTARTICON, iWindowName );

      }/* end method */


      /// <summary>
      /// Hides the SIP button on the Main Menu.  See Show_SIP_Button() to show the
      /// SIP button
      /// </summary>
      /// <param name="iWindowName">Use the Form's Text memeber (this.Text)</param>
      /// <returns>True if successfull, else false</returns>
      public static bool Hide_SIP_Button( string iWindowName )
      {
         return Do_SHFullScreen( WinCE.SHFS.HIDESIPBUTTON, iWindowName );

      }/* end method */


      /// <summary>
      /// Shows the SIP button on the Main Menu.  See Hide_SIP_Button() to hide the
      /// SIP button
      /// </summary>
      /// <param name="iWindowName">Use the Form's Text memeber (this.Text)</param>
      /// <returns>True if successfull, else false</returns>
      public static bool Show_SIP_Button( string iWindowName )
      {
         return Do_SHFullScreen( WinCE.SHFS.SHOWSIPBUTTON, iWindowName );

      }/* end method */


      /// <summary>
      /// Hides the Main Menu
      /// </summary>
      /// <param name="iWindowName">Use the Form's Text memeber (this.Text)</param>
      /// <returns>True if successfull, else false</returns>
      public static bool Hide_Main_Menu( string iWindowName )
      {
         return Hide_Main_Menu( WinCE.ShowState.SW_HIDE, iWindowName );
      }

      #endregion Public Methods


      #region Private Methods

      /// <summary>
      /// Show or Hide a particular item on the device screen
      /// </summary>
      /// <param name="iState">See WinCE.SHFS</param>
      /// <param name="iWindowName">Form's window name</param>
      /// <returns>True if successfull, else false</returns>
      private static bool Do_SHFullScreen( WinCE.SHFS iState, string iWindowName )
      {
         bool result = false;

         try
         {
            IntPtr hWnd = WinCE.FindWindowCE( null, iWindowName );

            if( hWnd != IntPtr.Zero )
            {
               result = WinCE.SHFullScreen( hWnd, iState );
            }
         }
         catch( Exception )
         {
            result = false;
         }

         return result;

      }/* end method */


      /// <summary>
      /// Sets the MainMenu's show state
      /// </summary>
      /// <param name="iState">See WinCE.ShowState</param>
      /// <param name="iWindowName">Form's window name</param>
      /// <returns>True if successfull, else false</returns>
      private static bool Hide_Main_Menu( WinCE.ShowState iState, string iWindowName )
      {
         bool result = false;

         try
         {
            IntPtr hWnd = WinCE.FindWindowCE( null, iWindowName );

            if( hWnd != IntPtr.Zero )
            {
               IntPtr hWndMainMenu = WinCE.SHFindMenuBar( hWnd );

               if( hWndMainMenu != IntPtr.Zero )
               {
                  return WinCE.ShowWindow( hWndMainMenu, (int) iState );
               }
            }
         }
         catch( Exception )
         {
            result = false;
         }

         return result;

      }/* end method */

      #endregion Private Methods

   } /* end class */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico 15th Jan 2010
//  Add to project
//----------------------------------------------------------------------------
