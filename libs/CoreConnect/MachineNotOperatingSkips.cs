﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class manages the reading and writing of the "Machine Not Operating" 
   /// skip collection settings to the Registry (under the Key OperatorSettings)
   /// </summary>
   public class MachineNotOperatingSkips : BaseMachineSkips, IMachineNotOperatingSkips
   {
      /// <summary>
      /// Get or Set whether an operator pressing the "Machine Not Operating" 
      /// button would result in all MCD only Points being skipped
      /// </summary>
      public Boolean SkipMCDPoints
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_MCD, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_MCD, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Get or Set whether an operator pressing the "Machine Not Operating" 
      /// button would result in all Process only Points being skipped
      /// </summary>
      public Boolean SkipProcessPoints
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_PROCESS, value );
         }
         get
         {            
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_PROCESS, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Get or Set whether an operator pressing the "Machine Not Operating" 
      /// button would result in all Inspection only Points being skipped
      /// </summary>
      public Boolean SkipInspectionPoints
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_INSPECTION, value );
         }
         get
         {           
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_MACHINE_NOP_SKIPS, VALUE_SKIP_INSPECTION, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return false;
            }            
         }
      }


      /// <summary>
      /// Overall check as to whether an operator on will have the "Machine OK" 
      /// (skip option) button enabled. The value of this property is derived 
      /// from the other three conditions, where if any single condition is set
      /// true, this property is true
      /// </summary>
      public Boolean SkipEnabled
      {
         get
         {
            if ( SkipMCDPoints || SkipProcessPoints || SkipInspectionPoints )
            {
               return true;
            }
            else
            {
               return false;
            }
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 21st June 2009
//  Add to project
//----------------------------------------------------------------------------