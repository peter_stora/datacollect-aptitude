﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public class DeviceTimeManager : IDeviceTimeManager
   {
      // create a new system time object
      private static WinCE.SystemTime sTimeStruct = new WinCE.SystemTime();
      

      /// <summary>
      /// Get the Local time on the device. Note this is primarily
      /// a test method to validate the Interop time methods. Ordinarily
      /// this value could be returned via 'DateTime.Now'
      /// </summary>
      /// <returns>Current local time</returns>
      public static DateTime GetLocalTime()
      {
         // firstly try and get the WinCE time
         try
         {
            WinCE.WinCEGetLocalTime( ref sTimeStruct );
         }
         // if that fails get the Win32 time (test only)
         catch
         {
            return DateTime.Now;
         }

         DateTime localTime = new DateTime(
            sTimeStruct.Year,
            sTimeStruct.Month,
            sTimeStruct.Day,
            sTimeStruct.Hour,
            sTimeStruct.Minute,
            sTimeStruct.Second,
            sTimeStruct.Millisecond );

         return localTime;

      }/* end method */


      /// <summary>
      /// Get the Local time on the device. Note this is primarily
      /// a test method to validate the Interop time methods. Ordinarily
      /// this value could be returned via 'DateTime.Now'
      /// </summary>
      /// <returns>Current local time</returns>
      public DateTime GetDeviceLocalTime()
      {
         return GetLocalTime();
      }/* end method */


      /// <summary>
      /// Get the System time on the device. Note this is primarily
      /// a test method to validate the Interop time methods. Ordinarily
      /// this value could be returned via 'DateTime.Now'
      /// </summary>
      /// <returns>Current system time</returns>
      public DateTime GetDeviceSystemTime()
      {
         // create a new system time object
         WinCE.SystemTime stime = new WinCE.SystemTime();

         // firstly try and get the WinCE time
         try
         {
            WinCE.WinCEGetSystemTime( ref stime );
         }
         // if that fails get the Win32 time (test only)
         catch
         {
            try
            {
               WinCE.Win32GetSystemTime( ref stime );
            }
            catch
            {
               return DateTime.Now;
            }
         }

         DateTime systemTime = new DateTime(
            stime.Year,
            stime.Month,
            stime.Day,
            stime.Hour,
            stime.Minute,
            stime.Second,
            stime.Millisecond );

         return systemTime;

      }/* end method */


      /// <summary>
      /// Sets the system date on a CE Device
      /// </summary>
      /// <param name="iNewTime">Updated time settings</param>
      /// <returns>true on success</returns>
      public Boolean SetDeviceSystemTime( DateTime iNewTime )
      {
         WinCE.SystemTime newtime = new WinCE.SystemTime();

         newtime.Year = (ushort) iNewTime.Year;
         newtime.Month = (ushort) iNewTime.Month;
         newtime.Day = (ushort) iNewTime.Day;
         newtime.Hour = (ushort) iNewTime.Hour;
         newtime.Minute = (ushort) iNewTime.Minute;
         newtime.Second = (ushort) iNewTime.Second;
         newtime.Millisecond = (ushort) iNewTime.Millisecond;

         try
         {
            return WinCE.WinCESetSystemTime( ref newtime );
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Sets the local date time on a CE Device
      /// </summary>
      /// <param name="iNewTime">Updated time settings</param>
      /// <returns>true on success</returns>
      public Boolean SetDeviceLocalTime( DateTime iNewTime )
      {
         WinCE.SystemTime newtime = new WinCE.SystemTime();

         newtime.Year = (ushort) iNewTime.Year;
         newtime.Month = (ushort) iNewTime.Month;
         newtime.Day = (ushort) iNewTime.Day;
         newtime.Hour = (ushort) iNewTime.Hour;
         newtime.Minute = (ushort) iNewTime.Minute;
         newtime.Second = (ushort) iNewTime.Second;
         newtime.Millisecond = (ushort) iNewTime.Millisecond;

         try
         {
            return WinCE.WinCESetLocalTime( ref newtime );
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Get the device's current timezone information
      /// </summary>
      /// <returns>TimeZoneInformation object</returns>
      public IDeviceTimeZoneInformation GetTimeZoneInformation()
      {
         // create a new timezone structure
         WinCE.TimeZoneInformation zoneInfo = new WinCE.TimeZoneInformation();

         Boolean result = true;

         // get the device timezone structure
         try
         {
            WinCE.WinCEGetTimeZoneInformation( out zoneInfo );
         }
         catch
         {
            try
            {
               WinCE.Win32GetTimeZoneInformation( out zoneInfo );
            }
            catch
            {
               result = false;
            }
         }

         if ( result )
         {
            IDeviceTimeZoneInformation deviceZoneInfo = 
               new DeviceTimeZoneInformation( zoneInfo );
            return deviceZoneInfo;
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Update the device's timezone information
      /// </summary>
      /// <param name="iDevZoneInfo">new timezone object</param>
      /// <returns>true on success, else false</returns>
      public Boolean SetTimeZoneInformation( IDeviceTimeZoneInformation iDevZoneInfo )
      {
         var result = false;
         //
         // convert the DeviceTimeZoneInfo object into a populated 
         // TimeZoneInformation structure
         //
         WinCE.TimeZoneInformation timeZoneInfo = 
            iDevZoneInfo.GetTimeZoneStructure();

         try
         {
            // set the new timezone info (CE only)
            if( WinCE.WinCESetTimeZoneInformation( ref timeZoneInfo ) > 0 )
            {
               result = true;
            }

         }
         catch(Exception)
         {
            result = false;
         }

         // if we reach here we've failed, so return false
         return result;

      }/* end method */


      /// <summary>
      /// Does a reference date fall under standard or summer time?
      /// </summary>
      /// <param name="iDevZoneInfo">TimeZone information</param>
      /// <param name="iRef">Date and Time to reference</param>
      /// <returns></returns>
      public Boolean IsDaylightSavingTime( IDeviceTimeZoneInformation iZoneInfo, DateTime iRef )
      {
         Boolean result = false;

         if ( ( iZoneInfo.DSTMonth > 0 ) & ( iZoneInfo.DSTDay > 0 ) )
         {
            // get a datetime of the start of daylight saving
            DateTime dst = new DateTime(
               iRef.Year,
               iZoneInfo.DSTMonth,
               iZoneInfo.DSTDay,
               iZoneInfo.DSTHour,
               0, 0 );

            // get a datetime for the end of daylight saving
            DateTime st = new DateTime(
               iRef.Year,
               iZoneInfo.STMonth,
               iZoneInfo.STDay,
               iZoneInfo.STHour,
               0, 0 );

            //
            // we only need to check for changes in seasonal time offsets
            // if the daylight bias and standard bias are not equal
            //
            if ( iZoneInfo.DaylightBias != iZoneInfo.StandardBias )
            {
               //
               // for northern hemisphere timezone regions the StandardTime
               // start month will fall after the DaylightSaving start month
               //
               if ( iZoneInfo.STMonth > iZoneInfo.DSTMonth )
               {
                  if ( ( iRef.Ticks >= dst.Ticks ) & ( iRef.Ticks < st.Ticks ) )
                  {
                     return true;
                  }
               }

               //
               // for southern hemisphere timezone regions the DaylightSaving
               // start month will fall after the StandardTime start month
               //
               else if ( iZoneInfo.STMonth < iZoneInfo.DSTMonth )
               {
                  if ( ( iRef.Ticks <= st.Ticks ) | ( iRef.Ticks > dst.Ticks ) )
                  {
                     return true;
                  }
               }
            }
         }

         return result;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 18th November 2009
//  Add to project
//----------------------------------------------------------------------------
