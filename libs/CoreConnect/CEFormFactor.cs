﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Windows Mobile form styles
   /// </summary>
   public enum FormStyle
   {
      /// <summary>
      /// Unknown and/or unsupported screen size
      /// </summary>
      Unknown,

      /// <summary>
      /// Standard resolution (240x320 at 96 DPI) device 
      /// screen size (Intermec CN3e)
      /// </summary>
      QVGA,

      /// <summary>
      /// Extended resolution (240x400 at 96 DPI) device 
      /// screen size, primarily for phones
      /// </summary>
      WQVGA,

      /// <summary>
      /// Compact standard resolution (240x240 at 96 DPI) device
      /// screen size, primarily for phones
      /// * NOT SUPPORTED *
      /// </summary>
      SquareQVGA,

      /// <summary>
      /// High resolution (480x640 at 192 DPI) device 
      /// screen size (Intermec CN7e, Motorola MC75)
      /// </summary>
      VGA,

      /// <summary>
      /// Extended high resolution (480x800 at 192 DPI) device 
      /// screen size, primarily for phones (Phone 7)
      /// </summary>
      WVGA,

      /// <summary>
      /// Compact high resolution (480x480 at 192 DPI) device
      /// screen size, primarily for phones (Palm\Motorola)
      /// * NOT SUPPORTED *
      /// </summary>
      SquareVGA,
   }


   /// <summary>
   /// Supported screen resolutions
   /// </summary>
   public enum Resolution
   {
      DPI_96 = 0x60,
      DPI_192 = 0xC0
   }


   /// <summary>
   /// Static class that provides information on the screen size
   /// and form factor of the hosting device
   /// </summary>
   public static class CEFormFactor
   {
      /// <summary>
      /// Current form factor
      /// </summary>
      private static FormStyle sFormFactor = FormStyle.QVGA;

      /// <summary>
      /// Is current form factor supported
      /// </summary>
      private static Boolean sSupported = true;

      /// <summary>
      /// Current DPI
      /// </summary>
      private static double sDPI = 96.0;

      /// <summary>
      /// Full form height
      /// </summary>
      private static int sHeight;

      /// <summary>
      /// Full form width
      /// </summary>
      private static int sWidth;

      /// <summary>
      /// Corrected form height
      /// </summary>
      private static int sWorkingHeight;
      
      /// <summary>
      /// Corrected form width
      /// </summary>
      private static int sWorkingWidth;

      /// <summary>
      /// DPI Scaling factor
      /// </summary>
      private static int sScaleFactor;


      /// <summary>
      /// Determine the form factor for this PDA based on the screen
      /// resolution (in DPI) as well as the width and height
      /// </summary>
      /// <param name="iWidth">screen width</param>
      /// <param name="iHeight">screen height</param>
      /// <param name="iDPI">resolution in DPI</param>
      /// <returns>true if format is supported</returns>
      public static Boolean SetFactor( int iWidth, int iHeight,
         int iWorkingWidth, int iWorkingHeight, int iDPI )
      {
         sSupported = true;
         sDPI = iDPI;

         // for 96 DPI
         if ( iDPI == (int) Resolution.DPI_96 )
         {
            // standard resolution QVGA
            if ( iWidth == 240 & iHeight == 320 )
            {
               sFormFactor = FormStyle.QVGA;
               sSupported = true;
            }
            // standard resolution extended(Wide)QVGA
            else if ( iWidth == 240 & iHeight == 400 )
            {
               sFormFactor = FormStyle.WQVGA;
               sSupported = true;
            }
            // standard resolution square
            else if ( iWidth == 240 & iHeight == 240 )
            {
               sFormFactor = FormStyle.SquareQVGA;
               sSupported = false;
            }
            // unknown format
            else
            {
               sFormFactor = FormStyle.Unknown;
               sSupported = false;
            }
         }

         // for 192 DPI
         else if ( iDPI == (int) Resolution.DPI_192 )
         {
            // high resolution VGA  
            if ( iWidth == 480 & iHeight == 640 )
            {
               sFormFactor = FormStyle.VGA;
               sSupported = true;
            }
            // high resolution extended(Wide)VGA
            else if ( iWidth == 480 & iHeight == 800 )
            {
               sFormFactor = FormStyle.WVGA;
               sSupported = true;
            }
            // high resolution square
            else if ( iWidth == 480 & iHeight == 480 )
            {
               sFormFactor = FormStyle.SquareVGA;
               sSupported = false;
            }
            // unknown
            else
            {
               sFormFactor = FormStyle.Unknown;
               sSupported = false;
            }
         }

         else
         {
            sFormFactor = FormStyle.Unknown;
            sSupported = false;
         }

         if ( Supported )
         {
            sScaleFactor = Convert.ToInt32( iDPI / 96.0 );
         }
         else
         {
            sScaleFactor = 1;
         }

         sHeight = iHeight / sScaleFactor;
         sWidth = iWidth / sScaleFactor;
         sWorkingHeight = iWorkingHeight / sScaleFactor;
         sWorkingWidth = iWorkingWidth / sScaleFactor;

         // return the result
         return sSupported;

      }/* end method */


      /// <summary>
      /// Gets the current form factor
      /// </summary>
      public static FormStyle FormFactor
      {
         get
         {
            return sFormFactor;
         }
      }


      /// <summary>
      /// Gets the corrected working area width
      /// </summary>
      public static int WorkingAreaWidth
      {
         get
         {
            return sWorkingWidth;
         }
      }


      /// <summary>
      /// Gets the corrected wrking area height 
      /// </summary>
      public static int WorkingAreaHeight
      {
         get
         {
            return sWorkingHeight;
         }
      }


      /// <summary>
      /// Gets the corrected form width
      /// </summary>
      public static int FormWidth
      {
         get
         {
            return sWidth;
         }
      }


      /// <summary>
      /// Gets the corrected form height 
      /// </summary>
      public static int FormHeight
      {
         get
         {
            return sHeight;
         }
      }


      /// <summary>
      /// Gets the current screen resolution
      /// </summary>
      public static Double DotsPerInch
      {
         get
         {
            return sDPI;
         }
      }


      /// <summary>
      /// Gets the form scaling factor relative to 96 DPI
      /// </summary>
      public static int DpiScaling
      {
         get
         {
            return sScaleFactor;
         }
      }


      /// <summary>
      /// Gets whether the current form factor is supported
      /// </summary>
      public static Boolean Supported
      {
         get
         {
            return sSupported;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 4th February 2011
//  Add to project
//----------------------------------------------------------------------------
