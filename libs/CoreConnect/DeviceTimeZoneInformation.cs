﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class represents the Windows 'Struct' TimeZoneInfo
   /// </summary>
   public class DeviceTimeZoneInformation : IDeviceTimeZoneInformation
   {
      #region Private fields

      private int mBias;
      private int mStandardBias;
      private int mDaylightBias;
      private int mDSTHour;
      private int mDSTDay;
      private int mDSTMonth;
      private int mSTHour;
      private int mSTDay;
      private int mSTMonth;
      private int mCurrentYear;
      private string mStandardName;
      private string mDaylightName;

      #endregion

      /// <summary>
      /// Base Constructor
      /// </summary>
      public DeviceTimeZoneInformation()
      {
      }/* end constructor */


      /// <summary>
      /// Overload Constructor
      /// </summary>
      /// <param name="iZoneInfo"></param>
      public DeviceTimeZoneInformation( WinCE.TimeZoneInformation iZoneInfo )
      {
         PopulateObject( iZoneInfo );
      }/* end constructor */


      /// <summary>
      /// Gets or sets the start of daylight saving changeover hour
      /// </summary>
      public int DSTHour
      {
         get
         {
            return mDSTHour;
         }
         set
         {
            mDSTHour = value;
         }
      }


      /// <summary>
      /// Gets or sets the start of daylight saving changeover Day
      /// </summary>
      public int DSTDay
      {
         get
         {
            return mDSTDay;
         }
         set
         {
            mDSTDay = value;
         }
      }


      /// <summary>
      /// Gets or sets the start of daylight saving changeover Month
      /// </summary>
      public int DSTMonth
      {
         get
         {
            return mDSTMonth;
         }
         set
         {
            mDSTMonth = value;
         }
      }


      /// <summary>
      /// Gets or sets the start of standard time changeover Hour
      /// </summary>
      public int STHour
      {
         get
         {
            return mSTHour;
         }
         set
         {
            mSTHour = value;
         }
      }


      /// <summary>
      /// Gets or sets the start of standard time changeover Day
      /// </summary>
      public int STDay
      {
         get
         {
            return mSTDay;
         }
         set
         {
            mSTDay = value;
         }
      }


      /// <summary>
      /// Gets or sets the start of standard time changeover Month
      /// </summary>
      public int STMonth
      {
         get
         {
            return mSTMonth;
         }
         set
         {
            mSTMonth = value;
         }
      }


      /// <summary>
      /// Gets or sets the current Year
      /// </summary>
      public int CurrentYear
      {
         get
         {
            return mCurrentYear;
         }
         set
         {
            mCurrentYear = value;
         }
      }


      /// <summary>
      /// Specifies the current bias, in minutes, for local time translation 
      /// on this computer. The bias is the difference, in minutes, between 
      /// UTC and local time (so UTC = local time + bias)
      /// </summary>
      public int Bias
      {
         get
         {
            return mBias;
         }
         set
         {
            mBias = value;
         }
      }


      /// <summary>
      /// Specifies a null-terminated string associated with standard 
      /// time on this operating system. For example, this member could 
      /// contain EST to indicate Eastern Standard Time.
      /// This string is not used by the operating system, so anything 
      /// stored there by using the SetTimeZoneInformation function is 
      /// returned unchanged by the GetTimeZoneInformation function.
      /// This string can be empty. 
      /// </summary>
      public string StandardName
      {
         get
         {
            return mStandardName;
         }
         set
         {
            mStandardName = value;
         }
      }


      /// <summary>
      /// Specifies a bias value to be used during local time translations 
      /// that occur during standard time. This member is ignored if a value 
      /// for the StandardDate member is not supplied.
      /// This value is added to the value of the Bias member to form the 
      /// bias used during standard time. In most time zones, the value of 
      /// this member is zero. 
      /// </summary>
      public int StandardBias
      {
         get
         {
            return mStandardBias;
         }
         set
         {
            mStandardBias = value;
         }
      }


      /// <summary>
      /// Specifies a null-terminated string associated with daylight time 
      /// on this operating system. For example, this member could contain 
      /// PDT to indicate Pacific Daylight Time. This string is not used by 
      /// the operating system, so anything stored there by using the 
      /// "SetTimeZoneInformation" function is returned unchanged by the 
      /// "GetTimeZoneInformation" function. This string can be empty. 
      /// </summary>
      public string DaylightName
      {
         get
         {
            return mDaylightName;
         }
         set
         {
            mDaylightName = value;
         }
      }


      /// <summary>
      /// Specifies a bias value to be used during local time translations 
      /// that occur during daylight time. This member is ignored if a value 
      /// for the DaylightDate member is not supplied. This value is added 
      /// to the value of the Bias member to form the bias used during daylight 
      /// time. In most time zones, the value of this member is –60. 
      /// </summary>
      public int DaylightBias
      {
         get
         {
            return mDaylightBias;
         }
         set
         {
            mDaylightBias = value;
         }
      }


      /// <summary>
      /// Populate all fields in this object using a TimeZoneInformation struct
      /// </summary>
      /// <param name="iZoneInfo">Populated TimeZoneInformation struct</param>
      public void PopulateObject( WinCE.TimeZoneInformation iZoneInfo )
      {
         mBias = iZoneInfo.Bias;

         mDaylightBias = iZoneInfo.DaylightBias;
         mDaylightName = iZoneInfo.DaylightName;
         mStandardBias = iZoneInfo.StandardBias;
         mStandardName = iZoneInfo.StandardName;

         mDSTHour = iZoneInfo.DaylightDate.Hour;
         mDSTDay = iZoneInfo.DaylightDate.Day;
         mDSTMonth = iZoneInfo.DaylightDate.Month;

         mSTHour = iZoneInfo.StandardDate.Hour;
         mSTDay = iZoneInfo.StandardDate.Day;
         mSTMonth = iZoneInfo.StandardDate.Month;

      }/* end method */


      /// <summary>
      /// Returns all object property fields as a TimeZoneInfo structure 
      /// </summary>
      /// <returns>Populated TimeZoneInfo Structure</returns>
      public WinCE.TimeZoneInformation GetTimeZoneStructure()
      {
         WinCE.TimeZoneInformation timeZoneInfo = 
            new WinCE.TimeZoneInformation();

         timeZoneInfo.Bias = mBias;
         timeZoneInfo.DaylightBias = mDaylightBias;
         timeZoneInfo.DaylightName = mDaylightName;
         timeZoneInfo.StandardBias = mStandardBias;
         timeZoneInfo.StandardName = mStandardName;

         timeZoneInfo.DaylightDate.Hour   = (ushort)mDSTHour;
         timeZoneInfo.DaylightDate.Day    = (ushort)mDSTDay;
         timeZoneInfo.DaylightDate.Month  = (ushort)mDSTMonth;
         timeZoneInfo.DaylightDate.Year   = (ushort) mCurrentYear;
         timeZoneInfo.DaylightDate.Minute = 0;
         timeZoneInfo.DaylightDate.Second = 0;
         timeZoneInfo.DaylightDate.Millisecond = 0;
         
         timeZoneInfo.StandardDate.Hour   = (ushort)mSTHour;
         timeZoneInfo.StandardDate.Day    = (ushort)mSTDay;
         timeZoneInfo.StandardDate.Month  = (ushort)mSTMonth;
         timeZoneInfo.StandardDate.Year   = (ushort) mCurrentYear;
         timeZoneInfo.StandardDate.Minute = 0;
         timeZoneInfo.StandardDate.Second = 0;
         timeZoneInfo.StandardDate.Millisecond = 0;
         
         return timeZoneInfo;

      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 11th March 2010
//  Fix bug that confused DSTHour and STHour - causing the Core.dll WinCE
//  method SetTimeZoneInfo() to fail
//
//  Revision 0.0 APinkerton 18th November 2009
//  Add to project
//----------------------------------------------------------------------------