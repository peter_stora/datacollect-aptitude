﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public interface IRegistryConnect
   {
      /// <summary>
      /// Get or Set the Current User Registry Settings
      /// </summary>
      IOperatorRegistryKey Operators
      {
         get;
      }

      /// <summary>
      /// Gets the current Media registry setting
      /// </summary>
      IMediaReg Media
      {
         get;
      }

      /// <summary>
      /// Get the Device UUID
      /// </summary>
      string DeviceUID
      {
         get;
      }

      /// <summary>
      /// Get or set the Firmware Version
      /// </summary>
      string FirmwareVersion
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Middleware Server's HostName
      /// </summary>
      string HostName
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the "USE IP ADDRESS" flag
      /// </summary>
      string IPAddress
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Server type
      /// </summary>
      string ServerType
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the Middleware Server's Open Port Number
      /// </summary>
      int PortNumber
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the "USE IP ADDRESS" flag
      /// </summary>
      bool UseIPAddress
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the "Has Connected" flag
      /// </summary>
      Boolean HasConnected
      {
         get;
         set;
      }

      /// <summary>
      /// Get or set the "USE ANALYST CMMS" flag
      /// </summary>
      bool UseAnalystCMMS
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Device Serial No (or Friendly name)
      /// </summary>
      string DeviceSerialNo
      {
         get;
         set;
      }

      /// <summary>
      /// Get the default device name
      /// </summary>
      string DefaultSerialNo
      {
         get;
      }

      /// <summary>
      /// Get the default port number
      /// </summary>
      UInt16 DefaultportNo
      {
         get;
      }

      /// <summary>
      /// Gets or sets the ID of any natively supported Barcode engine
      /// </summary>
      ScanningCtl.BarcodeEngine Barcode
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the RFID engine type
      /// </summary>
      ScanningCtl.RFIDEngine RFID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether or not RFID is enabled
      /// </summary>
      bool RFIDEnabled
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the RFID Tag Type
      /// </summary>
      int RFIDTagType
      {
         get;
         set;
      }

      /// <summary>
      /// Get or sets the current BlueTooth Stack type.
      /// </summary>
      BlueToothCtl.DeviceStack BTDeviceStack
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the WMCD Bluetooth Address
      /// </summary>
      byte[] WMCD_BTAddress
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the RFID Gun Bluetooth Address
      /// </summary>
      byte[] RFID_BTAddress
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Temperature Gun Bluetooth Address
      /// </summary>
      byte[] TempGun_BTAddress
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Temperature Gun Bluetooth Name
      /// </summary>
      string TempGun_BTName
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the external vibration sensor type
      /// </summary>
      ExternalSensor.Vibration ExtVibSensor
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the external temperature sensor type
      /// </summary>
      ExternalSensor.Temperature ExtTempSensor
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the default localhost name
      /// </summary>
      string LocalHostName
      {
         get;
      }

      /// <summary>
      /// Gets the default localhost IP address
      /// </summary>
      string LocalHostIP
      {
         get;
      }

      /// <summary>
      /// Gets or sets whether the comms should use the actual
      /// UID of the device, or a fixed 'test' reference UID
      /// </summary>
      Boolean UseTestID
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Use Debug Mode flag
      /// </summary>
      Boolean UseDebug
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets whether the handheld should synch on a cradle detect
      /// </summary>
      Boolean CradleDetect
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the frequency units to use when viewing FFTs
      /// </summary>
      int FFTFreqUnits
      {
         get;
         set;
      }

      /// <summary>
      /// Gets the override to disable the marquee text (OTD# 4712 hack)
      /// </summary>
      bool DisableMarquee
      {
         get;
      }

      /// <summary>
      /// Gets the override to enable the Search feature
      /// </summary>
      bool EnableSearch
      {
         get;
      }

      /// <summary>
      /// This method validates all SKF keys within HKLM
      /// </summary>
      /// <returns>true if OK, error on fail</returns>
      Boolean ValidateLocalMachineHive();

      /// <summary>
      /// Build the current user Default Registry Hive
      /// </summary>
      void BuildCurrentUserHive();

      /// <summary>
      /// Build the Default Hardware Registry Hive
      /// </summary>
      void BuildLocalMachineHive();
   }

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2009
//  Add to project
//----------------------------------------------------------------------------