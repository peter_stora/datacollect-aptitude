﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using SKF.RS.MicrologInspector.HardwareConnect.Properties;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public class RegistryConnect : RegistryBase, IRegistryConnect
   {
      //-----------------------------------------------------------------------

      #region Private Fieds

      // default value fields
      private const string VALUE_UID                  = "DeviceUID";
      private const string VALUE_FIRMWARE             = "FirmwareVersion";
      private const string VALUE_LICENSE_KEY          = "LicenseKey";
      private const string VALUE_USE_IP               = "UseIPAddress";
      private const string VALUE_USE_ANALYST_CMMS     = "UseAnalystCMMS";
      private const string VALUE_HOST_NAME            = "HostName";
      private const string VALUE_BARCODE_ENGINE       = "Barcode";
      private const string VALUE_EXT_VIBR_SENSOR      = "ExtVibSensorType";
      private const string VALUE_EXT_TEMP_SENSOR      = "ExtTempSensorType";
      private const string VALUE_BT_STACK_TYPE        = "BluetoothStack";
      private const string VALUE_DEVICE_MANUFACTURER  = "Manufacturer";
      private const string VALUE_IP_ADDRESS           = "IPAddress";
      private const string VALUE_PORT_NO              = "PortNumber";
      private const string VALUE_DEVICE_SERIAL_NO     = "DeviceSerialNumber";
      private const string VALUE_WMCD_ADDRESS         = "WMCDBTAddress";
      private const string VALUE_TEMP_GUN_BT_ADDRESS  = "TempGunBTAddress";
      private const string VALUE_TEMP_GUN_BT_NAME     = "TempGunBTName";
      private const string VALUE_CONNECTED            = "HasConnected";
      private const string VALUE_USE_DEBUG            = "UseDebug";
      private const string VALUE_CRADLE_DETECT        = "CradleDetect";
      private const string VALUE_USE_TEST_UID         = "UseTestID";
      private const string VALUE_SERVER_TYPE          = "ServerType";
      private const string NULL_BT_ADDRESS            = "0000000000000000";
      private const string VALUE_RFID_ENGINE          = "RFID";
      private const string VALUE_RFID_ENABLED         = "RFIDEnabled";
      private const string VALUE_RFID_TAG_TYPE        = "RFIDTagType";
      private const string VALUE_FFT_FREQ_UNITS       = "FFTFreqUnits";
      private const string VALUE_DISABLE_MARQUEE      = "DisableMarquee";
      private const string VALUE_ENABLE_SEARCH        = "EnableSearch";

      // set the default server type to desktop
      private const string DEFAULT_SERVER_TYPE        = "DESKTOP"; 

      // default data value
      private int mDefaultPortNumber = Convert.ToInt16( Resources.REG_DEFAULT_PORT );

      // Link to Registry IO Interface
      private IOperatorRegistryKey mOperatorSettings;

      // Media registry settings
      private IMediaReg mMediaReg;

      #endregion

      //-----------------------------------------------------------------------

      /// <summary>
      /// Constructor
      /// </summary>
      public RegistryConnect()
      {
         mOperatorSettings = new OperatorRegistryKey();
         mMediaReg = new MediaReg( );
      }

      //-----------------------------------------------------------------------

      #region Public Properties

      /// <summary>
      /// Get the Current User Registry Settings
      /// </summary>
      public IOperatorRegistryKey Operators
      {
         get{ return mOperatorSettings; }
      }

      /// <summary>
      /// Gets the current Media registry setting
      /// </summary>
      public IMediaReg Media
      {
         get { return mMediaReg; }
      }

      /// <summary>
      /// Get or set the Firmware Version
      /// </summary>
      public string FirmwareVersion
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FIRMWARE, value );
         }
         get
         {
            try
            {
               string version = string.Empty;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FIRMWARE, ref version );
               return version;
            }
            catch ( Exception )
            {
               return string.Empty;
            }
         }
      }


      /// <summary>
      /// Get or set the "USE IP ADDRESS" flag
      /// </summary>
      public Boolean UseIPAddress
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_IP, value );
         }
         get
         {
            try
            {
               Boolean useIP = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_IP, ref useIP );
               return useIP;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Get or set the "Use Analyst CMMS" flag
      /// </summary>
      public Boolean UseAnalystCMMS
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_ANALYST_CMMS, value );
         }
         get
         {
            try
            {
               Boolean useAnalystCMMS = true;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_ANALYST_CMMS, ref useAnalystCMMS );
               return useAnalystCMMS;
            }
            catch ( Exception )
            {
               return true;
            }
         }
      }


      /// <summary>
      /// Get or set the "Has Connected" flag
      /// </summary>
      public Boolean HasConnected
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_CONNECTED, value );
         }
         get
         {
            try
            {
               Boolean hasConnected = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_CONNECTED, ref hasConnected );
               return hasConnected;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Get or set the Server type
      /// </summary>
      public string ServerType
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_SERVER_TYPE, value );
         }
         get
         {
            try
            {
               string serverType = DEFAULT_SERVER_TYPE;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_SERVER_TYPE, ref serverType );
               return serverType;
            }
            catch ( Exception )
            {
               return DEFAULT_SERVER_TYPE;
            }
         }
      }


      /// <summary>
      /// Get or set the Middleware Server's HostName
      /// </summary>
      public string HostName
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_HOST_NAME, value );
         }
         get
         {
            try
            {
               string hostName = Resources.REG_LOCALHOST_NAME;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_HOST_NAME, ref hostName );
               return hostName;
            }
            catch ( Exception )
            {
               return Resources.REG_LOCALHOST_NAME;
            }
         }
      }


      /// <summary>
      /// Get or set the Middleware Server's IP Address
      /// </summary>
      public string IPAddress
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_IP_ADDRESS, value );
         }
         get
         {
            try
            {
               string ipAddress = Resources.REG_LOCALHOST_IP;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_IP_ADDRESS, ref ipAddress );
               return ipAddress;
            }
            catch ( Exception )
            {
               return Resources.REG_LOCALHOST_IP;
            }
         }
      }


      /// <summary>
      /// Gets the default localhost name
      /// </summary>
      public string LocalHostName
      {
         get
         {
            return Resources.REG_LOCALHOST_NAME;
         }
      }


      /// <summary>
      /// Gets the default localhost IP address
      /// </summary>
      public string LocalHostIP
      {
         get
         {
            return Resources.REG_LOCALHOST_IP;
         }
      }


      /// <summary>
      /// Get or set the Middleware Server's Open Port Number
      /// </summary>
      public int PortNumber
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_PORT_NO, value );
         }
         get
         {
            try
            {
               int portNumber = mDefaultPortNumber;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_PORT_NO, ref portNumber );
               return portNumber;
            }
            catch ( Exception )
            {
               return mDefaultPortNumber;
            }
         }
      }


      /// <summary>
      /// Get or set the device Serial Number (or Friendly name)
      /// </summary>
      public string DeviceSerialNo
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_DEVICE_SERIAL_NO, value );
         }
         get
         {
            try
            {
               string tmp = Resources.REG_DEFAULT_DEVICE_NAME;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_DEVICE_SERIAL_NO, ref tmp );
               return tmp;
            }
            catch ( Exception )
            {
               return Resources.REG_DEFAULT_DEVICE_NAME;
            }
         }
      }


      /// <summary>
      /// Get the default device name
      /// </summary>
      public UInt16 DefaultportNo
      {
         get
         {
            return Convert.ToUInt16( Resources.REG_DEFAULT_PORT );
         }
      }


      /// <summary>
      /// Gets or sets the ID of any natively supported Barcode engine
      /// </summary>
      public ScanningCtl.BarcodeEngine Barcode
      {
         get
         {
            try
            {
               // set the initial engine type
               int scanEngine = (int) ScanningCtl.BarcodeEngine.None;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BARCODE_ENGINE, ref scanEngine );

               //
               // If its unknown then poke about in the device hardware.
               //
               if ( scanEngine == (int) ScanningCtl.BarcodeEngine.None )
               {
                  //
                  // checking the devices hardware configuration, what
                  // barcode engine do we find?
                  //
                  scanEngine = (int) ScanningCtl.Supported_Barcode_Engine( );

                  // let's save that
                  mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                     KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BARCODE_ENGINE, scanEngine );
               }

               // and return the value
               return (ScanningCtl.BarcodeEngine) scanEngine;
            }
            catch ( Exception )
            {
               return ScanningCtl.BarcodeEngine.None;
            }
         }
         set
         {
            int scanEngine = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BARCODE_ENGINE, scanEngine );
         }
      }


      /// <summary>
      /// Gets or sets the ID of any natively supported RFID engine
      /// </summary>
      public ScanningCtl.RFIDEngine RFID
      {
         get
         {
            try
            {
               //
               // Devices can add RFID support (e.g. CN70 added internal IM11...)
               // to be safe, always query...
               //
               // checking the devices hardware configuration, what
               // RFID reader engine do we find?
               //
               var rfidEngine = (int)ScanningCtl.Supported_RFID_Engine();

               // let's save that
               mRegistryIO.SetValue(RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                  KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENGINE, rfidEngine);

               // and return the value
               return (ScanningCtl.RFIDEngine) rfidEngine;
            }
            catch ( Exception )
            {
               return ScanningCtl.RFIDEngine.Unknown;
            }
         }
         set
         {
            int rfidEngine = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENGINE, rfidEngine );
         }
      }


      /// <summary>
      /// Gets or sets whether or not RFID is enabled
      /// </summary>
      public bool RFIDEnabled
      {
         get
         {
            try
            {
               // set the initial engine type
               bool enabled = false;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENABLED, ref enabled );

               // and return the value
               return enabled;

            }
            catch ( Exception )
            {
               return false;
            }
         }
         set
         {            
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENABLED, value );
         }
      }


      /// <summary>
      /// Gets or sets the RFID Tag Type.  See RFID.TagType enum
      /// </summary>
      public int RFIDTagType
      {
         get
         {
            var value = 0; // TagType.Unkown

            try
            {
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                  KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_TAG_TYPE, ref value );
            }
            catch( Exception )
            {
               value = 0;
            }
            return value;
         }
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_TAG_TYPE, value );
         }
      }


      /// <summary>
      /// Get or sets the current BlueTooth Stack type.
      /// Note: If the returned type is unknown, this property will attempt
      /// to determine the stack type itself and then resave the result.
      /// </summary>
      public BlueToothCtl.DeviceStack BTDeviceStack
      {
         set
         {
            int stackType = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BT_STACK_TYPE, stackType );
         }
         get
         {
            try
            {
               // set the initial stack type
               int stackType = (int) BlueToothCtl.DeviceStack.Unknown;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BT_STACK_TYPE, ref stackType );

               // if its unknown then poke about in the device hardware
               if ( stackType == (int) BlueToothCtl.DeviceStack.Unknown )
               {
                  stackType = (int) BlueToothCtl.GetDeviceStackType( );
               }

               // if we got something, then save to the registry
               if ( stackType != (int) BlueToothCtl.DeviceStack.Unknown )
               {
                  mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                      KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BT_STACK_TYPE, stackType );
               }

               // and return the value
               return (BlueToothCtl.DeviceStack) stackType;
            }
            catch ( Exception )
            {
               return BlueToothCtl.DeviceStack.Unknown;
            }
         }
      }


      /// <summary>
      /// Gets or sets the WMCD Bluetooth Address
      /// </summary>
      public byte[] WMCD_BTAddress
      {
         set
         {
            string hexValue = AESEncryption.ByteArrayToHexString( value );
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_WMCD_ADDRESS, hexValue );
         }
         get
         {
            try
            {
               string tmp = NULL_BT_ADDRESS;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_WMCD_ADDRESS, ref tmp );
               return AESEncryption.HexStringToByteArray( tmp );
            }
            catch ( Exception )
            {
               return AESEncryption.HexStringToByteArray( NULL_BT_ADDRESS );
            }
         }
      }


      /// <summary>
      /// Gets or sets the RFID Gun Bluetooth Address
      /// </summary>
      public byte[] RFID_BTAddress
      {
         get;
         set;
      }


      /// <summary>
      /// Gets or sets the Temperature Gun Bluetooth Address
      /// </summary>
      public byte[] TempGun_BTAddress
      {
         set
         {
            string hexValue = AESEncryption.ByteArrayToHexString( value );
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_TEMP_GUN_BT_ADDRESS, hexValue );
         }
         get
         {
            try
            {
               string tmp = NULL_BT_ADDRESS;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_TEMP_GUN_BT_ADDRESS, ref tmp );
               return AESEncryption.HexStringToByteArray( tmp );
            }
            catch ( Exception )
            {
               return AESEncryption.HexStringToByteArray( NULL_BT_ADDRESS );
            }
         }
      }


      /// <summary>
      /// Gets or sets the Temperature Gun Bluetooth Name
      /// </summary>
      public string TempGun_BTName
      {
         set
         {
            string btName = value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_TEMP_GUN_BT_NAME, btName );
         }
         get
         {
            try
            {
               string tmp = string.Empty;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_TEMP_GUN_BT_NAME, ref tmp );
               return tmp;
            }
            catch ( Exception )
            {
               return string.Empty;
            }
         }
      }


      /// <summary>
      /// Get the default device name
      /// </summary>
      public string DefaultSerialNo
      {
         get
         {
            return Resources.REG_DEFAULT_DEVICE_NAME;
         }
      }


      /// <summary>
      /// Get the Device UUID and set the registry value (reference only)
      /// </summary>
      public string DeviceUID
      {
         get
         {
            string deviceUID = GetDeviceID();
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
            KEY_ROOT_SKF, KEY_APP_NAME, VALUE_UID, GetDeviceID() );
            return deviceUID;
         }
      }


      /// <summary>
      /// Get or set the "UseDebug" flag
      /// </summary>
      public Boolean UseDebug
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_DEBUG, value );
         }
         get
         {
            try
            {
               Boolean useDebug = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_DEBUG, ref useDebug );
               return useDebug;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Gets or sets if the handheld should synch a on cradle detect
      /// </summary>
      public Boolean CradleDetect
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_CRADLE_DETECT, value );
         }
         get
         {
            try
            {
               Boolean cradleDetect = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_CRADLE_DETECT, ref cradleDetect );
               return cradleDetect;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Gets or sets whether the comms should use the actual
      /// UID of the device, or a fixed 'test' reference UID
      /// </summary>
      public Boolean UseTestID
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_TEST_UID, value );
         }
         get
         {
            try
            {
               Boolean useTestID = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_TEST_UID, ref useTestID );
               return useTestID;
            }
            catch ( Exception )
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Gets or sets the frequency units to use when viewing FFTs
      /// </summary>
      public int FFTFreqUnits
      {
         get
         {
            try
            {
               // set the initial engine type
               int freqUnits = 0x00; /* FFTFrequencyUnits.Hz */

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FFT_FREQ_UNITS, ref freqUnits );

               // and return the value
               return freqUnits;

            }
            catch ( Exception )
            {
               return 0x00; /* FFTFrequencyUnits.Hz */
            }
         }
         set
         {
            int freqUnits = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
               KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FFT_FREQ_UNITS, freqUnits );
         }
      }


      /// <summary>
      /// Gets or sets the external vibration sensor type
      /// </summary>
      public ExternalSensor.Vibration ExtVibSensor
      {
         get
         {
            try
            {
               // set the initial sensor type
               int vibSensor = (int) ExternalSensor.Vibration.WMCD;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_VIBR_SENSOR, ref vibSensor );

               // if we got something, then save to the registry
               if ( vibSensor != (int) ExternalSensor.Vibration.None )
               {
                  mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                      KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_VIBR_SENSOR, vibSensor );
               }

               // and return the value
               return (ExternalSensor.Vibration) vibSensor;
            }
            catch ( Exception )
            {
               return ExternalSensor.Vibration.WMCD;
            }
         }
         set
         {
            int vibSensor = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_VIBR_SENSOR, vibSensor );
         }
      }


      /// <summary>
      /// Gets or sets the external temperature sensor type
      /// </summary>
      public ExternalSensor.Temperature ExtTempSensor
      {
         get
         {
            try
            {
               // set the initial sensor type
               int tempGun = (int) ExternalSensor.Vibration.None;

               // lets see what's in the registry
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                   KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_TEMP_SENSOR, ref tempGun );

               // if we got something, then save to the registry
               if ( tempGun != (int) ExternalSensor.Vibration.None )
               {
                  mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                      KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_TEMP_SENSOR, tempGun );
               }

               // and return the value
               return (ExternalSensor.Temperature) tempGun;
            }
            catch ( Exception )
            {
               return ExternalSensor.Temperature.None;
            }
         }
         set
         {
            int tempGun = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_EXT_TEMP_SENSOR, tempGun );
         }
      }

      /// <summary>
      /// Gets the override to disable the marquee text (OTD# 4712 hack)
      /// </summary>
      public bool DisableMarquee
      {
         get
         {
            var result = false;
            try
            {
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                  KEY_ROOT_SKF, KEY_APP_NAME, VALUE_DISABLE_MARQUEE, ref result );
            }
            catch( Exception )
            {
               result = false;
            }
            return result;
         }
      }

      /// <summary>
      /// Gets the override to enable the Search feature
      /// </summary>
      public bool EnableSearch
      {
         get
         {
            var result = false;
            try
            {
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                  KEY_ROOT_SKF, KEY_APP_NAME, VALUE_ENABLE_SEARCH, ref result );
            }
            catch( Exception )
            {
               result = false;
            }
            return result;
         }
      }
      #endregion

      //-----------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// This method validates all SKF keys within HKLM
      /// </summary>
      /// <returns>true if OK, error on fail</returns>
      public Boolean ValidateLocalMachineHive()
      {
         string stringScratch = string.Empty;
         Boolean boolScratch = false;
         int intScratch = 0;
         int portNumber = mDefaultPortNumber;

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
            KEY_ROOT_SKF, KEY_APP_NAME, VALUE_WMCD_ADDRESS, ref stringScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                  KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FIRMWARE, ref stringScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_IP, ref boolScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
             KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_ANALYST_CMMS, ref boolScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
              KEY_ROOT_SKF, KEY_APP_NAME, VALUE_USE_DEBUG, ref boolScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_SERVER_TYPE, ref stringScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_HOST_NAME, ref stringScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_PORT_NO, ref portNumber ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_DEVICE_SERIAL_NO, ref stringScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_BT_STACK_TYPE, ref intScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENGINE, ref intScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_RFID_ENABLED, ref boolScratch ) )
         {
            return false;
         }

         if ( !mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
                KEY_ROOT_SKF, KEY_APP_NAME, VALUE_FFT_FREQ_UNITS, ref intScratch ) )
         {
            return false;
         }

         return true;

      }/* end method */


      /// <summary>
      /// Construct the HKEY_CURRENT_USER Hive and populate with the standard
      /// Microlog AX and GX Key hierarchies. The primary purpose of this method
      /// is to allow @ptitude to correctly identify the connected device
      /// </summary>
      public void BuildCurrentUserHive()
      {
         // Add the "Diagnostic Instuments" Application Name Key
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
             KEY_ROOT_DI, KEY_APP_NAME, "", "" );

         // Add the "Diagnostic Instruments" SW_Revisions Key
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
             KEY_ROOT_DI, KEY_SW_REV, "", "" );

         // Add the "SKF" Application Name Key
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
             KEY_ROOT_SKF, KEY_APP_NAME, "", "" );

      }/* end method */


      /// <summary>
      /// Construct the HKEY_LOCAL_MACHINE Hive and populate with the Microlog
      /// Inspector hardware settings (including Device UUID, Firmware version
      /// and TCP/IP configurations).  
      /// </summary>
      public void BuildLocalMachineHive()
      {
         //
         // Add the device connection status (will be true one connection to
         // a Middleware Server has been established
         //
         this.HasConnected = false;

         // Add the TCP Port number (default is 8089)
         this.PortNumber = mDefaultPortNumber;

         //
         // The desktop installer can set the IP Address, so if it is
         // present, don't overwrite
         //
         if ( this.IPAddress == Resources.REG_LOCALHOST_IP )
         {
            // Add the Middleware IP address (default is LocalHost)
            this.IPAddress = Resources.REG_LOCALHOST_IP;
         }

         //
         // The desktop installer will push the HostName down, so if it is
         // present, then don't overwrite it
         //
         if ( this.HostName == Resources.REG_LOCALHOST_NAME )
         {
            // Add the Middleware HostName (default is LocalHost)
            this.HostName = Resources.REG_LOCALHOST_NAME;
         }

         //
         // The desktop installer can set this flag down, so if it is
         // present, then don't overwrite it
         //
         if ( this.UseIPAddress != true )
         {
            // Add the Boolean "Use IP Address" flag (default is false)
            this.UseIPAddress = false;
         }

         // Add the Boolean "Use Debug" flag (default is false)
         this.UseDebug = false;

         // Check for Cradle Detection events (default is false)
         this.CradleDetect = false;

         // Add the Boolean "Use Analyst CMMS" flag (default is true)
         this.UseAnalystCMMS = true;

         //
         // The Microlog Inspector Tools can set this... so don't overwrite
         // if it's set to something...
         //
         if( DeviceSerialNo == Resources.REG_DEFAULT_DEVICE_NAME )
         {
            DeviceSerialNo = Resources.REG_DEFAULT_DEVICE_NAME;
         }

         // Add the selected server type (Desktop or Enterprise)
         this.ServerType = DEFAULT_SERVER_TYPE;

         // Add the encrypted License Key (default value is null)
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
             KEY_ROOT_SKF, KEY_APP_NAME, VALUE_LICENSE_KEY, "" );

         //
         // Fix for OTD# 1605, we don't want to clear the FW Version if the
         // HomePage has already set it!
         //
         if ( this.FirmwareVersion == string.Empty )
         {
            // Add the current Firmware Version (default value is 1.0.0.0)
            this.FirmwareVersion = Resources.REG_DEFAULT_VERSION;
         }

         // Add the default statck type
         this.BTDeviceStack = (int) BlueToothCtl.DeviceStack.Unknown;

         // Add the Device UID (default read from current unit)
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
              KEY_ROOT_SKF, KEY_APP_NAME, VALUE_UID, GetDeviceID() );

         // Add the target WMCD Bluetooth Address
         mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
            KEY_ROOT_SKF, KEY_APP_NAME, VALUE_WMCD_ADDRESS, NULL_BT_ADDRESS );

         // Add the 'uknown' RFID Engine
         this.RFID = ScanningCtl.RFIDEngine.Unknown;

         // Add the disabled RFID flag
         this.RFIDEnabled = false;

         // Add the FFT Frequency unit type
         this.FFTFreqUnits = 0x00; /* FFTFrequencyUnits.Hz */

         // Add the default external vibration sensor 
         this.ExtVibSensor = ExternalSensor.Vibration.WMCD;

         // Add the default external vibration sensor 
         this.ExtTempSensor = ExternalSensor.Temperature.None;

         // set temperature gun Bluetooth name
         this.TempGun_BTName = string.Empty;

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 16th March 2010 
//  Added support for "Use Dubug Mode" flag
//
//  Revision 1.0 APinkerton 15th January 2010 
//  Added support for the device Serial No (or Friendly Name)
//
//  Revision 0.0 APinkerton 16th March 2009 
//  Add to project
//----------------------------------------------------------------------------