﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System.Collections.Generic;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class manages all the Modules under the HIVE
   /// Key HKEY_LOCAL_MACHINE.
   /// </summary>
   public class ModulesReg : RegistryBase
   {
      #region Class Members

      /// <summary>
      /// A list of the modules in the registry and their values
      /// </summary>
      private List<ModuleRegistryKey> mModulesList;

      /// <summary>
      /// Registry branch for the Modules
      /// </summary>
      private const string mBranch = KEY_ROOT_SKF + KEY_APP_NAME; // SOFTWARE\SKF\MicrologInspector\

      #endregion Class Members


      #region Properties

      /// <summary>
      /// A list of the modules in the registry and their values
      /// </summary>
      public List<ModuleRegistryKey> ModulesList
      {
         get { return mModulesList; }
      }

      #endregion Properties


      #region Constructor

      /// <summary>
      /// Constructor
      /// </summary>
      public ModulesReg()
      {
         string[] moduleNames = null;
         mModulesList = new List<ModuleRegistryKey>( );
         
         mRegistryIO.GetSubKeyNames(RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE, mBranch, KEY_MODULES, ref moduleNames);

         if( moduleNames != null )
         {
            // populate the list
            for( int i = 0; i < moduleNames.Length; i++ )
            {
               ModuleRegistryKey modRegKey = new ModuleRegistryKey( moduleNames[i] );

               // check one of the properties to be sure we've gotten OK info
               if( modRegKey.AssemblyName != string.Empty )
               {
                  mModulesList.Add( modRegKey );
               }
            }
         }

      }/* end constructor */

      #endregion Constructor

   } /* end class */

} /* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 TBottalico December 4th 2009
//  Add to project
//----------------------------------------------------------------------------
