﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Identical to DeviceInfo - but battery update events are always running!
   /// Note: This class should be removed when DeviceInfo has been updated!
   /// </summary>
   public static class BatteryInfo
   {
      /// <summary>
      /// AC Line status
      /// </summary>
      public enum ACPower
      {
         On,
         Off,
         BackupBattery,
         Unknown
      }

      #region Class Members

      /// <summary>
      /// The power status
      /// </summary>
      private static WinCE.SYSTEM_POWER_STATUS_EX sPowerStatus;

      /// <summary>
      /// More friendly power info structure
      /// </summary>
      public struct PowerInfo
      {
         public ACPower ChargeState;
         public uint BatteryPercent;
      };

      /// <summary>
      /// Update timer for Power Info status
      /// </summary>
      private static Timer sUpdateTimer;

      /// <summary>
      /// Default Power Info status update interval
      /// </summary>
      public const int DEFAULT_UPDATE_INTERVAL = 5000;

      /// <summary>
      /// The power info structure
      /// </summary>
      private static PowerInfo sPowerInfo;

      #endregion

      /// <summary>
      /// Static constructor
      /// </summary>
      static BatteryInfo()
      {
         if ( !DeviceInfo.IsWindowsEmbeddedOrNewer )
         {
            sPowerInfo = new PowerInfo();
            sPowerStatus = new WinCE.SYSTEM_POWER_STATUS_EX();
            sUpdateTimer = new Timer( new TimerCallback( On_Update_Timer_Tick ),
                                                         null,
                                                         DEFAULT_UPDATE_INTERVAL,
                                                         DEFAULT_UPDATE_INTERVAL );
         }

      }/* end constructor */
      

      /// <summary>
      /// Gets the power state of the device
      /// </summary>
      /// <param name="iUpdate">True if you want to get current status, false if you can
      /// afford to have data that can be several seconds old</param>
      /// <returns></returns>
      public static PowerInfo Get_Power_State( bool iUpdate )
      {
         // initialize the structure
         sPowerStatus.ACLineStatus = 0;
         sPowerStatus.BackupBatteryFlag = 0;
         sPowerStatus.BackupBatteryFullLifeTime = 0;
         sPowerStatus.BackupBatteryLifePercent = 0;
         sPowerStatus.BackupBatteryLifeTime = 0;
         sPowerStatus.BatteryFlag = 0;
         sPowerStatus.BatteryFullLifeTime = 0;
         sPowerStatus.BatteryLifePercent = 0;
         sPowerStatus.BatteryLifeTime = 0;

         // initialize the structure
         sPowerInfo.BatteryPercent = 0;
         sPowerInfo.ChargeState = ACPower.Unknown;

         try
         {
            if( WinCE.GetSystemPowerStatusEx( sPowerStatus, iUpdate ) == true )
            {
               switch( sPowerStatus.ACLineStatus )
               {
                  case WinCE.AC_LINE_OFFLINE:
                     sPowerInfo.ChargeState = ACPower.Off;
                     break;
                  case WinCE.AC_LINE_ONLINE:
                     sPowerInfo.ChargeState = ACPower.On;
                     break;
                  case WinCE.AC_LINE_BACKUP_POWER:
                     sPowerInfo.ChargeState = ACPower.BackupBattery;
                     break;
                  case WinCE.AC_LINE_UNKNOWN:
                     sPowerInfo.ChargeState = ACPower.Unknown;
                     break;
                  default:
                     sPowerInfo.ChargeState = ACPower.Unknown;
                     break;
               }

               sPowerInfo.BatteryPercent = sPowerStatus.BatteryLifePercent;
            }
         }
         catch
         {
         }

         return sPowerInfo;

      }/* end method */


      /// <summary>
      /// Stop all future updates
      /// </summary>
      public static void Kill_Battery_Updates()
      {
         if ( sUpdateTimer != null )
         {
            sUpdateTimer.Dispose();
            sUpdateTimer = null;
         }

      }/* end method */


      /// <summary>
      /// Status timer click event handler
      /// </summary>
      /// <param name="iStateInfo"></param>
      private static void On_Update_Timer_Tick( object iStateInfo )
      {
         On_Power_Info_Update( Get_Power_State( true ) );

      }/* end method */      
          

      /// <summary>
      /// Update event used to get power info status updates.  Not thread
      /// safe.
      /// </summary>
      /// <param name="iError">PowerInfo structure</param>
      public delegate void PowerInfoUpdate( PowerInfo iPowerInfo );
      public static event PowerInfoUpdate PowerInfoUpdateEvent;

      private static void On_Power_Info_Update( PowerInfo iPowerInfo )
      {
         if( PowerInfoUpdateEvent != null )
         {
            PowerInfoUpdateEvent( iPowerInfo );
         }
      }


   }/* end class */

}

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 1st August 2011
//  Disable core functionality when running under Windows Mobile 6.5.3
//  or (for Intermec) "Windows Embedded Compact"
//
//  Revision 0.0 TBpttalico 5th April 2010
//  Add to project
//----------------------------------------------------------------------------