﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This Interface manages all Operator settings under the
   /// HIVE Key HKEY_CURRENT_USER.
   /// </summary>
   public interface IOperatorRegistryKey
   {
      /// <summary>
      /// Get or Set the currently logged in operator's access level
      /// </summary>
      int AccessLevel { get; set; }

      /// <summary>
      /// Get or Set what AlarmFilter the operator is using (In Danger, In Alert, etc.)
      /// </summary>
      int AlarmFilterType { get; set; }

      /// <summary>
      /// Get or Set whether the currently logged in operator can change
      /// their default login password
      /// </summary>
      bool CanChangePassword { get; set; }

      /// <summary>
      /// Get or Set whether the current operator can create a new Work 
      /// Notification from within the core application
      /// </summary>
      bool CreateWorkNotification { get; set; }

      /// <summary>
      /// Get or Set the time interval at which MARLIN considers data to be
      /// new (used to warn if MARLIN thinks data is being collected twice)
      /// </summary>
      int DataCollectionTime { get; set; }

      /// <summary>
      /// Gets or sets whether or not the camera option is enabled (assuming
      /// it's also supported)
      /// </summary>
      bool EnableCamera { get; set; }

      /// <summary>
      /// Font size to be used (i.e. in the Views)
      /// </summary>
      int FontSize { get; set; }

      /// <summary>
      /// Gets or sets the high constrast mode setting
      /// </summary>
      bool HighContrastMode { get; set; }

      /// <summary>
      /// Get or Set how the system will manage the operator selecting the
      /// "Machine Not Operating" button
      /// </summary>
      IMachineNotOperatingSkips MachineNotOperatingSkips { get; set; }

      /// <summary>
      /// Get or Set how the system will manage the operator selecting the
      /// "Machine OK" button
      /// </summary>
      IMachineOKSkips MachineOKSkips { get; set; }

      /// <summary>
      /// Get or Set what messages are to be displayed during data collection
      /// </summary>
      IMessagingPrefsRegistryKey MessagingPreferences { get; set; }

      /// <summary>
      /// Get or Set whether the system accept 'out of range' values from
      /// the numeric keypad
      /// </summary>
      bool NumericRangeProtection { get; set; }

      /// <summary>
      /// Get or Set the name of the currently logged in operator
      /// </summary>
      string OperatorName { get; set; }

      /// <summary>
      /// Get or Set the operators Password. Note password is lightly 
      /// encryped to prevent users browsing the Registry on the device 
      /// and obtaing the Admin password.
      /// </summary>
      string Password { get; set; }

      /// <summary>
      /// Get or Set: When Reset, the operator must change their password at
      /// login.  When Set, the operator has set their password.  When None,
      /// operation is as normal
      /// </summary>
      int ResetPassword { get; set; }

      /// <summary>
      /// Get or Set: When true, after an operator scans a Machine, they 
      /// will go to the first collection POINT - not the Machine dialog!
      /// </summary>
      bool ScanAndGoToFirstPoint { get; set; }

      /// <summary>
      /// Get or Set: Will the operator have to scan current asset Tag 
      /// before they can start collecting data
      /// </summary>
      bool ScanRequiredToCollect { get; set; }

      /// <summary>
      /// Get or Set the current operators ability to view the last 
      /// collected value for the current point - prior to collecting 
      /// new data! The default value will be false. 
      /// </summary>
      bool ShowPreviousData { get; set; }

      /// <summary>
      /// Specify whether to display the Feedback review display during 
      /// data collection. Choices are, Always, On Alarm, or Never
      /// </summary>
      int VerifyMode { get; set; }

      /// <summary>
      /// Get or Set whether the current  operator view the work 
      /// notification history
      /// </summary>
      bool ViewCMMSWorkHistory { get; set; }

      /// <summary>
      /// Get or set the tree filtering view. if true all POINTs that have data
      /// collected against them will be hidden.  See PacketBase.OverdueFilter
      /// </summary>
      int ViewOverdueOnly { get; set; }

      /// <summary>
      /// Get or Set whether the current operator is able to view FFT Data
      /// </summary>
      bool ViewSpectralData { get; set; }

      /// <summary>
      /// Get or Set which hierarchy items are to be shown on the 
      /// hierarchy display. Choices are POINT only , SET+POINT, 
      /// Machine+POINT, or All.  See PacketBase.ViewTreeAs
      /// </summary>
      int ViewTreeElement { get; set; }

      /// <summary>
      /// Get or Set what ViewType the operator is using (Tree, List, Map, etc.)
      /// </summary>
      int ViewType { get; set; }

   }/* end interface */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2009
//  Add to project
//----------------------------------------------------------------------------