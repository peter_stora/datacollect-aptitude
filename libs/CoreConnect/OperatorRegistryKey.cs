﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class manages all Operator settings under the
   /// HIVE Key HKEY_CURRENT_USER.
   /// </summary>
   public class OperatorRegistryKey : RegistryBase, IOperatorRegistryKey
   {
      //-----------------------------------------------------------------------

      #region Class Members

      // SKF\MicrologInspector branch
      private string mBranch = KEY_ROOT_SKF + KEY_APP_NAME;

      // Registry values
      private const string VALUE_RIGHTS               = "AccessLevel";
      private const string VALUE_NAME                 = "OperatorName";
      private const string VALUE_PASSWORD             = "Password";
      private const string VALUE_CAN_CHANGE_PW        = "CanChangePassword";
      private const string VALUE_CREATE_NOTIFICATION  = "CreateWorkNotification";
      private const string VALUE_COLLECTION_TIME      = "DataCollectionTime";
      private const string VALUE_ENABLE_CAMERA        = "EnableCamera";
      private const string VALUE_FONT_SIZE            = "FontSize";
      private const string VALUE_RESET_PASSWORD       = "ResetPassword";
      private const string VALUE_SCAN_TAG             = "ScanRequiredToCollect";
      private const string VALUE_SHOW_PREVIOUS        = "ShowPreviousData";
      private const string VALUE_VERIFY_MODE          = "VerifyMode";
      private const string VALUE_VIEW_OVERDUE         = "ViewOverdue";
      private const string VALUE_VIEW_TREE_ELEM       = "ViewTreeElement";
      private const string VALUE_VIEW_CMMS_HISTORY    = "ViewCMMSWorkHistory";
      private const string VALUE_RANGE_PROTECT        = "NumericRangeProtection";
      private const string VALUE_SCAN_TFP             = "ScanAndGoToFirstPoint";
      private const string VALUE_CAN_VIEW_FFT         = "ViewSpectralData";
      private const string VALUE_VIEW_TYPE            = "ViewType";
      private const string VALUE_ALARM_FILTER         = "AlarmFilter";
      private const string VALUE_HIGH_CONTRAST_MODE   = "HighContrastMode";

      // Message Preferences
      private IMessagingPrefsRegistryKey  mMessagingPreferences      = null;

      // Machine Skip Preferences
      private IMachineOKSkips             mMachineOkSkips            = null;
      private IMachineNotOperatingSkips   mMachineNotOperatingSkips  = null;
      
      // Default values used when getting value from registry fails
      private const int    DEFAULT_ACCESS_LEVEL          = 0x03; /* OperatorRights.Limited */
      private const int    DEFAULT_ALARM_FILTER          = 0x00; /* AlarmFilter.Off */
      private const bool   DEFAULT_CAN_CHANGE_PASSWORD   = false;
      private const bool   DEFAULT_CAN_CREATE_WN         = false;
      private const int    DEFAULT_COLLECTION_INTERVAL   = 45;
      private const bool   DEFAULT_ENABLE_CAMERA         = false;
      private const int    DEFAULT_FONT_SIZE             = 0x01; /* FontSizes.Large */
      private const bool   DEFAULT_HIGH_CONTRAST_MODE    = false;
      private const bool   DEFAULT_NUM_RANGE_PROTECTION  = false;
      private const string DEFAULT_OP_NAME               = "Default";
      private const string DEFAULT_PASSWORD              = "skf";
      private const int    DEFAULT_RESET_PASSWORD        = 0x00; /* ResetPassword.None */
      private const bool   DEFAULT_SCAN_GO_FIRST_POINT   = false;
      private const bool   DEFAULT_SCAN_REQUIRED         = false;
      private const bool   DEFAULT_SHOW_PREVIOUS         = false;
      private const int    DEFAULT_VERIFICATION_MODE     = 0x01; /* VerificationMode.OnAlarm */
      private const bool   DEFAUL_VIEW_CMMS              = false;
      private const bool   DEFAULT_VIEW_FFT              = false;
      private const int    DEFAULT_VIEW_OVERDUE          = 0x02; /* OverdueFilter.Off */
      private const int    DEFAULT_VIEW_TREE_AS          = 0x10; /* ViewTreeAs.All */
      private const int    DEFAULT_VIEW_TYPE             = 0x01; /* DCEViewType.SplitView */
      #endregion

      //-----------------------------------------------------------------------

      #region Properties

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      public OperatorRegistryKey()
      {
         // create the Registry Interop class
         mRegistryIO = new RegistryIO( );

         // create the node branches
         mMessagingPreferences = new MessagingPrefsRegistryKey( );
         mMachineOkSkips = new MachineOKSkips( );
         mMachineNotOperatingSkips = new MachineNotOperatingSkips( );

      }/* end constructor */


      /// <summary>
      /// Get or Set the name of the currently logged in operator
      /// </summary>
      public string OperatorName
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_NAME, value );
         }
         get
         {  
            try
            {
               string _value = "";
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_NAME, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_OP_NAME;
            }  
         }
      }


      /// <summary>
      /// Get or Set the currently logged in operator's access level
      /// </summary>
      public int AccessLevel
      {
         set
         {
            int _value = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_RIGHTS, _value );
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_RIGHTS, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_ACCESS_LEVEL;
            }            
         }
      }


      /// <summary>
      /// Get or Set whether the currently logged in operator can change
      /// their default login password
      /// </summary>
      public Boolean CanChangePassword
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_CAN_CHANGE_PW, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_CAN_CHANGE_PW, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_CAN_CHANGE_PASSWORD;
            }            
         }
      }


      /// <summary>
      /// Get or Set whether the current operator can create a new Work 
      /// Notification from within the core application
      /// </summary>
      public Boolean CreateWorkNotification
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_CREATE_NOTIFICATION, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_CREATE_NOTIFICATION, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_CAN_CREATE_WN;
            }            
         }
      }


      /// <summary>
      /// Get or Set the time interval at which MARLIN considers data to be
      /// new (used to warn if MARLIN thinks data is being collected twice)
      /// </summary>
      public int DataCollectionTime
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_COLLECTION_TIME, value );
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_COLLECTION_TIME, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_COLLECTION_INTERVAL;
            }
         }
      }


      /// <summary>
      /// Gets or sets whether the camera option is enabled (assuming it's also
      /// supported)
      /// </summary>
      public bool EnableCamera
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_ENABLE_CAMERA, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_ENABLE_CAMERA, ref _value );
               return _value;
            }
            catch( Exception )
            {
               return DEFAULT_ENABLE_CAMERA;
            }
         }
      }


      /// <summary>
      /// FontSize to be used (i.e. with Views)
      /// </summary>
      public int FontSize
      {
         set
         {
            //
            // We will raise an event when the fontsize changes, check the old
            // value to be sure it has changed
            // 
            int previousValue = 0;
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
              mBranch, KEY_OPERATOR_SETTINGS, VALUE_FONT_SIZE, ref previousValue );

            if ( previousValue != (int)value )
            {
               int _value = (int)value;
               mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_FONT_SIZE, _value );
            }
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_FONT_SIZE, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_FONT_SIZE;
            }
         }
      }


      /// <summary>
      /// Gets or sets the high constrast mode setting
      /// </summary>
      public bool HighContrastMode
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_HIGH_CONTRAST_MODE, value );
         }
         get
         {
            try
            {
               var result = DEFAULT_HIGH_CONTRAST_MODE;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_HIGH_CONTRAST_MODE, ref result );
               return result;
            }
            catch ( Exception )
            {
               return DEFAULT_HIGH_CONTRAST_MODE;
            }
         }
      }

      /// <summary>
      /// Get or Set the operators Password. Note password is lightly 
      /// encryped to prevent users browsing the Registry on the device 
      /// and obtaing the Admin password.
      /// </summary>
      public String Password
      {
         set
         {
            string _value = EncrptPassword( value );
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_PASSWORD, _value );
         }
         get
         {
            try
            {
               string _value = string.Empty;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_PASSWORD, ref _value );
               return DecryptPassword( _value );
            }
            catch ( Exception )
            {
               return DEFAULT_PASSWORD;
            }            
         }
      }


      /// <summary>
      /// Get or set: if true this option will reset the current user's 
      /// password to back to "skf"
      /// </summary>
      public int ResetPassword
      {
         set
         {
            int resetPassword = (int) value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_RESET_PASSWORD, resetPassword );
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_RESET_PASSWORD, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_RESET_PASSWORD;
            }            
         }
      }


      /// <summary>
      /// Get or Set: Will the operator have to scan current asset Tag 
      /// before they can start collecting data
      /// </summary>
      public Boolean ScanRequiredToCollect
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_SCAN_TAG, value );
         }
         get
         {            
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_SCAN_TAG, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_SCAN_REQUIRED;
            }            
         }
      }


      /// <summary>
      /// Get or Set the current operators ability to view the last 
      /// collected value for the current point - prior to collecting 
      /// new data! The default value will be false. 
      /// </summary>
      public Boolean ShowPreviousData
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_SHOW_PREVIOUS, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_SHOW_PREVIOUS, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_SHOW_PREVIOUS;
            }            
         }
      }


      /// <summary>
      /// Specify whether to display the Feedback review display during 
      /// data collection. Choices are, Always, On Alarm, or Never
      /// </summary>
      public int VerifyMode
      {
         set
         {
            int verifyMode = value;
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_VERIFY_MODE, verifyMode );
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VERIFY_MODE, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_VERIFICATION_MODE;
            }            
         }
      }


      /// <summary>
      /// Get or set the tree filtering view. if true all POINTs that have
      /// data collected against them will be hidden.  
      /// See PacketBase.OverdueFilter
      /// </summary>
      public int ViewOverdueOnly
      {
         set
         {
            //
            // We will raise an event when the Node Filter changes, check the old
            // value to be sure it has changed
            // 
            int previousValue = 0;
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
               mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_OVERDUE, ref previousValue );

            if ( previousValue != value )
            {
               mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_OVERDUE, value );
            }
         }
         get
         {
            try
            {
               int _value = DEFAULT_VIEW_OVERDUE;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_OVERDUE, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_VIEW_OVERDUE;
            }            
         }
      }


      /// <summary>
      /// Get or Set which hierarchy items are to be shown on the 
      /// hierarchy display. Choices are POINT only , SET+POINT, 
      /// Machine+POINT, or All.  See PacketBase.ViewTreeAs
      /// </summary>
      public int ViewTreeElement
      {
         set
         {
            //
            // We will raise an event when the Node Filter changes, check the old
            // value to be sure it has changed
            // 
            int previousValue = 0;
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
               mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TREE_ELEM, ref previousValue );

            if ( previousValue != value )
            {
               mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TREE_ELEM, value );
            }
         }
         get
         {            
            try
            {
               int _value = DEFAULT_VIEW_TREE_AS;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TREE_ELEM, ref _value );
               return _value;
            }
            catch(Exception)
            {
               return DEFAULT_VIEW_TREE_AS;
            }
         }
      }


      /// <summary>
      /// Get or Set whether the current  operator view the work 
      /// notification history
      /// </summary>
      public Boolean ViewCMMSWorkHistory
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_CMMS_HISTORY, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_CMMS_HISTORY, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAUL_VIEW_CMMS;
            }
         }
      }


      /// <summary>
      /// Get or Set whether the system accept 'out of range' values from
      /// the numeric keypad
      /// </summary>
      public Boolean NumericRangeProtection
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_RANGE_PROTECT, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_RANGE_PROTECT, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_NUM_RANGE_PROTECTION;
            }            
         }
      }


      /// <summary>
      /// Get or Set: When true, after an operator scans a Machine, they 
      /// will go to the first collection POINT - not the Machine dialog!
      /// </summary>
      public Boolean ScanAndGoToFirstPoint
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_SCAN_TFP, value );
         }
         get
         {            
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_SCAN_TFP, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_SCAN_GO_FIRST_POINT;
            }
         }
      }


      /// <summary>
      /// Get or Set whether the current operator is able to view FFT Data
      /// </summary>
      public Boolean ViewSpectralData
      {
         set
         {
            mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                mBranch, KEY_OPERATOR_SETTINGS, VALUE_CAN_VIEW_FFT, value );
         }
         get
         {
            try
            {
               bool _value = false;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_CAN_VIEW_FFT, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_VIEW_FFT;
            }
         }
      }


      /// <summary>
      /// Get or Set what messages are to be displayed during data collection
      /// </summary>
      public IMessagingPrefsRegistryKey MessagingPreferences
      {
         get
         {
            return mMessagingPreferences;
         }
         set
         {
            mMessagingPreferences = value;
         }
      }


      /// <summary>
      /// Get or Set how the system will manage the operator selecting the
      /// "Machine OK" button
      /// </summary>
      public IMachineOKSkips MachineOKSkips
      {
         get
         {
            return mMachineOkSkips;
         }
         set
         {
            mMachineOkSkips = value;
         }
      }


      /// <summary>
      /// Get or Set how the system will manage the operator selecting the
      /// "Machine Not Operating" button
      /// </summary>
      public IMachineNotOperatingSkips MachineNotOperatingSkips
      {
         get
         {
            return mMachineNotOperatingSkips;
         }
         set
         {
            mMachineNotOperatingSkips = value;
         }
      }


      /// <summary>
      /// Get or Set what ViewType the operator is using (Tree, List, Map, etc.)
      /// </summary>
      public int ViewType
      {
         set
         {
            //
            // We will raise an event when the ViewType changes, check the old
            // value to be sure it has changed
            //

            // set default to splitview
            int previousValue = 0x01; /* DCEViewType.SplitView */

            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
               mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TYPE, ref previousValue );

            if ( previousValue != value )
            {
               int _value = value;
               mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TYPE, _value );
            }
         }
         get
         {
            try
            {
               int _value = DEFAULT_VIEW_TYPE;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_VIEW_TYPE, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_VIEW_TYPE;
            }            
         }
      }


      /// <summary>
      /// Get or Set what AlarmFilter (In Danger, In Alert, etc.) the operator is using
      /// </summary>
      public int AlarmFilterType
      {
         set
         {
            //
            // We will raise an event when the AlarmFilter changes, check the old
            // value to be sure it has changed
            // 
            int previousValue = 0;
            mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
               mBranch, KEY_OPERATOR_SETTINGS, VALUE_ALARM_FILTER, ref previousValue );

            if ( previousValue != (int) value )
            {
               int _value = (int) value;
               mRegistryIO.SetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_ALARM_FILTER, _value );
            }
         }
         get
         {
            try
            {
               int _value = 0;
               mRegistryIO.GetValue( RegistryIO.RegistryHive.HKEY_CURRENT_USER,
                   mBranch, KEY_OPERATOR_SETTINGS, VALUE_ALARM_FILTER, ref _value );
               return _value;
            }
            catch ( Exception )
            {
               return DEFAULT_ALARM_FILTER;
            }            
         }
      }

      #endregion    

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 21st June 2009
//  Add to project
//----------------------------------------------------------------------------