﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Manages encryption and de-encryption of the operator's password 
   /// </summary>
   internal class AESEncryption
   {
      #region Private Fields

      /// <summary>
      /// General fixed Hash Key (for double encryption)
      /// </summary>
      private const string OPSLOCAL_HASH = "200DF0A5A0E8856AAFBB3E71684446" +
                   "C2688EA2D431470CB67378E49E481B456A585C3B10C9C778B96165" +
                   "0A0F322AE0BAE1";

      /// <summary>
      /// Encryption engine
      /// </summary>
      private Rijndael RijndaelAlg;

      /// <summary>
      /// Key
      /// </summary>
      private byte[] mKey;

      /// <summary>
      /// Intermediate
      /// </summary>
      private byte[] mIV;

      #endregion

      /// <summary>
      /// Class Constructor
      /// </summary>
      public AESEncryption()
      {
         RijndaelAlg = Rijndael.Create();
         mKey = RijndaelAlg.Key;
         mIV = RijndaelAlg.IV;

      }/* end constructor */


      /// <summary>
      /// Encrypt the source string using AES
      /// </summary>
      /// <param name="iSourceString">String to be encrypted</param>
      /// <returns>encrypted string64</returns>
      public String EncryptData( string iSourceString )
      {
         try
         {
            ExtractSecureKeysFromPW();
            byte[] encBytes = EncryptData( Encoding.UTF8.GetBytes( iSourceString ), mKey, mIV );
            return Convert.ToBase64String( encBytes );
         }
         catch ( Exception ex )
         {
            return "ERROR: " + ex.Message;
         }
      }/* end method */


      /// <summary>
      /// Decrypt an encrypted String64
      /// </summary>
      /// <param name="iEncryptedString">String to be decrypted</param>
      /// <returns>Original source string64</returns>
      public String DecryptData( string iEncryptedString )
      {
         try
         {
            ExtractSecureKeysFromPW();
            byte[] encBytes = Convert.FromBase64String( iEncryptedString );
            byte[] decBytes = DecryptData( encBytes, mKey, mIV );
            return Encoding.UTF8.GetString( decBytes, 0, decBytes.Length );
         }
         catch ( Exception ex )
         {
            return "ERROR: " + ex.Message;
         }
      }/* end method */


      /// <summary>
      /// Encrypt one byte array into another byte array using Rijndael AES
      /// </summary>
      /// <param name="iByteData">Un-encrypted data</param>
      /// <param name="iKey">Secure Key</param>
      /// <param name="iIV">Initialized vector</param>
      /// <returns></returns>
      private byte[] EncryptData( byte[] iByteData, byte[] iKey, byte[] iIV )
      {
         try
         {

            Rijndael RijndaelAlg = Rijndael.Create();

            using ( MemoryStream msEncrypt = new MemoryStream() )
            {
               using ( CryptoStream encStream = new CryptoStream( msEncrypt, RijndaelAlg.CreateEncryptor( iKey, iIV ), CryptoStreamMode.Write ) )
               {
                  encStream.Write( iByteData, 0, iByteData.Length );
                  encStream.FlushFinalBlock();
                  return msEncrypt.ToArray();
               }
            }
         }
         catch ( Exception ex )
         {
            return Encoding.UTF8.GetBytes( "ERROR: " + ex.Message );
         }

      }/* end method */


      /// <summary>
      /// Decrypt one byte array into another byte array using Rijndael AES
      /// </summary>
      /// <param name="iByteData">Encrypted data</param>
      /// <param name="iKey">Secure Key</param>
      /// <param name="iIV">Initialized vector</param>
      /// <returns></returns>
      private byte[] DecryptData( byte[] iByteData, byte[] iKey, byte[] iIV )
      {
         try
         {
            Rijndael RijndaelAlg = Rijndael.Create();
            using ( MemoryStream msDecrypt = new MemoryStream( iByteData ) )
            {
               using ( CryptoStream csDecrypt = new CryptoStream( msDecrypt, RijndaelAlg.CreateDecryptor( iKey, iIV ), CryptoStreamMode.Read ) )
               {
                  //
                  // Decrypted bytes will always be less than encrypted bytes, so
                  // length of the encrypted data will be big enouph for buffer.
                  //
                  byte[] fromEncrypt = new byte[ iByteData.Length ];

                  // Read as many bytes as possible.
                  int read = csDecrypt.Read( fromEncrypt, 0, fromEncrypt.Length );
                  if ( read < fromEncrypt.Length )
                  {
                     // Return a byte array of proper size.
                     byte[] clearBytes = new byte[ read ];
                     Buffer.BlockCopy( fromEncrypt, 0, clearBytes, 0, read );
                     return clearBytes;
                  }
                  return fromEncrypt;
               }
            }
         }
         catch ( Exception ex )
         {
            return Encoding.UTF8.GetBytes( "ERROR: " + ex.Message );
         }

      }/* end method */


      /// <summary>
      /// Converts a Byte Array into a single HEX string without
      /// any delimiting characters. On error this method will 
      /// return the characters "00"
      /// </summary>
      /// <param name="iByteBuffer">Byte Array to process</param>
      /// <returns>String of Bytes</returns>
      public static String ByteArrayToHexString( byte[] iByteBuffer )
      {
         try
         {
            StringBuilder hexString = new StringBuilder();
            foreach ( byte i in iByteBuffer )
            {
               hexString.Append( i.ToString( "X2" ) );
            }
            return hexString.ToString();
         }
         catch
         {
            return "00";
         }

      }/* end method */


      /// <summary>
      /// Converts a single HEX string to an array of bytes. On error
      /// this method will return a single byte (0x00)
      /// </summary>
      /// <param name="iHexString">Hex string</param>
      /// <returns>byte array</returns>
      public static byte[] HexStringToByteArray( string iHexString )
      {
         try
         {
            int byteLength = iHexString.Length / 2;
            byte[] bytes = new byte[ byteLength ];
            string hex;
            int j = 0;
            for ( int i = 0; i < bytes.Length; i++ )
            {
               hex = new String( new Char[] { iHexString[ j ], iHexString[ j + 1 ] } );
               bytes[ i ] = byte.Parse( hex, System.Globalization.NumberStyles.HexNumber );
               j = j + 2;
            }
            return bytes;
         }
         catch
         {
            byte[] fail = new byte[] { 0x00 };
            return fail;
         }

      }/* end method */


      /// <summary>
      /// Extract the contents of one array into another
      /// </summary>
      /// <param name="iSourceArray">Array containing all data</param>
      /// <param name="iDestinationArray">Array that will contain extracted data</param>
      /// <param name="iStart">Source array start point index</param>
      /// <param name="iBytes">Number of bytes to copy</param>
      /// <returns>false on error</returns>
      public static Boolean ExtractArray( byte[] iSourceArray,
                                         ref byte[] iDestinationArray,
                                         int iStart, int iBytes )
      {
         try
         {
            Array.Resize<byte>( ref iDestinationArray, iBytes );
            Array.Copy( iSourceArray, iStart, iDestinationArray, 0, iBytes );
            return true;
         }
         catch
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// This simple method extracts both the secure key and 
      /// the initialization variable from the secure password
      /// </summary>
      private void ExtractSecureKeysFromPW()
      {
         byte[] securePW = HexStringToByteArray( OPSLOCAL_HASH );
         int KeyLength = (int) securePW[ 0 ];
         ExtractArray( securePW, ref mKey, 1, KeyLength );
         ExtractArray( securePW, ref mIV, KeyLength + 1, ( securePW.Length - ( KeyLength + 1 ) ) );
      
      }/* end method */

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th November 2010
//  Add to project
//----------------------------------------------------------------------------


