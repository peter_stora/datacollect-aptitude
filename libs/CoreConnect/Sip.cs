﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Globalization;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   public static class Sip
   {
      #region Enums
      /// <summary>
      /// The device types
      /// </summary>
      private enum DeviceTypes
      {
         Unknown = 0,
         WindowsEmbedded = 1,
         WindowsMobile = 2
      }
      #endregion

      #region Class Members
      /// <summary>
      /// Flag indicating the SIP is currently shown
      /// </summary>
      private static bool sSipShown;

      /// <summary>
      /// Rectangle representing the area of the keyboard background
      /// </summary>
      private static Rectangle sKeyboardBackground;

      /// <summary>
      /// Rectangle representing the area of the keyboard
      /// </summary>
      private static Rectangle sKeyboard;

      /// <summary>
      /// Used to access the current culture
      /// </summary>
      private static bool sOsSupportsMove;

      /// <summary>
      /// Used to show and hide the SIP button
      /// </summary>
      private const int SIPF_ON = 1;
      private const int SIPF_OFF = 0;

      /// <summary>
      /// Window class name for the SIP button
      /// </summary>
      private const string WINDOW_SIP_BUTTON = "MS_SIPBUTTON";

      /// <summary>
      /// Window class names for the keyboard
      /// </summary>
      private const string WINDOW_SIP_WINDOW = "SipWndClass";
      private const string WINDOW_SIP_BD = "SipBackDropWndClass";

      /// <summary>
      /// NSL builds that don't support moving the SIP
      /// </summary>
      private const string ISO_CHINESE = "zh";
      private const string ISO_KOREAN  = "ko";
      #endregion

      #region Properties
      /// <summary>
      /// Gets whether or not the SIP is shown
      /// </summary>
      public static bool SipShown
      {
         get { return sSipShown; }
      }
      #endregion

      #region Constructor
      /// <summary>
      /// Constructor
      /// </summary>
      static Sip()
      {
         // some localized OS versions have an issue moving SIP
         sOsSupportsMove = Os_Supports_Sip_Move( );
      }
      #endregion

      #region Public Methods
      /// <summary>
      /// Toggle the sip
      /// </summary>
      /// <param name="iShow">True to show, false to hide</param>
      public static void Show_Keyboard( bool iShow )
      {
         var type = DeviceTypes.WindowsMobile;

         if( DeviceInfo.IsWindowsEmbeddedOrNewer )
            type = DeviceTypes.WindowsEmbedded;

         if( iShow && !sSipShown )
         {
            Enable_Keyboard( true, Get_Keyboard_Move( type ) );
            sSipShown = true;
         }
         else if( !iShow && sSipShown )
         {
            Enable_Keyboard( false, Get_Keyboard_Move( type ) );
            sSipShown = false;
         }
      }

      /// <summary>
      /// Hide the SIP button
      /// </summary>
      /// <returns>True if hidden OK</returns>
      public static bool Hide_Sip_Button( )
      {
         var result = false;
         var hTaskBarWindow = WinCE.FindWindowCE( WINDOW_SIP_BUTTON, null );
         if( hTaskBarWindow != IntPtr.Zero )
         {
            // get the window rectangle
            Rectangle rect;

            if( Get_Window_Rectangle( hTaskBarWindow, out rect ) )
            {
               WinCE.MoveWindow( hTaskBarWindow, rect.X, rect.Y + 1000, rect.Width, rect.Height, true );
               result = true;
            }
         }

         return result;
      }
      #endregion

      #region Private Methods
      /// <summary>
      /// Gets how much to move the keyboard (vertically)
      /// </summary>
      /// <param name="iDeviceType">The device type</param>
      /// <returns>the vertical move needed for the device</returns>
      private static int Get_Keyboard_Move( DeviceTypes iDeviceType )
      {
         var shiftValue = 0;

         if( iDeviceType == DeviceTypes.WindowsEmbedded )
         {
            // don't move for the CN70
            shiftValue = 20;
         }
         else if( iDeviceType == DeviceTypes.WindowsMobile )
         {
            shiftValue = 0;
         }

         return shiftValue;
      }

      /// <summary>
      /// Enable/disable the SIP button
      /// </summary>
      /// <param name="iEnable">True to enable, false to disable</param>
      /// <param name="iPixelsToMove">How many vertical pixels to move the 
      /// keyboard</param>
      private static void Enable_Keyboard( bool iEnable, int iPixelsToMove )
      {
         if( iEnable )
         {
            // show keyboard
            WinCE.SipShowIM( SIPF_ON );

            // hide native SIP button in case it is shown
            Hide_Sip_Button( );

            // slide the SIP to fit with our UI
            if( sOsSupportsMove && ( iPixelsToMove != 0 ) )
            {
               Move_Keyboard_Vertically( iPixelsToMove );
            }
         }
         else
         {
            // hide keyboard
            WinCE.SipShowIM( SIPF_OFF );

            // restore the SIPs location in case we moved it
            if( sOsSupportsMove && iPixelsToMove != 0 )
            {
               Restore_Keyboard( );
            }
         }
      }

      /// <summary>
      /// Moves the keyboard vertically
      /// </summary>
      /// <param name="iPixelsToMove">How many vertical pixels to move.  Positive
      /// moves down</param>
      private static void Move_Keyboard_Vertically( int iPixelsToMove )
      {
         var result = false;

         var hSipWindow = WinCE.FindWindowCE( WINDOW_SIP_WINDOW, null );
         if( hSipWindow != IntPtr.Zero )
         {
            // get the window rectangle
            Rectangle rect;
            if( Get_Window_Rectangle( hSipWindow, out rect ) )
            {
               //save previous state
               sKeyboard = rect;

               WinCE.MoveWindow( hSipWindow, rect.X, rect.Y + iPixelsToMove, rect.Width, rect.Height, true );

               result = true;
            }
         }

         if( !result )
            return;

         var hSipWindowBackDrop = WinCE.FindWindowCE( WINDOW_SIP_BD, null );
         if( hSipWindowBackDrop != IntPtr.Zero )
         {
            Rectangle rect;

            if( Get_Window_Rectangle( hSipWindowBackDrop, out rect ) )
            {
               //save previous state
               sKeyboardBackground = rect;

               WinCE.MoveWindow( hSipWindowBackDrop, rect.X, rect.Y + iPixelsToMove, rect.Width, rect.Height, true );
            }
         }
      }

      /// <summary>
      /// Restores the keyboard to original location
      /// </summary>
      private static void Restore_Keyboard( )
      {
         IntPtr hSipWindow = WinCE.FindWindowCE( WINDOW_SIP_WINDOW, null );
         if( hSipWindow != IntPtr.Zero && sKeyboard.Height > 0 && sKeyboard.Width > 0 )
         {
            WinCE.MoveWindow( hSipWindow, sKeyboard.X, sKeyboard.Y, sKeyboard.Width, sKeyboard.Height, true );
         }

         IntPtr hSipWindow2 = WinCE.FindWindowCE( WINDOW_SIP_BD, null );
         if( hSipWindow2 != IntPtr.Zero && sKeyboardBackground.Height > 0 && sKeyboardBackground.Width > 0 )
         {
            WinCE.MoveWindow( hSipWindow2, sKeyboardBackground.X, sKeyboardBackground.Y, sKeyboardBackground.Width, sKeyboardBackground.Height, true );
         }
      }

      /// <summary>
      /// Get a rectangle the size of the window
      /// </summary>
      /// <param name="hWindow">The handle to the window</param>
      /// <param name="oRectangle">The rectangle representing the rectangle area</param>
      /// <returns>A rectangle</returns>
      private static bool Get_Window_Rectangle( IntPtr hWindow, out Rectangle oRectangle )
      {
         var result = false;
         WinCE.RECT rectangle;
         oRectangle = new Rectangle( );

         if( WinCE.GetWindowRect( hWindow, out rectangle ) )
         {
            oRectangle.X = rectangle.left;
            oRectangle.Y = rectangle.top;
            oRectangle.Width = ( rectangle.right - rectangle.left + 1 );
            oRectangle.Height = ( rectangle.bottom - rectangle.top + 1 );
            result = true;
         }

         return result;
      }

      /// <summary>
      /// Determines if the Operating System supports moving the SIP... This is a workaround
      /// for an apparent Windows issue... when the SIP is moved on these versions it 
      /// gets reset to location of 0,0 (top of screen)
      /// </summary>
      /// <returns>True if SIP move is supported</returns>
      private static bool Os_Supports_Sip_Move()
      {
         bool result = true;

         //
         // Windows Embedded 6.5.3 Korean and Chinese builds have issues when
         // the SIP is moved... OTD# 4532
         //
         if( DeviceInfo.IsWindowsEmbeddedOrNewer )
         {
            var isoCode = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

            // Korean, Simplified Chinese and Traditional Chinese don't support moving
            if( isoCode == ISO_CHINESE || isoCode == ISO_KOREAN )
               result = false;
         }
         return result;
      }
      #endregion
   }
}
