﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
    /// <summary>
    /// This Interface manages the OperatorSettings\Messaging
    /// Preferences Registry Key
    /// </summary>
    public interface IMessagingPrefsRegistryKey
    {
        /// <summary>
        /// Get or Set whether the operator should see the "End of Route" message
        /// </summary>
        bool EndOfRoute { get; set; }

        /// <summary>
        /// Get or Set whether the operator should see the "Found in other Route" message
        /// </summary>
        bool FoundInOtherRoute { get; set; }

        /// <summary>
        /// Get or Set whether the operator should see the "save as current" message
        /// </summary>
        bool SaveDataAsCurrent { get; set; }

        /// <summary>
        /// Get or Set whether the operator should see sets while collecting Route data
        /// </summary>
        bool ShowSetsWhileCollecting { get; set; }


        /// <summary>
        /// Get or Set whether to display message when WMCD returns zero Accl value
        /// </summary>
        bool ShowZeroAcclWarning { get; set; }

    }/* end interface */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 20th June 2009
//  Add to project
//----------------------------------------------------------------------------