﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Used to ensure there's only a single instance of the application running
   /// </summary>
   public class AppExecutionManager : IDisposable
   {
      #region Class Members
      /// <summary>
      /// Event to ensure single instance of app
      /// </summary>
      private readonly IntPtr mEvent;

      /// <summary>
      /// Flag indicating this is the first instance of the application
      /// </summary>
      private readonly bool mIsFirstInstance;
      #endregion

      #region Properties
      /// <summary>
      /// Flag indicating this is the first instance of the application
      /// </summary>
      public bool IsFirstInstance
      {
         get { return mIsFirstInstance; }
      }
      #endregion

      #region Constructor
      /// <summary>
      /// Static constructor
      /// </summary>
      /// <param name="iApplicationName">The application name</param>
      public AppExecutionManager( string iApplicationName )
      {
         mEvent = WinCE.CreateEvent( IntPtr.Zero, true, false, iApplicationName + "Event" );
         mIsFirstInstance = Marshal.GetLastWin32Error( ) == 0;
      }
      #endregion

      #region Methods
      /// <summary>
      /// IDisposable implementation
      /// </summary>
      public void Dispose( )
      {
         if( mEvent != IntPtr.Zero )
         {
            WinCE.CloseHandle( mEvent );
         }
      }
      #endregion
   }
}
