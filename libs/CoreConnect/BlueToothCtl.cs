﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 - 2014 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// This class provides a number of static methods, allowing devices 
   /// supporting the Microsoft Bluetooth Stack, to have their Bluetooth 
   /// hardware switched ON or OFF. 
   /// 
   /// As devices that use the Widcomm and StoneStreet stacks are never 
   /// really off (their concept of "OFF" is "Connectable with Power Down
   /// on Timeout or Disconnect"), this class can do little more than try
   /// to correctly identify their power state.
   /// 
   /// Where things get really messy is with Microsoft's BthSetMode() and 
   /// BthGetMode() commands. Using these it possible to set Bluetooth to 
   /// either OFF, ON as "Connectable", or ON as both "Connectable and 
   /// Discoverable".
   /// 
   /// However, Intermec devices do not support BthSetMode, and only allow 
   /// control via IBT_ON and IBT_OFF, which means that when IBT_ON is set,
   /// power consumption is determined by whether the operator has left the
   /// checkbox "discoverable" ticked! 
   /// 
   /// For some reason that is not clear, Unitech devices always return
   /// BthGetHardwareStatus as HCI_HARDWARE_ERROR. However if we can still
   /// determine the connection mode, then it's still safe to assume that
   /// we're dealing with a Microsoft Stack
   /// </summary>
   public static class BlueToothCtl
   {
      #region Device Constants

      /// <summary>
      /// Intermec Bluetooth Library (including path)
      /// </summary>
      private const string INTERMEC_LIBRARY_PATH = @"\Windows\IBT.dll";

      /// <summary>
      /// Widcomm Version 3.0 Bluetooth Library (including path)
      /// </summary>
      private const string WIDCOMM_30_LIBRARY_PATH = @"\Windows\BtSdkCE30.dll";

      /// <summary>
      /// Widcomm Version 5.0 Bluetooth Library (including path)
      /// </summary>
      private const string WIDCOMM_50_LIBRARY_PATH = @"\Windows\BtSdkCE50.dll";

      /// <summary>
      /// StoneStreet One Bluetooth Library (including path)
      /// </summary>
      private const string BLUETOOTH_EXPLORER_LIB = @"\Windows\BTExplorer.exe";

      /// <summary>
      /// Required initialization period after Bluetooth Power On 
      /// </summary>
      private const int POWER_ON_SLEEP_MS = 2000;

      /// <summary>
      /// Registry Branch
      /// </summary>
      private static string SYMBOL_BLUETOOTH_BRANCH = "Software";
      
      /// <summary>
      /// Registry Key for newer Motorola device for Bluetooth control
      /// </summary>
      private static string SYMBOL_BLUETOOTH_KEY = "SymbolBluetooth";
      
      /// <summary>
      /// Registy Value for newer Motorola devoces which is used to
      /// switch the handheld between using Microsoft and StoneStreet
      /// Stack types (although it's an either \ or setting)
      /// </summary>
      private static string SYMBOL_BLUETOOTH_VALUE = "SSStack";

      #endregion

      /// <summary>
      /// Specifies the current status of the Bluetooth radio modes
      /// </summary>
      public enum RadioMode
      {
         /// <summary>
         /// Radio connection is off
         /// </summary>
         Off = 0x00,
         /// <summary>
         /// CE Device can connect to BT clients
         /// </summary>
         Connectable = 0x01,
         /// <summary>
         /// CE Device can be seen by other BT devices
         /// </summary>
         Discoverable = 0x02,
         /// <summary>
         /// Bluetooth device is discoverable and connectable 
         /// (StoneStreet only)
         /// </summary>
         DiscoverableAndConnectable = 0x03,
      }

      /// <summary>
      /// Specifies the current status of the Bluetooth hardware.
      /// </summary>
      public enum HardwareStatus : int
      {
         /// <summary>
         /// Status cannot be determined.
         /// </summary>
         Unknown = 0x00,
         /// <summary>
         /// Bluetooth radio not present.
         /// </summary>
         NotPresent = 0x01,
         /// <summary>
         /// Bluetooth radio is in the process of starting up.
         /// </summary>
         Initializing = 0x02,
         /// <summary>
         /// Bluetooth radio is active.
         /// </summary>
         Running = 0x03,
         /// <summary>
         /// Bluetooth radio is in the process of shutting down.
         /// </summary>
         Shutdown = 0x04,
         /// <summary>
         /// Bluetooth radio is in an error state.
         /// </summary>
         Error = 0x05,
      }


      /// <summary>
      /// Support the switchable Stack types with Windows 6.5
      /// Motorola handhelds (such as the MC9500, MC75, MC92N0)
      /// </summary>
      public enum MotorolaSS
      {
         /// <summary>
         /// Microsoft Bluetooth Stack
         /// </summary>
         Microsoft = 0x00,

         /// <summary>
         /// StoneStreet One Bluetooth Stack
         /// </summary>
         StoneStreet = 0x01
      }


      /// <summary>
      /// Specifies the supported Bluetooth hardware stacks
      /// </summary>
      public enum DeviceStack
      {
         /// <summary>
         /// Status cannot be determined.
         /// </summary>
         Unknown = 0x00,
         /// <summary>
         /// StoneStreet One for BARTEC/Motorola
         /// </summary>
         StoneStreetMotorola = 0x01,
         /// <summary>
         /// Microsoft Standard (i.e. HTC)
         /// </summary>
         MicrosoftStandard = 0x02,
         /// <summary>
         /// Microsoft customized for Motorola (i.e. MC92N0, MC75)
         /// </summary>
         MicrosoftMotorola = 0x03,
         /// <summary>
         /// Microsoft customized for Intermec (i.e CN3e)
         /// </summary>
         MicrosoftIntermec = 0x04,
         /// <summary>
         /// Widcom standard for HP, RECON, DELL etc
         /// </summary>
         WidcommStandard = 0x05
      }


      /// <summary>
      /// Specifies the Widcomm Stack Version
      /// </summary>
      public enum WidcommVersion
      {
         /// <summary>
         /// Type cannot be determined.
         /// </summary>
         Unknown = 0x00,
         /// <summary>
         /// Pocket PC 2003
         /// </summary>
         CE30 = 0x01,
         /// <summary>
         /// Windows Mobile 5.0
         /// </summary>
         CE50 = 0x02,
      }


      /// <summary>
      /// Switch ON or OFF the Bluetooth wireless on an Intermec device.
      /// </summary>
      /// <param name="iEnabled">TRUE to switch ON</param>
      /// <returns>true if success, else false on error or exception</returns>
      public static Boolean SetIntermecRadioMode( Boolean iEnabled )
      {
         if ( iEnabled )
         {
            try
            {
               if ( WinCE.IntermecBT_On() == 0 )
                  return true;
               else
                  return false;
            }
            catch
            {
               return false;
            }
         }
         else
         {
            try
            {
               if ( WinCE.IntermecBT_Off() == 0 )
                  return true;
               else
                  return false;
            }
            catch
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Switch ON or OFF the Bluetooth wireless on a Microsoft device.
      /// </summary>
      /// <param name="iEnabled">TRUE to switch ON</param>
      /// <returns>true if success, else false on error or exception</returns>
      public static Boolean SetMicrosoftRadioMode( Boolean iEnabled )
      {
         if ( iEnabled )
         {
            try
            {
               if ( WinCE.BthSetMode( WinCE.BTH_RADIO_MODE.BTH_DISCOVERABLE ) == 0 )
                  return true;
               else
                  return false;
            }
            catch
            {
               return false;
            }
         }
         else
         {
            try
            {
               if ( WinCE.BthSetMode( WinCE.BTH_RADIO_MODE.BTH_POWER_OFF ) == 0 )
                  return true;
               else
                  return false;
            }
            catch
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Returns the current radio mode on Microsoft stack devices 
      /// Note: This includes the Intermec range but "NOT" StoneStreet!
      /// </summary>
      /// <param name="iMode">returned radio mode</param>
      /// <returns>true if success, else false on error or exception</returns>
      public static Boolean GetMicrosoftRadioMode( ref RadioMode iMode )
      {
         WinCE.BTH_RADIO_MODE mode = WinCE.BTH_RADIO_MODE.BTH_POWER_OFF;
         try
         {
            if ( WinCE.BthGetMode( out mode ) == 0 )
            {
               switch ( mode )
               {
                  case WinCE.BTH_RADIO_MODE.BTH_POWER_OFF:
                     iMode = RadioMode.Off;
                     break;
                  case WinCE.BTH_RADIO_MODE.BTH_CONNECTABLE:
                     iMode = RadioMode.Connectable;
                     break;
                  case WinCE.BTH_RADIO_MODE.BTH_DISCOVERABLE:
                     iMode = RadioMode.Discoverable;
                     break;
               }
               return true;
            }
            else
            {
               return false;
            }
         }
         catch
         {
            return false;
         }
      }


      /// <summary>
      /// Returns the current radio mode on StoneStreet stack devices 
      /// </summary>
      /// <param name="oMode"></param>
      /// <returns></returns>
      public static Boolean GetStoneStreetRadioMode( ref RadioMode oMode )
      {
         //
         // first, let's check and see if we're dealing with one of the newer
         // Windows Mobile 6.5 Motorola devices (MC65, MC92N0 etc).
         // These handhelds typically come with two built in Bluetooth stacks: 
         // StoneStreet One and Microsoft. So lets check this first!
         //
         DeviceStack stack = StackIsStoneStreetOrMicrosoft();

         // OK, if the stack type is coming back as Microsoft, we're done!
         if ( stack == DeviceStack.MicrosoftMotorola ) 
         {
            return false;
         }

         // if the current stack is not StoneStreet, exit right here!
         if ( !StackIsMotorolaStoneStreet() )
         {
            return false;
         }

         // set a default results
         oMode = RadioMode.DiscoverableAndConnectable;
         return true;
      }


      /// <summary>
      /// Returns the current hardware status on Microsoft stack devices 
      /// Note: This includes the Intermec range but "NOT" StoneStreet!
      /// </summary>
      /// <param name="iStatus">returned hardware status</param>
      /// <returns>true if success, else false on error or exception</returns>
      public static Boolean GetMicrosoftHardwareStatus( ref HardwareStatus iStatus )
      {
         WinCE.BTH_PI_STATUS status = WinCE.BTH_PI_STATUS.HCI_HARDWARE_UNKNOWN;
         try
         {
            if ( WinCE.BthGetHardwareStatus( out status ) == 0 )
            {
               switch ( status )
               {
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_ERROR:
                     iStatus = HardwareStatus.Error;
                     break;
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_INITIALIZING:
                     iStatus = HardwareStatus.Initializing;
                     break;
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_NOT_PRESENT:
                     iStatus = HardwareStatus.NotPresent;
                     break;
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_RUNNING:
                     iStatus = HardwareStatus.Running;
                     break;
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_SHUTDOWN:
                     iStatus = HardwareStatus.Shutdown;
                     break;
                  case WinCE.BTH_PI_STATUS.HCI_HARDWARE_UNKNOWN:
                     iStatus = HardwareStatus.Unknown;
                     break;
               }
               return true;
            }
            else
            {
               return false;
            }
         }
         catch
         {
            return false;
         }
      }


      /// <summary>
      /// Checks device for presence of Widcomm Bluetooth Stack
      /// </summary>
      /// <returns>true if Widcomm, else false</returns>
      public static bool StackIsWidcomm( out WidcommVersion iCEVersion )
      {
         if ( File.Exists( WIDCOMM_30_LIBRARY_PATH ) )
         {
            iCEVersion = WidcommVersion.CE30;
            return true;
         }
         else if ( File.Exists( WIDCOMM_50_LIBRARY_PATH ) )
         {
            iCEVersion = WidcommVersion.CE50;
            return true;
         }
         else
         {
            iCEVersion = WidcommVersion.Unknown;
            return false;
         }
      }


      /// <summary>
      /// Checks device for presence of dedicated Intermec controls
      /// </summary>
      /// <returns>true if device is Intermec, else false</returns>
      public static Boolean StackIsMicrosoftIntermec()
      {
         //
         // As "ibt.dll" is factory installed on all Intermec computers, the
         // presence of this file is enough to indicate that we are on an
         // Interemc box.  Do not use resources for this!
         //
         if ( File.Exists( INTERMEC_LIBRARY_PATH ) )
         {
            return true;
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Checks device for presence of StoneStreet One Bluetooth Stack
      /// </summary>
      /// <returns>true if Widcomm, else false</returns>
      public static Boolean StackIsMotorolaStoneStreet()
      {
         if ( File.Exists( BLUETOOTH_EXPLORER_LIB ) )
         {
            return true;
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Checks to see if we're dealing with one of the Windows Mobile 6.5 
      /// Motorola devices. These handhelds come with two built in Bluetooth 
      /// stacks: StoneStreet One and Microsoft
      /// </summary>
      /// <returns>Detected Motorola stack type or type unknown</returns>
      private static DeviceStack StackIsStoneStreetOrMicrosoft()
      {
         RegistryIO reg = new RegistryIO();
         int oValue = (int)DeviceStack.Unknown;

         if ( reg.GetValue( 
            RegistryIO.RegistryHive.HKEY_LOCAL_MACHINE,
            SYMBOL_BLUETOOTH_BRANCH,
            SYMBOL_BLUETOOTH_KEY,
            SYMBOL_BLUETOOTH_VALUE,
            ref oValue ) )
         {
            if ( oValue == (int)MotorolaSS.Microsoft )
            {
               return DeviceStack.MicrosoftMotorola;
            }
            else if ( oValue == (int) MotorolaSS.StoneStreet )
            {
               return DeviceStack.StoneStreetMotorola;
            }
            else
            {
               return DeviceStack.Unknown;
            }
         }
         else
         {
            return DeviceStack.Unknown;
         }
      }


      /// <summary>
      /// Make a "wild stab" at trying to determine the stack type. 
      /// Note: Although this will work for some of the known devices,
      /// we have absolutely no idea what it will do on unknown ones!
      /// </summary>
      /// <returns>combination stack type and manufacturer</returns>
      public static DeviceStack GetDeviceStackType()
      {
         // set the default as unknown
         DeviceStack result = DeviceStack.Unknown;
         WidcommVersion widCEVersion = WidcommVersion.Unknown;
         WinCE.BTH_PI_STATUS status = WinCE.BTH_PI_STATUS.HCI_HARDWARE_UNKNOWN;
         WinCE.BTH_RADIO_MODE mode = WinCE.BTH_RADIO_MODE.BTH_POWER_OFF;

         //
         // first, let's check and see if we're dealing with one of the newer
         // Windows Mobile 6.5 Motorola devices (MC65, MC92N0 etc).
         // These handhelds typically come with two built in Bluetooth stacks: 
         // StoneStreet One and Microsoft. So lets check this first!
         //
         DeviceStack stack = StackIsStoneStreetOrMicrosoft();

         // OK, if the stack type was recognised, then return and exit here
         if ( ( stack == DeviceStack.MicrosoftMotorola ) ||
            ( stack == DeviceStack.StoneStreetMotorola ) )
         {
            return stack;
         }

         //
         // well if it's not a new device, check for stack as StoneStreet
         // on one of the older Motorola \ Symbol handheld (MC9090)
         //
         if ( StackIsMotorolaStoneStreet() )
         {
            return DeviceStack.StoneStreetMotorola;
         }

         // check for stack as Widcomm/Broadcomm
         else if ( StackIsWidcomm( out widCEVersion ) )
         {
            //
            // if we're using CE5.0 then we need to make sure that
            // the device is not using the Microsoft Stack
            //
            if ( widCEVersion == WidcommVersion.CE50 )
            {
               if ( ( WinCE.BthGetHardwareStatus( out status ) != 0 ) &
                  ( WinCE.BthGetMode( out mode ) != 0 ) )
               {
                  return DeviceStack.WidcommStandard;
               }
            }
            // for CE3.0 we can safely assme it's Widcomm only
            else if ( widCEVersion == WidcommVersion.CE30 )
            {
               return DeviceStack.WidcommStandard;
            }
         }

         // check as Microsoft (Intermec) stack
         else if ( StackIsMicrosoftIntermec() )
         {
            return DeviceStack.MicrosoftIntermec;
         }

         // check as default Microsoft stack
         else
         {
            // query the Btdrt.dll Microsoft Windows library
            if ( WinCE.BthGetHardwareStatus( out status ) != WinCE.E_FAIL )
            {
               if ( WinCE.BthGetMode( out mode ) != WinCE.E_FAIL )
               {
                  // is bluetooth running and connectable
                  bool runningConnectableDiscoverable = 
                        status == WinCE.BTH_PI_STATUS.HCI_HARDWARE_RUNNING &
                        ( mode == WinCE.BTH_RADIO_MODE.BTH_CONNECTABLE |
                        mode == WinCE.BTH_RADIO_MODE.BTH_DISCOVERABLE );

                  // or shutdown with a power off
                  bool shutdownPowerOff = 
                        status == WinCE.BTH_PI_STATUS.HCI_HARDWARE_SHUTDOWN &
                        mode == WinCE.BTH_RADIO_MODE.BTH_POWER_OFF;

                  //
                  // or just connectable and discoverable - but with 
                  // an error hardware status (aka Unitech devices)
                  //
                  bool hardwareErrorButConnectableDiscoverable = 
                        status == WinCE.BTH_PI_STATUS.HCI_HARDWARE_ERROR &
                        ( mode == WinCE.BTH_RADIO_MODE.BTH_CONNECTABLE |
                        mode == WinCE.BTH_RADIO_MODE.BTH_DISCOVERABLE );

                  // if any state is true, then this is a Microsoft Stack!
                  if ( runningConnectableDiscoverable |
                        shutdownPowerOff |
                        hardwareErrorButConnectableDiscoverable )
                  {
                     return DeviceStack.MicrosoftStandard;
                  }
               }
            }
         }

         // if we've reached here then concede defeat and return "unknown"
         return result;
      }


      /// <summary>
      /// Is the Microsoft stack currently connectable?
      /// </summary>
      /// <returns>true if yes</returns>
      public static Boolean MicrosoftRadioConnectable()
      {
         Boolean connectable = false;
         RadioMode radio = RadioMode.Off;
         if ( GetMicrosoftRadioMode( ref radio ) )
         {
            if ( ( radio == RadioMode.Connectable ) |
            ( radio == RadioMode.DiscoverableAndConnectable ) |
            ( radio == RadioMode.Discoverable ) )
            {
               connectable = true;
            }
         }
         return connectable;
      }


      /// <summary>
      /// Is the StoneStreet stack currently connectable?
      /// </summary>
      /// <returns>true if yes</returns>
      public static Boolean StoneStreetRadioConnectable()
      {
         Boolean connectable = false;
         RadioMode radio = RadioMode.Off;
         if ( GetStoneStreetRadioMode( ref radio ) )
         {
            if ( ( radio == RadioMode.Connectable ) |
            ( radio == RadioMode.DiscoverableAndConnectable ) )
            {
               connectable = true;
            }
         }
         return connectable;
      }


      /// <summary>
      /// Widcomm stack is always connectable
      /// </summary>
      /// <returns>always yes</returns>
      public static Boolean WidcommRadioConnectable()
      {
         return true;
      }


      /// <summary>
      /// When stack is Microsoft (either Standard or Intermec) check
      /// the RadoMode is connectable. If not, manually set it and then
      /// check again. If the test fails a second time, return false
      /// </summary>
      /// <returns>True is RadioMode is connectable, otherwise false</returns>
      public static Boolean RestartMicrosoftStack()
      {
         //
         // if the devices radio is currently switched OFF then
         // switch ON here. Note: As the Intermec IDL overrides
         // the Microsoft controls, this needs to be handled by
         // by itself (using IBT.DLL)
         //
         if ( !BlueToothCtl.MicrosoftRadioConnectable() )
         {
            if ( StackIsMicrosoftIntermec() )
            {
               BlueToothCtl.SetIntermecRadioMode( true );
            }
            else
            {
               BlueToothCtl.SetMicrosoftRadioMode( true );
            }
            // wait two seconds after a power on sequence
            System.Threading.Thread.Sleep( POWER_ON_SLEEP_MS );
         }

         //
         // now try again. If the result is still a failure then 
         // something is clealy wrong - somthing that is outside
         // the control of this application
         //
         if ( BlueToothCtl.MicrosoftRadioConnectable() )
         {
            return true;
         }
         else
         {
            // return failure result
            return false;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  BluetoothCtl.cs
//
//  Revision 3.0 APinkerton 25th February 2014
//  Extend Microsft Stack detection for Motorola MC92XX and MC75 devices
//  which support dual stack functionality (Stonestreet and Microsoft)
//
//  Revision 2.0 APinkerton 27th November 2012
//  Extend Microsft Stack detection for Unitech devices
//
//  Revision 1.0 APinkerton 1st November 2011
//  Remove dedicated support for BTExlporer
//
//  Revision 0.0 APinkerton 8th May 2010
//  Add to project
//----------------------------------------------------------------------------
