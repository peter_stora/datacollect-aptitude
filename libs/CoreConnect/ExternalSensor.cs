﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Static class that contains external sensor enumerator types
   /// </summary>
   public static class ExternalSensor
   {
      /// <summary>
      /// External vibration sensor types
      /// </summary>
      public enum Vibration
      {
         /// <summary>
         /// Default "fail state" return value
         /// </summary>
         None = 0x00,

         /// <summary>
         /// Sensor is Serial Port MCD or VibPak
         /// </summary>
         MCD = 0x01,

         /// <summary>
         /// Sensor is Bluetooth WMCD
         /// </summary>
         WMCD = 0x02,
      }

      /// <summary>
      /// External temperature sensor (gun) types
      /// </summary>
      public enum Temperature
      {
         /// <summary>
         /// Default "fail state" return value
         /// </summary>
         None = 0x00,

         /// <summary>
         /// Sensor
         /// </summary>
         Wired = 0x01,

         /// <summary>
         /// 
         /// </summary>
         Bluetooth = 0x02,
      }

      /// <summary>
      /// Gets or sets the external vibration sensor type
      /// </summary>
      public static Vibration VibrationSensorType
      {
         get
         {
            try
            {
               IRegistryConnect mReg = new RegistryConnect();
               return mReg.ExtVibSensor;
            }
            catch
            {
               return Vibration.WMCD;
            }
         }
         set
         {
            IRegistryConnect mReg = new RegistryConnect();
            mReg.ExtVibSensor = value;
         }
      }

      /// <summary>
      /// Gets or sets the external temperature sensor type
      /// </summary>
      public static Temperature TemperatureSensorType
      {
         get
         {
            try
            {
               IRegistryConnect mReg = new RegistryConnect();
               return mReg.ExtTempSensor;
            }
            catch
            {
               return Temperature.None;
            }
         }
         set
         {
            IRegistryConnect mReg = new RegistryConnect();
            mReg.ExtTempSensor = value;
         }
      }

      /// <summary>
      /// Gets or sets the TempGun Bluetooth address
      /// </summary>
      public static byte[] TempGunBTAdress
      {
         get
         {
            IRegistryConnect mReg = new RegistryConnect();
            return mReg.TempGun_BTAddress;
         }
         set
         {
            IRegistryConnect mReg = new RegistryConnect();
            mReg.TempGun_BTAddress = value;
         }
      }

      /// <summary>
      /// Gets or sets the TempGun Bluetooth name
      /// </summary>
      public static string TempGunBTName
      {
         get
         {
            IRegistryConnect mReg = new RegistryConnect();
            return mReg.TempGun_BTName;
         }
         set
         {
            IRegistryConnect mReg = new RegistryConnect();
            mReg.TempGun_BTName = value;
         }
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 17th February 2011
//  Add to project
//----------------------------------------------------------------------------