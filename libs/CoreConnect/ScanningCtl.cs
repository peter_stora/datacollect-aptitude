﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.IO;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// 
   /// </summary>
   public static class ScanningCtl
   {
      //-------------------------------------------------------------------------

      #region constants

      /// <summary>
      /// Registry branch within Intermec devices that is used for scanner support
      /// </summary>
      private const string INTERMEC_SCANNER_BRANCH = "\\Drivers";

      /// <summary>
      /// Registry key within Intermec that notifies presence of "Scan Manager"
      /// </summary>
      private static string INTERMEC_SCANNER_KEY = "Intermec";

      /// <summary>
      /// Registry branch within Symbol devices that is used for hardware support
      /// </summary>
      private static string SYMBOL_SCANNER_BRANCH = "\\Drivers\\BuiltIn";

      /// <summary>
      /// Registry key within Symbol that notifies Barcode support
      /// </summary>
      private static string SYMBOL_SCANNER_KEY = "BarcodeReader";

      /// <summary>
      /// Directory for the Smart Systems driver on Intermec devices.  If the
      /// device lacks this directory, it is a legacy OS and barcode isn't
      /// fully supported.
      /// </summary>
      private static string INTERMEC_SS_BRANCH = "\\SmartSystems";

      /// <summary>
      /// Registry branch within Unitech devices that is used for hardware support
      /// </summary>
      private static string UNITECH_SCANNER_BRANCH = "\\Software\\Unitech America Inc.\\Scanner";

      /// <summary>
      /// Registry key within Unitech that notifies Barcode support
      /// </summary>
      private static string UNITECH_SCANNER_KEY = "Settings";

      /// <summary>
      /// Registry branch within ECOM Ci70 devices that describes how the OEM 
      /// settings work along side the existing Intermec hardware
      /// </summary>
      private static string ECOMCI70_SCANNER_BRANCH = "\\Software\\ECOM";

      /// <summary>
      /// Registry key that identifies the device as ECOM 
      /// </summary>
      private static string ECOMCI70_SCANNER_KEY = "ECOMDeviceConfiglet";

      /// <summary>
      /// Registry key within ECOMDeviceConfiglet that details what type of 
      /// scanner is being used in Head Assembly
      /// 0x00 = NO DEVICE
      /// 0x01 = Symbol SE955 1D  Scanner
      /// 0x02 = Intermec EA15 2D Scanner 
      /// 0x03 = Intermec EX25 2D Scanner \ Imager
      /// </summary>
      private static string ECOMCI70_SCANNER_VALUE = "UART Device";

      /// <summary>
      /// Registry key within ECOMDeviceConfiglet that details what type of 
      /// RFID reader is being used in Head Assembly
      /// 0x00 = NO DEVICE
      /// 0x01 = L\F Tectus TLB30
      /// 0x02 = M\F Microsensys UNI13
      /// 0x03 = Q ARE8
      /// </summary>
      private static string ECOMCI70_RFID_VALUE = "USB Device";

      /// <summary>
      /// Registry branch within ECOM Ci70 devices that describes how the OEM 
      /// settings work along side the existing Intermec hardware
      /// </summary>
      private static string ECOMCI70_CONFIGLET_BRANCH = "\\Software\\ECOM\\ECOMDeviceConfiglet";

      /// <summary>
      /// Registry key for ECOM TLB30 LF RFID Head Module 
      /// </summary>
      private static string ECOM_TLB30_BRANCH = "RF_L_Settings";

      /// <summary>
      /// Registry key for ECOM UNI13 HF RFID Head Module 
      /// </summary>
      private static string ECOM_UNI13_BRANCH = "RF_H_Settings";

      /// <summary>
      /// TLB30 Tag Reader Mask
      /// </summary>
      private static string TLB30_READ_MASK =  "RF_L_Mask";

      /// <summary>
      /// UNI13 Tag Reader Mask
      /// </summary>
      private static string UNI13_READ_MASK =  "RF_H_Mask";

      /// <summary>
      /// Path for detecting if the Intermec RFID is supported
      /// </summary>
      private static string INTERMEC_RFID_SUPPORT_PATH = @"Software\Intermec\RFIDDevices";

      /// <summary>
      /// Path for detecting the particular Intermec RFID device
      /// </summary>
      private static string INTERMEC_RFID_DETECT_PATH = @"Software\Intermec\RFIDPorts";

      /// <summary>
      /// Path for determine what Intermec RFID port is being used
      /// </summary>
      private static string INTERMEC_RFID_PORT_KEY = "1";

      /// <summary>
      /// Holds the enabled RFID device
      /// </summary>
      private static string INTERMEC_RFID_ENABLED_DEVICE_VALUE = "EnabledDevice";

      /// <summary>
      /// Reg value for detecting IP30
      /// </summary>
      private const string INTERMEC_IP30 = "IP30";

      /// <summary>
      /// Reg value for detecting IP30
      /// </summary>
      private const string INTERMEC_IM11 = "IM11";

      /// <summary>
      /// Registry key for the PA690 HF RFID reader
      /// </summary>
      public const string REG_BRANCH_PA690_HF = @"Software";

      /// <summary>
      /// Registry key for the PA690 HF RFID reader
      /// </summary>
      public const string REG_KEY_PA690_HF = @"RFID_HF";

      /// <summary>
      /// Key holding a flag to whether or not the IP30 is supported
      /// </summary>
      private static string INSTALL_KEY = "IP30";
      #endregion

      //-------------------------------------------------------------------------

      #region enumerator types

      /// <summary>
      /// Enumeration of ECOM Ci70 Serial Device heads
      /// </summary>
      internal enum ECOM_UART_DeviceHead
      {
         /// <summary>
         /// No barcode device is connected to the UART
         /// </summary>
         None = 0x00,

         /// <summary>
         /// Symbol SE955 1D scanner connected to UART
         /// </summary>
         SE955 = 0x01,

         /// <summary>
         /// Intermec EA15 2D scanner connected to UART
         /// </summary>
         EA15 = 0x02,

         /// <summary>
         /// Intermec EX25 Imager connected to UART
         /// </summary>
         EX25 = 0x03
      }

      /// <summary>
      /// Enumeration of ECOM Ci70 USB Device heads
      /// </summary>
      internal enum ECOM_USB_DeviceHead
      {
         /// <summary>
         /// No barcode device is connected to the UART
         /// </summary>
         None = 0x00,

         /// <summary>
         /// Tectus L/F (125 KHz Reader for Ci70)
         /// </summary>
         TLB30 = 0x01,

         /// <summary>
         /// Microsensys M/F (13.56 MHz reader for Ci70)
         /// </summary>
         UNI13 = 0x02,

         /// <summary>
         /// ECOM UHF (938 MHz reader for Ci70)
         /// </summary>
         ARE8 = 0x03
      }

      /// <summary>
      /// Enumeration of internally supported Barcode engines
      /// </summary>
      public enum BarcodeEngine: int
      {
         /// <summary>
         /// Device does not support barcode scanning
         /// </summary>
         None = 0x00,

         /// <summary>
         /// Device supports native Intermec Barcode scanner
         /// </summary>
         Intermec = 0x02,

         /// <summary>
         /// Device supports native Symbol/Motorola Barcode scanner
         /// </summary>
         Symbol = 0x03,

         /// <summary>
         /// Error occurred in initialization
         /// </summary>
         InitError = 0x04,

         /// <summary>
         /// Device supports native Unitech Barcode scanner
         /// </summary>
         Unitech = 0x05,

         /// <summary>
         /// Device supports native Symbol SE955 Barcode scanner (Ci70)
         /// </summary>
         EcomSE955 = 0x06,

         /// <summary>
         /// Device supports managed Intermec EA15 (2D) Barcode scanner (Ci70)
         /// </summary>
         EcomEA15 = 0x07,
         
         /// <summary>
         /// Device supports managed Intermec EX25 (2d) Imager / scanner (Ci70)
         /// </summary>
         EcomEX25 = 0x08,
      }

      /// <summary>
      /// Enumeration of supported RFID engines
      /// </summary>
      public enum RFIDEngine : int
      {
         /// <summary>
         /// Unknown status - device may support RFID, but cannot be confirmed
         /// </summary>
         Unknown = 0x00,

         /// <summary>
         /// Device does not support native RFID scanning
         /// </summary>
         None = 0x01,

         /// <summary>
         /// Device supports native Intermec IP30 RFID Reader
         /// </summary>
         IntermecIP30 = 0x02,

         /// <summary>
         /// Device supports native Intermec IM11 Internal Reader
         /// </summary>
         IntermecIM11 = 0x03,

         /// <summary>
         /// Device supports native Symbol/Motorola RFID scanner
         /// </summary>
         SymbolUHF = 0x10,

         /// <summary>
         /// Device supports internal Bartec L\F 125 KHz RFID scanner
         /// </summary>
         BartecLF = 0x20,

         /// <summary>
         /// Device supports internal Bartec H\F 15.25 MHz RFID scanner
         /// </summary>
         BartecHF = 0x21,

         /// <summary>
         /// Device supports internal Bartec UHF 865 MHz RFID scanner
         /// </summary>
         BartecUHF = 0x22,

         /// <summary>
         /// ECOM Device supports Head Module L/F (125 KHz Reader for Ci70)
         /// </summary>
         EcomTLB30 = 0x30,

         /// <summary>
         /// ECOM Device supports Head Module H/F (13.56 MHz reader for Ci70)
         /// </summary>
         EcomUNI13 = 0x31,

         /// <summary>
         /// ECOM Device supports Head "Q" (UHF pending)
         /// </summary>
         EcomARE8 = 0x32,

         /// <summary>
         /// Unitech H/F 13.56 MHz (ISO 15693, ISO 14443 A/B)
         /// </summary>
         Unitech = 0x40
      }

      #endregion

      //-------------------------------------------------------------------------

      #region Determine what barcode scanner engine is supported

      /// <summary>
      /// Does this device support native (internal) scanning? 
      /// </summary>
      /// <returns>true on native scanner support</returns>
      public static BarcodeEngine Supported_Barcode_Engine()
      {
         //---------------------------------------------------------------------
         // This method is the main static entry point for confirming whether
         // or not the connected device supports Barcode scanning. This method 
         // should only return a known type if the scanner hardware is internal.
         // If the scanner engine is external to the device and relies on
         // either USB or Bluetooth to provide connectivity, this method should
         // not return a known type.
         //---------------------------------------------------------------------

         var result = BarcodeEngine.None;

         bool hasIntermecBranch = WinCE.DoesSubKeyExist( 
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            INTERMEC_SCANNER_BRANCH,
            INTERMEC_SCANNER_KEY );

         bool hasSymbolBranch = WinCE.DoesSubKeyExist( 
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            SYMBOL_SCANNER_BRANCH,
            SYMBOL_SCANNER_KEY );

         bool hasUnitechBranch = WinCE.DoesSubKeyExist(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            UNITECH_SCANNER_BRANCH,
            UNITECH_SCANNER_KEY );

         bool hasEcomDeviceBranch = WinCE.DoesSubKeyExist( 
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            ECOMCI70_SCANNER_BRANCH,
            ECOMCI70_SCANNER_KEY );

         //
         // does device support Intermec Barcode scanning?
         // Note: As ECOM devices are OEM versions of the Intermec CN70 we need 
         // to confirm that the device does not also include ECOM settings!
         //
         if ( hasIntermecBranch & !hasEcomDeviceBranch )
         {
            //
            // Verify the SmartSystems drivers are installed on the Intermec
            // device.  Fix for OTD# 1334
            //
            if ( Directory.Exists( INTERMEC_SS_BRANCH ) )
            {
               result = BarcodeEngine.Intermec;
            }
            else
            {
               result = BarcodeEngine.InitError;
            }
         }

         //
         // if we have both Intermec and Ecom branches, then we can be pretty
         // sure that what we have here is an ATEX ECOM Ci70. However, as this
         // device can support a range of interchangeable heads, we need to 
         // determine exactly what one is currently in use!
         //
         else if ( hasIntermecBranch & hasEcomDeviceBranch )
         {
            result = Get_ECOM_Head_Type();
         }

         // does the device support Symbol Barcode scanning
         else if ( hasSymbolBranch )
         {
            result = BarcodeEngine.Symbol;
         }

         // does the device support the Unitech scanner engine 
         else if ( hasUnitechBranch )
         {
            result = BarcodeEngine.Unitech;
         }

         // if no scanner/imaging support, return false
         else
         {
            result = BarcodeEngine.None;
         }

         return result;
      }


      /// <summary>
      /// If we know the device is an ECOM Ci70, then determine the
      /// actual UART (Barcode) head type currently connected.
      /// </summary>
      /// <returns>Detected type</returns>
      private static BarcodeEngine Get_ECOM_Head_Type()
      {
         // create a return object
         object value = new object();

         // now execute a generic query to the Registry
         bool hasEcomDeviceBranch = WinCE.GetKeyValue(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            ECOMCI70_SCANNER_BRANCH,
            ECOMCI70_SCANNER_KEY,
            ECOMCI70_SCANNER_VALUE,
            ref value );

         // now determine the actual head type
         if ( value != null )
         {
            switch ( (ECOM_UART_DeviceHead) value )
            {
               case ECOM_UART_DeviceHead.None:
                  return BarcodeEngine.None;
               case ECOM_UART_DeviceHead.SE955:
                  return BarcodeEngine.EcomSE955;
               case ECOM_UART_DeviceHead.EA15:
                  return BarcodeEngine.EcomEA15;
               case ECOM_UART_DeviceHead.EX25:
                  return BarcodeEngine.EcomEX25;
               default:
                  return BarcodeEngine.None;
            }
         }
         else
         {
            return BarcodeEngine.InitError;
         }
      }

      #endregion

      //-------------------------------------------------------------------------

      #region Determine what RFID type and supported Tag types

      /// <summary>
      /// What type of RFID reader does this device support
      /// </summary>
      /// <returns></returns>
      public static RFIDEngine Supported_RFID_Engine()
      {
         //---------------------------------------------------------------------
         // This method is the main static entry point for confirming whether
         // or not the connected device supports RFID. This method should only 
         // return a known type if the reader hardware is internal. If the reader 
         // engine is external to the device and relies on either USB or Bluetooth 
         // to provide connectivity, this method should not return a known type.
         //---------------------------------------------------------------------

         var result = RFIDEngine.None;

         bool hasIntermecBranch = WinCE.DoesSubKeyExist(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            INTERMEC_RFID_SUPPORT_PATH,
            INSTALL_KEY );

         bool hasEcomDeviceBranch = WinCE.DoesSubKeyExist(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            ECOMCI70_SCANNER_BRANCH,
            ECOMCI70_SCANNER_KEY );

         bool hasUnitechDeviceBranch = WinCE.DoesSubKeyExist(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            REG_BRANCH_PA690_HF,
            REG_KEY_PA690_HF );

         //
         // does device support the standard Intermec IP30 gun/IM11 reader?
         // Note: As ECOM devices are OEM versions of the Intermec CN70 we need 
         // to confirm that the device does not also include ECOM settings!
         //
         if (hasIntermecBranch && !hasEcomDeviceBranch)
         {
            if (Intermec_Rfid_Type_Enabled(INTERMEC_IP30))
               result = RFIDEngine.IntermecIP30;
            else if (Intermec_Rfid_Type_Enabled(INTERMEC_IM11))
               result = RFIDEngine.IntermecIM11;
         }

         //
         // if we have both Intermec and Ecom branches, then we can be pretty
         // sure that what we have here is an ATEX ECOM Ci70. However, as this
         // device can support a range of interchangeable heads, we need to 
         // determine exactly what one is currently in use!
         //
         else if ( hasIntermecBranch && hasEcomDeviceBranch )
         {
            result = Get_ECOM_Reader_Type();
         }

         // if we're looking at a Unitech device here
         else if ( hasUnitechDeviceBranch )
         {
            result = RFIDEngine.Unitech;
         }

         // if no scanner/imaging support, return false
         else
         {
            result = RFIDEngine.None;
         }

         return result;
      }

      /// <summary>
      /// Determines if an RFID type is enabled
      /// </summary>
      /// <returns>TRUE if YES</returns>
      private static bool Intermec_Rfid_Type_Enabled(string iRfidType)
      {
         var result = false;
         // create a return object
         var value = new object();

         // now execute a generic query to the Registry
         var detected = WinCE.GetKeyValue(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            INTERMEC_RFID_DETECT_PATH,
            INTERMEC_RFID_PORT_KEY,
            INTERMEC_RFID_ENABLED_DEVICE_VALUE,
            ref value);

         if (detected)
         {
            var rfidType = value as string;
            if (!string.IsNullOrEmpty(rfidType))
            {
               result = string.Equals(rfidType, iRfidType);
            }
         }
         return result;
      }


      /// <summary>
      /// Get the current reader mask to determine exactly what
      /// Tag types are currently supported!
      /// </summary>
      /// <returns>mask value, else 0 for not supported</returns>
      public static int Get_ECOM_Reader_Mask()
      {
         // get the current reader type
         RFIDEngine engine = Get_ECOM_Reader_Type();

         // based on the reader type return the mask integer
         if ( engine == RFIDEngine.EcomTLB30 )
         {
            return Get_TLB30_Reader_Mask();
         }
         else if ( engine == RFIDEngine.EcomUNI13 )
         {
            return Get_UNI13_Reader_Mask();
         }
         else
         {
            return 0;
         }
      }


      /// <summary>
      /// Get the TLB30 Reader Mask
      /// </summary>
      /// <returns></returns>
      private static int Get_TLB30_Reader_Mask()
      {
         int value = 0;
         if ( GetIntValue( ECOM_TLB30_BRANCH, TLB30_READ_MASK, ref value ) )
         {
            return value;
         }
         else
         {
            return 0;
         }
      }


      /// <summary>
      /// Get the UNI13 Reader Mask
      /// </summary>
      /// <returns></returns>
      private static int Get_UNI13_Reader_Mask()
      {
         int value = 0;
         if ( GetIntValue( ECOM_UNI13_BRANCH, UNI13_READ_MASK, ref value ) )
         {
            return value;
         }
         else
         {
            return 0;
         }
      }


      /// <summary>
      /// Query the ECOM Registry branch for an integer value 
      /// </summary>
      /// <param name="iKey">key to query against</param>
      /// <param name="oValue">returned string value</param>
      /// <returns>False on brach not found</returns>
      private static bool GetIntValue( string iBranch, string iKey, ref int oValue )
      {
         // create a return object
         object value = new object();

         // now execute a generic query to the Registry
         bool hasEcomDeviceBranch = WinCE.GetKeyValue(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            ECOMCI70_CONFIGLET_BRANCH,
            iBranch,
            iKey,
            ref value );

         oValue = Convert.ToInt32( value );
         return hasEcomDeviceBranch;
      }


      /// <summary>
      /// If we know the device is an ECOM Ci70, then determine the
      /// actual USB RFID reader head type that is currently connected.
      /// </summary>
      /// <returns>Detected type</returns>
      private static RFIDEngine Get_ECOM_Reader_Type()
      {
         // create a return object
         object value = new object();

         // now execute a generic query to the Registry
         bool hasEcomDeviceBranch = WinCE.GetKeyValue(
            WinCE.HKEY.HKEY_LOCAL_MACHINE,
            ECOMCI70_SCANNER_BRANCH,
            ECOMCI70_SCANNER_KEY,
            ECOMCI70_RFID_VALUE,
            ref value );

         // now determine the actual head type
         if ( value != null )
         {
            switch ( (ECOM_USB_DeviceHead) value )
            {
               case ECOM_USB_DeviceHead.None:
                  return RFIDEngine.None;
               case ECOM_USB_DeviceHead.TLB30:
                  return RFIDEngine.EcomTLB30;
               case ECOM_USB_DeviceHead.UNI13:
                  return RFIDEngine.EcomUNI13;
               case ECOM_USB_DeviceHead.ARE8:
                  return RFIDEngine.EcomARE8;
               default:
                  return RFIDEngine.Unknown;
            }
         }
         else
         {
            return RFIDEngine.Unknown;
         }
      }

      #endregion

      //-------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  ScanningCtl.cs
//
//  Revision 3.0 APinkerton 5th June 2013
//  Determine the supported RFID tag type mask for ECOM CiXX handhelds
//
//  Revision 2.0 APinkerton 21th November 2012
//  Add support for ECOM CiXX handhelds
//
//  Revision 1.0 TBottalico 16th November 2012
//  Add support for Unitech PA6XX handhelds
//
//  Revision 0.0 APinkerton 8th May 2010
//  Add to project
//-------------------------------------------------------------------------------
