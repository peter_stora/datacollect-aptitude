﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Class used for WinCE sound
   /// </summary>
   public class Sound
   {
      /// <summary>
      /// Plays a sound
      /// </summary>
      /// <param name="iFileName">The file name for the sound</param>
      /// <returns>True if played</returns>
      public static bool Play( string iFileName )
      {
         bool result;
         try
         {
            result = WinCE.PlaySound( iFileName, IntPtr.Zero,
                                ( (int)WinCE.PlaySoundFlags.SND_FILENAME | (int)WinCE.PlaySoundFlags.SND_SYNC ) ) == 1;

         }
         catch
         {
            result = false;
         }
         return result;
      }
   }
}
