﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SKF.RS.MicrologInspector.HardwareConnect
{
   /// <summary>
   /// Simple class to execute all low level read/write functionality to the Registry.
   /// </summary>
   public class RegistryIO : IRegistryIO
   {
      #region Private Fields

      /// <summary>
      /// Pointer to the Local Machine Hive
      /// </summary>
      private IntPtr mLocalMachineHive = 
         new IntPtr( unchecked( (int) WinCE.HKEY.HKEY_LOCAL_MACHINE ) );
      
      /// <summary>
      /// Pointer to the Current User Hive
      /// </summary>
      private IntPtr mCurrentUserHive = 
         new IntPtr( unchecked( (int) WinCE.HKEY.HKEY_CURRENT_USER ) );
      
      /// <summary>
      /// Pointer to the currently open Key
      /// </summary>
      private IntPtr mHKeyCurrent;
      
      /// <summary>
      /// Registry call result
      /// </summary>
      private int mIntReturn;
      
      /// <summary>
      /// Target Key already exists or was created
      /// </summary>
      private WinCE.REGDISP mRegdispResult;
      
      /// <summary>
      /// Maximum number of chars
      /// </summary>
      private const int cbMAX = 255;

      #endregion


      /// <summary>
      /// Enumeration of primary Registry Hives
      /// </summary>
      public enum RegistryHive
      {
         HKEY_LOCAL_MACHINE,
         HKEY_CURRENT_USER
      }


      /// <summary>
      /// Return the Target Registry Hive
      /// </summary>
      /// <param name="iHive">Registry Hive Type</param>
      /// <returns>Registry Hive Pointer</returns>
      private IntPtr GetTargetHive( RegistryHive iHive )
      {
         if ( iHive == RegistryHive.HKEY_CURRENT_USER )
         {
            return mCurrentUserHive;
         }
         else
         {
            return mLocalMachineHive;
         }
      }/* end method */


      #region Public Methods

      /// <summary>
      /// Delete a single Registry key
      /// </summary>
      /// <param name="strKeyName">name of the key to delete</param>
      /// <returns>true on success</returns>
      public bool DeleteKey( RegistryHive iHive, string iBranch, string strKeyName )
      {
         try
         {
            mIntReturn = WinCE.RegDeleteKey(
                                         GetTargetHive( iHive ),
                                         iBranch + @"\" + strKeyName );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegDeleteKey(
                           GetTargetHive( iHive ),
                           iBranch + @"\" + strKeyName );
         }

         // did that work?
         if ( mIntReturn != 0 )
            return false;

         return true;

      }/* end method */


      /// <summary>
      /// Read a System.string  from the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="oValue">returnes string value</param>
      /// <returns>true on success</returns>
      public bool GetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, ref string oValue )
      {
         //  Open the key and have its handle placed in mHKeyCurrent 
         OpenRegistryKey( iHive, iBranch, iKeyName );

         // did that work?
         if ( mIntReturn != 0 )
         {
            return false;
         }

         //  Create fields to hold the output.
         StringBuilder sbValue = new StringBuilder( cbMAX );
         int cbValue = cbMAX;

         //  Read the value from the registry into sbValue
         WinCE.REGTYPE rtType = 0;

         try
         {
            mIntReturn = WinCE.RegQueryValueEx(
                                        mHKeyCurrent,
                                        iValueName,
                                        0,
                                        ref rtType,
                                        sbValue,
                                        ref cbValue );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegQueryValueEx(
                            mHKeyCurrent,
                            iValueName,
                            0,
                            ref rtType,
                            sbValue,
                            ref cbValue );
         }

         if ( mIntReturn != 0 )
            return false;

         // close the selected Registry key
         CloseRegistryKey();

         // did that work?
         if ( mIntReturn != 0 )
            return false;

         //  Set the string into the output parameter.
         oValue = sbValue.ToString();

         return true;

      }/* end method */


      /// <summary>
      /// Read a System.int from the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="oValue">returns integer value</param>
      /// <returns>true on success</returns>
      public bool GetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, ref int oValue )
      {
         //  Open the key and have its handle placed in mHKeyCurrent 
         OpenRegistryKey( iHive, iBranch, iKeyName );

         if ( mIntReturn != 0 )
         {
            return false;
         }

         //
         //  Pull the value into intValue.  For platform independence, 
         //  use Marshal.SizeOf(intValue), not "4", to specify the size 
         //  in bytes of a System.int.
         //
         int cbValue = Marshal.SizeOf( oValue );

         WinCE.REGTYPE rtType = 0;

         try
         {
            mIntReturn = WinCE.RegQueryValueEx(
                                        mHKeyCurrent,
                                        iValueName,
                                        0,
                                        ref rtType,
                                        ref oValue,
                                        ref cbValue );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegQueryValueEx(
                            mHKeyCurrent,
                            iValueName,
                            0,
                            ref rtType,
                            ref oValue,
                            ref cbValue );
         }

         if ( mIntReturn != 0 )
            return false;

         // close the selected Registry key
         CloseRegistryKey();

         if ( mIntReturn != 0 )
            return false;

         return true;

      }/* end method */


      /// <summary>
      /// Read a System.bool from the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="oValue">returns boolean value</param>
      /// <returns>true on success</returns>
      public bool GetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, ref bool oValue )
      {
         //
         //  Use the integer version of GetValue to get the value 
         //  from the registry, then convert it to a boolean.
         //
         int intValue = 0;
         
         bool boolReturn = GetValue( iHive, iBranch, iKeyName, iValueName, ref intValue );

         if ( boolReturn )
            oValue = Convert.ToBoolean( intValue );

         return boolReturn;

      }/* end method */


      /// <summary>
      /// Write a System.string  to the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="iValue">string value to write to registry</param>
      /// <returns>true on success</returns>
      public bool SetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, string iValue )
      {
         //  Open the key and have its handle placed in mHKeyCurrent 
         OpenRegistryKey( iHive, iBranch, iKeyName );

         if ( mIntReturn != 0 )
            return false;

         //  Store strValue under the name strValueName.
         try
         {
            mIntReturn = WinCE.RegSetValueEx(
                                        mHKeyCurrent,
                                        iValueName,
                                        0,
                                        WinCE.REGTYPE.REG_SZ,
                                        iValue,
                                        iValue.Length * 2 + 1 );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegSetValueEx(
                            mHKeyCurrent,
                            iValueName,
                            0,
                            WinCE.REGTYPE.REG_SZ,
                            iValue,
                            iValue.Length * 2 + 1 );
         }

         if ( mIntReturn != 0 )
            return false;

         // close the selected Registry key
         CloseRegistryKey();

         if ( mIntReturn != 0 )
            return false;

         return true;

      }/* end method */


      /// <summary>
      /// Write a System.int to the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="iValue">integer value to write to registry</param>
      /// <returns>true on success</returns>
      public bool SetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, int iValue )
      {
         //  Open the key and have its handle placed in mHKeyCurrent 
         OpenRegistryKey( iHive, iBranch, iKeyName );

         if ( mIntReturn != 0 )
            return false;

         try
         {
            mIntReturn = WinCE.RegSetValueEx(
                                        mHKeyCurrent,
                                        iValueName,
                                        0,
                                        WinCE.REGTYPE.REG_DWORD,
                                        ref iValue,
                                        Marshal.SizeOf( iValue ) );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegSetValueEx(
                            mHKeyCurrent,
                            iValueName,
                            0,
                            WinCE.REGTYPE.REG_DWORD,
                            ref iValue,
                            Marshal.SizeOf( iValue ) );
         }

         if ( mIntReturn != 0 )
            return false;

         // close the selected Registry key
         CloseRegistryKey();

         if ( mIntReturn != 0 )
            return false;

         return true;

      }/* end method */


      /// <summary>
      /// Write a System.int to the registry
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iKeyName">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="iValueName">The actual value name</param>
      /// <param name="iValue">integer value to write to registry</param>
      /// <returns>true on success</returns>
      public bool SetValue( RegistryHive iHive, string iBranch, string iKeyName, string iValueName, bool iValue )
      {
         //
         // Cast the value as a boolean, then use the integer 
         // version of SetValue to set the value into the
         // registry. There is no Convert.ToInteger method.  
         // For platform independence, we cast to the smallest 
         // integer, which will always implicitly and successfully 
         // cast to int.
         //
         return SetValue( iHive, iBranch, iKeyName, iValueName, Convert.ToInt16( iValue ) );

      }/* end method */


      /// <summary>
      /// Retrieves subkeys for a given key in a string array
      /// </summary>
      /// <param name="iHive">Targer Hive (HKLM or HKCU)</param>
      /// <param name="iBranch">The full Registry branch path to the Key</param>
      /// <param name="iKeyName">The name of the target Key</param>
      /// <param name="oSubKeys">Reference to an array to hold the subkeys</param>
      /// <returns>True on success</returns>
      public bool GetSubKeyNames( RegistryHive iHive, string iBranch, string iKeyName, ref string[] oSubKeys )
      {
         WinCE.HKEY hKey;

         if ( iHive == RegistryHive.HKEY_CURRENT_USER )
         {
            hKey = WinCE.HKEY.HKEY_CURRENT_USER;
         }
         else
         {
            hKey = WinCE.HKEY.HKEY_LOCAL_MACHINE;
         }

         return WinCE.GetSubKeyNames( hKey, iBranch, iKeyName, ref oSubKeys );

      }/* end method*/

      #endregion


      /// <summary>
      /// Attempt to open the Registry Key on a CE Device. If that
      /// fails then attempt to open the Key under Win32
      /// </summary>
      /// <param name="iHive">Local Machine or Current User</param>
      /// <param name="iBranch">Registry Branch</param>
      /// <param name="iKeyName">Actual Registry Key</param>
      private void OpenRegistryKey( RegistryHive iHive, 
         string iBranch, string iKeyName )
      {
         try
         {
            mIntReturn = WinCE.RegCreateKeyEx(
                                         GetTargetHive( iHive ),
                                         iBranch + @"\" + iKeyName,
                                         0,
                                         string.Empty,
                                         0,
                                         0,
                                         IntPtr.Zero,
                                         ref mHKeyCurrent,
                                         ref mRegdispResult );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegCreateKeyEx(
                           GetTargetHive( iHive ),
                           iBranch + @"\" + iKeyName,
                           0,
                           string.Empty,
                           0,
                           0,
                           IntPtr.Zero,
                           ref mHKeyCurrent,
                           ref mRegdispResult );
         }

      }/* end method*/


      /// <summary>
      /// Close the open Registry Key
      /// </summary>
      private void CloseRegistryKey()
      {
         try
         {
            mIntReturn = WinCE.RegCloseKey( mHKeyCurrent );
         }
         catch
         {
            mIntReturn = WinCE.Win32RegCloseKey( mHKeyCurrent );
         }

      }/* end method*/

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 29th October 2009
//  Add support for Win32
//
//  Revision 0.0 APinkerton 16th March 2009
//  Add to project
//----------------------------------------------------------------------------
