﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the C_NOTES Table ResultSet
   /// </summary>
   public class ResultSetSavedCodedNotesTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Parameterized query string for recovering VELDATA for a particular UID
      /// </summary>
      private const string QUERY_CNOTES_BY_NOTEUID = "SELECT * FROM C_NOTES " +
          "WHERE NotesUid = @NotesUid";

      private const string DELETE_CODED_NOTES = "DELETE FROM C_NOTES " +
          "WHERE NotesUid = @NotesUid";

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsCNotes mColumnOrdinal = null;

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_C_NOTES;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current C_NOTES table
      /// </summary>
      /// <param name="iCodedNote">Populated Saved Coded Note object</param>
      /// <param name="iNoteUid">Uid of new parent Note object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CodedNoteRecord iCodedNote, Guid iNoteUid )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update table
               result = WriteRecord( iCodedNote, iNoteUid, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Code Note objects to the C_NOTES table 
      /// </summary>
      /// <param name="iCodedNotes">Enumerated List of C_NOTES objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumUploadCodedNotes iCodedNotes, Guid iNoteUid )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( UploadCodedNote codedNote in iCodedNotes.CodedNote )
               {
                  if ( !WriteRecord( codedNote, iNoteUid, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Code Note objects to the C_NOTES table 
      /// </summary>
      /// <param name="iCodedNotes">Enumerated List of C_NOTES objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumSelectedCodedNotes iCodedNotes )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( SelectedCodedNote codedNote in iCodedNotes.CodedNote )
               {
                  // create and populate a coded note object
                  CodedNoteRecord cNote = new CodedNoteRecord();
                  cNote.Code = codedNote.Code;
                  cNote.Text = codedNote.Text;

                  // now write to the table
                  if ( !WriteRecord( cNote, codedNote.NoteUid, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a series of Coded Note record objects from the C_NOTES 
      /// table using the NoteUid field (which links to NOTES.Uid) as 
      /// the lookup reference.
      /// </summary>
      /// <param name="iNoteUid">External link to Note Uid</param>
      /// <returns>List of populated Coded Note objects</returns>
      public EnumSelectedCodedNotes GetRecords( Guid iNoteUid )
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumSelectedCodedNotes codedNotes = new EnumSelectedCodedNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_CNOTES_BY_NOTEUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@NotesUid", SqlDbType.UniqueIdentifier ).Value
                = iNoteUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the C_NOTES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record objects
               while ( ceResultSet.Read() )
               {
                  SelectedCodedNote codedNote = new SelectedCodedNote();
                  PopulateObject( ceResultSet, ref codedNote );
                  codedNotes.AddCodedNoteRecord( codedNote );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return codedNotes;

      }/* end method */


      /// <summary>
      /// Quickly returns an enumerated list of all C_NOTES records
      /// </summary>
      /// <returns>an enumerated list of SelectedCodedNote objects</returns>
      public EnumSelectedCodedNotes GetAllRecords()
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumSelectedCodedNotes codedNotes = new EnumSelectedCodedNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTES";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the C_NOTES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record objects
               while ( ceResultSet.Read() )
               {
                  SelectedCodedNote codedNote = new SelectedCodedNote();
                  PopulateObject( ceResultSet, ref codedNote );
                  codedNotes.AddCodedNoteRecord( codedNote );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return codedNotes;

      }/* end method */

      #endregion


      #region Extended Methods

      /// <summary>
      /// Deletes all note codes for a given parent note uid
      /// </summary>
      /// <param name="iNoteUid">Unique note identifier</param>
      /// <returns>true if deleted - else false</returns>
      public Boolean DeleteCodedNotes( Guid iNoteUid )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DELETE_CODED_NOTES;
            command.Parameters.Add( "@NotesUid", SqlDbType.UniqueIdentifier ).Value = iNoteUid;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion

      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// C_NOTE table. This table will need to be revised in the event of any 
      /// changes being made to the C_NOTE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsCNotes( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new Coded Note record and add it 
      /// to the current C_NOTES table.
      /// </summary>
      /// <param name="iCodedNote">Populated Coded Note Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CodedNoteRecord iCodedNote, Guid iNoteUid,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.NotesUid ] = iNoteUid;
            newRecord[ mColumnOrdinal.Code ] = iCodedNote.Code;
            newRecord[ mColumnOrdinal.CodedTextNote ] = iCodedNote.Text;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch ( Exception ex )
         {
            if ( ex != null )
               result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Construct and populate a single new Coded Note record and add it 
      /// to the current C_NOTES table.
      /// </summary>
      /// <param name="iCodedNote">Populated Coded Note Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( UploadCodedNote iCodedNote, Guid iNoteUid,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.NotesUid ] = iNoteUid;
            newRecord[ mColumnOrdinal.Code ] = iCodedNote.Code;
            newRecord[ mColumnOrdinal.CodedTextNote ] = iCodedNote.Text;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch ( Exception ex )
         {
            if ( ex != null )
               result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single Saved Coded Note table record
      /// </summary>
      /// <param name="iCodedNote">Updated Coded Note Record object</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CodedNoteRecord iCodedNote,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.Code, iCodedNote.Code );
            ceResultSet.SetString( mColumnOrdinal.CodedTextNote, iCodedNote.Text );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated Coded Note Object with data from a single 
      /// C_NOTES table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iFFTChannelRecord">FFT Point object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref SelectedCodedNote iCodedNote )
      {
         try
         {
            iCodedNote.NoteUid = ceResultSet.GetGuid( mColumnOrdinal.NotesUid );
            iCodedNote.Code = ceResultSet.GetInt32( mColumnOrdinal.Code );
            iCodedNote.Text = ceResultSet.GetString( mColumnOrdinal.CodedTextNote );
         }
         catch
         {
            iCodedNote = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 19th March 2010
//  Add method GetAllRecords()
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------