﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the DERIVEDITEMS Table ResultSet
   /// </summary>
   public partial class ResultSetDerivedItemsTable : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsDerivedItem mColumnOrdinal = null;

      /// <summary>
      /// Query DERIVEDITEMS table based on the external Point Uid field. 
      /// This query will return an enumerated series of records ordered
      /// by the field "FormulaItemPosition".
      /// </summary>
      private const string QUERY_BY_DERIVEDUID = "SELECT * FROM DERIVEDITEMS " +
          "WHERE DerivedUid = @DerivedUid ORDER BY FormulaItemPosition ASC";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_DERIVEDITEMS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current DERIVEDITEMS table. 
      /// </summary>
      /// <param name="iDerivedItemObject">populated DerivedItemsRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( DerivedItemsRecord iDerivedItemObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDITEMS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update table
               result = WriteRecord( iDerivedItemObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a series of "DerivedItem" Record objects from the DERIVEDITEMS 
      /// table using the DerivedUid field (which links to POINTS.Uid) as 
      /// the lookup reference.
      /// </summary>
      /// <param name="iDerivedUid">External link to Point Uid</param>
      /// <returns>Populated Conditional Point object or null on error</returns>
      public EnumDerivedItems GetRecord( Guid iDerivedUid )
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumDerivedItems derivedItems = new EnumDerivedItems();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_DERIVEDUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@DerivedUid", SqlDbType.UniqueIdentifier ).Value
                                                                 = iDerivedUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the DERIVEDITEMS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record objects
               while ( ceResultSet.Read() )
               {
                  DerivedItemsRecord derivedItem = new DerivedItemsRecord();
                  PopulateObject( ceResultSet, ref derivedItem );
                  derivedItems.AddDerivedItemsRecord( derivedItem );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return derivedItems;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the DERIVEDITEMS table. 
      /// </summary>
      /// <param name="iDerivedItemObjects">populated DerivedItems objects</param>
      /// <param name="iProgress">progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumDerivedItems iDerivedItems,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDITEMS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( DerivedItemsRecord derivedItem in iDerivedItems )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( derivedItem, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the DERIVEDITEMS table. 
      /// </summary>
      /// <param name="iDerivedItemObjects">populated DerivedItems objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumDerivedItems iDerivedItems )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDITEMS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( DerivedItemsRecord derivedItem in iDerivedItems )
               {
                  if ( !WriteRecord( derivedItem, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all DERIVEDITEM records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumDerivedItems GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumDerivedItems derivedItems = new EnumDerivedItems();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDITEMS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the DERIVEDITEMS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  DerivedItemsRecord derivedItem = new DerivedItemsRecord();
                  PopulateObject( ceResultSet, ref derivedItem );
                  derivedItems.AddDerivedItemsRecord( derivedItem );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return derivedItems;

      }/* end method */

      #endregion


      #region Private methods


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// DERIVEDITEMS table. This table will need to be revised in the event of any 
      /// changes being made to the DERIVEDITEMS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsDerivedItem( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new DERIVEDITEMS record and add it 
      /// to the current DERIVEDITEMS table.
      /// </summary>
      /// <param name="iDerivedItemRecord">Populated DERIVEDITEMS Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( DerivedItemsRecord iDerivedItemRecord,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.ID ] = iDerivedItemRecord.ID;
            newRecord[ mColumnOrdinal.DerivedUid ] = iDerivedItemRecord.DerivedUid;
            newRecord[ mColumnOrdinal.FormulaItemPosition ] = iDerivedItemRecord.FormulaItemPosition;
            newRecord[ mColumnOrdinal.FormulaItemType ] = iDerivedItemRecord.FormulaItemType;
            newRecord[ mColumnOrdinal.VarKey ] = iDerivedItemRecord.VarKey;
            newRecord[ mColumnOrdinal.ConstValue ] = iDerivedItemRecord.ConstValue;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single DERIVEDITEMS table record
      /// </summary>
      /// <param name="iDerivedItemRecord">Updated DERIVEDITEMS Record object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( DerivedItemsRecord iDerivedItemRecord,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.ID, iDerivedItemRecord.ID );
            ceResultSet.SetGuid( mColumnOrdinal.DerivedUid, iDerivedItemRecord.DerivedUid );
            ceResultSet.SetInt32( mColumnOrdinal.FormulaItemPosition, iDerivedItemRecord.FormulaItemPosition );
            ceResultSet.SetInt32( mColumnOrdinal.FormulaItemType, iDerivedItemRecord.FormulaItemType );
            ceResultSet.SetInt32( mColumnOrdinal.VarKey, iDerivedItemRecord.VarKey );
            ceResultSet.SetDouble( mColumnOrdinal.ConstValue, iDerivedItemRecord.ConstValue );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated DERIVEDITEMS Object with data from a single 
      /// DERIVEDITEMS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iDerivedItemRecord">DERIVEDITEMS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref DerivedItemsRecord iDerivedItemRecord )
      {
         try
         {
            iDerivedItemRecord.ID = ceResultSet.GetInt32( mColumnOrdinal.ID );
            iDerivedItemRecord.DerivedUid = ceResultSet.GetGuid( mColumnOrdinal.DerivedUid );
            iDerivedItemRecord.FormulaItemPosition = ceResultSet.GetInt32( mColumnOrdinal.FormulaItemPosition );
            iDerivedItemRecord.FormulaItemType = ceResultSet.GetInt32( mColumnOrdinal.FormulaItemType );
            iDerivedItemRecord.VarKey = ceResultSet.GetInt32( mColumnOrdinal.VarKey );
            iDerivedItemRecord.ConstValue = ceResultSet.GetDouble( mColumnOrdinal.ConstValue );
         }
         catch
         {
            iDerivedItemRecord = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------