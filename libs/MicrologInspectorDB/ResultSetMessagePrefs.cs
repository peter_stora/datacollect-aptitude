﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the MESSAGINGPREFS Table ResultSet
   /// </summary>
   public class ResultSetMessagePrefs : ResultSetBase
   {
      //----------------------------------------------------------------------

      #region Private Fields and Constants


      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsMessagePrefs mColumnOrdinal = null;

      /// <summary>
      /// Query MESSAGINGPREFS table based on the Operator ID
      /// </summary>
      private const string QUERY_BY_OPERATOR_ID = "SELECT * FROM MESSAGINGPREFS " +
          "WHERE OperId = @OperId";


      #endregion

      //----------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_MESSAGINGPREFS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current MESSAGINGPREFS table. 
      /// </summary>
      /// <param name="iPreferences">populated MessagePreferences object</param>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( MessagePreferences iPreferences, int iOperId )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGINGPREFS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update the table contents
               result = WriteRecord( iPreferences, iOperId, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the MESSAGINGPREFS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( OperatorSet iOperSettings )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGINGPREFS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               int operId;

               // save all records directly to the MESSAGEPREFS table
               for ( int i = 0; i < iOperSettings.OperatorSettings.Count; ++i )
               {
                  // get the operator Id associated with the setting record
                  operId = iOperSettings.OperatorSettings.SettingsData[ i ].OperatorId;

                  // populate the table record
                  result = result & WriteRecord(
                              iOperSettings.OperatorSettings.SettingsData[ i ].Settings.MessagingPreferences,
                              operId, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close connection
         CloseCeConnection();

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the MESSAGINGPREFS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( EnumMessagingPrefs iMessagePrefs )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGINGPREFS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each record
               foreach ( MessagingPrefsRecord preference in iMessagePrefs )
               {
                  // populate the table record
                  result = result & WriteRecord(preference, ceResultSet);
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close connection
         CloseCeConnection();

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Resturn a list of all operator records from the MESSAGINGPREFS table 
      /// </summary>
      /// <returns>Enumerated list of raw MESSAGINGPREFS table records</returns>
      public EnumMessagingPrefs GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumMessagingPrefs messagingPrefs = new EnumMessagingPrefs();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGINGPREFS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGINGPREFS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MessagingPrefsRecord messagePrefs = new MessagingPrefsRecord();
                  PopulateObject( ceResultSet, ref messagePrefs );
                  messagingPrefs.AddRecord( messagePrefs );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return messagingPrefs;

      }/* end method */


      /// <summary>
      /// Given the current Operator ID return a single Message Preferences object
      /// </summary>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>Populated MessagePreferences object</returns>
      public MessagePreferences GetRecord( int OperId )
      {
         OpenCeConnection();

         // reset the default profile name
         MessagePreferences preferences = new MessagePreferences();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Int ).Value = OperId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGINGPREFS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref preferences );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return preferences;

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

      #region Private methods


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// MESSAGINGPREFS table. This table will need to be revised in the event of any 
      /// changes being made to the MESSAGINGPREFS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsMessagePrefs( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a new MESSAGINGPREFS record
      /// </summary>
      /// <param name="iPrefs">Populated MessagePreferences object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( MessagePreferences iPrefs, int iOperId,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iOperId;
            newRecord[ mColumnOrdinal.EndOfRoute ] = iPrefs.EndOfRoute;
            newRecord[ mColumnOrdinal.ZeroAcceleration ] = iPrefs.ZeroAcceleration;
            newRecord[ mColumnOrdinal.SaveDataAsCurrent ] = iPrefs.SaveDataAsCurrent;
            newRecord[ mColumnOrdinal.FoundInOtherRoute ] = iPrefs.FoundInOtherRoute;
            newRecord[ mColumnOrdinal.ShowSetsWhileCollecting ] = iPrefs.ShowSetsWhileCollecting;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Construct and populate a new MESSAGINGPREFS record
      /// </summary>
      /// <param name="iPrefs">Populated MessagePreferences object</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord(MessagingPrefsRecord iPrefs, SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iPrefs.OperId;
            newRecord[ mColumnOrdinal.EndOfRoute ] = iPrefs.EndOfRoute;
            newRecord[ mColumnOrdinal.ZeroAcceleration ] = iPrefs.ZeroAcceleration;
            newRecord[ mColumnOrdinal.SaveDataAsCurrent ] = iPrefs.SaveDataAsCurrent;
            newRecord[ mColumnOrdinal.FoundInOtherRoute ] = iPrefs.FoundInOtherRoute;
            newRecord[ mColumnOrdinal.ShowSetsWhileCollecting ] = iPrefs.ShowSetsWhileCollecting;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated MESSAGINGPREFS Object with data from a single 
      /// MESSAGINGPREFS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iPrefs">MESSAGINGPREFS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref MessagePreferences iPrefs )
      {
         try
         {
            iPrefs.EndOfRoute = ceResultSet.GetBoolean( mColumnOrdinal.EndOfRoute );
            iPrefs.ZeroAcceleration = ceResultSet.GetBoolean( mColumnOrdinal.ZeroAcceleration );
            iPrefs.SaveDataAsCurrent = ceResultSet.GetBoolean( mColumnOrdinal.SaveDataAsCurrent );
            iPrefs.FoundInOtherRoute = ceResultSet.GetBoolean( mColumnOrdinal.FoundInOtherRoute );
            iPrefs.ShowSetsWhileCollecting = ceResultSet.GetBoolean( mColumnOrdinal.ShowSetsWhileCollecting );
         }
         catch
         {
            iPrefs = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated MESSAGINGPREFS Object with data from a single 
      /// MESSAGINGPREFS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iPrefs">MESSAGINGPREFS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref MessagingPrefsRecord iPrefs )
      {
         try
         {
            iPrefs.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iPrefs.EndOfRoute = ceResultSet.GetBoolean( mColumnOrdinal.EndOfRoute );
            iPrefs.ZeroAcceleration = ceResultSet.GetBoolean( mColumnOrdinal.ZeroAcceleration );
            iPrefs.SaveDataAsCurrent = ceResultSet.GetBoolean( mColumnOrdinal.SaveDataAsCurrent );
            iPrefs.FoundInOtherRoute = ceResultSet.GetBoolean( mColumnOrdinal.FoundInOtherRoute );
            iPrefs.ShowSetsWhileCollecting = ceResultSet.GetBoolean( mColumnOrdinal.ShowSetsWhileCollecting );
         }
         catch
         {
            iPrefs = null;
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 20th June 2012
//  Add new methods PopulateObject()and GetAllRecords()
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------