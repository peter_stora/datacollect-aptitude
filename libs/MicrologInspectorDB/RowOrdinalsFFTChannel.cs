﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the FFTChannel table. This structure should be revised following
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsFFTChannel
   {
      public int Uid
      {
         get;
         set;
      }
      
      public int FFTPointUid
      {
         get;
         set;
      }
      
      public int PointUid
      {
         get;
         set;
      }
      
      public int PointHostIndex
      {
         get;
         set;
      }
      
      public int ChannelId
      {
         get;
         set;
      }
      
      public int SpecNum
      {
         get;
         set;
      }
      
      public int Timestamp
      {
         get;
         set;
      }
      
      public int NumberOfLines
      {
         get;
         set;
      }
      
      public int DataType
      {
         get;
         set;
      }
      
      public int NumberOfAverages
      {
         get;
         set;
      }
      
      public int Windowing
      {
         get;
         set;
      }
      
      public int SpeedOrPPF
      {
         get;
         set;
      }
      
      public int Factor
      {
         get;
         set;
      }
      
      public int Offset
      {
         get;
         set;
      }
      
      public int HiFreq
      {
         get;
         set;
      }
      
      public int LowFreq
      {
         get;
         set;
      }
      
      public int Load
      {
         get;
         set;
      }
      
      public int CompressionType
      {
         get;
         set;
      }
      
      public int UserIndex
      {
         get;
         set;
      }
      
      public int Units
      {
         get;
         set;
      }
      
      public int IsOverloaded
      {
         get;
         set;
      }
      
      public int DataLines
      {
         get;
         set;
      }
      
      public int SerialNo
      {
         get;
         set;
      }


      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsFFTChannel( SqlCeResultSet iCeResultSet )
      {
         GetFFTChannelTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// FFTChannel table. This table will need to be revised in the event of any 
      /// changes being made to the FFTChannel table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetFFTChannelTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.FFTPointUid = iCeResultSet.GetOrdinal( "FFTPointUid" );
         this.PointUid = iCeResultSet.GetOrdinal( "PointUid" );
         this.PointHostIndex = iCeResultSet.GetOrdinal( "PointHostIndex" );
         this.ChannelId = iCeResultSet.GetOrdinal( "ChannelId" );
         this.SpecNum = iCeResultSet.GetOrdinal( "SpecNum" );
         this.Timestamp = iCeResultSet.GetOrdinal( "Timestamp" );
         this.NumberOfLines = iCeResultSet.GetOrdinal( "NumberOfLines" );
         this.DataType = iCeResultSet.GetOrdinal( "DataType" );
         this.NumberOfAverages = iCeResultSet.GetOrdinal( "NumberOfAverages" );
         this.Windowing = iCeResultSet.GetOrdinal( "Windowing" );
         this.SpeedOrPPF = iCeResultSet.GetOrdinal( "SpeedOrPPF" );
         this.Factor = iCeResultSet.GetOrdinal( "Factor" );
         this.Offset = iCeResultSet.GetOrdinal( "Offset" );
         this.HiFreq = iCeResultSet.GetOrdinal( "HiFreq" );
         this.LowFreq = iCeResultSet.GetOrdinal( "LowFreq" );
         this.Load = iCeResultSet.GetOrdinal( "Load" );
         this.CompressionType = iCeResultSet.GetOrdinal( "CompressionType" );
         this.UserIndex = iCeResultSet.GetOrdinal( "UserIndex" );
         this.Units = iCeResultSet.GetOrdinal( "Units" );
         this.IsOverloaded = iCeResultSet.GetOrdinal( "IsOverloaded" );
         this.DataLines = iCeResultSet.GetOrdinal( "DataLines" );
         this.SerialNo = iCeResultSet.GetOrdinal( "SerialNo" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------