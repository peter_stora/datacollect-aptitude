﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the TEMPDATA Table ResultSet
   /// </summary>
   public class ResultSetTempDataTable : ResultSetMeasurementsBase
   {
      /// <summary>
      /// Gets the actual measurement table name
      /// </summary>
      protected override string MeasurementTable
      {
         get
         {
            return "TEMPDATA";
         }
      }

      #region Table Specific Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public override Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_TEMPDATA;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a series of measurements to the TEMPDATA table.
      /// </summary>
      /// <param name="iPointRecords">enumerated list of point records</param>
      /// <returns>false on exception raised</returns>
      public override Boolean AddEnumRecords( EnumPoints iEnumPoints, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the points list enumeration
         iEnumPoints.Reset();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // for each of the enumerated points records
                  foreach ( PointRecord point in iEnumPoints )
                  {
                     // add each of the measurements
                     for ( int j = 0; j < point.TempData.Count; ++j )
                     {
                        // raise a progress update
                        RaiseProgressUpdate( iProgress );

                        // write record to table
                        AddMeasDataRecord(
                            point.TempData.Measurement[ j ],
                            point.PointHostIndex,
                            point.Uid,
                            false,
                            Alarms.ALARM_NONE,
                            (byte) FieldTypes.SkippedType.None,
                            FieldTypes.NULL_OPERID,
                            ceResultSet );
                     }
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch(Exception ex)
            {
               if (ex.Message != "apples")
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a series of measurements to the TEMPDATA table.
      /// </summary>
      /// <param name="iPointRecords">enumerated list of point records</param>
      /// <returns>false on exception raised</returns>
      public override Boolean AddEnumRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // reset the points list enumeration
         iEnumPoints.Reset();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // for each of the enumerated points records
                  foreach ( PointRecord point in iEnumPoints )
                  {
                     // add each of the measurements
                     for ( int j = 0; j < point.TempData.Count; ++j )
                     {
                        AddMeasDataRecord(
                            point.TempData.Measurement[ j ],
                            point.PointHostIndex,
                            point.Uid,
                            false,
                            Alarms.ALARM_NONE,
                            (byte) FieldTypes.SkippedType.None,
                            FieldTypes.NULL_OPERID,
                            ceResultSet );
                     }
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a series of saved measurements to the appropriate table.
      /// </summary>
      /// <param name="iEnumMeasurements">enumerated list of saved measurements</param>
      /// <returns>false on exception raised</returns>
      public override Boolean AddEnumRecords( EnumSavedMeasurements iEnumMeasurements )
      {
         OpenCeConnection();

         // reset the points list enumeration
         iEnumMeasurements.Reset();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // for each of the enumerated measurement
                  foreach ( SavedMeasurement measurement in iEnumMeasurements )
                  {
                     AddMeasDataRecord( measurement, ceResultSet );
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;
      }


      /// <summary>
      /// Append a series of measurements from an enumerated list of points.
      /// </summary>
      /// <param name="iEnumPoints">point enumeration list</param>
      /// <param name="iOperId">current operator ID</param>
      /// <param name="iNoLastMeas">maximum number of historic measurements retain</param>
      /// <returns>true on success, else false</returns>
      public Boolean AppendEnumRecords( EnumPoints iEnumPoints, int iOperId, int iNoLastMeas )
      {
         OpenCeConnection();

         // reset the points list enumeration
         iEnumPoints.Reset();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // for each of the enumerated points records
                  foreach ( PointRecord point in iEnumPoints )
                  {
                     // get the newest point measurement
                     if ( point.TempData.Count > 0 )
                     {
                        PointMeasurement measurement = 
                        point.TempData.Measurement[ point.TempData.Count - 1 ];

                        // add the newest record to the TEMPDATA table
                        AddMeasDataRecord(
                              measurement,
                              point.PointHostIndex,
                              point.Uid,
                              true,
                              (byte) point.AlarmState,
                              (byte) point.Skipped,
                              iOperId,
                              ceResultSet );
                     }
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();

         //
         // if that all went well then remove any measurements that
         // are in excess of the maximum number of measurements we
         // intend to hold
         //
         if ( result )
         {
            result = RemoveOldestMeasurements( iEnumPoints, iNoLastMeas );
         }
         
         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Given an enumerated list of points, remove any older measurements that
      /// are in excess of a specified maximum
      /// </summary>
      /// <param name="iEnumPoints">enumerated list of points</param>
      /// <param name="iNoLastMeas">maximum number of measurements</param>
      /// <returns></returns>
      public Boolean RemoveOldestMeasurements( EnumPoints iEnumPoints, int iNoLastMeas )
      {
         OpenCeConnection();

         // reset the points enumeration list
         iEnumPoints.Reset();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // for each of the enumerated points records
               foreach ( PointRecord point in iEnumPoints )
               {
                  // reference the base query string
                  command.CommandText =
                     QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_UID;


                  // reference the PointUid field
                  command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = point.Uid;

                  // execute the result set
                  using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                              ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated 
                     // with  this table. As this is a 'one shot' the column ordinal
                     // object remains a singleton
                     //
                     GetTableColumns( ceResultSet );

                     command.Parameters.Clear();

                     // reset the record count
                     int recordCount = 0;

                     // if there are too many readings then delete those not required
                     while ( ceResultSet.Read() )
                     {
                        ++recordCount;
                        if ( recordCount > iNoLastMeas )
                        {
                           ceResultSet.Delete();
                        }
                     }

                     // close the resultSet
                     ceResultSet.Close();
                  }
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a new measurement to the TEMPDATA table and remove any older measurements.
      /// The actual value should be in one of the three measurement fields, and must be
      /// at index position zero in the enumerated list of measurements! This overloaded
      /// method allows for the Alarm State of the point to be over-ridden (i.e for MCD)
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <param name="iNoLastMeas">Number of measurements to maintain</param>
      /// <param name="iAlarmState">Override the point alarm state</param>
      /// <returns>true if record is written without errors</returns>
      public override Boolean AppendRecord( PointRecord iRecord, int iNoLastMeas,
         FieldTypes.NodeAlarmState iAlarmState, int iOperId )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText =
                  QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_UID;

               // reference the PointUid field
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iRecord.Uid;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // reset the record count
                  int recordCount = 0;

                  // if there are too many readings then delete those not required
                  while ( ceResultSet.Read() )
                  {
                     ++recordCount;
                     if ( recordCount >= iNoLastMeas )
                     {
                        ceResultSet.Delete();
                     }
                  }

                  // get the newest point measurement
                  PointMeasurement measurement = 
                  iRecord.TempData.Measurement[ iRecord.TempData.Count - 1 ];

                  // add the newest record to the TEMPDATA table
                  AddMeasDataRecord(
                        measurement,
                        iRecord.PointHostIndex,
                        iRecord.Uid,
                        true,
                        (byte) iAlarmState,
                        iRecord.Skipped,
                        iOperId,
                        ceResultSet );

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.1 APinkerton 3rd February 2010
//  Add support for download measurement object
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------