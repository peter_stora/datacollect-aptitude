﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the single or batch clearing of Tables
   /// </summary>
   public class ClearTable
   {
      /// <summary>
      /// reference to Table ResultSet object
      /// </summary>
      private ITableConnect mTable = null;

      /// <summary>
      /// reference to Progress update object
      /// </summary>
      private DataProgress mProgressPosition = null;

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iTable"></param>
      public ClearTable( ITableConnect iTable )
      {
         mTable = iTable;

         // instantiate progress position object
         mProgressPosition = new DataProgress();

         // reset the progress manager
         mProgressPosition.Reset();

      }/* end constructor */


      ///<summary>
      ///Gets or sets the progress position object
      ///</summary>
      public DataProgress ProgressPosition
      {
         get
         {
            return mProgressPosition;
         }
         set
         {
            mProgressPosition = value;
         }
      }


      #region Clear individual tables

      /// <summary>
      /// Clear all existing data from the C_NOTE table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean C_NOTE()
      {
         return mTable.CodedNote.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the C_NOTES table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean C_NOTES()
      {
         return mTable.SavedCodedNotes.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CONDITIONALPOINT table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean CONDITIONALPOINT()
      {
         return mTable.ConditionalPoint.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CMMS CORRECTIVEACTION table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean CORRECTIVEACTION()
      {
         return mTable.CorrectiveAction.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the DERIVEDITEMS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean DERIVEDITEMS()
      {
         return mTable.DerivedItems.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the DERIVEDPOINT table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean DERIVEDPOINT()
      {
         return mTable.DerivedPoint.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the ENVDATA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean ENVDATA()
      {
         return mTable.EnvData.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the FFTCHANNEL table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean FFTCHANNEL()
      {
         return mTable.FFTChannel.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the FFTPOINT table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean FFTPOINT()
      {
         return mTable.FFTPoint.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the INSPECTION table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean INSPECTION()
      {
         return mTable.Inspection.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the INSTRUCTIONS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean INSTRUCTIONS()
      {
         return mTable.Instructions.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the LOG table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean LOG()
      {
         return mTable.LogTable.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the MACHINENOPSKIPS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean MACHINENOPSKIPS()
      {
         return mTable.MachineNOpSkips.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the MACHINENOPSKIPS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean MACHINEOKSKIPS()
      {
         return mTable.MachineOkSkips.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the MCC table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean MCC()
      {
         return mTable.Mcc.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the MESSAGE table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean MESSAGE()
      {
         return mTable.Message.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the MESSAGINGPREFS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean MESSAGINGPREFS()
      {
         return mTable.MessagePreferences.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the NODE table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean NODE()
      {
         return mTable.Node.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the NOTES table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean NOTES()
      {
         return mTable.Notes.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the OPERATORS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean OPERATORS()
      {
         return mTable.Operators.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the POINTS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean POINTS()
      {
         return mTable.Points.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CMMS PRIORITY table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean PRIORITY()
      {
         return mTable.Priority.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the PROCESS table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean PROCESS()
      {
         return mTable.Process.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CMMS PROBLEM table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean PROBLEM()
      {
         return mTable.Problem.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the REFERENCEMEDIA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean REFERENCEMEDIA()
      {
         return mTable.ReferenceMedia.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the REFERENCEMEDIA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean SAVEDMEDIA()
      {
         return mTable.SavedMedia.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the STRUCTURED table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean STRUCTURED()
      {
         return mTable.Structured.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the STRUCTUREDLOG table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean STRUCTUREDLOG()
      {
         return mTable.StructuredLog.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the TEMPDATA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean TEMPDATA()
      {
         return mTable.TempData.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the TEXTDATA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean TEXTDATA()
      {
         return mTable.TextData.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the USERPREFERENCES table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean USERPREFERENCES()
      {
         return mTable.UserPreferences.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the VELDATA table
      /// </summary>
      /// <returns>true if success, else false </returns>
      public Boolean VELDATA()
      {
         return mTable.VelData.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CMMS WORKTYPE table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean WORKTYPE()
      {
         return mTable.WorkType.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all existing data from the CMMS WORKNOTIFICATION table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public Boolean WORKNOTIFICATION()
      {
         return mTable.WorkNotification.ClearTable();

      }/* end method */

      #endregion


      #region Clear and reset multiple tables

      /// <summary>
      /// Empty the entire database - everything!
      /// </summary>
      /// <returns></returns>
      public bool All()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = 32;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // clear C_NOTE table
         result = result & mTable.CodedNote.ClearTable();
         ProgressPosition.Update( true );

         // clear CONDITIONALPOINT table
         result = result & mTable.ConditionalPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear DERIVEDITEMS table
         result = result & mTable.DerivedItems.ClearTable();
         ProgressPosition.Update( true );

         // clear derived point table
         result = result & mTable.DerivedPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear ENVDATA table
         result = result & mTable.EnvData.ClearTable();
         ProgressPosition.Update( true );

         // clear FFTCHANNEL table
         result = result & mTable.FFTChannel.ClearTable();
         ProgressPosition.Update( true );

         // clear FFTPOINT table
         result = result & mTable.FFTPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear INSPECTIONS table
         result = result & mTable.Inspection.ClearTable();
         ProgressPosition.Update( true );

         // clear INSTRUCTIONS table
         result = result & mTable.Instructions.ClearTable();
         ProgressPosition.Update( true );

         // clear LOG table
         result = result & mTable.LogTable.ClearTable();
         ProgressPosition.Update( true );

         // clear MESSAGE table
         result = result & mTable.Message.ClearTable();
         ProgressPosition.Update( true );

         // clear MCC table
         result = result & mTable.Mcc.ClearTable();
         ProgressPosition.Update( true );

         // clear NODE table
         result = result & mTable.Node.ClearTable();
         ProgressPosition.Update( true );

         // clear POINTS table
         result = result & mTable.Points.ClearTable();
         ProgressPosition.Update( true );

         // clear PROCESS Table
         result = result & mTable.Process.ClearTable();
         ProgressPosition.Update( true );

         // clear REFERENCEMEDIA table
         result = result & mTable.ReferenceMedia.ClearTable();
         ProgressPosition.Update( true );

         // clear SAVEDMEDIA table
         result = result & mTable.SavedMedia.ClearTable();
         ProgressPosition.Update( true );

         // clear STRUCTURED table
         result = result & mTable.Structured.ClearTable();
         ProgressPosition.Update( true );

         // clear TEMPDATA table
         result = result & mTable.TempData.ClearTable();
         ProgressPosition.Update( true );

         // clear TEXTDATA Table
         result = result & mTable.TextData.ClearTable();
         ProgressPosition.Update( true );

         // clear USERPREFERENCES table
         result = result & mTable.UserPreferences.ClearTable();
         ProgressPosition.Update( true );

         // clear VELDATA table
         result = result & mTable.VelData.ClearTable();
         ProgressPosition.Update( true );

         // clear NOTES table
         result = result & mTable.Notes.ClearTable();
         ProgressPosition.Update( true );

         // clear C_NOTES table
         result = result & mTable.SavedCodedNotes.ClearTable();
         ProgressPosition.Update( true );

         // clear CORRECTIVEACTION table
         result = result & mTable.CorrectiveAction.ClearTable();
         ProgressPosition.Update( true );

         // clear PROBLEM table
         result = result & mTable.Problem.ClearTable();
         ProgressPosition.Update( true );

         // clear PRIORITY table
         result = result & mTable.Priority.ClearTable();
         ProgressPosition.Update( true );

         // clear WORKTYPE table
         result = result & mTable.WorkType.ClearTable();
         ProgressPosition.Update( true );

         // clear WORKNOTIFICATION table
         result = result & mTable.WorkNotification.ClearTable();
         ProgressPosition.Update( true );

         // clear MACHINENOPSKIPS table
         result = result & mTable.MachineNOpSkips.ClearTable();
         ProgressPosition.Update( true );

         // clear MACHINEIKSKIPS table
         result = result & mTable.MachineOkSkips.ClearTable();
         ProgressPosition.Update( true );

         // clear MESSAGEPREFS table
         result = result & mTable.MessagePreferences.ClearTable();
         ProgressPosition.Update( true );

         // clear OPERATORS table
         result = result & mTable.Operators.ClearTable();
         ProgressPosition.Update( true );

         // clear the DEVICEPROFILE table
         result = result & mTable.DeviceProfile.ClearTable();
         ProgressPosition.Update( true );

         return result;

      }/* end method */


      /// <summary>
      /// Clear the Profile Table to force a new download. 
      /// Note: This method will not clear the Operators or
      /// Measurement tables, so it will still be possible to
      /// Upload any collected meausrements
      /// </summary>
      /// <returns></returns>
      public bool Profile()
      {
         // clear the DEVICEPROFILE table
         return mTable.DeviceProfile.ClearTable();

      }/* end method */


      /// <summary>
      /// Clear all tables associated with a @ptitude CMMS download
      /// </summary>
      /// <returns></returns>
      public bool DownloadCmms()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = 5;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // clear CORRECTIVEACTION table
         result = result & mTable.CorrectiveAction.ClearTable();
         ProgressPosition.Update( true );

         // clear PROBLEM table
         result = result & mTable.Problem.ClearTable();
         ProgressPosition.Update( true );

         // clear PRIORITY table
         result = result & mTable.Priority.ClearTable();
         ProgressPosition.Update( true );

         // clear WORKTYPE table
         result = result & mTable.WorkType.ClearTable();
         ProgressPosition.Update( true );

         // clear WORKNOTIFICATION table
         result = result & mTable.WorkNotification.ClearTable();
         ProgressPosition.Update( true );

         return result;

      }/* end method */


      /// <summary>
      /// Clear all tables associated with a downloaded profile
      /// </summary>
      /// <returns>true if success, else false</returns>
      public bool OperatorSettings()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = 4;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // clear MACHINENOPSKIPS table
         result = result & mTable.MachineNOpSkips.ClearTable();
         ProgressPosition.Update( true );

         // clear MACHINEIKSKIPS table
         result = result & mTable.MachineOkSkips.ClearTable();
         ProgressPosition.Update( true );

         // clear MESSAGEPREFS table
         result = result & mTable.MessagePreferences.ClearTable();
         ProgressPosition.Update( true );

         // clear OPERATORS table
         result = result & mTable.Operators.ClearTable();
         ProgressPosition.Update( true );

         return result;

      }/* end method */


      /// <summary>
      /// Clear all tables associated with a downloaded profile
      /// </summary>
      /// <returns>true if success, else false</returns>
      public bool DownloadProfile()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = 22;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // clear C_NOTE table
         result = result & mTable.CodedNote.ClearTable();
         ProgressPosition.Update( true );

         // clear CONDITIONALPOINT table
         result = result & mTable.ConditionalPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear DERIVEDITEMS table
         result = result & mTable.DerivedItems.ClearTable();
         ProgressPosition.Update( true );

         // clear derived point table
         result = result & mTable.DerivedPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear ENVDATA table
         result = result & mTable.EnvData.ClearTable();
         ProgressPosition.Update( true );

         // clear FFTCHANNEL table
         result = result & mTable.FFTChannel.ClearTable();
         ProgressPosition.Update( true );

         // clear FFTPOINT table
         result = result & mTable.FFTPoint.ClearTable();
         ProgressPosition.Update( true );

         // clear INSPECTIONS table
         result = result & mTable.Inspection.ClearTable();
         ProgressPosition.Update( true );

         // clear INSTRUCTIONS table
         result = result & mTable.Instructions.ClearTable();
         ProgressPosition.Update( true );

         // clear LOG table
         result = result & mTable.LogTable.ClearTable();
         ProgressPosition.Update( true );

         // clear MESSAGE table
         result = result & mTable.Message.ClearTable();
         ProgressPosition.Update( true );

         // clear MCC table
         result = result & mTable.Mcc.ClearTable();
         ProgressPosition.Update( true );

         // clear NODE table
         result = result & mTable.Node.ClearTable();
         ProgressPosition.Update( true );

         // clear POINTS table
         result = result & mTable.Points.ClearTable();
         ProgressPosition.Update( true );

         // clear PROCESS Table
         result = result & mTable.Process.ClearTable();
         ProgressPosition.Update( true );

         // clear REFERENCEMEDIA table
         result = result & mTable.ReferenceMedia.ClearTable();
         ProgressPosition.Update( true );

         // clear STRUCTURED table
         result = result & mTable.Structured.ClearTable();
         ProgressPosition.Update( true );

         // clear TEMPDATA table
         result = result & mTable.TempData.ClearTable();
         ProgressPosition.Update( true );

         // clear TEXTDATA Table
         result = result & mTable.TextData.ClearTable();
         ProgressPosition.Update( true );

         // clear VELDATA table
         result = result & mTable.VelData.ClearTable();
         ProgressPosition.Update( true );

         // clear NOTES table
         result = result & mTable.Notes.ClearTable();
         ProgressPosition.Update( true );

         // clear C_NOTES table
         result = result & mTable.SavedCodedNotes.ClearTable();
         ProgressPosition.Update( true );

         return result;

      }/* end method */


      /// <summary>
      /// Clear all data tables following a data upload and reset
      /// all status and alarm flags that were set during collection
      /// </summary>
      /// <returns>true if success, else false</returns>
      public bool CurrentProfileFlags()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = 11;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // clear C_NOTES table
         result = result & mTable.SavedCodedNotes.ClearTable();
         ProgressPosition.Update( true );

         // clear NOTES table
         result = result & mTable.Notes.ClearTable();
         ProgressPosition.Update( true );

         // clear LOGTABLE table
         result = result & mTable.LogTable.ClearTable();
         ProgressPosition.Update( true );

         // clear STRUCTUREDLOG table
         result = result & mTable.StructuredLog.ClearTable();
         ProgressPosition.Update( true );

         // clear FFTCHANNEL table
         result = result & mTable.FFTChannel.ClearTable();
         ProgressPosition.Update( true );

         // reset all flags within the TEMPDATA table
         result = result & mTable.TempData.ResetAllNewFlags();
         ProgressPosition.Update( true );

         // reset all flags within the VELDATA table
         result = result & mTable.VelData.ResetAllNewFlags();
         ProgressPosition.Update( true );

         // reset all flags within the ENVDATA table
         result = result & mTable.EnvData.ResetAllNewFlags();
         ProgressPosition.Update( true );

         // reset all flags within the NODE table
         result = result & mTable.Node.ResetAllFlags();
         ProgressPosition.Update( true );

         // reset all flags within the POINTS table
         result = result & mTable.Points.ResetAllFlags();
         ProgressPosition.Update( true );

         return result;

      }/* end method */


      /// <summary>
      /// Clear the Saved Media table
      /// </summary>
      /// <returns>true if success, else false</returns>
      public bool ClearSavedMedia()
      {
         Boolean result = mTable.SavedMedia.ClearTable();
         ProgressPosition.Update( true );
         return result;
      }

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 24th June 2009
//  Add to project
//----------------------------------------------------------------------------