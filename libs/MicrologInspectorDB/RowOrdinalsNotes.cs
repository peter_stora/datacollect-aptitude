﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the NOTES table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsNotes
   {
      public int Uid
      {
         get;
         set;
      }

      public int NoteType
      {
         get;
         set;
      }

      public int TextNote
      {
         get;
         set;
      }
      
      public int PointUid
      {
         get;
         set;
      }
      
      public int Number
      {
         get;
         set;
      }

      public int CollectionStamp
      {
         get;
         set;
      }

      public int LastModified
      {
         get;
         set;
      }

      public int OperId
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsNotes( SqlCeResultSet iCeResultSet )
      {
         GetNotesTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with 
      /// the NOTES table. This table will need to be revised in the event of 
      /// any changes being made to the NOTES table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetNotesTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.NoteType = iCeResultSet.GetOrdinal("NoteType");
         this.TextNote = iCeResultSet.GetOrdinal( "TextNote" );
         this.PointUid = iCeResultSet.GetOrdinal( "PointUid" );
         this.Number = iCeResultSet.GetOrdinal( "Number" );
         this.CollectionStamp = iCeResultSet.GetOrdinal( "CollectionStamp" );
         this.LastModified = iCeResultSet.GetOrdinal( "LastModified" );
         this.OperId = iCeResultSet.GetOrdinal("OperId");

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 19th March 2010
//  Implement support for Operator ID and Alarm Acknowledgement Compliance
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------
