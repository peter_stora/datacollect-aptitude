﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the C_NOTE Table ResultSet
   /// </summary>
   public class ResultSetCodedNoteTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Parameterized query string for recovering VELDATA for a particular UID
      /// </summary>
      private const string QUERY_NOTE_BY_CODE = "SELECT * FROM C_NOTE " +
          "WHERE Code = @Code";

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsCNote mColumnOrdinal = null;

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_C_NOTE;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current C_NOTE table.
      /// </summary>
      /// <param name="iCodedNote">populated Coded Note object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CodedNoteRecord iCodedNote )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write to table
               result = WriteRecord( iCodedNote, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ReferenceMedia objects to the C_NOTE 
      /// table.
      /// </summary>
      /// <param name="iCodedNotes">Enumerated List of C_NOTE objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCodedNotes iCodedNotes,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( CodedNoteRecord codedNote in iCodedNotes )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( codedNote, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ReferenceMedia objects to the C_NOTE 
      /// table.
      /// </summary>
      /// <param name="iCodedNotes">Enumerated List of C_NOTE objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCodedNotes iCodedNotes )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( CodedNoteRecord codedNote in iCodedNotes )
               {
                  if ( !WriteRecord( codedNote, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single Coded Note Record object from the C_NOTE table 
      /// using the external Note Code as the lookup reference.
      /// </summary>
      /// <param name="iNoteCode">Note Code</param>
      /// <returns>Populated Coded Note object</returns>
      public CodedNoteRecord GetRecord( int iNoteCode )
      {
         OpenCeConnection();

         // reset the default profile name
         CodedNoteRecord iCodedNote = new CodedNoteRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_NOTE_BY_CODE;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Code", SqlDbType.Int ).Value = iNoteCode;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref iCodedNote );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return iCodedNote;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all C_NOTE records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumCodedNotes GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCodedNotes codedNotes = new EnumCodedNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "C_NOTE";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the C_NOTE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CodedNoteRecord codedNote = new CodedNoteRecord();
                  PopulateObject( ceResultSet, ref codedNote );
                  codedNotes.AddCodedNoteRecord( codedNote );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return codedNotes;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// C_NOTE table. This table will need to be revised in the event of any 
      /// changes being made to the C_NOTE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsCNote( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new Coded Note record and add it 
      /// to the current C_NOTE table.
      /// </summary>
      /// <param name="iCodedNote">Populated Coded Note Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CodedNoteRecord iCodedNote,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Code ] = iCodedNote.Code;

            if ( iCodedNote.Text != null )
               newRecord[ mColumnOrdinal.Text ] = iCodedNote.Text;
            else
               newRecord[ mColumnOrdinal.Text ] = string.Empty;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single Coded Note table record
      /// </summary>
      /// <param name="iCodedNote">Updated Coded Note Record object</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CodedNoteRecord iCodedNote,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.Code, iCodedNote.Code );

            if ( iCodedNote.Text != null )
               ceResultSet.SetString( mColumnOrdinal.Text, iCodedNote.Text );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated FFT Channel Object with data from a single 
      /// FFTCHANNEL table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iFFTChannelRecord">FFT Point object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CodedNoteRecord iCodedNote )
      {
         try
         {
            iCodedNote.Code = ceResultSet.GetInt32( mColumnOrdinal.Code );
            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
               iCodedNote.Text = ceResultSet.GetString( mColumnOrdinal.Text );
         }
         catch
         {
            iCodedNote = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------