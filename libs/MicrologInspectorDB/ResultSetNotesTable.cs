﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the NOTES Table ResultSet
   /// </summary>
   public class ResultSetNotesTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Parameterized query string
      /// </summary>
      private const string QUERY_NOTES_BY_POINTUID = "SELECT * FROM NOTES " +
          "WHERE PointUid = @PointUid";

      /// <summary>
      /// Parameterized query string
      /// </summary>
      private const string QUERY_NOTES_BY_NOTEUID = "SELECT * FROM NOTES " +
          "WHERE Uid = @Uid";

      /// <summary>
      /// Parameterized query string
      /// </summary>
      private const string QUERY_NOTES_BY_NUMBER = "SELECT * FROM NOTES " +
          "WHERE Number = @Number";

      /// <summary>
      /// Parameterized query string
      /// </summary>
      private const string QUERY_BY_MEASUREMENT = "SELECT * FROM NOTES " +
          "WHERE PointUid = @PointUid AND CollectionStamp = @CollectionStamp";

      /// <summary>
      /// Parameterized query string
      /// </summary>
      private const string DELETE_MEASUREMENT_NOTE = "DELETE FROM NOTES " +
          "WHERE Uid = @Uid";

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsNotes mColumnOrdinal = null;

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_NOTES;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current NOTES table.
      /// </summary>
      /// <param name="iCodedNote">populated Coded Note object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( NotesRecord iNotesRecord )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NOTES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update the table
               result = WriteRecord( iNotesRecord, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of NotesRecord objects to the NOTES table. 
      /// </summary>
      /// <param name="iEnumUploadNotes">Enumerated List of NotesRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumNotes iEnumUploadNotes )
      {
         OpenCeConnection();

         // reset the enum list - just in case!
         iEnumUploadNotes.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NOTES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( NotesRecord note in iEnumUploadNotes )
               {
                  if ( !WriteRecord( note, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of Note Record object from the 
      /// NOTES table using the external Point Uid as the lookup.
      /// </summary>
      /// <param name="iPointUid">External Point Reference</param>
      /// <returns>Populated Notes object</returns>
      public EnumNotes GetRecords( Guid iPointUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumNotes enumFFTChannels = new EnumNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_NOTES_BY_POINTUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  NotesRecord noteRecord = new NotesRecord();

                  PopulateObject( ceResultSet, ref noteRecord );

                  // add to the enumerated list
                  if ( noteRecord != null )
                     enumFFTChannels.AddNoteRecord( noteRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumFFTChannels;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of Note Record object from the 
      /// NOTES table using the external Hierarchy index as the lookup.
      /// </summary>
      /// <param name="iPointUid">External Point Reference</param>
      /// <returns>Populated Notes object</returns>
      public EnumNotes GetRecords( int iNumber )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumNotes enumFFTChannels = new EnumNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_NOTES_BY_NUMBER;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.Int ).Value = iNumber;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  NotesRecord noteRecord = new NotesRecord();

                  PopulateObject( ceResultSet, ref noteRecord );

                  // add to the enumerated list
                  if ( noteRecord != null )
                     enumFFTChannels.AddNoteRecord( noteRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumFFTChannels;

      }/* end method */


      /// <summary>
      /// Returns a single Notes Record object from the NOTES table 
      /// using the external Hierarchy Number as the lookup reference.
      /// </summary>
      /// <param name="iPointUid">External Point Reference</param>
      /// <returns>Populated Notes object</returns>
      public NotesRecord GetRecord( Guid iNoteUid )
      {
         OpenCeConnection();

         // reset the default profile name
         NotesRecord notesRecord = new NotesRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_NOTES_BY_NOTEUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Number", SqlDbType.UniqueIdentifier ).Value = iNoteUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NOTES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref notesRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return notesRecord;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the NOTES table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public EnumNotes GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNotes notes = new EnumNotes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NOTES";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NOTES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  NotesRecord noteRecord = new NotesRecord();
                  PopulateObject( ceResultSet, ref noteRecord );
                  notes.AddNoteRecord( noteRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return notes;

      }/* end method */


      /// <summary>
      /// Update a single Notes Record
      /// </summary>
      /// <param name="iNoteRecord"></param>
      public void UpdateRecord( NotesRecord iNoteRecord )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_NOTES_BY_NOTEUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iNoteRecord.Uid;

            // execure the result set
            using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( CeResultSet );

               // now read the record object
               if ( CeResultSet.ReadFirst() )
               {
                  UpdateRecord( iNoteRecord, CeResultSet );
               }

               // close the resultSet
               CeResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Extended Methods

      /// <summary>
      /// This method returns the UID of a particular measurement note
      /// </summary>
      /// <param name="iPointUid">Point Uid lookup</param>
      /// <param name="iCollectionStamp">DateTime collection stamp</param>
      /// <returns>Guid based lookup value, or empty guid on fail</returns>
      public Guid GetMeasurementNoteUid( Guid iPointUid, DateTime iCollectionStamp )
      {
         OpenCeConnection();

         // reset the default profile name
         NotesRecord notesRecord = new NotesRecord();

         // create a blank guid
         Guid noteUid = Guid.Empty;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_MEASUREMENT;

            // reference the lookup fields as a parameters
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;
            command.Parameters.Add( "@CollectionStamp", SqlDbType.DateTime ).Value = iCollectionStamp;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NOTES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref notesRecord );
                  noteUid = notesRecord.Uid;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();
         return noteUid;

      }/* end method */


      /// <summary>
      /// Deletes a Note based on its UID
      /// </summary>
      /// <param name="iNoteUid">Unique note identifier</param>
      /// <returns>true if deleted - else false</returns>
      public Boolean DeleteSingleNote(Guid iNoteUid)
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DELETE_MEASUREMENT_NOTE;
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iNoteUid;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with
      /// the NOTES table. This table will need to be revised in the event of any 
      /// changes being made to the NOTES table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsNotes( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new upload Notes record and add it 
      /// to the current NOTES table.
      /// </summary>
      /// <param name="iNotesRecord">Populated Notes Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( NotesRecord iNotesRecord,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iNotesRecord.Uid;
            newRecord[ mColumnOrdinal.NoteType ] = iNotesRecord.NoteType;
            newRecord[ mColumnOrdinal.TextNote ] = iNotesRecord.TextNote;
            newRecord[ mColumnOrdinal.PointUid ] = iNotesRecord.PointUid;
            newRecord[ mColumnOrdinal.Number ] = iNotesRecord.Number;

            if ( iNotesRecord.CollectionStamp != DateTime.MinValue )
               newRecord[ mColumnOrdinal.CollectionStamp ] = iNotesRecord.CollectionStamp;

            newRecord[ mColumnOrdinal.LastModified ] = iNotesRecord.LastModified;
            newRecord[ mColumnOrdinal.OperId ] = iNotesRecord.OperId;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single Notes table record
      /// </summary>
      /// <param name="iNotesRecord">Updated Notes Record object</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( NotesRecord iNotesRecord,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetGuid( mColumnOrdinal.Uid, iNotesRecord.Uid );
            ceResultSet.SetByte( mColumnOrdinal.NoteType, iNotesRecord.NoteType );
            ceResultSet.SetString( mColumnOrdinal.TextNote, iNotesRecord.TextNote );
            ceResultSet.SetGuid( mColumnOrdinal.PointUid, iNotesRecord.PointUid );
            ceResultSet.SetInt32( mColumnOrdinal.Number, iNotesRecord.Number );

            if ( iNotesRecord.CollectionStamp != DateTime.MinValue )
               ceResultSet.SetDateTime( mColumnOrdinal.CollectionStamp, iNotesRecord.CollectionStamp );

            ceResultSet.SetDateTime( mColumnOrdinal.LastModified, iNotesRecord.LastModified );
            ceResultSet.SetInt32( mColumnOrdinal.OperId, iNotesRecord.OperId );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated NotesRecord Object with data from a single 
      /// NOTES table record. In the event of a raised exception the returned
      /// Notes object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iFFTChannelRecord">NotesTable to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref NotesRecord iNotesRecord )
      {
         try
         {
            iNotesRecord.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );

            if ( ceResultSet[ mColumnOrdinal.NoteType ] != DBNull.Value )
               iNotesRecord.NoteType = ceResultSet.GetByte( mColumnOrdinal.NoteType );
            else
               iNotesRecord.NoteType = (byte) FieldTypes.NoteType.AdHoc;

            if ( ceResultSet[ mColumnOrdinal.TextNote ] != DBNull.Value )
               iNotesRecord.TextNote = ceResultSet.GetString( mColumnOrdinal.TextNote );

            iNotesRecord.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iNotesRecord.Number = ceResultSet.GetInt32( mColumnOrdinal.Number );

            if ( ceResultSet[ mColumnOrdinal.CollectionStamp ] != DBNull.Value )
               iNotesRecord.CollectionStamp = ceResultSet.GetDateTime( mColumnOrdinal.CollectionStamp );
            else
               iNotesRecord.CollectionStamp = DateTime.MinValue;

            if ( ceResultSet[ mColumnOrdinal.LastModified ] != DBNull.Value )
               iNotesRecord.LastModified = ceResultSet.GetDateTime( mColumnOrdinal.LastModified );

            iNotesRecord.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
         }
         catch
         {
            iNotesRecord = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------