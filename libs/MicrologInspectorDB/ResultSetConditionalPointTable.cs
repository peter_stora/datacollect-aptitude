﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the CONDITIONAL_POINT Table ResultSet
   /// </summary>
   public class ResultSetConditionalPointTable : ResultSetBase
   {

      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsConditionalPoint mColumnOrdinal = null;

      /// <summary>
      /// Query CONDITIONALPOINT table based on the external Point Number field. 
      /// This query will return one point.
      /// </summary>
      private const string QUERY_BY_CONDITIONAL_POINT_NUMBER = "SELECT * FROM CONDITIONALPOINT " +
          "WHERE ConditionalPointNumber = @ConditionalPointNumber";

      /// <summary>
      /// Query CONDITIONALPOINT table based on the ReferencePointNumber field.
      /// This query will return one point.
      /// </summary>
      private const string QUERY_BY_REFERENCE_POINT_NUMBER = "SELECT * FROM FFTCHANNEL " +
          "WHERE ReferencePointNumber = @ReferencePointNumber";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         // set the default result
         Boolean result = true;

         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_CONDITIONALPOINT;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current CONDITIONALPOINT table. 
      /// </summary>
      /// <param name="iConditionalPointObject">populated ConditionalPointRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( ConditionalPointRecord iConditionalPointObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CONDITIONALPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write record to table
               result = WriteRecord( iConditionalPointObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the CONDITIONALPOINT table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of ConditionalPoint objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumConditionalPoints iConditionalPoints,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CONDITIONALPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all CONDITIONALPOINT records to the table 
               foreach ( ConditionalPointRecord conditionalPoint in iConditionalPoints )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( conditionalPoint, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the CONDITIONALPOINT table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of ConditionalPoint objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumConditionalPoints iConditionalPoints )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CONDITIONALPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all CONDITIONALPOINT records to the table 
               foreach ( ConditionalPointRecord derivedPoint in iConditionalPoints )
               {
                  // write record to table
                  if ( !WriteRecord( derivedPoint, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single Conditional Point Record object from the CONDITIONALPOINT 
      /// table using the external ConditionalPointNumber (links to POINTS.Number) as 
      /// the lookup reference. 
      /// </summary>
      /// <param name="iConditionalPointNo">External link to Point Number</param>
      /// <returns>Populated Conditional Point object or null on error</returns>
      public ConditionalPointRecord GetRecord( int iConditionalPointNo )
      {
         OpenCeConnection();

         // reset the default profile name
         ConditionalPointRecord conditionalPointRecord = new ConditionalPointRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CONDITIONAL_POINT_NUMBER;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@ConditionalPointNumber", SqlDbType.Int ).Value
                                                                 = iConditionalPointNo;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CONDITIONALPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref conditionalPointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();
         return conditionalPointRecord;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CONDITIONALPOINT records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumConditionalPoints GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumConditionalPoints conditionalPoints = new EnumConditionalPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CONDITIONALPOINT";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CONDITIONALPOINT table. As this is a 'one shot' the column  
               // ordinal object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ConditionalPointRecord conditionalPoint = new ConditionalPointRecord();
                  PopulateObject( ceResultSet, ref conditionalPoint );
                  conditionalPoints.AddConditionalPointRecord( conditionalPoint );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return conditionalPoints;

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Returns a single Conditional Point Record object from the CONDITIONALPOINT 
      /// table using the external ConditionalPointNumber (links to POINTS.Number) as 
      /// the lookup reference.
      /// </summary>
      /// <param name="iConditionalPointNo">External link to Point Number</param>
      /// <returns>Populated Conditional Point object or null on error</returns>
      public ConditionalPointRecord GetConditionalPointFromReference( int iReferencePointNo )
      {
         OpenCeConnection();

         // reset the default profile name
         ConditionalPointRecord conditionalPointRecord = new ConditionalPointRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_REFERENCE_POINT_NUMBER;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@ReferencePointNumber", SqlDbType.Int ).Value
                                                                 = iReferencePointNo;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CONDITIONALPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref conditionalPointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return conditionalPointRecord;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// CONDITIONALPOINT table. This table will need to be revised in the event of any 
      /// changes being made to the CONDITIONALPOINT table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsConditionalPoint( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new CONDITIONALPOINT record and add it 
      /// to the current CONDITIONALPOINT table.
      /// </summary>
      /// <param name="iConditionalPoint">Populated CONDITIONALPOINT Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( ConditionalPointRecord iConditionalPoint,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.ID ] = iConditionalPoint.ID;
            newRecord[ mColumnOrdinal.ConditionalPointNumber ] = iConditionalPoint.ConditionalPointNumber;
            newRecord[ mColumnOrdinal.ReferencePointNumber ] = iConditionalPoint.ReferencePointNumber;
            newRecord[ mColumnOrdinal.CriteriaType ] = iConditionalPoint.CriteriaType;
            newRecord[ mColumnOrdinal.MinRange ] = iConditionalPoint.MinRange;
            newRecord[ mColumnOrdinal.MaxRange ] = iConditionalPoint.MaxRange;
            newRecord[ mColumnOrdinal.ResultIdx ] = iConditionalPoint.ResultIdx;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single CONDITIONALPOINT table record
      /// </summary>
      /// <param name="iFFTChannelRecord">Updated CONDITIONALPOINT Record object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( ConditionalPointRecord iConditionalPoint,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.ID, iConditionalPoint.ID );
            ceResultSet.SetInt32( mColumnOrdinal.ConditionalPointNumber, iConditionalPoint.ConditionalPointNumber );
            ceResultSet.SetInt32( mColumnOrdinal.ReferencePointNumber, iConditionalPoint.ReferencePointNumber );
            ceResultSet.SetInt32( mColumnOrdinal.CriteriaType, iConditionalPoint.CriteriaType );
            ceResultSet.SetDouble( mColumnOrdinal.MinRange, iConditionalPoint.MinRange );
            ceResultSet.SetDouble( mColumnOrdinal.MaxRange, iConditionalPoint.MaxRange );
            ceResultSet.SetInt32( mColumnOrdinal.ResultIdx, iConditionalPoint.ResultIdx );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated CONDITIONALPOINT Object with data from a single 
      /// CONDITIONALPOINT table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iFFTChannelRecord">CONDITIONALPOINT object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref ConditionalPointRecord iConditionalPoint )
      {
         try
         {
            iConditionalPoint.ID = ceResultSet.GetInt32( mColumnOrdinal.ID );
            iConditionalPoint.ConditionalPointNumber = ceResultSet.GetInt32( mColumnOrdinal.ConditionalPointNumber );
            iConditionalPoint.ReferencePointNumber = ceResultSet.GetInt32( mColumnOrdinal.ReferencePointNumber );
            iConditionalPoint.CriteriaType = ceResultSet.GetInt32( mColumnOrdinal.CriteriaType );
            iConditionalPoint.MinRange = ceResultSet.GetDouble( mColumnOrdinal.MinRange );
            iConditionalPoint.MaxRange = ceResultSet.GetDouble( mColumnOrdinal.MaxRange );
            iConditionalPoint.ResultIdx = ceResultSet.GetInt32( mColumnOrdinal.ResultIdx );
         }
         catch
         {
            iConditionalPoint = null;
         }

      }/* end method */


      #endregion


   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------