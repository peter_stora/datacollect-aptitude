﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the INSTRUCTIONS table. This structure must be revised following 
    /// any changes made to the table's field structure.
    /// </summary>
    public class RowOrdinalsInstructions
    {
        public int PointUid { get; set; }
        public int TextFormat { get; set; }
        public int TextType { get; set; }
        public int TextTitle { get; set; }
        public int TextBody { get; set; }
        public int PointHostIndex { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsInstructions(SqlCeResultSet iCeResultSet)
        {
            GetInstructionTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// INSTRUCTIONS table. This method will need to be revised in the event of 
        /// any changes being made to the INSTRUCTIONS table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetInstructionTableColumns(SqlCeResultSet iCeResultSet)
        {

            this.PointUid = iCeResultSet.GetOrdinal("PointUid");
            this.TextFormat = iCeResultSet.GetOrdinal("TextFormat");
            this.TextType = iCeResultSet.GetOrdinal("TextType");
            this.TextTitle = iCeResultSet.GetOrdinal("TextTitle");
            this.TextBody = iCeResultSet.GetOrdinal("TextBody");
            this.PointHostIndex = iCeResultSet.GetOrdinal("PointHostIndex");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------