﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Database.Properties;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// This class opens all tables in the selected database and exposes
    /// a single entry point for all of them. Although the default Database
    /// is "Inspector.DB", if required another Database can be selected via
    /// the supplied overload constructor. When no longer required, the 
    /// dispose() method should be called to release the unmanaged 
    /// SqlConnection object.
    /// 
    /// Although there is only a single entry point, we actually employ two
    /// different connection objects. One manages the core connections (such
    /// as points and nodes etc), while the other manages reference style
    /// connections (such as Instructions, Conditional or Derived Points etc)
    /// 
    /// </summary>
    public class TableConnect : IDisposable, ITableConnect
    {
        #region Private Fields

        /// <summary>
        /// Connection that manages all core table connections 
        /// (i.e. those primarily held in memory)
        /// </summary>
        private SqlConnectionObject mSqlCeConnectCore = null;

        /// <summary>
        /// Connection that manages reference table connections
        /// (i.e. those that are referenced during collection)
        /// </summary>
        private SqlConnectionObject mSqlCeConnectRef = null;

        /// <summary>
        /// Path to datasource
        /// </summary>
        private string mDataSource;

        /// <summary>
        /// Is the current database version OK?
        /// </summary>
        private Boolean mVersionOK;

        /// <summary>
        /// What database version does the application have
        /// </summary>
        private string mCurrentDbVersion = string.Empty;

        private ResultSetCodedNoteTable mCodedNoteTableResultSet = null;
        private ResultSetConditionalPointTable mConditionalPointTableResultSet = null;
        private ResultSetCorrectiveAction mCorrectiveActionResultSet = null;
        private ResultSetDerivedItemsTable mDerivedItemsTableResultSet = null;
        private ResultSetDerivedPointTable mDerivedPointTableResultSet = null;
        private ResultSetDeviceProfileTable mDeviceProfileTableResultSet = null;
        private ResultSetEnvDataTable mEnvDataTableResultSet = null;
        private ResultSetFFTChannelTable mFFTChannelResultSet = null;
        private ResultSetFFTPointTable mFFTPointTableResultSet = null;
        private ResultSetInspectionTable mInspectionTableResultSet = null;
        private ResultSetInstructionsTable mInstructionsTableResultSet = null;
        private ResultSetUserPreferences mLocalSettings = null;
        private ResultSetLogTable mLogTableResultSet = null;
        private ResultSetMachineNOpSkips mMachineNOpSkipsResultSet = null;
        private ResultSetMachineOKSkips mMachineOkSkipsResultSet = null;
        private ResultSetMCCTable mMccTableResultSet = null;
        private ResultSetMessagePrefs mMessagePreferencesResultSet = null;
        private ResultSetMessageTable mMessageTableResultSet = null;
        private ResultSetNodeTable mNodeTableResultSet = null;
        private ResultSetNotesTable mNotesTableResultSet = null;
        private ResultSetOperators mOperatorsTableResultSet = null;
        private ResultSetPointsTable mPointsTableResultSet = null;
        private ResultSetPriority mPriorityTableResultSet = null;
        private ResultSetProblem mProblemTableResultSet = null;
        private ResultSetProcessTable mProcessTableResultSet = null;
        private ResultSetTextDataTable mTextDataTableResultSet = null;
        private ResultSetReferenceMediaTable mReferenceMediaTableResultSet = null;
        private ResultSetSavedMediaTable mSavedMediaTableResultSet = null;
        private ResultSetSavedCodedNotesTable mSavedCodedNotesTableResultSet = null;
        private ResultSetStructuredLogTable mStructuredLogTableResultSet = null;
        private ResultSetStructuredTable mStructuredTableResultSet = null;
        private ResultSetTempDataTable mTempDataTableResultSet = null;
        private ResultSetVelDataTable mVelDataTableResultSet = null;
        private ResultSetWorkNotification mWorkNotificationResultSet = null;
        private ResultSetWorkType mWorkTypeTableResultSet = null;

        // Track whether Dispose has been called.
        private bool mDisposed = false;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Base Constructor - defaults to the Database "Inspector.DB"
        /// </summary>
        public TableConnect()
           : this(SqlConnectionObject.DEFAULT_DATASOURCE)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor - allows selection of alternate Database
        /// </summary>
        public TableConnect(string iDataSource)
        {
            // reference the datasource
            mDataSource = iDataSource;

            // build and initialize the required result sets
            BuildConnection();

        }/* end constructor */


        /// <summary>
        /// Implement IDisposable.
        /// Note: A derived class must not be able to override this method!
        /// </summary>
        public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            //
            // tell the Garbage Collector that the Finalize process no 
            // longer needs to be run for this object.
            //
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios.
        /// If disposing equals true, the method has been called directly
        /// or indirectly by a user's code. Managed and unmanaged resources
        /// can be disposed.
        /// If disposing equals false, the method has been called by the
        /// runtime from inside the finalizer and you should not reference
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="iDisposeManagedResources"></param>
        private void Dispose(bool iDisposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (iDisposing)
                {
                    try
                    {
                        mSqlCeConnectCore.Dispose();
                        mSqlCeConnectCore = null;

                        mSqlCeConnectRef.Dispose();
                        mSqlCeConnectRef = null;
                    }
                    catch
                    {
                        mSqlCeConnectCore = null;
                        mSqlCeConnectRef = null;
                    }
                }
                // Note disposing has been done.
                mDisposed = true;
            }
        }


        /// <summary>
        /// This destructor will run only if the Dispose method
        /// does not get called.
        /// It gives your base class the opportunity to finalize.
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~TableConnect()
        {
            Dispose(false);
        }

        #endregion


        /// <summary>
        /// Build the connection object and test the valididy 
        /// of the SQL connection
        /// </summary>
        /// <returns>false on connection error</returns>
        private Boolean BuildConnection()
        {
            // create the new core SQL connection object
            if (mSqlCeConnectCore == null)
                mSqlCeConnectCore = new SqlConnectionObject(mDataSource);

            // create the new reference SQL connection object
            if (mSqlCeConnectRef == null)
                mSqlCeConnectRef = new SqlConnectionObject(mDataSource);

            // check to see if it's OK
            if (ResultSetBase.TestConnection(mSqlCeConnectCore) &
               (ResultSetBase.TestConnection(mSqlCeConnectRef)))
            {
                Console.WriteLine("Test connection OK");
                //
                // if SQL connection OK and system not already 
                // connected then build and initialize the required 
                // result sets
                //
                if (!mSqlCeConnectCore.Connected() & !mSqlCeConnectRef.Connected())
                {
                    if (GetCurrentDatabaseVersion() == Resources.IDS_VERSION_DB)
                    {
                        // set the DB version flag to good
                        mVersionOK = true;
                        Console.WriteLine("Db version ok");

                        // build and initialize the result sets
                        BuildAllResultSets();
                        InitialiseResultSets();
                    }
                    else
                    {
                        // set the DB version flag to fail
                        mVersionOK = false;

                        // if version wrong dispose of connection and return false
                        HandleConnectionFailed();
                        Console.WriteLine("Invalid db version");
                        return false;
                    }
                }
                return true;
            }
            else
            {
                Console.WriteLine("Test connection failed");
                // on error dispose of connection and return false
                HandleConnectionFailed();
                return false;
            }

        }/* end method */


        /// <summary>
        /// Close and dispose of the database connections
        /// </summary>
        private void HandleConnectionFailed()
        {
            mSqlCeConnectCore.Dispose();
            mSqlCeConnectCore = null;

            mSqlCeConnectRef.Disconnect();
            mSqlCeConnectRef = null;

        }/* end method */


        /// <summary>
        /// Get the current database version
        /// </summary>
        /// <returns></returns>
        private string GetCurrentDatabaseVersion()
        {
            ResultSetVersion version = new ResultSetVersion();
            version.Initialize(mSqlCeConnectRef);
            mCurrentDbVersion = version.GetCurrentVersion().ToString();
            return mCurrentDbVersion;

        }/* end method */


        /// <summary>
        /// Build all ResultSet objects
        /// </summary>
        private void BuildAllResultSets()
        {
            mCodedNoteTableResultSet = new ResultSetCodedNoteTable();
            mConditionalPointTableResultSet = new ResultSetConditionalPointTable();
            mCorrectiveActionResultSet = new ResultSetCorrectiveAction();
            mDerivedItemsTableResultSet = new ResultSetDerivedItemsTable();
            mDerivedPointTableResultSet = new ResultSetDerivedPointTable();
            mDeviceProfileTableResultSet = new ResultSetDeviceProfileTable();
            mEnvDataTableResultSet = new ResultSetEnvDataTable();
            mFFTChannelResultSet = new ResultSetFFTChannelTable();
            mFFTPointTableResultSet = new ResultSetFFTPointTable();
            mInspectionTableResultSet = new ResultSetInspectionTable();
            mInstructionsTableResultSet = new ResultSetInstructionsTable();
            mLocalSettings = new ResultSetUserPreferences();
            mLogTableResultSet = new ResultSetLogTable();
            mMachineNOpSkipsResultSet = new ResultSetMachineNOpSkips();
            mMachineOkSkipsResultSet = new ResultSetMachineOKSkips();
            mMccTableResultSet = new ResultSetMCCTable();
            mMessagePreferencesResultSet = new ResultSetMessagePrefs();
            mMessageTableResultSet = new ResultSetMessageTable();
            mNodeTableResultSet = new ResultSetNodeTable();
            mNotesTableResultSet = new ResultSetNotesTable();
            mOperatorsTableResultSet = new ResultSetOperators();
            mPointsTableResultSet = new ResultSetPointsTable();
            mPriorityTableResultSet = new ResultSetPriority();
            mProblemTableResultSet = new ResultSetProblem();
            mProcessTableResultSet = new ResultSetProcessTable();
            mTextDataTableResultSet = new ResultSetTextDataTable();
            mReferenceMediaTableResultSet = new ResultSetReferenceMediaTable();
            mSavedMediaTableResultSet = new ResultSetSavedMediaTable();
            mSavedCodedNotesTableResultSet = new ResultSetSavedCodedNotesTable();
            mStructuredLogTableResultSet = new ResultSetStructuredLogTable();
            mStructuredTableResultSet = new ResultSetStructuredTable();
            mTempDataTableResultSet = new ResultSetTempDataTable();
            mVelDataTableResultSet = new ResultSetVelDataTable();
            mWorkNotificationResultSet = new ResultSetWorkNotification();
            mWorkTypeTableResultSet = new ResultSetWorkType();

        }/* end method */


        /// <summary>
        /// Open all "Direct Access" enabled tables
        /// </summary>
        private void InitialiseResultSets()
        {
            //
            // initialize the core connections
            //
            mEnvDataTableResultSet.Initialize(mSqlCeConnectCore);
            mFFTChannelResultSet.Initialize(mSqlCeConnectCore);
            mLogTableResultSet.Initialize(mSqlCeConnectCore);
            mNodeTableResultSet.Initialize(mSqlCeConnectCore);
            mNotesTableResultSet.Initialize(mSqlCeConnectCore);
            mPointsTableResultSet.Initialize(mSqlCeConnectCore);
            mSavedMediaTableResultSet.Initialize(mSqlCeConnectCore);
            mSavedCodedNotesTableResultSet.Initialize(mSqlCeConnectCore);
            mStructuredLogTableResultSet.Initialize(mSqlCeConnectCore);
            mStructuredTableResultSet.Initialize(mSqlCeConnectCore);
            mTempDataTableResultSet.Initialize(mSqlCeConnectCore);
            mVelDataTableResultSet.Initialize(mSqlCeConnectCore);
            mWorkNotificationResultSet.Initialize(mSqlCeConnectCore);

            //
            // initialize the reference connections
            //
            mCodedNoteTableResultSet.Initialize(mSqlCeConnectRef);
            mConditionalPointTableResultSet.Initialize(mSqlCeConnectRef);
            mCorrectiveActionResultSet.Initialize(mSqlCeConnectRef);
            mInspectionTableResultSet.Initialize(mSqlCeConnectRef);
            mDerivedItemsTableResultSet.Initialize(mSqlCeConnectRef);
            mDerivedPointTableResultSet.Initialize(mSqlCeConnectRef);
            mDeviceProfileTableResultSet.Initialize(mSqlCeConnectRef);
            mFFTPointTableResultSet.Initialize(mSqlCeConnectRef);
            mInstructionsTableResultSet.Initialize(mSqlCeConnectRef);
            mLocalSettings.Initialize(mSqlCeConnectRef);
            mMachineNOpSkipsResultSet.Initialize(mSqlCeConnectRef);
            mMachineOkSkipsResultSet.Initialize(mSqlCeConnectRef);
            mMccTableResultSet.Initialize(mSqlCeConnectRef);
            mMessagePreferencesResultSet.Initialize(mSqlCeConnectRef);
            mMessageTableResultSet.Initialize(mSqlCeConnectRef);
            mOperatorsTableResultSet.Initialize(mSqlCeConnectRef);
            mPriorityTableResultSet.Initialize(mSqlCeConnectRef);
            mProblemTableResultSet.Initialize(mSqlCeConnectRef);
            mProcessTableResultSet.Initialize(mSqlCeConnectRef);
            mTextDataTableResultSet.Initialize(mSqlCeConnectRef);
            mReferenceMediaTableResultSet.Initialize(mSqlCeConnectRef);
            mWorkTypeTableResultSet.Initialize(mSqlCeConnectRef);

        }/* end method */


        #region Public Methods and Properties

        /// <summary>
        /// Gets or Sets the C_NOTE Table ResultSet
        /// </summary>
        public ResultSetCodedNoteTable CodedNote
        {
            get
            {
                return mCodedNoteTableResultSet;
            }
            set
            {
                mCodedNoteTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CONDITIONALPOINT Table ResultSet
        /// </summary>
        public ResultSetConditionalPointTable ConditionalPoint
        {
            get
            {
                return mConditionalPointTableResultSet;
            }
            set
            {
                mConditionalPointTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CMMS CORRECTIVEACTION Table ResultSet
        /// </summary>
        public ResultSetCorrectiveAction CorrectiveAction
        {
            get
            {
                return mCorrectiveActionResultSet;
            }
            set
            {
                mCorrectiveActionResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the DERIVEDITEMS Table ResultSet
        /// </summary>
        public ResultSetDerivedItemsTable DerivedItems
        {
            get
            {
                return mDerivedItemsTableResultSet;
            }
            set
            {
                mDerivedItemsTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the DERIVEDPOINT Table ResultSet
        /// </summary>
        public ResultSetDerivedPointTable DerivedPoint
        {
            get
            {
                return mDerivedPointTableResultSet;
            }
            set
            {
                mDerivedPointTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the DEVICEPROFILE Table ResultSet
        /// </summary>
        public ResultSetDeviceProfileTable DeviceProfile
        {
            get
            {
                return mDeviceProfileTableResultSet;
            }
            set
            {
                mDeviceProfileTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the ENVDATA Table ResultSet
        /// </summary>
        public ResultSetEnvDataTable EnvData
        {
            get
            {
                return mEnvDataTableResultSet;
            }
            set
            {
                mEnvDataTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the FFTCHANNEL Table ResultSet
        /// </summary>
        public ResultSetFFTChannelTable FFTChannel
        {
            get
            {
                return mFFTChannelResultSet;
            }
            set
            {
                mFFTChannelResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the FFTPOINT Table ResultSet
        /// </summary>
        public ResultSetFFTPointTable FFTPoint
        {
            get
            {
                return mFFTPointTableResultSet;
            }
            set
            {
                mFFTPointTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the INSPECTION Table ResultSet
        /// </summary>
        public ResultSetInspectionTable Inspection
        {
            get
            {
                return mInspectionTableResultSet;
            }
            set
            {
                mInspectionTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the INSTRUCTIONS Table ResultSet
        /// </summary>
        public ResultSetInstructionsTable Instructions
        {
            get
            {
                return mInstructionsTableResultSet;
            }
            set
            {
                mInstructionsTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the LOG Table ResultSet
        /// </summary>
        public ResultSetUserPreferences UserPreferences
        {
            get
            {
                return mLocalSettings;
            }
            set
            {
                mLocalSettings = value;
            }
        }


        /// <summary>
        /// Gets or Sets the LOG Table ResultSet
        /// </summary>
        public ResultSetLogTable LogTable
        {
            get
            {
                return mLogTableResultSet;
            }
            set
            {
                mLogTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the Operator MACHINENOPSKIPS Skips Table ResultSet
        /// </summary>
        public ResultSetMachineNOpSkips MachineNOpSkips
        {
            get
            {
                return mMachineNOpSkipsResultSet;
            }
            set
            {
                mMachineNOpSkipsResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the Operator MACHINEOKSKIPS Table ResultSet
        /// </summary>
        public ResultSetMachineOKSkips MachineOkSkips
        {
            get
            {
                return mMachineOkSkipsResultSet;
            }
            set
            {
                mMachineOkSkipsResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the MCC Table ResultSet
        /// </summary>
        public ResultSetMCCTable Mcc
        {
            get
            {
                return mMccTableResultSet;
            }
            set
            {
                mMccTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the Operator MESSAGINGPREFS Table ResultSet
        /// </summary>
        public ResultSetMessagePrefs MessagePreferences
        {
            get
            {
                return mMessagePreferencesResultSet;
            }
            set
            {
                mMessagePreferencesResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the MESSAGE Table ResultSet
        /// </summary>
        public ResultSetMessageTable Message
        {
            get
            {
                return mMessageTableResultSet;
            }
            set
            {
                mMessageTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the NODE Table ResultSet
        /// </summary>
        public ResultSetNodeTable Node
        {
            get
            {
                return mNodeTableResultSet;
            }
            set
            {
                mNodeTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the C_NOTES Table ResultSet
        /// </summary>
        public ResultSetNotesTable Notes
        {
            get
            {
                return mNotesTableResultSet;
            }
            set
            {
                mNotesTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the OPERATORS Table ResultSet
        /// </summary>
        public ResultSetOperators Operators
        {
            get
            {
                return mOperatorsTableResultSet;
            }
            set
            {
                mOperatorsTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the POINTS Table ResultSet
        /// </summary>
        public ResultSetPointsTable Points
        {
            get
            {
                return mPointsTableResultSet;
            }
            set
            {
                mPointsTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CMMS PRIORITY Table ResultSet
        /// </summary>
        public ResultSetPriority Priority
        {
            get
            {
                return mPriorityTableResultSet;
            }
            set
            {
                mPriorityTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CMMS PROBLEM Table ResultSet
        /// </summary>
        public ResultSetProblem Problem
        {
            get
            {
                return mProblemTableResultSet;
            }
            set
            {
                mProblemTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the PROCESS Table ResultSet
        /// </summary>
        public ResultSetProcessTable Process
        {
            get
            {
                return mProcessTableResultSet;
            }
            set
            {
                mProcessTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the TEXTDATA Table ResultSet
        /// </summary>
        public ResultSetTextDataTable TextData
        {
            get
            {
                return mTextDataTableResultSet;
            }
            set
            {
                mTextDataTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the REFERENCEMEDIA Table ResultSet
        /// </summary>
        public ResultSetReferenceMediaTable ReferenceMedia
        {
            get
            {
                return mReferenceMediaTableResultSet;
            }
            set
            {
                mReferenceMediaTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the SAVEDMEDIA Table ResultSet
        /// </summary>
        public ResultSetSavedMediaTable SavedMedia
        {
            get
            {
                return mSavedMediaTableResultSet;
            }
            set
            {
                mSavedMediaTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CODEDNOTES Table ResultSet
        /// </summary>
        public ResultSetSavedCodedNotesTable SavedCodedNotes
        {
            get
            {
                return mSavedCodedNotesTableResultSet;
            }
            set
            {
                mSavedCodedNotesTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the STRUCTUREDLOG Table ResultSet
        /// </summary>
        public ResultSetStructuredLogTable StructuredLog
        {
            get
            {
                return mStructuredLogTableResultSet;
            }
            set
            {
                mStructuredLogTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the STRUCTURED Table ResultSet
        /// </summary>
        public ResultSetStructuredTable Structured
        {
            get
            {
                return mStructuredTableResultSet;
            }
            set
            {
                mStructuredTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the TEMPDATA Table ResultSet
        /// </summary>
        public ResultSetTempDataTable TempData
        {
            get
            {
                return mTempDataTableResultSet;
            }
            set
            {
                mTempDataTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the VELDATA Table ResultSet
        /// </summary>
        public ResultSetVelDataTable VelData
        {
            get
            {
                return mVelDataTableResultSet;
            }
            set
            {
                mVelDataTableResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CMMS WORKNOTIFICATION Table ResultSet
        /// </summary>
        public ResultSetWorkNotification WorkNotification
        {
            get
            {
                return mWorkNotificationResultSet;
            }
            set
            {
                mWorkNotificationResultSet = value;
            }
        }


        /// <summary>
        /// Gets or Sets the CMMS WORKTYPE Table ResultSet
        /// </summary>
        public ResultSetWorkType WorkType
        {
            get
            {
                return mWorkTypeTableResultSet;
            }
            set
            {
                mWorkTypeTableResultSet = value;
            }
        }


        /// <summary>
        /// Test the SQL connection is valid
        /// </summary>
        /// <returns>false on connection error</returns>
        public Boolean TestConnection()
        {
            // test by simply building the connection
            return BuildConnection();

        }/* end method */


        /// <summary>
        /// Checks whether the database version is OK.
        /// </summary>
        /// <returns></returns>
        public Boolean DatabaseVersionOK
        {
            get
            {
                return mVersionOK;
            }
        }


        /// <summary>
        /// Gets the current database version
        /// </summary>
        public string CurrentDbVersion
        {
            get
            {
                return mCurrentDbVersion;
            }
        }

        #endregion

    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 3.0 APinkerton 11th September 2012
//  Add support for TextData Measurement types
//
//  Revision 2.0 APinkerton 25th February 2011
//  Add support for database version control
//
//  Revision 1.0 APinkerton 4th March 2010
//  Add support for the new LocalSettings table
//
//  Revision 0.0 APinkerton 24th June 2009
//  Add to project
//----------------------------------------------------------------------------

