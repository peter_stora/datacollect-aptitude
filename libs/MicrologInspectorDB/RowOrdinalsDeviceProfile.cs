﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the DEVICEPROFILE table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsDeviceProfile
   {
      public int ProfileName
      {
         get;
         set;
      }

      public int ProfileID
      {
         get;
         set;
      }

      public int DeviceName
      {
         get;
         set;
      }

      public int LastModified
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsDeviceProfile( SqlCeResultSet iCeResultSet )
      {
         GetDeviceProfileTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with 
      /// the DEVICEPROFILE table. This table will need to be revised in the event of 
      /// any changes being made to the DEVICEPROFILE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetDeviceProfileTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.ProfileName = iCeResultSet.GetOrdinal( "ProfileName" );
         this.ProfileID = iCeResultSet.GetOrdinal( "ProfileID" );
         this.DeviceName = iCeResultSet.GetOrdinal( "DeviceName" );
         this.LastModified = iCeResultSet.GetOrdinal( "LastModified" );
      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.1 APinkerton 25th February 2010
//  Add columns ProfileID and DeviceName
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------

