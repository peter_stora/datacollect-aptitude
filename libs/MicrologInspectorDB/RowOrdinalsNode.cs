﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the NODE table. This structure should be revised following any
    /// changes made to this table.
    /// </summary>
    public class RowOrdinalsNode
    {
        public int Uid { get; set; }
        public int Id { get; set; }
        public int Description { get; set; }
        public int LastModified { get; set; }
        public int LocationMethod { get; set; }
        public int LocationTag { get; set; }
        public int ParentUid { get; set; }
        public int PRISM { get; set; }
        public int Number { get; set; }
        public int HierarchyDepth { get; set; }
        public int Flags { get; set; }
        public int Structured { get; set; }
        public int RouteId { get; set; }
        public int FunctionalLocation { get; set; }
        public int DefWorkType { get; set; }
        public int LastInspResult { get; set; }
        public int NoLastMeas { get; set; }
        public int ElementType { get; set; }
        public int CheckMark { get; set; }
        public int AlarmState { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsNode(SqlCeResultSet iCeResultSet)
        {
            GetNodeTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// NODE table. This table will need to be revised in the event of any changes
        /// being made to the NODE table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetNodeTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.Id = iCeResultSet.GetOrdinal("Id");
            this.Description = iCeResultSet.GetOrdinal("Description");
            this.LastModified = iCeResultSet.GetOrdinal("LastModified");
            this.LocationMethod = iCeResultSet.GetOrdinal("LocationMethod");
            this.LocationTag = iCeResultSet.GetOrdinal("LocationTag");
            this.ParentUid = iCeResultSet.GetOrdinal("ParentUid");
            this.PRISM = iCeResultSet.GetOrdinal("PRISM");
            this.Number = iCeResultSet.GetOrdinal("Number");
            this.HierarchyDepth = iCeResultSet.GetOrdinal("HierarchyDepth");
            this.Flags = iCeResultSet.GetOrdinal("Flags");
            this.Structured = iCeResultSet.GetOrdinal("Structured");
            this.RouteId = iCeResultSet.GetOrdinal("RouteId");
            this.FunctionalLocation = iCeResultSet.GetOrdinal("FunctionalLocation");
            this.DefWorkType = iCeResultSet.GetOrdinal("DefWorkType");
            this.LastInspResult = iCeResultSet.GetOrdinal("LastInspResult");
            this.NoLastMeas = iCeResultSet.GetOrdinal("NoLastMeas");
            this.ElementType = iCeResultSet.GetOrdinal("ElementType");
            this.CheckMark = iCeResultSet.GetOrdinal("CheckMark");
            this.AlarmState = iCeResultSet.GetOrdinal("AlarmState");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------