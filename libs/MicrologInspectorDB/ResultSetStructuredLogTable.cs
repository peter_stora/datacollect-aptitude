﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the STRUCTUREDLOG Table ResultSet
   /// </summary>
   public class ResultSetStructuredLogTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Parameterized query string: Query by Route ID
      /// </summary>
      private const string QUERY_LOG_BY_ROUTEID = "SELECT * FROM STRUCTUREDLOG " +
          "WHERE RouteId = @RouteId";

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsStructuredLog mColumnOrdinal = null;

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_STRUCTUREDLOG;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current STRUCTUREDLOG table. 
      /// </summary>
      /// <param name="iStructuredLog">populated Structured Log object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( UploadStructuredLogRecord iStructuredLog )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTUREDLOG";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write record to table
               result = WriteRecord( iStructuredLog, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single Coded Note Record object from the STRUCTUREDLOG table 
      /// using the external Route ID as the lookup reference.
      /// </summary>
      /// <param name="iNoteCode">Note Code</param>
      /// <returns>Populated Coded Note object</returns>
      public UploadStructuredLogRecord GetRecord( int iRouteId )
      {
         OpenCeConnection();

         // reset the default profile name
         UploadStructuredLogRecord structuredLog = new UploadStructuredLogRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_LOG_BY_ROUTEID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@RouteId", SqlDbType.Int ).Value = iRouteId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the STRUCTUREDLOG  table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref structuredLog );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return structuredLog;

      }/* end method */


      /// <summary>
      /// Returns a single Coded Note Record object from the STRUCTUREDLOG table 
      /// using the external Route ID as the lookup reference.
      /// </summary>
      /// <param name="iNoteCode">Note Code</param>
      /// <returns>Populated Coded Note object</returns>
      public EnumUploadStructuredLogs GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumUploadStructuredLogs logs = new EnumUploadStructuredLogs();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTUREDLOG";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  UploadStructuredLogRecord logRecord = new UploadStructuredLogRecord();
                  PopulateObject( ceResultSet, ref logRecord );
                  logs.AddStructuredLog( logRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return logs;

      }/* end method */


      /// <summary>
      /// This method updates a single record in the Structured Log table
      /// using a reference log record supplied as a parameter. As this 
      /// method will update all fields in the record, the reference object
      /// must be fully populated prior to use!
      /// </summary>
      /// <param name="iRefLogItem">Object to update</param>
      public void UpdateRecord( UploadStructuredLogRecord iRefLogItem )
      {
         OpenCeConnection();

         // reset the default profile name
         UploadStructuredLogRecord updateLogItem = new UploadStructuredLogRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_LOG_BY_ROUTEID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@RouteId", SqlDbType.Int ).Value = iRefLogItem.RouteId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                ResultSetOptions.Scrollable | ResultSetOptions.Updatable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  // populate the Node object
                  PopulateObject( ceResultSet, ref updateLogItem );

                  // update the fields
                  updateLogItem.StartTime = iRefLogItem.StartTime;
                  updateLogItem.EndTime = iRefLogItem.EndTime;
                  updateLogItem.PercentComplete = iRefLogItem.PercentComplete;
                  updateLogItem.OperId = iRefLogItem.OperId;

                  //
                  // only do something if the node object was actually
                  // populated and is valid
                  //
                  if ( updateLogItem != null )
                  {
                     // now update the record and resturn the Parent Uid
                     UpdateRecord( updateLogItem, ceResultSet );
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// STRUCTUREDLOG table. This table will need to be revised in the event of any 
      /// changes being made to the STRUCTUREDLOG table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsStructuredLog( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new Coded Note record and add it 
      /// to the current STRUCTUREDLOG table.
      /// </summary>
      /// <param name="iStructuredLog">Populated Upload Structured Log object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( UploadStructuredLogRecord iStructuredLog,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.RouteId ] = iStructuredLog.RouteId;

            if ( iStructuredLog.StartTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.StartTime ] = iStructuredLog.StartTime;

            if ( iStructuredLog.EndTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.EndTime ] = iStructuredLog.EndTime;

            newRecord[ mColumnOrdinal.Duration ] = iStructuredLog.Duration;
            newRecord[ mColumnOrdinal.OperId ] = iStructuredLog.OperId;
            newRecord[ mColumnOrdinal.PercentComplete ] = iStructuredLog.PercentComplete;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single STUCTUREDLOG table record
      /// </summary>
      /// <param name="iStructuredLog">Updated Structured Log object</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( UploadStructuredLogRecord iStructuredLog,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // update the fields
            ceResultSet.SetInt32( mColumnOrdinal.RouteId, iStructuredLog.RouteId );

            if ( iStructuredLog.StartTime.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.StartTime, iStructuredLog.StartTime );

            if ( iStructuredLog.EndTime.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.EndTime, iStructuredLog.EndTime );

            ceResultSet.SetInt32( mColumnOrdinal.Duration, iStructuredLog.Duration );
            ceResultSet.SetInt32( mColumnOrdinal.OperId, iStructuredLog.OperId );
            ceResultSet.SetDouble( mColumnOrdinal.PercentComplete, iStructuredLog.PercentComplete );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated STUCTUREDLOG Object with data from a single 
      /// STUCTUREDLOG table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iStructuredLog">STUCTUREDLOG object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
             ref UploadStructuredLogRecord iStructuredLog )
      {
         try
         {
            iStructuredLog.RouteId = ceResultSet.GetInt32( mColumnOrdinal.RouteId );

            if ( ceResultSet[ mColumnOrdinal.StartTime ] != DBNull.Value )
               iStructuredLog.StartTime = ceResultSet.GetDateTime( mColumnOrdinal.StartTime );

            if ( ceResultSet[ mColumnOrdinal.EndTime ] != DBNull.Value )
               iStructuredLog.EndTime = ceResultSet.GetDateTime( mColumnOrdinal.EndTime );

            iStructuredLog.Duration = ceResultSet.GetInt32( mColumnOrdinal.Duration );
            iStructuredLog.PercentComplete = ceResultSet.GetDouble( mColumnOrdinal.PercentComplete );
         }
         catch
         {
            iStructuredLog = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------