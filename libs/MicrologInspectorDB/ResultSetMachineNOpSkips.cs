﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 to 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the MACHINENOPSKIPS Table ResultSet
   /// </summary>
   public class ResultSetMachineNOpSkips : ResultSetBase
   {
      //----------------------------------------------------------------------

      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsMachineSkips mColumnOrdinal = null;

      /// <summary>
      /// Query MACHINENOPSKIPS table based on the Operator ID
      /// </summary>
      private const string QUERY_BY_OPERATOR_ID = "SELECT * FROM MACHINENOPSKIPS " +
          "WHERE OperId = @OperId";

      #endregion

      //----------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_MACHINENOPSKIPS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current MACHINENOPSKIPS table. 
      /// </summary>
      /// <param name="iMachineOkSkipsObject">populated MachineOkSkipsRecord object</param>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( MachineSkips iMachineOkSkipsObject, int iOperId )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MACHINENOPSKIPS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update table contents
               result = WriteRecord( iMachineOkSkipsObject, iOperId, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the MACHINENOPSKIPS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( OperatorSet iOperSettings )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MACHINENOPSKIPS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               int operId;

               // save all records directly to the MACHINENOPSKIPS table
               for ( int i = 0; i < iOperSettings.OperatorSettings.Count; ++i )
               {
                  // get the operator Id associated with the setting record
                  operId = iOperSettings.OperatorSettings.SettingsData[ i ].OperatorId;

                  // populate the table record
                  result = result & WriteRecord(
                              iOperSettings.OperatorSettings.SettingsData[ i ].Settings.MachineNotOperatingSkips,
                              operId, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close connection
         CloseCeConnection();

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the MACHINENOPSKIPS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( EnumMachineSkips iMachineSkips )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MACHINENOPSKIPS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // add objects to table
               foreach ( MachineSkipRecord skip in iMachineSkips )
               {
                  result = result & WriteRecord( skip, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close connection
         CloseCeConnection();

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Given the current Operator ID return a single Machine OK Skips record
      /// </summary>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>Populated MachineSkip object</returns>
      public MachineSkips GetRecord( int OperId )
      {
         OpenCeConnection();

         // reset the default profile name
         MachineSkips derivedPoint = new MachineSkips();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Int ).Value = OperId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MACHINENOPSKIPS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref derivedPoint );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return derivedPoint;

      }/* end method */


      /// <summary>
      /// Resturn a list of all operator records from the MACHINENOPSKIPS table 
      /// </summary>
      /// <returns>Enumerated list of raw MACHINENOPSKIPS table records</returns>
      public EnumMachineSkips GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumMachineSkips returnRecords = new EnumMachineSkips();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MACHINENOPSKIPS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MACHINENOPSKIPS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MachineSkipRecord messageSkip = new MachineSkipRecord();
                  PopulateObject( ceResultSet, ref messageSkip );
                  returnRecords.AddRecord( messageSkip );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return returnRecords;

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// MACHINENOPSKIPS table. This table will need to be revised in the event of any 
      /// changes being made to the MACHINENOPSKIPS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsMachineSkips( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a new MACHINENOPSKIPS record
      /// </summary>
      /// <param name="iSkips">Populated Machine Skip object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( MachineSkips iSkips, int iOperId,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iOperId;
            newRecord[ mColumnOrdinal.MCD ] = iSkips.MCD;
            newRecord[ mColumnOrdinal.Process ] = iSkips.Process;
            newRecord[ mColumnOrdinal.Inspection ] = iSkips.Inspection;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Construct and populate a new MACHINENOPSKIPS record
      /// </summary>
      /// <param name="iSkips">Populated Machine Skip object</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( MachineSkipRecord iSkips, SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iSkips.OperId;
            newRecord[ mColumnOrdinal.MCD ] = iSkips.MCD;
            newRecord[ mColumnOrdinal.Process ] = iSkips.Process;
            newRecord[ mColumnOrdinal.Inspection ] = iSkips.Inspection;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated MACHINENOPSKIPS Object with data from a single 
      /// MACHINENOPSKIPS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMachineNopSkipsRecord">MACHINENOPSKIPS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref MachineSkips iMachineNopSkips )
      {
         try
         {
            iMachineNopSkips.Process = ceResultSet.GetBoolean( mColumnOrdinal.Process );
            iMachineNopSkips.MCD = ceResultSet.GetBoolean( mColumnOrdinal.MCD );
            iMachineNopSkips.Inspection = ceResultSet.GetBoolean( mColumnOrdinal.Inspection );

         }
         catch
         {
            iMachineNopSkips = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated MACHINENOPSKIPS Object with data from a single 
      /// MACHINENOPSKIPS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMachineNopSkipsRecord">MACHINENOPSKIPS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref MachineSkipRecord iMachineNopSkips )
      {
         try
         {
            iMachineNopSkips.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iMachineNopSkips.Process = ceResultSet.GetBoolean( mColumnOrdinal.Process );
            iMachineNopSkips.MCD = ceResultSet.GetBoolean( mColumnOrdinal.MCD );
            iMachineNopSkips.Inspection = ceResultSet.GetBoolean( mColumnOrdinal.Inspection );
         }
         catch
         {
            iMachineNopSkips = null;
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------