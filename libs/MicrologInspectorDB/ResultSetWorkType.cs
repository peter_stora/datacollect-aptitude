﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the WORKTYPE Table ResultSet
   /// </summary>
   public class ResultSetWorkType : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsWorkType mColumnOrdinal = null;

      /// <summary>
      /// Query WORKTYPE table based on the WorkType key (single result)
      /// </summary>
      private const string QUERY_BY_WORKTYPE_KEY = "SELECT * FROM WORKTYPE " +
          "WHERE MAWorkTypeKey = @MAWorkTypeKey";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_WORKTYPE;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current WORKTYPE table. 
      /// </summary>
      /// <param name="iValue">populated Cmms WorkType object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CmmsWorkTypeRecord iValue )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "WORKTYPE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write record to table
               result = WriteRecord( iValue, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the WORKTYPE table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of WorkType objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCmmsWorkTypeRecords iNotifications )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "WORKTYPE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all WORKTYPE records to the table 
               for ( int i = 0; i < iNotifications.Count; ++i )
               {
                  result = result && WriteRecord( iNotifications.WorkType[ i ], ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CMMS WorkType Records
      /// </summary>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsWorkTypes GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCmmsWorkTypes correctiveActions = new EnumCmmsWorkTypes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "WORKTYPE";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the WORKTYPE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsWorkTypeRecord correctiveAction = new CmmsWorkTypeRecord();
                  PopulateObject( ceResultSet, ref correctiveAction );
                  correctiveActions.AddWorkTypeRecord( correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveActions;

      }/* end method */


      /// <summary>
      /// Returns a single WorkType Record object from the WORKTYPE
      /// table using the WorkType Key as the lookup reference. 
      /// </summary>
      /// <param name="iWorkTypeKey">CMMMS WorkType Key value</param>
      /// <returns>Populated WorkType object</returns>
      public CmmsWorkTypeRecord GetRecord( Int32 iWorkTypeKey )
      {
         OpenCeConnection();

         // reset the default profile name
         CmmsWorkTypeRecord correctiveAction = new CmmsWorkTypeRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_WORKTYPE_KEY;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@MAWorkTypeKey", SqlDbType.Int ).Value = iWorkTypeKey;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the WORKTYPE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveAction;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// WORKTYPE table. This table will need to be revised in the event of any 
      /// changes being made to the WORKTYPE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsWorkType( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new WorkNotification record and add it 
      /// to the current WORKTYPE table.
      /// </summary>
      /// <param name="iValue">Populated Cmms WorkType object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CmmsWorkTypeRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.MAWorkTypeKey ] = iValue.MAWorkTypeKey;

            if ( iValue.WorkTypeText != null )
               newRecord[ mColumnOrdinal.WorkTypeText ] = iValue.WorkTypeText;
            else
               newRecord[ mColumnOrdinal.WorkTypeText ] = string.Empty;

            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single WORKTYPE table record
      /// </summary>
      /// <param name="iNotification">Updated CmmsWorkType object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CmmsWorkTypeRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.MAWorkTypeKey, iValue.MAWorkTypeKey );

            if ( iValue.WorkTypeText != null )
               ceResultSet.SetString( mColumnOrdinal.WorkTypeText, iValue.WorkTypeText );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotification Object with data from a single 
      /// WORKTYPE table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iValue">CmmsWorkType object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CmmsWorkTypeRecord iValue )
      {
         try
         {
            iValue.MAWorkTypeKey = ceResultSet.GetInt32( mColumnOrdinal.MAWorkTypeKey );

            if ( ceResultSet[ mColumnOrdinal.WorkTypeText ] != DBNull.Value )
               iValue.WorkTypeText = ceResultSet.GetString( mColumnOrdinal.WorkTypeText );
         }
         catch
         {
            iValue = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------
