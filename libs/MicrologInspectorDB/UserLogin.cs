﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;
using System.Collections;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Class to manage all operator settings and user authentication
   /// </summary>
   public class UserLogin
   {
      // reference to Table ResultSet object
      private ITableConnect mTable = null;
      private OperatorSet mOperators = null;


      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iTable"></param>
      public UserLogin( ITableConnect iTable )
      {
         mTable = iTable;
         mOperators = mTable.Operators.GetOperatorSet();

      }/* end constructor */


      /// <summary>
      /// Get the number of operators available
      /// </summary>
      public int Count
      {
         get
         {
            return mOperators.OperatorNames.Count;
         }
      }


      /// <summary>
      /// Reload the operator settings
      /// </summary>
      private void RefreshOperatorSettings()
      {
         FlushCurrentOperators();
         mOperators = mTable.Operators.GetOperatorSet();

      }/* end method */


      /// <summary>
      /// Reference all current operator settings to null. These can be
      /// collected by the garbage collector later - no rush.
      /// </summary>
      private void FlushCurrentOperators()
      {
         for ( int i=0; i < mOperators.OperatorNames.Count; ++i )
         {
            mOperators.OperatorNames.NameData[ i ] = null;
         }
         for ( int i=0; i < mOperators.OperatorSettings.Count; ++i )
         {
            mOperators.OperatorSettings.SettingsData[ i ] = null;
         }

         mOperators.OperatorNames.NameData.Clear();
         mOperators.OperatorSettings.SettingsData.Clear();

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of operators
      /// </summary>
      /// <returns>Enumerated list of operator logins</returns>
      public EnumLogins GetOperators()
      {
         // refresh the operator settings data
         RefreshOperatorSettings();

         var logins = new EnumLogins();
         for ( int i = 0; i < Count; ++i )
         {
            // create a new login record
            var login = new LoginRecord();

            // set the operator's name
            login.OperatorName =
                mOperators.OperatorNames.NameData[ i ].Name;

            // set the operator's ID
            login.OperatorId =
                mOperators.OperatorNames.NameData[ i ].OperatorId;

            // set the operator's password
            login.Password =
                mOperators.OperatorSettings.SettingsData[ i ].Settings.Password;

            // set the change password flag
            login.CanChangePassword = 
               mOperators.OperatorSettings.SettingsData[ i ].Settings.CanChangePassword;
            
            // barcode login
            login.BarcodeLogin = mOperators.OperatorSettings.SettingsData[i].Settings.BarcodeLogin;
            login.BarcodePassword = mOperators.OperatorSettings.SettingsData[i].Settings.BarcodePassword;

            logins.AddRecord( login );
         }
         return logins;

      }/* end method */


      /// <summary>
      /// Return the Operator Settings for a particular Operator ID
      /// </summary>
      /// <param name="iOperatorId">Operator ID to query</param>
      /// <returns>Populated Operator Settings object</returns>
      public OperatorSettingXML GetOperator( int iOperatorId )
      {
         OperatorSettingXML operatorSetting = null;
         for ( int i = 0; i < Count; ++i )
         {
            operatorSetting = mOperators.OperatorSettings.SettingsData[ i ];
            if ( operatorSetting.OperatorId == iOperatorId )
            {
               // get the Machine Skips info.  Fix for OTD# 1538
               operatorSetting.Settings.MachineNotOperatingSkips =
                  mTable.MachineNOpSkips.GetRecord( iOperatorId );
               
               operatorSetting.Settings.MachineOkSkips =
                  mTable.MachineOkSkips.GetRecord( iOperatorId );

               // get the Messaging Preferences.  Fix for OTD# 1546
               operatorSetting.Settings.MessagingPreferences =
                  mTable.MessagePreferences.GetRecord( iOperatorId );

               break;
            }
         }
         return operatorSetting;
      }/* end method */


      /// <summary>
      /// Change the current user password and update the OPERATORS record
      /// </summary>
      /// <param name="iOperatorId">Selected operator</param>
      /// <param name="iNewPassword">New password</param>
      public void ChangePassword( int iOperatorId, string iNewPassword )
      {
         OperatorSettingXML operatorSetting = null;
         for ( int i = 0; i < Count; ++i )
         {
            // get the current operator settings object
            operatorSetting = mOperators.OperatorSettings.SettingsData[ i ];

            // is this for the correct operator?
            if ( operatorSetting.OperatorId == iOperatorId )
            {
               // update password and force ResetPassword field to 'SET'
               mTable.Operators.ChangePassword(
                  iOperatorId,
                  iNewPassword );
               
               break;
            }
         }
      }/* end method */


   }/* end class */


   /// <summary>
   /// Simple Login class with support for Operator name and password
   /// </summary>
   public class LoginRecord
   {
      /// <summary>
      /// Operator's name
      /// </summary>
      public string OperatorName { get; set; }

      /// <summary>
      /// Operator's ID
      /// </summary>
      public int OperatorId { get; set; }

      /// <summary>
      /// Operator's password
      /// </summary>
      public string Password { get; set; }

      /// <summary>
      /// Can this operator change their password
      /// </summary>
      public Boolean CanChangePassword { get; set; }

      /// <summary>
      /// Gets or set location tag for logging in
      /// </summary>
      public string BarcodeLogin { get; set; }

      /// <summary>
      /// Gets or sets whether or not a password is needed to be entered after scanning tag
      /// </summary>
      public bool BarcodePassword { get; set; }

   }/* end class */


   /// <summary>
   /// Enumerated list of Operator names and passwords
   /// </summary>
   public class EnumLogins : IEnumerable, IEnumerator
   {
      private List<LoginRecord> mOperators;
      private int mIndex = -1;

      /// <summary>
      /// Constructor
      /// </summary>
      public EnumLogins()
      {
         mOperators = new List<LoginRecord>();
      }/* end constructor */


      /// <summary>
      /// Get or set elements in the raw Operator object list.
      /// This is primarily supplied for LINQ implementations
      /// </summary>
      public List<LoginRecord> Operator
      {
         get
         {
            return mOperators;
         }
         set
         {
            mOperators = value;
         }
      }


      /// <summary>
      /// Definition for IEnumerator.Current Property.
      /// </summary>
      public object Current
      {
         get
         {
            return mOperators[ mIndex ];
         }
      }/* end method */


      /// <summary>
      /// How many operator records are now available
      /// </summary>
      public int Count
      {
         get
         {
            return mOperators.Count;
         }
      }


      /// <summary>
      /// Add a new Operator record to the list
      /// </summary>
      /// <param name="item">populated Operator object</param>
      /// <returns>number Operator objects available</returns>
      public int AddRecord( LoginRecord item )
      {
         mOperators.Add( item );
         return mOperators.Count;
      }


      /// <summary>
      /// Definition for IEnumerator.Reset() method.
      /// </summary>
      public void Reset()
      {
         mIndex = -1;
      }/* end method */


      /// <summary>
      /// Definition for IEnumerator.MoveNext() method.
      /// </summary>
      /// <returns></returns>
      public bool MoveNext()
      {
         return ( ++mIndex < mOperators.Count );
      }/* end method */


      /// <summary>
      /// Definition for IEnumerable.GetEnumerator() method.
      /// </summary>
      /// <returns></returns>
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator) this;
      }/* end method */

   }/* end class */


}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 24th June 2009
//  Add to project
//----------------------------------------------------------------------------


