﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the CONDITIONALPOINT table. This structure must be revised following 
    /// any changes made to the table's field structure.
    /// </summary>
    public class RowOrdinalsConditionalPoint
    {
        public int ID { get; set; }
        public int ConditionalPointNumber { get; set; }
        public int ReferencePointNumber { get; set; }
        public int CriteriaType { get; set; }
        public int MinRange { get; set; }
        public int MaxRange { get; set; }
        public int ResultIdx { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsConditionalPoint(SqlCeResultSet iCeResultSet)
        {
            GetConditionalPointTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// CONDITIONALPOINT table. This method will need to be revised in the event of 
        /// any changes being made to the CONDITIONALPOINT table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetConditionalPointTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.ID = iCeResultSet.GetOrdinal("ID");
            this.ConditionalPointNumber = iCeResultSet.GetOrdinal("ConditionalPointNumber");
            this.ReferencePointNumber = iCeResultSet.GetOrdinal("ReferencePointNumber");
            this.CriteriaType = iCeResultSet.GetOrdinal("CriteriaType");
            this.MinRange = iCeResultSet.GetOrdinal("MinRange");
            this.MaxRange = iCeResultSet.GetOrdinal("MaxRange");
            this.ResultIdx = iCeResultSet.GetOrdinal("ResultIdx");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------