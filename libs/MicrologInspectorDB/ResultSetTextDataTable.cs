﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the TEXTDATA Table ResultSet
   /// </summary>
   public class ResultSetTextDataTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsTextData mColumnOrdinal = null;

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM TEXTDATA " +
          "WHERE PointUid = @PointUid";

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_ENUM_BY_POINT_UID = "SELECT * FROM TEXTDATA " +
          "WHERE ";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_TEXTDATA;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single TextDataRecord object to the TEXTDATA table. 
      /// </summary>
      /// <param name="iTextDataRecord">Populated TextDataRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( TextDataRecord iTextDataRecord )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "TEXTDATA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the TEXTDATA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               if ( !WriteRecord( iTextDataRecord, ceResultSet ) )
               {
                  result = false;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of TextDataRecord objects to the TEXTDATA table
      /// and also update the progress bar updates object
      /// </summary>
      /// <param name="iEnumTextData">Enumerated List of TextDataRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumTextData iEnumTextData, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the process objects list
         iEnumTextData.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "TEXTDATA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the TEXTDATA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( TextDataRecord process in iEnumTextData )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( process, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of TextDataRecord objects to the TEXTDATA table. 
      /// </summary>
      /// <param name="iEnumTextData">Enumerated List of TextDataRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumTextData iEnumTextData )
      {
         OpenCeConnection();

         // reset the process objects list
         iEnumTextData.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "TEXTDATA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the TEXTDATA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( TextDataRecord process in iEnumTextData )
               {
                  if ( !WriteRecord( process, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Populate and return a single TextDataRecord object given the associated
      /// Point Uid.
      /// </summary>
      /// <param name="iPointUid">Point Uid guid</param>
      /// <returns>Populated TextDataRecord object</returns>
      public TextDataRecord GetRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // create the default object
         TextDataRecord process = new TextDataRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;
            command.CommandType = CommandType.Text;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the TEXTDATA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // populate the process object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref process );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return process;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the TEXTDATA table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public EnumTextData GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumTextData processRecords = new EnumTextData();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "TEXTDATA";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the TEXTDATA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  TextDataRecord process = new TextDataRecord();
                  PopulateObject( ceResultSet, ref process );
                  if ( process != null )
                     processRecords.AddTextDataRecord( process );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return processRecords;

      }/* end method */


      /// <summary>
      /// Updates a TEXTDATA table record with the data from its associated
      /// business object (a single ProcesaRecord object). As all fields are
      /// replaced, this method should be used with caution! 
      /// </summary>
      /// <param name="iPointRecord">Populated TextDataRecord object</param>
      public void UpdateRecord( TextDataRecord iTextDataRecord )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iTextDataRecord.PointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  UpdateRecord( iTextDataRecord, ceResultSet );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Return an enumerated list of populated TextData objects given an
      /// array of Parent Uid (guids).
      /// </summary>
      /// <param name="iPointUids">Array of Point Uid guids</param>
      /// <returns>Enumerated list of populated TextData objects - or null on failure</returns>
      public EnumTextData GetEnumRecords( Guid[] iPointUids )
      {
         OpenCeConnection();

         // create new enumerated list of TextData objects
         EnumTextData enumTextData = new EnumTextData();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( Guid pointUid in iPointUids )
            {
               // if this is not the last parameter then add an additional OR clause
               if ( paramCount < iPointUids.Length )
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
               }
               // otherwise terminate query with semi-colon
               else
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() +
                      " ORDER BY Number;" );
               }
               command.Parameters.Add( "@P" + paramCount.ToString(), SqlDbType.UniqueIdentifier ).Value = pointUid;

               // increment the parameter count
               ++paramCount;
            }

            // referebce the full command text
            command.CommandText = query.ToString();

            try
            {
               // execure the result set
               using ( SqlCeResultSet ceResultSet = 
                  command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the TEXTDATA table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // convert TextData record to a TextDataRecord object
                  while ( ceResultSet.Read() )
                  {
                     TextDataRecord process = new TextDataRecord();
                     PopulateObject( ceResultSet, ref process );
                     enumTextData.AddTextDataRecord( process );
                  }
                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               enumTextData = null;
            }
         }
         CloseCeConnection();
         return enumTextData;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of populated TextData objects given an 
      /// enumerated list of populated Point objects. Note: This method does 
      /// not rely on the base class properties and so can be called directly 
      /// from a single instantiated ResultSet object.
      /// </summary>
      /// <param name="iPointUids">Enumerated list of Point objects</param>
      /// <returns>Enumerated list of populated TextData objects - or null on failure</returns>
      public EnumTextData GetEnumRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // create new enumerated list of TextData objects
         EnumTextData enumTextData = new EnumTextData();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( PointRecord point in iEnumPoints )
            {
               // ensure we are dealing with a process type
               if ( IsPointTextDataType( point ) )
               {
                  // if this is not the last parameter then add an additional OR clause
                  if ( paramCount < iEnumPoints.Count )
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
                  }
                  // otherwise terminate query with semi-colon
                  else
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() +
                         " ORDER BY Number;" );
                  }

                  // add the new parameter object
                  command.Parameters.Add( "@P" + paramCount.ToString(),
                      SqlDbType.UniqueIdentifier ).Value = point.Uid;

                  // increment the parameter count
                  ++paramCount;
               }
            }

            // if no process points found then just return null!
            if ( paramCount > 1 )
            {
               // referebce the full command text
               command.CommandText = query.ToString();

               try
               {
                  // execure the result set
                  using ( SqlCeResultSet ceResultSet = 
                     command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated with 
                     // the TEXTDATA table. As this is a 'one shot' the column ordinal 
                     // object remains a singleton
                     //
                     GetTableColumns( ceResultSet );

                     // convert TextData record to a TextDataRecord object
                     while ( ceResultSet.Read() )
                     {
                        TextDataRecord process = new TextDataRecord();
                        PopulateObject( ceResultSet, ref process );
                        enumTextData.AddTextDataRecord( process );
                     }

                     // close the resultSet
                     ceResultSet.Close();
                  }
               }
               catch
               {
                  enumTextData = null;
               }
            }
         }
         CloseCeConnection();
         return enumTextData;

      }/* end method */

      #endregion


      #region Private Methods

      /// <summary>
      /// Returns true if the PointRecord object is a TextData Type
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <returns>True if type is TextData</returns>
      private Boolean IsPointTextDataType( PointRecord iPointRecord )
      {
         FieldTypes.ProcessType prType = (FieldTypes.ProcessType) iPointRecord.ProcessType;

         if ( prType == FieldTypes.ProcessType.TextData )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// This method populates and returns the column ordinals associated with 
      /// the TEXTDATA table. This table will need to be revised in the event of 
      /// any changes being made to the TEXTDATA table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsTextData( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new TEXTDATA record and add it to 
      /// the current TEXTDATA table.
      /// </summary>
      /// <param name="iTextDataObject">Populated TextData Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( TextDataRecord iTextDataObject,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iTextDataObject.Uid;
            newRecord[ mColumnOrdinal.Number ] = iTextDataObject.Number;
            newRecord[ mColumnOrdinal.PointUid ] = iTextDataObject.PointUid;            
            newRecord[ mColumnOrdinal.MaxLength ] = iTextDataObject.MaxLength;
            newRecord[ mColumnOrdinal.AllowSpaces ] = iTextDataObject.AllowSpaces;
            newRecord[ mColumnOrdinal.AllowSpecial ] = iTextDataObject.AllowSpecial;
            newRecord[ mColumnOrdinal.UpperCase ] = iTextDataObject.UpperCase;
            newRecord[ mColumnOrdinal.ScannerInput ] = iTextDataObject.ScannerInput;
            newRecord[ mColumnOrdinal.TextType ] = iTextDataObject.TextType;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single TEXTDATA table record
      /// </summary>
      /// <param name="iTextDataRecord">Updated TextDataRecord object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( TextDataRecord iTextDataRecord,
          SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iTextDataRecord.Uid );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iTextDataRecord.Number );
            CeResultSet.SetGuid( mColumnOrdinal.PointUid, iTextDataRecord.PointUid );
            CeResultSet.SetByte( mColumnOrdinal.MaxLength, iTextDataRecord.MaxLength );
            CeResultSet.SetBoolean( mColumnOrdinal.AllowSpaces, iTextDataRecord.AllowSpaces );
            CeResultSet.SetBoolean( mColumnOrdinal.AllowSpecial, iTextDataRecord.AllowSpecial );
            CeResultSet.SetBoolean( mColumnOrdinal.UpperCase, iTextDataRecord.UpperCase );
            CeResultSet.SetBoolean( mColumnOrdinal.ScannerInput, iTextDataRecord.ScannerInput );
            CeResultSet.SetByte( mColumnOrdinal.TextType, iTextDataRecord.TextType );

            // update the record
            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated TextDataRecord Object with data from a single 
      /// TEXTDATA table record. In the event of a raised exception the returned 
      /// TextData object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iTextDataRecord">TextData object to be populated</param>
      private void PopulateObject( SqlCeResultSet CeResultSet,
          ref TextDataRecord iTextDataRecord )
      {
         try
         {
            iTextDataRecord.Uid = CeResultSet.GetGuid( mColumnOrdinal.Uid );
            iTextDataRecord.Number = CeResultSet.GetInt32( mColumnOrdinal.Number );
            iTextDataRecord.PointUid = CeResultSet.GetGuid( mColumnOrdinal.PointUid );
            iTextDataRecord.MaxLength = CeResultSet.GetByte( mColumnOrdinal.MaxLength );
            iTextDataRecord.TextType = CeResultSet.GetByte( mColumnOrdinal.TextType );

            if ( CeResultSet[ mColumnOrdinal.AllowSpaces ] != DBNull.Value )
               iTextDataRecord.AllowSpaces = CeResultSet.GetBoolean( mColumnOrdinal.AllowSpaces );

            if ( CeResultSet[ mColumnOrdinal.AllowSpecial ] != DBNull.Value )
               iTextDataRecord.AllowSpecial = CeResultSet.GetBoolean( mColumnOrdinal.AllowSpecial );

            if ( CeResultSet[ mColumnOrdinal.UpperCase ] != DBNull.Value )
               iTextDataRecord.UpperCase = CeResultSet.GetBoolean( mColumnOrdinal.UpperCase );

            if ( CeResultSet[mColumnOrdinal.ScannerInput ] != DBNull.Value )
               iTextDataRecord.ScannerInput = CeResultSet.GetBoolean(mColumnOrdinal.ScannerInput);
         }
         catch
         {
            iTextDataRecord = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  Revision 0.0 APinkerton 11th September 2012
//  Add to project
//----------------------------------------------------------------------------
