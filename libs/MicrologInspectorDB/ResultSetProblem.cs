﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the PROBLEM Table ResultSet
   /// </summary>
   public class ResultSetProblem : ResultSetBase
   {
      #region Private Fields and Constants


      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsProblem mColumnOrdinal = null;

      /// <summary>
      /// Query PROBLEM table based on the Problem key (single result)
      /// </summary>
      private const string QUERY_BY_PROBLEM_KEY = "SELECT * FROM PROBLEM " +
          "WHERE MAProbDescKey = @MAProbDescKey";


      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_PROBLEM;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current PROBLEM table. 
      /// </summary>
      /// <param name="iValue">populated Cmms Problem object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CmmsProblemRecord iValue )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROBLEM";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write record to table
               result = WriteRecord( iValue, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the PROBLEM table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of Problem objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCmmsProblemRecords iNotifications )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROBLEM";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all PROBLEM records to the table 
               for ( int i = 0; i < iNotifications.Count; ++i )
               {
                  result = result && WriteRecord( iNotifications.Problem[ i ], ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CMMS Problem Records
      /// </summary>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsProblems GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCmmsProblems correctiveActions = new EnumCmmsProblems();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROBLEM";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROBLEM table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsProblemRecord correctiveAction = new CmmsProblemRecord();
                  PopulateObject( ceResultSet, ref correctiveAction );
                  correctiveActions.AddProblemRecord( correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveActions;

      }/* end method */


      /// <summary>
      /// Returns a single Problem Record object from the PROBLEM
      /// table using the Problem Key as the lookup reference. 
      /// </summary>
      /// <param name="iProblemKey">CMMMS Problem Key value</param>
      /// <returns>Populated Problem object</returns>
      public CmmsProblemRecord GetRecord( Int32 iProblemKey )
      {
         OpenCeConnection();

         // reset the default profile name
         CmmsProblemRecord correctiveAction = new CmmsProblemRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_PROBLEM_KEY;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@MAProbDescKey", SqlDbType.Int ).Value = iProblemKey;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROBLEM table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveAction;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// PROBLEM table. This table will need to be revised in the event of any 
      /// changes being made to the PROBLEM table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsProblem( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new WorkNotification record and add it 
      /// to the current PROBLEM table.
      /// </summary>
      /// <param name="iValue">Populated Cmms Problem object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CmmsProblemRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.MAProbDescKey ] = iValue.MAProblemDescriptionKey;

            if ( iValue.ProblemText != null )
               newRecord[ mColumnOrdinal.ProbText ] = iValue.ProblemText;
            else
               newRecord[ mColumnOrdinal.ProbText ] = string.Empty;

            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single PROBLEM table record
      /// </summary>
      /// <param name="iNotification">Updated CmmsProblem object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CmmsProblemRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.MAProbDescKey, iValue.MAProblemDescriptionKey );

            if ( iValue.ProblemText != null )
               ceResultSet.SetString( mColumnOrdinal.ProbText, iValue.ProblemText );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotification Object with data from a single 
      /// PROBLEM table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iValue">CmmsProblem object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CmmsProblemRecord iValue )
      {
         try
         {
            iValue.MAProblemDescriptionKey = ceResultSet.GetInt32( mColumnOrdinal.MAProbDescKey );

            if ( ceResultSet[ mColumnOrdinal.ProbText ] != DBNull.Value )
               iValue.ProblemText = ceResultSet.GetString( mColumnOrdinal.ProbText );
         }
         catch
         {
            iValue = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------