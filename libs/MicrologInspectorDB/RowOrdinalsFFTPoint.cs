﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the FFTPoint table. This structure should be revised following any
    /// changes made to this table.
    /// </summary>
    public class RowOrdinalsFFTPoint
    {
        public int Uid { get; set; }
        public int Number { get; set; }
        public int PointUid { get; set; }
        public int TakeData { get; set; }
        public int UseConfiguration { get; set; }
        public int DefaultRunningSpeed { get; set; }
        public int EnvType { get; set; }
        public int EnvDetection { get; set; }
        public int EnvNumberOfLines { get; set; }
        public int EnvAverages { get; set; }
        public int EnvWindow { get; set; }
        public int EnvFreqMax { get; set; }
        public int VelType { get; set; }
        public int VelDetection { get; set; }
        public int VelNumberOfLines { get; set; }
        public int VelAverages { get; set; }
        public int VelWindow { get; set; }
        public int VelFreqMax { get; set; }
        public int AcclType { get; set; }
        public int AcclDetection { get; set; }
        public int AcclNumberOfLines { get; set; }
        public int AcclAverages { get; set; }
        public int AcclWindow { get; set; }
        public int AcclFreqMax { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsFFTPoint(SqlCeResultSet iCeResultSet)
        {
            GetFFTPointTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// FFTPoint table. This table will need to be revised in the event of any changes
        /// being made to the FFTPoint table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetFFTPointTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.Number = iCeResultSet.GetOrdinal("Number");
            this.PointUid = iCeResultSet.GetOrdinal("PointUid");
            this.TakeData = iCeResultSet.GetOrdinal("TakeData");
            this.UseConfiguration = iCeResultSet.GetOrdinal("UseConfiguration");
            this.DefaultRunningSpeed = iCeResultSet.GetOrdinal("DefaultRunningSpeed");
            this.EnvType = iCeResultSet.GetOrdinal("EnvType");
            this.EnvDetection = iCeResultSet.GetOrdinal("EnvDetection");
            this.EnvNumberOfLines = iCeResultSet.GetOrdinal("EnvNumberOfLines");
            this.EnvAverages = iCeResultSet.GetOrdinal("EnvAverages");
            this.EnvWindow = iCeResultSet.GetOrdinal("EnvWindow");
            this.EnvFreqMax = iCeResultSet.GetOrdinal("EnvFreqMax");
            this.VelType = iCeResultSet.GetOrdinal("VelType");
            this.VelDetection = iCeResultSet.GetOrdinal("VelDetection");
            this.VelNumberOfLines = iCeResultSet.GetOrdinal("VelNumberOfLines");
            this.VelAverages = iCeResultSet.GetOrdinal("VelAverages");
            this.VelWindow = iCeResultSet.GetOrdinal("VelWindow");
            this.VelFreqMax = iCeResultSet.GetOrdinal("VelFreqMax");
            this.AcclType = iCeResultSet.GetOrdinal("AcclType");
            this.AcclDetection = iCeResultSet.GetOrdinal("AcclDetection");
            this.AcclNumberOfLines = iCeResultSet.GetOrdinal("AcclNumberOfLines");
            this.AcclAverages = iCeResultSet.GetOrdinal("AcclAverages");
            this.AcclWindow = iCeResultSet.GetOrdinal("AcclWindow");
            this.AcclFreqMax = iCeResultSet.GetOrdinal("AcclFreqMax");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------