﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the USERPREFERENCES Table ResultSet. 
   /// 
   /// Note 1: 
   /// 
   /// Unlike most other ResultSet object, the contents of this
   /// table are not cleared on each synchronization. Rather missing
   /// preferences are added, orphaned preferences deleted and older
   /// preference objects overwriten with newer ones. 
   /// An additional "Changed" flag ensures that values just set by
   /// the operator are not replaced new downloaded value.
   /// 
   /// Note2:
   /// 
   /// This class depends on: 
   ///   UserPreferencesObject.cs, 
   ///   UserPreferencesStruct.cs,
   ///   EnumUserPreferenceRecords.cs
   /// 
   /// </summary>
   public class ResultSetUserPreferences : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsUserPreferences mColumnOrdinal = null;

      /// <summary>
      /// Query USERPREFERENCES table based on the Operator ID
      /// </summary>
      private const string QUERY_BY_OPERATOR_ID = "SELECT * FROM USERPREFERENCES " +
          "WHERE OperId = @OperId";

      /// <summary>
      /// Query USERPREFERENCES table based on the password being changed
      /// </summary>
      private const string QUERY_BY_CHANGED_SETTINGS = "SELECT * FROM USERPREFERENCES " +
          "WHERE Changed = @Changed";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_USERPREFERENCES;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current USERPREFERENCES table. 
      /// </summary>
      /// <param name="iLOCALSETTINGSObject">populated UserPreferences object</param>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( UserPreferencesRecord iPreferences )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Int ).Value =
               iPreferences.OperatorId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
               ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               if ( ceResultSet.HasRows )
               {
                  if ( ceResultSet.ReadFirst() )
                  {
                     // create and populate a new preferences record
                     UserPreferencesRecord upr = new UserPreferencesRecord();
                     PopulateObject( ceResultSet, ref upr );

                     // get the preferences object
                     UserPreferencesObject upOld = upr.GetPreferences();

                     // update new preferences object with the new version
                     UserPreferencesObject upNew = iPreferences.GetPreferences();
                     upNew.Version = ++upOld.Version;
                     iPreferences.SetPreferences( upNew );

                     // update this record
                     UpdateRecord( iPreferences, ceResultSet );
                  }
               }
               else
               {
                  result = WriteRecord( iPreferences, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Get an enumerated list of all preference records
      /// </summary>
      /// <returns>populated preferences list or null</returns>
      public EnumUserPreferences GetAllRecords()
      {
         OpenCeConnection();

         // create new preferences list object
         EnumUserPreferences settingRecords = new EnumUserPreferences();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "USERPREFERENCES";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the USERPREFERENCES table. As this is a 'one shot' the column 
               // ordinal object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  UserPreferencesRecord settings = new UserPreferencesRecord();
                  PopulateObject( ceResultSet, ref settings );
                  if ( settings != null )
                  {
                     settingRecords.AddRecord( settings );
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return settingRecords;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the USERPREFERENCES table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( EnumUserPreferences iSettings )
      {
         // open the current connection
         OpenCeConnection();

         // set the default result to true - update the device profile
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "USERPREFERENCES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( UserPreferencesRecord settings in iSettings )
               {
                  result = result & WriteRecord( settings, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close the table connection
         CloseCeConnection();

         // return the method result
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the USERPREFERENCES table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( OperatorSet iOperSettings )
      {
         // get a list of all records currently in the settings table
         EnumUserPreferences currentSettings = GetAllRecords();

         // get a list of all preference settings from the operators download
         EnumUserPreferences downloadSettings = GetDownloadSettings( iOperSettings );

         // open the current connection
         OpenCeConnection();

         // set the default result to true - update the device profile
         Boolean result = true;

         // list of all local settings to be written back to the database
         EnumUserPreferences revisedSettings = new EnumUserPreferences();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "USERPREFERENCES";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               //
               // if no result returned then add all new settings records, 
               // otherwise compare each record with with the new settings
               // and either add, update or delete as required
               //
               if ( !ceResultSet.HasRows )
               {
                  // save all received download settings records
                  result = SaveAllDownloadSettings( downloadSettings, ceResultSet );
               }
               //
               // if however the preferences table already contains some 
               // records, then process each download relative to each
               // record already present.
               //
               else
               {
                  // are there any new downloads to add to the table
                  foreach ( UserPreferencesRecord downloadSetting in downloadSettings )
                  {
                     //
                     // if an operator from the latest download is not in the current
                     // settings table, then add this operator's settings to the
                     // revised settings list
                     //
                     if ( !OperatorFound( currentSettings, downloadSetting.OperatorId ) )
                     {
                        revisedSettings.AddRecord( downloadSetting );
                     }
                  }

                  // reset the list pointers
                  currentSettings.Reset();
                  downloadSettings.Reset();

                  //
                  // now add those current records whose operator IDs match with those
                  // operator IDs in the download list
                  //
                  foreach ( UserPreferencesRecord currentRecord in currentSettings )
                  {
                     if ( OperatorFound( downloadSettings, currentRecord.OperatorId ) )
                     {
                        //
                        // if the current operator record has the flag set "true",
                        // then we don't want to overwrite this record with the newly
                        // downloaded copy
                        //
                        if ( currentRecord.Changed == true )
                        {
                           currentRecord.Changed = false;
                           revisedSettings.AddRecord( currentRecord );
                        }
                        // otherwise - for unchanged current settings
                        else
                        {
                           // get the the download record for the current operator ID 
                           UserPreferencesRecord downloadRecord =
                                   GetSettingsRecord( downloadSettings, currentRecord.OperatorId );

                           // assuming a record could be found in the download settings
                           if ( downloadRecord != null )
                           {
                              if ( downloadRecord.GetPreferences().Version >
                                 currentRecord.GetPreferences().Version )
                              {
                                 revisedSettings.AddRecord( downloadRecord );
                              }
                              else
                              {
                                 revisedSettings.AddRecord( currentRecord );
                              }
                           }
                        }
                     }
                     else
                     {
                        //
                        // if an an operator is in the preferences table that is
                        // not in the download prerences, BUT the changed flag is
                        // set true, then assume that this is a newly added blob
                        //
                        if ( currentRecord.Changed == true )
                        {
                           currentRecord.Changed = false;
                           revisedSettings.AddRecord( currentRecord );
                        }
                     }
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close the table connection
         CloseCeConnection();

         //
         // if there any entries in the revisions list then we need to
         // clear out all records in the preferences table and overwrite
         // with the revision contents
         //
         if ( revisedSettings.Count > 0 )
         {
            ClearTable();
            revisedSettings.Reset();
            result = AddEnumRecords( revisedSettings );
         }

         // return the method result
         return result;

      }/* end method */


      /// <summary>
      /// Given the current Operator ID return a single Machine OK Skips record
      /// </summary>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>Populated UserPreferences object</returns>
      public UserPreferencesRecord GetRecord( int OperId )
      {
         OpenCeConnection();

         // reset the default profile name
         UserPreferencesRecord localSettings = new UserPreferencesRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Int ).Value = OperId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the USERPREFERENCES table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref localSettings );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return localSettings;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of UserPreference records where the
      /// device operator has changed their local settings.
      /// </summary>
      /// <returns>Populated UserPreferences object</returns>
      public EnumUserPreferences GetChangedPreferences()
      {
         OpenCeConnection();

         // create new preferences list object
         EnumUserPreferences settingRecords = new EnumUserPreferences();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CHANGED_SETTINGS;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Changed", SqlDbType.Bit ).Value = true;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  UserPreferencesRecord settings = new UserPreferencesRecord();
                  PopulateObject( ceResultSet, ref settings );
                  if ( settings != null )
                  {
                     settingRecords.AddRecord( settings );
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return settingRecords;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// USERPREFERENCES table. This table will need to be revised in the event of any 
      /// changes being made to the USERPREFERENCES table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsUserPreferences( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a new USERPREFERENCES record
      /// </summary>
      /// <param name="iSkips">Populated Machine Skip object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( UserPreferencesRecord iLocalSettings,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iLocalSettings.OperatorId;
            newRecord[ mColumnOrdinal.Changed ] = iLocalSettings.Changed;
            newRecord[ mColumnOrdinal.HKCU ] = iLocalSettings.BinaryPreferencesBlob;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single USERPREFERENCES table record
      /// </summary>
      /// <param name="iFFTChannelRecord">Updated Local Settings Record object to write</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( UserPreferencesRecord iLocalSettings,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetBoolean( mColumnOrdinal.Changed, iLocalSettings.Changed );
            ceResultSet.SetBytes(
               mColumnOrdinal.HKCU,
               0,
               iLocalSettings.BinaryPreferencesBlob,
               0,
               UserPreferencesRecord.GetSettingsBytes() );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated UserPreferences Object with data from a single 
      /// USERPREFERENCES table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iLocalSettings">UserPreferences object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
         ref UserPreferencesRecord iLocalSettings )
      {
         try
         {
            // get the change state flag
            iLocalSettings.Changed = ceResultSet.GetBoolean( mColumnOrdinal.Changed );

            // get the operator ID
            iLocalSettings.OperatorId = ceResultSet.GetInt32( mColumnOrdinal.OperId );

            // get the HKCU data BLOB
            if ( ceResultSet[ mColumnOrdinal.HKCU ] != DBNull.Value )
            {
               int byteCount = UserPreferencesRecord.GetSettingsBytes();

               byte[] blob = new byte[ byteCount ];

               // read the Local Users HKCU ByteArray
               ceResultSet.GetBytes(
                  mColumnOrdinal.HKCU,
                  0,
                  blob,
                  0,
                  byteCount );

               iLocalSettings.BinaryPreferencesBlob = blob;

            }
         }
         catch
         {
            iLocalSettings = null;
         }

      }/* end method */

      #endregion


      #region Private Methods for extended AddEnumerated() Method

      /// <summary>
      /// Given an Operator ID, return a download settings object
      /// </summary>
      /// <param name="downloadSettings">List of download settings</param>
      /// <param name="iOperatorId">Reference Operator ID</param>
      /// <returns></returns>
      private UserPreferencesRecord GetSettingsRecord(
         EnumUserPreferences downloadSettings, int iOperatorId )
      {
         // use LINQ to select the setting
         IEnumerable<UserPreferencesRecord> settings =
                        from point in downloadSettings.UserPreferences
                        where ( point.OperatorId == iOperatorId )
                        orderby point.OperatorId ascending
                        select point;

         // hydrate the LINQ return list
         EnumUserPreferences settingsList = new EnumUserPreferences();
         foreach ( UserPreferencesRecord setting in settings )
         {
            if ( setting != null )
               settingsList.AddRecord( setting );
         }

         // and return 
         if ( settingsList.UserPreferences.Count > 0 )
         {
            return settingsList.UserPreferences[ 0 ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Is a given Operator a member of an operator set?
      /// </summary>
      /// <param name="iReferenceSettingsList">Set to reference</param>
      /// <param name="iOperatorId">Operator Id</param>
      /// <returns>true if member of set - else false</returns>
      private Boolean OperatorFound( EnumUserPreferences iReferenceSettingsList, int iOperatorId )
      {
         if ( GetSettingsRecord( iReferenceSettingsList, iOperatorId ) != null )
         {
            return true;
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Extract the new download settings as a list of UserPreferences records
      /// </summary>
      /// <param name="iOperSettings"></param>
      /// <returns></returns>
      private EnumUserPreferences GetDownloadSettings( OperatorSet iOperSettings )
      {
         EnumUserPreferences downloadSettings = new EnumUserPreferences();

         for ( int i = 0; i < iOperSettings.OperatorSettings.Count; ++i )
         {
            // create a new UserPreferences object
            UserPreferencesRecord localSettings = new UserPreferencesRecord();

            // and populate
            localSettings.Changed = false;

            localSettings.OperatorId =
               iOperSettings.OperatorSettings.SettingsData[ i ].OperatorId;

            localSettings.BinaryPreferencesBlob =
               iOperSettings.OperatorSettings.SettingsData[ i ].Settings.UserPreferences;

            // add to the download settings list
            downloadSettings.AddRecord( localSettings );
         }
         return downloadSettings;

      }/* end method */


      /// <summary>
      /// Save an individual downloaded preference
      /// </summary>
      /// <param name="iDownloadSetting"></param>
      /// <param name="ceResultSet"></param>
      /// <returns></returns>
      private Boolean SaveDownloadSettings( UserPreferencesRecord iDownloadSetting,
         SqlCeResultSet ceResultSet )
      {
         // set the default result to true - update the device profile
         Boolean result = true;

         // now populate the table record
         result = result & WriteRecord( iDownloadSetting, ceResultSet );

         // and how did that go?
         return result;

      }/* end method */


      /// <summary>
      /// Saves an enumerated list of download settings
      /// </summary>
      /// <param name="iOperSettings"></param>
      /// <param name="ceResultSet"></param>
      /// <returns></returns>
      private Boolean SaveAllDownloadSettings( EnumUserPreferences downloadSettings,
         SqlCeResultSet ceResultSet )
      {
         // set the default result to true - update the device profile
         Boolean result = true;

         try
         {
            // add each new settings record to the database
            foreach ( UserPreferencesRecord lsr in downloadSettings )
            {
               // now populate the table record
               result = result & WriteRecord(
                           lsr, ceResultSet );
            }
         }
         catch
         {
            result = false;
         }

         // and how did that go?
         return result;

      }/* end method */


      /// <summary>
      /// Get a list of all current operator settings
      /// </summary>
      /// <param name="iOperSettings"></param>
      /// <param name="ceResultSet"></param>
      /// <returns></returns>
      private void GetCurrentSettingRecords( OperatorSet iOperSettings, ref EnumUserPreferences iSettings,
         SqlCeResultSet ceResultSet )
      {
         while ( ceResultSet.Read() )
         {
            UserPreferencesRecord settingsRecord = new UserPreferencesRecord();
            PopulateObject( ceResultSet, ref settingsRecord );
            if ( settingsRecord != null )
            {
               iSettings.AddRecord( settingsRecord );
            }
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 06th March 2010
//  Add to project
//----------------------------------------------------------------------------