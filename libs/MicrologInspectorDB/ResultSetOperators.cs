﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 to 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the OPERATORS Table ResultSet
   /// </summary>
   public class ResultSetOperators : ResultSetBase
   {
      //----------------------------------------------------------------------
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsOperators mColumnOrdinal = null;

      /// <summary>
      /// Query OPERATORS table based on the Operator Name
      /// </summary>
      private const string QUERY_BY_OPERATOR_NAME = "SELECT * FROM OPERATORS " +
          "WHERE OperName = @OperName";

      /// <summary>
      /// Query OPERATORS table based on the Operator Id
      /// </summary>
      private const string QUERY_BY_OPERATOR_ID = "SELECT * FROM OPERATORS " +
          "WHERE OperId = @OperId";

      /// <summary>
      /// Query OPERATORS table based on the password being changed
      /// </summary>
      private const string QUERY_BY_CHANGED_PASSWORD = "SELECT * FROM OPERATORS " +
          "WHERE PasswordChanged = @PasswordChanged";

      #endregion

      //----------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_OPERATORS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current OPERATORS table. 
      /// </summary>
      /// <param name="iPreferences">populated OperatorSettingXML object</param>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( OperatorSettingXML iPreferences, string iOperName )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write the operator record
               result = WriteRecord( iPreferences, iOperName, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Given the current Operator ID return a single Message Preferences object
      /// </summary>
      /// <param name="iOperName">The current Operator Name</param>
      /// <returns>Populated OperatorSetting object</returns>
      public OperatorSettingXML GetRecord( string OperName )
      {
         OpenCeConnection();

         // reset the default profile name
         OperatorSettingXML operators = new OperatorSettingXML();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_NAME;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Text ).Value = OperName;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref operators );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return operators;

      }/* end method */


      /// <summary>
      /// Resturn a list of all operator records from the OPERATORS table 
      /// </summary>
      /// <returns>Enumerated list of raw OPERATORS table records</returns>
      public EnumOperators GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumOperators operatorSettings = new EnumOperators();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  OperatorRecord operatorSetting = new OperatorRecord();
                  PopulateObject( ceResultSet, ref operatorSetting );
                  operatorSettings.AddRecord( operatorSetting );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return operatorSettings;

      }/* end method */


      /// <summary>
      /// Resturn a list of all operator setting records from the 
      /// </summary>
      /// <returns>Enumerated list of XML operator Settings</returns>
      public EnumOperatorXmlSettings GetRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumOperatorXmlSettings operatorSettings = new EnumOperatorXmlSettings();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  OperatorSettingXML operatorSetting = new OperatorSettingXML();
                  PopulateObject( ceResultSet, ref operatorSetting );
                  operatorSettings.AddEnumOperSettings( operatorSetting );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return operatorSettings;

      }/* end method */


      /// <summary>
      /// Return the full OperatorSet which contains not only a list of
      /// all operator names, but also the settings associated with each
      /// operator.
      /// </summary>
      /// <returns>Populated OperatorSet object</returns>
      public OperatorSet GetOperatorSet()
      {
         OpenCeConnection();

         // reset the default profile name
         OperatorSet operatorSet = new OperatorSet();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  OperatorSettingXML operatorSetting = new OperatorSettingXML();
                  OperatorName operatorName = new OperatorName();

                  PopulateObject( ceResultSet, ref operatorSetting );
                  operatorSet.OperatorSettings.AddEnumOperSettings( operatorSetting );

                  PopulateObject( ceResultSet, ref operatorName );
                  operatorSet.OperatorNames.AddEnumOperName( operatorName );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return operatorSet;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of UploadSetting records where the
      /// device operator has changed their password.
      /// </summary>
      /// <returns>Populated UploadSetting object</returns>
      public EnumUploadSettings GetChangedPasswords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumUploadSettings uploadOperatorSettings = new EnumUploadSettings();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CHANGED_PASSWORD;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PasswordChanged", SqlDbType.Bit ).Value = true;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  UploadSettings uploadSettings = new UploadSettings();
                  PopulateObject( ceResultSet, ref uploadSettings );
                  uploadOperatorSettings.AddRecord( uploadSettings );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return uploadOperatorSettings;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the OPERATORS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( OperatorSet iOperSettings )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               string operName;

               // process each of the available records
               for ( int i = 0; i < iOperSettings.OperatorSettings.Count; ++i )
               {
                  if ( result )
                  {
                     //get the operator name associated with the setting record
                     operName = iOperSettings.OperatorNames.NameData[ i ].Name;

                     //populate the table record
                     result = result & WriteRecord(
                         iOperSettings.OperatorSettings.SettingsData[ i ],
                         operName, ceResultSet );
                  }
               }
               
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Operating settings to the OPERATORS table
      /// </summary>
      /// <param name="iOperSettings">list of operator settings</param>
      /// <returns>true if success, else false</returns>
      public Boolean AddEnumRecords( EnumOperators iOperSettings )
      {
         // open database connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "OPERATORS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( OperatorRecord oper in iOperSettings )
               {
                  //populate the table record
                  result = result & WriteRecord(oper, ceResultSet );
               }
               
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Change the current user password and update the OPERATORS record and
      /// also (if necessary) force a 'SET' update on the ResetPassword flag
      /// </summary>
      /// <param name="iOperatorId">Selected operator</param>
      /// <param name="iNewPassword">New password</param>
      public void ChangePassword( int iOperatorId, string iNewPassword )
      {
         OpenCeConnection();

         // update the password
         iNewPassword = PacketBase.EncryptPassword( iNewPassword );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_OPERATOR_ID;

            // reference the Operator ID field as a parameter
            command.Parameters.Add( "@OperId", SqlDbType.Int ).Value = iOperatorId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the OPERATORS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  UpdateRecord( iNewPassword, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// OPERATORS table. This table will need to be revised in the event of any 
      /// changes being made to the OPERATORS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsOperators( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a new OPERATORS record
      /// </summary>
      /// <param name="iOperSettings">Populated OperatorSettingXML object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( OperatorSettingXML iOperSettings, string iOperName,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iOperSettings.OperatorId;
            newRecord[ mColumnOrdinal.OperName ] = iOperName;
            newRecord[ mColumnOrdinal.DadType ] = (byte) iOperSettings.DadType;
            newRecord[ mColumnOrdinal.AccessLevel ] = (byte) iOperSettings.Settings.AccessLevel;
            newRecord[ mColumnOrdinal.CanChangePassword ] = iOperSettings.Settings.CanChangePassword;
            newRecord[ mColumnOrdinal.CreateWorkNotification ] = iOperSettings.Settings.CreateWorkNotification;
            newRecord[ mColumnOrdinal.DataCollectionTime ] = iOperSettings.Settings.DataCollectionTime;
            newRecord[ mColumnOrdinal.Password ] = iOperSettings.Settings.Password;
            newRecord[ mColumnOrdinal.ResetPassword ] = (byte) iOperSettings.Settings.ResetPassword;
            newRecord[ mColumnOrdinal.ScanRequiredToCollect ] = iOperSettings.Settings.ScanRequiredToCollect;
            newRecord[ mColumnOrdinal.ShowPreData ] = iOperSettings.Settings.ShowPreData;
            newRecord[ mColumnOrdinal.VerifyMode ] = (byte) iOperSettings.Settings.VerifyMode;
            newRecord[ mColumnOrdinal.ViewOverdueOnly ] = (byte) iOperSettings.Settings.ViewOverdueOnly;
            newRecord[ mColumnOrdinal.ViewTreeElement ] = (byte) iOperSettings.Settings.ViewTreeElement;
            newRecord[ mColumnOrdinal.ViewCMMSWorkHistory ] = iOperSettings.Settings.ViewCMMSWorkHistory;
            newRecord[ mColumnOrdinal.NumericRangeProtection ] = iOperSettings.Settings.NumericRangeProtection;
            newRecord[ mColumnOrdinal.ScanAndGoToFirstPoint ] = iOperSettings.Settings.ScanAndGoToFirstPoint;
            newRecord[ mColumnOrdinal.ViewSpectralFFT ] = iOperSettings.Settings.ViewSpectralFFT;
            newRecord[ mColumnOrdinal.EnableCamera ] = iOperSettings.Settings.EnableCamera;
            newRecord[ mColumnOrdinal.BarcodeLogin ] = iOperSettings.Settings.BarcodeLogin;
            newRecord[ mColumnOrdinal.BarcodePassword ] = iOperSettings.Settings.BarcodePassword;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Construct and populate a new OPERATORS record
      /// </summary>
      /// <param name="iOperSettings">Populated OperatorSettingXML object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( OperatorRecord iOperSettings, SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.OperId ] = iOperSettings.OperId;
            newRecord[ mColumnOrdinal.OperName ] = iOperSettings.OperName;
            newRecord[ mColumnOrdinal.DadType ] = (byte) iOperSettings.DadType;
            newRecord[ mColumnOrdinal.AccessLevel ] = (byte) iOperSettings.AccessLevel;
            newRecord[ mColumnOrdinal.CanChangePassword ] = iOperSettings.CanChangePassword;
            newRecord[ mColumnOrdinal.CreateWorkNotification ] = iOperSettings.CreateWorkNotification;
            newRecord[ mColumnOrdinal.DataCollectionTime ] = iOperSettings.DataCollectionTime;
            newRecord[ mColumnOrdinal.Password ] =  PacketBase.EncryptPassword(iOperSettings.Password);
            newRecord[ mColumnOrdinal.ResetPassword ] = (byte) iOperSettings.ResetPassword;
            newRecord[ mColumnOrdinal.ScanRequiredToCollect ] = iOperSettings.ScanRequiredToCollect;
            newRecord[ mColumnOrdinal.ShowPreData ] = iOperSettings.ShowPreData;
            newRecord[ mColumnOrdinal.VerifyMode ] = (byte) iOperSettings.VerifyMode;
            newRecord[ mColumnOrdinal.ViewOverdueOnly ] = (byte) iOperSettings.ViewOverdueOnly;
            newRecord[ mColumnOrdinal.ViewTreeElement ] = (byte) iOperSettings.ViewTreeElement;
            newRecord[ mColumnOrdinal.ViewCMMSWorkHistory ] = iOperSettings.ViewCMMSWorkHistory;
            newRecord[ mColumnOrdinal.NumericRangeProtection ] = iOperSettings.NumericRangeProtection;
            newRecord[ mColumnOrdinal.ScanAndGoToFirstPoint ] = iOperSettings.ScanAndGoToFirstPoint;
            newRecord[ mColumnOrdinal.ViewSpectralFFT ] = iOperSettings.ViewSpectralFFT;
            newRecord[mColumnOrdinal.EnableCamera] = iOperSettings.EnableCamera;
            newRecord[mColumnOrdinal.BarcodeLogin] = iOperSettings.BarcodeLogin;
            newRecord[mColumnOrdinal.BarcodePassword] = iOperSettings.BarcodePassword;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update the password and set the "PasswordChanged" flag true
      /// </summary>
      /// <param name="iNewPassword">updated operator password</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private Boolean UpdateRecord( String iNewPassword, SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetBoolean( mColumnOrdinal.PasswordChanged, true );
            ceResultSet.SetString( mColumnOrdinal.Password, iNewPassword );

            //
            // force update of the ResetPassword flag to 'SET'.
            // Note: We always have to do this so that @ptitude
            // can know to remove any ResetPassword flag that 
            // might be set against a specific operator.
            //
            ceResultSet.SetByte(
               mColumnOrdinal.ResetPassword,
               (byte) ResetPassword.Set );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated OPERATORS Object with data from a single 
      /// OPERATORS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iOperSettings">OPERATORS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref OperatorSettingXML iOperSettings )
      {
         try
         {
            iOperSettings.OperatorId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iOperSettings.DadType = ceResultSet.GetInt32( mColumnOrdinal.DadType );
            iOperSettings.Settings.AccessLevel = (OperatorRights) ceResultSet.GetByte( mColumnOrdinal.AccessLevel );
            iOperSettings.Settings.CanChangePassword = ceResultSet.GetBoolean( mColumnOrdinal.CanChangePassword );
            iOperSettings.Settings.CreateWorkNotification = ceResultSet.GetBoolean( mColumnOrdinal.CreateWorkNotification );
            iOperSettings.Settings.DataCollectionTime = ceResultSet.GetInt32( mColumnOrdinal.DataCollectionTime );
            iOperSettings.Settings.Password = PacketBase.DecryptPassword( ceResultSet.GetString( mColumnOrdinal.Password ) );
            iOperSettings.Settings.ResetPassword = (ResetPassword) ceResultSet.GetByte( mColumnOrdinal.ResetPassword );
            iOperSettings.Settings.ScanRequiredToCollect = ceResultSet.GetBoolean( mColumnOrdinal.ScanRequiredToCollect );
            iOperSettings.Settings.ShowPreData = ceResultSet.GetBoolean( mColumnOrdinal.ShowPreData );
            iOperSettings.Settings.VerifyMode = (VerificationMode) ceResultSet.GetByte( mColumnOrdinal.VerifyMode );
            iOperSettings.Settings.ViewOverdueOnly = (OverdueFilter) ceResultSet.GetByte( mColumnOrdinal.ViewOverdueOnly );
            iOperSettings.Settings.ViewTreeElement = (ViewTreeAs) ceResultSet.GetByte( mColumnOrdinal.ViewTreeElement );
            iOperSettings.Settings.ViewCMMSWorkHistory = ceResultSet.GetBoolean( mColumnOrdinal.ViewCMMSWorkHistory );
            iOperSettings.Settings.NumericRangeProtection = ceResultSet.GetBoolean( mColumnOrdinal.NumericRangeProtection );
            iOperSettings.Settings.ScanAndGoToFirstPoint = ceResultSet.GetBoolean( mColumnOrdinal.ScanAndGoToFirstPoint );
            iOperSettings.Settings.ViewSpectralFFT = ceResultSet.GetBoolean( mColumnOrdinal.ViewSpectralFFT );
            iOperSettings.Settings.EnableCamera = ceResultSet.GetBoolean( mColumnOrdinal.EnableCamera );
            iOperSettings.Settings.BarcodeLogin = ceResultSet.GetString( mColumnOrdinal.BarcodeLogin );
            iOperSettings.Settings.BarcodePassword = ceResultSet.GetBoolean( mColumnOrdinal.BarcodePassword );
         }
         catch
         {
            iOperSettings = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated OperatorName Object with data from a single 
      /// OPERATORS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iOperName">OPERATOR NAME object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref OperatorName iOperName )
      {
         try
         {
            iOperName.OperatorId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iOperName.Name = ceResultSet.GetString( mColumnOrdinal.OperName );
         }
         catch
         {
            iOperName = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated UploadSetting object.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iOperName">UploadSettings object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref UploadSettings iSettings )
      {
         try
         {
            iSettings.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iSettings.Password = ceResultSet.GetString( mColumnOrdinal.Password );
            iSettings.ResetPassword = ceResultSet.GetByte( mColumnOrdinal.ResetPassword );
         }
         catch
         {
            iSettings = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated OPERATOR table record object.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iOperName">OPERATOR table record object to populate</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref OperatorRecord iSettings )
      {
         try
         {
            iSettings.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iSettings.OperName = ceResultSet.GetString( mColumnOrdinal.OperName );
            iSettings.DadType = ceResultSet.GetInt32( mColumnOrdinal.DadType );
            iSettings.AccessLevel = (OperatorRights) ceResultSet.GetByte( mColumnOrdinal.AccessLevel );
            iSettings.CanChangePassword = ceResultSet.GetBoolean( mColumnOrdinal.CanChangePassword );
            iSettings.CreateWorkNotification = ceResultSet.GetBoolean( mColumnOrdinal.CreateWorkNotification );
            iSettings.DataCollectionTime = ceResultSet.GetInt32( mColumnOrdinal.DataCollectionTime );
            iSettings.Password = PacketBase.DecryptPassword( ceResultSet.GetString( mColumnOrdinal.Password ) );
            iSettings.ResetPassword = (ResetPassword) ceResultSet.GetByte( mColumnOrdinal.ResetPassword );
            iSettings.ScanRequiredToCollect = ceResultSet.GetBoolean( mColumnOrdinal.ScanRequiredToCollect );
            iSettings.ShowPreData = ceResultSet.GetBoolean( mColumnOrdinal.ShowPreData );
            iSettings.VerifyMode = (VerificationMode) ceResultSet.GetByte( mColumnOrdinal.VerifyMode );
            iSettings.ViewOverdueOnly = (OverdueFilter) ceResultSet.GetByte( mColumnOrdinal.ViewOverdueOnly );
            iSettings.ViewTreeElement = (ViewTreeAs) ceResultSet.GetByte( mColumnOrdinal.ViewTreeElement );
            iSettings.ViewCMMSWorkHistory = ceResultSet.GetBoolean( mColumnOrdinal.ViewCMMSWorkHistory );
            iSettings.NumericRangeProtection = ceResultSet.GetBoolean( mColumnOrdinal.NumericRangeProtection );
            iSettings.ScanAndGoToFirstPoint = ceResultSet.GetBoolean( mColumnOrdinal.ScanAndGoToFirstPoint );
            iSettings.ViewSpectralFFT = ceResultSet.GetBoolean( mColumnOrdinal.ViewSpectralFFT );
            iSettings.EnableCamera = ceResultSet.GetBoolean( mColumnOrdinal.EnableCamera );
            iSettings.BarcodeLogin = ceResultSet.GetString( mColumnOrdinal.BarcodeLogin );
            iSettings.BarcodePassword = ceResultSet.GetBoolean( mColumnOrdinal.BarcodePassword );
         }
         catch
         {
            iSettings = null;
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 20th June 2012
//  Add method "GetAllRecords()" to populate the new OperatorRecord object
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------