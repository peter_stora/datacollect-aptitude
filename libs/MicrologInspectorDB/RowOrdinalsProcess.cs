﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the PROCESS table. This structure should be revised following any
    /// changes made to this table.
    /// </summary>
    public class RowOrdinalsProcess
    {
        public int Uid { get; set; }
        public int Number { get; set; }
        public int PointUid { get; set; }
        public int Form { get; set; }
        public int RangeMin { get; set; }
        public int RangeMax { get; set; }
        public int Units { get; set; }
        public int AutoRange { get; set; }
        public int AlarmType { get; set; }
        public int AlarmState { get; set; }
        public int UpperAlert { get; set; }
        public int LowerAlert { get; set; }
        public int UpperDanger { get; set; }
        public int LowerDanger { get; set; }
        public int BaselineDataTime { get; set; }
        public int BaselineDataValue { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsProcess(SqlCeResultSet iCeResultSet)
        {
            GetProcessTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// PROCESS table. This table will need to be revised in the event of any changes
        /// being made to the PROCESS table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetProcessTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.Number = iCeResultSet.GetOrdinal("Number");
            this.PointUid = iCeResultSet.GetOrdinal("PointUid");
            this.Form = iCeResultSet.GetOrdinal("Form");
            this.RangeMin = iCeResultSet.GetOrdinal("RangeMin");
            this.RangeMax = iCeResultSet.GetOrdinal("RangeMax");
            this.Units = iCeResultSet.GetOrdinal("Units");
            this.AutoRange = iCeResultSet.GetOrdinal("AutoRange");
            this.AlarmType = iCeResultSet.GetOrdinal("AlarmType");
            this.AlarmState = iCeResultSet.GetOrdinal("AlarmState");
            this.UpperAlert = iCeResultSet.GetOrdinal("UpperAlert");
            this.LowerAlert = iCeResultSet.GetOrdinal("LowerAlert");
            this.UpperDanger = iCeResultSet.GetOrdinal("UpperDanger");
            this.LowerDanger = iCeResultSet.GetOrdinal("LowerDanger");
            this.BaselineDataTime = iCeResultSet.GetOrdinal("BaselineDataTime");
            this.BaselineDataValue = iCeResultSet.GetOrdinal("BaselineDataValue");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------