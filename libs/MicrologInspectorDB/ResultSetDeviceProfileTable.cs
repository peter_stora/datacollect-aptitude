﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the DEVICEPROFILE Table ResultSet
   /// </summary>
   public partial class ResultSetDeviceProfileTable : ResultSetBase
   {
      /// <summary>
      /// Parameterized query string for recovering the device profile record
      /// </summary>
      private const string QUERY_PROFILE = "SELECT * FROM DEVICEPROFILE";

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsDeviceProfile mColumnOrdinal = null;


      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_DEVICEPROFILE;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a new device profile record to the table.
      /// </summary>
      /// <param name="iProfile">Populated profile object</param>
      /// <returns>true if new record added without errors</returns>
      public Boolean AddDeviceProfile( DeviceProfile iProfile )
      {
         // open the current connection
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DEVICEPROFILE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // if no result returned then add a new record
               if ( ceResultSet.HasRows )
               {
                  ceResultSet.Delete();
               }

               // update table
               result = WriteRecord( iProfile, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close the table connection
         CloseCeConnection();

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Get the current device profile object
      /// </summary>
      /// <returns>Device profile or null</returns>
      public DeviceProfile GetCurrentProfile()
      {
         // open the current connection
         OpenCeConnection();

         // create a blank devive profile object
         DeviceProfile deviceProfile = new DeviceProfile();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_PROFILE;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               if ( ceResultSet.HasRows )
               {
                  ceResultSet.Read();

                  // populate the device profile object
                  PopulateObject( ceResultSet, ref deviceProfile );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close the table connection
         CloseCeConnection();

         // return the method result
         return deviceProfile;

      }/* end method */


      /// <summary>
      /// This method will check the current profile against the new profile 
      /// record. If the profile needs to be updated then the record will be
      /// updated and the result set true
      /// </summary>
      /// <param name="iProfile">Populated Profile object</param>
      /// <returns>true if profile was updated</returns>
      public Boolean ProfileWasUpdated( ProfileSettings iProfile )
      {
         // open the current connection
         OpenCeConnection();

         // set the default result to true - update the device profile
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_PROFILE;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               //
               // if no result returned then add a new record, otherwise
               // compare the new profile object against the existing one
               //
               if ( !ceResultSet.HasRows )
               {
                  result = WriteRecord( new DeviceProfile( iProfile ), ceResultSet );
               }
               else
               {
                  result = ExistingProfileUpdated( iProfile, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         // close the table connection
         CloseCeConnection();

         // return the method result
         return result;

      }/* end method */


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// C_NOTE table. This table will need to be revised in the event of any 
      /// changes being made to the C_NOTE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsDeviceProfile( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new Device Profile record and add it 
      /// to the current DEVICEPROFILE table.
      /// </summary>
      /// <param name="iDeviceProfile">Populated DeviceProfile Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( DeviceProfile iDeviceProfile,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.ProfileName ] = iDeviceProfile.ProfileName;
            newRecord[ mColumnOrdinal.ProfileID ] = iDeviceProfile.ProfileID;
            newRecord[ mColumnOrdinal.DeviceName ] = iDeviceProfile.DeviceName;
            newRecord[ mColumnOrdinal.LastModified ] = iDeviceProfile.LastModified;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate a device profile object from the single DEVICEPROFILE record.
      /// On error the device profile object will be reset to null
      /// </summary>
      /// <param name="iDeviceProfile">Populated DeviceProfile Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                 ref DeviceProfile iDeviceProfile )
      {
         try
         {
            iDeviceProfile.ProfileName =
               ceResultSet.GetString( mColumnOrdinal.ProfileName );

            iDeviceProfile.ProfileID =
               ceResultSet.GetString( mColumnOrdinal.ProfileID );

            iDeviceProfile.DeviceName =
               ceResultSet.GetString( mColumnOrdinal.DeviceName );

            iDeviceProfile.LastModified =
               ceResultSet.GetDateTime( mColumnOrdinal.LastModified );
         }
         catch
         {
            iDeviceProfile = null;
         }

      }/* end method */


      /// <summary>
      /// Do we need to update an existing profile record within the database? 
      /// If we do then execute that task here
      /// </summary>
      /// <param name="iProfile">new profile object from Middleware</param>
      /// <param name="ceResultSet">current CeResultSet</param>
      /// <returns>true if profile updated</returns>
      private Boolean ExistingProfileUpdated( ProfileSettings iProfile,
         SqlCeResultSet ceResultSet )
      {
         // set the default result to false - the profile was not updated
         Boolean result = false;

         // reference the actual profile object
         DeviceProfile profile = new DeviceProfile( iProfile );

         // only ever read the first record
         while ( ceResultSet.Read() )
         {
            // get the current profile from the database
            DeviceProfile currentProfile = new DeviceProfile();
            PopulateObject( ceResultSet, ref currentProfile );

            //
            // does this new profile object match the current profile record 
            // and is the 'dirty' flag still set false?
            //
            if ( ( currentProfile.ProfileName == profile.ProfileName ) &
                ( currentProfile.ProfileID == profile.ProfileID ) &
                ( currentProfile.DeviceName == profile.DeviceName ) &
                ( !iProfile.ProfileChanged ) )
            {
               // if yes, then set result true 
               result = false;
            }
            else
            {
               //
               // if not then update the profile record (including the last 
               // modified date and time flag)
               //
               ceResultSet.SetString( mColumnOrdinal.ProfileName, profile.ProfileName );
               ceResultSet.SetString( mColumnOrdinal.ProfileID, profile.ProfileID );
               ceResultSet.SetString( mColumnOrdinal.DeviceName, profile.DeviceName );
               ceResultSet.SetDateTime( mColumnOrdinal.LastModified, PacketBase.DateTimeNow );
               ceResultSet.Update();

               // and set the result false
               result = true;
            }
         }

         return result;

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------