﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the NODE Table ResultSet
   /// </summary>
   public class ResultSetNodeTable : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsNode mColumnOrdinal = null;

      /// <summary>
      /// Query NODE table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_NODE_UID = "SELECT * FROM NODE " +
          "WHERE Uid = @Uid";

      /// <summary>
      /// Query NODE table on parent UID to return a list of siblings
      /// </summary>
      private const string QUERY_ENUM_BY_NODE_PARENT_UID = "SELECT * FROM NODE " +
          "WHERE ParentUid = @ParentUid ORDER BY Number";


      /// <summary>
      /// Query NODE table on current UID to return list of children
      /// </summary>
      private const string QUERY_ENUM_BY_NODE_UID = "SELECT * FROM NODE " +
          "WHERE ParentUid = @ParentUid ORDER BY Number ASC";


      /// <summary>
      /// Query NODE table based purely on the "Flags" field (i.e. has anyone
      /// edited this Node!
      /// </summary>
      private const string QUERY_ENUM_BY_FLAGS = "SELECT * FROM NODE " +
          "WHERE Flags >= @Flags ORDER BY Number ASC";


      /// <summary>
      /// Query NODE table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_ENUM_FOR_ROOTS = "SELECT * FROM NODE " +
          "WHERE HierarchyDepth = @HierarchyDepth ORDER BY Number ASC";

      /// <summary>
      /// Query NODE table based on the LocationTag and order results by NUMBER 
      /// </summary>
      private const string QUERY_BY_LOCATION_TAG = "SELECT * FROM NODE " +
          "WHERE LocationTag = @LocationTag ORDER by NUMBER ASC";

      /// <summary>
      /// This is for OTD# 2511 and 2512, because @A doesn't limit the size of
      /// the "Asset Name" (or Functional Location) of a Machine, we may need
      /// to truncate this value
      /// </summary>
      private const int MAX_FUNCTIONAL_LOCATION_LENGTH = 50;

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_NODE;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single populated record to the NODE table. 
      /// </summary>
      /// <param name="iNodeRecord">Populated NodeRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( NodeRecord iNodeRecord )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NODE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               if ( !WriteRecord( iNodeRecord, ceResultSet ) )
               {
                  result = false;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of NodeRecord objects to the NODE table and
      /// return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of NodeRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumNodes iEnumNodes, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the nodes list
         iEnumNodes.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NODE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( NodeRecord node in iEnumNodes )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( node, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of NodeRecord objects to the NODE table. 
      /// </summary>
      /// <param name="iEnumPoints">Enumerated List of NodeRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumNodes iEnumNodes )
      {
         OpenCeConnection();

         // reset the nodes list
         iEnumNodes.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NODE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( NodeRecord node in iEnumNodes )
               {
                  if ( !WriteRecord( node, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single NodeRecord object from the NODE table using 
      /// the Uid(or unique Id field) as the lookup reference. 
      /// </summary>
      /// <param name="iNodeUid">Node Unique identifier</param>
      /// <returns>Populated node object or null on error</returns>
      public NodeRecord GetRecord( Guid iNodeUid )
      {
         OpenCeConnection();

         // reset the default profile name
         NodeRecord nodeRecord = new NodeRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref nodeRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return nodeRecord;

      }/* end method */


      /// <summary>
      /// Quickly returns an enumerated list of all NODE records ordered
      /// by the primary key field NODE.UID
      /// </summary>
      /// <returns>an enumerated list of NodeRecord objects</returns>
      public EnumNodes GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNodes nodeRecords = new EnumNodes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "NODE";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  NodeRecord nodeRecord = new NodeRecord();
                  PopulateObject( ceResultSet, ref nodeRecord );
                  nodeRecords.AddNodeRecord( nodeRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return nodeRecords;

      }/* end method */


      /// <summary>
      /// Updates a NODE table record with the data from its associated
      /// business object (a single NODE Record object). As all fields are
      /// replaced, this method should be used with caution! 
      /// </summary>
      /// <param name="iPointRecord">Populated NODE Record object</param>
      public void UpdateRecord( NodeRecord iNodeRecord )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iNodeRecord.Uid;

            // execure the result set
            using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( CeResultSet );

               // now read the record object
               if ( CeResultSet.ReadFirst() )
               {
                  UpdateRecord( iNodeRecord, CeResultSet );
               }

               // close the resultSet
               CeResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Returns a list of all Node table root objects (i.e ROUTES, HIERARCHY etc)
      /// </summary>
      /// <returns>enumerated array of NODE objects</returns>
      public EnumNodes GetRoots()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNodes nodeRecords = new EnumNodes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ENUM_FOR_ROOTS;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@HierarchyDepth", SqlDbType.Int ).Value = 0;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  NodeRecord nodeRecord = new NodeRecord();
                  PopulateObject( ceResultSet, ref nodeRecord );
                  nodeRecords.AddNodeRecord( nodeRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return nodeRecords;

      }/* end method */


      /// <summary>
      /// Returns a list of ancestor nodes in reverse Hierarchy order. To return
      /// a list of ancestors for a given point, the parameter (iParentUid) should
      /// reference POINT.NodeID, while for a node object it should reference the
      /// field NODE.ParentUid.
      /// </summary>
      /// <param name="iParentUid">base ancestor ID</param>
      /// <returns>EnumNode objects in descending hierarchy order</returns>
      public EnumNodes GetAncestors( Guid iParentUid )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNodes nodeRecords = new EnumNodes();

         // reference the parent UID and reset the hierarchy depth
         Guid parentUid = iParentUid;
         int hierarchyDepth = 0;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NODE_UID;
            command.CommandType = CommandType.Text;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier );

            // loop up through the ancestors
            do
            {
               // set the query against value
               command.Parameters[ 0 ].Value = parentUid;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = 
                  command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the NODE table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // now read the record object
                  if ( ceResultSet.ReadFirst() )
                  {
                     // create a new node object
                     NodeRecord nodeRecord = new NodeRecord();

                     // populate from the result object
                     PopulateObject( ceResultSet, ref nodeRecord );

                     // get the hierarchy depth and parent UID
                     hierarchyDepth = nodeRecord.HierarchyDepth;
                     parentUid = nodeRecord.ParentUid;

                     // add this node to the results list
                     nodeRecords.AddNodeRecord( nodeRecord );
                  }
                  else
                  {
                     hierarchyDepth = 0;
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }

            } while ( hierarchyDepth > 0 );

         }
         CloseCeConnection();
         return nodeRecords;

      }/* end method */


      /// <summary>
      /// This method updates a series of Node Records. This method 
      /// should primarily be used in conjunction with GetAncestors()
      /// </summary>
      /// <param name="iNodeRecords">EnumNodes to update</param>
      /// <returns>true on success, else false</returns>
      public Boolean UpdateRecords( EnumNodes iNodeRecords )
      {
         // set the initial result
         Boolean result = true;

         // reset the Node object list - essential this!!
         iNodeRecords.Reset();

         // open the connection
         OpenCeConnection();

         // better safe than sorry
         try
         {
            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand() )
            {
               // reference the base query string
               command.CommandText = QUERY_BY_NODE_UID;

               // reference the lookup field as a parameter
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier );

               foreach ( NodeRecord node in iNodeRecords )
               {
                  // reference the lookup field as a parameter
                  command.Parameters[ 0 ].Value = node.Uid;

                  // execute the result set
                  using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                              ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated 
                     // with the NODE table. As this is a  'one shot'  the column 
                     // ordinal object remains a singleton
                     //
                     GetTableColumns( CeResultSet );

                     // now read the record object
                     if ( CeResultSet.ReadFirst() )
                     {
                        UpdateRecord( node, CeResultSet );
                     }

                     // close the resultSet
                     CeResultSet.Close();
                  }
               }
            }
         }
         catch
         {
            result = false;
         }
         finally
         {
            CloseCeConnection();
         }

         // we're done here
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of sibling node objects, i.e nodes that 
      /// all have the same parent Uid as the lookup reference. 
      /// </summary>
      /// <param name="iParentUid">Unique identifier of parent</param>
      /// <returns>Enumerated list of NODE objects or null on error</returns>
      public EnumNodes GetSiblings( Guid iParentUid )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNodes nodeRecords = new EnumNodes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ENUM_BY_NODE_PARENT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@ParentUid", SqlDbType.UniqueIdentifier ).Value = iParentUid;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  NodeRecord nodeRecord = new NodeRecord();
                  PopulateObject( ceResultSet, ref nodeRecord );
                  nodeRecords.AddNodeRecord( nodeRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return nodeRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of sibling node objects, i.e nodes that 
      /// all have the same parent Uid as the lookup reference. 
      /// </summary>
      /// <param name="iLocationTag">Filter by LocationTag</param>
      /// <returns>Enumerated list of NODE objects or null on error</returns>
      public EnumNodes GetEnumRecords( string iLocationTag )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumNodes nodeRecords = new EnumNodes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ENUM_BY_NODE_PARENT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@LocationTag", SqlDbType.Text ).Value = iLocationTag;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  NodeRecord nodeRecord = new NodeRecord();
                  PopulateObject( ceResultSet, ref nodeRecord );
                  nodeRecords.AddNodeRecord( nodeRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return nodeRecords;

      }/* end method */


      /// <summary>
      /// This method checks the NODE table for all records that have had the
      /// "Flags" field set to "NodeTagChanged". All SQL results are then
      /// returned as a an enumerated list of UploadNodeRecord objects
      /// </summary>
      /// <returns>Enumerated list of UploadNodeRecord objects</returns>
      public EnumUploadNodeRecords GetUploadNodes()
      {
         OpenCeConnection();

         // reference the "Node has changed" enumerator
         Byte flagType = (byte) FieldTypes.NodeFlagType.NodeTagChanged;

         EnumUploadNodeRecords enumUploadNodes = new EnumUploadNodeRecords();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ENUM_BY_FLAGS;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Flags", SqlDbType.TinyInt ).Value = flagType;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // create a new 'generic' node record
               NodeRecord nodeRecord = new NodeRecord();

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // populate the generic node
                  PopulateObject( ceResultSet, ref nodeRecord );

                  // create a new upload node object
                  UploadNodeRecord uploadNode = new UploadNodeRecord();

                  // add the generic node fields to the upload node object
                  uploadNode.AddNodeRecord( nodeRecord );

                  // add the upload node to the enumerated node list
                  enumUploadNodes.AddNodeRecord( uploadNode );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumUploadNodes;

      }/*end method */


      /// <summary>
      /// This method updates the status fields of a specific NODE record. The
      /// main purpose of this method is to integrate READ and UPDATE methods
      /// into one method - thereby reducing the time taken to execute a pretty
      /// common method call.
      /// ResultSet object.
      /// </summary>
      /// <param name="iNodeUid">Node identifier (this node only)</param>
      /// <param name="iCheckStatus">The collection check flag status</param>
      /// <param name="iAlarmState">The Boolean alarm state logic</param>
      /// <returns>Parent Guid</returns>
      public Guid RefreshNodeState( Guid iNodeUid,
          FieldTypes.CollectionCheck iCheckStatus,
          FieldTypes.NodeAlarmState iAlarmState )
      {
         OpenCeConnection();

         // default NULL guid value
         Guid result = PacketBase.NODE_NULL_GUID;

         // reset the default profile name
         NodeRecord nodeRecord = new NodeRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                ResultSetOptions.Scrollable | ResultSetOptions.Updatable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  // populate the Node object
                  PopulateObject( ceResultSet, ref nodeRecord );

                  //
                  // only do something if the node object was actually
                  // populated and is valid
                  //
                  if ( nodeRecord != null )
                  {
                     //
                     // only update the alarm or checkstatus fields if either
                     // actually needs to be updated. If nothing needs to be
                     // done then skip this step and just return a NULL Guid.
                     //
                     if ( ( nodeRecord.CollectionCheck != (byte) iCheckStatus ) ||
                         ( nodeRecord.AlarmState != (byte) iAlarmState ) )
                     {
                        // reset the NODE status fields
                        nodeRecord.AlarmState = (byte) iAlarmState;
                        nodeRecord.CollectionCheck = (byte) iCheckStatus;

                        // now update the record and resturn the Parent Uid
                        if ( UpdateRecord( nodeRecord, ceResultSet ) )
                        {
                           result = nodeRecord.ParentUid;
                        }
                     }
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the NODE table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public Boolean ResetAllFlags()
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            byte maskLSB = 0x01;
            int flags = 0x00;

            try
            {
               // reference the base query string
               command.CommandText = "NODE";
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the NODE table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // go through each of the result records and reset the new flag
                  while ( ceResultSet.Read() )
                  {
                     // reset the collected children status to "none"
                     ceResultSet.SetByte( mColumnOrdinal.CheckMark, (byte) FieldTypes.CollectionCheck.None );

                     // reset the collected alarm status to "off"
                     ceResultSet.SetByte( mColumnOrdinal.AlarmState, (byte) FieldTypes.NodeAlarmState.Reset );

                     // mask out the "tag changed" bit
                     flags = ceResultSet.GetByte( mColumnOrdinal.Flags ) & maskLSB;
                     ceResultSet.SetByte( mColumnOrdinal.Flags, (byte) flags );

                     // update the node record
                     ceResultSet.Update();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/*end method */

      #endregion


      #region Private Methods

      /// <summary>
      /// Construct and populate a single new NODE record and add it 
      /// to the current NODE table.
      /// </summary>
      /// <param name="iNodeObject">Populated Node Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( NodeRecord iNodeObject,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iNodeObject.Uid;

            // OTD# 3005 - protect against Oracle null fields
            if ( iNodeObject.Id != null )
               newRecord[ mColumnOrdinal.Id ] = iNodeObject.Id;
            else
               newRecord[ mColumnOrdinal.Id ] = string.Empty;

            // OTD# 3005 - protect against Oracle null fields
            if ( iNodeObject.Description != null )
               newRecord[ mColumnOrdinal.Description ] = iNodeObject.Description;
            else
               newRecord[ mColumnOrdinal.Description ] = string.Empty;

            if ( iNodeObject.LastModified.Ticks > 0 )
               newRecord[ mColumnOrdinal.LastModified ] = iNodeObject.LastModified;

            newRecord[ mColumnOrdinal.LocationMethod ] = iNodeObject.LocationMethod;
            
            // protect against null field (OTD #3005)
            if ( iNodeObject.LocationTag != null )
               newRecord[ mColumnOrdinal.LocationTag ] = iNodeObject.LocationTag;
            else
               newRecord[ mColumnOrdinal.LocationTag ] = string.Empty;
            
            newRecord[ mColumnOrdinal.ParentUid ] = iNodeObject.ParentUid;
            newRecord[ mColumnOrdinal.PRISM ] = iNodeObject.PRISM;
            newRecord[ mColumnOrdinal.Number ] = iNodeObject.Number;
            newRecord[ mColumnOrdinal.HierarchyDepth ] = iNodeObject.HierarchyDepth;
            newRecord[ mColumnOrdinal.Flags ] = iNodeObject.Flags;
            newRecord[ mColumnOrdinal.Structured ] = iNodeObject.Structured;
            newRecord[ mColumnOrdinal.RouteId ] = iNodeObject.RouteId;
            newRecord[ mColumnOrdinal.AlarmState ] = iNodeObject.AlarmState;
            newRecord[ mColumnOrdinal.CheckMark ] = iNodeObject.CollectionCheck;

            // OTD# 2512 - since @A doesn't limit this length, we have to check...
            if ((iNodeObject.FunctionalLocation != null) && 
               ( iNodeObject.FunctionalLocation.Length > MAX_FUNCTIONAL_LOCATION_LENGTH ))
            {
               iNodeObject.FunctionalLocation = 
                  iNodeObject.FunctionalLocation.Substring( 0, MAX_FUNCTIONAL_LOCATION_LENGTH );
            }
            
            // OTD# 3005 - protect against Oracle null fields
            if ( iNodeObject.FunctionalLocation != null )
               newRecord[ mColumnOrdinal.FunctionalLocation ] = iNodeObject.FunctionalLocation;
            else
               newRecord[ mColumnOrdinal.FunctionalLocation ] = string.Empty;
            
            newRecord[ mColumnOrdinal.DefWorkType ] = iNodeObject.DefWorkType;
            newRecord[ mColumnOrdinal.LastInspResult ] = iNodeObject.LastInspResult;
            newRecord[ mColumnOrdinal.NoLastMeas ] = iNodeObject.NoLastMeas;
            newRecord[ mColumnOrdinal.ElementType ] = iNodeObject.ElementType;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single NODE table record
      /// </summary>
      /// <param name="iNodeRecord">Updated NodeRecord object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( NodeRecord iNodeRecord,
          SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iNodeRecord.Uid );
            
            if (iNodeRecord.Id != null)
               CeResultSet.SetString( mColumnOrdinal.Id, iNodeRecord.Id );
            
            if (iNodeRecord.Description != null)
               CeResultSet.SetString( mColumnOrdinal.Description, iNodeRecord.Description );

            if ( iNodeRecord.LastModified.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.LastModified, iNodeRecord.LastModified );

            //
            // Work around for OTD# 3214, @A can't handle a location tag of type RFID...
            // TODO: if @A ever supports RFID tag types... fix this
            //
            if( iNodeRecord.LocationMethod == (byte)FieldTypes.LocationType.RFID )
               CeResultSet.SetByte( mColumnOrdinal.LocationMethod, (byte)FieldTypes.LocationType.Barcode );
            else
               CeResultSet.SetByte( mColumnOrdinal.LocationMethod, iNodeRecord.LocationMethod );
             
            // protect against null field (OTD 3005)
            if (iNodeRecord.LocationTag != null) 
               CeResultSet.SetString( mColumnOrdinal.LocationTag, iNodeRecord.LocationTag );
            
            CeResultSet.SetGuid( mColumnOrdinal.ParentUid, iNodeRecord.ParentUid );
            CeResultSet.SetInt32( mColumnOrdinal.PRISM, iNodeRecord.PRISM );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iNodeRecord.Number );
            CeResultSet.SetInt32( mColumnOrdinal.HierarchyDepth, iNodeRecord.HierarchyDepth );
            CeResultSet.SetByte( mColumnOrdinal.Flags, iNodeRecord.Flags );
            CeResultSet.SetByte( mColumnOrdinal.Structured, iNodeRecord.Structured );
            CeResultSet.SetInt32( mColumnOrdinal.RouteId, iNodeRecord.RouteId );
            
            if (iNodeRecord.FunctionalLocation != null)
               CeResultSet.SetString( mColumnOrdinal.FunctionalLocation, iNodeRecord.FunctionalLocation );
            
            CeResultSet.SetInt32( mColumnOrdinal.DefWorkType, iNodeRecord.DefWorkType );
            CeResultSet.SetByte( mColumnOrdinal.LastInspResult, iNodeRecord.LastInspResult );
            CeResultSet.SetByte( mColumnOrdinal.NoLastMeas, iNodeRecord.NoLastMeas );
            CeResultSet.SetByte( mColumnOrdinal.ElementType, iNodeRecord.ElementType );
            CeResultSet.SetByte( mColumnOrdinal.CheckMark, iNodeRecord.CollectionCheck );
            CeResultSet.SetByte( mColumnOrdinal.AlarmState, iNodeRecord.AlarmState );

            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// NODE table. This table will need to be revised in the event of any changes
      /// being made to the NODE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsNode( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Populate an instantiated NodeObject with data from a single NODE
      /// table record. In the event of a raised exception the returned node
      /// object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="nodeRecord">Node object to be populated</param>
      private void PopulateObject( SqlCeResultSet CeResultSet,
          ref NodeRecord iNodeRecord )
      {
         try
         {
            iNodeRecord.Uid = CeResultSet.GetGuid( mColumnOrdinal.Uid );

            if ( CeResultSet[ mColumnOrdinal.Id ] != DBNull.Value )
               iNodeRecord.Id = CeResultSet.GetString( mColumnOrdinal.Id );

            if ( CeResultSet[ mColumnOrdinal.Description ] != DBNull.Value )
               iNodeRecord.Description = CeResultSet.GetString( mColumnOrdinal.Description );

            if ( CeResultSet[ mColumnOrdinal.LastModified ] != DBNull.Value )
               iNodeRecord.LastModified = CeResultSet.GetDateTime( mColumnOrdinal.LastModified );

            iNodeRecord.LocationMethod = CeResultSet.GetByte( mColumnOrdinal.LocationMethod );
            
            if ( CeResultSet[ mColumnOrdinal.LocationTag ] != DBNull.Value )
               iNodeRecord.LocationTag = CeResultSet.GetString( mColumnOrdinal.LocationTag );
            
            iNodeRecord.ParentUid = CeResultSet.GetGuid( mColumnOrdinal.ParentUid );
            iNodeRecord.PRISM = CeResultSet.GetInt32( mColumnOrdinal.PRISM );
            iNodeRecord.Number = CeResultSet.GetInt32( mColumnOrdinal.Number );
            iNodeRecord.HierarchyDepth = CeResultSet.GetInt32( mColumnOrdinal.HierarchyDepth );
            iNodeRecord.Flags = CeResultSet.GetByte( mColumnOrdinal.Flags );
            iNodeRecord.Structured = CeResultSet.GetByte( mColumnOrdinal.Structured );
            iNodeRecord.RouteId = CeResultSet.GetInt32( mColumnOrdinal.RouteId );

            // protect against null field value (OTD #3005)
            if ( CeResultSet[ mColumnOrdinal.FunctionalLocation ] != DBNull.Value )
               iNodeRecord.FunctionalLocation = CeResultSet.GetString( mColumnOrdinal.FunctionalLocation );
            
            iNodeRecord.DefWorkType = CeResultSet.GetInt32( mColumnOrdinal.DefWorkType );
            iNodeRecord.LastInspResult = CeResultSet.GetByte( mColumnOrdinal.LastInspResult );
            iNodeRecord.NoLastMeas = CeResultSet.GetByte( mColumnOrdinal.NoLastMeas );
            iNodeRecord.ElementType = CeResultSet.GetByte( mColumnOrdinal.ElementType );
            iNodeRecord.CollectionCheck = CeResultSet.GetByte( mColumnOrdinal.CheckMark );
            iNodeRecord.AlarmState = CeResultSet.GetByte( mColumnOrdinal.AlarmState );
         }
         catch
         {
            iNodeRecord = null;
         }

      }/* end method */

      #endregion

   }/*end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------