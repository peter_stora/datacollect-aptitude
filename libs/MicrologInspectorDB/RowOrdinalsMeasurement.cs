﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class contains the ordinal positions of each field within the 
   /// TEMP, ENV and VEL DATA tables. This class should be revised following 
   /// any changes made to any of these table structures.
   /// </summary>
   public class RowOrdinalsMeasurement
   {
      public int PointUid
      {
         get;
         set;
      }

      public int PointHostIndex
      {
         get;
         set;
      }

      public int LocalTime
      {
         get;
         set;
      }

      public int Value
      {
         get;
         set;
      }

      public int Text
      {
         get;
         set;
      }

      public int New
      {
         get;
         set;
      }

      public int AlarmState
      {
         get;
         set;
      }

      public int OperId
      {
         get;
         set;
      }

      public int Status
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsMeasurement( SqlCeResultSet iCeResultSet )
      {
         GetMeasurementTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// DATA tables. This table will need to be revised in the event of any changes
      /// being made to the DATA table field structures.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetMeasurementTableColumns( SqlCeResultSet iCeResultSet )
      {
         // identify each of the column ordinals
         this.PointUid = iCeResultSet.GetOrdinal( "PointUid" );
         this.PointHostIndex = iCeResultSet.GetOrdinal( "PointHostIndex" );
         this.LocalTime = iCeResultSet.GetOrdinal( "LocalTime" );
         this.Value = iCeResultSet.GetOrdinal( "Value" );
         this.Text = iCeResultSet.GetOrdinal( "Text" );
         this.New = iCeResultSet.GetOrdinal( "New" );
         this.AlarmState = iCeResultSet.GetOrdinal( "AlarmState" );
         this.OperId = iCeResultSet.GetOrdinal( "OperId" );   
         this.Status = iCeResultSet.GetOrdinal( "Status" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 14th September 2012
//  Add support for new TextData Measurement type
//
//  Revision 0.0 APinkerton 16th March 2009 (14:20)
//  Add to project
//----------------------------------------------------------------------------