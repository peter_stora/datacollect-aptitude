﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;
using System.Windows.Forms;
using System.Threading;
using SKF.RS.MicrologInspector.Database.Properties;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class is the main interface layer between the application and
   /// the SQL Server Compact database. All data passed between this class
   /// and object owners is via the business objects defined within the
   /// assembly InspectorPackets.dll
   /// </summary>
   public class DataConnect : IDisposable
   {
      //-------------------------------------------------------------------------

      #region Private Fields

      /// <summary>
      /// Maximum number of records that can be written
      /// in a single batch driven database update
      /// </summary>
      private const int MAX_BATCH_RECORDS = 100;

      /// <summary>
      /// The default TextData alarm state
      /// </summary>
      private const FieldTypes.NodeAlarmState TEXT_DATA_ALARM_STATE = 
         FieldTypes.NodeAlarmState.Clear;

      /// <summary>
      /// Is the current database version OK?
      /// </summary>
      private Boolean mVersionOK;

      /// <summary>
      /// What database version does the application have
      /// </summary>
      private string mCurrentDbVersion = string.Empty;

      /// <summary>
      /// Link to the table connection interface
      /// </summary>
      protected ITableConnect mTable = null;

      /// <summary>
      /// Populate Tables object
      /// </summary>
      protected PopulateTable mPopulateTable = null;

      /// <summary>
      /// Clear Tables object
      /// </summary>
      protected ClearTable mClearTable = null;

      /// <summary>
      /// Reference to the list of available operators
      /// </summary>
      protected UserLogin mOperatorSet = null;

      /// <summary>
      /// Reference to upload packet builder object
      /// </summary>
      protected BuildUploadPacket mBuildUpload = null;

      /// <summary>
      /// Reference to parent object
      /// </summary>
      protected Form mParent = null;

      /// <summary>
      /// FFT Channel scratchpad object 
      /// </summary>
      protected EnumFFTChannelRecords mFFTChannelsEnum = null;

      /// <summary>
      /// Temperature measurements scratchpad object
      /// </summary>
      protected EnumPointMeasurements mTempMeasurementsEnum = null;

      /// <summary>
      /// Envelope acceleration measurements scratchpad object
      /// </summary>
      protected EnumPointMeasurements mEnvMeasurementsEnum = null;

      /// <summary>
      /// Velocity measurements scratchpad object
      /// </summary>
      protected EnumPointMeasurements mVelMeasurementsEnum = null;

      /// <summary>
      /// FFT Base channel scratchpad object
      /// </summary>
      protected EnumBaseFFTChannels mFFTBaseChannelData;

      /// <summary>
      /// FFT Setup scratchpad object
      /// </summary>
      protected FFTPointRecord mFFTPoint = null;

      /// <summary>
      /// Upload notes scratchpad object
      /// </summary>
      protected UploadNoteRecord mUploadNoteRecord = null;

      /// <summary>
      /// Measurement count scratchpad
      /// </summary>
      protected int mMeasurements = 0;

      /// <summary>
      /// Is the current measurement to overwrite an existing one? Note: This
      /// member is an integer used as a Boolean, as raw Boolean types do not
      /// support the concept safe exchange by reference
      /// </summary>
      protected int mOverwrite = Convert.ToByte( false );

      /// <summary>
      /// Current operator ID
      /// </summary>
      protected int mOperId = FieldTypes.NULL_OPERID;

      /// <summary>
      /// Track whether Dispose has been called.
      /// </summary>
      private bool mDisposed = false;
      #endregion

      //-------------------------------------------------------------------------

      #region Public Constants
      /// <summary>
      /// The number of FFTs allowed to be stored in the DB
      /// </summary>
      public const int DB_CAPACITY_FFT = 500;

      /// <summary>
      /// The number of overalls allowed to be stored in the DB
      /// </summary>
      public const int DB_CAPACITY_OVERALLS = 5000;

      /// <summary>
      /// The number of FFTs allowed to be stored in the DB
      /// </summary>
      public const int DB_CAPACITY_FFT_WARNING = 300;

      /// <summary>
      /// The number of overalls allowed to be stored in the DB
      /// </summary>
      public const int DB_CAPACITY_OVERALLS_WARNING = 3000;
      #endregion

      //-------------------------------------------------------------------------

      #region Cached Table objects

      /// <summary>
      /// Populated list of current point objects
      /// </summary>
      protected EnumPoints mPointsEnum = null;

      /// <summary>
      /// Populated list of current node objects
      /// </summary>
      protected EnumNodes mNodesEnum = null;

      /// <summary>
      /// Populated list of current process setup objects
      /// </summary>
      protected EnumProcess mProcessEnum = null;

      /// <summary>
      /// Populated list of current text data setup objects
      /// </summary>
      protected EnumTextData mTextDataEnum = null;

      /// <summary>
      /// Populated list of current inspection setup objects
      /// </summary>
      protected EnumInspection mInspectionEnum = null;

      /// <summary>
      /// Populated list of current MCD setup objects
      /// </summary>
      protected EnumMCC mMccEnum = null;

      /// <summary>
      /// Populated list of available node code objects
      /// </summary>
      protected EnumCodedNotes mCodedNotes = null;

      /// <summary>
      /// Populated list of current FFT setup objects
      /// </summary>
      protected EnumFFTPoints mFFTPointsEnum = null;

      /// <summary>
      /// Populated list of hierarchy messages
      /// </summary>
      protected EnumMessages mMessagesEnum = null;

      /// <summary>
      /// Populate list of Conditional Point objects
      /// </summary>
      protected EnumConditionalPoints mConditionalPoints = null;

      /// <summary>
      /// Populated list of WN Corrective Actions
      /// </summary>
      protected EnumCmmsCorrectiveActions mCmmsCorrectiveActions = null;

      /// <summary>
      /// Populated list of WN Priorities
      /// </summary>
      protected EnumCmmsPriorities mCmmsPriorities = null;

      /// <summary>
      /// Populated list of WN Problem Descriptions
      /// </summary>
      protected EnumCmmsProblems mCmmsProblems = null;

      /// <summary>
      /// Populated list of WN Work Types
      /// </summary>
      protected EnumCmmsWorkTypes mCmmsWorkTypes = null;

      #endregion

      //----------------------------------------------------------------------------

      #region Delegates and Events

      /// <summary>
      /// Progress value delegate
      /// </summary>
      /// <param name="iDataProgress">populated data progress object</param>
      public delegate void ProgressValue( IDataProgress iDataProgress );

      /// <summary>
      /// Status flag delegate
      /// </summary>
      /// <param name="iBusy">true is DataConnect is busy</param>
      public delegate void BusyStatus( Boolean iBusy );

      /// <summary>
      /// Internal error codes
      /// </summary>
      /// <param name="iErrorCode"></param>
      public delegate void DataError( DataConnectError iError );

      /// <summary>
      /// Progress value delegate that synchs to the parent UI thread
      /// </summary>
      /// <param name="iDataProgress">populated data progress object</param>
      private delegate void SynchUIProgressValue( IDataProgress iDataProgress );

      /// <summary>
      /// Status flag delegate that synchs to the parent UI thread
      /// </summary>
      /// <param name="iBusy">populated data progress object</param>
      private delegate void SynchUIClientStatus( Boolean iBusy );

      /// <summary>
      /// Internal error codes synched to the UI
      /// </summary>
      /// <param name="iErrorCode"></param>
      private delegate void SynchUIError( DataConnectError iError );

      /// <summary>
      /// Event raised on change in Progress value
      /// </summary>
      public event ProgressValue ProgressValueEvent;

      /// <summary>
      /// Event raised on change in client busy state
      /// </summary>
      public event BusyStatus BusyStatusEvent;

      /// <summary>
      /// Event raised on error returned
      /// </summary>
      public event DataError ErrorEvent;

      #endregion

      //----------------------------------------------------------------------------

      #region Delegate support methods

      /// <summary>
      /// Raise event on update to cuurent progress position
      /// </summary>
      /// <param name="iDataProgress">populated data progress object</param>
      protected void RaiseProgressUpdateEvent( IDataProgress iDataProgress )
      {
         if ( ProgressValueEvent != null )
         {
            ProgressValueEvent( iDataProgress );
         }

      }/* end method */


      /// <summary>
      /// Synch progress updates with the parent thread.
      /// Note: if no reference to the parent form is supplied, 
      /// this method will not attempt to raise an event
      /// </summary>
      /// <param name="iDataProgress">populated data progress object</param>
      protected void RaiseProgressUpdateEventAsynch( IDataProgress iDataProgress )
      {
         if ( ( mParent != null ) && ( mParent.InvokeRequired ) )
         {
            SynchUIProgressValue theDelegate =
                    new SynchUIProgressValue( RaiseProgressUpdateEventAsynch );
            mParent.Invoke( theDelegate, new object[] { iDataProgress } );
         }
         else
         {
            RaiseProgressUpdateEvent( iDataProgress );
         }

      }/* end method */


      /// <summary>
      /// Raise event on change in client status (busy or done)
      /// </summary>
      /// <param name="iBusy">set true when object is busy</param>
      protected void RaiseBusyEvent( Boolean iBusy )
      {
         if ( BusyStatusEvent != null )
         {
            BusyStatusEvent( iBusy );
         }

      }/* end method */


      /// <summary>
      /// Synch status event to parent UI thread
      /// Note: if no reference to the parent form is supplied, 
      /// this method will not attempt to raise an event
      /// </summary>
      /// <param name="iBusy">set true when object is busy</param>
      protected void RaiseBusyEventAsync( Boolean iBusy )
      {
         if ( ( mParent != null ) && ( mParent.InvokeRequired ) )
         {
            SynchUIClientStatus theDelegate = new SynchUIClientStatus( RaiseBusyEventAsync );
            mParent.Invoke( theDelegate, new object[] { iBusy } );
         }
         else
         {
            RaiseBusyEvent( iBusy );
         }

      }/* end method */


      /// <summary>
      /// Raise event on Error raised
      /// </summary>
      /// <param name="iErrorCode">Returned Error Code</param>
      protected void RaiseErrorEvent( DataConnectError iError )
      {
         if ( ErrorEvent != null )
         {
            ErrorEvent( iError );
         }

      }/* end method */


      /// <summary>
      /// Synch raised error event to parent UI thread
      /// Note: if no reference to the parent form is supplied, 
      /// this method will not attempt to raise an event
      /// </summary>
      /// <param name="iErrorCode">Returned Error Code</param>
      protected void RaiseErrorEventAsync( DataConnectError iError )
      {
         if ( ( mParent != null ) && ( mParent.InvokeRequired ) )
         {
            SynchUIError theDelegate = new SynchUIError( RaiseErrorEventAsync );
            mParent.Invoke( theDelegate, new object[] { iError } );
         }
         else
         {
            RaiseErrorEvent( iError );
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Constructors and destructors

      /// <summary>
      /// Base Constructor
      /// </summary>
      public DataConnect() :
         this( null )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iSender">reference to calling object</param>
      public DataConnect( object iSender ) :
         this( iSender, string.Empty )
      {
      }/* end constructor */


      /// <summary>
      /// Overload constructor
      /// </summary>
      /// <param name="iSender">reference to calling object</param>
      /// <param name="iDataSource">Full path to (and name of) SDF Database</param>
      public DataConnect( object iSender, string iDataSource )
      {
         // reference the parent object
         if ( iSender != null )
            mParent = (Form) iSender;

         //
         // create the Table Connect object using either the
         // default database name and path, or via a specified
         // name and path
         //
         if ( iDataSource == string.Empty )
         {
            mTable = new TableConnect();
         }
         else
         {
            mTable = new TableConnect( iDataSource );
         }

         // is the current database version OK?
         mVersionOK = mTable.DatabaseVersionOK;

         // get the current version of the database in the app folder
         mCurrentDbVersion = mTable.CurrentDbVersion;

         // create a new Notes Buffer
         mUploadNoteRecord = new UploadNoteRecord();

      }/* end constructor */


      /// <summary>
      /// Implement IDisposable.
      /// Note: A derived class must not be able to override this method!
      /// </summary>
      public virtual void Dispose()
      {
         // dispose of the managed and unmanaged resources
         Dispose( true );

         //
         // tell the Garbage Collector that the Finalize process no 
         // longer needs to be run for this object.
         //
         GC.SuppressFinalize( this );
      }


      /// <summary>
      /// Dispose(bool disposing) executes in two distinct scenarios.
      /// If disposing equals true, the method has been called directly
      /// or indirectly by a user's code. Managed and unmanaged resources
      /// can be disposed.
      /// If disposing equals false, the method has been called by the
      /// runtime from inside the finalizer and you should not reference
      /// other objects. Only unmanaged resources can be disposed.
      /// </summary>
      /// <param name="iDisposeManagedResources"></param>
      private void Dispose( bool iDisposing )
      {
         // Check to see if Dispose has already been called.
         if ( !this.mDisposed )
         {
            // If disposing equals true, dispose all managed
            // and unmanaged resources.
            if ( iDisposing )
            {
               mTable.Dispose();

               ClearMeasurementsEnum();
               ClearAllMccEnum();
               ClearAllCodedNotesEnum();
               ClearAllInspectionsEnum();
               ClearAllProcessEnum();
               ClearAllTextDataEnum();
               ClearAllNodesEnum();
               ClearAllPointsEnum();
               ClearAllFFTPointsEnum();
               ClearAllMessagesEnum();
               ClearAllConditionalPointsEnum( );
               ClearAllCmmsCorrectiveActionsEnum( );
               ClearAllCmmsPriorityEnum( );
               ClearAllCmmsProblemEnum( );
               ClearAllCmmsWorkTypesEnum( );

               mPointsEnum = null;
               mFFTPointsEnum = null;
               mNodesEnum = null;
               mProcessEnum = null;
               mTextDataEnum = null;
               mInspectionEnum = null;
               mMccEnum = null;
               mCodedNotes = null;
               mUploadNoteRecord = null;
               mTempMeasurementsEnum = null;
               mMessagesEnum = null;
               mConditionalPoints = null;
               mCmmsCorrectiveActions = null;
               mCmmsPriorities = null;
               mCmmsProblems = null;
               mCmmsWorkTypes = null;
            }
            // Note disposing has been done.
            mDisposed = true;
         }
      }


      /// <summary>
      /// This destructor will run only if the Dispose method
      /// does not get called.
      /// It gives your base class the opportunity to finalize.
      /// Do not provide destructors in types derived from this class.
      /// </summary>
      ~DataConnect()
      {
         Dispose( false );
      }

      #endregion

      //----------------------------------------------------------------------------

      #region Feedback Management

      /// <summary>
      /// Set the progress event handles for each of the tables 
      /// </summary>
      private void PassThroughFeedbackEvents()
      {
         // Progress events from the Upload file builder
         mBuildUpload.ProgressPosition.OnUpdate +=
             new DataProgress.ProgressPosition( ReturnProgressPositionEvent );

         // Progress events from the table "bulk" populator
         mPopulateTable.ProgressPosition.OnUpdate +=
             new DataProgress.ProgressPosition( ReturnProgressPositionEvent );

         // Progress events from the table "bulk" clear and reset operations
         mClearTable.ProgressPosition.OnUpdate +=
            new DataProgress.ProgressPosition( ReturnProgressPositionEvent );

      }/* end method */


      /// <summary>
      /// Manage progress updates from table processing events
      /// </summary>
      /// <param name="Sender">calling object</param>
      void ReturnProgressPositionEvent( object Sender )
      {
         // cast reference to the calling object
         DataProgress progressPosition = (DataProgress) Sender;

         // and return progress object as an event
         RaiseProgressUpdateEventAsynch( progressPosition );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public properties

      /// <summary>
      /// Checks whether the database version is OK.
      /// </summary>
      /// <returns></returns>
      public Boolean DatabaseVersionOK
      {
         get
         {
            return mVersionOK;
         }
      }


      /// <summary>
      /// Gets the required database version
      /// </summary>
      public string ApplicationDbVersion
      {
         get
         {
            return Resources.IDS_VERSION_DB;
         }
      }


      /// <summary>
      /// Gets the version of the database in the app folder
      /// </summary>
      public string CurrentDbVersion
      {
         get
         {
            return mCurrentDbVersion;
         }
      }


      /// <summary>
      /// Gets or Sets a reference to the NODE enum objects
      /// </summary>
      public EnumNodes NodesEnum
      {
         get
         {
            return mNodesEnum;
         }
         set
         {
            mNodesEnum = value;
         }
      }


      /// <summary>
      /// Gets or Sets a reference to the POINTS enum objects
      /// </summary>
      public EnumPoints PointsEnum
      {
         get
         {
            return mPointsEnum;
         }
         set
         {
            mPointsEnum = value;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the PROCESS enum objects
      /// </summary>
      public EnumProcess ProcessEnum
      {
         get
         {
            return mProcessEnum;
         }
         set
         {
            mProcessEnum = value;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the TEXTDATA enum objects
      /// </summary>
      public EnumTextData TextDataEnum
      {
         get
         {
            return mTextDataEnum;
         }
         set
         {
            mTextDataEnum = value;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the INSPECTION enum objects
      /// </summary>
      public EnumInspection InspectionEnum
      {
         get
         {
            return mInspectionEnum;
         }
         set
         {
            mInspectionEnum = value;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the MCC enum objects
      /// </summary>
      public EnumMCC MCCEnum
      {
         get
         {
            return mMccEnum;
         }
         set
         {
            mMccEnum = value;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the Hierarchy Message enum objects
      /// </summary>
      public EnumMessages HierarchyMessages
      {
         get
         {
            return mMessagesEnum;
         }
         set
         {
            mMessagesEnum = value;
         }
      }


      /// <summary>
      /// Gets of sets a reference to the Conditional Points enum objects
      /// </summary>
      public EnumConditionalPoints ConditionalPoints
      {
         get
         {
            return mConditionalPoints;
         }
         set
         {
            mConditionalPoints = value;
         }
      }


      /// <summary>
      /// Gets whether CMMS settings are available on the device.  Since the
      /// user is only required to select a "Priority" and a "Problem 
      /// Description", only these two tables are checked...
      /// </summary>
      public bool CmmsSettingsAvailable
      {
         get
         {
            if ( ( mCmmsPriorities != null ) && ( mCmmsPriorities.Count > 0 ) &&
                ( mCmmsProblems != null ) && ( mCmmsProblems.Count > 0 ) )
            {
               return true;
            }
            else
            {
               return false;
            }
         }
      }


      /// <summary>
      /// Gets a reference to the WN Corrective Actions
      /// </summary>
      public EnumCmmsCorrectiveActions CmmsCorrectiveActions
      {
         get
         {
            return mCmmsCorrectiveActions;
         }
      }
      

      /// <summary>
      /// Gets a reference to the WN Priorities
      /// </summary>
      public EnumCmmsPriorities CmmsPriorities
      {
         get
         {
            return mCmmsPriorities;
         }
      }


      /// <summary>
      /// Gets a reference to the WN Problem Descriptions
      /// </summary>
      public EnumCmmsProblems CmmsProblems
      {
         get
         {
            return mCmmsProblems;
         }
      }


      /// <summary>
      /// Gets a reference to the WN Work Types
      /// </summary>
      public EnumCmmsWorkTypes CmmsWorkTypes
      {
         get
         {
            return mCmmsWorkTypes;
         }
      }


      /// <summary>
      /// Gets an enumerated list of TEMP measurememts
      /// </summary>
      public EnumPointMeasurements TempMeasurementsEnum
      {
         get
         {
            return mTempMeasurementsEnum;
         }
      }


      /// <summary>
      /// Gets an enumerated list of ENV measurememts
      /// </summary>
      public EnumPointMeasurements EnvMeasurementsEnum
      {
         get
         {
            return mEnvMeasurementsEnum;
         }
      }


      /// <summary>
      /// Gets an enumerated list of VEL measurememts
      /// </summary>
      public EnumPointMeasurements VelMeasurementsEnum
      {
         get
         {
            return mVelMeasurementsEnum;
         }
      }


      /// <summary>
      /// Gets or sets a reference to the enum CodedNote objects
      /// </summary>
      public EnumCodedNotes CodedNotesEnum
      {
         get
         {
            return mCodedNotes;
         }
         set
         {
            mCodedNotes = value;
         }
      }


      /// <summary>
      /// Gets or sets the a reference to the enum FFT Point objects
      /// </summary>
      public EnumFFTPoints FFTPointsEnum
      {
         get
         {
            return mFFTPointsEnum;
         }
         set
         {
            mFFTPointsEnum = value;
         }
      }


      /// <summary>
      /// Gets or Sets Bulk table population
      /// </summary>
      public PopulateTable Populate
      {
         get
         {
            return mPopulateTable;
         }
         set
         {
            mPopulateTable = value;
         }
      }


      /// <summary>
      /// Gets or Sets Clear one or more tables
      /// </summary>
      public ClearTable Clear
      {
         get
         {
            return mClearTable;
         }
         set
         {
            mClearTable = value;
         }
      }


      /// <summary>
      /// Gets or Sets Build the Upload Xml Packet
      /// </summary>
      public BuildUploadPacket Upload
      {
         get
         {
            return mBuildUpload;
         }
         set
         {
            mBuildUpload = value;
         }
      }


      /// <summary>
      /// Gets or Sets Configure the Operator Settings
      /// </summary>
      public UserLogin Operators
      {
         get
         {
            return mOperatorSet;
         }
         set
         {
            mOperatorSet = value;
         }
      }


      /// <summary>
      /// Gets or sets the upload note object
      /// </summary>
      public UploadNoteRecord NotesBuffer
      {
         get
         {
            return mUploadNoteRecord;
         }
         set
         {
            mUploadNoteRecord = value;
         }
      }

      /// <summary>
      /// Gets the number off FFTs in the database
      /// </summary>
      public int FftCount
      {
         get
         {
            // any FFT is new, we don't keep historic FFTs after a sync
            return GetFFTChannelRecordsEnum( ).Count / 2;
         }
      }

      /// <summary>
      /// Gets the number of overalls (Temp, Vel, Env)
      /// </summary>
      public int OverallCount
      {
         get
         {
            int result = 0;

            var tempData = GetTempMeasurementsEnum( ).Measurement;
            var newTempData = from t in tempData
                              where t.New
                              select t;

            if( newTempData != null )
               result += newTempData.Count( );

            var envData = GetEnvMeasurementsEnum( ).Measurement;
            var newEnvData = from e in envData
                              where e.New
                              select e;

            if( newEnvData != null )
               result += newEnvData.Count( );


            var velData = GetVelMeasurementsEnum( ).Measurement;
            var newVelData = from v in velData
                             where v.New
                             select v;

            if( newVelData != null )
               result += newVelData.Count( );

            return result;
         }
      }
      #endregion

      //----------------------------------------------------------------------------

      #region Database Connection methods

      /// <summary>
      /// Build the table connections and validate the SQL link. Note this
      /// method must be executed prior to any table IO
      /// </summary>
      /// <returns>false on SQL connection error</returns>
      public Boolean Connect()
      {
         return this.Connect( false );

      }/* end method */


      /// <summary>
      /// Synchronously builds the table connections and validate the SQL link. 
      /// Note this method must be executed prior to any table IO
      /// </summary>
      /// <param name="iPopulateResultSets">Pre-populate the Result Sets</param>
      /// <returns>false on SQL connection error</returns>
      public Boolean Connect( Boolean iPopulateResultSets )
      {
         // now test the connection!
         if ( mTable.TestConnection() )
         {
            if ( mPopulateTable == null )
               mPopulateTable = new PopulateTable( mTable );

            if ( mClearTable == null )
               mClearTable = new ClearTable( mTable );

            if ( mOperatorSet == null )
               mOperatorSet = new UserLogin( mTable );

            if ( mBuildUpload == null )
               mBuildUpload = new BuildUploadPacket( mTable );

            // if required, populate the point and node object lists
            if ( iPopulateResultSets )
            {
               RefreshHierarchyData( false );
            }

            // configure how aggregate events are processed  
            PassThroughFeedbackEvents();

            // and everything went OK
            return true;
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Asynchronously Builds the table connections and validate the SQL link. 
      /// Note this method must be executed prior to any table IO
      /// </summary>
      /// <returns>false on SQL connection error</returns>
      public Boolean ConnectAsynch()
      {
         return this.ConnectAsynch( false );

      }/* end method */


      /// <summary>
      /// Asynchronously Build the table connections and validate the SQL link. 
      /// Note this method must be executed prior to any table IO.
      /// </summary>
      /// <param name="iPopulateResultSets">Pre-populate the Result Sets</param>
      /// <returns>false on SQL connection or threadpool errors</returns>
      public Boolean ConnectAsynch( Boolean iPopulateResultSets )
      {
         // now test the connection!
         if ( mTable.TestConnection() )
         {
            //
            // if connection is OK then attempt to launch the connection 
            // task  in a new worker thread from the threadpool.  If the 
            // threadpool does not oblige us, then return false
            //
            if ( ThreadPool.QueueUserWorkItem( new WaitCallback( ConnectInThread ),
               iPopulateResultSets ) )
            {
               return true;
            }
            else
            {
               return false;
            }
         }
         // if connection failed - return false!
         else
         {
            return false;
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Synchronous Database Utilities and Management

      /// <summary>
      /// Clear all table contents, including Operators, Device 
      /// Profiles and Measurements (Nuclear option!)
      /// </summary>
      /// <returns>True if no errors, else false</returns>
      public Boolean BlitzDatabase()
      {
         return mClearTable.All();

      }/* end method */


      /// <summary>
      /// Forces a new download from the server by clearing the 
      /// contents of the Device Profile table while leaving both
      /// Measurements and Operators untouched (so an upload of 
      /// collected data is still possible)
      /// </summary>
      /// <returns></returns>
      public Boolean ResetProfile()
      {
         return mClearTable.Profile();
      }


      /// <summary>
      /// This method will remove all newly collected measurements 
      /// from the database. All collection flags will be reset when
      /// this method is called.
      /// </summary>
      public void ClearNewMeasurements()
      {
         // get a list of points that have new data collected
         EnumPoints points = LinqQueries.GetPointsWithNewData( mPointsEnum );

         //
         // go through the points table and for every record that has data
         // assigned to it, clear the associated TEMP, VEL and ENV tables
         //
         foreach ( PointRecord point in points )
         {
            // delete all physical measurements ..
            if ( point.ProcessType == (byte) FieldTypes.ProcessType.MCD )
            {
               mTable.TempData.DeleteNewRecords( point );
               mTable.EnvData.DeleteNewRecords( point );
               mTable.VelData.DeleteNewRecords( point );
            }
            else
            {
               mTable.TempData.DeleteNewRecords( point );
            }
         }

         // clear everything from the FFT channel table
         mTable.FFTChannel.ClearTable();

         // clear everything from the notes tables
         mTable.Notes.ClearTable();
         mTable.SavedCodedNotes.ClearTable();

         // clear all local measurements from within the Points table
         mTable.Points.ResetAllMeasurementsAndFlags();

         // reset all internal collection flags within the Node table
         mTable.Node.ResetAllFlags();

      }/* end method */


      /// <summary>
      /// Backup the current database to an available SD card
      /// </summary>
      /// <returns>false on error</returns>
      public Boolean BackupDatabase( string iBackupToPath )
      {
         // create a new backup object
         BackupAndRestore backup = new BackupAndRestore( mTable );

         // set the backup path
         backup.BackupPath = iBackupToPath;

         // backup all tabled
         return backup.Backup_All_Tables();

      }/* end method */


      /// <summary>
      /// Restore an archive to the database
      /// </summary>
      /// <returns>false on error</returns>
      public Boolean RestoreDatabase( string iRestoreFromPath )
      {
         // create a new backup object
         BackupAndRestore backup = new BackupAndRestore( mTable );

         // set the restore path
         backup.BackupPath = iRestoreFromPath;

         // restore all archived tables
         return backup.Restore_All_Tables( );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Device profile support methods

      /// <summary>
      /// This method will check the current profile against the new profile 
      /// record. If the profile needs to be updated then the record will be
      /// replaced and the result set true
      /// </summary>
      /// <param name="iProfile">Populated Profile object</param>
      /// <returns>true if profile was updated</returns>
      public Boolean ProfileWasUpdated( ProfileSettings iProfile )
      {
         return mTable.DeviceProfile.ProfileWasUpdated( iProfile );

      }/* end method */


      /// <summary>
      /// Get the name of the current device profile object
      /// </summary>
      /// <returns>Device profile object or null</returns>
      public DeviceProfile GetCurrentProfile()
      {
         return mTable.DeviceProfile.GetCurrentProfile();

      }/* end method */


      /// <summary>
      /// Delete the current profile
      /// </summary>
      /// <returns></returns>
      public void DeleteCurrentProfile()
      {
         mTable.DeviceProfile.ClearTable();

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public Hierarchy management methods (Synchronous)

      /// <summary>
      /// Synchronously clear and then reload all "in memory" records
      /// </summary>
      public void RefreshHierarchy( Boolean iAllowBusyStatusEvents )
      {
         // load objects but supress the busy status flag
         this.RefreshHierarchyData( iAllowBusyStatusEvents );

      }/* end method */


      /// <summary>
      /// Returns one or more instructions for a given Node object
      /// </summary>
      /// <param name="iNode">Populated node object</param>
      /// <returns>instruction(s) or null</returns>
      public EnumMessages GetMessagesRef( NodeRecord iNode )
      {
         return mTable.Message.GetRecords( iNode.Uid );

      }/* end method */


      /// <summary>
      /// Returns one or more instructions for a given Node object
      /// </summary>
      /// <param name="iNode">Populated node object</param>
      /// <returns>instruction(s) or null</returns>
      public EnumInstructions GetInstructionsRef( NodeRecord iNode )
      {
         return mTable.Instructions.GetRecords( iNode.PRISM );

      }/* end method */


      /// <summary>
      /// Returns one or more instructions for a given point object
      /// </summary>
      /// <param name="iPoint">Populated point object</param>
      /// <returns>instruction(s) or null</returns>
      public EnumInstructions GetInstructionsRef( PointRecord iPoint )
      {
         return mTable.Instructions.GetRecords( iPoint.PointHostIndex );

      }/* end method */


      /// <summary>
      /// Returns a list of all instructions 
      /// </summary>
      /// <returns>enumeration of instruction objects</returns>
      public EnumInstructions GetAllInstructionsRef()
      {
         return mTable.Instructions.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns a list of ancestor nodes in reverse Hierarchy order. 
      /// To return a list of ancestors for a given point, the parameter 
      /// (iParentUid) should reference POINT.NodeID, while for a node 
      /// object it should reference the field NODE.ParentUid.
      /// </summary>
      /// <param name="iParentUid">base ancestor ID</param>
      /// <returns>EnumNode objects in descending hierarchy order</returns>
      public EnumNodes GetAllAncestors( Guid iParentUid )
      {
         return mTable.Node.GetAncestors( iParentUid );
      }/* end method */


      /// <summary>
      /// This method updates a series of Node Records. This method 
      /// should primarily be used in conjunction with GetAncestors()
      /// </summary>
      /// <param name="iNodeRecords">EnumNodes to update</param>
      /// <returns>true on success, else false</returns>
      public Boolean UpdateNodes( EnumNodes iNodes )
      {
         return mTable.Node.UpdateRecords( iNodes );
      }/* end method */


      /// <summary>
      /// This method builds each of the the hierarchy top level nodes.
      /// These nodes are NONROUTE, HIERARCHY as well as any ROUTES that
      /// were uploaded to the device.
      /// </summary>
      /// <returns>Enumerated list of NODE records</returns>
      public EnumNodes GetTreeBaseNodes()
      {
         return mTable.Node.GetRoots();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of sibling node objects, i.e nodes that 
      /// all have the same parent Uid as the lookup reference. 
      /// </summary>
      /// <param name="iParentUid">Unique identifier of parent</param>
      /// <returns>Enumerated list of NODE objects or null on error</returns>
      public EnumNodes GetSiblings( Guid iParentUid )
      {
         return mTable.Node.GetSiblings( iParentUid );
      }/* end method */


      /// <summary>
      /// Returns a single FFTPoint record given the PointRecord.Uid
      /// </summary>
      /// <param name="iPointUid">PointRecord identifier</param>
      /// <returns>Populated FFTPoint object, else null</returns>
      public FFTPointRecord GetFFTPointRecord( Guid iPointUid )
      {
         return mTable.FFTPoint.GetRecordFromPointUid( iPointUid );
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of FFT Point Records
      /// </summary>
      /// <returns>Enumerated list of FFTPoint objects</returns>
      public EnumFFTPoints GetFFTPointsEnum( )
      {
         return mTable.FFTPoint.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of FFT Channel Records
      /// </summary>
      /// <returns>Enumerated list of FFTChannelRecord objects or null on error</returns>
      public EnumFFTChannelRecords GetFFTChannelRecordsEnum( )
      {
         return mTable.FFTChannel.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of FFT Channel Records, based on a POINT;s
      /// UID.
      /// </summary>
      /// <param name="iPointUid">The POINT's Uid</param>
      /// <returns>Enumerated list of FFTChannelRecord objects or null on error</returns>
      public EnumFFTChannelRecords GetFFTChannelRecordsEnum( Guid iPointUid )
      {
         return mTable.FFTChannel.GetRecordsFromPointUid( iPointUid );
      }/* end method */


      /// <summary>
      /// This method will return a Derived POINT Record, based on a POINT's
      /// UID.
      /// </summary>
      /// <param name="iPointUid">The POINT's Uid</param>
      /// <returns>A Derived POINT Record</returns>
      public DerivedPointRecord GetDerivedPoint( Guid iPointUid )
      {
         return mTable.DerivedPoint.GetRecord( iPointUid );
      }/* end method */


      /// <summary>
      /// This method will return an enumerated list of "Derived Items", given
      /// the POINTS.Uid lookup value. This list will be ordered on the table
      /// field "FormulaItemPosition"
      /// </summary>
      /// <param name="iPointUid">Derived Point Uid</param>
      /// <returns>enumerated list of Derived Items ordered by Formula Position</returns>
      public EnumDerivedItems GetDerivedItems( Guid iPointUid )
      {
         return mTable.DerivedItems.GetRecord( iPointUid );
      }/* end method */


      /// <summary>
      /// This method resets all measurement "NEW" flags from TRUE to FALSE in each
      /// of the three of the measurement tables (TEMP, VEL and ENV). The primary 
      /// purpose of this method is to reduce the synchronization bandwidth by only 
      /// replacing historic measurement values when the device profile is updated.
      /// </summary>
      /// <returns>True on success, else false</returns>
      public Boolean ResetAllNewMeasurementFlags()
      {
         Boolean result = true && mTable.TempData.ResetAllNewFlags();
         result = result && mTable.VelData.ResetAllNewFlags();
         result = result && mTable.EnvData.ResetAllNewFlags();
         return result;
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all Node Records
      /// </summary>
      /// <returns></returns>
      public EnumNodes GetNodesEnum()
      {
         return mTable.Node.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all available Point Records
      /// </summary>
      /// <returns></returns>
      public EnumPoints GetPointsEnum()
      {
         return mTable.Points.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of Point Records based on the parent Uid 
      /// </summary>
      /// <param name="iNodeUid">Parent Uid</param>
      /// <returns>Enumerated list of Point objects ordered by number</returns>
      public EnumPoints GetPointsEnum( Guid iNodeUid )
      {
         return mTable.Points.GetEnumRecords( iNodeUid );
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of Point Records based on the process type
      /// </summary>
      /// <param name="iProcessType">Process type (DC, Counts etc)</param>
      /// <returns>Enumerated list of Point objects ordered by number</returns>
      public EnumPoints GetPointsEnum( FieldTypes.ProcessType iProcessType )
      {
         return mTable.Points.GetEnumRecords( iProcessType );
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of Process objects
      /// </summary>
      /// <returns></returns>
      public EnumProcess GetProcessEnum()
      {
         return mTable.Process.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of MCC objects
      /// </summary>
      /// <returns></returns>
      public EnumInspection GetInspectionEnum()
      {
         return mTable.Inspection.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of MCC objects
      /// </summary>
      /// <returns></returns>
      public EnumMCC GetMCCEnum()
      {
         return mTable.Mcc.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of TextData objects
      /// </summary>
      /// <returns></returns>
      public EnumTextData GetTextDataEnum()
      {
         return mTable.TextData.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Add a new measurement to database
      /// </summary>
      /// <param name="iPointRecord">Populated point objrct</param>
      /// <param name="iNoLastMeas">maximum number of measurements for this point</param>
      public void AddMeasurement( PointRecord iPointRecord, int iOperId, int iNoLastMeas )
      {
         // what is the process type associated with this point?
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         switch ( processType )
         {
            // for single selection (Boolean YES/NO questions)
            case FieldTypes.ProcessType.SingleSelectInspection:
               break;

            // for multiple selection values
            case FieldTypes.ProcessType.MultiSelectInspection:
               break;

            // for derived (or calculated) values
            case FieldTypes.ProcessType.Derived:
               break;

            //for MCD values
            case FieldTypes.ProcessType.MCD:
               AddScalarMCDMeasurements( iPointRecord, iNoLastMeas );
               break;

            // for all other (process) values
            default:
               AddTempMeasurement( iPointRecord, iOperId, iNoLastMeas );
               break;
         }

      }/* end method */


      /// <summary>
      /// Overload method to manage the processing of FFT Channel data
      /// </summary>
      /// <param name="iPointRecord">Parent Point object</param>
      /// <param name="iNoLastMeas">Number of previous measurements to save</param>
      /// <param name="iFFTChannelData">Enumerated list of channel data objects</param>
      public void AddMeasurement( PointRecord iPointRecord, int iNoLastMeas,
                                 EnumBaseFFTChannels iFFTChannelData )
      {
         // get the process type associated with this point
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         // confirm the process type is MCD only
         if ( processType == FieldTypes.ProcessType.MCD )
         {
            AddScalarMCDMeasurements( iPointRecord, iNoLastMeas );
            AddFFTChannels( iPointRecord, iFFTChannelData );
         }

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of TEMPDATA measurements
      /// </summary>
      /// <returns></returns>
      public EnumSavedMeasurements GetTempMeasurementsEnum(  )
      {
         return mTable.TempData.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of VELDATA measurements
      /// </summary>
      /// <returns></returns>
      public EnumSavedMeasurements GetVelMeasurementsEnum()
      {
         return mTable.VelData.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of ENVDATA measurements
      /// </summary>
      /// <returns></returns>
      public EnumSavedMeasurements GetEnvMeasurementsEnum()
      {
         return mTable.EnvData.GetAllRecords();
      }/* end method */


      /// <summary>
      /// Returns a list of all measurements (TEMP, ENV and VEL) for a given Point.
      /// Calling this method will clear all objects in TempMeasurementsEnum,
      /// VelMeasurementsEnum and EnvMeasurementsEnum before repopulating. For non
      /// MCD points only TempMeasurementsEnum will be repopulated.
      /// Note: As this method queries data on the PointHostIndex field "ALL" 
      /// measurements associated with that point will be returned, and not just
      /// those associated with a particular Route's Point!
      /// </summary>
      /// <param name="iPointRecord">Populated PointRecord object</param>
      public void GetMeasurementsEnum( PointRecord iPointRecord )
      {
         // clear all existing measurements
         ClearMeasurementsEnum();

         // get all TEMP measurements
         mTempMeasurementsEnum =
            mTable.TempData.EnumAllMeasurements( iPointRecord.PointHostIndex );

         // reset the enumeration pointer
         if ( ( mTempMeasurementsEnum != null ) && ( mTempMeasurementsEnum.Count > 0 ) )
         {
            mTempMeasurementsEnum.Reset();
         }

         //
         // if we're dealing with an MCD point then we also need the
         // VEL and ENV measurement sets
         //
         if ( iPointRecord.ProcessType == (byte) FieldTypes.ProcessType.MCD )
         {
            // get all ENV measurements
            mEnvMeasurementsEnum =
               mTable.EnvData.EnumAllMeasurements( iPointRecord.PointHostIndex );

            // reset the enumeration pointer
            if ( ( mEnvMeasurementsEnum != null ) && ( mEnvMeasurementsEnum.Count > 0 ) )
            {
               mEnvMeasurementsEnum.Reset();
            }

            // get all VEL measurements
            mVelMeasurementsEnum =
               mTable.VelData.EnumAllMeasurements( iPointRecord.PointHostIndex );

            // reset the enumeration pointer
            if ( ( mVelMeasurementsEnum != null ) && ( mVelMeasurementsEnum.Count > 0 ) )
            {
               mVelMeasurementsEnum.Reset();
            }
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public Synchronous Saved Media Methods

      /// <summary>
      /// Synchronously add a saved media file reference to to the database
      /// Note: Due to the speed of this action, it has not been made asynchronous
      /// </summary>
      /// <param name="iFileName">Name and extension of the file to be reference</param>
      /// <param name="iFilePath">Location where file is currently saved</param>
      /// <param name="iOperId">Operator ID</param>
      /// <param name="iPointIndex">Machine to referenec, or null for off route</param>
      /// <returns>true if file exists and reference added without problems</returns>
      public bool AddMediaFile( string iFileName, string iFilePath, 
         int iOperId, int? iPointIndex )
      {         
         // create a new saved media object
         SavedMediaRecord mediaRecord = new SavedMediaRecord(
            iFileName, 
            iFilePath, 
            iOperId, 
            BinaryMediaOrigin.OffRoute, 
            iPointIndex, 
            null, 
            string.Empty );
         //
         // if this actually exists then create the record, otherwise
         // ignore and return false
         //
         if ( mediaRecord.FileExists() )
         {
            return mTable.SavedMedia.AddRecord( mediaRecord );
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Synchronously add a saved media file reference to to the database
      /// Note: Due to the speed of this action, it has not been made asynchronous
      /// </summary>
      /// <param name="iFileName">Name and extension of the file to be reference</param>
      /// <param name="iFilePath">Location where file is currently saved</param>
      /// <param name="iOperId">Operator ID</param>
      /// <param name="iNoteText">Note Associated with this media object or null</param>
      /// <param name="iPointIndex">Machine to referenec, or null for off route</param>
      /// <returns>true if file exists and reference added without problems</returns>
      public bool AddMediaFile( string iFileName, string iFilePath, 
         int iOperId, string iNoteText, int? iPointIndex )
      {         
         // create a new saved media object
         SavedMediaRecord mediaRecord = new SavedMediaRecord(
            iFileName, 
            iFilePath, 
            iOperId, 
            BinaryMediaOrigin.OffRoute, 
            iPointIndex, 
            null, 
            iNoteText );
         //
         // if this actually exists then create the record, otherwise
         // ignore and return false
         //
         if ( mediaRecord.FileExists() )
         {
            return mTable.SavedMedia.AddRecord( mediaRecord );
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Synchronously add a saved media file reference to to the database
      /// Note: Due to the speed of this action, it has not been made asynchronous
      /// </summary>
      /// <param name="iFileName">Name and extension of the file to be reference</param>
      /// <param name="iFilePath">Location where file is currently saved</param>
      /// <param name="iOperId">Operator ID</param>
      /// <param name="iNoteText">Note Associated with this media object or null</param>
      /// <param name="iPointIndex">TREEELEMID to referenec, or null for off route</param>
      /// <param name="iOrigin">Off Route, Point, Machine or Work Notification</param>
      /// <param name="iWorkNotificationID ">WN ID, or null for Off Route, Point or Machine</param>
      /// <returns>true if file exists and reference added without problems</returns>
      public bool AddMediaFile( string iFileName, string iFilePath, 
         int iOperId, string iNoteText, int? iPointIndex, 
         BinaryMediaOrigin iOrigin, int? iWorkNotificationID )
      {
         // create a new saved media object
         SavedMediaRecord mediaRecord = new SavedMediaRecord(
            iFileName, 
            iFilePath, 
            iOperId, 
            iOrigin, 
            iPointIndex, 
            iWorkNotificationID, 
            iNoteText );

         //
         // if this actually exists then create the record, otherwise
         // ignore and return false
         //
         if ( mediaRecord.FileExists() )
         {
            return mTable.SavedMedia.AddRecord( mediaRecord );
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Returns a single Saved Media record based on the file name
      /// </summary>
      /// <param name="iMediaFile">File name to search on</param>
      /// <returns>populated saved media object</returns>
      public SavedMediaRecord GetMediaFile(string iMediaFile)
      {
         return mTable.SavedMedia.GetRecord( iMediaFile );
      
      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all SavedMedia records that
      /// pertain to a specific file type (i.e. Image or Audio)
      /// </summary>
      /// <param name="iMediaType">Filetype to return</param>
      /// <returns>enumerated list of Saved Media records</returns>
      public EnumSavedMedia GetMediaFiles( BinaryMediaType iMediaType )
      {
         return mTable.SavedMedia.GetRecords( iMediaType );

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all SavedMedia records that
      /// are to be uploaded
      /// </summary>
      /// <param name="iPendingUpload">True for files awaiting upload</param>
      /// <returns>enumerated list of Saved Media records</returns>
      public EnumSavedMedia GetUploadMediaFiles( Boolean iPendingUpload )
      {
         return mTable.SavedMedia.GetRecords( iPendingUpload );

      }/* end method */


      /// <summary>
      /// Update a saved media object
      /// </summary>
      /// <param name="iMediaRecord">Populated media object</param>
      public void UpdateMediaFile( SavedMediaRecord iMediaRecord )
      {
         mTable.SavedMedia.UpdateRecord( iMediaRecord );
      
      }/* end method */


      /// <summary>
      /// This method will delete all media records that have been uploaded
      /// to @ptitude and are no longer required on the handheld
      /// </summary>
      public bool DeleteAllUploadedMedia()
      {
         return mTable.SavedMedia.ClearUploaded();
      
      }/* end method */


      /// <summary>
      /// Delete a specific file, irrespective of whether it has been
      /// uploaded to @ptitude
      /// </summary>
      /// <param name="iFileName">Reference using File Name</param>
      public bool DeleteMediaReference( string iFileName )
      {
         return mTable.SavedMedia.ClearRecord( iFileName );
         
      }/* end method */


      /// <summary>
      /// Delete a specific file, irrespective of whether it has been
      /// uploaded to @ptitude
      /// </summary>
      /// <param name="iUid">Reference using record Uid</param>
      public bool DeleteMediaReference( Guid iUid )
      {
         return mTable.SavedMedia.ClearRecord( iUid );

      }/* end method */


      /// <summary>
      /// This method sets the 'Uploaded' flags in all SAVEDMEDIA records
      /// to TRUE. This method should only be called after the server has
      /// acknowledged that all files were received.
      /// </summary>
      public void SetAllMediaFileUploadFlags()
      {
         mTable.SavedMedia.SetAllMediaFileUploadFlags();
      }

      #endregion

      //----------------------------------------------------------------------------

      #region Public Synchronous Notes Methods

      /// <summary>
      /// Add a single note record (including a coded list) to the database
      /// and update the associated Point record to reflect changes to the 
      /// "TagChanged" table field.
      /// </summary>
      /// <param name="iNoteRecord">Populated notes object with codes</param>
      /// <param name="iPoint">Populated point object</param>
      /// <param name="iUpdatePointsTable">True to update points table</param>
      public void AddUserNote( UploadNoteRecord iNoteRecord,
         ref PointRecord iPoint, Boolean iUpdatePointsTable )
      {
         // create an populate a note object
         NotesRecord noteRecord = new NotesRecord( true );

         noteRecord.PointUid = iPoint.Uid;
         noteRecord.Number = iPoint.Number;
         noteRecord.CollectionStamp = iNoteRecord.CollectionStamp;
         noteRecord.NoteType = iNoteRecord.NoteType;
         noteRecord.OperId = iNoteRecord.OperId;
         noteRecord.TextNote = iNoteRecord.TextNote;
         noteRecord.LastModified = iNoteRecord.LastModified;

         // add note record to the table
         mTable.Notes.AddRecord( noteRecord );

         // add each of the coded notes
         mTable.SavedCodedNotes.AddEnumRecords(
            iNoteRecord.CodedNotes, noteRecord.Uid );

         // do we want to update the points table while we're here?
         if ( iUpdatePointsTable )
            mTable.Points.UpdateRecord( iPoint, false );

      }/* end method */


      /// <summary>
      /// Add a single note record (including a coded list) to the database
      /// and update the associated Point record to reflect changes to the 
      /// "TagChanged" table field.
      /// </summary>
      /// <param name="iNoteRecord">Populated notes object with codes</param>
      /// <param name="iPoint">Populated point object</param>
      /// <param name="iUpdatePointsTable">True to update points table</param>
      public void AddUserNote( UploadNoteRecord iNoteRecord, PointRecord iPoint )
      {
         // create an populate a note object
         NotesRecord noteRecord = new NotesRecord( true );

         noteRecord.PointUid = iPoint.Uid;
         noteRecord.Number = iPoint.Number;
         noteRecord.CollectionStamp = iNoteRecord.CollectionStamp;
         noteRecord.NoteType = iNoteRecord.NoteType;
         noteRecord.OperId = iNoteRecord.OperId;
         noteRecord.TextNote = iNoteRecord.TextNote;
         noteRecord.LastModified = iNoteRecord.LastModified;

         // add note record to the table
         mTable.Notes.AddRecord( noteRecord );

         // add each of the coded notes
         mTable.SavedCodedNotes.AddEnumRecords(
            iNoteRecord.CodedNotes, noteRecord.Uid );

         // now we need to ensure that the Notes flag is set ..
         iPoint.TagChanged =
            (byte) ( iPoint.TagChanged | (byte) FieldTypes.TagChanged.PointHasNotes );

         // and the note count incremented ..
         int noteCount = iPoint.GetNoteCount();
         iPoint.SetNoteCount( ++noteCount );

         // lastly we need to update the points table
         mTable.Points.UpdateRecord( iPoint, false );

      }/* end method */


      /// <summary>
      /// Synchronously update a single user note obect along with 
      /// its associated note codes. Note: This action will always
      /// revert the note type to Adhoc and reset the CollectionStamp
      /// to DateTime.MinValue
      /// </summary>
      /// <param name="iNoteRecord">Note record to update</param>
      /// <param name="iPointUid">Point reference</param>
      public void UpdateUserNote( UploadNoteRecord iNoteRecord, Guid iPointUid )
      {
         // create a new note object
         NotesRecord noteRecord = new NotesRecord( true );

         // populate all object fields
         noteRecord.Uid = iNoteRecord.GetNoteUid();
         noteRecord.CollectionStamp = iNoteRecord.CollectionStamp;
         noteRecord.NoteType = iNoteRecord.NoteType;
         noteRecord.OperId = iNoteRecord.OperId;
         noteRecord.PointUid = iPointUid;
         noteRecord.TextNote = iNoteRecord.TextNote;
         noteRecord.LastModified = iNoteRecord.LastModified;

         // add note record to the table
         mTable.Notes.UpdateRecord( noteRecord );

         // delete all existing codes associated with this note
         mTable.SavedCodedNotes.DeleteCodedNotes( noteRecord.Uid );

         // now add each of the updated notes
         mTable.SavedCodedNotes.AddEnumRecords(
            iNoteRecord.CodedNotes, noteRecord.Uid );

      }/* end method */


      /// <summary>
      /// Delete a particular Point Measurement and update the Tag Changed status 
      /// of the reference Point record.
      /// </summary>
      /// <param name="iPoint">Point object to reference and return</param>
      /// <param name="iCollectionStamp">measurement collection stamp</param>
      /// <param name="iUpdatePointsTable">is this method to update the points table</param>
      /// <returns></returns>
      public Boolean DeleteMeasurementNote( ref PointRecord iPoint,
         DateTime iCollectionStamp, Boolean iUpdatePointsTable )
      {
         // set initial result
         Boolean result = false;

         // get the UID for the target measurement note
         Guid noteUid = mTable.Notes.GetMeasurementNoteUid( iPoint.Uid, iCollectionStamp );

         //
         // provided the returned UID was not empty then delete both the note
         // and any associated note codes
         //
         if ( noteUid != Guid.Empty )
         {
            if ( mTable.Notes.DeleteSingleNote( noteUid ) )
            {
               result = mTable.SavedCodedNotes.DeleteCodedNotes( noteUid );
            }
         }

         // do we want to update the points table while we're here?
         if ( ( result ) && ( iUpdatePointsTable ) )
            mTable.Points.UpdateRecord( iPoint, false );

         // return the result
         return result;

      }/* end method */


      /// <summary>
      /// Returns a list of upload notes previously saved against this point
      /// </summary>
      /// <param name="iPoint">Reference point</param>
      /// <returns>Enumerated list of saved upload notes, else null</returns>
      public EnumUploadNotes GetNotesSavedForPoint( PointRecord iPoint )
      {
         EnumUploadNotes uploadNotes = null;

         // get an enumeration of notes saved for this point
         EnumNotes notes = mTable.Notes.GetRecords( iPoint.Uid );

         // only continue if there actually are some saved notes
         if ( notes.Count > 0 )
         {
            // get a list of all operator saved coded notes
            EnumSelectedCodedNotes codedNotes = mTable.SavedCodedNotes.GetAllRecords();

            // create the return enum object
            uploadNotes = new EnumUploadNotes();

            // process each note record
            foreach ( NotesRecord note in notes )
            {
               // create and populate a new selected note record
               SelectedNotesRecord selectedNotes = new SelectedNotesRecord( note );

               // get the coded notes assigned to this selected note
               selectedNotes.CodedNotes =
                   LinqQueries.GetCodedNotesForNoteUid( codedNotes, selectedNotes.Uid );

               // now add these notes to the current upload point
               AddPointNotes( selectedNotes, ref uploadNotes );
            }
         }

         // return all currently saved notes
         return uploadNotes;

      }/* end method */


      /// <summary>
      /// Adds a new upload note record to an enumerated list of upload notes
      /// </summary>
      /// <param name="iNotesRecord">note record to add</param>
      /// <param name="iUploadNotes">existing notes list</param>
      public void AddPointNotes( SelectedNotesRecord iNotesRecord,
         ref EnumUploadNotes iUploadNotes )
      {
         //
         // create a new "upload note record" object. This is a greatly simplified 
         // version of the full note record, and contains only those fields actually
         // required by the Mobile Device Server
         //
         UploadNoteRecord uploadNoteRecord = new UploadNoteRecord();

         // set time that the notes were added
         uploadNoteRecord.LastModified = iNotesRecord.LastModified;

         // add the freehand note text
         uploadNoteRecord.TextNote = iNotesRecord.TextNote;

         // add the operator Id
         uploadNoteRecord.OperId = iNotesRecord.OperId;

         // add the Note UID (we need this for editing a note!)
         uploadNoteRecord.SetNoteUid( iNotesRecord.Uid );

         // add the note type
         uploadNoteRecord.NoteType = iNotesRecord.NoteType;

         // add the collection stamp
         uploadNoteRecord.CollectionStamp = iNotesRecord.CollectionStamp;

         // add the coded notes assigned to the current point
         for ( int i = 0; i < iNotesRecord.CodedNotes.Count; ++i )
         {
            UploadCodedNote uploadNote = new UploadCodedNote();
            uploadNote.Code = iNotesRecord.CodedNotes.CodedNote[ i ].Code;
            uploadNote.Text = iNotesRecord.CodedNotes.CodedNote[ i ].Text;

            uploadNoteRecord.AddCodedNote( uploadNote );
         }

         // add this new upload note to the enumerated list of upload notes
         iUploadNotes.AddUploadNotesRecord( uploadNoteRecord );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public Asynchronous Notes Methods

      /// <summary>
      /// Adds a new Measurement Note to the database and update both the NoteCount 
      /// and TagChanged status of the reference Point record. These updates to the 
      /// PointRecord object will be executed and returned (via ref) prior to the 
      /// database actions, and so are ready for use immediately 
      /// </summary>
      /// <param name="iNoteRecord">Populated notes object with codes</param>
      /// <param name="iPoint">Populated point object</param>
      /// <param name="iUpdatePointsTable">True to update points table</param>
      public void AddUserNoteAsynch( UploadNoteRecord iNoteRecord,
         ref PointRecord iPoint, Boolean iUpdatePointsTable )
      {
         // create a notes state object
         NotesStateObject notesStateObject = new NotesStateObject();

         // now populate
         notesStateObject.Point = iPoint;
         notesStateObject.UpdatePointsTable = iUpdatePointsTable;
         notesStateObject.UploadNote = iNoteRecord;

         // reflect addition of note in the POINTS.TagChanged field
         AddNoteToTagChangedField( ref iPoint );

         // set the busy flag before spinning off a new thread
         this.RaiseBusyEventAsync( true );

         // Queue the task and data (we don't need any delay here)
         ThreadPool.QueueUserWorkItem( WriteNotesToSql, notesStateObject );

      }/* end method */


      /// <summary>
      /// Delete a particular Measurement Note and update both the NoteCount and 
      /// TagChanged status of the reference Point record. These updates to the 
      /// PointRecord object will be executed and returned (via ref) prior to the 
      /// database actions, and so are ready for use immediately 
      /// </summary>
      /// <param name="iNoteRecord">Populated notes object with codes</param>
      /// <param name="iPoint">Populated point object</param>
      /// <param name="iUpdatePointsTable">True to update points table</param>
      public void DeleteMeasurementNoteAsynch( ref PointRecord iPoint,
         DateTime iCollectionStamp, Boolean iUpdatePointsTable )
      {
         if ( iCollectionStamp != DateTime.MinValue )
         {
            // create a notes state object
            NotesStateObject notesStateObject = new NotesStateObject();

            // now populate
            notesStateObject.Point = iPoint;
            notesStateObject.UpdatePointsTable = iUpdatePointsTable;
            notesStateObject.CollectionStamp = iCollectionStamp;

            //
            // decrement the point's note count and update the
            // tag changed field as necessary
            //
            RemoveNoteFromTagChangedField( ref iPoint );

            // set the busy flag before spinning off a new thread
            this.RaiseBusyEventAsync( true );

            // Queue the task and data (we don't need any delay here)
            ThreadPool.QueueUserWorkItem( DeleteNoteFromSql, notesStateObject );
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Private Notes support Methods

      /// <summary>
      /// Update the Notes, Coded Notes and Point Table      
      /// </summary>
      /// <param name="iNotesStateObject">Notes state object</param>
      private void WriteNotesToSql( object iNotesStateObject )
      {
         // cast parameter object as notes state object 
         NotesStateObject notesStateObject = (NotesStateObject) iNotesStateObject;

         // create an populate a note object
         NotesRecord noteRecord = new NotesRecord( true );

         noteRecord.PointUid = notesStateObject.Point.Uid;
         noteRecord.Number = notesStateObject.Point.Number;
         noteRecord.CollectionStamp = notesStateObject.UploadNote.CollectionStamp;
         noteRecord.NoteType = notesStateObject.UploadNote.NoteType;
         noteRecord.OperId = notesStateObject.UploadNote.OperId;
         noteRecord.TextNote = notesStateObject.UploadNote.TextNote;
         noteRecord.LastModified = notesStateObject.UploadNote.LastModified;

         // add note record to the table
         mTable.Notes.AddRecord( noteRecord );

         // add each of the coded notes
         mTable.SavedCodedNotes.AddEnumRecords(
            notesStateObject.UploadNote.CodedNotes, noteRecord.Uid );

         // do we want to update the points table while we're here?
         if ( notesStateObject.UpdatePointsTable )
            mTable.Points.UpdateRecord( notesStateObject.Point, false );

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Update the Notes, Coded Notes and Point Table      
      /// </summary>
      /// <param name="iNotesStateObject">Notes state object</param>
      private void DeleteNoteFromSql( object iNotesStateObject )
      {
         // cast parameter object as notes state object 
         NotesStateObject notesStateObject = (NotesStateObject) iNotesStateObject;

         // set initial result
         Boolean result = false;

         // get the UID for the target measurement note
         Guid noteUid = mTable.Notes.GetMeasurementNoteUid(
            notesStateObject.Point.Uid,
            notesStateObject.CollectionStamp );

         //
         // provided the returned UID was not empty then delete both the note
         // and any associated note codes
         //
         if ( noteUid != Guid.Empty )
         {
            if ( mTable.Notes.DeleteSingleNote( noteUid ) )
            {
               result = mTable.SavedCodedNotes.DeleteCodedNotes( noteUid );
            }
         }

         // do we want to update the points table while we're here?
         if ( ( result ) && ( notesStateObject.UpdatePointsTable ) )
            mTable.Points.UpdateRecord( notesStateObject.Point, false );

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// This method appends the "point has notes" bit to the 
      /// POINTS.TagChanged field
      /// </summary>
      /// <param name="iPoint">reference Point object</param>
      public void AddNoteToTagChangedField( ref PointRecord iPoint )
      {
         // increment the current note count!
         iPoint.IncrementNoteCount();

         // update the reference point TagChanged field
         FieldTypes.TagChanged tagState = (FieldTypes.TagChanged) iPoint.TagChanged;

         if ( tagState == FieldTypes.TagChanged.Unchanged )
         {
            iPoint.TagChanged = (byte) FieldTypes.TagChanged.PointHasNotes +
                (byte) FieldTypes.TagChanged.Unchanged;
         }

         else if ( tagState == FieldTypes.TagChanged.PointTagChanged )
         {
            iPoint.TagChanged = (byte) FieldTypes.TagChanged.PointHasNotes +
                (byte) FieldTypes.TagChanged.PointTagChanged;
         }

      }/* end method */


      /// <summary>
      /// This method removes the "point has notes" bit from the 
      /// POINTS.TagChanged field
      /// </summary>
      /// <param name="iPoint">reference Point object</param>
      public void RemoveNoteFromTagChangedField( ref PointRecord iPoint )
      {
         // decrement the current note count
         iPoint.DecrementNoteCount();

         // if note count is zero then update the TagChanged field
         if ( iPoint.GetNoteCount() == 0 )
         {
            // update the reference point TagChanged field
            byte tagState = iPoint.TagChanged;

            // what is the byte mask for notes and tag changed?
            byte tagChangeAndNotes = (byte) ( FieldTypes.TagChanged.PointTagChanged &
               FieldTypes.TagChanged.PointHasNotes );

            // if point just has notes, then revert to unchanged
            if ( tagState == (byte) FieldTypes.TagChanged.PointHasNotes )
            {
               iPoint.TagChanged = (byte) FieldTypes.TagChanged.Unchanged;
            }

            // otherwise revert to changed
            else if ( tagState == tagChangeAndNotes )
            {
               iPoint.TagChanged = (byte) FieldTypes.TagChanged.PointTagChanged;
            }
         }

      }/* end method */


      /// <summary>
      /// This method appends the "node has notes" bit to the 
      /// NODE.Flags field. Note this method should be used with
      /// caution as @ptitude 5.0 does not support node based notes!
      /// </summary>
      /// <param name="iNode">reference Node object</param>
      public void AddNoteToNodeFlagsField( ref NodeRecord iNode )
      {
         // update the reference node TagChanged field
         FieldTypes.NodeFlagType flagState = (FieldTypes.NodeFlagType) iNode.Flags;


         if ( flagState == FieldTypes.NodeFlagType.Machine )
         {
            iNode.Flags = (byte) FieldTypes.NodeFlagType.NodeHasNotes +
                (byte) FieldTypes.NodeFlagType.Machine;
         }

         else if ( flagState == FieldTypes.NodeFlagType.NodeTagChanged )
         {
            iNode.Flags = (byte) FieldTypes.NodeFlagType.NodeHasNotes +
                (byte) FieldTypes.NodeFlagType.NodeTagChanged;
         }

         else if ( flagState == FieldTypes.NodeFlagType.Other )
         {
            iNode.Flags = (byte) FieldTypes.NodeFlagType.NodeHasNotes +
                (byte) FieldTypes.NodeFlagType.Other;
         }

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public "User Preference" support methods

      /// <summary>
      /// Get the populated user preferences object for a specified operator
      /// </summary>
      /// <param name="iOperatorID">operator index</param>
      /// <returns>preferences object or null</returns>
      public UserPreferencesObject GetUserPreferences( int iOperatorID )
      {
         // get the required record
         UserPreferencesRecord upr = 
            mTable.UserPreferences.GetRecord( iOperatorID );

         if ( upr != null )
         {
            return upr.GetPreferences();
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Add or update a User Preferences object
      /// </summary>
      /// <param name="iPreferences">populated preferences object</param>
      /// <param name="iOperatorID">operator index</param>
      /// <returns>true on success, else false</returns>
      public Boolean SetUserPreferences( UserPreferencesObject iPreferences, int iOperatorID )
      {
         // create a new preferences record
         UserPreferencesRecord upr = new UserPreferencesRecord();

         // add the preferences object
         upr.SetPreferences( iPreferences );

         // add the operator and set the changed flag true
         upr.OperatorId = iOperatorID;
         upr.Changed = true;

         // add or update the preferences table
         return mTable.UserPreferences.AddRecord( upr );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public Work Notification methods

      /// <summary>
      /// Gets an enumerated list of all CMMS Work Notification Records
      /// </summary>
      /// <returns>an enumerated list of all CMMS Work Notification Records</returns>
      public EnumCmmsWorkNotifications Get_All_WN_Records()
      {
         return mTable.WorkNotification.GetAllRecords( );
      }

      /// <summary>
      /// Get the Work Notification records for a given Machine
      /// </summary>
      /// <param name="iMachineUid">The Machine's Uid</param>
      /// <returns>List of CmmsWorkNotification records for the Machine</returns>
      public EnumCmmsWorkNotifications Get_WN_Records( Guid iMachineUid )
      {
         return mTable.WorkNotification.GetRecords( iMachineUid );

      }/* end method */


      /// <summary>
      /// Get the Work Notification record for a given ID
      /// </summary>
      /// <param name="iNotificationID">The Work Notification's ID</param>
      /// <returns>A CmmsWorkNotification</returns>
      public CmmsWorkNotification Get_WN_Record( int iNotificationID )
      {
         return mTable.WorkNotification.GetRecordFromWorkNotification( iNotificationID );

      }/* end method */


      /// <summary>
      /// Add a single Work Notification object. This method will overwrite an 
      /// existing record if the WorkNotificationID is the same
      /// </summary>
      /// <param name="iWorkNotification">populated CmmsWorkNotification object</param>
      /// <returns>false on exception raised</returns>
      public bool Add_WN_Record( CmmsWorkNotification iWorkNotification )
      {
         return mTable.WorkNotification.AddRecord( iWorkNotification );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Private Methods

      /// <summary>
      /// Identical to the public Connect() method, except that this
      /// method sets the connection from within a worker thread
      /// </summary>
      /// <param name="iPopulateResultSets">Pre-poulate ResultSets?</param>
      private void ConnectInThread( object iPopulateResultSets )
      {
         // reference the boolean parameter
         Boolean populateResultSets = (Boolean) iPopulateResultSets;

         // set the busy flag true
         RaiseBusyEventAsync( true );

         try
         {
            if ( mTable.TestConnection() )
            {
               if ( mPopulateTable == null )
                  mPopulateTable = new PopulateTable( mTable );

               if ( mClearTable == null )
                  mClearTable = new ClearTable( mTable );

               if ( mOperatorSet == null )
                  mOperatorSet = new UserLogin( mTable );

               if ( mBuildUpload == null )
                  mBuildUpload = new BuildUploadPacket( mTable );

               //
               // if required, populate the point and node object lists
               // As this method is already being executed asynchronously
               // we do not need to also do this asynchronously!
               //
               if ( populateResultSets )
               {
                  RefreshHierarchyData( false );
               }

               // configure how aggregate events are processed  
               PassThroughFeedbackEvents();
            }
         }
         finally
         {
            // reset the busy flag
            RaiseBusyEventAsync( false );
         }

      }/* end method */


      /// <summary>
      /// This method clears all hierarchy data from the LINQ objects,
      /// and then completely repopulates them from the database.
      /// </summary>
      /// <param name="iShowAsBusy">should the busy event flags be enabled?</param>
      private void RefreshHierarchyData( object iShowAsBusy )
      {
         // reference the boolean parameter
         Boolean showAsBusy = (Boolean) iShowAsBusy;

         // if we are to enable the busy flag - then do so
         if ( showAsBusy )
            RaiseBusyEventAsync( true );

         // refresh the POINTS data
         ClearAllPointsEnum();
         mPointsEnum = mTable.Points.GetAllRecords();

         // refresh the NODE data
         ClearAllNodesEnum();
         mNodesEnum = mTable.Node.GetAllRecords();

         // refresh the PROCESS data
         ClearAllProcessEnum();
         mProcessEnum = mTable.Process.GetAllRecords();

         // refresh the TEXT data
         ClearAllTextDataEnum();
         mTextDataEnum = mTable.TextData.GetAllRecords();

         // refresh the INSPECTION data
         ClearAllInspectionsEnum();
         mInspectionEnum = mTable.Inspection.GetAllRecords();

         // refresh the MCC data
         ClearAllMccEnum();
         mMccEnum = mTable.Mcc.GetAllRecords();

         // refresh the enumerated Coded Notes
         ClearAllCodedNotesEnum();
         mCodedNotes = mTable.CodedNote.GetAllRecords();

         // refresh all FFTPointRecord objects
         ClearAllFFTPointsEnum();
         mFFTPointsEnum = mTable.FFTPoint.GetAllRecords();

         // refresh all hierarchy message objects
         ClearAllMessagesEnum();
         mMessagesEnum = mTable.Message.GetAllRecords();

         // refresh all Conditional POINT objects
         ClearAllConditionalPointsEnum();
         mConditionalPoints = mTable.ConditionalPoint.GetAllRecords();

         // refresh all WN Corrective Action objects
         ClearAllCmmsCorrectiveActionsEnum( );
         mCmmsCorrectiveActions = mTable.CorrectiveAction.GetAllRecords( );

         // refresh all WN Priorities
         ClearAllCmmsPriorityEnum( );
         mCmmsPriorities = mTable.Priority.GetAllRecords( );

         // refresh all WN Problem Descriptions
         ClearAllCmmsProblemEnum( );
         mCmmsProblems = mTable.Problem.GetAllRecords( );

         // refresh all WN Work Types
         ClearAllCmmsWorkTypesEnum( );
         mCmmsWorkTypes = mTable.WorkType.GetAllRecords( );

          // just bin any existing 'cached' measurements 
         ClearMeasurementsEnum();

         // if we are to enable the busy flag - then do so
         if ( showAsBusy )
            RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Clear all TEMP Meaurements Enum records
      /// </summary>
      private void ClearMeasurementsEnum()
      {
         if ( mTempMeasurementsEnum != null )
         {
            for ( int i = 0; i < mTempMeasurementsEnum.Count; ++i )
               mTempMeasurementsEnum.Measurement[ i ] = null;
            mTempMeasurementsEnum.Measurement.Clear();
         }

         if ( mEnvMeasurementsEnum != null )
         {
            for ( int i = 0; i < mEnvMeasurementsEnum.Count; ++i )
               mEnvMeasurementsEnum.Measurement[ i ] = null;
            mEnvMeasurementsEnum.Measurement.Clear();
         }

         if ( mVelMeasurementsEnum != null )
         {
            for ( int i = 0; i < mVelMeasurementsEnum.Count; ++i )
               mVelMeasurementsEnum.Measurement[ i ] = null;
            mVelMeasurementsEnum.Measurement.Clear();
         }

      }/* end method */


      /// <summary>
      /// Clear all EnumMCC records
      /// </summary>
      private void ClearAllMccEnum()
      {
         if ( mMccEnum != null )
         {
            for ( int i=0; i < mMccEnum.Count; ++i )
               mMccEnum.MCC[ i ] = null;
            mMccEnum.MCC.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all Coded Notes records
      /// </summary>
      private void ClearAllCodedNotesEnum()
      {
         if ( mCodedNotes != null )
         {
            for ( int i=0; i < mCodedNotes.Count; ++i )
               mCodedNotes.CodedNote[ i ] = null;
            mCodedNotes.CodedNote.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumInspections records
      /// </summary>
      private void ClearAllInspectionsEnum()
      {
         if ( mInspectionEnum != null )
         {
            for ( int i=0; i < mInspectionEnum.Count; ++i )
               mInspectionEnum.Inspection[ i ] = null;
            mInspectionEnum.Inspection.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumProcess records
      /// </summary>
      private void ClearAllProcessEnum()
      {
         if ( mProcessEnum != null )
         {
            for ( int i=0; i < mProcessEnum.Count; ++i )
               mProcessEnum.Process[ i ] = null;
            mProcessEnum.Process.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumTextData records
      /// </summary>
      private void ClearAllTextDataEnum()
      {
         if ( mTextDataEnum != null )
         {
            for ( int i=0; i < mTextDataEnum.Count; ++i )
               mTextDataEnum.TextData[ i ] = null;
            mTextDataEnum.TextData.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumFFTPoint records
      /// </summary>
      private void ClearAllFFTPointsEnum()
      {
         if ( mFFTPointsEnum != null )
         {
            for ( int i=0; i < mFFTPointsEnum.Count; ++i )
               mFFTPointsEnum.FFTPoint[ i ] = null;
            mFFTPointsEnum.FFTPoint.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumMessage records
      /// </summary>
      private void ClearAllMessagesEnum()
      {
         if ( mMessagesEnum != null )
         {
            for ( int i=0; i < mMessagesEnum.Count; ++i )
               mMessagesEnum.Message[ i ] = null;
            mMessagesEnum.Message.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumConditionalPoint records
      /// </summary>
      private void ClearAllConditionalPointsEnum()
      {
         if ( mConditionalPoints != null )
         {
            for ( int i=0; i < mConditionalPoints.Count; ++i )
               mConditionalPoints.ConditionalPoint[ i ] = null;
            mConditionalPoints.ConditionalPoint.Clear();
         }
      }/* end method */
                 

      /// <summary>
      /// Clear all EnumCmmsCorrectiveActions records
      /// </summary>
      private void ClearAllCmmsCorrectiveActionsEnum()
      {
         if ( mCmmsCorrectiveActions != null )
         {
            for ( int i=0; i < mCmmsCorrectiveActions.Count; ++i )
               mCmmsCorrectiveActions.CorrectiveAction[i] = null;
            mCmmsCorrectiveActions.CorrectiveAction.Clear( );
         }
      }/* end method */

            
      /// <summary>
      /// Clear all EnumCmmsPriorities records
      /// </summary>
      private void ClearAllCmmsPriorityEnum()
      {
         if ( mCmmsPriorities != null )
         {
            for ( int i=0; i < mCmmsPriorities.Count; ++i )
               mCmmsPriorities.Priority[i] = null;
            mCmmsPriorities.Priority.Clear( );
         }
      }/* end method */

      
      /// <summary>
      /// Clear all EnumCmmsProblems records
      /// </summary>
      private void ClearAllCmmsProblemEnum()
      {
         if ( mCmmsProblems != null )
         {
            for ( int i=0; i < mCmmsProblems.Count; ++i )
               mCmmsProblems.Problem[i] = null;
            mCmmsProblems.Problem.Clear( );
         }
      }/* end method */
      
      
      /// <summary>
      /// Clear all EnumCmmsWorkTypes records
      /// </summary>
      private void ClearAllCmmsWorkTypesEnum()
      {
         if ( mCmmsWorkTypes != null )
         {
            for ( int i=0; i < mCmmsWorkTypes.Count; ++i )
               mCmmsWorkTypes.WorkType[i] = null;
            mCmmsWorkTypes.WorkType.Clear( );
         }
      }/* end method */

            
      /// <summary>
      /// Clear all EnumNode records
      /// </summary>
      private void ClearAllNodesEnum()
      {
         if ( mNodesEnum != null )
         {
            for ( int i=0; i < mNodesEnum.Count; ++i )
               mNodesEnum.NODE[ i ] = null;
            mNodesEnum.NODE.Clear();
         }
      }/* end method */


      /// <summary>
      /// Clear all EnumPoints records
      /// </summary>
      private void ClearAllPointsEnum()
      {
         if ( mPointsEnum != null )
         {
            for ( int i=0; i < mPointsEnum.Count; ++i )
               mPointsEnum.POINTS[ i ] = null;
            mPointsEnum.POINTS.Clear();
         }
      }/* end method */


      /// <summary>
      /// Add a single TEMPDATA measurement to the TEMP data table
      /// </summary>
      /// <param name="iPointRecord">Populate Point object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="iNoLastMeas">Number or measurements to retain</param>
      private void AddTempMeasurement( PointRecord iPointRecord, int iOperId, int iNoLastMeas )
      {
         //
         // add the newly collected value to the TEMPDATA measurement
         // table, and (assuming the update is successful) recursively
         // set the Alarm and 
         //

         // set the DataSaved flag in the current POINT object
         iPointRecord.DataSaved = true;

         // update the actual POINTS table
         mTable.Points.UpdateRecord( iPointRecord, true );

         // set the collection and alarm state flags
         SetCollectionAndAlertFlags( iPointRecord );

         // update the TEMPDATA table
         mTable.TempData.AppendRecord( iPointRecord, iOperId, iNoLastMeas );

      }/* end method */


      /// <summary>
      /// Add the scalar only MCD measurement values (temperature, velocity and 
      /// envelope acceleration)to their associated tables in the database
      /// </summary>
      /// <param name="iPointRecord">The parent PointRecord object</param>
      /// <param name="iNoLastMeas">Maximum number of historic measurements</param>
      private void AddScalarMCDMeasurements( PointRecord iPointRecord, int iNoLastMeas )
      {
      }/* end method */


      /// <summary>
      /// Add the FFT Channel records associated with this point.
      /// </summary>
      /// <param name="iPointRecord">The parent PointRecord object</param>
      /// <param name="iFFTChannels">enumerated list of FFT Channels</param>
      private void AddFFTChannels( PointRecord iPointRecord, EnumBaseFFTChannels iFFTChannels )
      {
         //
         // Is there an FFT associated with the current Point object? 
         // If yes then we'll need to add the FFT Channel to the database
         //
         FFTPointRecord fftPoint = mTable.FFTPoint.GetRecordFromPointUid( iPointRecord.Uid );

         // if yes then add the channel record
         if ( fftPoint != null )
         {
            mFFTChannelsEnum = PopulateFFTChannelsEnum( iPointRecord, iFFTChannels, fftPoint );

            // synchronously write to the database
            foreach ( FFTChannelRecord fftChannelRecord in mFFTChannelsEnum )
            {
               mTable.FFTChannel.AddRecord( fftChannelRecord );
            }
         }

      }/* end method */


      /// <summary>
      /// Upscales a series of FFTChannelBase objects into a full 
      /// FFTChannel object with FFTPoint header
      /// </summary>
      /// <param name="iPointRecord">Populated Point record</param>
      /// <param name="iFFTChannels">Series of FFT channel objects</param>
      /// <param name="fftPoint">Populated FFTPoint object</param>
      private EnumFFTChannelRecords PopulateFFTChannelsEnum(
         PointRecord iPointRecord,
         EnumBaseFFTChannels iFFTChannels,
         FFTPointRecord fftPoint )
      {
         // default fft channel object
         EnumFFTChannelRecords fftChannels = null;

         // only continue if we actually have channel data
         if ( iFFTChannels != null )
         {
            // clear any existing FFT channel objects
            fftChannels = new EnumFFTChannelRecords();

            // reset the FFT channel enumerator
            iFFTChannels.Reset();

            // process each of the FFT Channel base objects
            foreach ( FFTChannelBase channelBase in iFFTChannels )
            {
               // create a new FFT Channel object to populate
               FFTChannelRecord channelRecord = new FFTChannelRecord();

               // populate the generic object fields
               channelRecord.Uid = System.Guid.NewGuid();
               channelRecord.FFTPointUid = fftPoint.Uid;
               channelRecord.PointUid = iPointRecord.Uid;
               channelRecord.PointHostIndex = iPointRecord.PointHostIndex;
               channelRecord.ChannelId = channelBase.ChannelId;
               channelRecord.SpecNum = channelBase.SpecNum;
               channelRecord.Timestamp = channelBase.Timestamp;
               channelRecord.SpeedOrPPF = fftPoint.DefaultRunningSpeed;
               channelRecord.Factor = channelBase.Factor;
               channelRecord.LowFreq = channelBase.LowFreq;

               // populate those fields specific to Envelope Acceleration
               if ( channelBase.ChannelId == (byte) FieldTypes.FFTChannelId.EnvAccl )
               {
                  channelRecord.NumberOfLines = fftPoint.EnvNumberOfLines;
                  channelRecord.NumberOfAverages = fftPoint.EnvAverages;
                  channelRecord.Windowing = fftPoint.EnvWindow;
                  channelRecord.DataType = fftPoint.EnvType;
                  channelRecord.HiFreq = fftPoint.EnvFreqMax;
               }

               // populate those fields specific to Velocity
               else if ( channelBase.ChannelId == (byte) FieldTypes.FFTChannelId.Velocity )
               {
                  channelRecord.NumberOfLines = fftPoint.VelNumberOfLines;
                  channelRecord.NumberOfAverages = fftPoint.VelAverages;
                  channelRecord.Windowing = fftPoint.VelWindow;
                  channelRecord.DataType = fftPoint.VelType;
                  channelRecord.HiFreq = fftPoint.VelFreqMax;
               }

               // populate those fields specific to acceleration
               else if ( channelBase.ChannelId == (byte) FieldTypes.FFTChannelId.Acceleration )
               {
                  channelRecord.NumberOfLines = fftPoint.AcclNumberOfLines;
                  channelRecord.NumberOfAverages = fftPoint.AcclAverages;
                  channelRecord.Windowing = fftPoint.AcclWindow;
                  channelRecord.DataType = fftPoint.AcclType;
                  channelRecord.HiFreq = fftPoint.AcclFreqMax;
               }

               // add the collected data lines
               channelRecord.DataLines = channelBase.DataLines;

               // add the WMCD Serial Number
               channelRecord.SerialNo = channelBase.SerialNo;

               // append fft channel object to the channels list
               fftChannels.AddFFTChannelRecord( channelRecord );
            }
         }

         // reference the FFT channels object
         return fftChannels;

      }/* end method */


      /// <summary>
      /// This method sets both the "collected checkmark" and the alarm state
      /// of the current points (machine) parent node, as well as recursively
      /// setting the same fields in subsequent ancestor nodes - all the way 
      /// to the top of the tree (ROUTE or HIERARCHY nodes)
      /// </summary>
      /// <param name="iPointRecord">Current Point Object</param>
      private void SetCollectionAndAlertFlags( PointRecord iPointRecord )
      {
         // set some initial alarm and node state flags
         FieldTypes.NodeAlarmState alarmState = FieldTypes.NodeAlarmState.None;
         FieldTypes.CollectionCheck checkMark = FieldTypes.CollectionCheck.None;

         // get a list of the parent POINTS object's siblings
         EnumPoints points = GetPointsEnum( iPointRecord.NodeUid );

         //
         // how many of these siblings have been checked and what is
         // the worst Alarm state amongst them?
         //
         int count = 0;
         foreach ( PointRecord point in points )
         {
            // increment datasaved count
            if ( point.DataSaved )
               ++count;

            // set the new alarm state for the parent node
            if ( point.AlarmState > (byte) alarmState )
            {
               alarmState = (FieldTypes.NodeAlarmState) point.AlarmState;
            }
         }

         // what checkmark should we assign to the parent machine?
         checkMark = MachineCheckedState( points.Count, count );

         // process the machine node and get its parent Uid
         Guid parentNode = mTable.Node.RefreshNodeState(
             iPointRecord.NodeUid,
             checkMark,
             alarmState );

         //
         // recurse up the chain! Note: for this operation only the 
         // collection and alarm states are set - with the alarm state
         // being based on OR logic (worst case preveails) while the
         // collection state is based on AND logic
         //
         do
         {
            if ( parentNode != PacketBase.NODE_NULL_GUID )
            {
               //
               // get an enumerated list of sibling nodes from the NODE table
               // (i.e. nodes that have a common parent Uid)
               //
               EnumNodes nodes = mTable.Node.GetSiblings( parentNode );

               //
               // check the status of each node and revise the current check
               // state based on the collection and alarm conditions
               //
               foreach ( NodeRecord node in nodes )
               {
                  checkMark = CheckFlagLogic( checkMark,
                      (FieldTypes.CollectionCheck) node.CollectionCheck );

                  // set the new alarm state for the parent node
                  if ( node.AlarmState > (byte) alarmState )
                  {
                     alarmState = (FieldTypes.NodeAlarmState) node.AlarmState;
                  }
               }

               //
               // set and current node and return its parent GUID if the
               // parent also needs to be updated
               //
               parentNode = mTable.Node.RefreshNodeState(
                   parentNode, checkMark, alarmState );
            }
            else
            {
               break;
            }

         } while ( parentNode != PacketBase.ROOT_NODE_GUID );

      }/* end method */


      /// <summary>
      /// Reference the current POINTS record DataSaved count against the
      /// total number of points available, and return the CollectionCheck
      /// status flag.
      /// </summary>
      /// <param name="iEnumCount">Total number of machine points</param>
      /// <param name="count">the current DataSaved count</param>
      /// <returns></returns>
      private FieldTypes.CollectionCheck MachineCheckedState( int iEnumCount, int count )
      {
         FieldTypes.CollectionCheck checkMark;

         // what should the parent machine check mark be?
         if ( count == iEnumCount )
         {
            checkMark = FieldTypes.CollectionCheck.All;
         }
         else if ( count < iEnumCount )
         {
            checkMark = FieldTypes.CollectionCheck.Some;
         }
         else
         {
            checkMark = FieldTypes.CollectionCheck.None;
         }

         return checkMark;

      }/* end method */


      /// <summary>
      /// Logic engine for comparing one CheckFlag state against another
      /// </summary>
      /// <param name="iCurrent">Current CheckFlag state</param>
      /// <param name="iPrevious">CheckFlag to reference</param>
      /// <returns>Logic result</returns>
      private FieldTypes.CollectionCheck CheckFlagLogic(
          FieldTypes.CollectionCheck iCurrent,
          FieldTypes.CollectionCheck iPrevious )
      {
         // when both states are NONE then the return will be NONE
         if ( ( iCurrent == FieldTypes.CollectionCheck.None ) &&
             ( iPrevious == FieldTypes.CollectionCheck.None ) )
         {
            return FieldTypes.CollectionCheck.None;
         }

         // only when both states are ALL will the return be ALL
         else if ( ( iCurrent == FieldTypes.CollectionCheck.All ) &&
             ( iPrevious == FieldTypes.CollectionCheck.All ) )
         {
            return FieldTypes.CollectionCheck.All;
         }

         // for all other conditions the return will be SOME
         else
         {
            return FieldTypes.CollectionCheck.Some;
         }

      }/* end method */


      /// <summary>
      /// Returns the first non-alarmed inspection choice
      /// </summary>
      /// <param name="iSetup">populated inspection record</param>
      /// <returns>first valid non-alarmed inspection choice</returns>
      private FieldTypes.InspectionChoices GetFirstNonAlarmInspection( InspectionRecord iSetup )
      {
         // reference the "no alarm" bit
         byte noAlarm = (byte) FieldTypes.InspectionAlarms.None;

         if ( iSetup.AlarmType1 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice1;
         }

         if ( iSetup.AlarmType2 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice2;
         }

         if ( iSetup.AlarmType3 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice3;
         }

         if ( iSetup.AlarmType4 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice4;
         }

         if ( iSetup.AlarmType5 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice5;
         }

         if ( iSetup.AlarmType6 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice6;
         }

         if ( iSetup.AlarmType7 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice7;
         }

         if ( iSetup.AlarmType8 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice8;
         }

         if ( iSetup.AlarmType9 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice9;
         }

         if ( iSetup.AlarmType10 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice10;
         }

         if ( iSetup.AlarmType11 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice11;
         }

         if ( iSetup.AlarmType12 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice12;
         }

         if ( iSetup.AlarmType13 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice13;
         }

         if ( iSetup.AlarmType14 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice14;
         }

         if ( iSetup.AlarmType15 == noAlarm )
         {
            return FieldTypes.InspectionChoices.Choice15;
         }

         // if we've reached this point - then return a fail!
         return FieldTypes.InspectionChoices.NoneSet;

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Public Asynch Methods

      /// <summary>
      /// Add a new measurement to database
      /// </summary>
      /// <param name="iPointRecord">Populated point objrct</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">maximum number of measurements for this point</param>
      public void AddMeasurementAsync( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas )
      {
         // what is the process type associated with this point?
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         switch ( processType )
         {
            //for MCD values
            case FieldTypes.ProcessType.MCD:
               AddScalarMeasurementAsync( iPointRecord, iOperId, iNoLastMeas, false );
               break;

            // for all other (process) values
            default:
               AddSingleMeasurementAsync( iPointRecord, iOperId, iNoLastMeas, false );
               break;
         }

      }/* end method */


      /// <summary>
      /// Overload method to manage the processing of FFT Channel data
      /// </summary>
      /// <param name="iPointRecord">Parent Point object</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Number of previous measurements to save</param>
      /// <param name="iFFTChannelData">Enumerated list of channel data objects</param>
      public void AddMeasurementAsync( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas, EnumBaseFFTChannels iFFTChannelData )
      {
         // get the process type associated with this point
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         // confirm the process type is MCD only and set the overwrite flag false!
         if ( processType == FieldTypes.ProcessType.MCD )
         {
            AddScalarAndFFTMeasurementAsync(
               iPointRecord,
               iOperId,
               iNoLastMeas,
               iFFTChannelData,
               false );
         }

      }/* end method */


      /// <summary>
      /// Asynchronously overwrites the last measurement for the Point Record
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Number of measurements to retain</param>
      public void OverwriteMeasurementAsynch( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas )
      {
         // what is the process type associated with this point?
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         switch ( processType )
         {
            //for MCD values - set overwrite flag true!
            case FieldTypes.ProcessType.MCD:
               AddScalarMeasurementAsync( iPointRecord, iOperId, iNoLastMeas, true );
               break;

            // for all other (process) values - set overwrite flag true!
            default:
               AddSingleMeasurementAsync( iPointRecord, iOperId, iNoLastMeas, true );
               break;
         }

      }/* end method */


      /// <summary>
      /// Asynchronously overwrites the last measurement for the Point Record
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Number of measurements to retain</param>
      /// <param name="iFFTChannelData">Enumerated list of channel data objects</param>
      public void OverwriteMeasurementAsynch( PointRecord iPointRecord, int iOperId, int iNoLastMeas,
                                 EnumBaseFFTChannels iFFTChannelData )
      {
         // get the process type associated with this point
         FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

         // confirm the process type is MCD only and set the overwrite flag true!
         if ( processType == FieldTypes.ProcessType.MCD )
         {
            AddScalarAndFFTMeasurementAsync(
               iPointRecord,
               iOperId,
               iNoLastMeas,
               iFFTChannelData,
               true );
         }

      }/* end method */


      /// <summary>
      /// Updates a POINTS table record following a 'one off' change to the
      /// associated PointRecord object (i.e. setting the POINTS.Skipped
      /// field to "MACHINE NOT OPERATING" or "COLLECTION FAILED".
      /// </summary>
      /// <param name="iPointRecord">Populated point objrct</param>
      /// <param name="iUpdateCompliance">Whether or not to update compliance</param>
      public void UpdatePointAsync( PointRecord iPointRecord, bool iUpdateCompliance )
      {
         // Queue the task and data (we don't need any delay here)
         PointUpdateStateObject state = 
            new PointUpdateStateObject( iPointRecord, iUpdateCompliance );

         // set the busy flag before spinning off a new thread
         this.RaiseBusyEventAsync( true );

         ThreadPool.QueueUserWorkItem( UpdateSqlPointAsynch, state );

      }/* end method */


      /// <summary>
      /// Updates a Node table record following a 'one off' change to the
      /// associated NodeRecord object (i.e. setting the Location Method,
      /// Location Tag, Flags, etc.)
      /// </summary>
      /// <param name="iNodeRecord">Populated node object</param>
      public void UpdateNodeAsync( NodeRecord iNodeRecord )
      {
         // set the busy flag before spinning off a new thread
         this.RaiseBusyEventAsync( true );

         // Queue the task and data (we don't need any delay here)
         ThreadPool.QueueUserWorkItem( UpdateSqlNodeAsynch, iNodeRecord );

      }/* end method */


      /// <summary>
      /// Set the state of a machine to OK, the collection state of all
      /// child points to "AutoCollectedMachineOK" and the DataSaved flag
      /// to TRUE. This method also allows for the option to set all 
      /// available Inspection points to their first "non-alarmed" result.
      /// </summary>
      /// <param name="iNode">Reference Machine to set OK</param>
      /// <param name="iOperId">Current Operator Id</param>
      /// <param name="iUpdateInspections">Should we set the inspection points?</param>
      /// <param name="iNoLastMeas">maximum number of measurements for this point</param>
      public void MachineOKAsynch( ref NodeRecord iNode, int iOperId,
            Boolean iUpdateInspections, int iNoLastMeas )
      {
         // reference the machine bit mask
         byte machine = (Byte) FieldTypes.NodeFlagType.Machine;

         // lets make sure we're actually dealing with a machine here!
         if ( ( iNode.Flags & machine ) == machine )
         {
            // create a new 'machine OK' state object
            MachineOKStateObject macOKStateObject = new MachineOKStateObject();

            // set the operator ID
            macOKStateObject.OperatorId = iOperId;

            // reference the number of historic measurements we've to retain
            macOKStateObject.NoLastMeasurements = iNoLastMeas;

            // we need to set the collection and alarm flags
            iNode.CollectionCheck = (byte) FieldTypes.CollectionCheck.All;
            iNode.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

            // and also do this for the local NodesEnum cache!
            NodeRecord node = LinqQueries.GetNodeRecord( NodesEnum, iNode.Uid );
            node.CollectionCheck = (byte) FieldTypes.CollectionCheck.All;
            node.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

            // now lets get a list of points for this machine
            EnumPoints points = LinqQueries.GetChildPoints( PointsEnum, iNode.Uid );

            // reset the list pointer
            points.Reset();

            // now lets process each point object
            foreach ( PointRecord point in points )
            {
               // the point is being modified
               point.LastModified = PacketBase.DateTimeNow;

               // set the alarm state
               point.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

               // set the skipped flag to 'Machine not operating'
               point.Skipped = (byte) FieldTypes.SkippedType.AutoCollectedMachineOk;

               // the datasaved flag needs to be set true
               point.DataSaved = true;

               //
               // for an Inspection Point we need to set the first
               // clear value, while for process values we simply 
               // write a null measurement to the database. This
               // method also returns the overall node alarm state
               //
               iNode.AlarmState = AddMachineOKMeasurements( 
                  iUpdateInspections, 
                  iNode.AlarmState, 
                  point );

               // reference the alarm state to the cached node
               node.AlarmState = iNode.AlarmState;
            }

            // update the node's ancestor collection flags and return those ancestors
            EnumNodes ancestors = UpdateAlarmAndCheckFlags( iNode );

            // now add the whole nodes list to the state object
            macOKStateObject.NodesEnum = ancestors;

            // add the node record to the state object
            macOKStateObject.NodeRecord = iNode;

            // populate the state object
            macOKStateObject.PointsEnum = points;

            // set the busy flag
            this.RaiseBusyEventAsync( true );

            // Queue the task and data (we don't need any delay here)
            ThreadPool.QueueUserWorkItem( WriteMachineOkToSql, macOKStateObject );
         }

      }/* end method */


      /// <summary>
      /// This method will stamp a 'Machine not operating' note against
      /// each and every point in a given machine. It will also set each
      /// point's 'skipped' flag to MachineNotOperating
      /// </summary>
      /// <param name="iNode">reference node object</param>
      /// <param name="iOperId">current operator ID</param>
      /// <param name="iNoLastMeas">maximum number of measurements for this point</param>
      public void MachineNotOperatingAsynch( ref NodeRecord iNode, int iOperId, 
         int iNoLastMeas )
      {
         // reference the machine bit mask
         byte machine = (Byte) FieldTypes.NodeFlagType.Machine;

         // lets make sure we're actually dealing with a machine here!
         if ( ( iNode.Flags & machine ) == machine )
         {
            // create a new 'not operating' state object
            NotOperatingStateObject nopStateObject = new NotOperatingStateObject();

            // set the operator ID
            nopStateObject.OperatorId = iOperId;

            // reference the number of historic measurements we've to retain
            nopStateObject.NoLastMeasurements = iNoLastMeas;

            // we need to set the collection and alarm flags
            iNode.CollectionCheck = (byte) FieldTypes.CollectionCheck.All;
            iNode.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

            // and also do this for the local NodesEnum cache!
            NodeRecord node = LinqQueries.GetNodeRecord( NodesEnum, iNode.Uid );
            node.CollectionCheck = (byte) FieldTypes.CollectionCheck.All;
            node.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

            // update the node's ancestor collection flags and return those ancestors
            EnumNodes ancestors = UpdateAlarmAndCheckFlags( iNode );

            // now add the whole nodes list to the state object
            nopStateObject.NodesEnum = ancestors;

            // add the node record to the state object
            nopStateObject.NodeRecord = iNode;

            // now lets get a list of points for this machine
            EnumPoints points = LinqQueries.GetChildPoints( PointsEnum, iNode.Uid );

            // reset the list pointer
            points.Reset();

            EnumNotes notesEnum = new EnumNotes();
            EnumSelectedCodedNotes codedNotesEnum = new EnumSelectedCodedNotes();

            // now lets process each point object
            foreach ( PointRecord point in points )
            {
               // the point is being modified
               point.LastModified = PacketBase.DateTimeNow;

               // we need to flag that the point has notes
               point.TagChanged = (byte) FieldTypes.TagChanged.PointHasNotes;

               // set the alarm state
               point.AlarmState = (byte) FieldTypes.NodeAlarmState.None;

               // set the note count to one
               point.SetNoteCount( point.GetNoteCount() + 1 );

               // set the skipped flag to 'Machine not operating'
               point.Skipped = (byte) FieldTypes.SkippedType.MachineNotOperating;

               // the datasaved flag needs to be set true
               point.DataSaved = true;

               // add the "Machine Not Running" note
               AddNotRunningNote( iOperId, notesEnum, codedNotesEnum, point );

               // create and populate a new measurement object
               AddNullMeasurement( point );
            }

            // populate the state object
            nopStateObject.PointsEnum = points;
            nopStateObject.NotesEnum = notesEnum;
            nopStateObject.CodedNotesEnum = codedNotesEnum;

            // set the busy flag
            this.RaiseBusyEventAsync( true );

            // Queue the task and data (we don't need any delay here)
            ThreadPool.QueueUserWorkItem( WriteNotOperatingToSql, nopStateObject );
         }

      }/* end method */


      /// <summary>
      /// Conditionally skip Points and update it's parent accordingly.
      /// NOTE: All POINTs should be from the same Machine
      /// </summary>
      /// <param name="iNode">EnumPoints to skip</param>
      public void CondSkipPointsAsynch( EnumPoints iPoints, int iOperId, 
         int iNoLastMeas )
      {
         // if we don't have any points, return
         if ( ( iPoints == null ) || ( iPoints.POINTS.Count == 0 ) )
         {
            return;
         }

         // process each point object, setting point to skipped
         foreach ( PointRecord point in iPoints )
         {
            // the point is being modified
            point.LastModified = PacketBase.DateTimeNow;

            // set the skipped flag to 'Condition Failed'
            point.Skipped = (byte) FieldTypes.SkippedType.ConditionFailed;

            // set the datasaved flag
            point.DataSaved = true;

            // set the alarm state to none
            point.AlarmState = (byte)FieldTypes.NodeAlarmState.None;

            // create and populate a new measurement object
            AddNullMeasurement( point );

            // update all alarm and check flag states
            UpdateAlarmAndCheckFlags( point );
         }

         // create a new 'condionally skipped' object
         CondSkipStateObject skipObject = new CondSkipStateObject();

         // set the operator ID
         skipObject.OperatorId = iOperId;

         // set the maximum last measurement count
         skipObject.NoLastMeasurements = iNoLastMeas;

         //
         // get the parent of the POINTs, we just use index 0's NodeUID, 
         // should be the same for all POINTs here
         //
         NodeRecord parent = LinqQueries.GetNodeRecord( this.NodesEnum, iPoints.POINTS[ 0 ].NodeUid );

         // update the node's ancestor collection flags and return those ancestors
         EnumNodes nodes = UpdateAlarmAndCheckFlags( parent );

         // now add the whole nodes list to the state object
         skipObject.NodesEnum = nodes;

         // and all the points
         skipObject.PointsEnum = iPoints;

         // set the busy flag before spinning off a new thread
         this.RaiseBusyEventAsync( true );

         // Queue the task and data (we don't need any delay here)
         ThreadPool.QueueUserWorkItem( WriteConditionSkippedToSql, skipObject );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Add Non Collection Measurements and Notes

      /// <summary>
      /// This method will create a null point measurement with the
      /// status set to "MachineAutoCollectedOK". If the point is an
      /// inspection type and the "Set Inspections" flag is set, then
      /// a new "real" inspection will be collected.
      /// </summary>
      /// <param name="iSetInspections">Are inspections to be set</param>
      /// <param name="iNodeAlarm">Current node alarm status</param>
      /// <param name="iPoint">Point reference</param>
      /// <returns>New node alarm status</returns>
      private byte AddMachineOKMeasurements( 
         Boolean iSetInspections,
         byte iNodeAlarm,
         PointRecord iPoint )
      {
         byte nodeAlarm = iNodeAlarm;
         
         // do we want to add some dummy inspection values?
         if ( iSetInspections )
         {
            // is this an inspection point?
            if ( ( iPoint.ProcessType == (byte) FieldTypes.ProcessType.SingleSelectInspection ) |
               ( iPoint.ProcessType == (byte) FieldTypes.ProcessType.MultiSelectInspection ) )
            {
               AddDummyInspectionMeasurement( iPoint );
               if ( iPoint.AlarmState > nodeAlarm )
               {
                  nodeAlarm = iPoint.AlarmState;
               }
            }
            else
            {
               AddNullMeasurement( iPoint );
            }
         }
         else
         {
            AddNullMeasurement( iPoint );
         }

         return nodeAlarm;

      }/* end method */


      /// <summary>
      /// Add a null measurement to the TEMPDATA table
      /// </summary>
      /// <param name="point">Point reference</param>
      private static void AddNullMeasurement( PointRecord iPoint )
      {
         // create and populate a new measurement object
         PointMeasurement temp = new PointMeasurement();
         temp.LocalTime = iPoint.LastModified;
         temp.Value = float.NaN;

         // add to the point
         iPoint.TempData.AddMeasurement( temp );

      }/* end method */


      /// <summary>
      /// Add a "set" Inspection measurement to the TEMPDATA table
      /// </summary>
      /// <param name="point">Point reference</param>
      private void AddDummyInspectionMeasurement( PointRecord iPoint )
      {
         // get the points inspection setup record
         InspectionRecord setup = 
                     LinqQueries.GetPointsInspectionRecord(
            InspectionEnum,
            iPoint.Uid );

         // get the first non-alarmed result
         FieldTypes.InspectionChoices choice = 
                     GetFirstNonAlarmInspection( setup );

         // make sure we selected something!
         if ( choice != FieldTypes.InspectionChoices.NoneSet )
         {
            // create and populate a new measurement object
            PointMeasurement temp = new PointMeasurement();
            temp.LocalTime = iPoint.LastModified;
            temp.Value = (int) choice;

            // update the 'clear' status
            SetInspectionAlarmFlags( iPoint, temp, setup );

            // add to the point
            iPoint.TempData.AddMeasurement( temp );
         }

      }/* end method */


      /// <summary>
      /// Add a "Machine Not Running" compliance note to a point
      /// </summary>
      /// <param name="iOperId">Current operator Id</param>
      /// <param name="iNotesEnum">Available notes object</param>
      /// <param name="iCodedNotesEnum">List of coded notes</param>
      /// <param name="iPoint">Point reference</param>
      private void AddNotRunningNote( int iOperId,EnumNotes iNotesEnum,
         EnumSelectedCodedNotes iCodedNotesEnum, PointRecord iPoint )
      {
         // create and populate a new note object
         NotesRecord note = new NotesRecord( true );
         note.NoteType = (byte) FieldTypes.NoteType.Compliance;
         note.Number = iPoint.Number;
         note.OperId = iOperId;
         note.PointUid = iPoint.Uid;
         note.CollectionStamp = PacketBase.DateTimeNow;
         note.LastModified = note.CollectionStamp;

         // add to the notes list
         iNotesEnum.NOTE.Add( note );

         // create and populate a new coded note record
         CodedNoteRecord cNote = new CodedNoteRecord();

         // create a new NOP coded note
         SelectedCodedNote nopCode = new SelectedCodedNote();
         nopCode.Code = CodedNotesEnum.CodedNote[ 0 ].Code;
         nopCode.Text = CodedNotesEnum.CodedNote[ 0 ].Text;
         nopCode.NoteUid = note.Uid;

         // add to the selected coded notes list
         iCodedNotesEnum.AddCodedNoteRecord( nopCode );

      }/* end method */

      #endregion

      //----------------------------------------------------------------------------

      #region Private Asynch Methods

      /// <summary>
      /// Write all of the "Machine Not Operating" objects to the database
      /// </summary>
      /// <param name="iNotOperatingObject"></param>
      private void WriteNotOperatingToSql( object iNotOperatingObject )
      {
         // cast parameter object as not operating state object
         NotOperatingStateObject stateObject = 
            (NotOperatingStateObject) iNotOperatingObject;

         if ( stateObject != null )
         {
            // update the node table
            mTable.Node.UpdateRecords( stateObject.NodesEnum );

            // update the Points Table
            UpdatePointsTable( stateObject.PointsEnum, true );

            // update the measurements table
            UpdateMeasurementsTable( 
               stateObject.PointsEnum, 
               stateObject.OperatorId, 
               stateObject.NoLastMeasurements );

            // update the Notes table
            UpdateNotesTable( stateObject );

            // update the Coded Notes table
            UpdateCodedNotesTable( stateObject );
         }

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Write all of the "Machine OK" objects to the database 
      /// </summary>
      /// <param name="iSender">populated (untyped) MachineOKStateObject object</param>
      private void WriteMachineOkToSql( object iSender )
      {
         // cast sender as state object
         MachineOKStateObject stateObject = (MachineOKStateObject) iSender;
         
         if ( stateObject != null )
         {
            // update the node table
            mTable.Node.UpdateRecords( stateObject.NodesEnum );

            // update the Points Table
            UpdatePointsTable( stateObject.PointsEnum, true );

            // update the measurements table
            UpdateMeasurementsTable(
               stateObject.PointsEnum,
               stateObject.OperatorId,
               stateObject.NoLastMeasurements );
         }

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Write the "Machine Conditionally Skipped" object to the database 
      /// </summary>
      /// <param name="iCondSkipObject">The Conditionally Skipped
      /// object</param>
      private void WriteConditionSkippedToSql( object iCondSkipObject )
      {
         // cast parameter object as not operating state object
         CondSkipStateObject stateObject = 
            iCondSkipObject as CondSkipStateObject;

         if ( stateObject != null )
         {
            // update the node table
            mTable.Node.UpdateRecords( stateObject.NodesEnum );

            // update the Points Table
            UpdatePointsTable( stateObject.PointsEnum, true );

            // update the measurements table
            UpdateMeasurementsTable(
               stateObject.PointsEnum,
               stateObject.OperatorId,
               stateObject.NoLastMeasurements );
         }

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Update the Points table in a sequence of scheduled
      /// batch writes - with each batch limited to a fixed
      /// number of records!
      /// </summary>
      /// <param name="iPoints">Enumeration of point records</param>
      /// <param name="iUpdateCompliance">Whether or not to update compliance</param>
      private void UpdatePointsTable( EnumPoints iPoints, bool iUpdateCompliance )
      {
         int pointIndex = 0;
         EnumPoints points = new EnumPoints();

         while ( pointIndex < iPoints.Count )
         {
            points.POINTS.Clear();
            for ( int i=0; i < MAX_BATCH_RECORDS; ++i )
            {
               if ( pointIndex < iPoints.Count )
               {
                  points.POINTS.Add( iPoints.POINTS[ pointIndex ] );
               }
               ++pointIndex;
            }
            mTable.Points.UpdateRecords( points, iUpdateCompliance );
         }

         points.POINTS.Clear();

      }/* end method */


      /// <summary>
      /// Update the TEMP table in a sequence of scheduled
      /// batch writes - with each batch limited to a fixed
      /// number of records!
      /// </summary>
      /// <param name="iPoints">Enumeration of points</param>
      /// <param name="iOperId">current Operator</param>
      /// <param name="iNoLastMeas">Number of measurements to write</param>
      private void UpdateMeasurementsTable( EnumPoints iPoints, int iOperId, int iNoLastMeas )
      {
         int pointIndex = 0;
         EnumPoints points = new EnumPoints();

         while ( pointIndex < iPoints.Count )
         {
            points.POINTS.Clear();
            for ( int i=0; i < MAX_BATCH_RECORDS; ++i )
            {
               if ( pointIndex < iPoints.Count )
               {
                  points.POINTS.Add( iPoints.POINTS[ pointIndex ] );
               }
               ++pointIndex;
            }

            mTable.TempData.AppendEnumRecords(
               points,
               iOperId,
               iNoLastMeas );
         }

         points.POINTS.Clear();

      }/* end method */


      /// <summary>
      /// Update the Notes table in a sequence of scheduled
      /// batch writes - with each batch limited to a fixed
      /// number of records!
      /// </summary>
      /// <param name="stateObject">NOP state object</param>
      private void UpdateNotesTable( NotOperatingStateObject stateObject )
      {
         int index = 0;
         EnumNotes notes = new EnumNotes();

         while ( index < stateObject.NotesEnum.Count )
         {
            notes.NOTE.Clear();
            for ( int i=0; i < MAX_BATCH_RECORDS; ++i )
            {
               if ( index < stateObject.CodedNotesEnum.Count )
               {
                  notes.NOTE.Add( stateObject.NotesEnum.NOTE[ index ] );
               }
               ++index;
            }
            mTable.Notes.AddEnumRecords( notes );
         }

         notes.NOTE.Clear();

      }/* end method */


      /// <summary>
      /// Update the Coded Notes table in a sequence of scheduled
      /// batch writes - with each batch limited to a fixed number
      /// of records!
      /// </summary>
      /// <param name="stateObject">NOP State object</param>
      private void UpdateCodedNotesTable( NotOperatingStateObject stateObject )
      {
         int index = 0;
         EnumSelectedCodedNotes notes = new EnumSelectedCodedNotes();

         while ( index < stateObject.CodedNotesEnum.Count )
         {
            notes.CodedNote.Clear();

            for ( int i=0; i < MAX_BATCH_RECORDS; ++i )
            {
               if ( index < stateObject.CodedNotesEnum.Count )
               {
                  notes.CodedNote.Add( stateObject.CodedNotesEnum.CodedNote[ index ] );
               }
               ++index;
            }
            mTable.SavedCodedNotes.AddEnumRecords( notes );
         }

         notes.CodedNote.Clear();

      }/* end method */


      /// <summary>
      /// Adds a single Process or Inspection measurement to both the
      /// object hierarchy and the SQL database
      /// </summary>
      /// <param name="iPointRecord">Populated point object (with measurements)</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Measurements 'roll register'</param>
      private void AddSingleMeasurementAsync( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas, Boolean iOverwrite )
      {
         if ( iPointRecord.TempData.Count > 0 )
         {
            // set the data saved flag
            iPointRecord.DataSaved = true;

            // reference the current measurement node
            PointMeasurement measurement = 
               iPointRecord.TempData.Measurement[ iPointRecord.TempData.Count - 1 ];

            // what is the process type associated with this point?
            FieldTypes.ProcessType processType =
                (FieldTypes.ProcessType) iPointRecord.ProcessType;

            // set the initial point alarm for the current measurement 
            if ( ( processType == FieldTypes.ProcessType.MultiSelectInspection ) |
            ( processType == FieldTypes.ProcessType.SingleSelectInspection ) )
            {
               // get the specific process record for the current point 
               InspectionRecord settings =  
                  LinqQueries.GetPointsInspectionRecord( this.InspectionEnum, iPointRecord.Uid );

               // set the point and process alarm flags and update both objects
               SetInspectionAlarmFlags( iPointRecord, measurement, settings );
            }
            else if ( processType == FieldTypes.ProcessType.TextData )
            {
               // set the point alarm flag and update the point object
               SetTextDataAlarmFlags( iPointRecord );
            }
            else
            {
               // get the specific process record for the current point 
               ProcessRecord settings =  
                  LinqQueries.GetPointsProcessRecord( this.ProcessEnum, iPointRecord.Uid );

               // set the point alarm flag and update the point object
               SetProcessAlarmFlags( iPointRecord, measurement, settings );
            }

            // update all alarm and check flag states
            UpdateAlarmAndCheckFlags( iPointRecord );

            // update the database
            WriteToSqlAsynch( iPointRecord, iOperId, iNoLastMeas, iOverwrite );
         }

      }/* end method */


      /// <summary>
      /// Adds a series of measurements that are associated with the WMCD
      /// (Temp, Envelope and Envelope Acceleration) as well as updating
      /// the hierarchy collection and alarm condition states 
      /// </summary>
      /// <param name="iPointRecord">Populated point object (with measurements)</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Measurements 'roll register'</param>
      private void AddScalarMeasurementAsync( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas, Boolean iOverwrite )
      {
         // set the data saved flag
         iPointRecord.DataSaved = true;

         // get the specific process record for the current point 
         MCCRecord settings =  
                  LinqQueries.GetPointsMCCRecord( this.MCCEnum, iPointRecord.Uid );

         // set the point and process alarm flags and update both objects
         SetMCDAlarmFlags( iPointRecord, settings );

         // update all alarm and check flag states
         UpdateAlarmAndCheckFlags( iPointRecord );

         // update the database
         WriteToSqlAsynch( iPointRecord, iOperId, iNoLastMeas, iOverwrite );

      }/* end method */


      /// <summary>
      /// Adds a series of measurements that are associated with the WMCD
      /// (Temp, Envelope and Envelope Acceleration) as well as updating
      /// the hierarchy collection and alarm condition states and writing 
      /// the raw FFT data to the FFTChannel table.
      /// </summary>
      /// <param name="iPointRecord">Populated point object (with measurements)</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Measurements 'roll register'</param>
      /// <param name="iFFTChannelData">List of populated FFT Channel objects</param>
      private void AddScalarAndFFTMeasurementAsync(
         PointRecord iPointRecord,
         int iOperId,
         int iNoLastMeas,
         EnumBaseFFTChannels iFFTChannelData,
         Boolean iOverwrite )
      {
         // set the data saved flag
         iPointRecord.DataSaved = true;

         // get the specific process record for the current point 
         MCCRecord settings =  
                  LinqQueries.GetPointsMCCRecord( this.MCCEnum, iPointRecord.Uid );

         // set the point and process alarm flags and update both objects
         SetMCDAlarmFlags( iPointRecord, settings );

         // update all alarm and check flag states
         UpdateAlarmAndCheckFlags( iPointRecord );

         //
         // Is there an FFT associated with the current Point object? 
         //
         mFFTPoint = LinqQueries.GetFFTPointRecord( FFTPointsEnum, iPointRecord.Uid );

         //
         // if there was an FFTPoint object then populate the FFTChannel
         // objects prior to updating the database
         //
         if ( ( mFFTPoint != null ) & ( iFFTChannelData != null ) )
         {
            mFFTChannelsEnum = PopulateFFTChannelsEnum( iPointRecord, iFFTChannelData, mFFTPoint );
         }
         else
         {
            mFFTChannelsEnum = null;
         }

         // update the database
         WriteToSqlAsynch( iPointRecord, iOperId, iNoLastMeas, iOverwrite );

      }/* end method */


      /// <summary>
      /// Core method that updates both the alarm and collection check flags
      /// of every parent node for the current point.
      /// </summary>
      /// <param name="iPointRecord">Populated point object</param>
      private void UpdateAlarmAndCheckFlags( PointRecord iPointRecord )
      {
         // reset the sibling and check counters
         int checkCount = 0;
         int siblingCount = 0;

         // set the initial alarm and node state flags
         FieldTypes.NodeAlarmState alarmState = FieldTypes.NodeAlarmState.None;
         FieldTypes.CollectionCheck checkMark = FieldTypes.CollectionCheck.All;

         // reference alarm state for the current point 
         alarmState = (FieldTypes.NodeAlarmState) iPointRecord.AlarmState;

         // get a list of all sibling points
         EnumPoints siblings = LinqQueries.GetChildPoints( this.PointsEnum, iPointRecord.NodeUid );

         // get the overall collection and alarm state for each points siblings
         foreach ( PointRecord point in siblings )
         {
            //
            // increment datasaved count if either data has been saved OR
            // if the POINT was Conditionally skipped
            //
            if ( ( point.DataSaved ) || ( point.Skipped == (byte) FieldTypes.SkippedType.ConditionFailed ) )
            {
               ++checkCount;
            }

            // set the new alarm state for the parent node
            if ( ( iPointRecord.Uid != point.Uid ) && ( point.AlarmState > (byte) alarmState ) )
            {
               alarmState = (FieldTypes.NodeAlarmState) point.AlarmState;
            }
         }

         // what checkmark should we assign to the parent machine?
         checkMark = MachineCheckedState( siblings.Count, checkCount );

         // now get a list of all ancestor nodes
         EnumNodes ancestors = LinqQueries.GetAllAncestors( this.NodesEnum, iPointRecord.NodeUid );

         // make sure we have some ancestors
         if ( ancestors != null )
         {
            // reset the ancestors sibling counter
            siblingCount = 0;

            // and check for each ancestral layer!
            foreach ( NodeRecord node in ancestors )
            {
               if ( node.HierarchyDepth > 0 )
               {
                  // set the alarm state and collection logic for the current node
                  node.AlarmState = (byte) alarmState;
                  node.CollectionCheck = (byte) checkMark;

                  // get a list of siblings for the current node
                  EnumNodes siblingNodes = LinqQueries.GetSiblings( this.NodesEnum, node );

                  // determine the collection and alarm states for the next node parent
                  foreach ( NodeRecord cousin in siblingNodes )
                  {
                     checkMark = CheckFlagLogic( checkMark,
                        (FieldTypes.CollectionCheck) cousin.CollectionCheck );

                     // set the new alarm state for the parent node
                     if ( cousin.AlarmState > (byte) alarmState )
                     {
                        alarmState = (FieldTypes.NodeAlarmState) cousin.AlarmState;
                     }
                  }

                  // increment the sibling count
                  siblingCount++;

                  // if this is the last sibling then set!
                  if ( siblingCount == ancestors.Count )
                  {
                     node.AlarmState = (byte) alarmState;
                     node.CollectionCheck = (byte) checkMark;
                  }
               }
               //
               // for a hierarchy depth of zero we can't go any higher, 
               // so we need to stop and reference what's here
               //
               else
               {
                  node.AlarmState = (byte) alarmState;
                  node.CollectionCheck = (byte) checkMark;
               }
            }
         }

         // Update the PointsEnum list
         UpdatePointsEnumObject( iPointRecord );

      }/* end method */


      /// <summary>
      /// Overloaded method that starts with a Machine node instead of a point
      /// </summary>
      /// <param name="iNodeRecord">Node record to reference</param>
      private EnumNodes UpdateAlarmAndCheckFlags( NodeRecord iNodeRecord )
      {
         // reset sibling counter
         int siblingCount = 0;

         // set the initial alarm and node state flags
         FieldTypes.NodeAlarmState alarmState = FieldTypes.NodeAlarmState.None;
         FieldTypes.CollectionCheck checkMark = FieldTypes.CollectionCheck.All;

         alarmState = (FieldTypes.NodeAlarmState) iNodeRecord.AlarmState;
         checkMark = (FieldTypes.CollectionCheck)iNodeRecord.CollectionCheck;

         // now get a list of all ancestor nodes
         EnumNodes ancestors = 
            LinqQueries.GetAllAncestors( this.NodesEnum, iNodeRecord.Uid );

         // make sure we have some ancestors
         if ( ancestors != null )
         {
            // reset the ancestors sibling counter
            siblingCount = 0;

            // and check for each ancestral layer!
            foreach ( NodeRecord node in ancestors )
            {
               if ( node.HierarchyDepth > 0 )
               {
                  // set the alarm state and collection logic for the current node
                  node.AlarmState = (byte) alarmState;
                  node.CollectionCheck = (byte) checkMark;

                  // get a list of siblings for the current node
                  EnumNodes siblingNodes = LinqQueries.GetSiblings( this.NodesEnum, node );

                  // determine the collection and alarm states for the next node parent
                  foreach ( NodeRecord cousin in siblingNodes )
                  {
                     checkMark = CheckFlagLogic( checkMark,
                        (FieldTypes.CollectionCheck) cousin.CollectionCheck );

                     // set the new alarm state for the parent node
                     if ( cousin.AlarmState > (byte) alarmState )
                     {
                        alarmState = (FieldTypes.NodeAlarmState) cousin.AlarmState;
                     }
                  }

                  // increment the sibling count
                  siblingCount++;

                  // if this is the last sibling then set!
                  if ( siblingCount == ancestors.Count )
                  {
                     node.AlarmState = (byte) alarmState;
                     node.CollectionCheck = (byte) checkMark;
                  }
               }
               //
               // for a hierarchy depth of zero we can't go any higher, 
               // so we need to stop and reference what's here
               //
               else
               {
                  node.AlarmState = (byte) alarmState;
                  node.CollectionCheck = (byte) checkMark;
               }
            }
         }

         return ancestors;

      }/* end method */


      /// <summary>
      /// Updates the PointsEnum list to reflect any changes to a Point object
      /// that has been disconnected from the main list
      /// </summary>
      /// <param name="iPointRecord"></param>
      private void UpdatePointsEnumObject( PointRecord iPointRecord )
      {
         //
         // Update the points list.
         // What we're doing here is copying all of the edited fields in the
         // current point object, back into the orginal point object that's
         // still in the PointsEnum list. If we don't do this, then all alarm
         // calculations will fail!
         //

         // OK, get the original list object
         PointRecord sourcePoint = LinqQueries.GetPointRecord(
            PointsEnum, iPointRecord.Uid );

         // now copy all of the updated fields to it!
         sourcePoint.CloneObject( iPointRecord );

      }/* end method */


      /// <summary>
      /// Asynchronously update the POINTS table for a single Points
      /// </summary>
      /// <param name="state">A PointUpdateStateObject object</param>
      private void UpdateSqlPointAsynch( object state )
      {
         // Don't check for null below, if this is null we need an exception!
         PointUpdateStateObject updateObject =
            state as PointUpdateStateObject;

         // update the POINTS table
         mTable.Points.UpdateRecord( updateObject.Point, updateObject.UpdateCompliance );

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Asynchronously update the Node table for a single Node
      /// </summary>
      /// <param name="iNodeRecord">Populated Node object</param>
      private void UpdateSqlNodeAsynch( object iNodeRecord )
      {
         // reference the point object
         NodeRecord nodeRecord = (NodeRecord) iNodeRecord;

         // update the Node table
         mTable.Node.UpdateRecord( nodeRecord );

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Asynchronously update the NODE, POINTS and TEMPDATA tables
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="iNoLastMeas">Number of measurements to retain</param>
      private void WriteToSqlAsynch( PointRecord iPointRecord,
         int iOperId, int iNoLastMeas, Boolean iOverwrite )
      {
         // reference the number of measurements count
         Interlocked.Exchange( ref mMeasurements, iNoLastMeas );

         // reference the current Operator Id
         Interlocked.Exchange( ref mOperId, iOperId );

         //
         // reference whether a measurement overwrite is to be executed
         // Note: As exchange does not support Boolean types, we have to
         // effectively revert back to int
         //
         Interlocked.Exchange( ref mOverwrite, Convert.ToByte( iOverwrite ) );

         // set the busy flag before spinning off a new thread
         this.RaiseBusyEventAsync( true );

         // Queue the task and data (we don't need any delay here)
         ThreadPool.QueueUserWorkItem( WriteMeasurementsToSql, iPointRecord );

      }/* end method */


      /// <summary>
      /// Update the NODE, POINTS and TEMPDATA tables
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      private void WriteMeasurementsToSql( object iPointRecord )
      {
         // reference the point object
         PointRecord pointRecord = (PointRecord) iPointRecord;

         // set the default (or Text Data) alarm state
         FieldTypes.NodeAlarmState tempAlarm = TEXT_DATA_ALARM_STATE;

         //
         // is this to be a new measurement or a replacement of
         // an existing one? If a replacement, then we need to delete
         // the previous measurement(s) from the database
         //
         if ( Convert.ToBoolean( mOverwrite ) )
         {
            DeleteLastMeasurementFromSql( pointRecord );
         }

         //
         // if this is an MCD point then we need to update the 
         // TEMPDATA, VELDATA and ENVDATA tables. We also need to
         // return the individual alarm states for each value (as
         // opposed to the overall alarm state of the point)
         //
         if ( pointRecord.ProcessType == (byte) FieldTypes.ProcessType.MCD )
         {
            // get the current mcc record
            MCCRecord setup 
               = LinqQueries.GetPointsMCCRecord( MCCEnum, pointRecord.Uid );

            //
            // MCD POINTs don't have to contain a Temperature reading.
            // Check the DTS on the 'latest' Temperature reading to see
            // if it belongs to this set of data
            //
            int lastTempMeasIndex = pointRecord.TempData.Count - 1;

            if ( lastTempMeasIndex >= 0 )
            {
               // if the DTS of the temperature matches the vib...
               if ( pointRecord.TempData.Measurement[ lastTempMeasIndex ].LocalTime ==
                   pointRecord.VelData.GetLatestMeasurement().LocalTime )
               {
                  //
                  // if this is a Text Data measurement then the alarm status will 
                  // always be CLEAR, otherwise we need to reprocess the saved value 
                  // to get the actual alarm level
                  //
                  if ( pointRecord.ProcessType != (byte) FieldTypes.ProcessType.TextData )
                  {
                     tempAlarm = Alarms.GetMCDTempAlarmState( setup,
                           pointRecord.TempData.GetLatestMeasurement().Value );
                  }

                  // and update the TEMPDATA table
                  mTable.TempData.AppendRecord(
                     pointRecord,
                     mMeasurements,
                     tempAlarm,
                     mOperId );
               }
            }

            // now get the ENVDATA alarm state
            FieldTypes.NodeAlarmState envAlarm = 
               Alarms.GetMCDEnvelopeAlarmState( setup,
               pointRecord.EnvData.GetLatestMeasurement().Value );

            // and update the ENVDATA table
            mTable.EnvData.AppendRecord(
               pointRecord,
               mMeasurements,
               envAlarm,
               mOperId );

            // now get the ENVDATA alarm state
            FieldTypes.NodeAlarmState velAlarm = 
               Alarms.GetMCDVelocityAlarmState( setup,
               pointRecord.VelData.GetLatestMeasurement().Value );

            // update the VELDATA table
            mTable.VelData.AppendRecord(
               pointRecord,
               mMeasurements,
               velAlarm,
               mOperId );

            if ( mFFTPoint != null )
            {
               // synchronously write to the database
               if ( mFFTChannelsEnum != null )
               {
                  //
                  // Part of OTD# 6545 - prevent upload issues caused by too many FFTs
                  //
                  if( FftCount < DB_CAPACITY_FFT )
                  {
                     mFFTChannelsEnum.Reset( );
                     foreach( FFTChannelRecord fftChannelRecord in mFFTChannelsEnum )
                     {
                        mTable.FFTChannel.AddRecord( fftChannelRecord );
                     }
                  }
               }
            }
         }

         // for process or inspection points we don't need to 
         // recalculate the alarm state as its already known
         // and set in the field 'PointRecord.AlarmState'
         else
         {
            mTable.TempData.AppendRecord(
               pointRecord,
               mOperId,
               mMeasurements );
         }

         // update the POINTS table
         mTable.Points.UpdateRecord( pointRecord, true );

         // update the NODE table
         EnumNodes ancestors = 
            LinqQueries.GetAllAncestors( this.NodesEnum, pointRecord.NodeUid );
         mTable.Node.UpdateRecords( ancestors );

         // reset the busy flag
         this.RaiseBusyEventAsync( false );

      }/* end method */


      /// <summary>
      /// Deletes the last measurment of the PointRecord.  
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      private void DeleteLastMeasurementFromSql( PointRecord iPointRecord )
      {
         // reference the point object
         PointRecord pointRecord = (PointRecord) iPointRecord;

         //
         // MCD POINT, delete from VELDATA, ENVDATA, and possibly TEMPDATA
         //
         if ( pointRecord.ProcessType == (byte) FieldTypes.ProcessType.MCD )
         {
            //
            // Since MCD measurements don't have to have Temperature
            // measurements, we need to see if there is a Temperature
            // measurement with the last reading, so we can just check
            // the DTS on the last Velocity reading and see if theres
            // a Temperature reading at the same time
            //
            DateTime velReadingDTS = DateTime.MinValue;
            DateTime tempReadingDTS = DateTime.MinValue;

            mTable.VelData.GetLastMeasurementDTS( pointRecord, ref velReadingDTS );
            mTable.TempData.GetLastMeasurementDTS( pointRecord, ref tempReadingDTS );

            if ( ( velReadingDTS == tempReadingDTS ) && ( tempReadingDTS != DateTime.MinValue ) )
            {
               mTable.TempData.DeleteLastRecord( iPointRecord );
            }

            // we'll always have vib readings, kill 'em
            mTable.EnvData.DeleteLastRecord( iPointRecord );
            mTable.VelData.DeleteLastRecord( iPointRecord );

            // lets not forget about the FFT Channels!
            mTable.FFTChannel.DeleteLastRecord( iPointRecord, velReadingDTS );
         }
         // Process POINT, just delete the last record from TEMPDATA
         else
         {
            mTable.TempData.DeleteLastRecord( pointRecord );
         }

         // update the POINTS table
         mTable.Points.UpdateRecord( pointRecord, false );

      }/* end method */


      /// <summary>
      /// Calculates the current Point and Process alarm states and updates
      /// both objects as required
      /// </summary>
      /// <param name="iPointRecord">reference point object</param>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">process record object</param>
      private static void SetProcessAlarmFlags( PointRecord iPointRecord,
         PointMeasurement measurement, ProcessRecord settings )
      {
         // set the current process alarm state
         FieldTypes.AlarmState processAlarmState = 
            Alarms.GetProcessAlarmFlag( measurement.Value, settings );

         // set the points alarm state
         iPointRecord.AlarmState =
            (byte) Alarms.MapProcessAlarmToNodeAlarm( processAlarmState );

      }/* end method */


      /// <summary>
      /// Set the alarm state of a Text Data POINT to always be CLEAR!
      /// </summary>
      /// <param name="iPointRecord">reference point object</param>
      private static void SetTextDataAlarmFlags( PointRecord iPointRecord )
      {
         // set the points alarm state
         iPointRecord.AlarmState = (byte) TEXT_DATA_ALARM_STATE;

      }/* end method */


      /// <summary>
      /// Set the alarm flags for both single and multiple-select Inspections
      /// </summary>
      /// <param name="iPointRecord">populated reference point</param>
      /// <param name="measurement">current measurement</param>
      /// <param name="settings">populated point inspection</param>
      private static void SetInspectionAlarmFlags( PointRecord iPointRecord,
         PointMeasurement measurement, InspectionRecord settings )
      {
         // get the current inspection alarm state
         FieldTypes.NodeAlarmState inspectionAlarmState = Alarms.GetInspectionAlarmFlag(
            (FieldTypes.ProcessType) iPointRecord.ProcessType,
            measurement.Value,
            settings );

         // set the point alarm state
         iPointRecord.AlarmState = (byte) inspectionAlarmState;

      }/* end method */


      /// <summary>
      /// Calculates the current Point and MCD alarm states and updates
      /// both objects as required
      /// </summary>
      /// <param name="iPointRecord">reference point object</param>
      /// <param name="settings">process record object</param>
      private static void SetMCDAlarmFlags( PointRecord iPointRecord,
         MCCRecord settings )
      {
         // set the points alarm state
         iPointRecord.AlarmState =
            (byte) Alarms.GetMCDAlarmFlag( iPointRecord, settings );

      }/* end method */
      #endregion

      //----------------------------------------------------------------------------

   }/* end class */


   /// <summary>
   /// Basic State Object for updating the Notes Table
   /// </summary>
   internal class NotesStateObject
   {
      /// <summary>
      /// Gets or sets a reference to the Upload Notes object
      /// </summary>
      public UploadNoteRecord UploadNote
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets a reference to the Point Record object
      /// </summary>
      public PointRecord Point
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Update Points table flag
      /// </summary>
      public Boolean UpdatePointsTable
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the measurement note collection stamp
      /// </summary>
      public DateTime CollectionStamp
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Basic state Object for skipping Machines\POINTs due to Conditionality
   /// </summary>
   internal class CondSkipStateObject
   {
      /// <summary>
      /// Gets or sets the machine node object
      /// </summary>
      public EnumNodes NodesEnum
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets a reference to the Upload Notes object
      /// </summary>
      public EnumPoints PointsEnum
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the maximum number of measurements to
      /// be saved against a single point
      /// </summary>
      public int NoLastMeasurements
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the current operator ID
      /// </summary>
      public int OperatorId
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Basic state Object for setting the Machine OK state
   /// </summary>
   internal class MachineOKStateObject : CondSkipStateObject
   {
      /// <summary>
      /// Gets or sets the machine node object
      /// </summary>
      public NodeRecord NodeRecord
      {
         get;
         set;
      }
     
   }/* end class */


   /// <summary>
   /// Basic state Object for setting the Machine NOP state
   /// </summary>
   internal class NotOperatingStateObject : MachineOKStateObject
   {
      /// <summary>
      /// Gets or sets an enumerated lits of note objects
      /// </summary>
      public EnumNotes NotesEnum
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets an enumerated list of coded notes
      /// </summary>
      public EnumSelectedCodedNotes CodedNotesEnum
      {
         get;
         set;
      }

   }/* end class */


   /// <summary>
   /// Basic state Object for updating a POINT
   /// </summary>
   internal class PointUpdateStateObject
   {
      /// <summary>
      /// Gets the POINT to update
      /// </summary>
      public PointRecord Point
      {
         get;
         private set;
      }

      /// <summary>
      /// Gets whether or not to update the compliance for this POINT
      /// </summary>
      public bool UpdateCompliance
      {
         get;
         private set;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iPoint">the POINT to update</param>
      /// <param name="iUpdateCompliance">whether or not to update the compliance 
      /// for this POINT</param>
      public PointUpdateStateObject( PointRecord iPoint, bool iUpdateCompliance )
      {
         this.Point = iPoint;
         this.UpdateCompliance = iUpdateCompliance;
      }

   }/* end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 9.0 APinkerton 26th April 2012
//  Increase inspection fields from five to fifteen
//
//  Revision 8.0 TBottalico 25th March 2011
//  Allow a POINT update to specifiy whether or not its compliance will be
//  updated.  OTD# 2567
//
//  Revision 7.0 APinkerton 25th February 2011
//  Add support for database version control
//
//  Revision 6.0 APinkerton 7th October 2010
//  Add support for true "Non Collection" measurement status
//
//  Revision 5.0 APinkerton 4th May 2010
//  Implement two connection objects and add support buffered hierarchy 
//  message objects
//
//  Revision 4.0 APinkerton 29th April 2010
//  Add support for "Machine not Operating" and "Machine OK" functionality
//
//  Revision 3.0 APinkerton 9th April 2010
//  Add new TempMeasurementsEnum, EnvMeasurementsEnum, VelMeasurementsEnum
//  properties as well as method GetMeasurementsEnum() - which will populate
//  the fields behind these properties
//
//  Revision 2.0 APinkerton 18th March 2010
//  Add support for WMCD Serial Number
//
//  Revision 1.0 APinkerton 06th October 2009
//  Add support for Asynchronous updates 
//
//  Revision 0.0 APinkerton 16th March 2009
//  Add to project
//----------------------------------------------------------------------------
