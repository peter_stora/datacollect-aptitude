﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the DERIVEDPOINT Table ResultSet
   /// </summary>
   public class ResultSetDerivedPointTable : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsDerivedPoint mColumnOrdinal = null;

      /// <summary>
      /// Query DERIVEDPOINTS table based on the external Point Uid field. 
      /// This query will return an enumerated series of records ordered
      /// by the field "FormulaItemPosition".
      /// </summary>
      private const string QUERY_BY_POINTUID = "SELECT * FROM DERIVEDPOINT " +
          "WHERE PointUid = @PointUid";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_DERIVEDPOINT;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current DERIVEDPOINT table. 
      /// </summary>
      /// <param name="iDerivedPointObject">populated DerivedPointRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( DerivedPointRecord iDerivedPointObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               result = WriteRecord( iDerivedPointObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of MessageRecord objects to the DERIVEDPOINT 
      /// table and return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of DerivePoint objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumDerivedPoints iDerivedPoints,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( DerivedPointRecord derivedPoint in iDerivedPoints )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( derivedPoint, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the DERIVEDPOINT table. 
      /// </summary>
      /// <param name="iDerivedItemObjects">populated DerivedItems objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumDerivedPoints iDerivedPoints )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDPOINT";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               foreach ( DerivedPointRecord derivedPoint in iDerivedPoints )
               {
                  // write record to table
                  if ( !WriteRecord( derivedPoint, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single Derived Point Record object from the DERIVEDPOINT 
      /// table using the external PointUid (links to POINTS.Uid) as the lookup
      /// reference.
      /// </summary>
      /// <param name="iPointUid">External link to Point Uid</param>
      /// <returns>Populated Derived Point object or null on error</returns>
      public DerivedPointRecord GetRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // reset the default profile name
         DerivedPointRecord derivedPoint = new DerivedPointRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINTUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value
                                                                 = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the DERIVEDPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref derivedPoint );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return derivedPoint;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all DERIVEDPOINT records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumDerivedPoints GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumDerivedPoints derivedPoints = new EnumDerivedPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "DERIVEDPOINT";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the DERIVEDPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  DerivedPointRecord derivedPoint = new DerivedPointRecord();
                  PopulateObject( ceResultSet, ref derivedPoint );
                  derivedPoints.AddDerivedPointRecord( derivedPoint );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return derivedPoints;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// DERIVEDPOINT table. This table will need to be revised in the event of any 
      /// changes being made to the DERIVEDPOINT table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsDerivedPoint( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new DERIVEDPOINT record and add it 
      /// to the current DERIVEDPOINT table.
      /// </summary>
      /// <param name="iDerivedPointRecord">Populated DERIVEDPOINT Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( DerivedPointRecord iDerivedPointRecord,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.ID ] = iDerivedPointRecord.ID;
            newRecord[ mColumnOrdinal.PointUid ] = iDerivedPointRecord.PointUid;
            newRecord[ mColumnOrdinal.EvaluationTime ] = iDerivedPointRecord.EvaluationTime;
            newRecord[ mColumnOrdinal.NumberOfItems ] = iDerivedPointRecord.NumberOfItems;
            newRecord[ mColumnOrdinal.RouteId ] = iDerivedPointRecord.RouteId;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single DERIVEDPOINT table record
      /// </summary>
      /// <param name="iDerivedPointRecord">Updated DERIVEDPOINT Record object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( DerivedPointRecord iDerivedPointRecord,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.ID, iDerivedPointRecord.ID );
            ceResultSet.SetGuid( mColumnOrdinal.PointUid, iDerivedPointRecord.PointUid );
            ceResultSet.SetInt32( mColumnOrdinal.EvaluationTime, iDerivedPointRecord.EvaluationTime );
            ceResultSet.SetInt32( mColumnOrdinal.NumberOfItems, iDerivedPointRecord.NumberOfItems );
            ceResultSet.SetInt32( mColumnOrdinal.RouteId, iDerivedPointRecord.RouteId );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated DERIVEDPOINT Object with data from a single 
      /// DERIVEDPOINT table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iDerivedPointRecord">DERIVEDPOINT object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref DerivedPointRecord iDerivedPointRecord )
      {
         try
         {
            iDerivedPointRecord.ID = ceResultSet.GetInt32( mColumnOrdinal.ID );
            iDerivedPointRecord.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iDerivedPointRecord.EvaluationTime = ceResultSet.GetInt32( mColumnOrdinal.EvaluationTime );
            iDerivedPointRecord.NumberOfItems = ceResultSet.GetInt32( mColumnOrdinal.NumberOfItems );
            iDerivedPointRecord.RouteId = ceResultSet.GetInt32( mColumnOrdinal.RouteId );
         }
         catch
         {
            iDerivedPointRecord = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------