﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the POINTS Table ResultSet
   /// </summary>
   public class ResultSetPointsTable : ResultSetBase
   {
      //-------------------------------------------------------------------------

      #region Private fields and constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsPoint mColumnOrdinal = null;


      /// <summary>
      /// Query Points table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM POINTS " +
          "WHERE Uid = @Uid";


      /// <summary>
      /// Query Points table based purely on Tag Changes
      /// </summary>
      private const string QUERY_BY_CHANGED_AND_SAVED = "SELECT * FROM POINTS " +
          "WHERE TagChanged > @TagChanged " +
          "OR DataSaved = @DataSaved " +
          "OR Skipped > @Skipped";

      /// <summary>
      /// Query Points table based purely on Tag Changes
      /// </summary>
      private const string QUERY_BY_NON_COLLECTION = "SELECT * FROM POINTS " +
          "WHERE TagChanged = @TagChanged " +
          "AND DataSaved = @DataSaved " +
          "AND Skipped = @Skipped";


      /// <summary>
      /// Reset all measurements and flags in the POINTS table
      /// </summary>
      private const string RESET_POINT_MEASUREMENTS = @"UPDATE POINTS SET 
            TempDataValue = NULL,
            TextDataValue = NULL, 
            TempDataUTC = NULL, 
            EnvDataValue = NULL, 
            EnvDataUTC = NULL, 
            VelDataValue = NULL, 
            VelDataUTC = NULL, 
            LastTempDataValue = NULL, 
            LastTempDataUTC = NULL, 
            LastEnvDataValue = NULL, 
            LastEnvDataUTC = NULL, 
            LastVelDataValue = NULL, 
            LastVelDataUTC = NULL, 
            LastModified = NULL,
            TagChanged = @TagChanged,
            DataSaved = @DataSaved,
            Skipped = @Skipped, 
            AlarmState = @AlarmState, 
            NoteCount = @NoteCount";

      /// <summary>
      /// Query Points table based purely on Tag Changes
      /// </summary>
      private const string QUERY_BY_CHANGED_AND_SAVED_WITH_ROUTEID = 
          "SELECT * FROM POINTS WHERE (TagChanged > @TagChanged " +
          "OR DataSaved = @DataSaved " +
          "OR Skipped > @Skipped) " +
          "AND RouteID = @RouteID";

      /// <summary>
      /// Query Points table based purely on non-collection state
      /// </summary>
      private const string QUERY_BY_NON_COLLECTION_WITH_ROUTEID = 
          "SELECT * FROM POINTS WHERE (TagChanged = @TagChanged " +
          "AND DataSaved = @DataSaved " +
          "AND Skipped = @Skipped) " +
          "AND RouteID = @RouteID";


      /// <summary>
      /// Query Points table based on Node Uid and order results on the NUMBER column 
      /// </summary>
      private const string QUERY_BY_NODE_UID = "SELECT * FROM POINTS " +
          "WHERE NodeUid = @NodeUid ORDER by NUMBER ASC";


      /// <summary>
      /// Query Points table based on the Process Type and order results on the NUMBER column 
      /// </summary>
      private const string QUERY_BY_PROCESS_TYPE = "SELECT * FROM POINTS " +
          "WHERE ProcessType = @ProcessType ORDER by NUMBER ASC";

      /// <summary>
      /// Query Points table based on the Alarm State and order results on the NUMBER column 
      /// </summary>
      private const string QUERY_BY_ALARM_STATE = "SELECT * FROM POINTS " +
          "WHERE AlarmState = @AlarmState ORDER by NUMBER ASC";

      /// <summary>
      /// Query Points table based on the LocationTag and order results on the NUMBER column 
      /// </summary>
      private const string QUERY_BY_LOCATION_TAG = "SELECT * FROM POINTS " +
          "WHERE LocationTag = @LocationTag ORDER by NUMBER ASC";

      #endregion

      //-------------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_POINTS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current POINTS table.
      /// </summary>
      /// <param name="iPointObject">populated POINT object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( PointRecord iPointObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "POINTS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {

               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write to the database
               result = WriteRecord( iPointObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of PointRecord objects to the POINTS table. 
      /// </summary>
      /// <param name="iEnumPoints">Enumerated List of PointRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // reset the enum list - just in case!
         iEnumPoints.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "POINTS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( PointRecord point in iEnumPoints )
               {
                  if ( !WriteRecord( point, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of PointRecord objects to the POINTS table and
      /// return a progress position event on each update
      /// </summary>
      /// <param name="iEnumPoints">Enumerated List of PointRecord objects</param>
      /// <param name="iProgress">false on exception raised</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumPoints iEnumPoints, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the enum list - just in case!
         iEnumPoints.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "POINTS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {

               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( PointRecord point in iEnumPoints )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( point, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single PointRecord object from the POINTS table using 
      /// the Uid(or unique Id field) as the lookup reference. Note: This
      /// method does not rely on the base class properties and so can be
      /// called directly from a single instantiated ResultSet object.
      /// </summary>
      /// <param name="iPointUid">Point Unique identifier</param>
      /// <returns>Populated point object or null on error</returns>
      public PointRecord GetPointRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // reset the default profile name
         PointRecord pointRecord = new PointRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return pointRecord;

      }/* end method */


      /// <summary>
      /// Quickly returns an enumerated list of all POINT records ordered 
      /// by the primary key field POINT.UID
      /// </summary>
      /// <returns>an enumerated list of PointRecord objects</returns>
      public EnumPoints GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumPoints pointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "POINTS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  PointRecord pointRecord = new PointRecord();
                  PopulateObject( ceResultSet, ref pointRecord );
                  if ( pointRecord != null )
                     pointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return pointRecords;

      }/* end method */


      /// <summary>
      /// Updates a POINTS table record with the data from its associated
      /// business object (a single PointRecord object). As all fields are
      /// replaced, this method should be used with caution! 
      /// </summary>
      /// <param name="iPointRecord">Populated PointRecord object</param>
      public void UpdateRecord( PointRecord iPointRecord, bool iUpdateCompliance )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iPointRecord.Uid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read and update the PointRecord object
               if ( ceResultSet.ReadFirst() )
               {
                  UpdateRecord( iPointRecord, iUpdateCompliance, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Table Specific Methods

      /// <summary>
      /// This method updates a series of Point Records. This method 
      /// should primarily be used in conjunction with GetAncestors()
      /// </summary>
      /// <param name="iPointRecords">EnumPoints to update</param>
      /// <param name="iUpdateCompliance">Whether or not to update the compliance</param>
      /// <returns>true on success, else false</returns>
      public Boolean UpdateRecords( EnumPoints iPointRecords, bool iUpdateCompliance )
      {
         // set the initial result
         Boolean result = true;

         // reset the Node object list - essential this!!
         iPointRecords.Reset();

         // open the connection
         OpenCeConnection();

         // better safe than sorry
         try
         {
            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand() )
            {
               // reference the base query string
               command.CommandText = QUERY_BY_POINT_UID;

               // reference the lookup field as a parameter
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier );

               foreach ( PointRecord point in iPointRecords )
               {
                  // reference the lookup field as a parameter
                  command.Parameters[ 0 ].Value = point.Uid;

                  // execute the result set
                  using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                              ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated 
                     // with the POINT table. As this is a  'one shot'  the column 
                     // ordinal object remains a singleton
                     //
                     GetTableColumns( CeResultSet );

                     // now read the record object
                     if ( CeResultSet.ReadFirst() )
                     {
                        UpdateRecord( point, iUpdateCompliance, CeResultSet );
                     }

                     // close the resultSet
                     CeResultSet.Close();
                  }
               }
            }
         }
         catch
         {
            result = false;
         }
         finally
         {
            CloseCeConnection();
         }

         // we're done here
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecord object given the
      /// Unique Identifier of the parent machine node. 
      /// </summary>
      /// <param name="iNodeUid">Unique machine node identifier</param>
      /// <returns>enumerated list of Point objects</returns>
      public EnumPoints GetEnumRecords( Guid iNodeUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@NodeUid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecord object given the
      /// collection Process Type 
      /// </summary>
      /// <param name="iProcessType">Process type enumerator</param>
      /// <returns>enumerated list of Point objects</returns>
      public EnumPoints GetEnumRecords( FieldTypes.ProcessType iProcessType )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_PROCESS_TYPE;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@ProcessType", SqlDbType.TinyInt ).Value = (byte) iProcessType;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecord object given the
      /// collection Alarm State
      /// </summary>
      /// <param name="iAlarmState">AlarmState type enumerator</param>
      /// <returns>enumerated list of Point objects</returns>
      public EnumPoints GetEnumRecords( FieldTypes.AlarmType iAlarmState )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_ALARM_STATE;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@AlarmState", SqlDbType.TinyInt ).Value = (byte) iAlarmState;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecord object given the
      /// Location Tag field (should just be one record!)
      /// </summary>
      /// <param name="iLocationTag">Location Tag string</param>
      /// <returns>enumerated list of Point objects</returns>
      public EnumPoints GetEnumRecords( string iLocationTag )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_LOCATION_TAG;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@LocationTag", SqlDbType.Text ).Value = iLocationTag;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Counts the number of Point records ready to be uploaded
      /// </summary>
      /// <returns>record count</returns>
      public int CountEnumUploadRecords()
      {
         OpenCeConnection();

         int result = 0;

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CHANGED_AND_SAVED;

            // reference the TagChanged field as a parameter
            byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
            command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

            // reference the TagChanged field as a parameter
            command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = true;

            // reference the Point Skipped field
            byte skipped = (byte) FieldTypes.SkippedType.None;
            command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ++result;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Counts the number of 'non-collection' Point records ready to be uploaded
      /// </summary>
      /// <returns>record count</returns>
      public int CountEnumNonCollectedUploadRecords()
      {
         OpenCeConnection();

         int result = 0;

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NON_COLLECTION;

            // reference the TagChanged field as a parameter
            byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
            command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

            // reference the TagChanged field as a parameter
            command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = false;

            // reference the Point Skipped field
            byte skipped = (byte) FieldTypes.SkippedType.None;
            command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ++result;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecords that either have 
      /// data and/or notes saved to them or have been edited by the user
      /// </summary>
      /// <returns>Enumerated list of point objects</returns>
      public EnumPoints GetEnumUploadRecords()
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CHANGED_AND_SAVED;

            // reference the TagChanged field as a parameter
            byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
            command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

            // reference the TagChanged field as a parameter
            command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = true;

            // reference the Point Skipped field
            byte skipped = (byte) FieldTypes.SkippedType.None;
            command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecords that either have 
      /// data and/or notes saved to them or have been edited by the user
      /// </summary>
      /// <returns>Enumerated list of point objects</returns>
      public EnumPoints GetEnumUploadRecords( int iRouteID )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_CHANGED_AND_SAVED_WITH_ROUTEID;

            // reference the TagChanged field as a parameter
            byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
            command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

            // reference the TagChanged field as a parameter
            command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = true;

            // reference the Point Skipped field
            byte skipped = (byte) FieldTypes.SkippedType.None;
            command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

            // reference the RouteID
            command.Parameters.Add( "@RouteID", SqlDbType.Int ).Value = iRouteID;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of PointRecords that have no collection
      /// states assigned to them
      /// </summary>
      /// <returns>Enumerated list of point objects</returns>
      public EnumPoints GetEnumNonCollectedUploadRecords( int iRouteID )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPoints enumPointRecords = new EnumPoints();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NON_COLLECTION_WITH_ROUTEID;

            // reference the TagChanged field as a parameter
            byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
            command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

            // reference the TagChanged field as a parameter
            command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = false;

            // reference the Point Skipped field
            byte skipped = (byte) FieldTypes.SkippedType.None;
            command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

            // reference the RouteID
            command.Parameters.Add( "@RouteID", SqlDbType.Int ).Value = iRouteID;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointRecord pointRecord = new PointRecord();

                  PopulateObject( ceResultSet, ref pointRecord );

                  // add to the enumerated list
                  if ( pointRecord != null )
                     enumPointRecords.AddPointsRecord( pointRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumPointRecords;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the POINTS table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public Boolean ResetAllFlags()
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = "POINTS";
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the POINTS table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // go through each of the result records and reset the new flag
                  while ( ceResultSet.Read() )
                  {
                     // reset the DataSaved flag
                     ceResultSet.SetBoolean( mColumnOrdinal.DataSaved, false );

                     // reset the collected alarm status to "off"
                     ceResultSet.SetByte( mColumnOrdinal.AlarmState, (byte) FieldTypes.NodeAlarmState.Reset );

                     // reset the Tag Changed flag
                     ceResultSet.SetByte( mColumnOrdinal.TagChanged, (byte) FieldTypes.TagChanged.Unchanged );

                     // reset the skipped flag
                     ceResultSet.SetByte( mColumnOrdinal.Skipped, (byte) FieldTypes.SkippedType.None );

                     // reset the Note Count
                     ceResultSet.SetInt32( mColumnOrdinal.NoteCount, 0 );

                     // update the node record
                     ceResultSet.Update();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/*end method */


      /// <summary>
      /// Resets all measurements and flags within the POINTS table.
      /// This method completely reworked to address OTD #2524
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public Boolean ResetAllMeasurementsAndFlags()
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         try
         {
            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand() )
            {
               // get the full reset command
               command.CommandText = RESET_POINT_MEASUREMENTS;

               // Set TagChanged field to null
               byte tagUnchanged = (byte) FieldTypes.TagChanged.Unchanged;
               command.Parameters.Add( "@TagChanged", SqlDbType.TinyInt ).Value = tagUnchanged;

               // Set DataSaved field to false
               command.Parameters.Add( "@DataSaved", SqlDbType.Bit ).Value = false;

               // Set the Skipped field to None
               byte skipped = (byte) FieldTypes.SkippedType.None;
               command.Parameters.Add( "@Skipped", SqlDbType.TinyInt ).Value = skipped;

               // Set the AlarmState field to OFF
               byte AlarmState = (byte) FieldTypes.AlarmState.Off;
               command.Parameters.Add( "@AlarmState", SqlDbType.TinyInt ).Value = AlarmState;

               // Reset the Note Count to zero
               command.Parameters.Add( "@NoteCount", SqlDbType.Int ).Value = 0;

               // execute the query
               command.ExecuteNonQuery();
            }
         }
         finally
         {
            CloseCeConnection();
         }

         return result;

      }/*end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Private Methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// POINTS table. This table will need to be revised in the event of any changes
      /// being made to the POINTS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsPoint( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Populate an instantiated PointObject with data from a single POINTS
      /// table record. In the event of a raised exception the returned point
      /// object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="pointRecord">Point object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet, ref PointRecord pointRecord )
      {
         try
         {
            pointRecord.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );

            if ( ceResultSet[ mColumnOrdinal.Id ] != DBNull.Value )
               pointRecord.Id = ceResultSet.GetString( mColumnOrdinal.Id );

            pointRecord.NodeUid = ceResultSet.GetGuid( mColumnOrdinal.NodeUid );

            if ( ceResultSet[ mColumnOrdinal.Description ] != DBNull.Value )
               pointRecord.Description = ceResultSet.GetString( mColumnOrdinal.Description );

            pointRecord.ProcessType = ceResultSet.GetByte( mColumnOrdinal.ProcessType );
            pointRecord.ScheduleDays = ceResultSet.GetDouble( mColumnOrdinal.ScheduleDays );

            if ( ( ceResultSet[ mColumnOrdinal.Calendar ] != DBNull.Value ) &
               ( ceResultSet[ mColumnOrdinal.ComplianceWindow ] != DBNull.Value ) )
            {
               pointRecord.SetCalendarCompliance(
                  (FieldTypes.Calendar) ceResultSet.GetByte( mColumnOrdinal.Calendar ),
                  (UInt16) ceResultSet.GetInt16( mColumnOrdinal.ComplianceWindow ) );
            }

            if ( ceResultSet[ mColumnOrdinal.StartFrom ] != DBNull.Value )
               pointRecord.StartFrom = ceResultSet.GetDateTime( mColumnOrdinal.StartFrom );

            pointRecord.LocationMethod = ceResultSet.GetByte( mColumnOrdinal.LocationMethod );

            if ( ceResultSet[ mColumnOrdinal.LocationTag ] != DBNull.Value )
               pointRecord.LocationTag = ceResultSet.GetString( mColumnOrdinal.LocationTag );

            if ( ceResultSet[ mColumnOrdinal.LastModified ] != DBNull.Value )
               pointRecord.LastModified = ceResultSet.GetDateTime( mColumnOrdinal.LastModified );

            pointRecord.DataSaved = ceResultSet.GetBoolean( mColumnOrdinal.DataSaved );
            pointRecord.AlarmState = ceResultSet.GetByte( mColumnOrdinal.AlarmState );

            if ( ceResultSet[ mColumnOrdinal.PointIndex ] != DBNull.Value )
               pointRecord.PointIndex = ceResultSet.GetInt32( mColumnOrdinal.PointIndex );

            pointRecord.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            pointRecord.Number = ceResultSet.GetInt32( mColumnOrdinal.Number );
            pointRecord.TagChanged = ceResultSet.GetByte( mColumnOrdinal.TagChanged );
            pointRecord.RouteId = ceResultSet.GetInt32( mColumnOrdinal.RouteId );
            pointRecord.Skipped = ceResultSet.GetByte( mColumnOrdinal.Skipped );

            // get the current note count
            if ( ceResultSet[ mColumnOrdinal.NoteCount ] != DBNull.Value )
               pointRecord.SetNoteCount( ceResultSet.GetInt32( mColumnOrdinal.NoteCount ) );

            // add the LASTTEMPDATA record
            if ( ( ( ceResultSet[ mColumnOrdinal.LastTempDataValue ] != DBNull.Value ) |
                   ( ceResultSet[ mColumnOrdinal.LastTextDataValue ] != DBNull.Value ) ) &
               ( ceResultSet[ mColumnOrdinal.LastTempDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement tempData = new PointMeasurement();

               // get either the last numeric or text data value
               if ( ceResultSet[ mColumnOrdinal.LastTempDataValue ] != DBNull.Value )
                  tempData.Value = ceResultSet.GetDouble( mColumnOrdinal.LastTempDataValue );
               else
                  tempData.TextValue = ceResultSet.GetString( mColumnOrdinal.LastTextDataValue );

               // get the last local datetime
               tempData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LastTempDataUTC );

               // index = 0
               pointRecord.TempData.AddMeasurement( tempData );
            }

            // add the last TEMPDATA record
            if ( ( ( ceResultSet[ mColumnOrdinal.TempDataValue ] != DBNull.Value ) |
                   ( ceResultSet[ mColumnOrdinal.TextDataValue ] != DBNull.Value ) ) &
               ( ceResultSet[ mColumnOrdinal.TempDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement tempData = new PointMeasurement();

               // get either the numeric or text data value
               if ( ceResultSet[ mColumnOrdinal.TempDataValue ] != DBNull.Value )
                  tempData.Value = ceResultSet.GetDouble( mColumnOrdinal.TempDataValue );
               else
                  tempData.TextValue = ceResultSet.GetString( mColumnOrdinal.TextDataValue );

               // get the local datetime
               tempData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.TempDataUTC );

               // index = 1 if we have LASTTEMPDATA, else index = 0
               pointRecord.TempData.AddMeasurement( tempData );
            }

            // add the last LASTENVDATA record 
            if ( ( ceResultSet[ mColumnOrdinal.LastEnvDataValue ] != DBNull.Value ) &
               ( ceResultSet[ mColumnOrdinal.LastEnvDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement envData = new PointMeasurement();
               envData.Value = ceResultSet.GetDouble( mColumnOrdinal.LastEnvDataValue );
               envData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LastEnvDataUTC );

               // index = 0
               pointRecord.EnvData.AddMeasurement( envData );
            }

            // add the last ENVDATA record 
            if ( ( ceResultSet[ mColumnOrdinal.EnvDataValue ] != DBNull.Value ) &
               ( ceResultSet[ mColumnOrdinal.EnvDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement envData = new PointMeasurement();
               envData.Value = ceResultSet.GetDouble( mColumnOrdinal.EnvDataValue );
               envData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.EnvDataUTC );

               // index = 1 if we have LASTENVDATA, else index = 0
               pointRecord.EnvData.AddMeasurement( envData );
            }

            // add the last LASTVELDATA record 
            if ( ( ceResultSet[ mColumnOrdinal.LastVelDataValue ] != DBNull.Value ) &
               ( ceResultSet[ mColumnOrdinal.LastVelDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement velData = new PointMeasurement();
               velData.Value = ceResultSet.GetDouble( mColumnOrdinal.LastVelDataValue );
               velData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LastVelDataUTC );

               // index = 0
               pointRecord.VelData.AddMeasurement( velData );
            }

            // add the last VELDATA record 
            if ( ( ceResultSet[ mColumnOrdinal.VelDataValue ] != DBNull.Value ) &
               ( ceResultSet[ mColumnOrdinal.VelDataUTC ] != DBNull.Value ) )
            {
               PointMeasurement velData = new PointMeasurement();
               velData.Value = ceResultSet.GetDouble( mColumnOrdinal.VelDataValue );
               velData.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.VelDataUTC );

               // index = 1 if we have LASTVELDATA, else index = 0
               pointRecord.VelData.AddMeasurement( velData );
            }
         }
         catch
         {
            pointRecord = null;
         }

      }/* end method */


      /// <summary>
      /// Construct and populate a single new POINT record and add it 
      /// to the current POINTS table.
      /// </summary>
      /// <param name="iPointObject">Populated Point Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private Boolean WriteRecord( PointRecord iPointObject,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            // populate record object
            newRecord[ mColumnOrdinal.Uid ] = iPointObject.Uid;

            // OTD# 3005 - protect against Oracle null fields
            if ( iPointObject.Id != null )
               newRecord[ mColumnOrdinal.Id ] = iPointObject.Id;
            else
               newRecord[ mColumnOrdinal.Id ] = string.Empty;

            newRecord[ mColumnOrdinal.NodeUid ] = iPointObject.NodeUid;

            // OTD# 3005 - protect against Oracle null fields
            if ( iPointObject.Description != null )
               newRecord[ mColumnOrdinal.Description ] = iPointObject.Description;
            else
               newRecord[ mColumnOrdinal.Description ] = string.Empty;

            newRecord[ mColumnOrdinal.ProcessType ] = iPointObject.ProcessType;
            newRecord[ mColumnOrdinal.ScheduleDays ] = iPointObject.ScheduleDays;

            // write the compliance fields
            WriteComplianceFields( iPointObject, newRecord );

            newRecord[ mColumnOrdinal.LocationMethod ] = iPointObject.LocationMethod;

            // OTD# 3005 - protect against Oracle null fields
            if ( iPointObject.LocationTag != null )
               newRecord[ mColumnOrdinal.LocationTag ] = iPointObject.LocationTag;
            else
               newRecord[ mColumnOrdinal.LocationTag ] = string.Empty;

            if ( iPointObject.LastModified.Ticks > 0 )
               newRecord[ mColumnOrdinal.LastModified ] = iPointObject.LastModified;

            newRecord[ mColumnOrdinal.DataSaved ] = iPointObject.DataSaved;
            newRecord[ mColumnOrdinal.AlarmState ] = iPointObject.AlarmState;
            newRecord[ mColumnOrdinal.PointIndex ] = 0;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iPointObject.PointHostIndex;
            newRecord[ mColumnOrdinal.Number ] = iPointObject.Number;
            newRecord[ mColumnOrdinal.TagChanged ] = iPointObject.TagChanged;
            newRecord[ mColumnOrdinal.NoteCount ] = iPointObject.GetNoteCount();
            newRecord[ mColumnOrdinal.RouteId ] = iPointObject.RouteId;
            newRecord[ mColumnOrdinal.Skipped ] = iPointObject.Skipped;

            // write the temp, vel and env fields
            WriteValueFields( iPointObject, newRecord );

            // add new record to the database 
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single POINT table record.
      /// Note: Checks for "null" were added after OTD#2101 was raised
      /// </summary>
      /// <param name="iPointRecord">Updated PointRecord object to write</param>
      /// <param name="iUpdateCompliance">Whether or not to update the compliance fields</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( PointRecord iPointRecord, bool iUpdateCompliance,
         SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iPointRecord.Uid );

            if ( iPointRecord.Id != null )
               CeResultSet.SetString( mColumnOrdinal.Id, iPointRecord.Id );

            CeResultSet.SetGuid( mColumnOrdinal.NodeUid, iPointRecord.NodeUid );

            if ( iPointRecord.Description != null )
               CeResultSet.SetString( mColumnOrdinal.Description, iPointRecord.Description );

            CeResultSet.SetByte( mColumnOrdinal.ProcessType, iPointRecord.ProcessType );
            CeResultSet.SetDouble( mColumnOrdinal.ScheduleDays, iPointRecord.ScheduleDays );

            // update the compliance fields if required OTD# 2567
            if ( iUpdateCompliance )
            {
               UpdateCompliance( iPointRecord, CeResultSet );
            }

            //
            // Work around for OTD# 3214, @A can't handle a location tag of type RFID...
            // TODO: if @A ever supports RFID tag types... fix this
            //
            if ( iPointRecord.LocationMethod == (byte) FieldTypes.LocationType.RFID )
               CeResultSet.SetByte( mColumnOrdinal.LocationMethod, (byte) FieldTypes.LocationType.Barcode );
            else
               CeResultSet.SetByte( mColumnOrdinal.LocationMethod, iPointRecord.LocationMethod );

            if ( iPointRecord.LocationTag != null )
               CeResultSet.SetString( mColumnOrdinal.LocationTag, iPointRecord.LocationTag );

            if ( iPointRecord.LastModified.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.LastModified, iPointRecord.LastModified );

            CeResultSet.SetBoolean( mColumnOrdinal.DataSaved, iPointRecord.DataSaved );
            CeResultSet.SetByte( mColumnOrdinal.AlarmState, iPointRecord.AlarmState );
            CeResultSet.SetInt32( mColumnOrdinal.PointIndex, iPointRecord.PointIndex );
            CeResultSet.SetInt32( mColumnOrdinal.PointHostIndex, iPointRecord.PointHostIndex );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iPointRecord.Number );
            CeResultSet.SetByte( mColumnOrdinal.TagChanged, iPointRecord.TagChanged );
            CeResultSet.SetInt32( mColumnOrdinal.NoteCount, iPointRecord.GetNoteCount() );
            CeResultSet.SetInt32( mColumnOrdinal.RouteId, iPointRecord.RouteId );
            CeResultSet.SetByte( mColumnOrdinal.Skipped, iPointRecord.Skipped );

            // update the value fields
            UpdateValueFields( iPointRecord, CeResultSet );

            // write to the table
            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Table write and update support methods

      /// <summary>
      /// Write the compliance fields to the point record
      /// </summary>
      /// <param name="iPointObject">point object reference</param>
      /// <param name="newRecord">instantiated table record</param>
      private void WriteComplianceFields( PointRecord iPointObject,
         SqlCeUpdatableRecord newRecord )
      {
         // only continue here if we have a compliance value
         if ( iPointObject.Compliance != 0x00000000 )
         {
            // get the compliance calendar value
            FieldTypes.Calendar calendar = iPointObject.GetCalendarCompliance();

            // get the compliance window
            UInt16 complianceWindow = iPointObject.GetCalendarComplianceWindow();

            // write the compliance fields to the table
            newRecord[ mColumnOrdinal.Calendar ] = (byte) calendar;
            newRecord[ mColumnOrdinal.ComplianceWindow ] = complianceWindow;

            //
            // only write the start date if the compliance field is not
            // set to seconds (interval compliance) and there is actually
            // a valid start date to work with
            //
            if ( ( calendar != FieldTypes.Calendar.Second ) &&
               ( iPointObject.StartFrom.Ticks > 0 ) )
            {
               // first of all we need to refesh the object start date
               iPointObject.RefreshStartDate( calendar, complianceWindow );

               // now write to the table
               newRecord[ mColumnOrdinal.StartFrom ] = iPointObject.StartFrom;
            }
         }

      }/* end method */


      /// <summary>
      /// Write the historic Temp, Env and Vel values to the points table 
      /// </summary>
      /// <param name="iPointObject">point object reference</param>
      /// <param name="newRecord">instantiated table record</param>
      private void WriteValueFields( PointRecord iPointObject,
         SqlCeUpdatableRecord newRecord )
      {
         // add the last TempData measurement 
         if ( iPointObject.TempData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointObject.TempData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               // add either a numeric or text value
               if ( measurement.Measurement[ 0 ].Is_Numeric_Measurement() )
                  newRecord[ mColumnOrdinal.TempDataValue ] = measurement.Measurement[ 0 ].Value;
               else
                  newRecord[ mColumnOrdinal.TextDataValue ] = measurement.Measurement[ 0 ].TextValue;

               newRecord[ mColumnOrdinal.TempDataUTC ] = measurement.Measurement[ 0 ].LocalTime;
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               // add either a numeric or text value
               if ( measurement.Measurement[ 1 ].Is_Numeric_Measurement() )
                  newRecord[ mColumnOrdinal.LastTempDataValue ] = measurement.Measurement[ 1 ].Value;
               else
                  newRecord[ mColumnOrdinal.LastTextDataValue ] = measurement.Measurement[ 1 ].TextValue;

               newRecord[ mColumnOrdinal.LastTempDataUTC ] = measurement.Measurement[ 1 ].LocalTime;
            }
         }

         // add the last VelData measurement 
         if ( iPointObject.EnvData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointObject.EnvData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               newRecord[ mColumnOrdinal.EnvDataValue ] = measurement.Measurement[ 0 ].Value;
               newRecord[ mColumnOrdinal.EnvDataUTC ] = measurement.Measurement[ 0 ].LocalTime;
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               newRecord[ mColumnOrdinal.LastEnvDataValue ] = measurement.Measurement[ 1 ].Value;
               newRecord[ mColumnOrdinal.LastEnvDataUTC ] = measurement.Measurement[ 1 ].LocalTime;
            }
         }

         // add the last VelData measurement 
         if ( iPointObject.VelData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointObject.VelData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               newRecord[ mColumnOrdinal.VelDataValue ] = measurement.Measurement[ 0 ].Value;
               newRecord[ mColumnOrdinal.VelDataUTC ] = measurement.Measurement[ 0 ].LocalTime;
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               newRecord[ mColumnOrdinal.LastVelDataValue ] = measurement.Measurement[ 1 ].Value;
               newRecord[ mColumnOrdinal.LastVelDataUTC ] = measurement.Measurement[ 1 ].LocalTime;
            }
         }

      }/* end method */


      /// <summary>
      /// Update the compliance fields to the point record
      /// </summary>
      /// <param name="iPointObject">point object reference</param>
      /// <param name="newRecord">instantiated table record</param>
      private void UpdateCompliance( PointRecord iPointRecord, SqlCeResultSet CeResultSet )
      {
         // only continue here if we have a compliance value and have collected data
         if ( iPointRecord.Compliance != 0x00000000 )
         {
            // get the compliance calendar value
            FieldTypes.Calendar calendar = iPointRecord.GetCalendarCompliance();

            // get the compliance window
            UInt16 complianceWindow = iPointRecord.GetCalendarComplianceWindow();

            // update the compliance fields to the table
            CeResultSet.SetByte( mColumnOrdinal.Calendar, (byte) calendar );
            CeResultSet.SetInt16( mColumnOrdinal.ComplianceWindow, (short) complianceWindow );

            //
            // only update the start date if the compliance field is not
            // set to seconds (interval compliance) and there is actually
            // a valid start date to work with
            //
            if ( ( calendar != FieldTypes.Calendar.Second ) &&
               ( iPointRecord.StartFrom.Ticks > 0 ) )
            {
               // first of all we need to refesh the object start date
               iPointRecord.RefreshStartDate( calendar, complianceWindow );

               // now write to the table
               CeResultSet.SetDateTime( mColumnOrdinal.StartFrom, iPointRecord.StartFrom );
            }
         }

      }/* end method */


      /// <summary>
      /// Update the historic Temp, Env and Vel values to the points table 
      /// </summary>
      /// <param name="iPointObject">point object reference</param>
      /// <param name="newRecord">instantiated table record</param>
      private void UpdateValueFields( PointRecord iPointRecord, SqlCeResultSet CeResultSet )
      {
         // add the last TempData measurement 
         if ( iPointRecord.TempData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointRecord.TempData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.TempDataUTC, measurement.Measurement[ 0 ].LocalTime );
               if ( measurement.Measurement[ 0 ].Is_Numeric_Measurement() )
                  CeResultSet.SetDouble( mColumnOrdinal.TempDataValue, measurement.Measurement[ 0 ].Value );
               else
                  CeResultSet.SetString( mColumnOrdinal.TextDataValue, measurement.Measurement[ 0 ].TextValue );
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.LastTempDataUTC, measurement.Measurement[ 1 ].LocalTime );
               if ( measurement.Measurement[ 1 ].Is_Numeric_Measurement() )
                  CeResultSet.SetDouble( mColumnOrdinal.LastTempDataValue, measurement.Measurement[ 1 ].Value );
               else
                  CeResultSet.SetString( mColumnOrdinal.LastTextDataValue, measurement.Measurement[ 1 ].TextValue );
            }
         }

         // add the last VelData measurement 
         if ( iPointRecord.EnvData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointRecord.EnvData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.EnvDataUTC, measurement.Measurement[ 0 ].LocalTime );
               CeResultSet.SetDouble( mColumnOrdinal.EnvDataValue, measurement.Measurement[ 0 ].Value );
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.LastEnvDataUTC, measurement.Measurement[ 1 ].LocalTime );
               CeResultSet.SetDouble( mColumnOrdinal.LastEnvDataValue, measurement.Measurement[ 1 ].Value );
            }
         }

         // add the last EnvData measurement 
         if ( iPointRecord.VelData.Measurement.Count > 0 )
         {
            // get the two most recent point measurement objects
            EnumPointMeasurements measurement = 
                  LinqQueries.GetLatestMeasurements( iPointRecord.VelData, 2 );

            // save the most recent measurement
            if ( measurement.Measurement.Count > 0 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.VelDataUTC, measurement.Measurement[ 0 ].LocalTime );
               CeResultSet.SetDouble( mColumnOrdinal.VelDataValue, measurement.Measurement[ 0 ].Value );
            }

            // and also (if available) the second to last measurement
            if ( measurement.Measurement.Count > 1 )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.LastVelDataUTC, measurement.Measurement[ 1 ].LocalTime );
               CeResultSet.SetDouble( mColumnOrdinal.LastVelDataValue, measurement.Measurement[ 1 ].Value );
            }
         }

      }/* end method */

      //-------------------------------------------------------------------------

      #endregion

   }/* end class */

}/* end namespace */


//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 5.0 APinkerton 17th September 2012
//  Add support for TextData POINT type (TempData measurements only)
//
//  Revision 4.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 3.0 APinkerton 27th April 2010
//  Added support for Calendar based overdue reporting
//
//  Revision 2.0 APinkerton 27th April 2010
//  Added support for saving enumerated points list
//
//  Revision 1.0 APinkerton 10th April 2010
//  Added support for second measurement
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//-------------------------------------------------------------------------------