﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the MCC table. This structure should be revised following any
    /// changes made to this table.
    /// </summary>
    public class RowOrdinalsMCC
    {
        public int Uid { get; set; }
        public int Number { get; set; }
        public int PointUid { get; set; }
        public int EnvAlarmState { get; set; }
        public int EnvAlarm1 { get; set; }
        public int EnvAlarm2 { get; set; }
        public int EnvBaselineDataTime { get; set; }
        public int EnvBaselineDataValue { get; set; }
        public int VelAlarmState { get; set; }
        public int VelAlarm1 { get; set; }
        public int VelAlarm2 { get; set; }
        public int VelBaselineDataTime { get; set; }
        public int VelBaselineDataValue { get; set; }
        public int TmpAlarmState { get; set; }
        public int TmpAlarm1 { get; set; }
        public int TmpAlarm2 { get; set; }
        public int TmpBaselineDataTime { get; set; }
        public int TmpBaselineDataValue { get; set; }
        public int SystemUnits { get; set; }
        public int TempUnits { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsMCC(SqlCeResultSet iCeResultSet)
        {
            GetMCCTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// MCC table. This table will need to be revised in the event of any changes
        /// being made to the MCC table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetMCCTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.Number = iCeResultSet.GetOrdinal("Number");
            this.PointUid = iCeResultSet.GetOrdinal("PointUid");
            this.EnvAlarmState = iCeResultSet.GetOrdinal("EnvAlarmState");
            this.EnvAlarm1 = iCeResultSet.GetOrdinal("EnvAlarm1");
            this.EnvAlarm2 = iCeResultSet.GetOrdinal("EnvAlarm2");
            this.EnvBaselineDataTime = iCeResultSet.GetOrdinal("EnvBaselineDataTime");
            this.EnvBaselineDataValue = iCeResultSet.GetOrdinal("EnvBaselineDataValue");
            this.VelAlarmState = iCeResultSet.GetOrdinal("VelAlarmState");
            this.VelAlarm1 = iCeResultSet.GetOrdinal("VelAlarm1");
            this.VelAlarm2 = iCeResultSet.GetOrdinal("VelAlarm2");
            this.VelBaselineDataTime = iCeResultSet.GetOrdinal("VelBaselineDataTime");
            this.VelBaselineDataValue = iCeResultSet.GetOrdinal("VelBaselineDataValue");
            this.TmpAlarmState = iCeResultSet.GetOrdinal("TmpAlarmState");
            this.TmpAlarm1 = iCeResultSet.GetOrdinal("TmpAlarm1");
            this.TmpAlarm2 = iCeResultSet.GetOrdinal("TmpAlarm2");
            this.TmpBaselineDataTime = iCeResultSet.GetOrdinal("TmpBaselineDataTime");
            this.TmpBaselineDataValue = iCeResultSet.GetOrdinal("TmpBaselineDataValue");
            this.SystemUnits = iCeResultSet.GetOrdinal("SystemUnits");
            this.TempUnits = iCeResultSet.GetOrdinal("TempUnits");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------