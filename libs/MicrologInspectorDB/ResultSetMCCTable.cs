﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the MCC Table ResultSet
   /// </summary>
   public class ResultSetMCCTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsMCC mColumnOrdinal = null;


      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM MCC " +
          "WHERE PointUid = @PointUid";

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_ENUM_BY_POINT_UID = "SELECT * FROM MCC " +
          "WHERE ";

      #endregion


      #region Direct Table Access (i.e. without reference to the base class properties)

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_MCC;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single MCCRecord object to the MCC table and also update 
      /// the progress bar updates object
      /// </summary>
      /// <param name="iMCCRecord">Enumerated List of NodeRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( MCCRecord iMCCRecord )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MCC";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               if ( !WriteRecord( iMCCRecord, ceResultSet ) )
               {
                  result = false;
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of MCCRecord objects to the MCC table. 
      /// </summary>
      /// <param name="iEnumMCC">Enumerated List of NodeRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumMCC iEnumMCC, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the Inspection objects list
         iEnumMCC.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MCC";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( MCCRecord MCC in iEnumMCC )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( MCC, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of MCCRecord objects to the MCC table. 
      /// </summary>
      /// <param name="iEnumMCC">Enumerated List of NodeRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumMCC iEnumMCC )
      {
         OpenCeConnection();

         // reset the Inspection objects list
         iEnumMCC.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MCC";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( MCCRecord MCC in iEnumMCC )
               {
                  if ( !WriteRecord( MCC, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Populate and return a single MCC Record object given the associated
      /// Point Uid.
      /// </summary>
      /// <param name="iPointUid">Point Uid guid</param>
      /// <returns>Populated MCCRecord object</returns>
      public MCCRecord GetRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // create the default object
         MCCRecord mcc = new MCCRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;
            command.CommandType = CommandType.Text;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  the MCC table. As this is a 'one shot' the column 
               // ordinal object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // populate the process object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref mcc );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mcc;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all MCC records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumMCC GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumMCC MCCs = new EnumMCC();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MCC";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MCC table. As this is a 'one shot' the column ordinal object 
               // remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MCCRecord MCC = new MCCRecord();
                  PopulateObject( ceResultSet, ref MCC );
                  if ( MCC != null )
                     MCCs.AddMCCRecord( MCC );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return MCCs;

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Return an enumerated list of populated MCC objects given an array of 
      /// Parent Uid (guids). 
      /// </summary>
      /// <param name="iPointUids">Array of Point Uid guids</param>
      /// <returns>Enumerated list of populated Process objects</returns>
      public EnumMCC GetEnumMCCRecords( Guid[] iPointUids )
      {
         OpenCeConnection();

         // create new enumerated list of Process objects
         EnumMCC enumMCC = new EnumMCC();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( Guid pointUid in iPointUids )
            {
               // if this is not the last parameter then add an additional OR clause
               if ( paramCount < iPointUids.Length )
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
               }
               // otherwise terminate query with semi-colon
               else
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() +
                      " ORDER BY Number;" );
               }
               command.Parameters.Add( "@P" + paramCount.ToString(), SqlDbType.UniqueIdentifier ).Value = pointUid;

               // increment the parameter count
               ++paramCount;
            }

            // referebce the full command text
            command.CommandText = query.ToString();

            try
            {
               // execure the result set
               using ( SqlCeResultSet ceResultSet = 
                  command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the PROCESS table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // convert Process record to a ProcessRecord object
                  while ( ceResultSet.Read() )
                  {
                     MCCRecord mcc = new MCCRecord();
                     PopulateObject( ceResultSet, ref mcc );
                     enumMCC.AddMCCRecord( mcc );
                  }
                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               enumMCC = null;
            }
         }
         CloseCeConnection();
         return enumMCC;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of populated MCC objects given an 
      /// enumerated list of populated Point objects.
      /// </summary>
      /// <param name="iPointUids">Enumerated list of Point objects</param>
      /// <returns>Enumerated list of populated Process objects</returns>
      public EnumMCC GetEnumMCCRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // create new enumerated list of Process objects
         EnumMCC enumMCC = new EnumMCC();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( PointRecord point in iEnumPoints )
            {
               // ensure we are dealing with an MCC type
               if ( IsPointMCCType( point ) )
               {
                  // if this is not the last parameter then add an additional OR clause
                  if ( paramCount < iEnumPoints.Count )
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
                  }
                  // otherwise terminate query with semi-colon
                  else
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() +
                         " ORDER BY Number;" );
                  }

                  // add the new parameter object
                  command.Parameters.Add( "@P" + paramCount.ToString(),
                      SqlDbType.UniqueIdentifier ).Value = point.Uid;

                  // increment the parameter count
                  ++paramCount;
               }
            }

            // if no process points found then just return null!
            if ( paramCount > 1 )
            {
               // referebce the full command text
               command.CommandText = query.ToString();

               try
               {
                  // execure the result set
                  using ( SqlCeResultSet ceResultSet = 
                     command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated with 
                     // the PROCESS table. As this is a 'one shot' the column ordinal 
                     // object remains a singleton
                     //
                     GetTableColumns( ceResultSet );

                     // convert Process record to a ProcessRecord object
                     while ( ceResultSet.Read() )
                     {
                        MCCRecord mcc = new MCCRecord();
                        PopulateObject( ceResultSet, ref mcc );
                        enumMCC.AddMCCRecord( mcc );
                     }
                     // close the resultSet
                     ceResultSet.Close();
                  }
               }
               catch
               {
                  enumMCC = null;
               }
            }
         }
         CloseCeConnection();
         return enumMCC;

      }/* end method */

      #endregion


      #region Private Methods

      /// <summary>
      /// Returns true if the PointRecord object is a MCC Type
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <returns>True if type is MCC</returns>
      private Boolean IsPointMCCType( PointRecord iPointRecord )
      {
         FieldTypes.ProcessType prType = (FieldTypes.ProcessType) iPointRecord.ProcessType;

         if ( prType == FieldTypes.ProcessType.MCD )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// This method populates and returns the column ordinals associated 
      /// with the MCC table. This table will need to be revised in the event
      /// of any changes being made to the MCC table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsMCC( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new MCC record and add it to 
      /// the current MCC table.
      /// </summary>
      /// <param name="iProcessObject">Populated MCC Record object</param>
      /// <param name="ceResultSet">Instantiated MCC object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( MCCRecord iMCC,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iMCC.Uid;
            newRecord[ mColumnOrdinal.Number ] = iMCC.Number;
            newRecord[ mColumnOrdinal.PointUid ] = iMCC.PointUid;
            newRecord[ mColumnOrdinal.EnvAlarmState ] = iMCC.EnvAlarmState;
            newRecord[ mColumnOrdinal.EnvAlarm1 ] = iMCC.EnvAlarm1;
            newRecord[ mColumnOrdinal.EnvAlarm2 ] = iMCC.EnvAlarm2;

            if ( iMCC.EnvBaselineDataTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.EnvBaselineDataTime ] = iMCC.EnvBaselineDataTime;

            newRecord[ mColumnOrdinal.EnvBaselineDataValue ] = iMCC.EnvBaselineDataValue;
            newRecord[ mColumnOrdinal.VelAlarmState ] = iMCC.VelAlarmState;
            newRecord[ mColumnOrdinal.VelAlarm1 ] = iMCC.VelAlarm1;
            newRecord[ mColumnOrdinal.VelAlarm2 ] = iMCC.VelAlarm2;

            if ( iMCC.VelBaselineDataTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.VelBaselineDataTime ] = iMCC.VelBaselineDataTime;

            newRecord[ mColumnOrdinal.VelBaselineDataValue ] = iMCC.VelBaselineDataValue;
            newRecord[ mColumnOrdinal.TmpAlarmState ] = iMCC.TmpAlarmState;
            newRecord[ mColumnOrdinal.TmpAlarm1 ] = iMCC.TmpAlarm1;
            newRecord[ mColumnOrdinal.TmpAlarm2 ] = iMCC.TmpAlarm2;

            if ( iMCC.TmpBaselineDataTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.TmpBaselineDataTime ] = iMCC.TmpBaselineDataTime;

            newRecord[ mColumnOrdinal.TmpBaselineDataValue ] = iMCC.TmpBaselineDataValue;
            newRecord[ mColumnOrdinal.SystemUnits ] = iMCC.SystemUnits;
            newRecord[ mColumnOrdinal.TempUnits ] = iMCC.TempUnits;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single MCC table record
      /// </summary>
      /// <param name="iMCCRecord">Updated MCCRecord object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( MCCRecord iMCCRecord,
          SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iMCCRecord.Uid );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iMCCRecord.Number );
            CeResultSet.SetGuid( mColumnOrdinal.PointUid, iMCCRecord.PointUid );
            CeResultSet.SetByte( mColumnOrdinal.EnvAlarmState, iMCCRecord.EnvAlarmState );
            CeResultSet.SetDouble( mColumnOrdinal.EnvAlarm1, iMCCRecord.EnvAlarm1 );
            CeResultSet.SetDouble( mColumnOrdinal.EnvAlarm2, iMCCRecord.EnvAlarm2 );

            if ( iMCCRecord.EnvBaselineDataTime.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.EnvBaselineDataTime, iMCCRecord.EnvBaselineDataTime );

            CeResultSet.SetDouble( mColumnOrdinal.EnvBaselineDataValue, iMCCRecord.EnvBaselineDataValue );
            CeResultSet.SetByte( mColumnOrdinal.VelAlarmState, iMCCRecord.VelAlarmState );
            CeResultSet.SetDouble( mColumnOrdinal.VelAlarm1, iMCCRecord.VelAlarm1 );
            CeResultSet.SetDouble( mColumnOrdinal.VelAlarm2, iMCCRecord.VelAlarm2 );

            if ( iMCCRecord.VelBaselineDataTime.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.VelBaselineDataTime, iMCCRecord.VelBaselineDataTime );

            CeResultSet.SetDouble( mColumnOrdinal.VelBaselineDataValue, iMCCRecord.VelBaselineDataValue );
            CeResultSet.SetByte( mColumnOrdinal.TmpAlarmState, iMCCRecord.TmpAlarmState );
            CeResultSet.SetDouble( mColumnOrdinal.TmpAlarm1, iMCCRecord.TmpAlarm1 );
            CeResultSet.SetDouble( mColumnOrdinal.TmpAlarm2, iMCCRecord.TmpAlarm2 );

            if ( iMCCRecord.TmpBaselineDataTime.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.TmpBaselineDataTime, iMCCRecord.TmpBaselineDataTime );

            CeResultSet.SetDouble( mColumnOrdinal.TmpBaselineDataValue, iMCCRecord.TmpBaselineDataValue );
            CeResultSet.SetByte( mColumnOrdinal.SystemUnits, iMCCRecord.SystemUnits );
            CeResultSet.SetByte( mColumnOrdinal.TempUnits, iMCCRecord.TempUnits );

            // update the record
            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated MCCRecord Object with data from a single 
      /// MCC table record. In the event of a raised exception the returned 
      /// MCC object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMCCRecord">MCC object to be populated</param>
      private void PopulateObject( SqlCeResultSet CeResultSet,
          ref MCCRecord iMCCRecord )
      {
         try
         {
            iMCCRecord.Uid = CeResultSet.GetGuid( mColumnOrdinal.Uid );
            iMCCRecord.Number = CeResultSet.GetInt32( mColumnOrdinal.Number );
            iMCCRecord.PointUid = CeResultSet.GetGuid( mColumnOrdinal.PointUid );
            iMCCRecord.EnvAlarmState = CeResultSet.GetByte( mColumnOrdinal.EnvAlarmState );
            iMCCRecord.EnvAlarm1 = CeResultSet.GetDouble( mColumnOrdinal.EnvAlarm1 );
            iMCCRecord.EnvAlarm2 = CeResultSet.GetDouble( mColumnOrdinal.EnvAlarm2 );

            if ( CeResultSet[ mColumnOrdinal.EnvBaselineDataTime ] != DBNull.Value )
               iMCCRecord.EnvBaselineDataTime = CeResultSet.GetDateTime( mColumnOrdinal.EnvBaselineDataTime );

            iMCCRecord.EnvBaselineDataValue = CeResultSet.GetDouble( mColumnOrdinal.EnvBaselineDataValue );
            iMCCRecord.VelAlarmState = CeResultSet.GetByte( mColumnOrdinal.VelAlarmState );
            iMCCRecord.VelAlarm1 = CeResultSet.GetDouble( mColumnOrdinal.VelAlarm1 );
            iMCCRecord.VelAlarm2 = CeResultSet.GetDouble( mColumnOrdinal.VelAlarm2 );

            if ( CeResultSet[ mColumnOrdinal.VelBaselineDataTime ] != DBNull.Value )
               iMCCRecord.VelBaselineDataTime = CeResultSet.GetDateTime( mColumnOrdinal.VelBaselineDataTime );

            iMCCRecord.VelBaselineDataValue = CeResultSet.GetDouble( mColumnOrdinal.VelBaselineDataValue );
            iMCCRecord.TmpAlarmState = CeResultSet.GetByte( mColumnOrdinal.TmpAlarmState );
            iMCCRecord.TmpAlarm1 = CeResultSet.GetDouble( mColumnOrdinal.TmpAlarm1 );
            iMCCRecord.TmpAlarm2 = CeResultSet.GetDouble( mColumnOrdinal.TmpAlarm2 );

            if ( CeResultSet[ mColumnOrdinal.TmpBaselineDataTime ] != DBNull.Value )
               iMCCRecord.TmpBaselineDataTime = CeResultSet.GetDateTime( mColumnOrdinal.TmpBaselineDataTime );

            iMCCRecord.TmpBaselineDataValue = CeResultSet.GetDouble( mColumnOrdinal.TmpBaselineDataValue );
            iMCCRecord.SystemUnits = CeResultSet.GetByte( mColumnOrdinal.SystemUnits );
            iMCCRecord.TempUnits = CeResultSet.GetByte( mColumnOrdinal.TempUnits );
         }
         catch
         {
            iMCCRecord = null;
         }

      }/* end method */


      #endregion


   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------