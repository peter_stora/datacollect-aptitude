﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Use this interface to implement query level connection
   /// to each of the Microlog Inspector database tables
   /// </summary>
   public interface ITableConnect
   {
      /// <summary>
      /// Gets or Sets the C_NOTE Table ResultSet
      /// </summary>
      ResultSetCodedNoteTable CodedNote
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CONDITIONALPOINT Table ResultSet
      /// </summary>
      ResultSetConditionalPointTable ConditionalPoint
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CMMS CORRECTIVEACTION Table ResultSet
      /// </summary>
      ResultSetCorrectiveAction CorrectiveAction
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the DERIVEDITEMS Table ResultSet
      /// </summary>
      ResultSetDerivedItemsTable DerivedItems
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the DERIVEDPOINT Table ResultSet
      /// </summary>
      ResultSetDerivedPointTable DerivedPoint
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the DEVICEPROFILE Table ResultSet
      /// </summary>
      ResultSetDeviceProfileTable DeviceProfile
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the ENVDATA Table ResultSet
      /// </summary>
      ResultSetEnvDataTable EnvData
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the FFTCHANNEL Table ResultSet
      /// </summary>
      ResultSetFFTChannelTable FFTChannel
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the FFTPOINT Table ResultSet
      /// </summary>
      ResultSetFFTPointTable FFTPoint
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the INSPECTION Table ResultSet
      /// </summary>
      ResultSetInspectionTable Inspection
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the INSTRUCTIONS Table ResultSet
      /// </summary>
      ResultSetInstructionsTable Instructions
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the LOCALSETTINGS Table ResultSet
      /// </summary>
      ResultSetUserPreferences UserPreferences
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the LOG Table ResultSet
      /// </summary>
      ResultSetLogTable LogTable
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the Operator MACHINENOPSKIPS Skips Table ResultSet
      /// </summary>
      ResultSetMachineNOpSkips MachineNOpSkips
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the Operator MACHINEOKSKIPS Table ResultSet
      /// </summary>
      ResultSetMachineOKSkips MachineOkSkips
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the MCC Table ResultSet
      /// </summary>
      ResultSetMCCTable Mcc
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the Operator MESSAGINGPREFS Table ResultSet
      /// </summary>
      ResultSetMessagePrefs MessagePreferences
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the MESSAGE Table ResultSet
      /// </summary>
      ResultSetMessageTable Message
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the NODE Table ResultSet
      /// </summary>
      ResultSetNodeTable Node
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the C_NOTES Table ResultSet
      /// </summary>
      ResultSetNotesTable Notes
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the OPERATORS Table ResultSet
      /// </summary>
      ResultSetOperators Operators
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the POINTS Table ResultSet
      /// </summary>
      ResultSetPointsTable Points
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CMMS PRIORITY Table ResultSet
      /// </summary>
      ResultSetPriority Priority
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CMMS PROBLEM Table ResultSet
      /// </summary>
      ResultSetProblem Problem
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the PROCESS Table ResultSet
      /// </summary>
      ResultSetProcessTable Process
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the TEXTDATA Table ResultSet
      /// </summary>
      ResultSetTextDataTable TextData
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the REFERENCEMEDIA Table ResultSet
      /// </summary>
      ResultSetReferenceMediaTable ReferenceMedia
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the SAVEDMEDIA Table ResultSet
      /// </summary>
      ResultSetSavedMediaTable SavedMedia
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CODEDNOTES Table ResultSet
      /// </summary>
      ResultSetSavedCodedNotesTable SavedCodedNotes
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the STRUCTUREDLOG Table ResultSet
      /// </summary>
      ResultSetStructuredLogTable StructuredLog
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the STRUCTURED Table ResultSet
      /// </summary>
      ResultSetStructuredTable Structured
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the TEMPDATA Table ResultSet
      /// </summary>
      ResultSetTempDataTable TempData
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the VELDATA Table ResultSet
      /// </summary>
      ResultSetVelDataTable VelData
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CMMS WORKNOTIFICATION Table ResultSet
      /// </summary>
      ResultSetWorkNotification WorkNotification
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or Sets the CMMS WORKTYPE Table ResultSet
      /// </summary>
      ResultSetWorkType WorkType
      {
         get;
         set;
      }

      /// <summary>
      /// Checks whether the database version is OK.
      /// </summary>
      /// <returns></returns>
      Boolean DatabaseVersionOK
      {
         get;
      }

      /// <summary>
      /// Gets the current database version
      /// </summary>
      string CurrentDbVersion
      {
         get;
      }

      /// <summary>
      /// Test the valididy of the SQL connection
      /// </summary>
      /// <returns>false on connection error</returns>
      Boolean TestConnection();

      /// <summary>
      /// Release object
      /// </summary>
      void Dispose();

   }/* end Interface */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 3.0 APinkerton 11th September 2012
//  Add support for TextData
//
//  Revision 2.0 APinkerton 25th February 2011
//  Add support for database version control
//
//  Revision 1.0 APinkerton 4th March 2010
//  Add support for the new LocalSettings table
//
//  Revision 0.0 APinkerton 9th October 2009
//  Add to project
//----------------------------------------------------------------------------