﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the POINTS table. This structure should be revised following any
   /// changes made to this table.
   /// </summary>
   public class RowOrdinalsPoint
   {
      public int Uid
      {
         get;
         set;
      }
      public int Id
      {
         get;
         set;
      }
      public int NodeUid
      {
         get;
         set;
      }
      public int Description
      {
         get;
         set;
      }
      public int ProcessType
      {
         get;
         set;
      }
      public int ScheduleDays
      {
         get;
         set;
      }
      public int LocationMethod
      {
         get;
         set;
      }
      public int LocationTag
      {
         get;
         set;
      }
      public int LastModified
      {
         get;
         set;
      }
      public int DataSaved
      {
         get;
         set;
      }
      public int AlarmState
      {
         get;
         set;
      }
      public int PointIndex
      {
         get;
         set;
      }
      public int PointHostIndex
      {
         get;
         set;
      }
      public int Number
      {
         get;
         set;
      }
      public int TagChanged
      {
         get;
         set;
      }
      public int NoteCount
      {
         get;
         set;
      }
      public int RouteId
      {
         get;
         set;
      }
      public int Skipped
      {
         get;
         set;
      }
      public int TempDataValue
      {
         get;
         set;
      }
      public int TempDataUTC
      {
         get;
         set;
      }
      public int EnvDataValue
      {
         get;
         set;
      }
      public int EnvDataUTC
      {
         get;
         set;
      }
      public int VelDataValue
      {
         get;
         set;
      }
      public int VelDataUTC
      {
         get;
         set;
      }
      public int TextDataValue
      {
         get;
         set;
      }
      public int LastTempDataValue
      {
         get;
         set;
      }
      public int LastTempDataUTC
      {
         get;
         set;
      }
      public int LastEnvDataValue
      {
         get;
         set;
      }
      public int LastEnvDataUTC
      {
         get;
         set;
      }
      public int LastVelDataValue
      {
         get;
         set;
      }
      public int LastVelDataUTC
      {
         get;
         set;
      }
      public int LastTextDataValue
      {
         get;
         set;
      }
      public int Calendar
      {
         get;
         set;
      }
      public int ComplianceWindow
      {
         get;
         set;
      }
      public int StartFrom
      {
         get;
         set;
      }


      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsPoint( SqlCeResultSet iCeResultSet )
      {
         GetPointTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// POINTS table. This table will need to be revised in the event of any changes
      /// being made to the POINTS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetPointTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.Id = iCeResultSet.GetOrdinal( "Id" );
         this.NodeUid = iCeResultSet.GetOrdinal( "NodeUid" );
         this.Description = iCeResultSet.GetOrdinal( "Description" );
         this.ProcessType = iCeResultSet.GetOrdinal( "ProcessType" );
         this.ScheduleDays = iCeResultSet.GetOrdinal( "ScheduleDays" );
         this.LocationMethod = iCeResultSet.GetOrdinal( "LocationMethod" );
         this.LocationTag = iCeResultSet.GetOrdinal( "LocationTag" );
         this.LastModified = iCeResultSet.GetOrdinal( "LastModified" );
         this.DataSaved = iCeResultSet.GetOrdinal( "DataSaved" );
         this.AlarmState = iCeResultSet.GetOrdinal( "AlarmState" );
         this.PointIndex = iCeResultSet.GetOrdinal( "PointIndex" );
         this.PointHostIndex = iCeResultSet.GetOrdinal( "PointHostIndex" );
         this.Number = iCeResultSet.GetOrdinal( "Number" );
         this.TagChanged = iCeResultSet.GetOrdinal( "TagChanged" );
         this.NoteCount = iCeResultSet.GetOrdinal( "NoteCount" );
         this.RouteId = iCeResultSet.GetOrdinal( "RouteId" );
         this.Skipped = iCeResultSet.GetOrdinal( "Skipped" );
         this.TempDataValue = iCeResultSet.GetOrdinal("TempDataValue");
         this.TempDataUTC = iCeResultSet.GetOrdinal( "TempDataUTC" );
         this.EnvDataValue = iCeResultSet.GetOrdinal( "EnvDataValue" );
         this.EnvDataUTC = iCeResultSet.GetOrdinal( "EnvDataUTC" );
         this.VelDataValue = iCeResultSet.GetOrdinal( "VelDataValue" );
         this.VelDataUTC = iCeResultSet.GetOrdinal( "VelDataUTC" );
         this.TextDataValue = iCeResultSet.GetOrdinal( "TextDataValue" );
         this.LastTempDataValue = iCeResultSet.GetOrdinal( "LastTempDataValue" );
         this.LastTempDataUTC = iCeResultSet.GetOrdinal( "LastTempDataUTC" );
         this.LastEnvDataValue = iCeResultSet.GetOrdinal( "LastEnvDataValue" );
         this.LastEnvDataUTC = iCeResultSet.GetOrdinal( "LastEnvDataUTC" );
         this.LastVelDataValue = iCeResultSet.GetOrdinal( "LastVelDataValue" );
         this.LastVelDataUTC = iCeResultSet.GetOrdinal( "LastVelDataUTC" );
         this.LastTextDataValue = iCeResultSet.GetOrdinal( "LastTextDataValue" );
         this.Calendar = iCeResultSet.GetOrdinal( "Calendar" );
         this.ComplianceWindow = iCeResultSet.GetOrdinal( "ComplianceWindow" );
         this.StartFrom = iCeResultSet.GetOrdinal( "StartFrom" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 27th April 2011
//  Added support for Calendar based overdue reporting
//
//  Revision 1.0 APinkerton 10th April 2010
//  Added second TEMP, ENV and VEL cache data fields
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------