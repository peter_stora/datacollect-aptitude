﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the MESSAGE Table ResultSet
   /// </summary>
   public class ResultSetMessageTable : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsMessage mColumnOrdinal = null;

      /// <summary>
      /// All Messages based on Node Uid 
      /// </summary>
      private const string QUERY_ALL_BY_NODE_UID = "SELECT * FROM MESSAGE " +
          "WHERE NodeUid = @NodeUid";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_MESSAGE;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current MESSAGE table. 
      /// </summary>
      /// <param name="iMessage">populated Message object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( MessageRecord iMessage )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write to table
               result = WriteRecord( iMessage, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of MessageRecord objects to the MESSAGE 
      /// table and return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of MessageRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumMessages iMessages,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the available records
               foreach ( MessageRecord message in iMessages )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( message, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of MessageRecord objects to the MESSAGE table
      /// </summary>
      /// <param name="iMediaRecords">Enumerated List of Message objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumMessages iMessages )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGE";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the available records
               foreach ( MessageRecord message in iMessages )
               {
                  if ( !WriteRecord( message, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all Message object types 
      /// given the Unique Identifier of the parent machine or Point node.
      /// </summary>
      /// <param name="iNodeUid">Unique machine node identifier</param>
      /// <returns>enumerated list of Message objects</returns>
      public EnumMessageRecords GetRecord( Guid iNodeUid )
      {
         OpenCeConnection();

         // create the object result set
         EnumMessageRecords messageRecords = new EnumMessageRecords();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ALL_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@NodeUid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MessageRecord message = new MessageRecord();
                  PopulateObject( ceResultSet, ref message );
                  messageRecords.AddMessageRecord( message );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return messageRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all Message object types 
      /// given the Unique Identifier of the parent machine or Point node.
      /// </summary>
      /// <param name="iNodeUid">Unique machine node identifier</param>
      /// <returns>enumerated list of Message objects</returns>
      public EnumMessages GetRecords( Guid iNodeUid )
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumMessages messages = new EnumMessages();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ALL_BY_NODE_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@NodeUid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MessageRecord message = new MessageRecord();
                  PopulateObject( ceResultSet, ref message );
                  messages.AddMessageRecord( message );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return messages;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all MESSAGE table records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumMessages GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumMessages messageRecords = new EnumMessages();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "MESSAGE";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  MessageRecord message = new MessageRecord();
                  PopulateObject( ceResultSet, ref message );
                  messageRecords.AddMessageRecord( message );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return messageRecords;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with
      /// the MESSAGE table. This table will need to be revised in the event of
      /// any changes being made to the MESSAGE table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsMessage( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new MessageRecord and add it 
      /// to the current MESSAGE table.
      /// </summary>
      /// <param name="iMessage">Populated Message Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( MessageRecord iMessage,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iMessage.Uid;
            newRecord[ mColumnOrdinal.NodeUid ] = iMessage.NodeUid;

            if ( iMessage.ID != null )
               newRecord[ mColumnOrdinal.ID ] = iMessage.ID;
            else
               newRecord[ mColumnOrdinal.ID ] = string.Empty;
            
            newRecord[ mColumnOrdinal.Number ] = iMessage.Number;

            if ( iMessage.Text != null )
               newRecord[ mColumnOrdinal.Text ] = iMessage.Text;
            else
               newRecord[ mColumnOrdinal.Text ] = string.Empty;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated MessageRecord Object with data from a 
      /// sinble MESSAGE table record. In the event of a raised exception 
      /// the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMessage">Message object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref MessageRecord iMessage )
      {
         try
         {
            // recast from nvarchar to guid OTD #4573
            if ( ceResultSet[ mColumnOrdinal.Uid ] != DBNull.Value )
            {
               string Uid = ceResultSet.GetString( mColumnOrdinal.Uid );
               iMessage.Uid = new Guid( Uid );
            }

            if ( ceResultSet[ mColumnOrdinal.NodeUid ] != DBNull.Value )
            {
               string NodeUid = ceResultSet.GetString( mColumnOrdinal.NodeUid );
               iMessage.NodeUid = new Guid( NodeUid );
            }

            if ( ceResultSet[ mColumnOrdinal.ID ] != DBNull.Value )
            {
               iMessage.ID = ceResultSet.GetString( mColumnOrdinal.ID );
            }
            
            iMessage.Number = ceResultSet.GetInt32( mColumnOrdinal.Number );

            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
            {
               iMessage.Text = ceResultSet.GetString( mColumnOrdinal.Text );
            }
         }
         catch
         {
            iMessage = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  ResultSetMessageTable.cs
//
//  Revision 1.0 APinkerton 28th November 2012
//  Add method GetRecords()
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------