﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the STRUCTUREDLOG table. This structure should be revised following 
    /// any changes made to this table.
    /// </summary>
    public class RowOrdinalsStructuredLog
    {
        public int RouteId { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int Duration { get; set; }
        public int OperId { get; set; }
        public int PercentComplete { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsStructuredLog(SqlCeResultSet iCeResultSet)
        {
            GetMessageTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with 
        /// the STRUCTUREDLOG table. This table will need to be revised in the event
        /// of any changes being made to the STRUCTUREDLOG table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetMessageTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.RouteId = iCeResultSet.GetOrdinal("RouteId");
            this.StartTime = iCeResultSet.GetOrdinal("StartTime");
            this.EndTime = iCeResultSet.GetOrdinal("EndTime");
            this.Duration = iCeResultSet.GetOrdinal("Duration");
            this.OperId = iCeResultSet.GetOrdinal("OperId");
            this.PercentComplete = iCeResultSet.GetOrdinal("PercentComplete");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------