﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the SAVEDMEDIA table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsSavedMedia
   {
      public int Uid
      {
         get;
         set;
      }
      
      public int PointHostIndex
      {
         get;
         set;
      }

      public int WNID
      {
         get;
         set;
      }

      public int TimeStamp
      {
         get;
         set;
      }

      public int UTM
      {
         get;
         set;
      }
     
      public int MediaType
      {
         get;
         set;
      }

      public int MediaOrigin
      {
         get;
         set;
      }
     
      public int FileName
      {
         get;
         set;
      }
     
      public int FilePath
      {
         get;
         set;
      }
      
      public int Note
      {
         get;
         set;
      }
     
      public int Uploaded
      {
         get;
         set;
      }

      public int OperId
      {
         get;
         set;
      }
      
      public int Size
      {
         get;
         set;
      }

      public int CRC32
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsSavedMedia( SqlCeResultSet iCeResultSet )
      {
         GetSavedMediaTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// SAVEDMEDIA table. This table will need to be revised in the event of 
      /// any changes being made to the SAVEDMEDIA table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetSavedMediaTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.PointHostIndex = iCeResultSet.GetOrdinal( "PointHostIndex" );
         this.WNID = iCeResultSet.GetOrdinal("WNID");
         this.TimeStamp = iCeResultSet.GetOrdinal("TimeStamp");
         this.UTM = iCeResultSet.GetOrdinal( "UTM" );
         this.MediaType = iCeResultSet.GetOrdinal( "MediaType" );
         this.MediaOrigin = iCeResultSet.GetOrdinal( "MediaOrigin" );
         this.FileName = iCeResultSet.GetOrdinal( "FileName" );
         this.FilePath = iCeResultSet.GetOrdinal( "FilePath" );
         this.Note = iCeResultSet.GetOrdinal( "Note" );
         this.Uploaded= iCeResultSet.GetOrdinal( "Uploaded" );
         this.OperId = iCeResultSet.GetOrdinal( "OperId" );
         this.Size = iCeResultSet.GetOrdinal( "Size" );
         this.CRC32 = iCeResultSet.GetOrdinal( "CRC32" );
      
      }/* end method */

   }/* end class */

}/* end namespace*/


//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15th September 2011
//  New fields
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//-------------------------------------------------------------------------------