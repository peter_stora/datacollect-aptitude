﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;


namespace SKF.RS.MicrologInspector.Database
{    
    /// <summary>
    /// Simple base class for all ResultSet table connections
    /// </summary>
    public class ResultSetBase
    {
        /// <summary>
        /// Static method to test the valididy of the SQL connection
        /// </summary>
        /// <returns>false on connection error</returns>
        public static Boolean TestConnection(SqlConnectionObject iSqlCeConnect)
        {
            try
            {
                if (iSqlCeConnect.DirectCeConnection.State == ConnectionState.Closed)
                {
                    iSqlCeConnect.DirectCeConnection.Open();
                    iSqlCeConnect.DirectCeConnection.Close();
                }
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Failed testconnection: " + ex.ToString());
                return false;
            }
        }
        
        
        /// <summary>
        /// Reference to the SqlConnection object
        /// </summary>
        protected SqlConnectionObject mSqlCeConnect = null;


        /// <summary>
        /// Read only property: Get the current connection
        /// </summary>
        protected SqlCeConnection CeConnection
        {
            get { return mSqlCeConnect.DirectCeConnection; }
            set
            {
                if (value == null)
                {
                    
                }
                mSqlCeConnect.DirectCeConnection = value;
            }
        }


        /// <summary>
        /// Read only property: Get the default ResultSet options
        /// </summary>
        protected ResultSetOptions resultSetOptions
        {
            get { return mSqlCeConnect.ResultSetOptions; }
        }


        /// <summary>
        /// Read only property: Get connection state of the Database
        /// </summary>
        protected Boolean Connected
        {
            get { return mSqlCeConnect.Connected(); }
        }


        /// <summary>
        /// Open the connection (if using unique connections)
        /// </summary>
        protected Boolean OpenCeConnection()
        {
            try
            {
                if (CeConnection.State == ConnectionState.Closed)
                    CeConnection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Close the connection (if using unique connections)
        /// </summary>
        protected void CloseCeConnection()
        {
            if (CeConnection.State != ConnectionState.Closed)
                CeConnection.Close();
        }


        /// <summary>
        /// Pass an active SqlCeConnection object to the selected
        /// datasource table.
        /// </summary>
        public void Initialize(SqlConnectionObject iCeConnection)
        {
            if (iCeConnection != null)
            {
                mSqlCeConnect = iCeConnection;
            }
            else
            {
                
            }

        }/* end method */


        /// <summary>
        /// Simple method that invokes a progress update method
        /// </summary>
        /// <param name="iProgress">external progress object</param>
        protected void RaiseProgressUpdate(DataProgress iProgress)
        {
            iProgress.Update(true);
        }/* end method */


    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------