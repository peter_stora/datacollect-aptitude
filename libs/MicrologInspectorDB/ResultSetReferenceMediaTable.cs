﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the REFERENCEMEDIA Table ResultSet
   /// </summary>
   public partial class ResultSetReferenceMediaTable : ResultSetBase
   {
      //-------------------------------------------------------------------------
      
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsReferenceMedia mColumnOrdinal = null;

      /// <summary>
      /// All Reference Media based on Prism ID 
      /// </summary>
      private const string QUERY_ALL_BY_PRISM_ID = "SELECT * FROM REFERENCEMEDIA " +
          "WHERE PointHostIndex = @PointHostIndex";

      /// <summary>
      /// Image only Reference Media based on Prism ID 
      /// </summary>
      private string QUERY_IMAGES_BY_PRISM_ID = "SELECT * FROM REFERENCEMEDIA " +
          "WHERE PointHostIndex = @PointHostIndex AND MediaType = " +
          ( (byte) BinaryMediaType.Image ).ToString();

      /// <summary>
      /// Audio only Reference Media based on Prism ID 
      /// </summary>
      private string QUERY_AUDIO_BY_PRISM_ID = "SELECT * FROM REFERENCEMEDIA " +
          "WHERE PointHostIndex = @PointHostIndex AND MediaType = " +
          ( (byte) BinaryMediaType.Audio ).ToString();

      /// <summary>
      /// Video only Reference Media based on Prism ID 
      /// </summary>
      private string QUERY_VIDEO_BY_PRISM_ID = "SELECT * FROM REFERENCEMEDIA " +
          "WHERE PointHostIndex = @PointHostIndex AND MediaType = " +
          ( (byte) BinaryMediaType.Video ).ToString();

      /// <summary>
      /// General Reference Media based on Prism ID 
      /// </summary>
      private string QUERY_MEDIA_BY_PRISM_ID = "SELECT * FROM REFERENCEMEDIA " +
          "WHERE PointHostIndex = @PointHostIndex AND MediaType = " +
          ( (byte) BinaryMediaType.RawBinary ).ToString();

      #endregion

      //-------------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_REFERENCEMEDIA;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current REFERENCEMEDIA table. 
      /// ResultSet object.
      /// </summary>
      /// <param name="iMedia">populated Reference object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( ReferenceMediaRecord iMedia )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "REFERENCEMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // add record to table
               result = WriteRecord( iMedia, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Media objects to the REFERENCEMEDIA table 
      /// and return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of Media objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumReferenceMedia iMediaRecords, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "REFERENCEMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( ReferenceMediaRecord mediaRecord in iMediaRecords )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( mediaRecord, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ReferenceMedia objects to the REFERENCEMEDIA 
      /// table.
      /// </summary>
      /// <param name="iMediaRecords">Enumerated List of ReferenceMedia objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumReferenceMedia iMediaRecords )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "REFERENCEMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( ReferenceMediaRecord mediaRecord in iMediaRecords )
               {
                  if ( !WriteRecord( mediaRecord, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ReferenceMedia objects to the REFERENCEMEDIA 
      /// table.
      /// </summary>
      /// <param name="iMediaRecords">Enumerated List of ReferenceMedia objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumReferenceMediaRecords iMediaRecords )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "REFERENCEMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               for ( int i = 0; i < iMediaRecords.Count; ++i )
               {
                  if ( !WriteRecord( iMediaRecords.ReferenceMedia[ i ], ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the REFERENCEMEDIA table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public EnumReferenceMedia GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumReferenceMedia mediaRecords = new EnumReferenceMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "REFERENCEMEDIA";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ReferenceMediaRecord referenceMedia = new ReferenceMediaRecord();
                  PopulateObject( ceResultSet, ref referenceMedia );
                  if ( referenceMedia != null )
                     mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Table Specific Methods

      /// <summary>
      /// Returns an enumerated list of all ReferenceMedia object types 
      /// given the Unique Identifier of the parent machine or Point node.
      /// </summary>
      /// <param name="iPointHostIndex">Unique machine node identifier</param>
      /// <returns>enumerated list of ReferenceMedia objects</returns>
      public EnumReferenceMedia GetRecords( int iPointHostIndex )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumReferenceMedia mediaRecords = new EnumReferenceMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ALL_BY_PRISM_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value = iPointHostIndex;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ReferenceMediaRecord referenceMedia = new ReferenceMediaRecord();
                  PopulateObject( ceResultSet, ref referenceMedia );
                  if ( referenceMedia != null )
                     mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all ReferenceMedia object types 
      /// given the Unique Identifier of the parent machine or Point node.
      /// </summary>
      /// <param name="iPointHostIndex">Unique machine node identifier</param>
      /// <returns>enumerated list of ReferenceMedia objects</returns>
      public EnumReferenceMedia GetRecords( int iPointHostIndex,
          BinaryMediaType iType )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumReferenceMedia mediaRecords = new EnumReferenceMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // Set the query type based on the binary media to return
            switch ( iType )
            {
               case BinaryMediaType.Audio:
                  command.CommandText = QUERY_AUDIO_BY_PRISM_ID;
                  break;

               case BinaryMediaType.Image:
                  command.CommandText = QUERY_IMAGES_BY_PRISM_ID;
                  break;

               case BinaryMediaType.Video:
                  command.CommandText = QUERY_VIDEO_BY_PRISM_ID;
                  break;

               case BinaryMediaType.RawBinary:
                  command.CommandText = QUERY_MEDIA_BY_PRISM_ID;
                  break;
            }

            // add the query parameter
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value = iPointHostIndex;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTPOINT table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ReferenceMediaRecord referenceMedia = new ReferenceMediaRecord();
                  PopulateObject( ceResultSet, ref referenceMedia );
                  if ( referenceMedia != null )
                     mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// REFERENCEMEDIA table. This table will need to be revised in the event of any 
      /// changes being made to the REFERENCEMEDIA table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsReferenceMedia( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new ReferenceMediaRecord and add it 
      /// to the current REFERENCEMEDIA table.
      /// </summary>
      /// <param name="iMedia">Populated RefernceMedia Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( ReferenceMediaRecord iMedia,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iMedia.Uid;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iMedia.PointHostIndex;
            newRecord[ mColumnOrdinal.MediaType ] = iMedia.MediaType;
            newRecord[ mColumnOrdinal.FileName ] = iMedia.FileName;
            newRecord[ mColumnOrdinal.FilePath ] = iMedia.FilePath;
            newRecord[ mColumnOrdinal.Size ] = iMedia.Size;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated Reference Media Object with data from a 
      /// sinble REFERENCEMEDIA table record. In the event of a raised exception 
      /// the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMedia">ReferenceMedia object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref ReferenceMediaRecord iMedia )
      {
         try
         {
            iMedia.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );
            iMedia.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            iMedia.MediaType = ceResultSet.GetByte( mColumnOrdinal.MediaType );
            iMedia.FileName = ceResultSet.GetString( mColumnOrdinal.FileName );
            iMedia.FilePath = ceResultSet.GetString( mColumnOrdinal.FilePath );
            iMedia.Size = ceResultSet.GetInt32( mColumnOrdinal.Size );
         }
         catch
         {
            iMedia = null;
         }

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

   }/* end class */

}/* end namespace */


//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 15th September 2011
//  New fields
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//-------------------------------------------------------------------------------