﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the STRUCTURED Table ResultSet
   /// </summary>
   public class ResultSetStructuredTable : ResultSetBase
   {

      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsStructuredRoute mColumnOrdinal = null;

      /// <summary>
      /// Query DERIVEDITSM table based on the external Point Uid field. 
      /// This query will return an enumerated series of records ordered
      /// by the field "FormulaItemPosition".
      /// </summary>
      private const string QUERY_BY_ROUTEID = "SELECT * FROM STRUCTURED " +
          "WHERE RouteId = @RouteId";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_STRUCTURED;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current STRUCTURED table. 
      /// </summary>
      /// <param name="iStructuredRouteObject">populated Structured Route object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( StructuredRecord iStructuredRouteObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTURED";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write record to table
               result = WriteRecord( iStructuredRouteObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Structured records to the STRUCTURED 
      /// table and return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of STRUCTURED objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumStructuredRoutes iStructuredRoutes,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTURED";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all structured route objects
               foreach ( StructuredRecord structured in iStructuredRoutes )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( structured, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of Structured Routes to the STRUCTURED table. 
      /// </summary>
      /// <param name="iStructuredRouteObject">populated Structured Routes list</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumStructuredRoutes iStructuredRoutes )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTURED";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all structured route objects
               foreach ( StructuredRecord structured in iStructuredRoutes )
               {
                  // write record to table
                  if ( !WriteRecord( structured, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a single Structured Route Record object from the STRUCTURED 
      /// table using the external RouteId (links to NODE.RouteId) as the lookup 
      /// reference.
      /// </summary>
      /// <param name="iRouteId">External link to the RouteId</param>
      /// <returns>Populated Structured Route object</returns>
      public StructuredRecord GetRecord( int iRouteId )
      {
         OpenCeConnection();

         // reset the default profile name
         StructuredRecord structuredRecord = new StructuredRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_ROUTEID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@RouteId", SqlDbType.Int ).Value = iRouteId;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the STRUCTURED table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref structuredRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return structuredRecord;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all STRUCTURED table records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumStructuredRoutes GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumStructuredRoutes structuredRecords = new EnumStructuredRoutes();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "STRUCTURED";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the STRUCTURED table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  StructuredRecord structured = new StructuredRecord();
                  PopulateObject( ceResultSet, ref structured );
                  structuredRecords.AddStructuredRecord( structured );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return structuredRecords;

      }/* end method */


      /// <summary>
      /// Updates a STRUCTURED table record with the data from its associated
      /// business object (a single STRUCTURED Record object). As all fields are
      /// replaced, this method should be used with caution!
      /// </summary>
      /// <param name="iPointRecord">Populated STRUCTURED Record object</param>
      public void UpdateRecord( StructuredRecord iStructuredRoute )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_ROUTEID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@RouteId", SqlDbType.Int ).Value
                                              = iStructuredRoute.RouteId;

            // execure the result set
            using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the STRUCTURED table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( CeResultSet );

               // now update the record object
               if ( CeResultSet.ReadFirst() )
               {
                  UpdateRecord( iStructuredRoute, CeResultSet );
               }

               // close the resultSet
               CeResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// STRUCTURED table. This table will need to be revised in the event of any 
      /// changes being made to the STRUCTURED table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsStructuredRoute( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new STRUCTURED record and add it 
      /// to the current STRUCTURED table.
      /// </summary>
      /// <param name="iDerivedItemRecord">Populated STRUCTURED object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( StructuredRecord iStructured,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iStructured.Uid;
            newRecord[ mColumnOrdinal.RouteId ] = iStructured.RouteId;

            if ( iStructured.StartTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.StartTime ] = iStructured.StartTime;

            if ( iStructured.EndTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.EndTime ] = iStructured.EndTime;

            newRecord[ mColumnOrdinal.Duration ] = iStructured.Duration;
            newRecord[ mColumnOrdinal.OperId ] = iStructured.OperId;
            newRecord[ mColumnOrdinal.NextPoint ] = iStructured.NextPoint;

            if ( iStructured.InternalStart.Ticks > 0 )
               newRecord[ mColumnOrdinal.InternalStart ] = iStructured.InternalStart;

            newRecord[ mColumnOrdinal.PercentComplete ] = iStructured.PercentComplete;
            newRecord[ mColumnOrdinal.Status ] = iStructured.Status;
            newRecord[ mColumnOrdinal.LastPoint ] = iStructured.LastPoint;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single STRUCTURED table record
      /// </summary>
      /// <param name="iDerivedItemRecord">Updated STRUCTURED object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( StructuredRecord iStructured,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetGuid( mColumnOrdinal.Uid, iStructured.Uid );
            ceResultSet.SetInt32( mColumnOrdinal.RouteId, iStructured.RouteId );
            if ( iStructured.StartTime.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.StartTime, iStructured.StartTime );
            if ( iStructured.EndTime.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.EndTime, iStructured.EndTime );
            ceResultSet.SetInt32( mColumnOrdinal.Duration, iStructured.Duration );
            ceResultSet.SetInt32( mColumnOrdinal.OperId, iStructured.OperId );
            ceResultSet.SetInt32( mColumnOrdinal.NextPoint, iStructured.NextPoint );
            if ( iStructured.InternalStart.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.InternalStart, iStructured.InternalStart );
            ceResultSet.SetDouble( mColumnOrdinal.PercentComplete, iStructured.PercentComplete );
            ceResultSet.SetByte( mColumnOrdinal.Status, iStructured.Status );
            ceResultSet.SetInt32( mColumnOrdinal.LastPoint, iStructured.LastPoint );

            // now update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated STRUCTURED Object with data from a single 
      /// STRUCTURED table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iDerivedItemRecord">DERIVEDITEMS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref StructuredRecord iStructured )
      {
         try
         {
            iStructured.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );
            iStructured.RouteId = ceResultSet.GetInt32( mColumnOrdinal.RouteId );

            // if no start date value then set field to current date and time 
            if ( ceResultSet[ mColumnOrdinal.StartTime ] != DBNull.Value )
               iStructured.StartTime = ceResultSet.GetDateTime( mColumnOrdinal.StartTime );
            else
               iStructured.StartTime = PacketBase.DateTimeNow;

            // ensure end time is not null
            if ( ceResultSet[ mColumnOrdinal.EndTime ] != DBNull.Value )
               iStructured.EndTime = ceResultSet.GetDateTime( mColumnOrdinal.EndTime );

            iStructured.Duration = ceResultSet.GetInt32( mColumnOrdinal.Duration );
            iStructured.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iStructured.NextPoint = ceResultSet.GetInt32( mColumnOrdinal.NextPoint );

            // if no datetime value then set field to current date and time
            if ( ceResultSet[ mColumnOrdinal.InternalStart ] != DBNull.Value )
               iStructured.InternalStart = ceResultSet.GetDateTime( mColumnOrdinal.InternalStart );
            else
               iStructured.InternalStart = PacketBase.DateTimeNow;

            iStructured.PercentComplete = ceResultSet.GetDouble( mColumnOrdinal.PercentComplete );
            iStructured.Status = ceResultSet.GetByte( mColumnOrdinal.Status );
            iStructured.LastPoint = ceResultSet.GetInt32( mColumnOrdinal.LastPoint );
         }
         catch
         {
            iStructured = null;
         }

      }/* end method */

      #endregion


   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------