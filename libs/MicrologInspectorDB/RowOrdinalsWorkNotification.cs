﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the WORKNOTIFICATION table. This structure should be revised following 
    /// any changes made to this table.
    /// </summary>
    public class RowOrdinalsWorkNotification
    {
        public int NodeUid { get; set; }
        public int WorkNotificationID { get; set; }
        public int RouteID { get; set; }
        public int Status { get; set; }
        public int KeyField { get; set; }
        public int MachineID { get; set; }
        public int LocationText { get; set; }
        public int WNTimeStamp { get; set; }
        public int OperatorID { get; set; }
        public int CompletionDate { get; set; }
        public int WorkTypeIDs { get; set; }
        public int PriorityID { get; set; }
        public int ProblemID { get; set; }
        public int ProblemDescription { get; set; }
        public int CorrectiveActionID { get; set; }
        public int CorrectiveDescription { get; set; }
        public int FixByDateSet { get; set; }
        public int PRISMNumber { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsWorkNotification(SqlCeResultSet iCeResultSet)
        {
            GetTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// WORKNOTIFICATION table. This table will need to be revised in the event of 
        /// any changes being made to the WORKNOTIFICATION table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.NodeUid = iCeResultSet.GetOrdinal("NodeUid");
            this.WorkNotificationID = iCeResultSet.GetOrdinal("WorkNotificationID");
            this.RouteID = iCeResultSet.GetOrdinal("RouteID");
            this.Status = iCeResultSet.GetOrdinal("Status");
            this.KeyField = iCeResultSet.GetOrdinal("KeyField");
            this.MachineID = iCeResultSet.GetOrdinal("MachineID");
            this.LocationText = iCeResultSet.GetOrdinal("LocationText");
            this.WNTimeStamp = iCeResultSet.GetOrdinal("WNTimeStamp");
            this.OperatorID = iCeResultSet.GetOrdinal("OperatorID");
            this.CompletionDate = iCeResultSet.GetOrdinal("CompletionDate");
            this.WorkTypeIDs = iCeResultSet.GetOrdinal("WorkTypeIDs");
            this.PriorityID = iCeResultSet.GetOrdinal("PriorityID");
            this.ProblemID = iCeResultSet.GetOrdinal("ProblemID");
            this.ProblemDescription = iCeResultSet.GetOrdinal("ProblemDescription");
            this.CorrectiveActionID = iCeResultSet.GetOrdinal("CorrectiveActionID");
            this.CorrectiveDescription = iCeResultSet.GetOrdinal("CorrectiveDescription");
            this.FixByDateSet = iCeResultSet.GetOrdinal("FixByDateSet");
            this.PRISMNumber = iCeResultSet.GetOrdinal("PRISMNumber");
        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2009
//  Add to project
//----------------------------------------------------------------------------
