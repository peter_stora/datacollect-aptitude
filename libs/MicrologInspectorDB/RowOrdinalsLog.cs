﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field
    /// within the LOG table. This structure should be revised 
    /// following any changes made to this table.
    /// </summary>
    public class RowOrdinalsLog
    {
        public int Time { get; set; }
        public int Event { get; set; }
        public int Name { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsLog(SqlCeResultSet iCeResultSet)
        {
            GetTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated
        /// with the LOG table. This table will need to be revised
        /// in the event of any changes being made to the LOG table 
        /// field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTableColumns(SqlCeResultSet iCeResultSet)
        {
            iCeResultSet.GetOrdinal("Time");
            iCeResultSet.GetOrdinal("Event");
            iCeResultSet.GetOrdinal("Name");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2009
//  Add to project
//----------------------------------------------------------------------------

