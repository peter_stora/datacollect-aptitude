﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the bulk population of Tables following 
   /// a full profile download. Note: This class does not support 
   /// IDisposable and so will not instantiate a TableConnect 
   /// object. Rather the TableConnect object should be passed
   /// as a constructor parameter.
   /// </summary>
   public class PopulateTable
   {
      #region Private fields

      private ITableConnect mTable = null;
      private DataProgress mProgressPosition = null;

      #endregion


      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iTable"></param>
      public PopulateTable( ITableConnect iTable )
      {
         // reference the table connection object
         mTable = iTable;

         // instantiate progress position object
         mProgressPosition = new DataProgress( DisplayPosition.SecondHalf );

         // reset the progress manager
         mProgressPosition.Reset();

      }/* end constructor */


      ///<summary>
      ///Gets or sets the progress position object
      ///</summary>
      public DataProgress ProgressPosition
      {
         get
         {
            return mProgressPosition;
         }
         set
         {
            mProgressPosition = value;
         }
      }


      #region Public Methods

      /// <summary>
      /// Repopulate an empty CORRECTIVEACTION table using an enumerated list
      /// of populated CorrectiveAction Record objects
      /// </summary>
      /// <param name="iCorrectiveAction">enumerated list of Corrective Action Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CmmsCorrectiveActionTable( EnumCmmsCorrectiveActionRecords iCorrectiveActions )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.CorrectiveAction.AddEnumRecords( iCorrectiveActions );

      }/* end method */


      /// <summary>
      /// Repopulate an empty PRIORITY table using an enumerated list
      /// of populated CMMS PRIORITY Record objects
      /// </summary>
      /// <param name="iCmmsPriority">enumerated list of CMMS Priority objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CmmsPriorityTable( EnumCmmsPriorityRecords iCmmsPriorities )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.Priority.AddEnumRecords( iCmmsPriorities );

      }/* end method */


      /// <summary>
      /// Repopulate an empty PROBLEM table using an enumerated list
      /// of populated CMMS PROBLEM Record objects
      /// </summary>
      /// <param name="iCmmsProblem">enumerated list of CMMS Problem objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CmmsProblemTable( EnumCmmsProblemRecords iCmmsProblems )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.Problem.AddEnumRecords( iCmmsProblems );

      }/* end method */


      /// <summary>
      /// Repopulate an empty WORKTYPE table using an enumerated list
      /// of populated CMMS WORKTYPE Record objects
      /// </summary>
      /// <param name="iCmmsWorkType">enumerated list of CMMS WorkType objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CmmsWorkTypeTable( EnumCmmsWorkTypeRecords iCmmsWorkTypes )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.WorkType.AddEnumRecords( iCmmsWorkTypes );

      }/* end method */


      /// <summary>
      /// Repopulate an empty WORKNOTIFICATION table using an enumerated list
      /// of populated CMMS WORKNOTIFICATION Record objects
      /// </summary>
      /// <param name="iCmmsWorkNotification">enumerated list of CMMS WorkNotification objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CmmsWorkNotificationTable( EnumCmmsWorkNotificationRecords iCmmsWorkNotifications )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.WorkNotification.AddEnumRecords( iCmmsWorkNotifications );

      }/* end method */


      /// <summary>
      /// Repopulate an empty C_NOTE table using an enumerated list
      /// of populated Coded Note Record objects
      /// </summary>
      /// <param name="iCodedNotes">enumerated list of C_NOTE Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean CodedNotesTable( EnumCodedNotes iCodedNotes )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iCodedNotes.Count;

         // and update the table
         return mTable.CodedNote.AddEnumRecords( iCodedNotes,
             ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty CONDITIONALPOINT table using an enumerated list
      /// of populated Instruction Record objects
      /// </summary>
      /// <param name="iDerivedPoint">enumerated list of Derived Point Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean ConditionalPointTable( EnumConditionalPoints iConditionalPoints )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // check how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iConditionalPoints.Count;

         // and update the table
         return mTable.ConditionalPoint.AddEnumRecords( iConditionalPoints, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty DERIVEDITEMS table using an enumerated list
      /// of populated Instruction Record objects
      /// </summary>
      /// <param name="iDerivedItems">enumerated list of Derived Items objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean DerivedItemsTable( EnumDerivedItems iDerivedItems )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // check how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iDerivedItems.Count;

         // and update the table
         return mTable.DerivedItems.AddEnumRecords( iDerivedItems, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty DERIVEDPOINT table using an enumerated list
      /// of populated Instruction Record objects
      /// </summary>
      /// <param name="iDerivedPoint">enumerated list of Derived Point Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean DerivedPointTable( EnumDerivedPoints iDerivedPoints )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // check how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iDerivedPoints.Count;

         // and update the table
         return mTable.DerivedPoint.AddEnumRecords( iDerivedPoints, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty ENVDATA table using an enumerated list
      /// of populated PointRecord objects
      /// </summary>
      /// <param name="iPointRecords">enumerated list of PointRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean EnvDataTable( EnumPoints iPointRecords, ref DataProgress iProgress )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.EnvData.AddEnumRecords( iPointRecords, ref iProgress );

      }/* end method */


      /// <summary>
      /// Repopulate an empty FFTPOINTS table using an enumerated list
      /// of populated FFTPointRecord objects
      /// </summary>
      /// <param name="iFFTPointRecords">enumerated list of FFTPointRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean FFTPointsTable( EnumFFTPoints iFFTPointRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iFFTPointRecords.Count;

         // and update the table
         return mTable.FFTPoint.AddEnumRecords( iFFTPointRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty FFTChannel table using an enumerated list
      /// of populated FFTChannel objects
      /// </summary>
      /// <param name="iFFTChannelRecords">enumerated list of FFTChannelRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean FFTChannelTable( EnumFFTChannelRecords iFFTChannelRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iFFTChannelRecords.Count;

         // and update the table
         return mTable.FFTChannel.AddEnumRecords( iFFTChannelRecords );

      }/* end method */


      /// <summary>
      /// Repopulate an empty INSPECTION table using an enumerated list
      /// of populated InspectionRecord objects
      /// </summary>
      /// <param name="iInspectionRecords">enumerated list of InspectionRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean InspectionTable( EnumInspection iInspectionRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iInspectionRecords.Count;

         // and update the table
         return mTable.Inspection.AddEnumRecords( iInspectionRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty INSTRUCTIONS table using an enumerated list
      /// of populated Instruction Record objects
      /// </summary>
      /// <param name="iInstructions">enumerated list of INSTRUCTION Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean InstructionsTable( EnumInstructions iInstructions )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iInstructions.Count;

         // and update the table
         return mTable.Instructions.AddEnumRecords( iInstructions,
             ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty LOCALSETTINGS table using an enumerated list
      /// of populated OperatorSet objects
      /// </summary>
      /// <param name="iOperSettings">enumerated list of operator settings</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean LocalSettingsTable( OperatorSet iOperSettings )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.UserPreferences.AddEnumRecords( iOperSettings );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MACHINENOPSKIPS table using an enumerated list
      /// of populated OperatorSet objects
      /// </summary>
      /// <param name="iOperSettings">enumerated list of operator settings</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean MachineNOPSkipsTable( OperatorSet iOperSettings )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.MachineNOpSkips.AddEnumRecords( iOperSettings );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MACHINENOPSKIPS table using an enumerated list
      /// of populated OperatorSet objects
      /// </summary>
      /// <param name="iOperSettings">enumerated list of operator settings</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean MachineOKSkipsTable( OperatorSet iOperSettings )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.MachineOkSkips.AddEnumRecords( iOperSettings );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MCC table using an enumerated list
      /// of populated MCCRecord objects
      /// </summary>
      /// <param name="iMCCRecords">enumerated list of MCCRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean MCCTable( EnumMCC iMCCRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iMCCRecords.Count;

         // Add all InspectionRecord objects directly to the MCC table.
         return mTable.Mcc.AddEnumRecords( iMCCRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MESSAGE table using an enumerated list
      /// of populated Message Record objects
      /// </summary>
      /// <param name="iMessages">enumerated list of MESSAGE Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean MessageTable( EnumMessages iMessageRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iMessageRecords.Count;

         // and update the table
         return mTable.Message.AddEnumRecords( iMessageRecords,
             ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MESSAGINGPREFS table using an enumerated list
      /// of populated OperatorSet objects
      /// </summary>
      /// <param name="iOperSettings">enumerated list of operator settings</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean MessagePrefsTable( OperatorSet iOperSettings )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.MessagePreferences.AddEnumRecords( iOperSettings );

      }/* end method */


      /// <summary>
      /// Repopulate an empty NODE table using an enumerated list
      /// of populated NodeRecord objects
      /// </summary>
      /// <param name="iNodeRecords">enumerated list of NodeRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean NodeTable( EnumNodes iNodeRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iNodeRecords.Count;

         // Add all NodeRecord objects directly to the NODE table.
         return mTable.Node.AddEnumRecords( iNodeRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty OPERATORS table using an enumerated list
      /// of populated OperatorSet objects
      /// </summary>
      /// <param name="iOperSettings"></param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean OperatorsTable( OperatorSet iOperSettings )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // Populate the Operators Set and get the initial result
         Boolean soFarSoGood = mTable.Operators.AddEnumRecords( iOperSettings );

         // if everything went OK then populate the messaging preferences table
         if ( soFarSoGood )
            soFarSoGood = MessagePrefsTable( iOperSettings );

         //
         // if everything went OK then populate the "machine not operating" 
         // preferences table
         //
         if ( soFarSoGood )
            soFarSoGood = MachineNOPSkipsTable( iOperSettings );

         //
         // if everything went OK then populate the "machine not operating" 
         // preferences table
         //
         if ( soFarSoGood )
            soFarSoGood = MachineOKSkipsTable( iOperSettings );

         //
         // if everything went OK then populate the "LocalSettings" 
         // preferences table
         //
         if ( soFarSoGood )
            soFarSoGood = LocalSettingsTable( iOperSettings );

         // return the method result
         return soFarSoGood;

      }/* end method */


      /// <summary>
      /// Repopulate an empty POINTS table using an enumerated list
      /// of populated PointRecord objects
      /// </summary>
      /// <param name="iPointRecords">enumerated list of PointRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean PointsTable( EnumPoints iPointRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = TotalPointProgressWrites( iPointRecords );


         // open the POINTS table and write all records
         Boolean result = mTable.Points.AddEnumRecords( iPointRecords, ref mProgressPosition );

         // open the TEMPDATA table and write all pending records
         if ( result )
         {
            result = TempDataTable( iPointRecords, ref mProgressPosition );
         }

         // open the ENVDATA table and write all pending records
         if ( result )
         {
            result = EnvDataTable( iPointRecords, ref mProgressPosition );
         }

         // open the VELDATA table and write all pending records
         if ( result )
         {
            result = VelDataTable( iPointRecords, ref mProgressPosition );
         }

         // return the method result
         return result;

      }/* end method */


      /// <summary>
      /// Repopulate an empty PROCESS table using an enumerated list
      /// of populated ProcessRecord objects
      /// </summary>
      /// <param name="iProcessRecords">enumerated list of ProcessRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean ProcessTable( EnumProcess iProcessRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iProcessRecords.Count;

         // and update the table
         return mTable.Process.AddEnumRecords( iProcessRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty TEXTDATA table using an enumerated list
      /// of populated TextData objects
      /// </summary>
      /// <param name="iTextData">enumerated list of TextDataRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean TextDataTable( EnumTextData iTextData )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iTextData.Count;

         // and update the table
         return mTable.TextData.AddEnumRecords( iTextData, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty REFERENCEMEDIA table using an enumerated list
      /// of populated ReferenceMedia objects
      /// </summary>
      /// <param name="iMediaRecords">enumerated list of ReferenceMedia objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean ReferenceMediaTable( EnumReferenceMedia iMediaRecords )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iMediaRecords.Count;

         // and update the table
         return mTable.ReferenceMedia.AddEnumRecords( iMediaRecords, ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty STRUCTURED table using an enumerated list
      /// of populated Structured Route Record objects
      /// </summary>
      /// <param name="iCodedNotes">enumerated list of STRUCTURED Record objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean StructuredRouteTable( EnumStructuredRoutes iStructuredRoutes )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // otherwise reset the progress object
         mProgressPosition.Reset();

         // how many record updates are there going to be
         mProgressPosition.NoOfItemsToProcess = iStructuredRoutes.Count;

         // and update the table
         return mTable.Structured.AddEnumRecords( iStructuredRoutes,
             ref mProgressPosition );

      }/* end method */


      /// <summary>
      /// Repopulate an empty TEMPDATA table using an enumerated list
      /// of populated PointRecord objects
      /// </summary>
      /// <param name="iInspectionRecords">enumerated list of PointRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean TempDataTable( EnumPoints iPointRecords, ref DataProgress iProgress )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.TempData.AddEnumRecords( iPointRecords, ref iProgress );

      }/* end method */


      /// <summary>
      /// Repopulate an empty MCC table using an enumerated list
      /// of populated MCCRecord objects
      /// </summary>
      /// <param name="iPointRecords">enumerated list of PointRecord objects</param>
      /// <returns>true on success, or false on exception</returns>
      public Boolean VelDataTable( EnumPoints iPointRecords, ref DataProgress iProgress )
      {
         // if table object not instantiated, then bomb out here!
         if ( mTable == null )
            return false;

         // and update the table
         return mTable.VelData.AddEnumRecords( iPointRecords, ref iProgress );

      }/* end method */

      #endregion


      /// <summary>
      /// Returns the total number of records that will be written to the
      /// database for the available list of points. This includes historic
      /// TEMP, ENV and VEL records returned from @ptitude
      /// </summary>
      /// <param name="iPointRecords"></param>
      /// <returns></returns>
      private int TotalPointProgressWrites( EnumPoints iPointRecords )
      {
         // reset the enum list
         iPointRecords.Reset();

         int count = 0;

         // for each of the enumerated points records
         foreach ( PointRecord i in iPointRecords )
         {
            count = count + i.TempData.Count;
            count = count + i.EnvData.Count;
            count = count + i.VelData.Count;
         }

         return count + iPointRecords.Count;

      }/* end method */


   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 24th June 2009
//  Add to project
//----------------------------------------------------------------------------
