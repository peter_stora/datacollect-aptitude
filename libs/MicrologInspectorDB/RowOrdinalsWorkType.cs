﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the WORKTYPE table. This structure should be revised following 
    /// any changes made to this table.
    /// </summary>
    public class RowOrdinalsWorkType
    {
        public int MAWorkTypeKey { get; set; }
        public int WorkTypeText{ get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsWorkType(SqlCeResultSet iCeResultSet)
        {
            GetTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with 
        /// the WORKTYPE table. This table will need to be revised in the event of 
        /// any changes being made to the WORKTYPE table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.MAWorkTypeKey = iCeResultSet.GetOrdinal("MAWorkTypeKey");
            this.WorkTypeText = iCeResultSet.GetOrdinal("WorkTypeText");
        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2009
//  Add to project
//----------------------------------------------------------------------------
