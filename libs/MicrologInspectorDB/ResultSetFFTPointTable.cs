﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// This class manages the FFTPOINT Table ResultSet
    /// </summary>
    public partial class ResultSetFFTPointTable : ResultSetBase
    {
        #region Private Fields and Constants

        /// <summary>
        /// Column ordinal object
        /// </summary>
        private RowOrdinalsFFTPoint mColumnOrdinal = null;

        /// <summary>
        /// Query FFTPoints table based purely on the Uid field (will return one point)
        /// </summary>
        private const string QUERY_BY_UID = "SELECT * FROM FFTPOINT " +
            "WHERE Uid = @Uid";


        /// <summary>
        /// Query FFTPoints table based on the external PointUid field (will return one point)
        /// </summary>
        private const string QUERY_BY_POINT_UID = "SELECT * FROM FFTPOINT " +
            "WHERE PointUid = @PointUid";


        /// <summary>
        /// Query FFTPoints table based on the Hierarchy Number field (will return one point)
        /// </summary>
        private const string QUERY_BY_HIERARCHY_NUMBER = "SELECT * FROM FFTPOINT " +
            "WHERE Number = @Number";


        #endregion


        #region Core Public Methods

        /// <summary>
        /// Clear the contents of this table.
        /// </summary>
        /// <returns>true on success, or false on failure</returns>
        public Boolean ClearTable()
        {
            OpenCeConnection();

            // set the default result
            Boolean result = true;

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = DeleteQuery.CLEAR_FFTPOINT;
                try
                {
                    command.ExecuteResultSet(ResultSetOptions.None);
                }
                catch
                {
                    result = false;
                }
            }

            CloseCeConnection();

            // return result
            return result;

        }/* end method */


        /// <summary>
        /// Add a single new record to the current FFTPOINT table. 
        /// </summary>
        /// <param name="iFFTPointObject">populated FFTPointRecord object</param>
        /// <returns>false on exception raised</returns>
        public Boolean AddRecord(FFTPointRecord iFFTPointObject)
        {
            OpenCeConnection();

            // set the default result
            Boolean result = true;

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = "FFTPOINT";
                command.CommandType = System.Data.CommandType.TableDirect;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                            ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                {
                   //
                   // if necessary get a list of all column ordinals associated 
                   // with  this table. As this is a 'one shot' the column ordinal
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // write to the table
                   result = WriteRecord( iFFTPointObject, ceResultSet );

                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return result;

        }/* end method */


        /// <summary>
        /// Add an enumerated list of FFTPoints objects to the FFTPOINT table and
        /// return the process updates
        /// </summary>
        /// <param name="iEnumFFTPoints">Enumerated List of FFTPointRecord objects</param>
        /// <param name="iProgress">Progress update object</param>
        /// <returns>false on exception raised</returns>
        public Boolean AddEnumRecords(EnumFFTPoints iEnumFFTPoints, ref DataProgress iProgress)
        {
            OpenCeConnection();

            // set the default result
            Boolean result = true;

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = "FFTPOINT";
                command.CommandType = System.Data.CommandType.TableDirect;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                            ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                {
                   //
                   // if necessary get a list of all column ordinals associated 
                   // with  this table. As this is a 'one shot' the column ordinal
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // process each of the enumerable records
                   foreach ( FFTPointRecord point in iEnumFFTPoints )
                   {
                      //
                      // If not using a custom configuration, @A doesn't send any
                      // configuration other than "TakeData", "RunningSpeed" and
                      // "UseConfiguration".  Therefore, we need to load the 
                      // defaults ourselves.  OTD# 1520
                      //
                      if ( ( point != null ) && ( point.UseConfiguration == 0 ) )
                      {
                         point.Use_Defaults();
                      }

                      // raise a progress update
                      RaiseProgressUpdate( iProgress );

                      // write record to tabl
                      if ( !WriteRecord( point, ceResultSet ) )
                      {
                         result = false;
                         break;
                      }
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return result;

        }/* end method */


        /// <summary>
        /// Add an enumerated list of FFTPointRecord objects to the FFTPOINT table. 
        /// </summary>
        /// <param name="iEnumFFTPoints">Enumerated List of FFTPointRecord objects</param>
        /// <returns>false on exception raised</returns>
        public Boolean AddEnumRecords(EnumFFTPoints iEnumFFTPoints)
        {
            OpenCeConnection();
            
            // set the default result
            Boolean result = true;

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = "FFTPOINT";
                command.CommandType = System.Data.CommandType.TableDirect;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                            ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
                {

                   //
                   // if necessary get a list of all column ordinals associated 
                   // with  this table. As this is a 'one shot' the column ordinal
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // process each of the enumerable records
                   foreach ( FFTPointRecord point in iEnumFFTPoints )
                   {
                      if ( !WriteRecord( point, ceResultSet ) )
                      {
                         result = false;
                         break;
                      }
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return result;

        }/* end method */


        /// <summary>
        /// Returns a single FFTPoint Record object from the FFTPOINT table 
        /// using the table's internal Uid as the lookup reference.
        /// </summary>
        /// <param name="iUid">Unique identifier</param>
        /// <returns>Populated FFT Point object or null on error</returns>
        public FFTPointRecord GetRecord(Guid iUid)
        {
            OpenCeConnection();
            
            // reset the default profile name
            FFTPointRecord fftPointRecord = new FFTPointRecord();

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = QUERY_BY_UID;

                // reference the lookup field as a parameter
                command.Parameters.Add("@Uid", SqlDbType.UniqueIdentifier).Value = iUid;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                {
                   //
                   // if necessary get a list of all column ordinals associated with 
                   // the FFTPOINT table. As this is a 'one shot' the column ordinal 
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // now read the record object
                   if ( ceResultSet.ReadFirst() )
                   {
                      PopulateObject( ceResultSet, ref fftPointRecord );
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return fftPointRecord;

        }/* end method */


        /// <summary>
        /// Returns an enumerated list of all FFTPOINT records
        /// </summary>
        /// <returns>Enumerated list or null on error</returns>
        public EnumFFTPoints GetAllRecords()
        {
            OpenCeConnection();

            // reset the default profile name
            EnumFFTPoints pointRecords = new EnumFFTPoints();

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = "FFTPOINT";

                // reference the lookup field as a parameter
                command.CommandType = CommandType.TableDirect;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                {

                   //
                   // if necessary get a list of all column ordinals associated with 
                   // the FFTPOINT table. As this is a 'one shot' the column ordinal 
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // now read the record object
                   while ( ceResultSet.Read() )
                   {
                      FFTPointRecord fftPointRecord = new FFTPointRecord();
                      PopulateObject( ceResultSet, ref fftPointRecord );
                      pointRecords.AddFFTPointRecord( fftPointRecord );
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return pointRecords;

        }/* end method */

        #endregion


        #region Table Specific Methods

        /// <summary>
        /// Returns a single FFTPoint Record object from the FFTPOINT table 
        /// using the external PointUid as the lookup reference.
        /// </summary>
        /// <param name="iPointUid">Point Unique identifier</param>
        /// <returns>Populated FFT Point object or null</returns>
        public FFTPointRecord GetRecordFromPointUid(Guid iPointUid)
        {
            OpenCeConnection();
            
            // reset the default profile name
            FFTPointRecord fftPointRecord = null;

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = QUERY_BY_POINT_UID;

                // reference the lookup field as a parameter
                command.Parameters.Add("@PointUid", SqlDbType.UniqueIdentifier).Value = iPointUid;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                {
                   // did the query actually return anything?
                   if ( ceResultSet.HasRows )
                   {
                      // if yes, then instantiate the return object
                      fftPointRecord = new FFTPointRecord();

                      //
                      // if necessary get a list of all column ordinals associated with 
                      // the FFTPOINT table. As this is a 'one shot' the column ordinal 
                      // object remains a singleton
                      //
                      GetTableColumns( ceResultSet );

                      // now read the record object
                      if ( ceResultSet.ReadFirst() )
                      {
                         PopulateObject( ceResultSet, ref fftPointRecord );
                      }
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return fftPointRecord;

        }/* end method */


        /// <summary>
        /// Returns a single FFTPoint Record object from the FFTPOINT table 
        /// using the external Hierarchy Number as the lookup reference. 
        /// </summary>
        /// <param name="iNumber">Hierarchy position index</param>
        /// <returns>Populated FFT Point object or null on error</returns>
        public FFTPointRecord GetRecordFromNumber(int iNumber)
        {
            OpenCeConnection();
            
            // reset the default profile name
            FFTPointRecord fftPointRecord = new FFTPointRecord();

            // create a new (self disposing) Sql Command object
            using (SqlCeCommand command = CeConnection.CreateCommand())
            {
                // reference the base query string
                command.CommandText = QUERY_BY_HIERARCHY_NUMBER;

                // reference the lookup field as a parameter
                command.Parameters.Add("@PointUid", SqlDbType.UniqueIdentifier).Value = iNumber;

                // execure the result set
                using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                {
                   //
                   // if necessary get a list of all column ordinals associated with 
                   // the FFTPOINT table. As this is a 'one shot' the column ordinal 
                   // object remains a singleton
                   //
                   GetTableColumns( ceResultSet );

                   // now read the record object
                   if ( ceResultSet.ReadFirst() )
                   {
                      PopulateObject( ceResultSet, ref fftPointRecord );
                   }
                   // close the resultSet
                   ceResultSet.Close();
                }
            }
            CloseCeConnection();
            return fftPointRecord;

        }/* end method */

        #endregion


        #region Private methods

        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// FFTPoints table. This table will need to be revised in the event of any changes
        /// being made to the FFTPoints table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTableColumns(SqlCeResultSet iCeResultSet)
        {
            if (mColumnOrdinal == null)
                mColumnOrdinal = new RowOrdinalsFFTPoint(iCeResultSet);

        }/* end method */


        /// <summary>
        /// Construct and populate a single new FFT Point record and add it 
        /// to the currentFFTPOINT table.
        /// </summary>
        /// <param name="iFFTPoint">Populated FFT Point Record object</param>
        /// <param name="ceResultSet">Instantiated ResultSet object</param>
        /// <returns>False on exception raised, else true by default</returns>
        private bool WriteRecord(FFTPointRecord iFFTPoint,
            SqlCeResultSet ceResultSet)
        {
            // set the default result
            Boolean result = true;

            // create updatable record object
            SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

            try
            {
                //
                // populate record object
                //
                newRecord[mColumnOrdinal.Uid] = iFFTPoint.Uid;
                newRecord[mColumnOrdinal.Number] = iFFTPoint.Number;
                newRecord[mColumnOrdinal.PointUid] = iFFTPoint.PointUid;
                newRecord[mColumnOrdinal.TakeData] = iFFTPoint.TakeData;
                newRecord[mColumnOrdinal.UseConfiguration] = iFFTPoint.UseConfiguration;
                newRecord[mColumnOrdinal.DefaultRunningSpeed] = iFFTPoint.DefaultRunningSpeed;
                newRecord[mColumnOrdinal.EnvType] = iFFTPoint.EnvType;
                newRecord[mColumnOrdinal.EnvDetection] = iFFTPoint.EnvDetection;
                newRecord[mColumnOrdinal.EnvNumberOfLines] = iFFTPoint.EnvNumberOfLines;
                newRecord[mColumnOrdinal.EnvAverages] = iFFTPoint.EnvAverages;
                newRecord[mColumnOrdinal.EnvWindow] = iFFTPoint.EnvWindow;
                newRecord[mColumnOrdinal.EnvFreqMax] = iFFTPoint.EnvFreqMax;
                newRecord[mColumnOrdinal.VelType] = iFFTPoint.VelType;
                newRecord[mColumnOrdinal.VelDetection] = iFFTPoint.VelDetection;
                newRecord[mColumnOrdinal.VelNumberOfLines] = iFFTPoint.VelNumberOfLines;
                newRecord[mColumnOrdinal.VelAverages] = iFFTPoint.VelAverages;
                newRecord[mColumnOrdinal.VelWindow] = iFFTPoint.VelWindow;
                newRecord[mColumnOrdinal.VelFreqMax] = iFFTPoint.VelFreqMax;
                newRecord[mColumnOrdinal.AcclType] = iFFTPoint.AcclType;
                newRecord[mColumnOrdinal.AcclDetection] = iFFTPoint.AcclDetection;
                newRecord[mColumnOrdinal.AcclNumberOfLines] = iFFTPoint.AcclNumberOfLines;
                newRecord[mColumnOrdinal.AcclAverages] = iFFTPoint.AcclAverages;
                newRecord[mColumnOrdinal.AcclWindow] = iFFTPoint.AcclWindow;
                newRecord[mColumnOrdinal.AcclFreqMax] = iFFTPoint.AcclFreqMax;

                //
                // add record to table and (if no errors) return true
                //
                ceResultSet.Insert(newRecord, DbInsertOptions.PositionOnInsertedRow);
            }
            catch
            {
                result = false;
            }
            return result;

        }/* end method */


        /// <summary>
        /// Update a single FFTPOINT table record
        /// </summary>
        /// <param name="iFFTPointRecord">Updated FFTPoint Record object to write</param>
        /// <param name="CeResultSet">Instantiated CeResultSet</param>
        /// <returns>True on success, else false</returns>
        private Boolean UpdateRecord(FFTPointRecord iFFTPointRecord,
            SqlCeResultSet ceResultSet)
        {
            Boolean result = true;
            try
            {
                // set the record fields
                ceResultSet.SetGuid(mColumnOrdinal.Uid, iFFTPointRecord.Uid);
                ceResultSet.SetInt32(mColumnOrdinal.Number, iFFTPointRecord.Number);
                ceResultSet.SetGuid(mColumnOrdinal.PointUid, iFFTPointRecord.PointUid);
                ceResultSet.SetByte(mColumnOrdinal.TakeData, iFFTPointRecord.TakeData);
                ceResultSet.SetByte(mColumnOrdinal.UseConfiguration, iFFTPointRecord.UseConfiguration);
                ceResultSet.SetDouble(mColumnOrdinal.DefaultRunningSpeed, iFFTPointRecord.DefaultRunningSpeed);
                ceResultSet.SetByte(mColumnOrdinal.EnvType, iFFTPointRecord.EnvType);
                ceResultSet.SetByte(mColumnOrdinal.EnvDetection, iFFTPointRecord.EnvDetection);
                ceResultSet.SetInt32(mColumnOrdinal.EnvNumberOfLines, iFFTPointRecord.EnvNumberOfLines);
                ceResultSet.SetInt32(mColumnOrdinal.EnvAverages, iFFTPointRecord.EnvAverages);
                ceResultSet.SetByte(mColumnOrdinal.EnvWindow, iFFTPointRecord.EnvWindow);
                ceResultSet.SetDouble(mColumnOrdinal.EnvFreqMax, iFFTPointRecord.EnvFreqMax);
                ceResultSet.SetByte(mColumnOrdinal.VelType, iFFTPointRecord.VelType);
                ceResultSet.SetByte(mColumnOrdinal.VelDetection, iFFTPointRecord.VelDetection);
                ceResultSet.SetInt32(mColumnOrdinal.VelNumberOfLines, iFFTPointRecord.VelNumberOfLines);
                ceResultSet.SetInt32(mColumnOrdinal.VelAverages, iFFTPointRecord.VelAverages);
                ceResultSet.SetByte(mColumnOrdinal.VelWindow, iFFTPointRecord.VelWindow);
                ceResultSet.SetDouble(mColumnOrdinal.VelFreqMax, iFFTPointRecord.VelFreqMax);
                ceResultSet.SetByte(mColumnOrdinal.AcclType, iFFTPointRecord.AcclType);
                ceResultSet.SetByte(mColumnOrdinal.AcclDetection, iFFTPointRecord.AcclDetection);
                ceResultSet.SetInt32(mColumnOrdinal.AcclNumberOfLines, iFFTPointRecord.AcclNumberOfLines);
                ceResultSet.SetInt32(mColumnOrdinal.AcclAverages, iFFTPointRecord.AcclAverages);
                ceResultSet.SetByte(mColumnOrdinal.AcclWindow, iFFTPointRecord.AcclWindow);
                ceResultSet.SetDouble(mColumnOrdinal.AcclFreqMax, iFFTPointRecord.AcclFreqMax);

                // update the record
                ceResultSet.Update();
            }
            catch
            {
                result = false;
            }
            return result;

        }/* end method */


        /// <summary>
        /// Populate an instantiated FFT Point Object with data from a single 
        /// FFTPOINT table record. In the event of a raised exception the 
        /// returned point object will be null.
        /// </summary>
        /// <param name="ceResultSet">Current CE ResultSet record</param>
        /// <param name="iFFTPointRecord">FFT Point object to be populated</param>
        private void PopulateObject(SqlCeResultSet ceResultSet,
                                         ref FFTPointRecord iFFTPointRecord)
        {
            try
            {
                iFFTPointRecord.Uid = ceResultSet.GetGuid(mColumnOrdinal.Uid);
                iFFTPointRecord.Number = ceResultSet.GetInt32(mColumnOrdinal.Number);
                iFFTPointRecord.PointUid = ceResultSet.GetGuid(mColumnOrdinal.PointUid);
                iFFTPointRecord.TakeData = ceResultSet.GetByte(mColumnOrdinal.TakeData);
                iFFTPointRecord.UseConfiguration = ceResultSet.GetByte(mColumnOrdinal.UseConfiguration);
                iFFTPointRecord.DefaultRunningSpeed = ceResultSet.GetDouble(mColumnOrdinal.DefaultRunningSpeed);
                iFFTPointRecord.EnvType = ceResultSet.GetByte(mColumnOrdinal.EnvType);
                iFFTPointRecord.EnvDetection = ceResultSet.GetByte(mColumnOrdinal.EnvDetection);
                iFFTPointRecord.EnvNumberOfLines = ceResultSet.GetInt32(mColumnOrdinal.EnvNumberOfLines);
                iFFTPointRecord.EnvAverages = ceResultSet.GetInt32(mColumnOrdinal.EnvAverages);
                iFFTPointRecord.EnvWindow = ceResultSet.GetByte(mColumnOrdinal.EnvWindow);
                iFFTPointRecord.EnvFreqMax = ceResultSet.GetDouble(mColumnOrdinal.EnvFreqMax);
                iFFTPointRecord.VelType = ceResultSet.GetByte(mColumnOrdinal.VelType);
                iFFTPointRecord.VelDetection = ceResultSet.GetByte(mColumnOrdinal.VelDetection);
                iFFTPointRecord.VelNumberOfLines = ceResultSet.GetInt32(mColumnOrdinal.VelNumberOfLines);
                iFFTPointRecord.VelAverages = ceResultSet.GetInt32(mColumnOrdinal.VelAverages);
                iFFTPointRecord.VelWindow = ceResultSet.GetByte(mColumnOrdinal.VelWindow);
                iFFTPointRecord.VelFreqMax = ceResultSet.GetDouble(mColumnOrdinal.VelFreqMax);
                iFFTPointRecord.AcclType = ceResultSet.GetByte(mColumnOrdinal.AcclType);
                iFFTPointRecord.AcclDetection = ceResultSet.GetByte(mColumnOrdinal.AcclDetection);
                iFFTPointRecord.AcclNumberOfLines = ceResultSet.GetInt32(mColumnOrdinal.AcclNumberOfLines);
                iFFTPointRecord.AcclAverages = ceResultSet.GetInt32(mColumnOrdinal.AcclAverages);
                iFFTPointRecord.AcclWindow = ceResultSet.GetByte(mColumnOrdinal.AcclWindow);
                iFFTPointRecord.AcclFreqMax = ceResultSet.GetDouble(mColumnOrdinal.AcclFreqMax);
            }
            catch
            {
                iFFTPointRecord = null;
            }

        }/* end method */


        #endregion

    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------