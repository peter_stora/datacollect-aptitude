﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Static class containing a list of queries required to clear each table
   /// </summary>
   public static class DeleteQuery
   {
      /// <summary>
      /// Query string "DELETE FROM C_NOTE;"
      /// </summary>
      public static string CLEAR_C_NOTE = "DELETE FROM C_NOTE;";

      /// <summary>
      /// Query string "DELETE FROM C_NOTES;"
      /// </summary>
      public static string CLEAR_C_NOTES = "DELETE FROM C_NOTES;";

      /// <summary>
      /// Query string "DELETE FROM CONDITIONALPOINT;"
      /// </summary>
      public static string CLEAR_CONDITIONALPOINT = "DELETE FROM CONDITIONALPOINT;";

      /// <summary>
      /// Query string "DELETE FROM DERIVEDITEMS;"
      /// </summary>
      public static string CLEAR_DERIVEDITEMS = "DELETE FROM DERIVEDITEMS;";

      /// <summary>
      /// Query string "DELETE FROM DERIVEDPOINT;"
      /// </summary>
      public static string CLEAR_DERIVEDPOINT = "DELETE FROM DERIVEDPOINT;";

      /// <summary>
      /// Query string "DELETE FROM DEVICEPROFILE;"
      /// </summary>
      public static string CLEAR_DEVICEPROFILE = "DELETE FROM DEVICEPROFILE;";

      /// <summary>
      /// Query string "DELETE FROM ENVDATA;"
      /// </summary>
      public static string CLEAR_ENVDATA = "DELETE FROM ENVDATA;";

      /// <summary>
      /// Query string "DELETE FROM INSPECTION;"
      /// </summary>
      public static string CLEAR_INSPECTION = "DELETE FROM INSPECTION;";

      /// <summary>
      /// Query string "DELETE FROM INSTRUCTIONS;"
      /// </summary>
      public static string CLEAR_INSTRUCTIONS = "DELETE FROM INSTRUCTIONS;";

      /// <summary>
      /// Query string "DELETE FROM LOG;"
      /// </summary>
      public static string CLEAR_LOG = "DELETE FROM LOG;";

      /// <summary>
      /// Query string "DELETE FROM MACHINENOTOPERATINGSKIPS;"
      /// </summary>
      public static string CLEAR_MACHINENOPSKIPS = "DELETE FROM MACHINENOPSKIPS;";

      /// <summary>
      /// Query string "DELETE FROM MACHINEOKSKIPS;"
      /// </summary>
      public static string CLEAR_MACHINEOKSKIPS = "DELETE FROM MACHINEOKSKIPS;";

      /// <summary>
      /// Query string "DELETE FROM MCC;"
      /// </summary>
      public static string CLEAR_MCC = "DELETE FROM MCC;";

      /// <summary>
      /// Query string "DELETE FROM MESSAGE;"
      /// </summary>
      public static string CLEAR_MESSAGE = "DELETE FROM MESSAGE;";

      /// <summary>
      /// Query string "DELETE FROM MESSAGINGPREFERENCES;"
      /// </summary>
      public static string CLEAR_MESSAGINGPREFS = "DELETE FROM MESSAGINGPREFS;";

      /// <summary>
      /// Query string "DELETE FROM NODE;"
      /// </summary>
      public static string CLEAR_NODE = "DELETE FROM NODE;";

      /// <summary>
      /// Query string "DELETE FROM NOTES;"
      /// </summary>
      public static string CLEAR_NOTES = "DELETE FROM NOTES;";

      /// <summary>
      /// Query string "DELETE FROM OPERATORSET;"
      /// </summary>
      public static string CLEAR_OPERATORS = "DELETE FROM OPERATORS;";

      /// <summary>
      /// Query string "DELETE FROM POINTS;"
      /// </summary>
      public static string CLEAR_POINTS = "DELETE FROM POINTS;";

      /// <summary>
      /// Query string "DELETE FROM PROCESS;"
      /// </summary>
      public static string CLEAR_PROCESS = "DELETE FROM PROCESS;";

      /// <summary>
      /// Query string "DELETE FROM REFERENCEMEDIA;"
      /// </summary>
      public static string CLEAR_REFERENCEMEDIA = "DELETE FROM REFERENCEMEDIA;";

      /// <summary>
      /// Query string "DELETE FROM SAVEDMEDIA;"
      /// </summary>
      public static string CLEAR_SAVEDMEDIA = "DELETE FROM SAVEDMEDIA;";

      /// <summary>
      /// Query string "DELETE FROM STRUCTURED;"
      /// </summary>
      public static string CLEAR_STRUCTURED = "DELETE FROM STRUCTURED;";

      /// <summary>
      /// Query string "DELETE FROM STRUCTUREDLOG;"
      /// </summary>
      public static string CLEAR_STRUCTUREDLOG = "DELETE FROM STRUCTUREDLOG;";

      /// <summary>
      /// Query string "DELETE FROM TEMPDATA;"
      /// </summary>
      public static string CLEAR_TEMPDATA = "DELETE FROM TEMPDATA;";

      /// <summary>
      /// Query string "DELETE FROM TEXTDATA;"
      /// </summary>
      public static string CLEAR_TEXTDATA = "DELETE FROM TEXTDATA;";

      /// <summary>
      /// Query string "DELETE FROM USERPREFERENCES;"
      /// </summary>
      public static string CLEAR_USERPREFERENCES = "DELETE FROM USERPREFERENCES;";

      /// <summary>
      /// Query string "DELETE FROM VELDATA;"
      /// </summary>
      public static string CLEAR_VELDATA = "DELETE FROM VELDATA;";

      /// <summary>
      /// Query string "DELETE FROM CORRECTIVEACTION;"
      /// </summary>
      public static string CLEAR_CORRECTIVEACTION = "DELETE FROM CORRECTIVEACTION;";

      /// <summary>
      /// Query string "DELETE FROM PRIORITY;"
      /// </summary>
      public static string CLEAR_PRIORITY = "DELETE FROM PRIORITY;";

      /// <summary>
      /// Query string "DELETE FROM PROBLEM;"
      /// </summary>
      public static string CLEAR_PROBLEM = "DELETE FROM PROBLEM;";

      /// <summary>
      /// Query string "DELETE FROM WORKTYPE;"
      /// </summary>
      public static string CLEAR_WORKTYPE = "DELETE FROM WORKTYPE;";

      /// <summary>
      /// Query string "DELETE FROM WORKNOTIFICATION;"
      /// </summary>
      public static string CLEAR_WORKNOTIFICATION = "DELETE FROM WORKNOTIFICATION;";

      /// <summary>
      /// Query string "DELETE FROM FFTPOINTS;"
      /// </summary>
      public static string CLEAR_FFTPOINT = "DELETE FROM FFTPOINT";

      /// <summary>
      /// Query string "DELETE FROM FFTCHANNEL;"
      /// </summary>
      public static string CLEAR_FFTCHANNELS = "DELETE FROM FFTCHANNEL";

   }/*end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th March 2009 (14:20)
//  Add to project
//----------------------------------------------------------------------------
