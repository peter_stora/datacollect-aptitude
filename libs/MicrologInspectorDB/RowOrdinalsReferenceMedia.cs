﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the REFERENCEMEDIA table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsReferenceMedia
   {
      public int Uid
      {
         get;
         set;
      }

      public int PointHostIndex
      {
         get;
         set;
      }

      public int MediaType
      {
         get;
         set;
      }

      public int FileName
      {
         get;
         set;
      }

      public int FilePath
      {
         get;
         set;
      }

      public int Size
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsReferenceMedia( SqlCeResultSet iCeResultSet )
      {
         GetReferenceMediaTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// REFERENCEMEDIA table. This table will need to be revised in the event of 
      /// any changes being made to the REFERENCEMEDIA table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetReferenceMediaTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.PointHostIndex = iCeResultSet.GetOrdinal( "PointHostIndex" );
         this.MediaType = iCeResultSet.GetOrdinal( "MediaType" );
         this.FileName = iCeResultSet.GetOrdinal( "FileName" );
         this.FileName = iCeResultSet.GetOrdinal( "FilePath" );
         this.Size = iCeResultSet.GetOrdinal( "Size" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------