﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the PRIORITY Table ResultSet
   /// </summary>
   public class ResultSetPriority : ResultSetBase
   {
      #region Private Fields and Constants


      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsPriority mColumnOrdinal = null;

      /// <summary>
      /// Query PRIORITY table based on the Priority key (single result)
      /// </summary>
      private const string QUERY_BY_PRIORITY_KEY = "SELECT * FROM PRIORITY " +
          "WHERE MAPriorityKey = @MAPriorityKey";

      /// <summary>
      /// Query PRIORITY table, get all records ordered by MAPriorityCode
      /// </summary>
      private const string QUERY_ORDER_BY_PRIORITY_CODE = "SELECT * FROM PRIORITY ORDER BY MAPriorityCode";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_PRIORITY;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current PRIORITY table. 
      /// </summary>
      /// <param name="iValue">populated Cmms Priority object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CmmsPriorityRecord iValue )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PRIORITY";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update table record
               result = WriteRecord( iValue, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the PRIORITY table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of Priority objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCmmsPriorityRecords iNotifications )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PRIORITY";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all PRIORITY records to the table 
               for ( int i = 0; i < iNotifications.Count; ++i )
               {
                  result = result && WriteRecord( iNotifications.Priority[ i ], ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CMMS Priority Records, ordered by
      /// MAPriorityKey
      /// </summary>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsPriorities GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCmmsPriorities correctiveActions = new EnumCmmsPriorities();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_ORDER_BY_PRIORITY_CODE;

            // reference the lookup field as a parameter
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PRIORITY table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsPriorityRecord correctiveAction = new CmmsPriorityRecord();
                  PopulateObject( ceResultSet, ref correctiveAction );
                  correctiveActions.AddPriorityRecord( correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveActions;

      }/* end method */


      /// <summary>
      /// Returns a single Priority Record object from the PRIORITY
      /// table using the Priority Key as the lookup reference. 
      /// </summary>
      /// <param name="iPriorityKey">CMMMS Priority Key value</param>
      /// <returns>Populated Priority object</returns>
      public CmmsPriorityRecord GetRecord( Int32 iPriorityKey )
      {
         OpenCeConnection();

         // reset the default profile name
         CmmsPriorityRecord correctiveAction = new CmmsPriorityRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_PRIORITY_KEY;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@MAPriorityKey", SqlDbType.Int ).Value = iPriorityKey;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PRIORITY table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveAction;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// PRIORITY table. This table will need to be revised in the event of any 
      /// changes being made to the PRIORITY table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsPriority( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new WorkNotification record and add it 
      /// to the current PRIORITY table.
      /// </summary>
      /// <param name="iValue">Populated Cmms Priority object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CmmsPriorityRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.MAPriorityKey ] = iValue.MAPriorityKey;

            if ( iValue.MAPriorityCode != null )
               newRecord[ mColumnOrdinal.MAPriorityCode ] = iValue.MAPriorityCode;
            else
               newRecord[ mColumnOrdinal.MAPriorityCode ] = string.Empty;

            if ( iValue.PriorityText != null )
               newRecord[ mColumnOrdinal.PriorityText ] = iValue.PriorityText;
            else
               newRecord[ mColumnOrdinal.PriorityText ] = string.Empty;

            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single PRIORITY table record
      /// </summary>
      /// <param name="iNotification">Updated CmmsPriority object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CmmsPriorityRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.MAPriorityKey, iValue.MAPriorityKey );

            if ( iValue.MAPriorityCode != null )
               ceResultSet.SetString( mColumnOrdinal.MAPriorityCode,
                  iValue.MAPriorityCode );

            if ( iValue.PriorityText != null )
               ceResultSet.SetString( mColumnOrdinal.PriorityText,
                  iValue.PriorityText );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotification Object with data from a single 
      /// PRIORITY table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iValue">CmmsPriority object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CmmsPriorityRecord iValue )
      {
         try
         {
            iValue.MAPriorityKey = ceResultSet.GetInt32( mColumnOrdinal.MAPriorityKey );

            if ( ceResultSet[ mColumnOrdinal.MAPriorityCode ] != DBNull.Value )
               iValue.MAPriorityCode =
                  ceResultSet.GetString( mColumnOrdinal.MAPriorityCode );

            if ( ceResultSet[ mColumnOrdinal.PriorityText ] != DBNull.Value )
               iValue.PriorityText =
                  ceResultSet.GetString( mColumnOrdinal.PriorityText );
         }
         catch
         {
            iValue = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------