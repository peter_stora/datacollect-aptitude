﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the STRUCTURED table. This structure must be revised following 
    /// any changes made to the table's field structure.
    /// </summary>
    public class RowOrdinalsStructuredRoute
    {
        public int Uid { get; set; }
        public int RouteId { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int Duration { get; set; }
        public int OperId { get; set; }
        public int NextPoint { get; set; }
        public int InternalStart { get; set; }
        public int PercentComplete { get; set; }
        public int Status { get; set; }
        public int LastPoint { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsStructuredRoute(SqlCeResultSet iCeResultSet)
        {
            GetStructuredRouteTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// STRUCTURED table. This method will need to be revised in the event of 
        /// any changes being made to the STRUCTURED table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetStructuredRouteTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.Uid = iCeResultSet.GetOrdinal("Uid");
            this.RouteId = iCeResultSet.GetOrdinal("RouteId");
            this.StartTime = iCeResultSet.GetOrdinal("StartTime");
            this.EndTime = iCeResultSet.GetOrdinal("EndTime");
            this.Duration = iCeResultSet.GetOrdinal("Duration");
            this.OperId = iCeResultSet.GetOrdinal("OperatorId");
            this.NextPoint = iCeResultSet.GetOrdinal("NextPoint");
            this.InternalStart = iCeResultSet.GetOrdinal("InternalStart");
            this.PercentComplete = iCeResultSet.GetOrdinal("PercentComplete");
            this.Status = iCeResultSet.GetOrdinal("Status");
            this.LastPoint = iCeResultSet.GetOrdinal("LastPoint");

        }/* end method */

    }/* end class */


}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------
