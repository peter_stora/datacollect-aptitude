﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the INSTRUCTIONS Table ResultSet
   /// </summary>
   public partial class ResultSetInstructionsTable : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsInstructions mColumnOrdinal = null;

      /// <summary>
      /// Query INSTRUCTIONS table based on the external Point Uid field. 
      /// This query will return an enumerated series of records ordered
      /// by the field "FormulaItemPosition".
      /// </summary>
      private const string QUERY_BY_POINTUID = "SELECT * FROM INSTRUCTIONS " +
          "WHERE PointUid = @PointUid ORDER BY TextType ASC";


      /// <summary>
      /// Query INSTRUCTIONS table based on the external PointHostIndex field. 
      /// This query will return an enumerated series of records ordered
      /// by the field "FormulaItemPosition".
      /// </summary>
      private const string QUERY_BY_POINTHOSTINDEX = "SELECT * FROM INSTRUCTIONS " +
          "WHERE PointHostIndex = @PointHostIndex ORDER BY TextType ASC";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_INSTRUCTIONS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current INSTRUCTIONS table. 
      /// </summary>
      /// <param name="iInstructionsObject">populated InstructionsRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( InstructionsRecord iInstructionsObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSTRUCTIONS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write to table
               result = WriteRecord( iInstructionsObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of InstructionRecord objects to the INSTRUCTIONS 
      /// table and return the process updates
      /// </summary>
      /// <param name="iEnumNodes">Enumerated List of Instruction Records</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumInstructions iInstructionRecords,
          ref DataProgress iProgress )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSTRUCTIONS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the available records
               foreach ( InstructionsRecord instruction in iInstructionRecords )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( instruction, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the INSTRUCTIONS table. 
      /// </summary>
      /// <param name="iDerivedItemObjects">populated Instruction objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumInstructions iInstructionRecords )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSTRUCTIONS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the available records
               foreach ( InstructionsRecord instruction in iInstructionRecords )
               {
                  // write record to table
                  if ( !WriteRecord( instruction, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns a series of Instruction record objects from the INSTRUCTIONS 
      /// table using the PointUid field (which links to POINTS.Uid) as 
      /// the lookup reference.
      /// </summary>
      /// <param name="iPointUid">External link to Point Uid</param>
      /// <returns>Populated Instructions Point object or null on error</returns>
      public EnumInstructions GetRecords( Guid iPointUid )
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumInstructions instructions = new EnumInstructions();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINTUID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value
                = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the INSTRUCTIONS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record objects
               while ( ceResultSet.Read() )
               {
                  InstructionsRecord instruction = new InstructionsRecord();
                  PopulateObject( ceResultSet, ref instruction );
                  instructions.AddInstructionsRecord( instruction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return instructions;

      }/* end method */


      /// <summary>
      /// Returns a series of Instruction record objects from the INSTRUCTIONS 
      /// table using the PointUid field (which links to POINTS.Uid) as 
      /// the lookup reference.
      /// </summary>
      /// <param name="iPointUid">External link to Point Uid</param>
      /// <returns>Populated Instructions Point object or null on error</returns>
      public EnumInstructions GetRecords( int iPointHostIndex )
      {
         OpenCeConnection();

         // create the return enumeration object
         EnumInstructions instructions = new EnumInstructions();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINTHOSTINDEX;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value
                = iPointHostIndex;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the INSTRUCTIONS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record objects
               while ( ceResultSet.Read() )
               {
                  InstructionsRecord instruction = new InstructionsRecord();
                  PopulateObject( ceResultSet, ref instruction );
                  instructions.AddInstructionsRecord( instruction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return instructions;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all INSTRUCTION records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumInstructions GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumInstructions instructions = new EnumInstructions();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSTRUCTIONS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = 
                   command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the INSTRUCTIONS table. As this is a 'one shot' the column  
               // ordinal object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  InstructionsRecord instruction = new InstructionsRecord();
                  PopulateObject( ceResultSet, ref instruction );
                  instructions.AddInstructionsRecord( instruction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return instructions;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// INSTRUCTIONS table. This table will need to be revised in the event of any 
      /// changes being made to the INSTRUCTIONS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsInstructions( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new INSTRUCTIONS record and add it 
      /// to the current INSTRUCTIONS table.
      /// </summary>
      /// <param name="iInstruction">Populated INSTRUCTIONS Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( InstructionsRecord iInstruction,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.PointUid ] = iInstruction.PointUid;
            newRecord[ mColumnOrdinal.TextFormat ] = iInstruction.TextFormat;
            newRecord[ mColumnOrdinal.TextType ] = iInstruction.TextType;

            if ( iInstruction.TextTitle != null )
               newRecord[ mColumnOrdinal.TextTitle ] = iInstruction.TextTitle;
            else
               newRecord[ mColumnOrdinal.TextTitle ] = string.Empty;

            if ( iInstruction.TextBody != null )
               newRecord[ mColumnOrdinal.TextBody ] = iInstruction.TextBody;
            else
               newRecord[ mColumnOrdinal.TextBody ] = string.Empty;

            newRecord[ mColumnOrdinal.PointHostIndex ] = iInstruction.PointHostIndex;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single INSTRUCTIONS table record
      /// </summary>
      /// <param name="iInstruction">Updated INSTRUCTIONS Record object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( InstructionsRecord iInstruction,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetGuid( mColumnOrdinal.PointUid, iInstruction.PointUid );
            ceResultSet.SetByte( mColumnOrdinal.TextFormat, iInstruction.TextFormat );
            ceResultSet.SetByte( mColumnOrdinal.TextType, iInstruction.TextType );
            
            if (iInstruction.TextTitle != null)
               ceResultSet.SetString( mColumnOrdinal.TextTitle, 
                  iInstruction.TextTitle );
            
            if (iInstruction.TextBody != null)
               ceResultSet.SetString( mColumnOrdinal.TextBody, 
                  iInstruction.TextBody );
            
            ceResultSet.SetInt32( mColumnOrdinal.PointHostIndex, 
               iInstruction.PointHostIndex );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated INSTRUCTIONS Object with data from a single 
      /// INSTRUCTIONS table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iInstruction">INSTRUCTIONS object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref InstructionsRecord iInstruction )
      {
         try
         {
            iInstruction.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iInstruction.TextFormat = ceResultSet.GetByte( mColumnOrdinal.TextFormat );
            iInstruction.TextType = ceResultSet.GetByte( mColumnOrdinal.TextType );

            if ( ceResultSet[ mColumnOrdinal.TextTitle ] != DBNull.Value )
               iInstruction.TextTitle = ceResultSet.GetString( mColumnOrdinal.TextTitle );

            if ( ceResultSet[ mColumnOrdinal.TextBody ] != DBNull.Value )
               iInstruction.TextBody = ceResultSet.GetString( mColumnOrdinal.TextBody );
            
            iInstruction.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
         }
         catch
         {
            iInstruction = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------