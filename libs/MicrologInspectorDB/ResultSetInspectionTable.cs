﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the INSPECTION Table ResultSet
   /// </summary>
   public class ResultSetInspectionTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsInspection mColumnOrdinal = null;

      /// <summary>
      /// Query Points table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM INSPECTION " +
          "WHERE PointUid = @PointUid";

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_ENUM_BY_POINT_UID = "SELECT * FROM PROCESS " +
          "WHERE ";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_INSPECTION;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single InspectionRecord object to the INSPECTION table. 
      /// </summary>
      /// <param name="iInspection">Populated InspectionRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( InspectionRecord iInspection )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSPECTION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               if ( !WriteRecord( iInspection, ceResultSet ) )
               {
                  result = false;
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of InspectionRecord objects to the INSPECTION 
      /// table and also update the progress bar updates object
      /// </summary>
      /// <param name="iEnumInspections">Enumerated List of InspectionRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumInspection iEnumInspections, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the Inspection objects list
         iEnumInspections.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSPECTION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( InspectionRecord inspection in iEnumInspections )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( inspection, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of InspectionRecord objects to the INSPECTION 
      /// table.
      /// </summary>
      /// <param name="iEnumInspections">Enumerated List of InspectionRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumInspection iEnumInspections )
      {
         OpenCeConnection();

         // reset the Inspection objects list
         iEnumInspections.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSPECTION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( InspectionRecord inspection in iEnumInspections )
               {
                  if ( !WriteRecord( inspection, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Populate and return a single InspectionRecord object given the 
      /// associated Point Uid.
      /// </summary>
      /// <param name="iPointUid">Point Uid guid</param>
      /// <returns>Populated InspectionRecord object</returns>
      public InspectionRecord GetRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // create the default object
         InspectionRecord inspection = new InspectionRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the INSPECTION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // populate the inspection object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref inspection );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return inspection;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all INSPECTION records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumInspection GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumInspection inspections = new EnumInspection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "INSPECTION";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CONDITIONALPOINT table. As this is a 'one shot' the column  
               // ordinal object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  InspectionRecord inspection = new InspectionRecord();
                  PopulateObject( ceResultSet, ref inspection );
                  if ( inspection != null )
                     inspections.AddInspectionRecord( inspection );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return inspections;

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Return an enumerated list of populated Inspection objects given an
      /// array of Parent Uid (guids).
      /// </summary>
      /// <param name="iPointUids">Array of Point Uid guids</param>
      /// <returns>Enumerated list of populated Process objects - or null on failure</returns>
      public EnumInspection GetEnumRecords( Guid[] iPointUids )
      {
         OpenCeConnection();

         // create new enumerated list of Inspection objects
         EnumInspection enumInspection = new EnumInspection();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( Guid pointUid in iPointUids )
            {
               // if this is not the last parameter then add an additional OR clause
               if ( paramCount < iPointUids.Length )
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
               }
               // otherwise terminate query with semi-colon
               else
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() +
                      " ORDER BY Number;" );
               }
               command.Parameters.Add( "@P" + paramCount.ToString(), SqlDbType.UniqueIdentifier ).Value = pointUid;

               // increment the parameter count
               ++paramCount;
            }

            // referebce the full command text
            command.CommandText = query.ToString();

            try
            {
               // execure the result set
               using ( SqlCeResultSet ceResultSet = 
                  command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the INSPECTION table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // convert Process record to a InspectionRecord object
                  while ( ceResultSet.Read() )
                  {
                     InspectionRecord inspection = new InspectionRecord();
                     PopulateObject( ceResultSet, ref inspection );
                     enumInspection.AddInspectionRecord( inspection );
                  }
                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               enumInspection = null;
            }
         }
         CloseCeConnection();
         return enumInspection;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of populated Inspection objects given an 
      /// enumerated list of populated Point objects.
      /// </summary>
      /// <param name="iPointUids">Enumerated list of Point objects</param>
      /// <returns>Enumerated list of populated Process objects - or null on failure</returns>
      public EnumInspection GetEnumRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // create new enumerated list of Process objects
         EnumInspection enumInspection = new EnumInspection();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( PointRecord point in iEnumPoints )
            {
               // ensure we are dealing with a process type
               if ( IsPointInspectionType( point ) )
               {
                  // if this is not the last parameter then add an additional OR clause
                  if ( paramCount < iEnumPoints.Count )
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
                  }
                  // otherwise terminate query with semi-colon
                  else
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() +
                         " ORDER BY Number;" );
                  }

                  // add the new parameter object
                  command.Parameters.Add( "@P" + paramCount.ToString(),
                      SqlDbType.UniqueIdentifier ).Value = point.Uid;

                  // increment the parameter count
                  ++paramCount;
               }
            }

            // if no process points found then just return null!
            if ( paramCount > 1 )
            {
               // referebce the full command text
               command.CommandText = query.ToString();

               try
               {
                  // execure the result set
                  using ( SqlCeResultSet ceResultSet = 
                     command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated with 
                     // the INSPECTION table. As this is a 'one shot' the column ordinal 
                     // object remains a singleton
                     //
                     GetTableColumns( ceResultSet );

                     // convert Process record to a ProcessRecord object
                     while ( ceResultSet.Read() )
                     {
                        InspectionRecord inspection = new InspectionRecord();
                        PopulateObject( ceResultSet, ref inspection );
                        enumInspection.AddInspectionRecord( inspection );
                     }
                     // close the resultSet
                     ceResultSet.Close();
                  }
               }
               catch
               {
                  enumInspection = null;
               }
            }
         }
         CloseCeConnection();
         return enumInspection;

      }/* end method */

      #endregion


      #region Private Methods

      /// <summary>
      /// Returns true if the PointRecord object is an Inspection Type
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <returns>True if type is Inspection</returns>
      private Boolean IsPointInspectionType( PointRecord iPointRecord )
      {
         FieldTypes.ProcessType prType = (FieldTypes.ProcessType) iPointRecord.ProcessType;

         if ( ( prType == FieldTypes.ProcessType.MultiSelectInspection ) ||
         ( prType == FieldTypes.ProcessType.SingleSelectInspection ) )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// INSPECTION table. This table will need to be revised in the event of any 
      /// changes being made to the INSPECTION table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsInspection( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new INSPECTION record and add it 
      /// to the current INSPECTION table.
      /// </summary>
      /// <param name="iInspectionObject">Populated Inspection Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( InspectionRecord iInspectionObject, SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iInspectionObject.Uid;
            newRecord[ mColumnOrdinal.Number ] = iInspectionObject.Number;
            newRecord[ mColumnOrdinal.PointUid ] = iInspectionObject.PointUid;

            if ( iInspectionObject.Question != null )
               newRecord[ mColumnOrdinal.Question ] = iInspectionObject.Question;
            else
               newRecord[ mColumnOrdinal.Question ] = string.Empty;

            if ( iInspectionObject.AlertText != null )
               newRecord[ mColumnOrdinal.AlertText ] = iInspectionObject.AlertText;
            else
               newRecord[ mColumnOrdinal.AlertText ] = string.Empty;

            if ( iInspectionObject.DangerText != null )
               newRecord[ mColumnOrdinal.DangerText ] = iInspectionObject.DangerText;
            else
               newRecord[ mColumnOrdinal.DangerText ] = string.Empty;
            
            newRecord[ mColumnOrdinal.NumberOfChoices ] = iInspectionObject.NumberOfChoices;

            if ( iInspectionObject.Choice1 != null )
               newRecord[ mColumnOrdinal.Choice1 ] = iInspectionObject.Choice1;
            else
               newRecord[ mColumnOrdinal.Choice1 ] = string.Empty;

            if ( iInspectionObject.Choice2 != null )
               newRecord[ mColumnOrdinal.Choice2 ] = iInspectionObject.Choice2;
            else
               newRecord[ mColumnOrdinal.Choice2 ] = string.Empty;

            if ( iInspectionObject.Choice3 != null )
               newRecord[ mColumnOrdinal.Choice3 ] = iInspectionObject.Choice3;
            else
               newRecord[ mColumnOrdinal.Choice3 ] = string.Empty;

            if ( iInspectionObject.Choice4 != null )
               newRecord[ mColumnOrdinal.Choice4 ] = iInspectionObject.Choice4;
            else
               newRecord[ mColumnOrdinal.Choice4 ] = string.Empty;

            if ( iInspectionObject.Choice5 != null )
               newRecord[ mColumnOrdinal.Choice5 ] = iInspectionObject.Choice5;
            else
               newRecord[ mColumnOrdinal.Choice5 ] = string.Empty;

            if ( iInspectionObject.Choice6 != null )
               newRecord[ mColumnOrdinal.Choice6 ] = iInspectionObject.Choice6;
            else
               newRecord[ mColumnOrdinal.Choice6 ] = string.Empty;

            if ( iInspectionObject.Choice7 != null )
               newRecord[ mColumnOrdinal.Choice7 ] = iInspectionObject.Choice7;
            else
               newRecord[ mColumnOrdinal.Choice7 ] = string.Empty;

            if ( iInspectionObject.Choice8 != null )
               newRecord[ mColumnOrdinal.Choice8 ] = iInspectionObject.Choice8;
            else
               newRecord[ mColumnOrdinal.Choice8 ] = string.Empty;

            if ( iInspectionObject.Choice9 != null )
               newRecord[ mColumnOrdinal.Choice9 ] = iInspectionObject.Choice9;
            else
               newRecord[ mColumnOrdinal.Choice9 ] = string.Empty;

            if ( iInspectionObject.Choice10 != null )
               newRecord[ mColumnOrdinal.Choice10 ] = iInspectionObject.Choice10;
            else
               newRecord[ mColumnOrdinal.Choice10 ] = string.Empty;

            if ( iInspectionObject.Choice11 != null )
               newRecord[ mColumnOrdinal.Choice11 ] = iInspectionObject.Choice11;
            else
               newRecord[ mColumnOrdinal.Choice11 ] = string.Empty;

            if ( iInspectionObject.Choice12 != null )
               newRecord[ mColumnOrdinal.Choice12 ] = iInspectionObject.Choice12;
            else
               newRecord[ mColumnOrdinal.Choice12 ] = string.Empty;

            if ( iInspectionObject.Choice13 != null )
               newRecord[ mColumnOrdinal.Choice13 ] = iInspectionObject.Choice13;
            else
               newRecord[ mColumnOrdinal.Choice13 ] = string.Empty;

            if ( iInspectionObject.Choice14 != null )
               newRecord[ mColumnOrdinal.Choice14 ] = iInspectionObject.Choice14;
            else
               newRecord[ mColumnOrdinal.Choice14 ] = string.Empty;

            if ( iInspectionObject.Choice15 != null )
               newRecord[ mColumnOrdinal.Choice15 ] = iInspectionObject.Choice15;
            else
               newRecord[ mColumnOrdinal.Choice15 ] = string.Empty;

            newRecord[ mColumnOrdinal.AlarmType1 ] = iInspectionObject.AlarmType1;
            newRecord[ mColumnOrdinal.AlarmType2 ] = iInspectionObject.AlarmType2;
            newRecord[ mColumnOrdinal.AlarmType3 ] = iInspectionObject.AlarmType3;
            newRecord[ mColumnOrdinal.AlarmType4 ] = iInspectionObject.AlarmType4;
            newRecord[ mColumnOrdinal.AlarmType5 ] = iInspectionObject.AlarmType5;
            newRecord[ mColumnOrdinal.AlarmType6 ] = iInspectionObject.AlarmType6;
            newRecord[ mColumnOrdinal.AlarmType7 ] = iInspectionObject.AlarmType7;
            newRecord[ mColumnOrdinal.AlarmType8 ] = iInspectionObject.AlarmType8;
            newRecord[ mColumnOrdinal.AlarmType9 ] = iInspectionObject.AlarmType9;
            newRecord[ mColumnOrdinal.AlarmType10 ] = iInspectionObject.AlarmType10;
            newRecord[ mColumnOrdinal.AlarmType11 ] = iInspectionObject.AlarmType11;
            newRecord[ mColumnOrdinal.AlarmType12 ] = iInspectionObject.AlarmType12;
            newRecord[ mColumnOrdinal.AlarmType13 ] = iInspectionObject.AlarmType13;
            newRecord[ mColumnOrdinal.AlarmType14 ] = iInspectionObject.AlarmType14;
            newRecord[ mColumnOrdinal.AlarmType15 ] = iInspectionObject.AlarmType15;

            if ( iInspectionObject.BaselineDataTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.BaselineDataTime ] = iInspectionObject.BaselineDataTime;

            newRecord[ mColumnOrdinal.BaselineDataValue ] = iInspectionObject.BaselineDataValue;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single INSPECTION table record
      /// </summary>
      /// <param name="iInspectionRecord">Updated InspectionRecord object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( InspectionRecord iInspectionRecord, SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iInspectionRecord.Uid );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iInspectionRecord.Number );
            CeResultSet.SetGuid( mColumnOrdinal.PointUid, iInspectionRecord.PointUid );

            if ( iInspectionRecord.Question != null )
               CeResultSet.SetString( mColumnOrdinal.Question, iInspectionRecord.Question );

            if ( iInspectionRecord.AlertText != null )
               CeResultSet.SetString( mColumnOrdinal.AlertText, iInspectionRecord.AlertText );

            if ( iInspectionRecord.DangerText != null )
               CeResultSet.SetString( mColumnOrdinal.DangerText, iInspectionRecord.DangerText );
            
            CeResultSet.SetByte( mColumnOrdinal.NumberOfChoices, iInspectionRecord.NumberOfChoices );

            if ( iInspectionRecord.Choice1 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice1, iInspectionRecord.Choice1 );

            if ( iInspectionRecord.Choice2 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice2, iInspectionRecord.Choice2 );

            if ( iInspectionRecord.Choice3 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice3, iInspectionRecord.Choice3 );

            if ( iInspectionRecord.Choice4 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice4, iInspectionRecord.Choice4 );

            if ( iInspectionRecord.Choice5 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice5, iInspectionRecord.Choice5 );

            if ( iInspectionRecord.Choice6 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice6, iInspectionRecord.Choice6 );

            if ( iInspectionRecord.Choice7 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice7, iInspectionRecord.Choice7 );

            if ( iInspectionRecord.Choice8 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice8, iInspectionRecord.Choice8 );

            if ( iInspectionRecord.Choice9 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice9, iInspectionRecord.Choice9 );

            if ( iInspectionRecord.Choice10 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice10, iInspectionRecord.Choice10 );

            if ( iInspectionRecord.Choice11 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice11, iInspectionRecord.Choice11 );

            if ( iInspectionRecord.Choice12 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice12, iInspectionRecord.Choice12 );

            if ( iInspectionRecord.Choice13 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice13, iInspectionRecord.Choice13 );

            if ( iInspectionRecord.Choice14 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice14, iInspectionRecord.Choice14 );

            if ( iInspectionRecord.Choice15 != null )
               CeResultSet.SetString( mColumnOrdinal.Choice15, iInspectionRecord.Choice15 );

            CeResultSet.SetByte( mColumnOrdinal.AlarmType1, iInspectionRecord.AlarmType1 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType2, iInspectionRecord.AlarmType2 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType3, iInspectionRecord.AlarmType3 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType4, iInspectionRecord.AlarmType4 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType5, iInspectionRecord.AlarmType5 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType6, iInspectionRecord.AlarmType6 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType7, iInspectionRecord.AlarmType7 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType8, iInspectionRecord.AlarmType8 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType9, iInspectionRecord.AlarmType9 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType10, iInspectionRecord.AlarmType10 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType11, iInspectionRecord.AlarmType11 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType12, iInspectionRecord.AlarmType12 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType13, iInspectionRecord.AlarmType13 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType14, iInspectionRecord.AlarmType14 );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType15, iInspectionRecord.AlarmType15 );

            if ( iInspectionRecord.BaselineDataTime.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.BaselineDataTime, iInspectionRecord.BaselineDataTime );

            CeResultSet.SetByte( mColumnOrdinal.BaselineDataValue, iInspectionRecord.BaselineDataValue );

            // update the record
            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated InspectionRecord Object with data from a single 
      /// INSPECTION table record. In the event of a raised exception the returned 
      /// Inspection object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iInspection">Inspection object to be populated</param>
      private void PopulateObject( SqlCeResultSet CeResultSet,
          ref InspectionRecord iInspectionRecord )
      {
         try
         {
            iInspectionRecord.Uid = CeResultSet.GetGuid( mColumnOrdinal.Uid );
            iInspectionRecord.Number = CeResultSet.GetInt32( mColumnOrdinal.Number );
            iInspectionRecord.PointUid = CeResultSet.GetGuid( mColumnOrdinal.PointUid );

            if ( CeResultSet[ mColumnOrdinal.Question ] != DBNull.Value )
               iInspectionRecord.Question = CeResultSet.GetString( mColumnOrdinal.Question );

            if ( CeResultSet[ mColumnOrdinal.AlertText ] != DBNull.Value )
               iInspectionRecord.AlertText = CeResultSet.GetString( mColumnOrdinal.AlertText );

            if ( CeResultSet[ mColumnOrdinal.DangerText ] != DBNull.Value )
               iInspectionRecord.DangerText = CeResultSet.GetString( mColumnOrdinal.DangerText );
            
            iInspectionRecord.NumberOfChoices = CeResultSet.GetByte( mColumnOrdinal.NumberOfChoices );

            if ( CeResultSet[ mColumnOrdinal.Choice1 ] != DBNull.Value )
               iInspectionRecord.Choice1 = CeResultSet.GetString( mColumnOrdinal.Choice1 );

            if ( CeResultSet[ mColumnOrdinal.Choice2 ] != DBNull.Value )
               iInspectionRecord.Choice2 = CeResultSet.GetString( mColumnOrdinal.Choice2 );

            if ( CeResultSet[ mColumnOrdinal.Choice3 ] != DBNull.Value )
               iInspectionRecord.Choice3 = CeResultSet.GetString( mColumnOrdinal.Choice3 );

            if ( CeResultSet[ mColumnOrdinal.Choice4 ] != DBNull.Value )
               iInspectionRecord.Choice4 = CeResultSet.GetString( mColumnOrdinal.Choice4 );

            if ( CeResultSet[ mColumnOrdinal.Choice5 ] != DBNull.Value )
               iInspectionRecord.Choice5 = CeResultSet.GetString( mColumnOrdinal.Choice5 );

            if ( CeResultSet[ mColumnOrdinal.Choice6 ] != DBNull.Value )
               iInspectionRecord.Choice6 = CeResultSet.GetString( mColumnOrdinal.Choice6 );

            if ( CeResultSet[ mColumnOrdinal.Choice7 ] != DBNull.Value )
               iInspectionRecord.Choice7 = CeResultSet.GetString( mColumnOrdinal.Choice7 );

            if ( CeResultSet[ mColumnOrdinal.Choice8 ] != DBNull.Value )
               iInspectionRecord.Choice8 = CeResultSet.GetString( mColumnOrdinal.Choice8 );

            if ( CeResultSet[ mColumnOrdinal.Choice9 ] != DBNull.Value )
               iInspectionRecord.Choice9 = CeResultSet.GetString( mColumnOrdinal.Choice9 );

            if ( CeResultSet[ mColumnOrdinal.Choice10 ] != DBNull.Value )
               iInspectionRecord.Choice10 = CeResultSet.GetString( mColumnOrdinal.Choice10 );

            if ( CeResultSet[ mColumnOrdinal.Choice11 ] != DBNull.Value )
               iInspectionRecord.Choice11 = CeResultSet.GetString( mColumnOrdinal.Choice11 );

            if ( CeResultSet[ mColumnOrdinal.Choice12 ] != DBNull.Value )
               iInspectionRecord.Choice12 = CeResultSet.GetString( mColumnOrdinal.Choice12 );

            if ( CeResultSet[ mColumnOrdinal.Choice13 ] != DBNull.Value )
               iInspectionRecord.Choice13 = CeResultSet.GetString( mColumnOrdinal.Choice13 );

            if ( CeResultSet[ mColumnOrdinal.Choice14 ] != DBNull.Value )
               iInspectionRecord.Choice14 = CeResultSet.GetString( mColumnOrdinal.Choice14 );

            if ( CeResultSet[ mColumnOrdinal.Choice15 ] != DBNull.Value )
               iInspectionRecord.Choice15 = CeResultSet.GetString( mColumnOrdinal.Choice15 );
            
            iInspectionRecord.AlarmType1 = CeResultSet.GetByte( mColumnOrdinal.AlarmType1 );
            iInspectionRecord.AlarmType2 = CeResultSet.GetByte( mColumnOrdinal.AlarmType2 );
            iInspectionRecord.AlarmType3 = CeResultSet.GetByte( mColumnOrdinal.AlarmType3 );
            iInspectionRecord.AlarmType4 = CeResultSet.GetByte( mColumnOrdinal.AlarmType4 );
            iInspectionRecord.AlarmType5 = CeResultSet.GetByte( mColumnOrdinal.AlarmType5 );
            iInspectionRecord.AlarmType6 = CeResultSet.GetByte( mColumnOrdinal.AlarmType6 );
            iInspectionRecord.AlarmType7 = CeResultSet.GetByte( mColumnOrdinal.AlarmType7 );
            iInspectionRecord.AlarmType8 = CeResultSet.GetByte( mColumnOrdinal.AlarmType8 );
            iInspectionRecord.AlarmType9 = CeResultSet.GetByte( mColumnOrdinal.AlarmType9 );
            iInspectionRecord.AlarmType10 = CeResultSet.GetByte( mColumnOrdinal.AlarmType10 );
            iInspectionRecord.AlarmType11 = CeResultSet.GetByte( mColumnOrdinal.AlarmType11 );
            iInspectionRecord.AlarmType12 = CeResultSet.GetByte( mColumnOrdinal.AlarmType12 );
            iInspectionRecord.AlarmType13 = CeResultSet.GetByte( mColumnOrdinal.AlarmType13 );
            iInspectionRecord.AlarmType14 = CeResultSet.GetByte( mColumnOrdinal.AlarmType14 );
            iInspectionRecord.AlarmType15 = CeResultSet.GetByte( mColumnOrdinal.AlarmType15 );

            if ( CeResultSet[ mColumnOrdinal.BaselineDataTime ] != DBNull.Value )
               iInspectionRecord.BaselineDataTime = CeResultSet.GetDateTime( mColumnOrdinal.BaselineDataTime );
            
            iInspectionRecord.BaselineDataValue = CeResultSet.GetByte( mColumnOrdinal.BaselineDataValue );
         }
         catch
         {
            iInspectionRecord = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$#
//
//  Revision 2.0 APinkerton 26th April 2012
//  Increase inspection fields from five to fifteen
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------