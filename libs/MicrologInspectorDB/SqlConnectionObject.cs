﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Microlog Inspector SQL Connection Object. This class manages a single
    /// connection and entry point to the Microlog Inspector Database and will
    /// work with both "run time" and "design time" application builds
    /// </summary>
    public class SqlConnectionObject : IDisposable
    {
        #region Private Fields

        /// <summary>
        /// The default name of the SQL Compact Datasource
        /// </summary>
        public static string DEFAULT_DATASOURCE = "InspectorDB.sdf";

        /// <summary>
        /// Connection string Data Source field
        /// </summary>
        public static string CONNECTION_STRING = "Data Source";

        /// <summary>
        /// The full connection string data source path
        /// </summary>
        private string mConnectionString;

        /// <summary>
        /// Single instance of the SqlCeConnection object
        /// </summary>
        private SqlCeConnection mCeConnection = null;

        /// <summary>
        /// Default ResultSet options
        /// </summary>
        private ResultSetOptions mResultSetOptions;

        /// <summary>
        /// Track whether Dispose has been called.
        /// </summary>
        private bool mDisposed = false;

        #endregion


        /// <summary>
        /// Base Constructor (uses default DataSource)
        /// </summary>
        public SqlConnectionObject()
           : this(DEFAULT_DATASOURCE)
        {
        }/* end constructor */


        /// <summary>
        /// Overload Constructor 
        /// </summary>
        /// <param name="iDataSource">DataSource name including extension</param>
        public SqlConnectionObject(string iDataSource)
        {
            // construct the ResultSet options
            BuildResultSetOptions();

            //
            // To enable design time support, the resultSetConnectionString 
            // property needs to switched between a Designtime and Runtime 
            // connection string. The design time connection string is used 
            // to make a valid connection to the database and retrieve schema 
            // information during desktop running and debug
            // 
            /*if (IsDesignTime())
            {
                mConnectionString = GetDesignTimePath(iDataSource);
            }
            else
            {
                mConnectionString = GetRunTimePath(iDataSource);
            }*/

            mConnectionString = GetRunTimePath(iDataSource);

            Console.WriteLine("Connection string:" + mConnectionString);

            // connect to datasource
            BuildConnection();

        }/* end constructor */


        /// <summary>
        /// Close and dispose of the CE Connection object
        /// </summary>
        public void CloseCeConnection()
        {
            if (mCeConnection != null)
            {
                try
                {
                    if (Connected())
                    {
                        mCeConnection.Close();
                    }
                }
                finally
                {
                    mCeConnection.Dispose();
                    mCeConnection = null;
                }
            }

        }/* end disposal */


        /// <summary>
        /// Implement IDisposable.
        /// Note: A derived class must not be able to override this method!
        /// </summary>
        public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            //
            // tell the Garbage Collector that the Finalize process no 
            // longer needs to be run for this object.
            //
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios.
        /// If disposing equals true, the method has been called directly
        /// or indirectly by a user's code. Managed and unmanaged resources
        /// can be disposed.
        /// If disposing equals false, the method has been called by the
        /// runtime from inside the finalizer and you should not reference
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="iDisposeManagedResources"></param>
        private void Dispose(bool iDisposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (iDisposing)
                {
                    CloseCeConnection();
                }
                // Note disposing has been done.
                mDisposed = true;
            }
        }


        /// <summary>
        /// This destructor will run only if the Dispose method
        /// does not get called.
        /// It gives your base class the opportunity to finalize.
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~SqlConnectionObject()
        {
            Dispose(false);
        }


        /// <summary>
        /// Get the current connection
        /// </summary>
        public SqlCeConnection DirectCeConnection
        {
            get
            {
                return mCeConnection;
            }
            set
            {
                if (value == null)
                {
                    
                }
                else
                {

                }
                mCeConnection = value;
            }
        }


        /// <summary>
        /// Get or set the ResultSetOptions
        /// </summary>
        public ResultSetOptions ResultSetOptions
        {
            get
            {
                return mResultSetOptions;
            }
            set
            {
                mResultSetOptions = value;
            }
        }


        #region Private Methods

        /// <summary>
        /// Get the current "Run Time" path. This method will only be 
        /// invoked when the client application is run on Windows Mobile 
        /// or Pocket PC Device
        /// </summary>
        /// <param name="iDataSource">Name of the SQL Compact DataSource</param>
        /// <returns>Full Data Source path</returns>
        private string GetRunTimePath(string iDataSource)
        {
            //
            // is there already a path specified? If yes then 
            // confirm it is valid and just return
            //
            if (iDataSource.Contains("\\"))
            {
                if (File.Exists(iDataSource))
                {
                    return CONNECTION_STRING + " = " + iDataSource;
                }
            }

            // otherwise use the default path
            string connectionString =
                   Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            return CONNECTION_STRING + " = " + @connectionString + "\\" + iDataSource + ";";

        }/* end method */


        /// <summary>
        /// Get the current "Design Time" path. This method will only be 
        /// invoked when the client application is run on desktop PC (hence
        /// the use of literal strings)
        /// </summary>
        /// <param name="iDataSource">Name of the SQL Compact DataSource</param>
        /// <returns>Full Data Source path</returns>
        private string GetDesignTimePath(string iDataSource)
        {
            string connectionString =
                   Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connectionString = connectionString.Remove(0, 6);
            int startIndex = connectionString.IndexOf(@"\MobileTcpClient\bin\Debug");

            if (startIndex > 0)
            {
                connectionString = connectionString.Remove(startIndex, connectionString.Length - startIndex);

            }
            return "Data Source = " + connectionString + "\\MicrologInspectorDB\\" + iDataSource;

        }/* end method */


        /// <summary>
        /// Set the default ResultSet options
        /// </summary>
        private void BuildResultSetOptions()
        {
            mResultSetOptions = System.Data.SqlServerCe.ResultSetOptions.Scrollable;
            mResultSetOptions = (mResultSetOptions | System.Data.SqlServerCe.ResultSetOptions.Sensitive);
            mResultSetOptions = (mResultSetOptions | System.Data.SqlServerCe.ResultSetOptions.Updatable);

        }/* end method */


        /// <summary>
        /// Create new DataSource connection object and initiate
        /// the connection. In the event of errors return the 
        /// connection object to null.
        /// </summary>
        private void BuildConnection()
        {
            //
            // if the current connection is null, then create a 
            // new connection and open it
            //
            try
            {
                if (mCeConnection == null)
                {
                    try
                    {
                        mCeConnection = new SqlCeConnection(mConnectionString);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Failed SqlCeConnection: " + ex.ToString());
                    }
                }
            }
            catch
            {
                mCeConnection = null;
            }

        }/* end method */


        /// <summary>
        /// Determine if this instance is running against the 
        /// .NET Framework by using the MSCoreLib PublicKeyToken
        /// </summary>
        /// <returns>true if design time</returns>
        private bool IsDesignTime()
        {
            System.Reflection.Assembly mscorlibAssembly = typeof(int).Assembly;
            if ((mscorlibAssembly != null))
            {
                if (mscorlibAssembly.FullName.ToUpper().EndsWith("B77A5C561934E089"))
                {
                    return true;
                }
            }
            return false;

        }/* end method */


        /// <summary>
        /// Determine if this instance is running against the .Net
        /// Compact Framework by using the MSCoreLib PublicKeyToken
        /// </summary>
        /// <returns></returns>
        private bool IsRunTime()
        {
            System.Reflection.Assembly mscorlibAssembly = typeof(int).Assembly;
            if ((mscorlibAssembly != null))
            {
                if (mscorlibAssembly.FullName.ToUpper().EndsWith("969DB8053D3322AC"))
                {
                    return true;
                }
            }
            return false;

        }/* end method */

        #endregion


        #region Public Methods

        /// <summary>
        /// Check the current DataSource connection state
        /// </summary>
        /// <returns>true if DataSource is valid and connected</returns>
        public Boolean Connected()
        {
            if ((mCeConnection != null) && (mCeConnection.State == ConnectionState.Open))
            {
                return true;
            }
            else
            {
                return false;
            }
        }/* end method */


        /// <summary>
        /// Disconnect the current DataSource
        /// </summary>
        public void Disconnect()
        {
            if (mCeConnection != null)
            {
                if (Connected())
                {
                    mCeConnection.Close();
                }
            }

        }/* end method */

        #endregion

    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------