﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the FFTCHANNEL Table ResultSet
   /// </summary>
   public class ResultSetFFTChannelTable : ResultSetBase
   {
      #region Private Fields and Constants

      private const int MAX_LINES = 4096;

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsFFTChannel mColumnOrdinal = null;

      /// <summary>
      /// Query FFTCHANNEL table based on the external FFTPointUid field (will return one point)
      /// </summary>
      private const string QUERY_BY_FFTPOINT_UID = "SELECT * FROM FFTCHANNEL " +
          "WHERE FFTPointUid = @FFTPointUid";

      /// <summary>
      /// Query FFTCHANNEL table based on the external PointUid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM FFTCHANNEL " +
          "WHERE PointUid = @PointUid";


      /// <summary>
      /// Query FFTCHANNEL table based on the external PointUid field 
      /// </summary>
      private const string QUERY_ENVDATA_DESC_BY_UID = "SELECT * FROM FFTCHANNEL " +
          "WHERE PointUid = @Uid AND TimeStamp = @TimeStamp " +
          "ORDER BY TimeStamp DESC";


      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_FFTCHANNELS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current FFTCHANNEL table. 
      /// </summary>
      /// <param name="iFFTChannelObject">populated FFTChannelRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( FFTChannelRecord iFFTChannelObject )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "FFTCHANNEL";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write to table
               result = WriteRecord( iFFTChannelObject, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all FFTCHANNEL records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumFFTChannelRecords GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumFFTChannelRecords channelRecords = new EnumFFTChannelRecords();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "FFTCHANNEL";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  FFTChannelRecord fftChannelRecord = new FFTChannelRecord();
                  PopulateObject( ceResultSet, ref fftChannelRecord );
                  channelRecords.AddFFTChannelRecord( fftChannelRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return channelRecords;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of FFTChannel objects to the FFTCHANNEL table. 
      /// </summary>
      /// <param name="iEnumFFTChannels">Enumerated List of FFTChannel objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumFFTChannelRecords iEnumFFTChannels )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "FFTCHANNEL";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {

               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( FFTChannelRecord channel in iEnumFFTChannels )
               {
                  if ( !WriteRecord( channel, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Returns a single FFTChannel Record object from the FFTCHANNEL table 
      /// using the external FFTPointUid as the lookup reference. 
      /// </summary>
      /// <param name="iFFTPointUid">Point Unique identifier</param>
      /// <returns>Populated FFT Channel object or null on error</returns>
      public FFTChannelRecord GetRecordFromFFTPointUid( Guid iFFTPointUid )
      {
         OpenCeConnection();

         // reset the default profile name
         FFTChannelRecord fftChannelRecord = new FFTChannelRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_FFTPOINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@FFTPointUid", SqlDbType.UniqueIdentifier ).Value = iFFTPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref fftChannelRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return fftChannelRecord;

      }/* end method */


      /// <summary>
      /// Returns a single FFTChannel Record object from the FFTCHANNEL table 
      /// using the external PointUid as the lookup reference. 
      /// </summary>
      /// <param name="iPointUid">Point Unique identifier</param>
      /// <returns>Populated FFT Channel object or null on error</returns>
      public FFTChannelRecord GetRecordFromPointUid( Guid iPointUid )
      {
         OpenCeConnection();

         // reset the default profile name
         FFTChannelRecord fftChannelRecord = new FFTChannelRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref fftChannelRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return fftChannelRecord;

      }/* end method */


      /// <summary>
      /// Return an enumerated list of FFT Channel Records
      /// </summary>
      /// <param name="iPointUid">Point UID to Reference</param>
      /// <returns>Enumerated list of FFT channels</returns>
      public EnumFFTChannelRecords GetRecordsFromPointUid( Guid iPointUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumFFTChannelRecords enumFFTChannels = new EnumFFTChannelRecords();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  FFTChannelRecord fftChannel = new FFTChannelRecord();

                  PopulateObject( ceResultSet, ref fftChannel );

                  // add to the enumerated list
                  if ( fftChannel != null )
                     enumFFTChannels.AddFFTChannelRecord( fftChannel );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumFFTChannels;

      }/*end method */


      /// <summary>
      /// Return an enumerated list of FFT Channel Records
      /// </summary>
      /// <param name="iPointUid">Point UID to Reference</param>
      /// <returns>Enumerated list of FFT channels</returns>
      public EnumBaseFFTChannels GetBaseRecordsFromPointUid( Guid iPointUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumBaseFFTChannels enumFFTChannels = new EnumBaseFFTChannels();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the FFTCHANNEL table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // create a new 'reference' FFT Channel object
               FFTChannelRecord fftChannel = new FFTChannelRecord();

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // populate the reference object
                  PopulateObject( ceResultSet, ref fftChannel );

                  //
                  // create a new base object and populate its fields
                  // with data from the reference object
                  //
                  FFTChannelBase channelBase = new FFTChannelBase();
                  channelBase.PopulateBaseFields( fftChannel );

                  // add the base object to the enumerated list
                  if ( fftChannel != null )
                     enumFFTChannels.AddFFTChannel( channelBase );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumFFTChannels;

      }/*end method */


      /// <summary>
      /// Deletes that last record from ENVDATA table if it exists
      /// Fix for Defect #868
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <returns>true if record is deleted without errors</returns>
      public Boolean DeleteLastRecord( PointRecord iRecord, DateTime iDeleteAt )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = QUERY_ENVDATA_DESC_BY_UID;

               // reference the PointUid field
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iRecord.Uid;
               command.Parameters.Add( "@TimeStamp", SqlDbType.DateTime ).Value = iDeleteAt;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  //
                  // delete all records returned by our query
                  //
                  while ( ceResultSet.Read() )
                  {
                     ceResultSet.Delete();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// FFTChannel table. This table will need to be revised in the event of any 
      /// changes being made to the FFTChannel table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsFFTChannel( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new FFT Channel record and add it 
      /// to the current FFTCHANNEL table.
      /// </summary>
      /// <param name="iFFTChannel">Populated FFT Channel Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( FFTChannelRecord iFFTChannel,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iFFTChannel.Uid;
            newRecord[ mColumnOrdinal.FFTPointUid ] = iFFTChannel.FFTPointUid;
            newRecord[ mColumnOrdinal.PointUid ] = iFFTChannel.PointUid;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iFFTChannel.PointHostIndex;
            newRecord[ mColumnOrdinal.ChannelId ] = iFFTChannel.ChannelId;
            newRecord[ mColumnOrdinal.SpecNum ] = iFFTChannel.SpecNum;

            if ( iFFTChannel.Timestamp.Ticks > 0 )
               newRecord[ mColumnOrdinal.Timestamp ] = iFFTChannel.Timestamp;

            newRecord[ mColumnOrdinal.NumberOfLines ] = iFFTChannel.NumberOfLines;
            newRecord[ mColumnOrdinal.DataType ] = iFFTChannel.DataType;
            newRecord[ mColumnOrdinal.NumberOfAverages ] = iFFTChannel.NumberOfAverages;
            newRecord[ mColumnOrdinal.Windowing ] = iFFTChannel.Windowing;
            newRecord[ mColumnOrdinal.SpeedOrPPF ] = iFFTChannel.SpeedOrPPF;
            newRecord[ mColumnOrdinal.Factor ] = iFFTChannel.Factor;
            newRecord[ mColumnOrdinal.Offset ] = iFFTChannel.Offset;
            newRecord[ mColumnOrdinal.HiFreq ] = iFFTChannel.HiFreq;
            newRecord[ mColumnOrdinal.LowFreq ] = iFFTChannel.LowFreq;
            newRecord[ mColumnOrdinal.Load ] = iFFTChannel.Load;
            newRecord[ mColumnOrdinal.CompressionType ] = iFFTChannel.CompressionType;
            newRecord[ mColumnOrdinal.UserIndex ] = iFFTChannel.UserIndex;
            newRecord[ mColumnOrdinal.Units ] = iFFTChannel.Units;
            newRecord[ mColumnOrdinal.IsOverloaded ] = iFFTChannel.IsOverloaded;
            newRecord[ mColumnOrdinal.DataLines ] = iFFTChannel.DataLines;
            newRecord[ mColumnOrdinal.SerialNo ] = iFFTChannel.SerialNo;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single FFTCHANNEL table record
      /// </summary>
      /// <param name="iFFTChannelRecord">Updated FFT Channel Record object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( FFTChannelRecord iFFTChannelRecord,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetGuid( mColumnOrdinal.Uid, iFFTChannelRecord.Uid );
            ceResultSet.SetGuid( mColumnOrdinal.FFTPointUid, iFFTChannelRecord.FFTPointUid );
            ceResultSet.SetGuid( mColumnOrdinal.PointUid, iFFTChannelRecord.PointUid );
            ceResultSet.SetInt32( mColumnOrdinal.PointHostIndex, iFFTChannelRecord.PointHostIndex );
            ceResultSet.SetByte( mColumnOrdinal.ChannelId, iFFTChannelRecord.ChannelId );
            ceResultSet.SetByte( mColumnOrdinal.SpecNum, iFFTChannelRecord.SpecNum );

            if ( iFFTChannelRecord.Timestamp.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.Timestamp, iFFTChannelRecord.Timestamp );

            ceResultSet.SetInt32( mColumnOrdinal.NumberOfLines, iFFTChannelRecord.NumberOfLines );
            ceResultSet.SetByte( mColumnOrdinal.DataType, iFFTChannelRecord.DataType );
            ceResultSet.SetInt32( mColumnOrdinal.NumberOfAverages, iFFTChannelRecord.NumberOfAverages );
            ceResultSet.SetByte( mColumnOrdinal.Windowing, iFFTChannelRecord.Windowing );
            ceResultSet.SetDouble( mColumnOrdinal.SpeedOrPPF, iFFTChannelRecord.SpeedOrPPF );
            ceResultSet.SetDouble( mColumnOrdinal.Factor, iFFTChannelRecord.Factor );
            ceResultSet.SetDouble( mColumnOrdinal.Offset, iFFTChannelRecord.Offset );
            ceResultSet.SetDouble( mColumnOrdinal.HiFreq, iFFTChannelRecord.HiFreq );
            ceResultSet.SetDouble( mColumnOrdinal.LowFreq, iFFTChannelRecord.LowFreq );
            ceResultSet.SetInt32( mColumnOrdinal.Load, iFFTChannelRecord.Load );
            ceResultSet.SetByte( mColumnOrdinal.CompressionType, iFFTChannelRecord.CompressionType );
            ceResultSet.SetInt32( mColumnOrdinal.UserIndex, iFFTChannelRecord.UserIndex );
            ceResultSet.SetInt32( mColumnOrdinal.Units, iFFTChannelRecord.Units );
            ceResultSet.SetBoolean( mColumnOrdinal.IsOverloaded, iFFTChannelRecord.IsOverloaded );
            ceResultSet.SetBytes( mColumnOrdinal.DataLines, 0, iFFTChannelRecord.DataLines, 0, MAX_LINES );
            ceResultSet.SetString( mColumnOrdinal.SerialNo, iFFTChannelRecord.SerialNo );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated FFT Channel Object with data from a single 
      /// FFTCHANNEL table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iFFTChannelRecord">FFT Point object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref FFTChannelRecord iFFTChannelRecord )
      {
         try
         {
            iFFTChannelRecord.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );
            iFFTChannelRecord.FFTPointUid = ceResultSet.GetGuid( mColumnOrdinal.FFTPointUid );
            iFFTChannelRecord.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iFFTChannelRecord.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            iFFTChannelRecord.ChannelId = ceResultSet.GetByte( mColumnOrdinal.ChannelId );
            iFFTChannelRecord.SpecNum = ceResultSet.GetByte( mColumnOrdinal.SpecNum );

            if ( ceResultSet[ mColumnOrdinal.Timestamp ] != DBNull.Value )
               iFFTChannelRecord.Timestamp = ceResultSet.GetDateTime( mColumnOrdinal.Timestamp );

            iFFTChannelRecord.NumberOfLines = ceResultSet.GetInt32( mColumnOrdinal.NumberOfLines );
            iFFTChannelRecord.DataType = ceResultSet.GetByte( mColumnOrdinal.DataType );
            iFFTChannelRecord.NumberOfAverages = ceResultSet.GetInt32( mColumnOrdinal.NumberOfAverages );
            iFFTChannelRecord.Windowing = ceResultSet.GetByte( mColumnOrdinal.Windowing );
            iFFTChannelRecord.SpeedOrPPF = ceResultSet.GetDouble( mColumnOrdinal.SpeedOrPPF );
            iFFTChannelRecord.Factor = ceResultSet.GetDouble( mColumnOrdinal.Factor );
            iFFTChannelRecord.Offset = ceResultSet.GetDouble( mColumnOrdinal.Offset );
            iFFTChannelRecord.HiFreq = ceResultSet.GetDouble( mColumnOrdinal.HiFreq );
            iFFTChannelRecord.LowFreq = ceResultSet.GetDouble( mColumnOrdinal.LowFreq );
            iFFTChannelRecord.Load = ceResultSet.GetInt32( mColumnOrdinal.Load );
            iFFTChannelRecord.CompressionType = ceResultSet.GetByte( mColumnOrdinal.CompressionType );
            iFFTChannelRecord.UserIndex = ceResultSet.GetInt32( mColumnOrdinal.UserIndex );
            iFFTChannelRecord.Units = ceResultSet.GetInt32( mColumnOrdinal.Units );
            iFFTChannelRecord.IsOverloaded = ceResultSet.GetBoolean( mColumnOrdinal.IsOverloaded );

            if ( ceResultSet[ mColumnOrdinal.DataLines ] != DBNull.Value )
            {
               // we have to manually specify the read buffer size here (based on the number of lines)
               iFFTChannelRecord.DataLines = new byte[ iFFTChannelRecord.NumberOfLines * 2 ];
               // read the FFT Channel DataLine ByteArray
               ceResultSet.GetBytes( mColumnOrdinal.DataLines, 0, iFFTChannelRecord.DataLines, 0, MAX_LINES );
            }

            if ( ceResultSet[ mColumnOrdinal.SerialNo ] != DBNull.Value )
               iFFTChannelRecord.SerialNo = ceResultSet.GetString( mColumnOrdinal.SerialNo );
         }
         catch
         {
            iFFTChannelRecord = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 3rd May 2010
//  Add DeleteLastRecord method
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------