﻿//------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Data.SqlServerCe;
using System.IO;
using System.Xml;
using System.Globalization;


namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Creates a backup of the Microlog Inspector Database and also restores
   /// from that same backup. Note: This should be used with EXTREME caution!
   /// </summary>
   public class BackupAndRestore
   {
      //------------------------------------------------------------------------

      /// <summary>
      /// Total number of table progress counts to return
      /// </summary>
      private const int TABLE_PROGRESS_COUNT = 30;

      /// <summary>
      /// Default level to restore users to
      /// </summary>
      private const OperatorRights DEFAULT_RIGHTS = OperatorRights.Review;

      /// <summary>
      /// reference to Table ResultSet object
      /// </summary>
      private ITableConnect mTable = null;

      /// <summary>
      /// reference to Progress update object
      /// </summary>
      private DataProgress mProgressPosition = null;

      /// <summary>
      /// 
      /// </summary>
      private int mArchiveVersion = 0;

      /// <summary>
      /// 
      /// </summary>
      private DateTime mArchiveTimeStamp = DateTime.MinValue;

      /// <summary>
      /// Archive file name and path
      /// </summary>
      private string mArchiveFile = string.Empty;

      //------------------------------------------------------------------------

      ///<summary>
      ///Gets or sets the progress position object
      ///</summary>
      public DataProgress ProgressPosition
      {
         get
         {
            return mProgressPosition;
         }
         set
         {
            mProgressPosition = value;
         }
      }


      /// <summary>
      /// Gets or sets the backup file and path
      /// </summary>
      public string BackupPath
      {
         get
         {
            return mArchiveFile;
         }
         set
         {
            mArchiveFile = @value;
         }
      }


      /// <summary>
      /// Gets or sets the current archive version
      /// </summary>
      public int Version
      {
         get
         {
            return mArchiveVersion;
         }
         set
         {
            mArchiveVersion = value;
         }
      }


      /// <summary>
      /// Gets or sets the current archive timestamp
      /// </summary>
      public DateTime TimeStamp
      {
         get
         {
            return mArchiveTimeStamp;
         }
         set
         {
            mArchiveTimeStamp = value;
         }
      }

      //------------------------------------------------------------------------

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iTable"></param>
      public BackupAndRestore( ITableConnect iTable )
      {
         mTable = iTable;

         // instantiate progress position object
         mProgressPosition = new DataProgress();

         // reset the progress manager
         mProgressPosition.Reset();

         // reset the archive file properties
         this.TimeStamp = DateTime.MinValue;
         this.Version = 0;
      }

      //------------------------------------------------------------------------

      #region Public Methods

      /// <summary>
      /// Backup all tables
      /// </summary>
      /// <returns></returns>
      public Boolean Backup_All_Tables()
      {
         // set the initial state
         Boolean result = true;

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = TABLE_PROGRESS_COUNT;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // create a new archive management object
         PacketArchive archive = new PacketArchive( mTable.CurrentDbVersion );

         // Add NODE table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Node.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add POINTS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Points.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add PROCESS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Process.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add TEXTDATA table to the archive
         if ( archive.Add_Table_To_Archive( mTable.TextData.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add INSPECTIONS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Inspection.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add MCC table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Mcc.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add FFTCHANNEL table to the archive
         if ( archive.Add_Table_To_Archive( mTable.FFTChannel.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add FFTPOINT table to the archive
         if ( archive.Add_Table_To_Archive( mTable.FFTPoint.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add C_NOTE table to the archive
         if ( archive.Add_Table_To_Archive( mTable.CodedNote.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add TEMPDATA table to the archive
         if ( archive.Add_Table_To_Archive( mTable.TempData.GetAllRecords(),
            ArchiveTable.TEMPDATA ) )
         {
            ProgressPosition.Update( true );
         }

         // Add VELDATA table to the archive
         if ( archive.Add_Table_To_Archive( mTable.VelData.GetAllRecords(),
            ArchiveTable.VELDATA ) )
         {
            ProgressPosition.Update( true );
         }

         // Add ENVDATA table to the archive
         if ( archive.Add_Table_To_Archive( mTable.EnvData.GetAllRecords(),
            ArchiveTable.ENVDATA ) )
         {
            ProgressPosition.Update( true );
         }

         // Add OPERATORS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Operators.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add MESSAGEPREFERENCES table to the archive
         if ( archive.Add_Table_To_Archive( mTable.MessagePreferences.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add MACHINENOPSKIPS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.MachineNOpSkips.GetAllRecords(),
            ArchiveTable.MACHINENOPSKIPS ) )
         {
            ProgressPosition.Update( true );
         }

         // Add MACHINEOKSKIPS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.MachineOkSkips.GetAllRecords(),
            ArchiveTable.MACHINEOKSKIPS ) )
         {
            ProgressPosition.Update( true );
         }

         // Add USERPREFERENCES table to the archive
         if ( archive.Add_Table_To_Archive( mTable.UserPreferences.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add CORRECTIVEACTION table to the archive
         if ( archive.Add_Table_To_Archive( mTable.CorrectiveAction.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add WORKTYPE table to the archive
         if ( archive.Add_Table_To_Archive( mTable.WorkType.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add PRIORITY table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Priority.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add PROBLEM table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Problem.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add WORKNOTIFICATION table to the archive
         if ( archive.Add_Table_To_Archive( mTable.WorkNotification.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add NOTES table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Notes.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add C_NOTES table to the archive
         if ( archive.Add_Table_To_Archive( mTable.SavedCodedNotes.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Archice the MESSAGE table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Message.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add INSTRUCTIONS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.Instructions.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add CONDITIONALPOINT table to the archive
         if ( archive.Add_Table_To_Archive( mTable.ConditionalPoint.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add SAVEDMEDIA table to the archive
         if ( archive.Add_Table_To_Archive( mTable.SavedMedia.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add DERIVEDITEMS table to the archive
         if ( archive.Add_Table_To_Archive( mTable.DerivedItems.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add DERIVEDPOINT table to the archive
         if ( archive.Add_Table_To_Archive( mTable.DerivedPoint.GetAllRecords() ) )
         {
            ProgressPosition.Update( true );
         }

         // Add DEVICEPROFILE table to the archive
         if ( archive.Add_Table_To_Archive( mTable.DeviceProfile.GetCurrentProfile() ) )
         {
            ProgressPosition.Update( true );
         }

         // lastly set the Archive date and time
         archive.ArchiveDate = DateTime.Now;

         // save this archive
         archive.Save( this.BackupPath, archive );

         // confirm this archive was actually created
         if ( !File.Exists( this.BackupPath ) )
         {
            result = false;
         }

         // and return the result
         return result;
      }


      /// <summary>
      /// Restore Database from backup
      /// </summary>
      /// <returns></returns>
      public Boolean Restore_All_Tables( )
      {
         // confirm this archive was actually created
         if ( !File.Exists( mArchiveFile ) )
         {
            return false;
         }
         else
         {
            // ok, so we know the file exists ..
            if ( Unzip_Archive() )
            {
               Restore();
               return true;
            }
            else
            {
               return false;
            }
         }
      }

      #endregion

      //------------------------------------------------------------------------

      #region Restore all existing Tables

      /// <summary>
      /// Restore all archived tables
      /// </summary>
      private void Restore()
      {
         // create a new desrialization engine
         XmlDeserializer deserializer = new XmlDeserializer();

         // how many tables are we going to clear here?
         ProgressPosition.NoOfItemsToProcess = TABLE_PROGRESS_COUNT;

         // ensure this is set to full sweep
         ProgressPosition.DisplayAt = DisplayPosition.FullSweep;

         // reset the progress position counter
         ProgressPosition.Reset();

         // restore all tables from the current archive
         Restore_DEVICEPROFILE_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_NODE_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_POINTS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_PROCESS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_INSPECTION_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_MCC_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_CNOTE_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_TEMPDATA_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_TEXTDATA_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_VELDATA_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_ENVDATA_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_FFTPOINT_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_FFTCHANNEL_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_NOTES_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_CNOTES_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_MESSAGE_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_CONDITIONALPOINT_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_INSTRUCTIONS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_OPERATORS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_MESSAGINGPREFS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_MACHINENOPSKIPS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_MACHINENOKSKIPS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_USERPREFERENCES_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_WORKNOTIFICATION_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_PROBLEM_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_PRIORITY_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_WORKTYPE_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_CORRECTIVEACTION_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_DERIVEDITEMS_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_DERIVEDPOINT_Table( deserializer );
         ProgressPosition.Update( true );

         Restore_SAVEDMEDIA_Table( deserializer );
         ProgressPosition.Update( true );
      }


      /// <summary>
      /// Clear and then update the contents of the DEVICEPROFILE table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_DEVICEPROFILE_Table( XmlDeserializer iDeserializer )
      {
         // clear the current details
         mTable.DeviceProfile.ClearTable();

         // deserialize the table
         DeviceProfile profile = iDeserializer.DeserializeDeviceProfile( 0 );

         // populate the table
         if ( profile != null )
         {
            mTable.DeviceProfile.AddDeviceProfile( profile );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the NODE table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_NODE_Table( XmlDeserializer iDeserializer )
      {
         // clear the current details
         mTable.Node.ClearTable();

         // deserialize the table
         EnumNodes nodes = iDeserializer.DeserializeNodeList( 0, false );

         // populate the table
         if ( nodes != null && nodes.Count > 0 )
         {
            mTable.Node.AddEnumRecords( nodes );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the POINTS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_POINTS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Points.ClearTable();

         // deserialize the table
         EnumPoints points = iDeserializer.DeserializePointsList( 0, false );

         // populate the table 
         if ( points != null && points.Count > 0 )
         {
            mTable.Points.AddEnumRecords( points );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the PROCESS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_PROCESS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Process.ClearTable();

         // deserialize the table
         EnumProcess process = iDeserializer.DeserializeProcessList( 0 );

         // populate the table 
         if ( process != null && process.Count > 0 )
         {
            mTable.Process.AddEnumRecords( process );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the TEXTDATA table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_TEXTDATA_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.TextData.ClearTable();

         // deserialize the table
         EnumTextData textData = iDeserializer.DeserializeTextDataList( 0 );

         // populate the table 
         if ( textData != null && textData.Count > 0 )
         {
            mTable.TextData.AddEnumRecords( textData );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the INSPECTION table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_INSPECTION_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Inspection.ClearTable();

         // deserialize the table
         EnumInspection inspections = iDeserializer.DeserializeInspectionList( 0 );

         // populate the table 
         if ( inspections != null && inspections.Count > 0 )
         {
            mTable.Inspection.AddEnumRecords( inspections );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the MCC table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_MCC_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Mcc.ClearTable();

         // deserialize the table
         EnumMCC mcc = iDeserializer.DeserializeMCCList( 0 );

         // populate the table 
         if ( mcc != null && mcc.Count > 0 )
         {
            mTable.Mcc.AddEnumRecords( mcc );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the MESSAGE table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_MESSAGE_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Message.ClearTable();

         // deserialize the table
         EnumMessages messages = iDeserializer.DeserializeMessages( 0 );

         // populate the table 
         if ( messages != null && messages.Count > 0 )
         {
            mTable.Message.AddEnumRecords( messages );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the CNOTE table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_CNOTE_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.CodedNote.ClearTable();

         // deserialize the table
         EnumCodedNotes noteCodes = iDeserializer.DeserializeCodedNotes( 0 );

         // populate the table 
         if ( noteCodes != null && noteCodes.Count > 0 )
         {
            mTable.CodedNote.AddEnumRecords( noteCodes );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the CNOTES table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_CNOTES_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.SavedCodedNotes.ClearTable();

         // deserialize the table
         EnumSelectedCodedNotes noteCodes = iDeserializer.DeserializeSelectedCodedNotes( 0 );

         // populate the table 
         if ( noteCodes != null && noteCodes.Count > 0 )
         {
            mTable.SavedCodedNotes.AddEnumRecords( noteCodes );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the CONDITIONALPOINT table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_CONDITIONALPOINT_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.ConditionalPoint.ClearTable();

         // deserialize the table
         EnumConditionalPoints conditionals = iDeserializer.DeserializeConditionalPoints( 0 );

         // populate the table 
         if ( conditionals != null && conditionals.Count > 0 )
         {
            mTable.ConditionalPoint.AddEnumRecords( conditionals );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the NOTES table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_NOTES_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Notes.ClearTable();

         // deserialize the table
         EnumNotes noteCodes = iDeserializer.DeserializeNotes( 0 );

         // populate the table 
         if ( noteCodes != null && noteCodes.Count > 0 )
         {
            mTable.Notes.AddEnumRecords( noteCodes );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the TEMPDATA table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_TEMPDATA_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.TempData.ClearTable();

         // deserialize the table
         EnumSavedMeasurements measurements = 
            iDeserializer.DeserializeSavedMeasurements( 0, TableName.TEMPDATA );

         // populate the table 
         if ( measurements != null && measurements.Count > 0 )
         {
            mTable.TempData.AddEnumRecords( measurements );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the VELDATA table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_VELDATA_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.VelData.ClearTable();

         // deserialize the table
         EnumSavedMeasurements measurements = 
            iDeserializer.DeserializeSavedMeasurements( 0, TableName.VELDATA );

         // populate the table 
         if ( measurements != null && measurements.Count > 0 )
         {
            mTable.VelData.AddEnumRecords( measurements );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the ENVDATA table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_ENVDATA_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.EnvData.ClearTable();

         // deserialize the table
         EnumSavedMeasurements measurements = 
            iDeserializer.DeserializeSavedMeasurements( 0, TableName.ENVDATA );

         // populate the table 
         if ( measurements != null && measurements.Count > 0 )
         {
            mTable.EnvData.AddEnumRecords( measurements );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the FFTPOINT table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_FFTPOINT_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.FFTPoint.ClearTable();

         // deserialize the table
         EnumFFTPoints fftPoints = 
            iDeserializer.DeserializeFFTPointsList( 0 );

         // populate the table 
         if ( fftPoints != null && fftPoints.Count > 0 )
         {
            mTable.FFTPoint.AddEnumRecords( fftPoints );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the FFTCHANNEL table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_FFTCHANNEL_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.FFTChannel.ClearTable();

         // deserialize the table
         EnumFFTChannelRecords fftPoints = 
            iDeserializer.DeserializeFFTChannelList( 0 );

         // populate the table 
         if ( fftPoints != null && fftPoints.Count > 0 )
         {
            mTable.FFTChannel.AddEnumRecords( fftPoints );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the INSTRUCTIONS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_INSTRUCTIONS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Instructions.ClearTable();

         // deserialize the table
         EnumInstructions instructions = 
            iDeserializer.DeserializeInstructions( 0 );

         // populate the table 
         if ( instructions != null && instructions.Count > 0 )
         {
            mTable.Instructions.AddEnumRecords( instructions );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the OPERATORS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_OPERATORS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Operators.ClearTable();

         // deserialize the table
         EnumOperators operators = 
            iDeserializer.DeserializeOperators( 0 );

         // populate the table 
         if ( operators != null && operators.Count > 0 )
         {
            //
            // reset the operator rights for each and every
            // user to "Review" only and also ensure that any
            // pending password changes are deleted.
            //
            foreach ( OperatorRecord oper in operators )
            {
               oper.PasswordChanged = false;
               oper.CanChangePassword = false;
               oper.AccessLevel = DEFAULT_RIGHTS;
            }
            // reset the operators list
            operators.Reset();

            // write to the database
            mTable.Operators.AddEnumRecords( operators );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the MESSAGINGPREFS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_MESSAGINGPREFS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.MessagePreferences.ClearTable();

         // deserialize the table
         EnumMessagingPrefs prefs = 
            iDeserializer.DeserializeMessagingPrefs( 0 );

         // populate the table 
         if ( prefs != null && prefs.Count > 0 )
         {
            mTable.MessagePreferences.AddEnumRecords( prefs );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the MACHINENOPSKIPS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_MACHINENOPSKIPS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.MachineNOpSkips.ClearTable();

         // deserialize the table
         EnumMachineSkips skips = 
            iDeserializer.DeserializeMachineSkips( 0, TableName.MACHINENOPSKIPS );

         // populate the table 
         if ( skips != null && skips.Count > 0 )
         {
            mTable.MachineNOpSkips.AddEnumRecords( skips );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the MACHINEOKSKIPS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_MACHINENOKSKIPS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.MachineOkSkips.ClearTable();

         // deserialize the table
         EnumMachineSkips skips = 
            iDeserializer.DeserializeMachineSkips( 0, TableName.MACHINEOKSKIPS );

         // populate the table 
         if ( skips != null && skips.Count > 0 )
         {
            mTable.MachineOkSkips.AddEnumRecords( skips );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the USERPREFERENCES table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_USERPREFERENCES_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.UserPreferences.ClearTable();

         // deserialize the table
         EnumUserPreferences skips = 
            iDeserializer.DeserializeUserPreferences( 0 );

         // populate the table 
         if ( skips != null && skips.Count > 0 )
         {
            mTable.UserPreferences.AddEnumRecords( skips );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the WORKNOTIFICATION table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_WORKNOTIFICATION_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.WorkNotification.ClearTable();

         // deserialize the table
         EnumCmmsWorkNotifications notifications = 
            iDeserializer.DeserializeWorkNotifications( 0 );

         // populate the table 
         if ( notifications != null && notifications.Count > 0 )
         {
            mTable.WorkNotification.AddEnumRecords( notifications );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the PROBLEM table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_PROBLEM_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Problem.ClearTable();

         // deserialize the table
         EnumCmmsProblems problems = 
            iDeserializer.DeserializeProblems( 0 );

         // populate the table 
         if ( problems != null && problems.Count > 0 )
         {
            mTable.Problem.AddEnumRecords( problems );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the PRIORITY table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_PRIORITY_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.Priority.ClearTable();

         // deserialize the table
         EnumCmmsPriorities problems = 
            iDeserializer.DeserializePriorities( 0 );

         // populate the table 
         if ( problems != null && problems.Count > 0 )
         {
            mTable.Priority.AddEnumRecords( problems );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the WORKTYPE table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_WORKTYPE_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.WorkType.ClearTable();

         // deserialize the table
         EnumCmmsWorkTypes workTypes = 
            iDeserializer.DeserializeWorkTypes( 0 );

         // populate the table 
         if ( workTypes != null && workTypes.Count > 0 )
         {
            mTable.WorkType.AddEnumRecords( workTypes );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the CORRECTIVEACTION table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_CORRECTIVEACTION_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.CorrectiveAction.ClearTable();

         // deserialize the table
         EnumCmmsCorrectiveActions correctiveActions = 
            iDeserializer.DeserializeCorrectiveActions( 0 );

         // populate the table 
         if ( correctiveActions != null && correctiveActions.Count > 0 )
         {
            mTable.CorrectiveAction.AddEnumRecords( correctiveActions );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the DERIVEDITEMS table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_DERIVEDITEMS_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.DerivedItems.ClearTable();

         // deserialize the table
         EnumDerivedItems derivedItems = 
            iDeserializer.DeserializeDerivedItems( 0 );

         // populate the table 
         if ( derivedItems != null && derivedItems.Count > 0 )
         {
            mTable.DerivedItems.AddEnumRecords( derivedItems );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the DERIVEDPOINT table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_DERIVEDPOINT_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.DerivedPoint.ClearTable();

         // deserialize the table
         EnumDerivedPoints derivedPoints = 
            iDeserializer.DeserializeDerivedPoints( 0 );

         // populate the table 
         if ( derivedPoints != null && derivedPoints.Count > 0 )
         {
            mTable.DerivedPoint.AddEnumRecords( derivedPoints );
         }
      }


      /// <summary>
      /// Clear and then update the contents of the SAVEDMEDIA table
      /// </summary>
      /// <param name="iDeserializer">Reference to Deserializer engine</param>
      private void Restore_SAVEDMEDIA_Table( XmlDeserializer iDeserializer )
      {
         // clear the current table
         mTable.SavedMedia.ClearTable();

         // deserialize the table
         EnumSavedMedia savedMedia = 
            iDeserializer.DeserializeSavedMedia( 0 );

         // populate the table 
         if ( savedMedia != null && savedMedia.Count > 0 )
         {
            mTable.SavedMedia.AddEnumRecords( savedMedia );
         }
      }

      #endregion

      //------------------------------------------------------------------------

      #region Unzip the Archive into the cache

      /// <summary>
      /// Manually deserialize the contents of the received NODE object
      /// and return feedback events on each record processed
      /// </summary>
      /// <param name="iTotalRecords">how many records does the file contain</param>
      /// <returns>enumerated list of nodes</returns>
      private Boolean Unzip_Archive()
      {
         Boolean isElement = false;
         string streamXml = string.Empty;
         StringBuilder xmlElement = new StringBuilder();

         // create an empty list of point objects
         PacketArchive archive = new PacketArchive();

         // open the points file for reading
         using ( StreamReader stream = new System.IO.StreamReader( this.BackupPath ) )
         {
            // reference the header
            string hdr1 = stream.ReadLine().Trim();
            string hdr2 = streamXml = stream.ReadLine().Trim();

            // read until we hit the end of file
            while ( !stream.EndOfStream )
            {
               // read a line from the file
               streamXml = stream.ReadLine().Trim();

               // extract the archive timestamp
               if ( streamXml.StartsWith( "<ArchiveDate>" ) )
               {
                  Extract_Archive_Timestamp( streamXml, hdr1, hdr2 );
               }

               // extract the database version from the archive
               if ( streamXml.StartsWith( "<ArchiveVersion>" ) )
               {
                  Extract_Archive_Version( streamXml, hdr1, hdr2 );
                  
                  // if this archive isn't valid then fail here!
                  if ( !Is_Version_Valid() )
                  {
                     return false;
                  }
               }

               // process against each known table type
               foreach ( ArchiveTable archiveTable in Get_Archive_Tables() )
               {
                  isElement = Build_Cache_File( isElement, streamXml,
                     xmlElement, hdr1, hdr2, archiveTable );
               }

               if ( isElement )
               {
                  xmlElement.Append( streamXml );
               }
               else
               {
                  xmlElement.Length = 0;
               }
            }

            // close the stream - we're done here!
            stream.Close();
         }

         //reset the StringBuilder object
         xmlElement.Length = 0;
         
         // return the result
         return true;

      }/* end method */


      /// <summary>
      /// Extract the Timestamp from the Archive file 
      /// </summary>
      /// <param name="iStreamXml">Text line from archive</param>
      /// <param name="iHdr1">Formatted XML Header line</param>
      /// <param name="iHdr2">Formatted XML Header line</param>
      private void Extract_Archive_Timestamp( string iStreamXml, string iHdr1, string iHdr2 )
      {
         StringBuilder xmlElement = new StringBuilder();

         // add the header elements
         xmlElement.Append( iHdr1 );
         xmlElement.Append( iHdr2 );

         // append the element text and terminate with ACK
         xmlElement.Append( iStreamXml );
         xmlElement.Append( "</ACK>" );

         // create an xml document object
         XmlDocument xmlRecord = new XmlDocument();

         // load the xml object into it
         xmlRecord.LoadXml( xmlElement.ToString() );

         // extract the data element
         try
         {
            this.TimeStamp = DateTime.Parse(
               xmlRecord.InnerText,
               CultureInfo.InvariantCulture );
         }
         catch
         {
            this.TimeStamp = DateTime.MinValue;
         }
      }


      /// <summary>
      /// Extract the Database version from the Archive file 
      /// </summary>
      /// <param name="iStreamXml">Text line from archive</param>
      /// <param name="iHdr1">Formatted XML Header line</param>
      /// <param name="iHdr2">Formatted XML Header line</param>
      private void Extract_Archive_Version( string iStreamXml, string iHdr1, string iHdr2 )
      {
         StringBuilder xmlElement = new StringBuilder();

         // add the header elements
         xmlElement.Append( iHdr1 );
         xmlElement.Append( iHdr2 );

         // append the element text and terminate with ACK
         xmlElement.Append( iStreamXml );
         xmlElement.Append( "</ACK>" );

         // create an xml document object
         XmlDocument xmlRecord = new XmlDocument();

         // load the xml object into it
         xmlRecord.LoadXml( xmlElement.ToString() );

         // now extract the data element
         try
         {
            this.Version = Convert.ToInt32( xmlRecord.InnerText );
         }
         catch
         {
            this.Version = 0;
         }
      }


      /// <summary>
      /// Is the current archive actually usable?
      /// </summary>
      /// <returns>true if YES, false if no</returns>
      private Boolean Is_Version_Valid()
      {
         // ensure we can get the current DB version as an integer 
         int currentDB;
         try
         {
            currentDB = Convert.ToInt32( mTable.CurrentDbVersion );
         }
         catch
         {
            return false;
         }

         //
         // provided the current database version exceeds, or is
         // equal to the Archive version, then we're good to go.
         // Otherwise just fail this method.
         //
         if ( currentDB >= this.Version )
         {
            return true;
         }
         else
         {
            return false;
         }
      }


      /// <summary>
      /// Builds and saved a single tables XML cache file
      /// </summary>
      /// <param name="iIsElement">Is this a braced element?</param>
      /// <param name="iStreamXml">Single line read from the file stream</param>
      /// <param name="iXmlElement"></param>
      /// <param name="iHdr1">Xml Header - line 1</param>
      /// <param name="iHdr2">Xml Header - line 2</param>
      /// <param name="iTable">Archive table to build</param>
      /// <returns>Is the current line a braced element</returns>
      private Boolean Build_Cache_File( Boolean iIsElement,
         string iStreamXml, StringBuilder iXmlElement,
         string iHdr1, string iHdr2, ArchiveTable iTable )
      {
         // is this the start of a Point element
         if ( iStreamXml == "<" + iTable.ToString() + ">" )
         {
            // add the header elements
            iXmlElement.Append( iHdr1 );
            iXmlElement.Append( iHdr2 );

            // flag as being an element
            iIsElement = true;
         }
         // .. or the end of the element
         else if ( iStreamXml == "</" + iTable.ToString() + ">" )
         {
            // flag as not an element
            iIsElement = false;

            // append the element text and terminate with ACK
            iXmlElement.Append( iStreamXml );
            iXmlElement.Append( "</ACK>" );

            // convert to a compressed packet
            PacketArchive archive = 
               PacketBase.DeserializeObject<PacketArchive>( iXmlElement.ToString() );

            // get the archive table from the general archive
            ArchiveRecord arc = Get_Archive_Record( iTable, archive );

            // decompress the archive table and save to a cache file
            arc.DecompressToXmlFile(
               arc.ByteData,
               PacketBase.XML_ELEMENT_UID,
               @iTable.ToString() + ".xml" );
         }
         return iIsElement;
      }


      /// <summary>
      /// Returns a list of all available tables in the current archive
      /// </summary>
      /// <returns></returns>
      private List<ArchiveTable> Get_Archive_Tables()
      {
         List<ArchiveTable>table = new List<ArchiveTable>();
         table.Add( ArchiveTable.C_NOTE );
         table.Add( ArchiveTable.C_NOTES );
         table.Add( ArchiveTable.CONDITIONALPOINT );
         table.Add( ArchiveTable.CORRECTIVEACTION );
         table.Add( ArchiveTable.DERIVEDITEMS );
         table.Add( ArchiveTable.DERIVEDPOINT );
         table.Add( ArchiveTable.DEVICEPROFILE );
         table.Add( ArchiveTable.ENVDATA );
         table.Add( ArchiveTable.FFTCHANNEL );
         table.Add( ArchiveTable.FFTPOINT );
         table.Add( ArchiveTable.INSPECTION );
         table.Add( ArchiveTable.INSTRUCTIONS );
         table.Add( ArchiveTable.LOG );
         table.Add( ArchiveTable.MACHINENOPSKIPS );
         table.Add( ArchiveTable.MACHINEOKSKIPS );
         table.Add( ArchiveTable.MCC );
         table.Add( ArchiveTable.MESSAGE );
         table.Add( ArchiveTable.MESSAGINGPREFS );
         table.Add( ArchiveTable.NODE );
         table.Add( ArchiveTable.NOTES );
         table.Add( ArchiveTable.OPERATORS );
         table.Add( ArchiveTable.POINTS );
         table.Add( ArchiveTable.PRIORITY );
         table.Add( ArchiveTable.PROBLEM );
         table.Add( ArchiveTable.PROCESS );
         table.Add( ArchiveTable.SAVEDMEDIA );
         table.Add( ArchiveTable.TEMPDATA );
         table.Add( ArchiveTable.TEXTDATA );
         table.Add( ArchiveTable.USERPREFERENCES );
         table.Add( ArchiveTable.VELDATA );
         table.Add( ArchiveTable.WORKNOTIFICATION );
         table.Add( ArchiveTable.WORKTYPE );
         return table;
      }


      /// <summary>
      /// Returns a single table object from the entire archive
      /// </summary>
      /// <param name="iTable">Table to return</param>
      /// <param name="iArchive">Current archive</param>
      /// <returns>Archived table object</returns>
      private ArchiveRecord Get_Archive_Record( ArchiveTable iTable, PacketArchive iArchive )
      {
         switch ( iTable )
         {
            case ArchiveTable.C_NOTE:
               return iArchive.C_NOTE;
            case ArchiveTable.C_NOTES:
               return iArchive.C_NOTES;
            case ArchiveTable.CONDITIONALPOINT:
               return iArchive.CONDITIONALPOINT;
            case ArchiveTable.CORRECTIVEACTION:
               return iArchive.CORRECTIVEACTION;
            case ArchiveTable.DERIVEDITEMS:
               return iArchive.DERIVEDITEMS;
            case ArchiveTable.DERIVEDPOINT:
               return iArchive.DERIVEDPOINT;
            case ArchiveTable.DEVICEPROFILE:
               return iArchive.DEVICEPROFILE;
            case ArchiveTable.ENVDATA:
               return iArchive.ENVDATA;
            case ArchiveTable.FFTCHANNEL:
               return iArchive.FFTCHANNEL;
            case ArchiveTable.FFTPOINT:
               return iArchive.FFTPOINT;
            case ArchiveTable.INSPECTION:
               return iArchive.INSPECTION;
            case ArchiveTable.INSTRUCTIONS:
               return iArchive.INSTRUCTIONS;
            case ArchiveTable.MACHINENOPSKIPS:
               return iArchive.MACHINENOPSKIPS;
            case ArchiveTable.MACHINEOKSKIPS:
               return iArchive.MACHINEOKSKIPS;
            case ArchiveTable.MCC:
               return iArchive.MCC;
            case ArchiveTable.MESSAGE:
               return iArchive.MESSAGE;
            case ArchiveTable.MESSAGINGPREFS:
               return iArchive.MESSAGINGPREFS;
            case ArchiveTable.NODE:
               return iArchive.NODE;
            case ArchiveTable.NOTES:
               return iArchive.NOTES;
            case ArchiveTable.OPERATORS:
               return iArchive.OPERATORS;
            case ArchiveTable.POINTS:
               return iArchive.POINTS;
            case ArchiveTable.PRIORITY:
               return iArchive.PRIORITY;
            case ArchiveTable.PROBLEM:
               return iArchive.PROBLEM;
            case ArchiveTable.PROCESS:
               return iArchive.PROCESS;
            case ArchiveTable.SAVEDMEDIA:
               return iArchive.SAVEDMEDIA;
            case ArchiveTable.TEMPDATA:
               return iArchive.TEMPDATA;
            case ArchiveTable.TEXTDATA:
               return iArchive.TEXTDATA;
            case ArchiveTable.USERPREFERENCES:
               return iArchive.USERPREFERENCES;
            case ArchiveTable.VELDATA:
               return iArchive.VELDATA;
            case ArchiveTable.WORKNOTIFICATION:
               return iArchive.WORKNOTIFICATION;
            case ArchiveTable.WORKTYPE:
               return iArchive.WORKTYPE;
         }
         return null;
      }

      #endregion

      //------------------------------------------------------------------------

   }/* end class */

}/* end namespace */

//------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th July 2012
//  Add to project
//------------------------------------------------------------------------------