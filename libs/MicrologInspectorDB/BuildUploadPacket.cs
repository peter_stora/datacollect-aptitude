﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;
using System.Xml;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// This class will go through the entire database (point, nodes,
    /// measurements, images etc) to create a "single" XML upload file.
    /// Although there is only a single public method "BuildUpload()"
    /// which returns a "Compressed" packet, properties have also been
    /// included to allow certain elements of the full upload packet
    /// to be ignored (i.e. Images, Audio etc)
    /// </summary>
    public class BuildUploadPacket
    {
        //------------------------------------------------------------------------

        #region Private constants

        /// <summary>
        /// How many steps are there in this process?
        /// </summary>
        private const int UPLOAD_PROGRESS_STEPS = 100;

        /// <summary>
        /// How many items should the upload be broken down as
        /// </summary>
        private const int UPLOAD_ITEM_STEPS = 7;

        /// <summary>
        /// Name of the temporary cache file
        /// </summary>
        private const string CACHE_FILE = "cache.xml";

        /// <summary>
        /// Default: Should this upload support media records
        /// </summary>
        private const Boolean ADD_MEDIA_RECORD_DEFAULT = true;

        /// <summary>
        /// Default: Should this upload support Work Norications
        /// </summary>
        private const Boolean ADD_WORK_NOTIFICATIONS_DEFAULT = true;

        /// <summary>
        /// Default: Should Non-Collections be added to each container?
        /// </summary>
        private const Boolean ADD_NON_COLLECTIONS_TO_CONTAINER = false;

        /// <summary>
        /// Set to 0x0f to mask out Scanned Data collection, or 0xff 
        /// to return all status properties
        /// </summary>
        private const byte SKIPPED_STATUS_MASK = 0x0f;

        #endregion

        //------------------------------------------------------------------------

        #region Private fields

        /// <summary>
        /// Reference to Table ResultSet object
        /// </summary>
        private ITableConnect mTable = null;

        /// <summary>
        /// Should this upload support media records
        /// </summary>
        private Boolean mAddMediaRecords = ADD_MEDIA_RECORD_DEFAULT;

        /// <summary>
        /// Should this upload support Work Norications
        /// </summary>
        private Boolean mAddCmmsNotifications = ADD_WORK_NOTIFICATIONS_DEFAULT;

        /// <summary>
        /// Should Non-Collections be added to each container?
        /// </summary>
        private Boolean mAddNonCollectionsToContainer = ADD_NON_COLLECTIONS_TO_CONTAINER;

        /// <summary>
        /// Should this upload support new Work Notifications only
        /// </summary>
        private Boolean mNewNotificationsOnly = true;

        /// <summary>
        /// Is there a media content to this upload?
        /// </summary>
        private Boolean mHasMedia = false;

        /// <summary>
        /// Feedback management
        /// </summary>
        private DataProgress mProgressPosition = null;

        /// <summary>
        /// Temp measurements cache table
        /// </summary>
        private EnumDownloadMeasurements mEnumTempMeasurements;

        /// <summary>
        /// Velocity measurements cache table
        /// </summary>
        private EnumDownloadMeasurements mEnumVelMeasurements;

        /// <summary>
        /// Envelope measurements cache table
        /// </summary>
        private EnumDownloadMeasurements mEnumEnvMeasurements;

        #endregion

        //------------------------------------------------------------------------

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iTable"></param>
        public BuildUploadPacket(ITableConnect iTable)
        {
            // reference the table connection object
            mTable = iTable;

            // instantiate progress position object with default settings
            mProgressPosition = new DataProgress();

            // set the progress manager properties
            mProgressPosition.FeedbackSteps = UPLOAD_PROGRESS_STEPS;
            mProgressPosition.NoOfItemsToProcess = UPLOAD_PROGRESS_STEPS;

            // reset the progress manager
            mProgressPosition.Reset();

        }/* end constructor */

        //------------------------------------------------------------------------

        #region Public Properties

        /// <summary>
        /// Gets or sets the progress position object
        /// </summary>
        public DataProgress ProgressPosition
        {
            get
            {
                return mProgressPosition;
            }
            set
            {
                mProgressPosition = value;
            }
        }

        /// <summary>
        /// Gets or Sets whether media records should be included with the
        /// upload packet (these can be very large!)
        /// </summary>
        public Boolean AddSavedMediaRecords
        {
            get
            {
                return mAddMediaRecords;
            }
            set
            {
                mAddMediaRecords = value;
            }
        }

        /// <summary>
        /// Gets or Sets whether Cmms notification records should be included
        /// with the upload packet. These would typically be disabled for
        /// direct connection to the CMMS system via the middleware.
        /// </summary>
        public Boolean AddCmmsNotifications
        {
            get
            {
                return mAddCmmsNotifications;
            }
            set
            {
                mAddCmmsNotifications = value;
            }
        }

        /// <summary>
        /// Gets or Sets whether the upload file should include only those 
        /// notifications raised by the device user, or all notifications
        /// available in the database. 
        /// </summary>
        public Boolean NewNotificationsOnly
        {
            get
            {
                return mNewNotificationsOnly;
            }
            set
            {
                mNewNotificationsOnly = value;
            }
        }

        /// <summary>
        /// Gets or sets whether non-collections should also be added to each
        /// of the container records. This is required for scheduled and carry
        /// forward functionality with Smart Profiles
        /// </summary>
        public Boolean AddNonCollectionsToContainer
        {
            get
            {
                return mAddNonCollectionsToContainer;
            }
            set
            {
                mAddNonCollectionsToContainer = value;
            }
        }

        #endregion

        //------------------------------------------------------------------------

        #region Public Methods

        /// <summary>
        /// Builds and returns a compressed Upload file
        /// </summary>
        /// <param name="iDeviceUid">Device UID</param>
        /// <param name="iLastOperator">Last logged in Operator</param>
        /// <param name="iDeviceTimeZoneInfo">Device TimeZone</param>
        /// <returns>true if built and saved OK</returns>
        public PacketUploadCompressed BuildCompressedUpload(String iDeviceUid,
           String iLastOperator, TimeZoneRecord iDeviceTimeZoneInfo)
        {
            // create a new compression packet
            PacketUploadCompressed compressed = new PacketUploadCompressed();

            // create and save the upload cache file
            if (BuildUploadCacheFile(iDeviceUid, iLastOperator, iDeviceTimeZoneInfo, CACHE_FILE))
            {
                var str = File.ReadAllText(CACHE_FILE);
                Console.WriteLine("xml: \n\n" + str + "\n\n");
                // compress the cache file and return as an array of bytes
                compressed.ByteData = Compression.CompressFile(CACHE_FILE);
            }

            // if cache file already exists then delete it
            PacketBase.DeleteFile(CACHE_FILE);

            // are there any media files
            compressed.HasMedia = mHasMedia;

            // assign the hosting ID
            compressed.HostingPacket = (byte)PacketType.UPLOAD_FILE;

            // increment progress step
            ProgressPosition.Update(true);

            // and force to the end stop position!
            mProgressPosition.JumpToValue(100);

            // return the upload packet
            return compressed;

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

        #region Private Methods

        /// <summary>
        /// Build the upload cache file and save locally to the device
        /// </summary>
        /// <param name="iDeviceUid">Device UID</param>
        /// <param name="iLastOperator">Last logged in Operator</param>
        /// <param name="iDeviceTimeZoneInfo">Device TimeZone</param>
        /// <param name="iCacheFile">Cache file name and path</param>
        /// <returns>true if built and saved OK</returns></returns>
        private Boolean BuildUploadCacheFile(
           String iDeviceUid, String iLastOperator,
           TimeZoneRecord iDeviceTimeZoneInfo, String iCacheFile)
        {
            // reset the progress object
            ProgressPosition.Reset();

            // increment progress step
            ProgressPosition.Update(true);

            // cache all new measurements
            CacheAllNewMeasurements();

            //
            // create an upload record object. This is the blank 
            // upload object into which all other objects (nodes, 
            // points etc) are added. 
            //
            PacketUploadRecord uploadFile = new PacketUploadRecord();

            //
            // populate the upload packet header fields
            //
            uploadFile.UploadDateTime = PacketBase.DateTimeNow;
            uploadFile.LastOperator = iLastOperator;
            uploadFile.DeviceUID = iDeviceUid;
            uploadFile.DeviceTimeZoneInfo = iDeviceTimeZoneInfo;

            //
            // populate the upload file with a list of all nodes
            // that have been edited on the device by the user
            //
            uploadFile.NodeTable = mTable.Node.GetUploadNodes();

            // how many points are we going to process
            int pointsCount = mTable.Points.CountEnumUploadRecords();

            // lets now re-reference the progress bar based on the number of points
            mProgressPosition.NoOfItemsToProcess = UPLOAD_ITEM_STEPS + pointsCount;

            //
            // if we're also going to add non-collected points, then we
            // need to recalculate the progress bar settings here!
            //
            if (AddNonCollectionsToContainer)
            {
                pointsCount += mTable.Points.CountEnumNonCollectedUploadRecords();
                mProgressPosition.NoOfItemsToProcess = UPLOAD_ITEM_STEPS + pointsCount + 1;
            }

            // reset the progress object
            ProgressPosition.Reset();

            // add each of the containers to the upload file
            AddContainers(ref uploadFile);

            // add the structured route statistics to the upload
            AddStructuredLogRecords(ref uploadFile);

            // add the CMMS Work Notification reference tables to the upload
            AddCmmsWorkNotificationTables(ref uploadFile);

            // add the CMMS Work Notifications to the upload
            AddCmmsWorkNotifications(ref uploadFile);

            // add media objects (bitmap, WAV files etc) to the upload
            AddMediaRecords(ref uploadFile);

            // add the revised operator settings packet (typically password)
            AddOperatorRecords(ref uploadFile);

            // increment progress step
            ProgressPosition.Update(true);

            // ----------------------------------------------------------

            // dump a copy of the upload object to a cache file
            Boolean result = uploadFile.XmlToCacheFile(uploadFile, iCacheFile);

            // release all local measurements
            ReleaseCacheMeasurements();
            uploadFile.ClearElements();

            // return the result
            return result;

        }/* end method */


        /// <summary>
        /// Add each Container objects to the upload. Currently a container  
        /// can only be a Route or Workspace as Inspector does not support
        /// the concept of unloading raw hierarchy elements
        /// </summary>
        /// <param name="uploadFile">Upload file to add contianer to</param>
        private void AddContainers(ref PacketUploadRecord uploadFile)
        {
            int records;

            // reset the filtered points list
            EnumPoints filteredPoints = null;

            // get a list of all available nodes
            EnumNodes allNodes = mTable.Node.GetAllRecords();

            // get each of the container elements
            EnumNodes containers = GetContainers(allNodes);

            // process each container
            foreach (NodeRecord node in containers)
            {
                // reset the total record count
                records = 0;

                // create a new container record
                UploadContainerRecord container = new UploadContainerRecord(node);

                // get all points for the current container
                EnumPoints allPoints =
                   mTable.Points.GetEnumUploadRecords(node.RouteId);

                // ----------------------------------------------------------

                // get list of points that have only been edited    
                filteredPoints = GetEditedPoints(allPoints);

                // set the initial record count
                records = filteredPoints.Count;

                // add this filtered list to the upload
                AddEditedPoints(filteredPoints, ref container);

                // increment progress step
                ProgressPosition.Update(true);

                // ----------------------------------------------------------

                // get list of points that only have data collected
                filteredPoints = GetSavedDataPoints(allPoints);

                // increment the record count
                records += filteredPoints.Count;

                // add this filtered list to the upload
                AddPointMeasurements(filteredPoints, ref container);

                // increment progress step
                ProgressPosition.Update(true);

                // ----------------------------------------------------------

                // get list of points that have user notes 
                filteredPoints = GetPointsWithNotes(allPoints);

                // increment the record count
                records += filteredPoints.Count;

                // add this filtered list to the upload
                AddPointNotes(filteredPoints, ref container);

                // increment progress step
                ProgressPosition.Update(true);

                // ----------------------------------------------------------

                // get a list of points that have the Skipped flag checked
                filteredPoints = GetSkippedPoints(allPoints);

                // add this filtered list to the upload
                AddPointSkipped(filteredPoints, ref container);

                // increment progress step
                ProgressPosition.Update(true);

                // ----------------------------------------------------------

                //
                // if we want to also add non-collected Points, then we do so here.
                // Note: This is primarily for "Smart-Profiles" and "Carry Forward"
                // functionality where the Service needs to know what Points were
                // within what Route - were NOT collected.
                //
                if (AddNonCollectionsToContainer)
                {
                    // get all points for the current container
                    filteredPoints = mTable.Points.GetEnumNonCollectedUploadRecords(node.RouteId);

                    // increment the record count
                    records += filteredPoints.Count;

                    // add this filtered list to the upload
                    AddPointSkipped(filteredPoints, ref container);

                    // increment progress step
                    ProgressPosition.Update(true);
                }

                // ----------------------------------------------------------

                // only add the container if there are records available
                if (records > 0)
                {
                    uploadFile.Containers.AddContainerRecord(container);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all saved media objects to the upload 
        /// </summary>
        private void AddMediaRecords(ref PacketUploadRecord iUploadFile)
        {
            // reset the media flag
            mHasMedia = false;

            if (AddSavedMediaRecords)
            {
                //
                // run through the saved media table and delete any records
                // that cannot be associated with a specific media file
                //
                DeleteOrphanMediaFiles();

                // get a list of all files that should be uploaded
                EnumUploadSavedMedia savedMediaRecords = mTable.SavedMedia.GetAllUploadRecords();

                // add to the upload process list
                foreach (UploadSavedMediaRecord mediaRecord in savedMediaRecords)
                {
                    if (mediaRecord != null)
                        iUploadFile.Media.AddMediaRecord(mediaRecord);
                }
            }

            //
            // if there are any media files to be processed then
            // set the "Has Media" flag true
            //
            if (iUploadFile.Media.Count > 0)
            {
                mHasMedia = true;
            }

        }/* end method */


        /// <summary>
        /// Delete any orphaned media files
        /// </summary>
        private void DeleteOrphanMediaFiles()
        {
            EnumSavedMedia savedMediaRecords = mTable.SavedMedia.GetAllRecords();

            StringBuilder fileName = new StringBuilder();

            foreach (SavedMediaRecord mediaRecord in savedMediaRecords)
            {
                fileName.Remove(0, fileName.Length);
                fileName.Append(mediaRecord.FilePath);
                fileName.Append(@"\");
                fileName.Append(mediaRecord.FileName);

                if (!File.Exists(fileName.ToString()))
                {
                    mTable.SavedMedia.ClearRecord(mediaRecord.Uid);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all revised Operator passwords to the upload 
        /// </summary>
        /// <param name="iUploadFile"></param>
        private void AddOperatorRecords(ref PacketUploadRecord iUploadFile)
        {
            // get a list of all operators
            OperatorSet operatorSet =
               mTable.Operators.GetOperatorSet();

            // add to the upload operators list
            for (int i = 0; i < operatorSet.OperatorNames.Count; ++i)
            {
                UploadOperator oper =
                   new UploadOperator(operatorSet.OperatorNames.NameData[i]);
                iUploadFile.Operators.AddOperator(oper);
            }

            // return an enumerated list of all changed passwords
            EnumUploadSettings uploadSettings =
               mTable.Operators.GetChangedPasswords();

            // return an enumerated list of all revised setting objects
            EnumUserPreferences userPreferences =
               mTable.UserPreferences.GetChangedPreferences();

            // add any user preferences to the upload settings
            uploadSettings =
               AddPreferencesToSettings(uploadSettings, userPreferences);

            // add the revised passwords to the upload file
            foreach (UploadSettings uploadSetting in uploadSettings)
            {
                if (uploadSetting != null)
                    iUploadFile.OperatorSettings.AddRecord(uploadSetting);
            }

        }/* end method */


        /// <summary>
        /// Add the Work Notification reference tables to the upload packet
        /// This method addresses OTDs #2719 and #2551
        /// </summary>
        /// <param name="iUploadFile"></param>
        private void AddCmmsWorkNotificationTables(ref PacketUploadRecord iUploadFile)
        {
            if (AddCmmsNotifications)
            {
                // create a new work notifications table object
                CmmsUploadTables wnTables = new CmmsUploadTables();

                // get a list of all available problems and add to the core
                EnumCmmsProblems problems = mTable.Problem.GetAllRecords();
                foreach (CmmsProblemRecord problem in problems)
                {
                    wnTables.AddCmmsProblem(problem);
                }

                // get a list of all available priorities and add to the core
                EnumCmmsPriorities priorities = mTable.Priority.GetAllRecords();
                foreach (CmmsPriorityRecord priority in priorities)
                {
                    wnTables.AddCmmsPriority(priority);
                }

                // get a list of all corrective actions and add to the core
                EnumCmmsCorrectiveActions correctiveActions = mTable.CorrectiveAction.GetAllRecords();
                foreach (CmmsCorrectiveActionRecord correctiveAction in correctiveActions)
                {
                    wnTables.AddCmmsCorrectiveAction(correctiveAction);
                }

                // get a list of all available work types and add to the core
                EnumCmmsWorkTypes workTypes = mTable.WorkType.GetAllRecords();
                foreach (CmmsWorkTypeRecord workType in workTypes)
                {
                    wnTables.AddCmmsWorkType(workType);
                }

                // add the WN tables to the upload file
                iUploadFile.ReferenceWorkNotifications = wnTables;
            }

        }/* end method */


        /// <summary>
        /// Add all new work notifications to the upload
        /// </summary>
        private void AddCmmsWorkNotifications(ref PacketUploadRecord iUploadFile)
        {
            if (AddCmmsNotifications)
            {
                // get a list of all newly created work notifications
                EnumCmmsUploadNotifications workNotifications =
                        mTable.WorkNotification.GetAllUploadRecords(NewNotificationsOnly);

                // add each to the upload packet
                foreach (CmmsWorkNotificationBase notification in workNotifications)
                {
                    if (notification != null)
                        iUploadFile.CMMSWorkNotifications.AddRecord(notification);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all Structured Route Log stats
        /// </summary>
        private void AddStructuredLogRecords(ref PacketUploadRecord iUploadFile)
        {
            EnumUploadStructuredLogs structuredLogRecords = mTable.StructuredLog.GetAllRecords();
            foreach (UploadStructuredLogRecord structuredLog in structuredLogRecords)
            {
                if (structuredLog != null)
                    iUploadFile.StructuredLogTable.AddStructuredLog(structuredLog);
            }
        }/* end method */


        /// <summary>
        /// Add all user edited points to the upload file
        /// </summary>
        /// <param name="iEnumEditedPoints">enumerated list of edited points</param>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private void AddEditedPoints(EnumPoints iEnumEditedPoints,
            ref UploadContainerRecord iUploadContainer)
        {
            // process each user edited points
            foreach (PointRecord point in iEnumEditedPoints)
            {
                if (point != null)
                {
                    // create a new upload point record
                    UploadPointRecord uploadPoint = new UploadPointRecord();

                    // add the point host index (TreeElemID)
                    uploadPoint.PointHostIndex = point.PointHostIndex;

                    // set the process type (fix for OTD#2351)
                    uploadPoint.ProcessType = point.ProcessType;

                    // add the current point record to this upload point 
                    uploadPoint.AddPointRevisions(point);

                    // now add revisions for MCD process type 
                    if (point.ProcessType == (byte)FieldTypes.ProcessType.MCD)
                    {
                        MCCRecord mcc = GetEditedMccPoint(point.Uid);
                        uploadPoint.AddMCCRecord(mcc, point.PointHostIndex);
                    }

                    // now add revisions for multi-point inspection type
                    else if ((point.ProcessType == (byte)FieldTypes.ProcessType.MultiSelectInspection) |
                    ((point.ProcessType == (byte)FieldTypes.ProcessType.SingleSelectInspection)))
                    {
                        InspectionRecord inspection = GetEditedInspectionPoint(point.Uid);
                        uploadPoint.AddInspectionRecord(inspection, point.PointHostIndex);
                    }

                    // now add revisions for general process types
                    else
                    {
                        ProcessRecord process = GetEditedProcessPoint(point.Uid);
                        uploadPoint.AddProcessRecord(process, point.PointHostIndex);
                    }

                    // add to the upload file
                    iUploadContainer.PointsTable.AddPointsRecord(uploadPoint);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all newly collected Notes (freehand and coded) to the upload packet
        /// </summary>
        /// <param name="iFilteredPoints">Enumerated list of PointRecord objects</param>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private void AddPointNotes(EnumPoints iFilteredPoints,
            ref UploadContainerRecord iContainer)
        {
            Boolean addAsNew;

            // get an enumerated list of all user notes
            EnumNotes userNotes = mTable.Notes.GetAllRecords();

            // get a list of all operator saved coded notes
            EnumSelectedCodedNotes codedNotes = mTable.SavedCodedNotes.GetAllRecords();

            // process each of the points with collected data
            foreach (PointRecord point in iFilteredPoints)
            {
                if (point != null)
                {
                    addAsNew = false;

                    //
                    // try to get a reference to this point in the current
                    // upload packet
                    //
                    UploadPointRecord uploadPoint =
                              GetReference(point.PointHostIndex, ref iContainer);

                    //
                    // OK, if the returned reference was null, then this
                    // point does not yet exist in the upload packet, so 
                    // let's add it now.
                    //
                    if (uploadPoint == null)
                    {
                        uploadPoint = new UploadPointRecord();
                        uploadPoint.PointHostIndex = point.PointHostIndex;
                        addAsNew = true;
                    }

                    // get an enumerated list of the notes assigned to this point
                    EnumNotes notes = LinqQueries.GetNotesForPoint(userNotes, point.Uid);

                    // process each note record
                    foreach (NotesRecord note in notes)
                    {
                        // create and populate a new selected note record
                        SelectedNotesRecord selectedNotes = new SelectedNotesRecord(note);

                        // get the coded notes assigned to this selected note
                        selectedNotes.CodedNotes =
                           LinqQueries.GetCodedNotesForNoteUid(codedNotes, selectedNotes.Uid);

                        // now add these notes to the current upload point
                        uploadPoint.AddPointNotes(selectedNotes, point.PointHostIndex);
                    }

                    // add to the upload file
                    if (addAsNew)
                        iContainer.PointsTable.AddPointsRecord(uploadPoint);
                }
            }

            // if we're done here lets help the GC a bit
            for (int i = 0; i < userNotes.Count; ++i)
                userNotes.NOTE[i] = null;

            for (int i = 0; i < codedNotes.Count; ++i)
                codedNotes.CodedNote[i] = null;

            codedNotes.CodedNote.Clear();
            userNotes.NOTE.Clear();

        }/* end method */


        /// <summary>
        /// When the field POINTS.Skipped is set, add to the upload point
        /// </summary>
        /// <param name="iFilteredPoints">Enumerated list of PointRecord objects</param>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private void AddPointSkipped(EnumPoints iFilteredPoints,
            ref UploadContainerRecord iContainer)
        {
            Boolean addAsNew;

            // process each of the points with collected data
            foreach (PointRecord point in iFilteredPoints)
            {
                if (point != null)
                {
                    addAsNew = false;

                    //
                    // try to get a reference to this point in the current
                    // upload packet
                    //
                    UploadPointRecord uploadPoint =
                              GetReference(point.PointHostIndex, ref iContainer);

                    //
                    // OK, if the returned reference was null, then this
                    // point does not yet exist in the upload packet, so 
                    // let's add it now.
                    //
                    if (uploadPoint == null)
                    {
                        uploadPoint = new UploadPointRecord();
                        uploadPoint.PointHostIndex = point.PointHostIndex;
                        addAsNew = true;
                    }

                    // set a suitable mask against the skipped field
                    point.Skipped = (byte)(SKIPPED_STATUS_MASK & point.Skipped);

                    // populate the skipped node element
                    uploadPoint.AddPointSkippedField(point, AddNonCollectionsToContainer);

                    // add to the upload file
                    if (addAsNew)
                        iContainer.PointsTable.AddPointsRecord(uploadPoint);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all newly collected measurements to each upload point
        /// along with the alarm state at time of collection
        /// </summary>
        /// <param name="iFilteredPoints">Enumerated list of PointRecord objects</param>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private void AddPointMeasurements(EnumPoints iFilteredPoints,
            ref UploadContainerRecord iUploadContainer)
        {
            Boolean addAsNew;

            // process each of the points with collected data
            foreach (PointRecord point in iFilteredPoints)
            {
                if (point != null)
                {
                    // increment progress step
                    ProgressPosition.Update(true);

                    addAsNew = false;

                    //
                    // try to get a reference to this point in the current
                    // upload packet
                    //
                    UploadPointRecord uploadPoint =
                              GetReference(point.PointHostIndex, ref iUploadContainer);

                    //
                    // OK, if the returned reference was null, then this
                    // point does not yet exist in the upload packet, so 
                    // let's add it now.
                    //
                    if (uploadPoint == null)
                    {
                        uploadPoint = new UploadPointRecord();
                        uploadPoint.PointHostIndex = point.PointHostIndex;
                        uploadPoint.ProcessType = point.ProcessType;
                        addAsNew = true;
                    }

                    // Add MCC measurements
                    if (point.ProcessType == (byte)FieldTypes.ProcessType.MCD)
                    {
                        // add the TempData measurements along with the alarm state
                        AddUploadTempMeasurement(point, uploadPoint);

                        //// add the VelData measurements along with the alarm state
                        AddUploadVelMeasurement(point, uploadPoint);

                        //// add the EnvData measurements along with the alarm state
                        AddUploadEnvMeasurement(point, uploadPoint);

                        // add any FFT Channel data
                        AddUploadFFTMeasurement(point, uploadPoint);
                    }

                    // Add INSPECTION measurements
                    else if ((point.ProcessType == (byte)FieldTypes.ProcessType.MultiSelectInspection) |
                    ((point.ProcessType == (byte)FieldTypes.ProcessType.SingleSelectInspection)))
                    {
                        // add the TempData measurements along with the alarm state
                        AddUploadTempMeasurement(point, uploadPoint);
                    }

                    // Add PROCESS measurements
                    else
                    {
                        // add the TempData measurements along with the alarm state
                        AddUploadTempMeasurement(point, uploadPoint);
                    }

                    // add to the upload file
                    if (addAsNew)
                        iUploadContainer.PointsTable.AddPointsRecord(uploadPoint);
                }
            }

        }/* end method */


        /// <summary>
        /// Add all newly collected FFT Channel measurements to the 
        /// upload points file.
        /// </summary>
        /// <param name="point">Reference point object</param>
        /// <param name="uploadPoint">Upload Point object to populate</param>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private void AddUploadFFTMeasurement(PointRecord point,
            UploadPointRecord uploadPoint)
        {
            // get an enumerated list of fft channel data
            EnumBaseFFTChannels fftChannels =
                   mTable.FFTChannel.GetBaseRecordsFromPointUid(point.Uid);

            // add each channel to the upload packet
            foreach (FFTChannelBase fftChannel in fftChannels)
            {
                if (fftChannel != null)
                    uploadPoint.Measurements.AddFFTChannel(fftChannel);
            }

        }/* end method */


        /// <summary>
        /// Add a TEMP measurement value to the upload file
        /// </summary>
        /// <param name="iPoint">measurement point reference</param>
        /// <param name="iUploadPoint">current upload point</param>
        private void AddUploadTempMeasurement(PointRecord iPoint, UploadPointRecord iUploadPoint)
        {
            // use LINQ to select points with new data
            IEnumerable<DownloadMeasurement> measurements =
                           from measurement in mEnumTempMeasurements.Measurement
                           where (measurement.PointUid == iPoint.Uid)
                           orderby measurement.LocalTime descending
                           select measurement;

            // hydrate the LINQ return list
            foreach (DownloadMeasurement tempMeasurement in measurements)
            {
                if (tempMeasurement != null)
                {
                    // extract the upload measurement and add to the list
                    iUploadPoint.AddTempMeasurement(
                       tempMeasurement.GetUploadMeasurent(SKIPPED_STATUS_MASK));
                }
            }

        }/* end method */


        /// <summary>
        /// Add a VEL measurement value to the upload file
        /// </summary>
        /// <param name="iPoint">measurement point reference</param>
        /// <param name="iUploadPoint">current upload point</param>
        private void AddUploadVelMeasurement(PointRecord iPoint, UploadPointRecord iUploadPoint)
        {
            // use LINQ to select points with new data
            IEnumerable<DownloadMeasurement> measurements =
                           from measurement in mEnumVelMeasurements.Measurement
                           where (measurement.PointUid == iPoint.Uid)
                           orderby measurement.LocalTime descending
                           select measurement;

            // hydrate the LINQ return list
            foreach (DownloadMeasurement velMeasurement in measurements)
            {
                if (velMeasurement != null)
                {
                    // extract the upload measurement and add to the list
                    iUploadPoint.AddVelMeasurement(
                       velMeasurement.GetUploadMeasurent(SKIPPED_STATUS_MASK));
                }
            }

        }/* end method */


        /// <summary>
        /// Add a ENV measurement value to the upload file
        /// </summary>
        /// <param name="iPoint">measurement point reference</param>
        /// <param name="iUploadPoint">current upload point</param>
        private void AddUploadEnvMeasurement(PointRecord iPoint, UploadPointRecord iUploadPoint)
        {
            // use LINQ to select points with new data
            IEnumerable<DownloadMeasurement> measurements =
                           from measurement in mEnumEnvMeasurements.Measurement
                           where (measurement.PointUid == iPoint.Uid)
                           orderby measurement.LocalTime descending
                           select measurement;

            // hydrate the LINQ return list
            foreach (DownloadMeasurement envMeasurement in measurements)
            {
                if (envMeasurement != null)
                {
                    // extract the upload measurement and add to the list
                    iUploadPoint.AddEnvMeasurement(
                       envMeasurement.GetUploadMeasurent(SKIPPED_STATUS_MASK));
                }
            }

        }/* end method */


        /// <summary>
        /// Return the edited process record associated with the current point
        /// </summary>
        /// <param name="iPointUid">Point UID</param>
        /// <returns>populated process record</returns>
        private ProcessRecord GetEditedProcessPoint(Guid iPointUid)
        {
            return mTable.Process.GetRecord(iPointUid);
        }/* end method */


        /// <summary>
        /// Return the edited inspection record associated with the current point
        /// </summary>
        /// <param name="iPointUid">Point UID</param>
        /// <returns>populated inspection record</returns>
        private InspectionRecord GetEditedInspectionPoint(Guid iPointUid)
        {
            return mTable.Inspection.GetRecord(iPointUid);
        }/* end method */


        /// <summary>
        /// Return the edited mcc record associated with the current point
        /// </summary>
        /// <param name="iPointUid">Point UID</param>
        /// <returns>populated mcc record</returns>
        private MCCRecord GetEditedMccPoint(Guid iPointUid)
        {
            return mTable.Mcc.GetRecord(iPointUid);
        }/* end method */


        /// <summary>
        /// Get an enumerated list of Point Records that have been
        /// edited by the user.
        /// </summary>
        /// <param name="myPoints">enumerated list of all points</param>
        /// <returns>enumerated list of edited points</returns>
        private static EnumPoints GetEditedPoints(EnumPoints iAllPoints)
        {
            // reference the Point Changed mask
            byte editedMask = (byte)FieldTypes.TagChanged.PointTagChanged;

            // select all points that have only been edited 
            IEnumerable<PointRecord> points =
                           from point in iAllPoints.POINTS
                           where ((point.TagChanged & editedMask) == editedMask)
                           select point;

            // hydrate the LINQ return
            EnumPoints pointsList = new EnumPoints();
            foreach (PointRecord point in points)
            {
                if (point != null)
                    pointsList.AddPointsRecord(point);
            }

            // return
            return pointsList;

        }/* end method */


        /// <summary>
        /// Get an enumerated list of Point Records that have been
        /// edited by the user.
        /// </summary>
        /// <param name="myPoints">enumerated list of all points</param>
        /// <returns>enumerated list of edited points</returns>
        private static EnumPoints GetSavedDataPoints(EnumPoints iAllPoints)
        {
            // use LINQ to select points with new data
            IEnumerable<PointRecord> points =
                           from point in iAllPoints.POINTS
                           where (point.DataSaved == true)
                           select point;

            // hydrate the LINQ return list
            EnumPoints pointsList = new EnumPoints();
            foreach (PointRecord point in points)
            {
                if (point != null)
                    pointsList.AddPointsRecord(point);
            }

            // and return 
            return pointsList;

        }/* end method */


        /// <summary>
        /// Get an enumerated list of only those points that have had
        /// notes assigned to them
        /// </summary>
        /// <param name="myPoints">enumerated list of all points</param>
        /// <returns>enumerated list of edited points</returns>
        private EnumPoints GetPointsWithNotes(EnumPoints iAllPoints)
        {
            // reference the Point Changed mask
            byte editedMask = (byte)FieldTypes.TagChanged.PointHasNotes;

            // select all points that have only been edited 
            IEnumerable<PointRecord> points =
                           from point in iAllPoints.POINTS
                           where ((point.TagChanged & editedMask) == editedMask)
                           select point;

            // hydrate the LINQ return
            EnumPoints pointsList = new EnumPoints();
            foreach (PointRecord point in points)
            {
                if (point != null)
                    pointsList.AddPointsRecord(point);
            }

            // return
            return pointsList;

        }/* end method */


        /// <summary>
        /// Get an enumerated list of Point Records that have been
        /// "skipped" by the user
        /// </summary>
        /// <param name="myPoints">enumerated list of all points</param>
        /// <returns>enumerated list of edited points</returns>
        private static EnumPoints GetSkippedPoints(EnumPoints iAllPoints)
        {
            // reference the Point Changed mask
            byte editedMask = (byte)FieldTypes.SkippedType.None;

            // select all points that have only been edited 
            IEnumerable<PointRecord> points =
                           from point in iAllPoints.POINTS
                           where (point.Skipped > editedMask)
                           select point;

            // hydrate the LINQ return
            EnumPoints pointsList = new EnumPoints();
            foreach (PointRecord point in points)
            {
                if (point != null)
                    pointsList.AddPointsRecord(point);
            }

            // return
            return pointsList;

        }/* end method */


        /// <summary>
        /// Return a reference to an existing upload point based on the
        /// Point Host Index of that point.
        /// </summary>
        /// <param name="iPointHostIndex">Index to query against</param>
        /// <returns>refernce to upload point - else null</returns>
        /// <param name="iUploadFile">the upload packet being populater</param>
        private UploadPointRecord GetReference(int iPointHostIndex,
            ref UploadContainerRecord iUploadContainer)
        {
            for (int i = 0; i < iUploadContainer.PointsTable.POINT.Count; ++i)
            {
                if (iUploadContainer.PointsTable.POINT[i].PointHostIndex == iPointHostIndex)
                {
                    return iUploadContainer.PointsTable.POINT[i];
                }
            }
            return null;
        }/* end method */


        /// <summary>
        /// Add any revised user preferences to the upload settings list
        /// </summary>
        /// <param name="uploadSettings">upload settings list</param>
        /// <param name="userPreferences">updated preferences list</param>
        /// <returns>list of preferences ordered on operator ID</returns>
        private EnumUploadSettings AddPreferencesToSettings(
           EnumUploadSettings iSettings, EnumUserPreferences iPreferences)
        {
            // if the upload settings list is currently null then instantiate
            if (iSettings == null)
            {
                iSettings = new EnumUploadSettings();
            }

            // process each of the user preference objects
            foreach (UserPreferencesRecord preferences in iPreferences)
            {
                //
                // check to see if the operator ID associated with the current 
                // preferences object is already in the upload settings list.
                // If it is then return the actual upload setting object
                //
                UploadSettings settingsRecord =
                   GetSettingsRecord(iSettings, preferences.OperatorId);

                // if an upload settings object was returned ..
                if (settingsRecord != null)
                {
                    //
                    // .. then set the the preferences field to the prefernces
                    // field in the current prefernces object
                    //
                    settingsRecord.UserPreferences =
                       preferences.BinaryPreferencesBlob;
                }
                else
                {
                    // .. otherwise create a new settings object
                    UploadSettings settings = new UploadSettings();

                    // and populate with the preferences fields
                    settings.OperId = preferences.OperatorId;
                    settings.UserPreferences = preferences.BinaryPreferencesBlob;
                    settings.ResetPassword = (byte)preferences.ResetPassword;

                    // add to the upload settings object
                    iSettings.AddRecord(settings);
                }
            }

            // return a revised settings list - sorted by Operator ID
            return SortOnOperatorId(iSettings);

        }/* end method */


        /// <summary>
        /// Sort the Upload Settings list based on the Operator ID field
        /// </summary>
        /// <param name="iSettings">Settings list to sort</param>
        /// <returns>Sorted list</returns>
        private EnumUploadSettings SortOnOperatorId(EnumUploadSettings iSettings)
        {
            // use LINQ to select the setting
            IEnumerable<UploadSettings> settings =
                           from setting in iSettings.Settings
                           orderby setting.OperId ascending
                           select setting;

            // hydrate the LINQ return list
            EnumUploadSettings settingsList = new EnumUploadSettings();

            foreach (UploadSettings setting in settings)
            {
                if (setting != null)
                    settingsList.AddRecord(setting);
            }

            return settingsList;

        }/* end method */


        /// <summary>
        /// Given a list of setting objects, return a single setting object
        /// based on the Operator ID
        /// </summary>
        /// <param name="iSettings">Reference Upload Settings list</param>
        /// <param name="iOperId">Operator ID to search on</param>
        /// <returns></returns>
        private UploadSettings GetSettingsRecord(EnumUploadSettings iSettings, int iOperId)
        {
            // use LINQ to select the setting
            IEnumerable<UploadSettings> settings =
                           from uSetting in iSettings.Settings
                           where (uSetting.OperId == iOperId)
                           orderby uSetting.OperId ascending
                           select uSetting;

            // hydrate the LINQ return list
            EnumUploadSettings settingsList = new EnumUploadSettings();
            foreach (UploadSettings setting in settings)
            {
                if (setting != null)
                    settingsList.AddRecord(setting);
            }

            // and return 
            if (settingsList.Settings.Count > 0)
            {
                return settingsList.Settings[0];
            }
            else
            {
                return null;
            }

        }/* end method */


        /// <summary>
        /// Returns a list of container nodes (Hierarchies, Workspaces etc.)
        /// </summary>
        /// <param name="iNodes">list of all available nodes</param>
        /// <returns>Container node elements</returns>
        private EnumNodes GetContainers(EnumNodes iNodes)
        {
            // use LINQ to select the setting
            IEnumerable<NodeRecord> nodes =
                           (from uSetting in iNodes.NODE
                            where (uSetting.HierarchyDepth == 0)
                            orderby uSetting.RouteId ascending
                            select uSetting).Distinct();

            // hydrate the LINQ return list
            EnumNodes nodesList = new EnumNodes();
            foreach (NodeRecord node in nodes)
            {
                if (node != null)
                    nodesList.AddNodeRecord(node);
            }

            // and return 
            return nodesList;

        }/* end method */


        /// <summary>
        /// Get an enumerated list of all new upload measurements
        /// </summary>
        private void CacheAllNewMeasurements()
        {
            mEnumTempMeasurements =
                  mTable.TempData.GetAllNewMeasurements();

            mEnumVelMeasurements =
                  mTable.VelData.GetAllNewMeasurements();

            mEnumEnvMeasurements =
                  mTable.EnvData.GetAllNewMeasurements();

        }/* end method */


        /// <summary>
        /// Release all cached measurement data
        /// </summary>
        private void ReleaseCacheMeasurements()
        {
            if (mEnumTempMeasurements != null)
            {
                for (int i = 0; i < mEnumTempMeasurements.Count; ++i)
                {
                    mEnumTempMeasurements.Measurement[i] = null;
                }
                mEnumTempMeasurements.Measurement.Clear();
                mEnumTempMeasurements = null;
            }

            if (mEnumVelMeasurements != null)
            {
                for (int i = 0; i < mEnumVelMeasurements.Count; ++i)
                {
                    mEnumVelMeasurements.Measurement[i] = null;
                }
                mEnumVelMeasurements.Measurement.Clear();
                mEnumVelMeasurements = null;
            }

            if (mEnumEnvMeasurements != null)
            {
                for (int i = 0; i < mEnumEnvMeasurements.Count; ++i)
                {
                    mEnumEnvMeasurements.Measurement[i] = null;
                }
                mEnumEnvMeasurements.Measurement.Clear();
                mEnumEnvMeasurements = null;
            }

        }/* end method */

        #endregion

        //------------------------------------------------------------------------

    }/* end class */

}/* end namespace */

//-------------------------------------------------------------------------------
//  BuildUploadPacket.cs
//
//  Revision 4.0 APinkerton 29th May 2012
//  Allow non-collected Points to be added to the Upload (for Smart-Profiles) 
//
//  Revision 4.0 APinkerton 29th May 2012
//  Allow non-collected Points to be added to the Upload (for Smart-Profiles) 
//
//  Revision 3.0 APinkerton 25th November 2010
//  Change upload serialization to use a cache file instead of the memory stream 
//
//  Revision 2.0 APinkerton 15th July 2010
//  All point objects now returned via the new CONTAINER packet 
//
//  Revision 1.0 APinkerton 19th March 2010
//  Implement LINQ queries to accelerate processing of User Notes 
//
//  Revision 0.0 APinkerton 27th June 2009
//  Add to project
//-------------------------------------------------------------------------------