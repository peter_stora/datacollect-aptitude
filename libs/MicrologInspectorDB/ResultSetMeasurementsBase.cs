﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 - 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This base class manages the TEMP/VEL/ENVDATA Table ResultSet
   /// </summary>
   public abstract class ResultSetMeasurementsBase : ResultSetBase
   {
      //----------------------------------------------------------------------

      #region Query Strings

      /// <summary>
      /// Parameterized query string prefix
      /// </summary>
      protected const string QUERY_PREFIX = "SELECT * FROM ";

      /// <summary>
      /// Parameterized query string for recovering Data for a particular UID
      /// </summary>
      protected string QUERY_DATA_BY_UID = 
         "WHERE PointUid = @Uid ORDER BY LocalTime DESC";

      /// <summary>
      /// Parameterized query string for recovering Measurement Data for a 
      /// particular Point Host Index 
      /// </summary>
      protected const string QUERY_DATA_BY_PHI =
          "WHERE PointHostIndex = @PointHostIndex ORDER BY LocalTime DESC";

      /// <summary>
      /// Parameterized query string for recovering Measurement Data for 
      /// a particular Point Host Index 
      /// </summary>
      protected const string QUERY_NEW_DATA_BY_PHI = 
          "WHERE PointHostIndex = @PointHostIndex AND New = @New " +
          "ORDER BY LocalTime DESC";

      /// <summary>
      /// Parameterized query string for recovering Measurement Data for 
      /// a particular Point Host Index 
      /// </summary>
      protected const string QUERY_NEW_DATA_BY_UID = 
          "WHERE PointUid = @PointUid AND New = @New " +
          "ORDER BY LocalTime DESC";

      /// <summary>
      /// Parameterized query string for recovering all new Measurement Data
      /// </summary>
      protected const string QUERY_NEW_DATA = 
          "WHERE New = @New " +
          "ORDER BY PointUid ASC";

      #endregion

      //----------------------------------------------------------------------

      /// <summary>
      /// Column ordinal object
      /// </summary>
      protected RowOrdinalsMeasurement mColumnOrdinal = null;

      //----------------------------------------------------------------------

      /// <summary>
      /// Gets the actual measurement table name
      /// </summary>
      protected virtual string MeasurementTable
      {
         get
         {
            return string.Empty;
         }
      }

      //----------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public abstract Boolean ClearTable();


      /// <summary>
      /// Add a series of measurements to the appropriate table.
      /// </summary>
      /// <param name="iPointRecords">enumerated list of point records</param>
      /// <returns>false on exception raised</returns>
      public abstract Boolean AddEnumRecords( EnumPoints iEnumPoints, 
         ref DataProgress iProgress );


      /// <summary>
      /// Add a series of measurements to the appropriate table.
      /// </summary>
      /// <param name="iPointRecords">enumerated list of point records</param>
      /// <returns>false on exception raised</returns>
      public abstract Boolean AddEnumRecords( EnumPoints iEnumPoints );


      /// <summary>
      /// Add a series of saved measurements to the appropriate table.
      /// </summary>
      /// <param name="iEnumMeasurements">enumerated list of measurements</param>
      /// <returns>false on exception raised</returns>
      public abstract Boolean AddEnumRecords( EnumSavedMeasurements iEnumMeasurements );


      /// <summary>
      /// Adds a single measurement to the table.
      /// </summary>
      /// <param name="iMeasurement">measurement object</param>
      /// <param name="iPointUid">associated Point Uid</param>
      /// <param name="iNewValue">Is this a new value</param>
      /// <param name="iAlarmFlags">AlarmState for this measurement</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( PointMeasurement iMeasurement,
          int iPointIndex, Guid iPointUid, Boolean iNewValue,
         Byte iAlarmFlags, Byte iStatus, int iOperId )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // add a new measurement record
                  iPointUid = AddMeasDataRecord(
                      iMeasurement,
                      iPointIndex,
                      iPointUid,
                      iNewValue,
                      iAlarmFlags,
                      iStatus,
                      iOperId,
                      ceResultSet );

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all measurement records
      /// </summary>
      /// <returns>Enumerated list or null on error</returns>
      public EnumSavedMeasurements GetAllRecords()
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumSavedMeasurements enumMeasurements = new EnumSavedMeasurements();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = MeasurementTable;

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new download measurement object
                  SavedMeasurement measurement = new SavedMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all PointMeasurement objects given the  
      /// Point Host Index of the current point.
      /// </summary>
      /// <param name="iPointHostIndex">@ptitude Hierarchy ID</param>
      /// <returns>enumerated list of Point objects</returns>
      public EnumMeasurementRecords EnumMeasurements( int iPointHostIndex )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumMeasurementRecords enumMeasurements = new EnumMeasurementRecords();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = command.CommandText =
               QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_PHI;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value = iPointHostIndex;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointMeasurement measurement = new PointMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Returns an enumerated list of all PointMeasurement objects given the  
      /// Point Host Index of the current point. 
      /// </summary>
      /// <param name="iPointHostIndex">@ptitude Hierarchy ID</param>
      /// <returns>enumerated list of Point measurement objects</returns>
      public EnumPointMeasurements EnumAllMeasurements( int iPointHostIndex )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPointMeasurements enumMeasurements = new EnumPointMeasurements();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = command.CommandText =
               QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_PHI;

            // reference the lookup fields as parameters
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value = iPointHostIndex;
            command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointMeasurement measurement = new PointMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Returns an enumerated list of new PointMeasurement objects given the  
      /// Point Host Index of the current point. 
      /// </summary>
      /// <param name="iPointHostIndex">@ptitude Hierarchy ID</param>
      /// <returns>enumerated list of Point measurement objects</returns>
      public EnumPointMeasurements EnumNewMeasurements( int iPointHostIndex )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPointMeasurements enumMeasurements = new EnumPointMeasurements();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = command.CommandText =
               QUERY_PREFIX + MeasurementTable + " " + QUERY_NEW_DATA_BY_PHI;

            // reference the lookup fields as parameters
            command.Parameters.Add( "@PointHostIndex", SqlDbType.Int ).Value = iPointHostIndex;
            command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointMeasurement measurement = new PointMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Returns an enumerated list of new PointMeasurement objects given the  
      /// PointUID of the current point. 
      /// </summary>
      /// <param name="iPointUid">Point GUID</param>
      /// <returns>enumerated list of Point measurement objects</returns>
      public EnumPointMeasurements EnumNewMeasurements( Guid iPointUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumPointMeasurements enumMeasurements = new EnumPointMeasurements();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText =
               QUERY_PREFIX + MeasurementTable + " " + QUERY_NEW_DATA_BY_UID;

            // reference the lookup fields as parameters
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;
            command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  PointMeasurement measurement = new PointMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Returns an enumerated list of new PointMeasurement objects given the  
      /// PointUID of the current point. 
      /// </summary>
      /// <param name="iPointUid">Point GUID</param>
      /// <returns>enumerated list of Point measurement objects</returns>
      public EnumUploadMeasurements EnumNewUploadMeasurements( Guid iPointUid )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumUploadMeasurements enumMeasurements = new EnumUploadMeasurements();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText =
               QUERY_PREFIX + MeasurementTable + " " + QUERY_NEW_DATA_BY_UID;

            // reference the lookup fields as parameters
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;
            command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // each of the DATA tables. As this is a 'one shot', the column ordinal 
               // object remains a singleton
               //
               if ( mColumnOrdinal == null )
                  mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  // create a new Point object
                  UploadMeasurement measurement = new UploadMeasurement();

                  PopulateMeasurementObject( ceResultSet, ref measurement );

                  // add to the enumerated list
                  if ( measurement != null )
                     enumMeasurements.AddMeasurement( measurement );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Returns an enumerated list of new PointMeasurement objects given the  
      /// PointUID of the current point. 
      /// </summary>
      /// <param name="iPointUid">Point GUID</param>
      /// <returns>enumerated list of Point measurement objects</returns>
      public EnumDownloadMeasurements GetAllNewMeasurements()
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumDownloadMeasurements enumMeasurements = new EnumDownloadMeasurements();

         try
         {
            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand() )
            {
               // reference the base query string
               command.CommandText =
                  QUERY_PREFIX + MeasurementTable + " " + QUERY_NEW_DATA;

               // reference the lookup fields as parameters
               command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // each of the DATA tables. As this is a 'one shot', the column ordinal 
                  // object remains a singleton
                  //
                  if ( mColumnOrdinal == null )
                     mColumnOrdinal = new RowOrdinalsMeasurement( ceResultSet );

                  // now read the record object
                  while ( ceResultSet.Read() )
                  {
                     // create a new download measurement object
                     DownloadMeasurement measurement = new DownloadMeasurement();

                     PopulateMeasurementObject( ceResultSet, ref measurement );

                     // add to the enumerated list
                     if ( measurement != null )
                        enumMeasurements.AddMeasurement( measurement );
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
         }
         catch
         {
            enumMeasurements = null;
         }

         CloseCeConnection();
         return enumMeasurements;

      }/* end measurement */


      /// <summary>
      /// Add a new measurement to the table and remove any older measurements.
      /// The actual value should be in one of the three measurement fields, and 
      /// must be at index position zero in the enumerated list of measurements! 
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <param name="iNoLastMeas">Number of measurements to maintain</param>
      /// <returns>true if record is written without errors</returns>
      public Boolean AppendRecord( PointRecord iRecord, int iOperId, int iNoLastMeas )
      {
         return AppendRecord(
            iRecord,
            iNoLastMeas,
            (FieldTypes.NodeAlarmState) iRecord.AlarmState,
            iOperId );

      }/* end method */


      /// <summary>
      /// Add a new measurement to the table and remove any older measurements.
      /// The actual value should be in one of the three measurement fields, and 
      /// must be at index position zero in the enumerated list of measurements! 
      /// This overloaded method allows for the Alarm State of the point to be 
      /// over-ridden (i.e for MCD)
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <param name="iNoLastMeas">Number of measurements to maintain</param>
      /// <param name="iAlarmState">Override the point alarm state</param>
      /// <returns>true if record is written without errors</returns>
      public abstract Boolean AppendRecord( PointRecord iRecord, int iNoLastMeas,
         FieldTypes.NodeAlarmState iAlarmState, int iOperId );


      /// <summary>
      /// This method resets the measurement NEW flag from TRUE to FALSE. The primary
      /// purpose of this method is to reduce the synchronization bandwidth by only
      /// replacing historic measurement values when the device profile is updated.
      /// </summary>
      /// <returns>True on success, else false</returns>
      public Boolean ResetAllNewFlags()
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText = MeasurementTable;
               command.CommandType = CommandType.TableDirect;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  // go through each of the result records and reset the new flag
                  while ( ceResultSet.Read() )
                  {
                     ceResultSet.SetBoolean( ceResultSet.GetOrdinal( "New" ), false );
                     ceResultSet.SetByte( ceResultSet.GetOrdinal( "AlarmState" ),
                           (byte) FieldTypes.NodeAlarmState.Reset );
                     ceResultSet.Update();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Deletes that last record from measurement table if it exists
      /// Fix for Defect #868
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <returns>true if record is deleted without errors</returns>
      public Boolean DeleteLastRecord( PointRecord iRecord )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText =
                  QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_UID;

               // reference the PointUid field
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iRecord.Uid;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  //
                  // Our query above would have got the newest reading in the 
                  // first record, that's the one we want to kill
                  //
                  if ( ceResultSet.Read() )
                  {
                     ceResultSet.Delete();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Delete all new measurements for a specific point
      /// </summary>
      /// <param name="iRecord">Populated record object</param>
      /// <returns>true if record is deleted without errors</returns>
      public Boolean DeleteNewRecords( PointRecord iPoint )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText =
                  QUERY_PREFIX + MeasurementTable + " " + QUERY_NEW_DATA_BY_UID;

               // reference the lookup fields as parameters
               command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPoint.Uid;
               command.Parameters.Add( "@New", SqlDbType.Bit ).Value = 1;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  //
                  // Our query above would have got the newest reading in the 
                  // first record, that's the one we want to kill
                  //
                  if ( ceResultSet.Read() )
                  {
                     ceResultSet.Delete();
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Gets the DTS of the last measurement
      /// </summary>
      /// <param name="iRecord">The populate PointRecord</param>
      /// <param name="iDTS">Reference to a DateTime object</param>
      /// <returns>True if a DTS was found</returns>
      public bool GetLastMeasurementDTS( PointRecord iRecord, ref DateTime iDTS )
      {
         OpenCeConnection();

         // default boolean result (no errors raised)
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            try
            {
               // reference the base query string
               command.CommandText =
                  QUERY_PREFIX + MeasurementTable + " " + QUERY_DATA_BY_UID;

               // reference the PointUid field
               command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iRecord.Uid;

               // execute the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  //
                  // Our query above would have got the newest reading in the 
                  // first record, that's the one we want to get the DTS for
                  //
                  if ( ceResultSet.Read() )
                  {
                     // Get the DTS
                     iDTS = ceResultSet.GetDateTime( mColumnOrdinal.LocalTime );
                  }

                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */

      #endregion

      //----------------------------------------------------------------------

      #region Protected Methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// MEASUREMENTS table. This table will need to be revised in the event of any 
      /// changes being made to the MEASUREMENTS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      protected void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsMeasurement( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Populate an instantiated PointMeasurement object with data from a single POINTS
      /// measurements. In the event of a raised exception the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="pointRecord">PointMeasurement object to be populated</param>
      protected void PopulateMeasurementObject( SqlCeResultSet ceResultSet,
          ref PointMeasurement iMeasurement )
      {
         try
         {
            //
            // if the Text field is not null then reference that value, otherwise
            // default to the numeric value
            //
            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
               iMeasurement.TextValue = ceResultSet.GetString( mColumnOrdinal.Text );
            else
               iMeasurement.Value = ceResultSet.GetDouble( mColumnOrdinal.Value );
            
            if ( ceResultSet[ mColumnOrdinal.LocalTime ] != DBNull.Value )
               iMeasurement.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LocalTime );
         }
         catch
         {
            iMeasurement = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated PointMeasurement object with data from a single POINTS
      /// measurements. In the event of a raised exception the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="pointRecord">UploadMeasurement object to be populated</param>
      protected void PopulateMeasurementObject( SqlCeResultSet ceResultSet,
          ref UploadMeasurement iMeasurement )
      {
         try
         {
            //
            // if the Text field is not null then reference that value, otherwise
            // default to the numeric value
            //
            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
               iMeasurement.TextValue = ceResultSet.GetString( mColumnOrdinal.Text );
            else
               iMeasurement.Value = ceResultSet.GetDouble( mColumnOrdinal.Value );

            iMeasurement.AlarmState = ceResultSet.GetByte( mColumnOrdinal.AlarmState );
            iMeasurement.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );
            iMeasurement.Status = ceResultSet.GetByte( mColumnOrdinal.Status );

            if ( ceResultSet[ mColumnOrdinal.LocalTime ] != DBNull.Value )
               iMeasurement.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LocalTime );
         }
         catch
         {
            iMeasurement = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated DownloadMeasurement object with data from a single. 
      /// In the event of a raised exception the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="pointRecord">PointMeasurement object to be populated</param>
      protected void PopulateMeasurementObject( SqlCeResultSet ceResultSet,
          ref DownloadMeasurement iMeasurement )
      {
         try
         {
            iMeasurement.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iMeasurement.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            iMeasurement.AlarmState = ceResultSet.GetByte( mColumnOrdinal.AlarmState );
            iMeasurement.Status = ceResultSet.GetByte( mColumnOrdinal.Status );
            iMeasurement.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );

            //
            // if the Text field is not null then reference that value, otherwise
            // default to the numeric value
            //
            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
               iMeasurement.TextValue = ceResultSet.GetString( mColumnOrdinal.Text );
            else
               iMeasurement.Value = ceResultSet.GetDouble( mColumnOrdinal.Value );

            if ( ceResultSet[ mColumnOrdinal.LocalTime ] != DBNull.Value )
               iMeasurement.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LocalTime );
         }
         catch
         {
            iMeasurement = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated SavedMeasurement object with data from a single. 
      /// In the event of a raised exception the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="pointRecord">PointMeasurement object to be populated</param>
      protected void PopulateMeasurementObject( SqlCeResultSet ceResultSet,
          ref SavedMeasurement iMeasurement )
      {
         try
         {
            iMeasurement.PointUid = ceResultSet.GetGuid( mColumnOrdinal.PointUid );
            iMeasurement.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            iMeasurement.AlarmState = ceResultSet.GetByte( mColumnOrdinal.AlarmState );
            iMeasurement.Status = ceResultSet.GetByte( mColumnOrdinal.Status );
            iMeasurement.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );

            //
            // if the Text field is not null then reference that value, otherwise
            // default to the numeric value
            //
            if ( ceResultSet[ mColumnOrdinal.Text ] != DBNull.Value )
               iMeasurement.TextValue = ceResultSet.GetString( mColumnOrdinal.Text );
            else
               iMeasurement.Value = ceResultSet.GetDouble( mColumnOrdinal.Value );
            
            iMeasurement.New = ceResultSet.GetBoolean( mColumnOrdinal.New );

            if ( ceResultSet[ mColumnOrdinal.LocalTime ] != DBNull.Value )
               iMeasurement.LocalTime = ceResultSet.GetDateTime( mColumnOrdinal.LocalTime );
         }
         catch
         {
            iMeasurement = null;
         }

      }/* end method */


      /// <summary>
      /// Add a single record to the measurement table
      /// </summary>
      /// <returns></returns>
      protected void AddMeasDataRecord( SavedMeasurement iMeasurement,
         SqlCeResultSet ceResultSet )
      {
         if ( iMeasurement.LocalTime.Ticks > 0 )
         {
            // create a new record
            SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

            newRecord[ mColumnOrdinal.PointUid ] = iMeasurement.PointUid;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iMeasurement.PointHostIndex;
            newRecord[ mColumnOrdinal.LocalTime ] = iMeasurement.LocalTime;

            // only write to one value field (either/or)
            if ( iMeasurement.Is_Numeric_Measurement() )
               newRecord[ mColumnOrdinal.Value ] = iMeasurement.Value;
            else
               newRecord[ mColumnOrdinal.Text ] = iMeasurement.TextValue;

            newRecord[ mColumnOrdinal.New ] = iMeasurement.New;
            newRecord[ mColumnOrdinal.AlarmState ] = iMeasurement.AlarmState;
            newRecord[ mColumnOrdinal.OperId ] = iMeasurement.OperId;
            newRecord[ mColumnOrdinal.Status ] = iMeasurement.Status;

            ceResultSet.Insert( newRecord, DbInsertOptions.KeepCurrentPosition );
         }

      }/* end method */


      /// <summary>
      /// Add a single record to the measurement table
      /// </summary>
      /// <param name="iMeasurement">populated measurement object</param>
      /// <param name="iPointIndex">Current Point Index</param>
      /// <param name="iPointUid">Point Uid of this measurement</param>
      /// <param name="iNewValue">is this an operator entered value</param>
      /// <param name="iAlarmFlags">Set alarm flags</param>
      /// <param name="iOperId">ID of the operator who collected this measurement</param>
      /// <param name="ceResultSet">CeResult set object to reference</param>
      /// <returns></returns>
      protected Guid AddMeasDataRecord( PointMeasurement iMeasurement,
         int iPointIndex,
         Guid iPointUid,
         Boolean iNewValue,
         Byte iAlarmFlags,
         Byte iStatus,
         int iOperId,
         SqlCeResultSet ceResultSet )
      {
         if ( iMeasurement.LocalTime.Ticks > 0 )
         {
            // create a new record
            SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

            newRecord[ mColumnOrdinal.PointUid ] = iPointUid;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iPointIndex;
            newRecord[ mColumnOrdinal.LocalTime ] = iMeasurement.LocalTime;
            
            // only write to one value field (either/or)
            if (iMeasurement.Is_Numeric_Measurement())
               newRecord[ mColumnOrdinal.Value ] = iMeasurement.Value;
            else
               newRecord[ mColumnOrdinal.Text ] = iMeasurement.TextValue;
            
            newRecord[ mColumnOrdinal.New ] = iNewValue;
            newRecord[ mColumnOrdinal.AlarmState ] = iAlarmFlags;
            newRecord[ mColumnOrdinal.OperId ] = iOperId;
            newRecord[ mColumnOrdinal.Status ] = iStatus;

            ceResultSet.Insert( newRecord, DbInsertOptions.KeepCurrentPosition );
         }
         return iPointUid;

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 2.0 APinkerton 14th September 2012
//  Add support for new TextData Measurement type
//
//  Revision 1.0 APinkerton 3rd February 2010
//  Add support for download measurement object
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------
