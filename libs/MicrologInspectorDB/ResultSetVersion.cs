﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class returns the current Database version
   /// </summary>
   public class ResultSetVersion : ResultSetBase
   {
      /// <summary>
      /// Gets the current database version
      /// </summary>
      /// <returns></returns>
      public int GetCurrentVersion()
      {
         // open the CE connection
         OpenCeConnection();

         // set a default result
         int result = 0;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "VERSION";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            try
            {
               // execure the result set and get the DB version
               using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  ceResultSet.Read();
                  result = ceResultSet.GetInt32( 0 );
                  ceResultSet.Close();
               }
            }
            catch
            {
               result = -1;
            }
         }

         // close connection and finish up ..
         CloseCeConnection();

         // return the method result
         return result;

      }/* end method */


   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd February 2011
//  Add to project
//----------------------------------------------------------------------------
