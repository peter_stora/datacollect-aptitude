﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the PROBLEM table. This structure should be revised following 
    /// any changes made to this table.
    /// </summary>
    public class RowOrdinalsProblem
    {
        public int MAProbDescKey { get; set; }
        public int ProbText{ get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsProblem(SqlCeResultSet iCeResultSet)
        {
            GetTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with 
        /// the PROBLEM table. This table will need to be revised in the event of 
        /// any changes being made to the PROBLEM table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.MAProbDescKey = iCeResultSet.GetOrdinal("MAProbDescKey");
            this.ProbText = iCeResultSet.GetOrdinal("ProbText");
        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2009
//  Add to project
//----------------------------------------------------------------------------
