﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the Log Table ResultSet
   /// </summary>
   public class ResultSetLogTable : ResultSetBase
   {
      #region Private Fields and Constants


      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsLog mColumnOrdinal = null;

      /// <summary>
      /// Query Log table based on the Event number
      /// </summary>
      private const string QUERY_BY_EVENT = "SELECT * FROM LOG " +
          "WHERE Event = @Event";


      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_LOG;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current Log table. 
      /// </summary>
      /// <param name="iMachineOkSkipsObject">populated MachineOkSkipsRecord object</param>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( DateTime iTime, int iEvent, string iName )
      {
         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "LOG";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update table contents
               result = WriteRecord( iTime, iEvent, iName, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }
         return result;

      }/* end method */


      /// <summary>
      /// Given the current Operator ID return a single Machine OK Skips record
      /// </summary>
      /// <param name="iOperId">The current Operator ID</param>
      /// <returns>Populated MachineSkip object</returns>
      public void GetRecord( ref DateTime oTime, ref int oEvent, ref string oName )
      {
         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_EVENT;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Event", SqlDbType.Int ).Value = oEvent;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the Log table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref oTime,
                                              ref oEvent,
                                              ref oName );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

      }/* end method */

      #endregion


      #region Private methods


      /// <summary>
      /// This method populates and returns the column ordinals associated with
      /// the Log table. This table will need to be revised in the event of any 
      /// changes being made to the Log table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsLog( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a new Log record
      /// </summary>
      /// <param name="iSkips">Populated Machine Skip object</param>
      /// <param name="iOperId">Current Operator ID</param>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>true on success, else false</returns>
      private bool WriteRecord( DateTime iTime, int iEvent, string iName,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Time ] = iTime;
            newRecord[ mColumnOrdinal.Event ] = iEvent;
            newRecord[ mColumnOrdinal.Name ] = iName;
            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Return each of the Log table fields as individual items
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <param name="iTime">returned time</param>
      /// <param name="iEvent">returned event</param>
      /// <param name="iName">returned name</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref DateTime oTime,
                                       ref int oEvent,
                                       ref string oName )
      {
         try
         {
            oTime = ceResultSet.GetDateTime( mColumnOrdinal.Time );
            oEvent = ceResultSet.GetInt32( mColumnOrdinal.Event );
            oName = ceResultSet.GetString( mColumnOrdinal.Name );

         }
         catch
         {
            oTime = PacketBase.DateTimeNow;
            oEvent = 0;
            oName = "";
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------