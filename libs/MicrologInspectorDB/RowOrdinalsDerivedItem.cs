﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the DERIVEDITEMS table. This structure must be revised following 
    /// any changes made to the table's field structure.
    /// </summary>
    public class RowOrdinalsDerivedItem
    {
        public int ID { get; set; }
        public int DerivedUid { get; set; }
        public int FormulaItemPosition { get; set; }
        public int FormulaItemType { get; set; }
        public int VarKey { get; set; }
        public int ConstValue { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsDerivedItem(SqlCeResultSet iCeResultSet)
        {
            GetDerivedItemsTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with the
        /// DERIVEDITEMS table. This method will need to be revised in the event of 
        /// any changes being made to the DERIVEDITEMS table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetDerivedItemsTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.ID = iCeResultSet.GetOrdinal("ID");
            this.DerivedUid = iCeResultSet.GetOrdinal("DerivedUid");
            this.FormulaItemPosition = iCeResultSet.GetOrdinal("FormulaItemPosition");
            this.FormulaItemType = iCeResultSet.GetOrdinal("FormulaItemType");
            this.VarKey = iCeResultSet.GetOrdinal("VarKey");
            this.ConstValue = iCeResultSet.GetOrdinal("ConstValue");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------