﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the CORRECTIVEACTION Table ResultSet
   /// </summary>
   public class ResultSetCorrectiveAction : ResultSetBase
   {
      #region Private Fields and Constants


      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsCorrectiveAction mColumnOrdinal = null;

      /// <summary>
      /// Query CORRECTIVEACTION table based on the CorrectiveAction key (single result)
      /// </summary>
      private const string QUERY_BY_ACTION_KEY = "SELECT * FROM CORRECTIVEACTION " +
          "WHERE MACorrActnKey = @MACorrActnKey";


      #endregion

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_CORRECTIVEACTION;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current CORRECTIVEACTION table. 
      /// </summary>
      /// <param name="iValue">populated Cmms CorrectiveAction object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CmmsCorrectiveActionRecord iValue )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CORRECTIVEACTION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // update the table
               result = WriteRecord( iValue, ceResultSet );

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the CORRECTIVEACTION table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of CorrectiveAction objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCmmsCorrectiveActionRecords iNotifications )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CORRECTIVEACTION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all CORRECTIVEACTION records to the table 
               for ( int i = 0; i < iNotifications.Count; ++i )
               {
                  result = result && WriteRecord( iNotifications.CorrectiveAction[ i ], ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CMMS CorrectiveAction Records
      /// </summary>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsCorrectiveActions GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCmmsCorrectiveActions correctiveActions = new EnumCmmsCorrectiveActions( );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "CORRECTIVEACTION";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CORRECTIVEACTION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsCorrectiveActionRecord correctiveAction = new CmmsCorrectiveActionRecord();
                  PopulateObject( ceResultSet, ref correctiveAction );
                  correctiveActions.AddCorrectiveActionRecord( correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveActions;

      }/* end method */


      /// <summary>
      /// Returns a single CorrectiveAction Record object from the CORRECTIVEACTION
      /// table using the CorrectiveAction Key as the lookup reference. 
      /// </summary>
      /// <param name="iNotificationID">WorkNotificationID</param>
      /// <returns>Populated WorkNotification object</returns>
      public CmmsCorrectiveActionRecord GetRecord( Int32 iActionKey )
      {
         OpenCeConnection();

         // reset the default profile name
         CmmsCorrectiveActionRecord correctiveAction = new CmmsCorrectiveActionRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_ACTION_KEY;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@MACorrActnKey", SqlDbType.Int ).Value = iActionKey;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the CORRECTIVEACTION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref correctiveAction );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return correctiveAction;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// CORRECTIVEACTION table. This table will need to be revised in the event of any 
      /// changes being made to the CORRECTIVEACTION table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsCorrectiveAction( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new WorkNotification record and add it 
      /// to the current CORRECTIVEACTION table.
      /// </summary>
      /// <param name="iValue">Populated Cmms CorrectiveAction object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CmmsCorrectiveActionRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.MACorrActnKey ] = iValue.MACorrectiveActionKey;

            if ( iValue.CorrectiveActionText != null )
               newRecord[ mColumnOrdinal.CorrActText ] = iValue.CorrectiveActionText;
            else
               newRecord[ mColumnOrdinal.CorrActText ] = string.Empty;
            
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single CORRECTIVEACTION table record
      /// </summary>
      /// <param name="iNotification">Updated CmmsCorrectiveAction object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CmmsCorrectiveActionRecord iValue,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetInt32( mColumnOrdinal.MACorrActnKey, 
               iValue.MACorrectiveActionKey );

            if ( iValue.CorrectiveActionText != null )
               ceResultSet.SetString( mColumnOrdinal.CorrActText, 
                  iValue.CorrectiveActionText );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotification Object with data from a single 
      /// CORRECTIVEACTION table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iValue">CmmsCorrectiveAction object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CmmsCorrectiveActionRecord iValue )
      {
         try
         {
            iValue.MACorrectiveActionKey = ceResultSet.GetInt32( mColumnOrdinal.MACorrActnKey );
            if ( ceResultSet[ mColumnOrdinal.CorrActText ] != DBNull.Value )
               iValue.CorrectiveActionText = ceResultSet.GetString( mColumnOrdinal.CorrActText );

         }
         catch
         {
            iValue = null;
         }

      }/* end method */


      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------