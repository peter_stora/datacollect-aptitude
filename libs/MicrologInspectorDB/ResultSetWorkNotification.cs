﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the WORKNOTIFICATION Table ResultSet
   /// </summary>
   public class ResultSetWorkNotification : ResultSetBase
   {
      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsWorkNotification mColumnOrdinal = null;

      /// <summary>
      /// Query WORKNOTIFICATION table based on the NotificationID field (single result)
      /// </summary>
      private const string QUERY_BY_NOTIFICATION_ID = "SELECT * FROM WORKNOTIFICATION " +
          "WHERE WorkNotificationID = @WorkNotificationID";

      /// <summary>
      /// Query WORKNOTIFICATION table based on the external NodeUid field (single result)
      /// </summary>
      private const string QUERY_BY_NODE_UID = "SELECT * FROM WORKNOTIFICATION " +
          "WHERE NodeUid = @NodeUid";

      /// <summary>
      /// Query WORKNOTIFICATION table based on the NotificationID field (single result)
      /// </summary>
      private const string QUERY_BY_STATUS_NEW = "SELECT * FROM WORKNOTIFICATION " +
          "WHERE Status = @Status";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_WORKNOTIFICATION;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }

         CloseCeConnection();

         // return result
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current WORKNOTIFICATION table. This
      /// method will overwrite an existing record if the WorkNotificationID
      /// is the same
      /// </summary>
      /// <param name="iWorkNotification">populated CmmsWorkNotification object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( CmmsWorkNotification iWorkNotification )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         try
         {
            //
            // Check if there's an existing record and delete it
            //

            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand( ) )
            {
               // reference the base query string
               command.CommandText = QUERY_BY_NOTIFICATION_ID;

               // reference the lookup field as a parameter
               command.Parameters.Add( "@WorkNotificationID", SqlDbType.Int ).Value =
                  iWorkNotification.WorkNotificationID;

               // execure the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the WORKNOTIFICATION table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // now read the record object (should only be one)
                  if ( ceResultSet.ReadFirst( ) )
                  {
                     // and delete this sucker!
                     ceResultSet.Delete( );
                  }

                  // close the resultSet
                  ceResultSet.Close( );
               }
            }

            //
            // Now, add the record
            //

            // create a new (self disposing) Sql Command object
            using ( SqlCeCommand command = CeConnection.CreateCommand( ) )
            {
               // reference the base query string
               command.CommandText = "WORKNOTIFICATION";
               command.CommandType = System.Data.CommandType.TableDirect;

               // execure the result set
               using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                           ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated 
                  // with  this table. As this is a 'one shot' the column ordinal
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // write record to table
                  result = WriteRecord( iWorkNotification, ceResultSet );

                  // close the resultSet
                  ceResultSet.Close( );
               }
            }
         }
         catch
         {
            result = false;
         }

         CloseCeConnection();

         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the WORKNOTIFICATION table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of WorkNotification objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumCmmsWorkNotificationRecords iNotifications )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "WORKNOTIFICATION";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all WORKNOTIFICATION records to the table 
               for ( int i = 0; i < iNotifications.Count; ++i )
               {
                  result = result && WriteRecord( iNotifications.WorkNotification[ i ], ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all CMMS Work Notification Records
      /// </summary>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsWorkNotifications GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumCmmsWorkNotifications workNotifications = 
                new EnumCmmsWorkNotifications( );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "WORKNOTIFICATION";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the WORKNOTIFICATION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsWorkNotification workNotification = new CmmsWorkNotification();
                  PopulateObject( ceResultSet, ref workNotification );
                  workNotifications.AddWorkNotificationRecord( workNotification );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return workNotifications;

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Returns an enumerated list of all CMMS Work Notification Records
      /// </summary>
      /// <param name="iNewOnly">Return only new records</param>
      /// <returns>Enumerated list of Work Notification objects</returns>
      public EnumCmmsUploadNotifications GetAllUploadRecords( Boolean iNewOnly )
      {
         OpenCeConnection();

         // instantiate point record enumerations list
         EnumCmmsUploadNotifications workNotifications = new EnumCmmsUploadNotifications();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_STATUS_NEW;

            // if only new records required then do not include uploaded records
            if ( !iNewOnly )
            {
               command.Parameters.Add( "@Status", SqlDbType.TinyInt ).Value =
                   (byte) FieldTypes.CmmsStatus.Uploaded;
            }

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Status", SqlDbType.TinyInt ).Value =
                (byte) FieldTypes.CmmsStatus.New;

            // execute the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the WORKNOTIFICATION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsWorkNotificationBase workNotification = new CmmsWorkNotificationBase();
                  PopulateUploadObject( ceResultSet, ref workNotification );
                  workNotifications.AddRecord( workNotification );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return workNotifications;

      }/* end method */


      /// <summary>
      /// Returns a single FFTChannel Record object from the WORKNOTIFICATION table 
      /// using the Notification IS as the lookup reference. 
      /// </summary>
      /// <param name="iNotificationID">WorkNotificationID</param>
      /// <returns>Populated WorkNotification object</returns>
      public CmmsWorkNotification GetRecordFromWorkNotification( Int32 iNotificationID )
      {
         OpenCeConnection();

         // reset the default profile name
         CmmsWorkNotification workNotification = new CmmsWorkNotification();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_NOTIFICATION_ID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@WorkNotificationID", SqlDbType.Int ).Value = iNotificationID;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the WORKNOTIFICATION table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref workNotification );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return workNotification;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all WorkNotification object types 
      /// given the Unique Identifier of the parent machine or Point node.
      /// </summary>
      /// <param name="iNodeUid">Unique machine node identifier</param>
      /// <returns>enumerated list of WorkNotification objects</returns>
      public EnumCmmsWorkNotifications GetRecords( Guid iNodeUid )
      {
         OpenCeConnection();

         // create the object result set
         EnumCmmsWorkNotifications wnRecords = 
                new EnumCmmsWorkNotifications( );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand workNotifications = CeConnection.CreateCommand() )
         {
            // reference the base query string
            workNotifications.CommandText = QUERY_BY_NODE_UID;

            // reference the lookup field as a parameter
            workNotifications.Parameters.Add( "@NodeUid", SqlDbType.UniqueIdentifier ).Value = iNodeUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet =
                    workNotifications.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the MESSAGE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  CmmsWorkNotification notification = new CmmsWorkNotification();
                  PopulateObject( ceResultSet, ref notification );
                  wnRecords.AddWorkNotificationRecord( notification );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return the method result
         return wnRecords;

      }/* end method */

      #endregion


      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// WORKNOTIFICATION table. This table will need to be revised in the event of any 
      /// changes being made to the WORKNOTIFICATION table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsWorkNotification( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new WorkNotification record and add it 
      /// to the current WORKNOTIFICATION table.
      /// </summary>
      /// <param name="iNotification">Populated CmmsWorkNotification object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( CmmsWorkNotification iNotification,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.NodeUid ] = iNotification.NodeUid;
            newRecord[ mColumnOrdinal.WorkNotificationID ] = iNotification.WorkNotificationID;
            newRecord[ mColumnOrdinal.RouteID ] = iNotification.RouteID;
            newRecord[ mColumnOrdinal.Status ] = iNotification.Status;
            newRecord[ mColumnOrdinal.KeyField ] = iNotification.KeyField;
            newRecord[ mColumnOrdinal.MachineID ] = iNotification.MachineID;
            newRecord[ mColumnOrdinal.LocationText ] = iNotification.LocationText;

            if ( iNotification.WNTimeStamp.Ticks > 0 )
               newRecord[ mColumnOrdinal.WNTimeStamp ] = iNotification.WNTimeStamp;

            newRecord[ mColumnOrdinal.OperatorID ] = iNotification.OperatorID;

            if ( iNotification.CompletionDate.Ticks > 0 )
               newRecord[ mColumnOrdinal.CompletionDate ] = iNotification.CompletionDate;

            newRecord[ mColumnOrdinal.WorkTypeIDs ] = iNotification.WorkTypeIDs;
            newRecord[ mColumnOrdinal.PriorityID ] = iNotification.PriorityID;
            newRecord[ mColumnOrdinal.ProblemID ] = iNotification.ProblemID;
            newRecord[ mColumnOrdinal.ProblemDescription ] = iNotification.ProblemDescription;
            newRecord[ mColumnOrdinal.CorrectiveActionID ] = iNotification.CorrectiveActionID;
            newRecord[ mColumnOrdinal.CorrectiveDescription ] = iNotification.CorrectiveDescription;
            newRecord[ mColumnOrdinal.FixByDateSet ] = iNotification.FixByDateSet;
            newRecord[ mColumnOrdinal.PRISMNumber ] = iNotification.PRISM;

            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single WORKNOTIFICATION table record
      /// </summary>
      /// <param name="iNotification">Updated CmmsWorkNotification object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( CmmsWorkNotification iNotification,
          SqlCeResultSet ceResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            ceResultSet.SetGuid( mColumnOrdinal.NodeUid, iNotification.NodeUid );
            ceResultSet.SetInt32( mColumnOrdinal.WorkNotificationID, iNotification.WorkNotificationID );
            ceResultSet.SetInt32( mColumnOrdinal.RouteID, iNotification.RouteID );
            ceResultSet.SetByte( mColumnOrdinal.Status, iNotification.Status );
            ceResultSet.SetByte( mColumnOrdinal.KeyField, iNotification.KeyField );
            ceResultSet.SetInt32( mColumnOrdinal.MachineID, iNotification.MachineID );
            ceResultSet.SetString( mColumnOrdinal.LocationText, iNotification.LocationText );

            if ( iNotification.WNTimeStamp.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.WNTimeStamp, iNotification.WNTimeStamp );

            ceResultSet.SetInt32( mColumnOrdinal.OperatorID, iNotification.OperatorID );

            if ( iNotification.CompletionDate.Ticks > 0 )
               ceResultSet.SetDateTime( mColumnOrdinal.CompletionDate, iNotification.CompletionDate );

            ceResultSet.SetString( mColumnOrdinal.WorkTypeIDs, iNotification.WorkTypeIDs );
            ceResultSet.SetInt32( mColumnOrdinal.PriorityID, iNotification.PriorityID );
            ceResultSet.SetInt32( mColumnOrdinal.ProblemID, iNotification.ProblemID );
            ceResultSet.SetString( mColumnOrdinal.ProblemDescription, iNotification.ProblemDescription );
            ceResultSet.SetInt32( mColumnOrdinal.CorrectiveActionID, iNotification.CorrectiveActionID );
            ceResultSet.SetString( mColumnOrdinal.CorrectiveDescription, iNotification.CorrectiveDescription );
            ceResultSet.SetBoolean( mColumnOrdinal.FixByDateSet, iNotification.FixByDateSet );
            ceResultSet.SetInt32( mColumnOrdinal.PRISMNumber, iNotification.PRISM );

            // update the record
            ceResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotification Object with data from a single 
      /// WORKNOTIFICATION table record. In the event of a raised exception the 
      /// returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iNotification">CmmsWorkNotification object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref CmmsWorkNotification iNotification )
      {
         try
         {
            if ( ceResultSet[ mColumnOrdinal.NodeUid ] != DBNull.Value )
               iNotification.NodeUid = ceResultSet.GetGuid( mColumnOrdinal.NodeUid );

            if ( ceResultSet[ mColumnOrdinal.WorkNotificationID ] != DBNull.Value )
               iNotification.WorkNotificationID = ceResultSet.GetInt32( mColumnOrdinal.WorkNotificationID );

            if ( ceResultSet[ mColumnOrdinal.RouteID ] != DBNull.Value )
               iNotification.RouteID = ceResultSet.GetInt32( mColumnOrdinal.RouteID );

            if ( ceResultSet[ mColumnOrdinal.Status ] != DBNull.Value )
               iNotification.Status = ceResultSet.GetByte( mColumnOrdinal.Status );

            if ( ceResultSet[ mColumnOrdinal.KeyField ] != DBNull.Value )
               iNotification.KeyField = ceResultSet.GetByte( mColumnOrdinal.KeyField );

            if ( ceResultSet[ mColumnOrdinal.MachineID ] != DBNull.Value )
               iNotification.MachineID = ceResultSet.GetInt32( mColumnOrdinal.MachineID );

            if ( ceResultSet[ mColumnOrdinal.LocationText ] != DBNull.Value )
               iNotification.LocationText = ceResultSet.GetString( mColumnOrdinal.LocationText );

            if ( ceResultSet[ mColumnOrdinal.WNTimeStamp ] != DBNull.Value )
               iNotification.WNTimeStamp = ceResultSet.GetDateTime( mColumnOrdinal.WNTimeStamp );

            if ( ceResultSet[ mColumnOrdinal.OperatorID ] != DBNull.Value )
               iNotification.OperatorID = ceResultSet.GetInt32( mColumnOrdinal.OperatorID );

            if ( ceResultSet[ mColumnOrdinal.CompletionDate ] != DBNull.Value )
               iNotification.CompletionDate = ceResultSet.GetDateTime( mColumnOrdinal.CompletionDate );

            if ( ceResultSet[ mColumnOrdinal.WorkTypeIDs ] != DBNull.Value )
               iNotification.WorkTypeIDs = ceResultSet.GetString( mColumnOrdinal.WorkTypeIDs );

            if ( ceResultSet[ mColumnOrdinal.PriorityID ] != DBNull.Value )
               iNotification.PriorityID = ceResultSet.GetInt32( mColumnOrdinal.PriorityID );

            if ( ceResultSet[ mColumnOrdinal.ProblemID ] != DBNull.Value )
               iNotification.ProblemID = ceResultSet.GetInt32( mColumnOrdinal.ProblemID );

            if ( ceResultSet[ mColumnOrdinal.ProblemDescription ] != DBNull.Value )
               iNotification.ProblemDescription = ceResultSet.GetString( mColumnOrdinal.ProblemDescription );

            if ( ceResultSet[ mColumnOrdinal.CorrectiveActionID ] != DBNull.Value )
               iNotification.CorrectiveActionID = ceResultSet.GetInt32( mColumnOrdinal.CorrectiveActionID );

            if ( ceResultSet[ mColumnOrdinal.CorrectiveDescription ] != DBNull.Value )
               iNotification.CorrectiveDescription = ceResultSet.GetString( mColumnOrdinal.CorrectiveDescription );

            if ( ceResultSet[ mColumnOrdinal.FixByDateSet ] != DBNull.Value )
               iNotification.FixByDateSet = ceResultSet.GetBoolean( mColumnOrdinal.FixByDateSet );

            if ( ceResultSet[ mColumnOrdinal.PRISMNumber ] != DBNull.Value )
               iNotification.PRISM = ceResultSet.GetInt32( mColumnOrdinal.PRISMNumber );
         }
         catch
         {
            iNotification = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated WorkNotificationBAse Object with data from a 
      /// single WORKNOTIFICATION table record. In the event of a raised exception 
      /// the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iNotification">CmmsWorkNotification object to be populated</param>
      private void PopulateUploadObject( SqlCeResultSet ceResultSet,
                                       ref CmmsWorkNotificationBase iNotification )
      {
         try
         {
            if ( ceResultSet[ mColumnOrdinal.WorkNotificationID ] != DBNull.Value )
               iNotification.WorkNotificationID = ceResultSet.GetInt32( mColumnOrdinal.WorkNotificationID );

            if ( ceResultSet[ mColumnOrdinal.LocationText ] != DBNull.Value )
               iNotification.LocationText = ceResultSet.GetString( mColumnOrdinal.LocationText );

            if ( ceResultSet[ mColumnOrdinal.WNTimeStamp ] != DBNull.Value )
               iNotification.WNTimeStamp = ceResultSet.GetDateTime( mColumnOrdinal.WNTimeStamp );

            if ( ceResultSet[ mColumnOrdinal.OperatorID ] != DBNull.Value )
               iNotification.OperatorID = ceResultSet.GetInt32( mColumnOrdinal.OperatorID );

            if ( ceResultSet[ mColumnOrdinal.CompletionDate ] != DBNull.Value )
               iNotification.CompletionDate = ceResultSet.GetDateTime( mColumnOrdinal.CompletionDate );

            if ( ceResultSet[ mColumnOrdinal.WorkTypeIDs ] != DBNull.Value )
               iNotification.WorkTypeIDs = ceResultSet.GetString( mColumnOrdinal.WorkTypeIDs );

            if ( ceResultSet[ mColumnOrdinal.PriorityID ] != DBNull.Value )
               iNotification.PriorityID = ceResultSet.GetInt32( mColumnOrdinal.PriorityID );

            if ( ceResultSet[ mColumnOrdinal.ProblemID ] != DBNull.Value )
               iNotification.ProblemID = ceResultSet.GetInt32( mColumnOrdinal.ProblemID );

            if ( ceResultSet[ mColumnOrdinal.ProblemDescription ] != DBNull.Value )
               iNotification.ProblemDescription = ceResultSet.GetString( mColumnOrdinal.ProblemDescription );

            if ( ceResultSet[ mColumnOrdinal.CorrectiveActionID ] != DBNull.Value )
               iNotification.CorrectiveActionID = ceResultSet.GetInt32( mColumnOrdinal.CorrectiveActionID );

            if ( ceResultSet[ mColumnOrdinal.CorrectiveDescription ] != DBNull.Value )
               iNotification.CorrectiveDescription = ceResultSet.GetString( mColumnOrdinal.CorrectiveDescription );

            if ( ceResultSet[ mColumnOrdinal.PRISMNumber ] != DBNull.Value )
               iNotification.PRISM = ceResultSet.GetInt32( mColumnOrdinal.PRISMNumber );

            if ( ceResultSet[ mColumnOrdinal.KeyField ] != DBNull.Value )
               iNotification.KeyField = ceResultSet.GetByte( mColumnOrdinal.KeyField );
         }
         catch
         {
            iNotification = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------