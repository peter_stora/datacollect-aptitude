﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;


namespace SKF.RS.MicrologInspector.Database
{
    /// <summary>
    /// Structure containing the ordinal positions of each field within
    /// the NOTES table. This structure should be revised following 
    /// any changes made to this table.
    /// </summary>
    public class RowOrdinalsCNotes
    {
        public int NotesUid { get; set; }
        public int Code { get; set; }
        public int CodedTextNote { get; set; }

        /// <summary>
        /// Constructor (no overloads)
        /// </summary>
        /// <param name="iCeResultSet"></param>
        public RowOrdinalsCNotes(SqlCeResultSet iCeResultSet)
        {
            GetCNotesTableColumns(iCeResultSet);
        }/* end constructor */


        /// <summary>
        /// This method populates and returns the column ordinals associated with 
        /// the NOTES table. This table will need to be revised in the event of 
        /// any changes being made to the NOTES table field structure.
        /// </summary>
        /// <param name="ceResultSet">CE Results Set object to reference</param>
        /// <returns>populated structure of column enumerable</returns>
        private void GetCNotesTableColumns(SqlCeResultSet iCeResultSet)
        {
            this.NotesUid = iCeResultSet.GetOrdinal("NotesUid");
            this.Code = iCeResultSet.GetOrdinal("Code");
            this.CodedTextNote = iCeResultSet.GetOrdinal("CodedTextNote");

        }/* end method */

    }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------
