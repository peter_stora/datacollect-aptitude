﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System.Data.SqlServerCe;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the OPERATORS table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsOperators
   {
      public int AccessLevel { get; set; }
      public int BarcodeLogin { get; set; }
      public int BarcodePassword { get; set; }
      public int CanChangePassword { get; set; }
      public int CreateWorkNotification { get; set; }
      public int DadType { get; set; }
      public int DataCollectionTime { get; set; }
      public int EnableCamera { get; set; }
      public int NumericRangeProtection { get; set; }
      public int OperId { get; set; }
      public int OperName { get; set; }
      public int Password { get; set; }
      public int PasswordChanged { get; set; }
      public int ResetPassword { get; set; }
      public int ScanAndGoToFirstPoint { get; set; }
      public int ScanRequiredToCollect { get; set; }
      public int ShowPreData { get; set; }
      public int VerifyMode { get; set; }
      public int ViewCMMSWorkHistory { get; set; }
      public int ViewOverdueOnly { get; set; }
      public int ViewSpectralFFT { get; set; }
      public int ViewTreeElement { get; set; }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsOperators( SqlCeResultSet iCeResultSet )
      {
         GetTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated
      /// with the OPERATORS table. This table will need to be revised
      ///  in the event of any changes being made to the OPERATORS table 
      ///  field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.AccessLevel = iCeResultSet.GetOrdinal( "AccessLevel" );
         this.ViewSpectralFFT = iCeResultSet.GetOrdinal( "BarcodeLogin" );
         this.ViewSpectralFFT = iCeResultSet.GetOrdinal( "BarcodePassword" );
         this.CanChangePassword = iCeResultSet.GetOrdinal( "CanChangePassword" );
         this.CreateWorkNotification = iCeResultSet.GetOrdinal( "CreateWorkNotification" );
         this.DadType = iCeResultSet.GetOrdinal( "DadType" );
         this.DataCollectionTime = iCeResultSet.GetOrdinal( "DataCollectionTime" );
         this.ViewSpectralFFT = iCeResultSet.GetOrdinal( "EnableCamera" );
         this.NumericRangeProtection = iCeResultSet.GetOrdinal( "NumericRangeProtection" );
         this.OperId = iCeResultSet.GetOrdinal( "OperId" );
         this.OperName = iCeResultSet.GetOrdinal( "OperName" );
         this.Password = iCeResultSet.GetOrdinal( "Password" );
         this.PasswordChanged = iCeResultSet.GetOrdinal( "PasswordChanged" );
         this.ResetPassword = iCeResultSet.GetOrdinal( "ResetPassword" );
         this.ScanAndGoToFirstPoint = iCeResultSet.GetOrdinal( "ScanAndGoToFirstPoint" );
         this.ScanRequiredToCollect = iCeResultSet.GetOrdinal( "ScanRequiredToCollect" );
         this.ShowPreData = iCeResultSet.GetOrdinal( "ShowPreData" );
         this.VerifyMode = iCeResultSet.GetOrdinal( "VerifyMode" );
         this.ViewCMMSWorkHistory = iCeResultSet.GetOrdinal( "ViewCMMSWorkHistory" );
         this.ViewOverdueOnly = iCeResultSet.GetOrdinal( "ViewOverdueOnly" );
         this.ViewSpectralFFT = iCeResultSet.GetOrdinal( "ViewSpectralFFT" );
         this.ViewTreeElement = iCeResultSet.GetOrdinal( "ViewTreeElement" );
         this.EnableCamera = iCeResultSet.GetOrdinal( "EnableCamera" );
         this.BarcodeLogin = iCeResultSet.GetOrdinal( "BarcodeLogin" );
         this.BarcodePassword = iCeResultSet.GetOrdinal( "BarcodePassword" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 23rd June 2009
//  Add to project
//----------------------------------------------------------------------------

