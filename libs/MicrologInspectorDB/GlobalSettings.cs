﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SKF.RS.MicrologInspector.Database
{   
    /// <summary>
    /// Static global settings class
    /// </summary>
    public static class Tables
    {   
        /// <summary>
        /// Enumerated list of all available tables
        /// </summary>
        public enum InspectorTable
        {
            DEFAULT,
            C_NOTE,
            C_NOTES,
            CONDITIONALPOINT,
            CORRECTIVEACTION,
            DERIVEDITEMS,
            DERIVEDPOINT,
            DEVICEPROFILE,
            ENVDATA,
            FFTCHANNEL,
            FFTPOINT,
            INSPECTION,
            INSTRUCTIONS,
            LOG,
            MACHINENOPSKIPS,
            MACHINEOKSKIPS,
            MCC,
            MESSAGE,
            MESSAGINGPREFS,
            NODE,
            NOTES,
            OPERATORS,
            POINTS,
            PRIORITY,
            PROBLEM,
            PROCESS,
            REFERENCEMEDIA,
            SAVEDMEDIA,
            STRUCTURED,
            STRUCTUREDLOG,
            TEMPDATA,
            USERPREFERENCES,
            VELDATA,
            WORKNOTIFICATION,
            WORKTYPE
        }

    }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 6th October 2009
//  Add to project
//----------------------------------------------------------------------------