﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the INSPECTION table. This structure should be revised following any
   /// changes made to this table.
   /// </summary>
   public class RowOrdinalsInspection
   {
      public int Uid
      {
         get;
         set;
      }
      public int Number
      {
         get;
         set;
      }
      public int PointUid
      {
         get;
         set;
      }
      public int Question
      {
         get;
         set;
      }
      public int AlertText
      {
         get;
         set;
      }
      public int DangerText
      {
         get;
         set;
      }
      public int NumberOfChoices
      {
         get;
         set;
      }
      public int Choice1
      {
         get;
         set;
      }
      public int Choice2
      {
         get;
         set;
      }
      public int Choice3
      {
         get;
         set;
      }
      public int Choice4
      {
         get;
         set;
      }
      public int Choice5
      {
         get;
         set;
      }
      public int Choice6
      {
         get;
         set;
      }
      public int Choice7
      {
         get;
         set;
      }
      public int Choice8
      {
         get;
         set;
      }
      public int Choice9
      {
         get;
         set;
      }
      public int Choice10
      {
         get;
         set;
      }
      public int Choice11
      {
         get;
         set;
      }
      public int Choice12
      {
         get;
         set;
      }
      public int Choice13
      {
         get;
         set;
      }
      public int Choice14
      {
         get;
         set;
      }
      public int Choice15
      {
         get;
         set;
      }
      public int AlarmType1
      {
         get;
         set;
      }
      public int AlarmType2
      {
         get;
         set;
      }
      public int AlarmType3
      {
         get;
         set;
      }
      public int AlarmType4
      {
         get;
         set;
      }
      public int AlarmType5
      {
         get;
         set;
      }
      public int AlarmType6
      {
         get;
         set;
      }
      public int AlarmType7
      {
         get;
         set;
      }
      public int AlarmType8
      {
         get;
         set;
      }
      public int AlarmType9
      {
         get;
         set;
      }
      public int AlarmType10
      {
         get;
         set;
      }
      public int AlarmType11
      {
         get;
         set;
      }
      public int AlarmType12
      {
         get;
         set;
      }
      public int AlarmType13
      {
         get;
         set;
      }
      public int AlarmType14
      {
         get;
         set;
      }
      public int AlarmType15
      {
         get;
         set;
      }
      public int BaselineDataTime
      {
         get;
         set;
      }
      public int BaselineDataValue
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsInspection( SqlCeResultSet iCeResultSet )
      {
         GetInspectionTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// INSPECTION table. This table will need to be revised in the event of any 
      /// changes being made to the INSPECTION table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetInspectionTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.Uid = iCeResultSet.GetOrdinal( "Uid" );
         this.Number = iCeResultSet.GetOrdinal( "Number" );
         this.PointUid = iCeResultSet.GetOrdinal( "PointUid" );
         this.Question = iCeResultSet.GetOrdinal( "Question" );
         this.AlertText = iCeResultSet.GetOrdinal( "AlertText" );
         this.DangerText = iCeResultSet.GetOrdinal( "DangerText" );
         this.NumberOfChoices = iCeResultSet.GetOrdinal( "NumberOfChoices" );
         this.Choice1 = iCeResultSet.GetOrdinal( "Choice1" );
         this.Choice2 = iCeResultSet.GetOrdinal( "Choice2" );
         this.Choice3 = iCeResultSet.GetOrdinal( "Choice3" );
         this.Choice4 = iCeResultSet.GetOrdinal( "Choice4" );
         this.Choice5 = iCeResultSet.GetOrdinal( "Choice5" );
         this.Choice6 = iCeResultSet.GetOrdinal( "Choice6" );
         this.Choice7 = iCeResultSet.GetOrdinal( "Choice7" );
         this.Choice8 = iCeResultSet.GetOrdinal( "Choice8" );
         this.Choice9 = iCeResultSet.GetOrdinal( "Choice9" );
         this.Choice10 = iCeResultSet.GetOrdinal( "Choice10" );
         this.Choice11 = iCeResultSet.GetOrdinal( "Choice11" );
         this.Choice12 = iCeResultSet.GetOrdinal( "Choice12" );
         this.Choice13 = iCeResultSet.GetOrdinal( "Choice13" );
         this.Choice14 = iCeResultSet.GetOrdinal( "Choice14" );
         this.Choice15 = iCeResultSet.GetOrdinal( "Choice15" );
         this.AlarmType1 = iCeResultSet.GetOrdinal( "AlarmType1" );
         this.AlarmType2 = iCeResultSet.GetOrdinal( "AlarmType2" );
         this.AlarmType3 = iCeResultSet.GetOrdinal( "AlarmType3" );
         this.AlarmType4 = iCeResultSet.GetOrdinal( "AlarmType4" );
         this.AlarmType5 = iCeResultSet.GetOrdinal( "AlarmType5" );
         this.AlarmType6 = iCeResultSet.GetOrdinal( "AlarmType6" );
         this.AlarmType7 = iCeResultSet.GetOrdinal( "AlarmType7" );
         this.AlarmType8 = iCeResultSet.GetOrdinal( "AlarmType8" );
         this.AlarmType9 = iCeResultSet.GetOrdinal( "AlarmType9" );
         this.AlarmType10 = iCeResultSet.GetOrdinal( "AlarmType10" );
         this.AlarmType11 = iCeResultSet.GetOrdinal( "AlarmType11" );
         this.AlarmType12 = iCeResultSet.GetOrdinal( "AlarmType12" );
         this.AlarmType13 = iCeResultSet.GetOrdinal( "AlarmType13" );
         this.AlarmType14 = iCeResultSet.GetOrdinal( "AlarmType14" );
         this.AlarmType15 = iCeResultSet.GetOrdinal( "AlarmType15" );

         this.BaselineDataTime = iCeResultSet.GetOrdinal( "BaselineDataTime" );
         this.BaselineDataValue = iCeResultSet.GetOrdinal( "BaselineDataValue" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 14th June 2009
//  Add to project
//----------------------------------------------------------------------------