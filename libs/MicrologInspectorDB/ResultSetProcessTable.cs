﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the PROCESS Table ResultSet
   /// </summary>
   public class ResultSetProcessTable : ResultSetBase
   {
      #region Private fields and constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsProcess mColumnOrdinal = null;

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_BY_POINT_UID = "SELECT * FROM PROCESS " +
          "WHERE PointUid = @PointUid";

      /// <summary>
      /// Query table based purely on the Uid field (will return one point)
      /// </summary>
      private const string QUERY_ENUM_BY_POINT_UID = "SELECT * FROM PROCESS " +
          "WHERE ";

      #endregion


      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table. 
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_PROCESS;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single ProcessRecord object to the PROCESS table. 
      /// </summary>
      /// <param name="iProcessRecord">Populated ProcessRecord object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( ProcessRecord iProcessRecord )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROCESS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROCESS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               if ( !WriteRecord( iProcessRecord, ceResultSet ) )
               {
                  result = false;
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ProcessRecord objects to the PROCESS table
      /// and also update the progress bar updates object
      /// </summary>
      /// <param name="iEnumProcess">Enumerated List of ProcessRecord objects</param>
      /// <param name="iProgress">Progress update object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumProcess iEnumProcess, ref DataProgress iProgress )
      {
         OpenCeConnection();

         // reset the process objects list
         iEnumProcess.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROCESS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROCESS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( ProcessRecord process in iEnumProcess )
               {
                  // raise a progress update
                  RaiseProgressUpdate( iProgress );

                  // write record to table
                  if ( !WriteRecord( process, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of ProcessRecord objects to the PROCESS table. 
      /// </summary>
      /// <param name="iEnumProcess">Enumerated List of ProcessRecord objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumProcess iEnumProcess )
      {
         OpenCeConnection();

         // reset the process objects list
         iEnumProcess.Reset();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROCESS";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROCESS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // process each of the enumerable records
               foreach ( ProcessRecord process in iEnumProcess )
               {
                  if ( !WriteRecord( process, ceResultSet ) )
                  {
                     result = false;
                     break;
                  }
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Populate and return a single ProcessRecord object given the associated
      /// Point Uid.
      /// </summary>
      /// <param name="iPointUid">Point Uid guid</param>
      /// <returns>Populated ProcessRecord object</returns>
      public ProcessRecord GetRecord( Guid iPointUid )
      {
         OpenCeConnection();

         // create the default object
         ProcessRecord process = new ProcessRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;
            command.CommandType = CommandType.Text;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iPointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROCESS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // populate the process object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref process );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return process;

      }/* end method */


      /// <summary>
      /// Reset all Internal collection and edit flags within the PROCESS table
      /// </summary>
      /// eturns>true on success, or false on failure</returns>
      public EnumProcess GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumProcess processRecords = new EnumProcess();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "PROCESS";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the PROCESS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  ProcessRecord process = new ProcessRecord();
                  PopulateObject( ceResultSet, ref process );
                  if ( process != null )
                     processRecords.AddProcessRecord( process );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return processRecords;

      }/* end method */


      /// <summary>
      /// Updates a PROCESS table record with the data from its associated
      /// business object (a single ProcesaRecord object). As all fields are
      /// replaced, this method should be used with caution! 
      /// </summary>
      /// <param name="iPointRecord">Populated ProcessRecord object</param>
      public void UpdateRecord( ProcessRecord iProcessRecord )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_POINT_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@PointUid", SqlDbType.UniqueIdentifier ).Value = iProcessRecord.PointUid;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the POINTS table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  UpdateRecord( iProcessRecord, ceResultSet );
               }
               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion


      #region Table Specific Methods

      /// <summary>
      /// Return an enumerated list of populated Process objects given an
      /// array of Parent Uid (guids).
      /// </summary>
      /// <param name="iPointUids">Array of Point Uid guids</param>
      /// <returns>Enumerated list of populated Process objects - or null on failure</returns>
      public EnumProcess GetEnumRecords( Guid[] iPointUids )
      {
         OpenCeConnection();

         // create new enumerated list of Process objects
         EnumProcess enumProcess = new EnumProcess();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( Guid pointUid in iPointUids )
            {
               // if this is not the last parameter then add an additional OR clause
               if ( paramCount < iPointUids.Length )
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
               }
               // otherwise terminate query with semi-colon
               else
               {
                  query.Append( " PointUid = @P" + paramCount.ToString() +
                      " ORDER BY Number;" );
               }
               command.Parameters.Add( "@P" + paramCount.ToString(), SqlDbType.UniqueIdentifier ).Value = pointUid;

               // increment the parameter count
               ++paramCount;
            }

            // referebce the full command text
            command.CommandText = query.ToString();

            try
            {
               // execure the result set
               using ( SqlCeResultSet ceResultSet = 
                  command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
               {
                  //
                  // if necessary get a list of all column ordinals associated with 
                  // the PROCESS table. As this is a 'one shot' the column ordinal 
                  // object remains a singleton
                  //
                  GetTableColumns( ceResultSet );

                  // convert Process record to a ProcessRecord object
                  while ( ceResultSet.Read() )
                  {
                     ProcessRecord process = new ProcessRecord();
                     PopulateObject( ceResultSet, ref process );
                     enumProcess.AddProcessRecord( process );
                  }
                  // close the resultSet
                  ceResultSet.Close();
               }
            }
            catch
            {
               enumProcess = null;
            }
         }
         CloseCeConnection();
         return enumProcess;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of populated Process objects given an 
      /// enumerated list of populated Point objects. Note: This method does 
      /// not rely on the base class properties and so can be called directly 
      /// from a single instantiated ResultSet object.
      /// </summary>
      /// <param name="iPointUids">Enumerated list of Point objects</param>
      /// <returns>Enumerated list of populated Process objects - or null on failure</returns>
      public EnumProcess GetEnumRecords( EnumPoints iEnumPoints )
      {
         OpenCeConnection();

         // create new enumerated list of Process objects
         EnumProcess enumProcess = new EnumProcess();

         // create new query string
         StringBuilder query = new StringBuilder( QUERY_ENUM_BY_POINT_UID );

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            int paramCount = 1;
            foreach ( PointRecord point in iEnumPoints )
            {
               // ensure we are dealing with a process type
               if ( IsPointProcessType( point ) )
               {
                  // if this is not the last parameter then add an additional OR clause
                  if ( paramCount < iEnumPoints.Count )
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() + " OR" );
                  }
                  // otherwise terminate query with semi-colon
                  else
                  {
                     query.Append( " PointUid = @P" + paramCount.ToString() +
                         " ORDER BY Number;" );
                  }

                  // add the new parameter object
                  command.Parameters.Add( "@P" + paramCount.ToString(),
                      SqlDbType.UniqueIdentifier ).Value = point.Uid;

                  // increment the parameter count
                  ++paramCount;
               }
            }

            // if no process points found then just return null!
            if ( paramCount > 1 )
            {
               // referebce the full command text
               command.CommandText = query.ToString();

               try
               {
                  // execure the result set
                  using ( SqlCeResultSet ceResultSet = 
                     command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
                  {
                     //
                     // if necessary get a list of all column ordinals associated with 
                     // the PROCESS table. As this is a 'one shot' the column ordinal 
                     // object remains a singleton
                     //
                     GetTableColumns( ceResultSet );

                     // convert Process record to a ProcessRecord object
                     while ( ceResultSet.Read() )
                     {
                        ProcessRecord process = new ProcessRecord();
                        PopulateObject( ceResultSet, ref process );
                        enumProcess.AddProcessRecord( process );
                     }

                     // close the resultSet
                     ceResultSet.Close();
                  }
               }
               catch
               {
                  enumProcess = null;
               }
            }
         }
         CloseCeConnection();
         return enumProcess;

      }/* end method */

      #endregion


      #region Private Methods

      /// <summary>
      /// Returns true if the PointRecord object is a Process Type
      /// </summary>
      /// <param name="iPointRecord">Populated Point object</param>
      /// <returns>True if type is Process</returns>
      private Boolean IsPointProcessType( PointRecord iPointRecord )
      {
         FieldTypes.ProcessType prType = (FieldTypes.ProcessType) iPointRecord.ProcessType;

         if ( ( prType != FieldTypes.ProcessType.MCD ) ||
         ( prType != FieldTypes.ProcessType.MultiSelectInspection ) ||
         ( prType != FieldTypes.ProcessType.SingleSelectInspection ) ||
         ( prType != FieldTypes.ProcessType.Derived ) )
         {
            return true;
         }
         else
         {
            return false;
         }
      }/* end method */


      /// <summary>
      /// This method populates and returns the column ordinals associated with 
      /// the PROCESS table. This table will need to be revised in the event of 
      /// any changes being made to the PROCESS table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsProcess( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new PROCESS record and add it to 
      /// the current PROCESS table.
      /// </summary>
      /// <param name="iProcessObject">Populated Process Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( ProcessRecord iProcessObject,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         //
         // validate the process ranges, and if necessary set the 
         // AutoRange flag. This is a fix for OTD #2199
         //
         Alarms.ValidateRanges( ref iProcessObject );

         try
         {
            //
            // populate record object
            //
            newRecord[ mColumnOrdinal.Uid ] = iProcessObject.Uid;
            newRecord[ mColumnOrdinal.Number ] = iProcessObject.Number;
            newRecord[ mColumnOrdinal.PointUid ] = iProcessObject.PointUid;
            newRecord[ mColumnOrdinal.Form ] = iProcessObject.Form;
            newRecord[ mColumnOrdinal.RangeMin ] = iProcessObject.RangeMin;
            newRecord[ mColumnOrdinal.RangeMax ] = iProcessObject.RangeMax;

            if ( iProcessObject.Units != null )
               newRecord[ mColumnOrdinal.Units ] = iProcessObject.Units;
            else
               newRecord[ mColumnOrdinal.Units ] = string.Empty;

            newRecord[ mColumnOrdinal.AutoRange ] = iProcessObject.AutoRange;
            newRecord[ mColumnOrdinal.AlarmType ] = iProcessObject.AlarmType;
            newRecord[ mColumnOrdinal.AlarmState ] = iProcessObject.AlarmState;
            newRecord[ mColumnOrdinal.UpperAlert ] = iProcessObject.UpperAlert;
            newRecord[ mColumnOrdinal.LowerAlert ] = iProcessObject.LowerAlert;
            newRecord[ mColumnOrdinal.UpperDanger ] = iProcessObject.UpperDanger;
            newRecord[ mColumnOrdinal.LowerDanger ] = iProcessObject.LowerDanger;

            if ( iProcessObject.BaselineDataTime.Ticks > 0 )
               newRecord[ mColumnOrdinal.BaselineDataTime ] = iProcessObject.BaselineDataTime;

            newRecord[ mColumnOrdinal.BaselineDataValue ] = iProcessObject.BaselineDataValue;

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Update a single PROCESS table record
      /// </summary>
      /// <param name="iProcessRecord">Updated ProcessRecord object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( ProcessRecord iProcessRecord,
          SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            // set the record fields
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iProcessRecord.Uid );
            CeResultSet.SetInt32( mColumnOrdinal.Number, iProcessRecord.Number );
            CeResultSet.SetGuid( mColumnOrdinal.PointUid, iProcessRecord.PointUid );
            CeResultSet.SetByte( mColumnOrdinal.Form, iProcessRecord.Form );
            CeResultSet.SetDouble( mColumnOrdinal.RangeMin, iProcessRecord.RangeMin );
            CeResultSet.SetDouble( mColumnOrdinal.RangeMax, iProcessRecord.RangeMax );

            if ( iProcessRecord.Units != null )
               CeResultSet.SetString( mColumnOrdinal.Units, iProcessRecord.Units );

            CeResultSet.SetBoolean( mColumnOrdinal.AutoRange, iProcessRecord.AutoRange );
            CeResultSet.SetByte( mColumnOrdinal.AlarmType, iProcessRecord.AlarmType );
            CeResultSet.SetByte( mColumnOrdinal.AlarmState, iProcessRecord.AlarmState );
            CeResultSet.SetDouble( mColumnOrdinal.UpperAlert, iProcessRecord.UpperAlert );
            CeResultSet.SetDouble( mColumnOrdinal.LowerAlert, iProcessRecord.LowerAlert );
            CeResultSet.SetDouble( mColumnOrdinal.UpperDanger, iProcessRecord.UpperDanger );
            CeResultSet.SetDouble( mColumnOrdinal.LowerDanger, iProcessRecord.LowerDanger );

            if ( iProcessRecord.BaselineDataTime.Ticks > 0 )
               CeResultSet.SetDateTime( mColumnOrdinal.BaselineDataTime, iProcessRecord.BaselineDataTime );

            CeResultSet.SetDouble( mColumnOrdinal.BaselineDataValue, iProcessRecord.BaselineDataValue );

            // update the record
            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated ProcessRecord Object with data from a single 
      /// PROCESS table record. In the event of a raised exception the returned 
      /// Process object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iProcessRecord">Process object to be populated</param>
      private void PopulateObject( SqlCeResultSet CeResultSet,
          ref ProcessRecord iProcessRecord )
      {
         try
         {
            iProcessRecord.Uid = CeResultSet.GetGuid( mColumnOrdinal.Uid );
            iProcessRecord.Number = CeResultSet.GetInt32( mColumnOrdinal.Number );
            iProcessRecord.PointUid = CeResultSet.GetGuid( mColumnOrdinal.PointUid );
            iProcessRecord.Form = CeResultSet.GetByte( mColumnOrdinal.Form );
            iProcessRecord.RangeMin = CeResultSet.GetDouble( mColumnOrdinal.RangeMin );
            iProcessRecord.RangeMax = CeResultSet.GetDouble( mColumnOrdinal.RangeMax );

            if ( CeResultSet[ mColumnOrdinal.Units ] != DBNull.Value )
               iProcessRecord.Units = CeResultSet.GetString( mColumnOrdinal.Units );

            if ( CeResultSet[ mColumnOrdinal.AutoRange ] != DBNull.Value )
               iProcessRecord.AutoRange = CeResultSet.GetBoolean( mColumnOrdinal.AutoRange );

            iProcessRecord.AlarmType = CeResultSet.GetByte( mColumnOrdinal.AlarmType );
            iProcessRecord.AlarmState = CeResultSet.GetByte( mColumnOrdinal.AlarmState );
            iProcessRecord.UpperAlert = CeResultSet.GetDouble( mColumnOrdinal.UpperAlert );
            iProcessRecord.LowerAlert = CeResultSet.GetDouble( mColumnOrdinal.LowerAlert );
            iProcessRecord.UpperDanger = CeResultSet.GetDouble( mColumnOrdinal.UpperDanger );
            iProcessRecord.LowerDanger = CeResultSet.GetDouble( mColumnOrdinal.LowerDanger );

            if ( CeResultSet[ mColumnOrdinal.BaselineDataTime ] != DBNull.Value )
               iProcessRecord.BaselineDataTime = CeResultSet.GetDateTime( mColumnOrdinal.BaselineDataTime );

            iProcessRecord.BaselineDataValue = CeResultSet.GetDouble( mColumnOrdinal.BaselineDataValue );
         }
         catch
         {
            iProcessRecord = null;
         }

      }/* end method */

      #endregion

   }/* end class */

}/* end namespace */


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 17th July 2011
//  Protect against ORACLE null string fields (OTD# 3005)
//
//  Revision 0.0 APinkerton 16th June 2009
//  Add to project
//----------------------------------------------------------------------------