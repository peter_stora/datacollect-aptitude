﻿//-------------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009-2011 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//-------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;
using System.Text;
using System.IO;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class manages the SAVEDMEDIA Table ResultSet
   /// </summary>
   public partial class ResultSetSavedMediaTable : ResultSetBase
   {
      //-------------------------------------------------------------------------

      #region Private Fields and Constants

      /// <summary>
      /// Column ordinal object
      /// </summary>
      private RowOrdinalsSavedMedia mColumnOrdinal = null;

      /// <summary>
      /// Empty UTM string (for GPS positioning)
      /// </summary>
      private const string UTM_NULL = "00 00 00.00 N 00 00 00.00 W";

      /// <summary>
      /// Get a specific file based on its unique name 
      /// </summary>
      private const string QUERY_BY_FILENAME = "SELECT * FROM SAVEDMEDIA " +
          "WHERE FileName = @FileName";

      /// <summary>
      /// Get a specific file based on its record key field
      /// </summary>
      private const string QUERY_BY_KEY = "SELECT * FROM SAVEDMEDIA " +
          "WHERE Uid = @Uid";

      /// <summary>
      /// Get all records that are pending upload
      /// </summary>
      private const string QUERY_BY_UPLOAD_PENDING = "SELECT * FROM SAVEDMEDIA " +
          "WHERE Uploaded = @Uploaded";

      /// <summary>
      /// Get all image records
      /// </summary>
      private string QUERY_IMAGES = "SELECT * FROM SAVEDMEDIA " +
          "WHERE MediaType = " + ( (byte) BinaryMediaType.Image ).ToString();

      /// <summary>
      /// Get all audio records 
      /// </summary>
      private string QUERY_AUDIO = "SELECT * FROM SAVEDMEDIA " +
          "WHERE MediaType = " + ( (byte) BinaryMediaType.Audio ).ToString();

      /// <summary>
      /// Get all audio records 
      /// </summary>
      private string QUERY_VIDEO = "SELECT * FROM SAVEDMEDIA " +
          "WHERE MediaType = " + ( (byte) BinaryMediaType.Video ).ToString();

      /// <summary>
      /// Get all media that is neither image or audio
      /// </summary>
      private string QUERY_MEDIA = "SELECT * FROM SAVEDMEDIA " +
          "WHERE MediaType = " + ( (byte) BinaryMediaType.RawBinary ).ToString();

      /// <summary>
      /// Delete all uploaded entries 
      /// </summary>
      private const string DELETE_WHEN_UPLOADED = "DELETE FROM SAVEDMEDIA " +
          "WHERE Uploaded = 1";

      /// <summary>
      /// Delete a single saved media record based on the filename
      /// </summary>
      private const string DELETE_SINGLE_RECORD_FILENAME = "DELETE FROM SAVEDMEDIA " +
          "WHERE FileName = @FileName";

      /// <summary>
      /// Delete a single saved media record based on the UID
      /// </summary>
      private const string DELETE_SINGLE_RECORD_WITH_UID = "DELETE FROM SAVEDMEDIA " +
          "WHERE Uid = @Uid";

      #endregion

      //-------------------------------------------------------------------------

      #region Core Public Methods

      /// <summary>
      /// Clear the contents of this table.
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearTable()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DeleteQuery.CLEAR_SAVEDMEDIA;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add a single new record to the current SAVEDMEDIA table. 
      /// ResultSet object.
      /// </summary>
      /// <param name="iMedia">populated Saved object</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddRecord( SavedMediaRecord iMedia )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "SAVEDMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable );
            //
            // if necessary get a list of all column ordinals associated 
            // with  this table. As this is a 'one shot' the column ordinal
            // object remains a singleton
            //
            GetTableColumns( ceResultSet );

            result = WriteRecord( iMedia, ceResultSet );
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Add an enumerated list of records to the SAVEDMEDIA table. 
      /// </summary>
      /// <param name="iConditionalPointObjects">populated list of SavedMedia objects</param>
      /// <returns>false on exception raised</returns>
      public Boolean AddEnumRecords( EnumSavedMedia iSavedMedia )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "SAVEDMEDIA";
            command.CommandType = System.Data.CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated 
               // with  this table. As this is a 'one shot' the column ordinal
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // write all SAVEDMEDIA records to the table 
               foreach ( SavedMediaRecord mediaRecord in iSavedMedia )
               {
                  result = result && WriteRecord( mediaRecord, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }

         CloseCeConnection();

         // return method result
         return result;

      }/* end method */


      /// <summary>
      /// This method returns an enumerated list of ALL records pending upload 
      /// to @ptitude in the SAVEDMEDIA table.
      /// </summary>
      /// <returns>Enumerated list of UploadSavedMedia objects</returns>
      public EnumUploadSavedMedia GetAllUploadRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumUploadSavedMedia mediaRecords = new EnumUploadSavedMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_UPLOAD_PENDING;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uploaded", SqlDbType.Bit ).Value = false;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  UploadSavedMediaRecord referenceMedia = new UploadSavedMediaRecord();
                  PopulateUploadObject( ceResultSet, ref referenceMedia );
                  mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */


      /// <summary>
      /// Returns a list of all records in the SAVEDMEDIA table
      /// </summary>
      /// <returns>Enumerated list of UploadSavedMedia objects</returns>
      public EnumSavedMedia GetAllRecords()
      {
         OpenCeConnection();

         // reset the default profile name
         EnumSavedMedia mediaRecords = new EnumSavedMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = "SAVEDMEDIA";

            // reference the lookup field as a parameter
            command.CommandType = CommandType.TableDirect;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  SavedMediaRecord referenceMedia = new SavedMediaRecord();
                  PopulateObject( ceResultSet, ref referenceMedia );
                  mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */


      /// <summary>
      /// Updates a SAVEDMEDIA table record with the data from its associated
      /// business object (a single SAVEDMEDIA Record object). As all fields are
      /// replaced, this method should be used with caution! 
      /// </summary>
      /// <param name="iPointRecord">Populated SAVEDMEDIA Record object</param>
      public void UpdateRecord( SavedMediaRecord iMedia )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_FILENAME;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@FileName", SqlDbType.NVarChar ).Value = iMedia.FileName;

            // execure the result set
            using ( SqlCeResultSet CeResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( CeResultSet );

               // now read the record object
               if ( CeResultSet.ReadFirst() )
               {
                  UpdateRecord( iMedia, CeResultSet );
               }

               // close the resultSet
               CeResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Table Specific Methods

      /// <summary>
      /// Clear all records where Uploaded flag = true
      /// </summary>
      /// <returns>true on success, or false on failure</returns>
      public Boolean ClearUploaded()
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DELETE_WHEN_UPLOADED;
            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Clear a specific record
      /// </summary>
      /// <param name="iFileName">file to remove</param>
      /// <returns>true on success or false on failure</returns>
      public Boolean ClearRecord( string iFileName )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DELETE_SINGLE_RECORD_FILENAME;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@FileName", SqlDbType.NVarChar ).Value = iFileName;

            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Clear a specific record
      /// </summary>
      /// <param name="iUid">UID of file to remove</param>
      /// <returns>true on success or false on failure</returns>
      public Boolean ClearRecord( Guid iUid )
      {
         OpenCeConnection();

         // set the default result
         Boolean result = true;

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = DELETE_SINGLE_RECORD_WITH_UID;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.UniqueIdentifier ).Value = iUid;

            try
            {
               command.ExecuteResultSet( ResultSetOptions.None );
            }
            catch
            {
               result = false;
            }
         }
         CloseCeConnection();
         return result;

      }/* end method */


      /// <summary>
      /// Get a single Saved media record based on its Unique Filename
      /// </summary>
      /// <param name="iFileName">Filename with extenstion</param>
      /// <returns>Populated Saved Media record</returns>
      public SavedMediaRecord GetRecord( String iFileName )
      {
         OpenCeConnection();

         // create a new result object
         SavedMediaRecord mediaRecord = new SavedMediaRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_FILENAME;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@FileName", SqlDbType.NVarChar ).Value = iFileName;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref mediaRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecord;

      }/* end method */


      /// <summary>
      /// Get a single Saved media object based on its Key field
      /// </summary>
      /// <param name="iKey">Filename</param>
      /// <returns>Populated Saved Media record</returns>
      public SavedMediaRecord GetRecord( Guid iKey )
      {
         OpenCeConnection();

         // create a new result object
         SavedMediaRecord mediaRecord = new SavedMediaRecord();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_FILENAME;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uid", SqlDbType.Text ).Value = iKey;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the NODE table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               if ( ceResultSet.ReadFirst() )
               {
                  PopulateObject( ceResultSet, ref mediaRecord );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecord;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all SavedMedia records that
      /// pertain to a specific file type (i.e. Image or Audio)
      /// </summary>
      /// <param name="iType">Filetype to return</param>
      /// <returns>enumerated list of Saved Media records</returns>
      public EnumSavedMedia GetRecords( BinaryMediaType iType )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumSavedMedia mediaRecords = new EnumSavedMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // Set the query type based on the binary media to return
            switch ( iType )
            {
               case BinaryMediaType.Audio:
                  command.CommandText = QUERY_AUDIO;
                  break;

               case BinaryMediaType.Image:
                  command.CommandText = QUERY_IMAGES;
                  break;

               case BinaryMediaType.Video:
                  command.CommandText = QUERY_VIDEO;
                  break;

               case BinaryMediaType.RawBinary:
                  command.CommandText = QUERY_MEDIA;
                  break;
            }

            // execure the result set
            SqlCeResultSet ceResultSet = command.ExecuteResultSet( ResultSetOptions.Scrollable );

            //
            // if necessary get a list of all column ordinals associated with 
            // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
            // object remains a singleton
            //
            GetTableColumns( ceResultSet );

            // now read the record object
            while ( ceResultSet.Read() )
            {
               SavedMediaRecord referenceMedia = new SavedMediaRecord();
               PopulateObject( ceResultSet, ref referenceMedia );
               mediaRecords.AddMediaRecord( referenceMedia );
            }
         }
         CloseCeConnection();
         return mediaRecords;

      }/* end method */


      /// <summary>
      /// Returns an enumerated list of all SavedMedia records depending
      /// on whether the upload flag is either set or not
      /// </summary>
      /// <param name="iPendingUpload">True for files awaiting upload</param>
      /// <returns>enumerated list of Saved Media records</returns>
      public EnumSavedMedia GetRecords( Boolean iPendingUpload )
      {
         OpenCeConnection();

         // reset the default profile name
         EnumSavedMedia mediaRecords = new EnumSavedMedia();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_UPLOAD_PENDING;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uploaded", SqlDbType.Bit ).Value = !iPendingUpload;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = 
               command.ExecuteResultSet( ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  SavedMediaRecord referenceMedia = new SavedMediaRecord();
                  PopulateObject( ceResultSet, ref referenceMedia );
                  mediaRecords.AddMediaRecord( referenceMedia );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();
         return mediaRecords;


      }/* end method */


      /// <summary>
      /// This method sets the 'Uploaded' flags in all SAVEDMEDIA records
      /// to TRUE. This method should only be called after the server has
      /// acknowledged that all files were received.
      /// </summary>
      public void SetAllMediaFileUploadFlags( )
      {
         OpenCeConnection();

         // create a new (self disposing) Sql Command object
         using ( SqlCeCommand command = CeConnection.CreateCommand() )
         {
            // reference the base query string
            command.CommandText = QUERY_BY_UPLOAD_PENDING;

            // reference the lookup field as a parameter
            command.Parameters.Add( "@Uploaded", SqlDbType.Bit ).Value = false;
            command.CommandType = CommandType.Text;

            // execure the result set
            using ( SqlCeResultSet ceResultSet = command.ExecuteResultSet(
                        ResultSetOptions.Updatable | ResultSetOptions.Scrollable ) )
            {
               //
               // if necessary get a list of all column ordinals associated with 
               // the SAVEDMEDIA table. As this is a 'one shot' the column ordinal 
               // object remains a singleton
               //
               GetTableColumns( ceResultSet );

               // now read the record object
               while ( ceResultSet.Read() )
               {
                  SavedMediaRecord savedMedia = new SavedMediaRecord();
                  PopulateObject( ceResultSet, ref savedMedia );
                  savedMedia.Uploaded = true;
                  UpdateRecord( savedMedia, ceResultSet );
               }

               // close the resultSet
               ceResultSet.Close();
            }
         }
         CloseCeConnection();

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      #region Private methods

      /// <summary>
      /// This method populates and returns the column ordinals associated with the
      /// SAVEDMEDIA table. This table will need to be revised in the event of any 
      /// changes being made to the SAVEDMEDIA table field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         if ( mColumnOrdinal == null )
            mColumnOrdinal = new RowOrdinalsSavedMedia( iCeResultSet );

      }/* end method */


      /// <summary>
      /// Construct and populate a single new SavedMediaRecord and add it 
      /// to the current SAVEDMEDIA table.
      /// </summary>
      /// <param name="iMedia">Populated RefernceMedia Record object</param>
      /// <param name="ceResultSet">Instantiated ResultSet object</param>
      /// <returns>False on exception raised, else true by default</returns>
      private bool WriteRecord( SavedMediaRecord iMedia,
          SqlCeResultSet ceResultSet )
      {
         // set the default result
         Boolean result = true;

         // create updatable record object
         SqlCeUpdatableRecord newRecord = ceResultSet.CreateRecord();

         try
         {
            //
            // populate record object
            // Note: We need to protect ORACLE from null fields!
            //
            newRecord[ mColumnOrdinal.Uid ] = iMedia.Uid;
            newRecord[ mColumnOrdinal.PointHostIndex ] = iMedia.PointHostIndex;
            newRecord[ mColumnOrdinal.WNID ] = iMedia.WNID;

            // don't allow a null time stamp
            if ( iMedia.TimeStamp.Ticks > 0 )
            {
               newRecord[ mColumnOrdinal.TimeStamp ] = iMedia.TimeStamp;
            }
            else
            {
               newRecord[ mColumnOrdinal.TimeStamp ] = PacketBase.DateTimeNow;
            }

            // this could be null so protect
            if ( iMedia.UTM != null )
            {
               newRecord[ mColumnOrdinal.UTM ] = iMedia.UTM;
            }

            newRecord[ mColumnOrdinal.MediaType ] = iMedia.MediaType;
            newRecord[ mColumnOrdinal.MediaOrigin ] = iMedia.MediaOrigin;

            if ( iMedia.FileName != null )
               newRecord[ mColumnOrdinal.FileName ] = iMedia.FileName;

            if ( iMedia.FilePath != null )
            {
               newRecord[ mColumnOrdinal.FilePath ] = iMedia.FilePath;
            }

            if ( iMedia.Note != null )
            {
               newRecord[ mColumnOrdinal.Note ] = iMedia.Note;
            }

            newRecord[ mColumnOrdinal.Uploaded ] = iMedia.Uploaded;
            newRecord[ mColumnOrdinal.OperId ] = iMedia.OperId;
            newRecord[ mColumnOrdinal.Size ] = iMedia.Size;

            if ( iMedia.CRC32 != null )
            {
               newRecord[ mColumnOrdinal.CRC32 ] = iMedia.CRC32;
            }

            //
            // add record to table and (if no errors) return true
            //
            ceResultSet.Insert( newRecord, DbInsertOptions.PositionOnInsertedRow );
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary1>
      /// Update a single SAVEDMEDIA table record
      /// </summary>
      /// <param name="iNodeRecord">Updated SavedMedia object to write</param>
      /// <param name="CeResultSet">Instantiated CeResultSet</param>
      /// <returns>True on success, else false</returns>
      private Boolean UpdateRecord( SavedMediaRecord iMedia,
          SqlCeResultSet CeResultSet )
      {
         Boolean result = true;
         try
         {
            CeResultSet.SetGuid( mColumnOrdinal.Uid, iMedia.Uid );
            CeResultSet.SetInt32( mColumnOrdinal.PointHostIndex, iMedia.PointHostIndex );
            CeResultSet.SetInt32( mColumnOrdinal.WNID, iMedia.WNID );

            if ( iMedia.TimeStamp != null )
            {
               CeResultSet.SetDateTime( mColumnOrdinal.TimeStamp, iMedia.TimeStamp );
            }

            if ( iMedia.UTM != null )
            {
               CeResultSet.SetString( mColumnOrdinal.UTM, iMedia.UTM );
            }

            CeResultSet.SetByte( mColumnOrdinal.MediaType, iMedia.MediaType );
            CeResultSet.SetByte( mColumnOrdinal.MediaOrigin, iMedia.MediaOrigin );

            if ( iMedia.FileName != null )
            {
               CeResultSet.SetString( mColumnOrdinal.FileName, iMedia.FileName );
            }

            if ( iMedia.FilePath != null )
            {
               CeResultSet.SetString( mColumnOrdinal.FilePath, iMedia.FilePath );
            }

            if ( iMedia.Note != null )
            {
               CeResultSet.SetString( mColumnOrdinal.Note, iMedia.Note );
            }

            CeResultSet.SetBoolean( mColumnOrdinal.Uploaded, iMedia.Uploaded );
            CeResultSet.SetInt32( mColumnOrdinal.Size, (Int32) iMedia.Size );

            if ( iMedia.CRC32 != null )
            {
               CeResultSet.SetString( mColumnOrdinal.CRC32, iMedia.CRC32 );
            }

            CeResultSet.Update();
         }
         catch
         {
            result = false;
         }
         return result;

      }/* end method */


      /// <summary>
      /// Populate an instantiated Saved Media Object with data from a 
      /// sinble SAVEDMEDIA table record. In the event of a raised exception 
      /// the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMedia">SavedMedia object to be populated</param>
      private void PopulateObject( SqlCeResultSet ceResultSet,
                                       ref SavedMediaRecord iMedia )
      {
         try
         {
            iMedia.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );

            // if there's no point host index then return -1 (off route)
            if ( ceResultSet[ mColumnOrdinal.PointHostIndex ] != DBNull.Value )
            {
               iMedia.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            }
            else
            {
               iMedia.PointHostIndex = -1;
            }

            // if there's no Work Notification ID then return -1 (Point or Machine)
            if ( ceResultSet[ mColumnOrdinal.WNID ] != DBNull.Value )
            {
               iMedia.WNID = ceResultSet.GetInt32( mColumnOrdinal.WNID );
            }
            else
            {
               iMedia.WNID = -1;
            }

            // there should always be a time stamp - but just in case!
            if ( ceResultSet[ mColumnOrdinal.TimeStamp ] != DBNull.Value )
            {
               iMedia.TimeStamp = ceResultSet.GetDateTime( mColumnOrdinal.TimeStamp );
            }
            else
            {
               iMedia.TimeStamp = PacketBase.DateTimeNow;
            }

            // if note field is null, then return an empty string
            if ( ceResultSet[ mColumnOrdinal.UTM ] != DBNull.Value )
            {
               iMedia.UTM = ceResultSet.GetString( mColumnOrdinal.UTM );
            }

            // if note field is null, then return an empty string
            if ( ceResultSet[ mColumnOrdinal.Note ] != DBNull.Value )
            {
               iMedia.Note = ceResultSet.GetString( mColumnOrdinal.Note );
            }
            else
            {
               iMedia.Note = string.Empty;
            }

            iMedia.MediaType = ceResultSet.GetByte( mColumnOrdinal.MediaType );
            iMedia.MediaOrigin = ceResultSet.GetByte( mColumnOrdinal.MediaOrigin );

            // protect against null file name (shouldn't ever happen)
            if ( ceResultSet[ mColumnOrdinal.FileName ] != DBNull.Value )
            {
               iMedia.FileName = ceResultSet.GetString( mColumnOrdinal.FileName );
            }
            else
            {
               iMedia.FileName = string.Empty;
            }

            // protect against null file path (shouldn't ever happen)
            if ( ceResultSet[ mColumnOrdinal.FilePath ] != DBNull.Value )
            {
               iMedia.FilePath = ceResultSet.GetString( mColumnOrdinal.FilePath );
            }
            else
            {
               iMedia.FilePath = string.Empty;
            }

            iMedia.Uploaded = ceResultSet.GetBoolean( mColumnOrdinal.Uploaded );
            iMedia.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );

            // if byte size is null, then return -1 (not saved)
            if ( ceResultSet[ mColumnOrdinal.Size ] != DBNull.Value )
            {
               iMedia.Size = ceResultSet.GetInt32( mColumnOrdinal.Size );
            }
            else
            {
               iMedia.Size = -1;
            }

            // protect against null CRC32 value (shouldn't ever happen)
            if ( ceResultSet[ mColumnOrdinal.CRC32 ] != DBNull.Value )
            {
               iMedia.CRC32 = ceResultSet.GetString( mColumnOrdinal.CRC32 );
            }
            else
            {
               iMedia.CRC32 = string.Empty;
            }
         }
         catch
         {
            iMedia = null;
         }

      }/* end method */


      /// <summary>
      /// Populate an instantiated Saved Media Object with data from a 
      /// single SAVEDMEDIA table record. In the event of a raised exception 
      /// the returned point object will be null.
      /// </summary>
      /// <param name="ceResultSet">Current CE ResultSet record</param>
      /// <param name="iMedia">SavedMedia object to be populated</param>
      private void PopulateUploadObject( SqlCeResultSet ceResultSet,
                                       ref UploadSavedMediaRecord iMedia )
      {
         try
         {
            // get the unique record identifier
            iMedia.Uid = ceResultSet.GetGuid( mColumnOrdinal.Uid );

            // if there's no point host index then return -1 (off route)
            if ( ceResultSet[ mColumnOrdinal.PointHostIndex ] != DBNull.Value )
            {
               iMedia.PointHostIndex = ceResultSet.GetInt32( mColumnOrdinal.PointHostIndex );
            }
            else
            {
               iMedia.PointHostIndex = -1;
            }

            // if there's no Work Notification ID then return -1 (Point or Machine)
            if ( ceResultSet[ mColumnOrdinal.WNID ] != DBNull.Value )
            {
               iMedia.WNID = ceResultSet.GetInt32( mColumnOrdinal.WNID );
            }
            else
            {
               iMedia.WNID = -1;
            }

            // there should always be a time stamp - but just in case!
            if ( ceResultSet[ mColumnOrdinal.TimeStamp ] != DBNull.Value )
            {
               iMedia.TimeStamp = ceResultSet.GetDateTime( mColumnOrdinal.TimeStamp );
            }
            else
            {
               iMedia.TimeStamp = PacketBase.DateTimeNow;
            }

            // if note field is null, then return an empty string
            if ( ceResultSet[ mColumnOrdinal.UTM ] != DBNull.Value )
            {
               iMedia.UTM = ceResultSet.GetString( mColumnOrdinal.UTM );
            }

            // if note field is null, then return an empty string
            if ( ceResultSet[ mColumnOrdinal.Note ] != DBNull.Value )
            {
               iMedia.Note = ceResultSet.GetString( mColumnOrdinal.Note );
            }
            else
            {
               iMedia.Note = string.Empty;
            }

            iMedia.MediaType = ceResultSet.GetByte( mColumnOrdinal.MediaType );
            iMedia.MediaOrigin = ceResultSet.GetByte( mColumnOrdinal.MediaOrigin );

            // protect against null file name (shouldn't ever happen)
            if ( ceResultSet[ mColumnOrdinal.FileName ] != DBNull.Value )
            {
               iMedia.FileName = ceResultSet.GetString( mColumnOrdinal.FileName );
            }
            else
            {
               iMedia.FileName = string.Empty;
            }

            iMedia.OperId = ceResultSet.GetInt32( mColumnOrdinal.OperId );

            // if byte size is null, then return -1 (not saved)
            if ( ceResultSet[ mColumnOrdinal.Size ] != DBNull.Value )
            {
               iMedia.Size = ceResultSet.GetInt32( mColumnOrdinal.Size );
            }
            else
            {
               iMedia.Size = -1;
            }

            // protect against null CRC32 value (shouldn't ever happen)
            if ( ceResultSet[ mColumnOrdinal.CRC32 ] != DBNull.Value )
            {
               iMedia.CRC32 = ceResultSet.GetString( mColumnOrdinal.CRC32 );
            }
            else
            {
               iMedia.CRC32 = string.Empty;
            }
         }
         catch
         {
            iMedia = null;
         }

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

   }/* end class */

}/* end namespace */


//-------------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 1.0 APinkerton 20th September 2011
//  New fields
//
//  Revision 0.0 APinkerton 3rd July 2009
//  Add to project
//-------------------------------------------------------------------------------