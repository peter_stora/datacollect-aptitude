﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2009 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   public class LinqQueries
   {
      /// <summary>
      /// Returns all NODE children for a given ParentNodeUid
      /// </summary>
      /// <param name="iEnumNodes">List of node objects to query</param>
      /// <param name="iParentNode">Current parent Node UID</param>
      /// <returns>Enumerated list, ordered by field "Number"</returns>
      public static EnumNodes GetChildNodes( EnumNodes iEnumNodes, Guid iParentNode )
      {
         // use LINQ to select points with new data
         IEnumerable<NodeRecord> nodes =
                        from node in iEnumNodes.NODE
                        where ( node.ParentUid == iParentNode )
                        orderby node.Number ascending
                        select node;

         // hydrate the LINQ return list
         EnumNodes nodesList = new EnumNodes();
         foreach ( NodeRecord node in nodes )
         {
            if ( node != null )
               nodesList.AddNodeRecord( node );
         }

         // and return 
         return nodesList;

      }/* end method */


      /// <summary>
      /// Returns an enumerable list of all ancestor nodes for a 
      /// selected point's Node Uid. The list is reverse-ordered 
      /// on the field HierarchyDepth. 
      /// </summary>
      /// <param name="iEnumNodes">List of node objects to query</param>
      /// <param name="iParentNode">Point's node UID</param>
      /// <returns>Enumerated list, reverse ordered by HierarchyDepth</returns>
      public static EnumNodes GetAllAncestors( EnumNodes iEnumNodes, Guid iParentNode )
      {
         // reference the parent UID
         Guid parentUid = iParentNode;

         // create a new results list
         EnumNodes nodesList = new EnumNodes();

         // reset the hierarchy depth counter
         int hierarchyDepth = 0;

         do
         {
            // query the nodes list
            IEnumerable<NodeRecord> nodes =
                            from node in iEnumNodes.NODE
                            where ( node.Uid == parentUid )
                            select node;

            // hydrate the LINQ result
            foreach ( NodeRecord ancestor in nodes )
            {
               if ( ancestor != null )
               {
                  // add the ancestor to the results
                  nodesList.AddNodeRecord( ancestor );

                  // get the depth and parent ID for this ancestor
                  hierarchyDepth = ancestor.HierarchyDepth;
                  parentUid = ancestor.ParentUid;
               }
               else
               {
                  hierarchyDepth = 0;
               }
            }
         } while ( hierarchyDepth > 0 );

         // pass back the hydrated ancestor list
         return nodesList;

      }/* end method */


      /// <summary>
      /// Returns a list of all available root Nodes
      /// </summary>
      /// <param name="iEnumNodes">List of node objects to query</param>
      /// <returns>Enumerated list, ordered by field "Number"</returns>
      public static EnumNodes GetRootNodes( EnumNodes iEnumNodes )
      {
         // use LINQ to select points with new data
         IEnumerable<NodeRecord> nodes =
                        from node in iEnumNodes.NODE
                        where ( node.HierarchyDepth == 0 )
                        orderby node.Number ascending
                        select node;

         // hydrate the LINQ return list
         EnumNodes nodesList = new EnumNodes();
         foreach ( NodeRecord node in nodes )
         {
            if ( node != null )
               nodesList.AddNodeRecord( node );
         }

         // and return 
         return nodesList;

      }/* end method */


      /// <summary>
      /// Returns all POINT children for a given Machine (NODE) UID.
      /// </summary>
      /// <param name="iEnumNodes">List of POINT objects to query</param>
      /// <param name="iParentNode">Machine (Node) UID</param>
      /// <returns>Enumerated list, ordered by field "Number"</returns>
      public static EnumPoints GetChildPoints( EnumPoints iEnumPoints, Guid iNodeUID )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( point.NodeUid == iNodeUID )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */


      /// <summary>
      /// This method will take an enumerated list of POINT objects and
      /// order them on the NUMBER field and in ascending order. 
      /// </summary>
      /// <param name="iEnumPoints">List of POINT objects to order</param>
      /// <returns>Ordered points list</returns>
      public static EnumPoints GetPointsOrderedByNumber( EnumPoints iEnumPoints )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */


      /// <summary>
      /// This method will take an enumerated list of NODE objects and
      /// order them on the NUMBER field and in ascending order. 
      /// </summary>
      /// <param name="iEnumNodes">List of NODE objects to order</param>
      /// <returns>Ordered points list</returns>
      public static EnumNodes GetNodesOrderedByNumber( EnumNodes iEnumNodes )
      {
         // use LINQ to select points with new data
         IEnumerable<NodeRecord> nodes =
                        from node in iEnumNodes.NODE
                        orderby node.Number ascending
                        select node;

         // hydrate the LINQ return list
         EnumNodes nodesList = new EnumNodes();
         foreach ( NodeRecord node in nodes )
         {
            if ( node != null )
               nodesList.AddNodeRecord( node );
         }

         // and return 
         return nodesList;

      }/* end method */


      /// <summary>
      /// This method will return a given node's siblings from a list of
      /// EnumNode objects. The returned list will be sorted in ascending
      /// order based based on the NUMBER field.
      /// </summary>
      /// <param name="iEnumNodes">Enumerated list of available nodes</param>
      /// <param name="iNode">Single node object to query against</param>
      /// <returns>Ordered nodes list</returns>
      public static EnumNodes GetSiblings( EnumNodes iEnumNodes, NodeRecord iNode )
      {
         // use LINQ to select points with new data
         IEnumerable<NodeRecord> nodes =
                        from node in iEnumNodes.NODE
                        where ( node.ParentUid == iNode.ParentUid )
                        orderby node.Number ascending
                        select node;

         // hydrate the LINQ return list
         EnumNodes nodesList = new EnumNodes();
         foreach ( NodeRecord node in nodes )
         {
            if ( node != null )
               nodesList.AddNodeRecord( node );
         }

         // and return 
         return nodesList;

      }/* end method */


      /// <summary>
      /// This method will return an ordered list of points which are  
      /// set as DANGER and have new measurements saved against them. 
      /// The returned list will be sorted in ascending order, based on
      /// the NUMBER field.
      /// </summary>
      /// <param name="iEnumPoints">reference points list to query</param>
      /// <returns>ordered points list</returns>
      public static EnumPoints PointAlarmFilterIsDanger( EnumPoints iEnumPoints )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( ( point.AlarmState == (byte) FieldTypes.NodeAlarmState.Danger ) &&
                               ( point.DataSaved == true ) )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */


      /// <summary>
      /// This method will return an ordered list of points which are  
      /// set as ALERT and have new measurements saved against them. 
      /// The returned list will be sorted in ascending order, based on
      /// the NUMBER field.
      /// </summary>
      /// <param name="iEnumPoints">reference points list to query</param>
      /// <returns>ordered points list</returns>
      public static EnumPoints PointAlarmFilterIsAlert( EnumPoints iEnumPoints )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( ( point.AlarmState == (byte) FieldTypes.NodeAlarmState.Alert ) &&
                               ( point.DataSaved == true ) )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */


      /// <summary>
      /// This method will return a list of points which have collected 
      /// measurements that are "currenly" in either Alarm or Danger state,
      /// including "Hi" and "Low". The returned list will be sorted in 
      /// ascending order based based on the NUMBER field.
      /// </summary>
      /// <param name="iEnumPoints">reference points list to query</param>
      /// <param name="iAlarmState">alarm state to query against</param>
      /// <returns>ordered points list</returns>
      public static EnumPoints PointFilterAnyAlarm( EnumPoints iEnumPoints )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( (
                              ( point.AlarmState == (byte) FieldTypes.NodeAlarmState.Danger ) |
                              ( point.AlarmState == (byte) FieldTypes.NodeAlarmState.Alert ) ) &&
                              ( point.DataSaved == true ) )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */

      //-------------------------------------------------------------------------

      #region Overdue Compliance filter support

      /// <summary>
      /// This method will return a list of points which are overdue for collection
      /// based on the points "SheduleDays", the current time and when the point
      /// was last modified
      /// </summary>
      /// <param name="iEnumPoints">The unfiltered points</param>
      /// <returns>The overdue points</returns>
      public static EnumPoints PointFilterOverdue( EnumPoints iEnumPoints )
      {
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( point.IsOverdue() )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */


      /// <summary>
      /// Returns all overdue POINT children for a given Machine (NODE) UID.
      /// </summary>
      /// <param name="iEnumNodes">List of POINT objects to query</param>
      /// <param name="iParentNode">Machine (Node) UID</param>
      /// <returns>Enumerated list, ordered by field "Number"</returns>
      public static EnumPoints GetOverdueChildPoints( EnumPoints iEnumPoints, Guid iNodeUID )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( point.NodeUid == iNodeUID && point.IsOverdue() ) 
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         return pointsList;

      }/* end method */

      #endregion

      //-------------------------------------------------------------------------

      /// <summary>
      /// Returns a single Node record from the reference points list
      /// </summary>
      /// <param name="iEnumPoints">Reference nodes list</param>
      /// <param name="iPointUID">Node Uid lookup</param>
      /// <returns>Populated Node record</returns>
      public static NodeRecord GetNodeRecord( EnumNodes iEnumNodes, Guid iNodeUid )
      {
         // use LINQ to select points with new data
         IEnumerable<NodeRecord> nodes =
                        from point in iEnumNodes.NODE
                        where ( point.Uid == iNodeUid )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumNodes nodesList = new EnumNodes();
         foreach ( NodeRecord point in nodes )
         {
            if ( point != null )
               nodesList.AddNodeRecord( point );
         }

         // and return 
         if ( nodesList.NODE.Count > 0 )
         {
            return nodesList.NODE[ 0 ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Returns a single point record from the reference points list
      /// </summary>
      /// <param name="iEnumPoints">Reference points list</param>
      /// <param name="iPointUID">Point Uid lookup</param>
      /// <returns>Populated Point record</returns>
      public static PointRecord GetPointRecord( EnumPoints iEnumPoints, Guid iPointUID )
      {
         // use LINQ to select points with new data
         IEnumerable<PointRecord> points =
                        from point in iEnumPoints.POINTS
                        where ( point.Uid == iPointUID )
                        orderby point.Number ascending
                        select point;

         // hydrate the LINQ return list
         EnumPoints pointsList = new EnumPoints();
         foreach ( PointRecord point in points )
         {
            if ( point != null )
               pointsList.AddPointsRecord( point );
         }

         // and return 
         if ( pointsList.POINTS.Count > 0 )
         {
            return pointsList.POINTS[ 0 ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Get's the ProcessRecord for a particular Process Point
      /// </summary>
      /// <param name="iPoint">The PointRecord that you want a ProcessRecord for</param>
      /// <returns>The Point's ProcessRecord</returns>
      public static ProcessRecord GetPointsProcessRecord( EnumProcess iEnumProcess, Guid iPointUid )
      {
         // use LINQ to select points with new data
         IEnumerable<ProcessRecord> processRecords =
                        from process in iEnumProcess.Process
                        where ( process.PointUid == iPointUid )
                        select process;

         // hydrate the LINQ return list
         EnumProcess processList = new EnumProcess();
         foreach ( ProcessRecord process in processRecords )
         {
            if ( process != null )
               processList.AddProcessRecord( process );
         }

         // and return 
         if ( processList.Process.Count > 0 )
         {
            return processList.Process[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Get's the TextData Record for a particular TextData Point
      /// </summary>
      /// <param name="iPoint">The PointRecord that you want a ProcessRecord for</param>
      /// <returns>The Point's TextData Record</returns>
      public static TextDataRecord GetPointsTextDataRecord( EnumTextData iEnumTextData, Guid iPointUid )
      {
         // use LINQ to select points with new data
         IEnumerable<TextDataRecord> textDataRecords =
                        from process in iEnumTextData.TextData
                        where ( process.PointUid == iPointUid )
                        select process;

         // hydrate the LINQ return list
         EnumTextData textDataList = new EnumTextData();
         foreach ( TextDataRecord text in textDataRecords )
         {
            if ( text != null )
               textDataList.AddTextDataRecord( text );
         }

         // and return 
         if ( textDataList.TextData.Count > 0 )
         {
            return textDataList.TextData[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Get's the FFTPointRecord for a particular Process Point
      /// </summary>
      /// <param name="iPoint">The PointRecord that you want a ProcessRecord for</param>
      /// <returns>Populated FFTPoint record - or null</returns>
      public static FFTPointRecord GetFFTPointRecord( EnumFFTPoints iEnumFFTPoints, Guid iPointUid )
      {
         // use LINQ to select points with new data
         IEnumerable<FFTPointRecord> fftPointRecords =
                        from fftPoint in iEnumFFTPoints.FFTPoint
                        where ( fftPoint.PointUid == iPointUid )
                        select fftPoint;

         // hydrate the LINQ return list
         EnumFFTPoints fftPointsList = new EnumFFTPoints();
         foreach ( FFTPointRecord process in fftPointRecords )
         {
            if ( process != null )
               fftPointsList.AddFFTPointRecord( process );
         }

         // and return 
         if ( fftPointsList.FFTPoint.Count > 0 )
         {
            return fftPointsList.FFTPoint[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Get's the InspectionRecord for a particular Inspection Point
      /// </summary>
      /// <param name="iPoint">The PointRecord that you want an InspectionRecord for</param>
      /// <returns>The Point's InspectionRecord</returns>
      public static InspectionRecord GetPointsInspectionRecord( EnumInspection iEnumInspection, Guid iPointUid )
      {
         // use LINQ to select points with new data
         IEnumerable<InspectionRecord> inspectionRecords =
                        from inspection in iEnumInspection.Inspection
                        where ( inspection.PointUid == iPointUid )
                        select inspection;

         // hydrate the LINQ return list
         EnumInspection inspectionList = new EnumInspection();
         foreach ( InspectionRecord inspection in inspectionRecords )
         {
            if ( inspection != null )
               inspectionList.AddInspectionRecord( inspection );
         }

         // and return 
         if ( inspectionList.Inspection.Count > 0 )
         {
            return inspectionList.Inspection[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Get's the MCCRecord for a particular MCD Point
      /// </summary>
      /// <param name="iPoint">The PointRecord that you want an MCCRecord for</param>
      /// <returns>The Point's MCCRecord</returns>
      public static MCCRecord GetPointsMCCRecord( EnumMCC iEnumMCC, Guid iPointUid )
      {
         // use LINQ to select points with new data
         IEnumerable<MCCRecord> mccRecords =
                        from mcc in iEnumMCC.MCC
                        where ( mcc.PointUid == iPointUid )
                        select mcc;

         // hydrate the LINQ return list
         EnumMCC mccList = new EnumMCC();
         foreach ( MCCRecord mcc in mccRecords )
         {
            if ( mcc != null )
               mccList.AddMCCRecord( mcc );
         }

         // and return 
         if ( mccList.MCC.Count > 0 )
         {
            return mccList.MCC[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Return the most recent measurement from an emumerated list of measurements
      /// </summary>
      /// <param name="iMeasurements">Enumerated list of measurements</param>
      /// <returns>Single point measurement object</returns>
      public static PointMeasurement GetLatestMeasurement( EnumPointMeasurements iMeasurements )
      {
         // use LINQ to select points with new data
         IEnumerable<PointMeasurement> measurements =
                        from measurement in iMeasurements.Measurement
                        orderby measurement.LocalTime descending
                        select measurement;

         // hydrate the LINQ return list
         EnumPointMeasurements measurementsEnum = new EnumPointMeasurements();
         foreach ( PointMeasurement measurement in measurements )
         {
            if ( measurement != null )
               measurementsEnum.AddMeasurement( measurement );
         }

         // and return 
         if ( measurementsEnum.Measurement.Count > 0 )
         {
            return measurementsEnum.Measurement[ 0 ];
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      /// Return the most recent measurement from a basic list of measurements
      /// </summary>
      /// <param name="iMeasurements">Basic list of measurements</param>
      /// <returns>Single point measurement object</returns>
      public static PointMeasurement GetLatestMeasurement( EnumMeasurementRecords iMeasurements )
      {
         // add IEnumerable support to the measurements list
         EnumPointMeasurements measurementsEnum = new EnumPointMeasurements();
         for ( int i=0; i < iMeasurements.Count; ++i )
         {
            measurementsEnum.AddMeasurement( iMeasurements.Measurement[ i ] );
         }

         // get the most recent measurement
         return GetLatestMeasurement( measurementsEnum );

      }/* end method */


      /// <summary>
      ///// Returns a fixed size enumeration of the most recent measurements 
      ///// from a basic list of all available measurements.
      /// </summary>
      /// <param name="iMeasurements">Basic list of measurements</param>
      /// <param name="iMeasurementCount">How many measurements to return</param>
      /// <returns>Enumerated list</returns>
      public static EnumPointMeasurements GetLatestMeasurements(
         EnumPointMeasurements iMeasurements, int iMeasurementCount )
      {
         // use LINQ to select points with new data
         IEnumerable<PointMeasurement> measurements =
                        from measurement in iMeasurements.Measurement
                        orderby measurement.LocalTime descending
                        select measurement;

         // hydrate the LINQ return list
         EnumPointMeasurements measurementsEnum = new EnumPointMeasurements();

         int count = 0;
         foreach ( PointMeasurement measurement in measurements )
         {
            if ( measurement != null )
            {
               measurementsEnum.AddMeasurement( measurement );
               ++count;
            }
            if ( count >= iMeasurementCount )
               break;
         }

         // and return 
         if ( measurementsEnum.Measurement.Count > 0 )
         {
            return measurementsEnum;
         }
         else
         {
            return null;
         }
      }/* end method */


      /// <summary>
      ///// Returns a fixed size enumeration of the most recent measurements 
      ///// from a basic list of all available measurements.
      /// </summary>
      /// <param name="iMeasurements">Basic list of measurements</param>
      /// <param name="iMeasurementCount">How many measurements to return</param>
      /// <returns>Enumerated list</returns>
      public static EnumPointMeasurements GetLatestMeasurements(
         EnumMeasurementRecords iMeasurements, int iMeasurementCount )
      {
         // add IEnumerable support to the measurements list
         EnumPointMeasurements measurementsEnum = new EnumPointMeasurements();
         for ( int i=0; i < iMeasurements.Count; ++i )
         {
            measurementsEnum.AddMeasurement( iMeasurements.Measurement[ i ] );
         }

         // get the most recent measurement
         return GetLatestMeasurements( measurementsEnum, iMeasurementCount );

      }/* end method */


      /// <summary>
      /// Returns an enumerable list of Notes records for a given Point Uid
      /// </summary>
      /// <param name="iEnumNotes">raw list of available notes</param>
      /// <param name="iPointUid">Point Uid to search on</param>
      /// <returns>filtered list of Notes records</returns>
      public static EnumNotes GetNotesForPoint( EnumNotes iEnumNotes, Guid iPointUid )
      {
         // create a new results list
         EnumNotes notesList = new EnumNotes();

         // query the nodes list
         IEnumerable<NotesRecord> notes =
                            from note in iEnumNotes.NOTE
                            where ( note.PointUid == iPointUid )
                            select note;

         // hydrate the LINQ result
         foreach ( NotesRecord thisNote in notes )
         {
            if ( thisNote != null )
            {
               // add the ancestor to the results
               notesList.AddNoteRecord( thisNote );
            }
         }

         // pass back the hydrated ancestor list
         return notesList;

      }/* end method */


      /// <summary>
      /// Returns an enumerable list of Coded Notes records for a given Note Uid
      /// </summary>
      /// <param name="iEnumNotes">raw list of available notes</param>
      /// <param name="iPointUid">Point Uid to search on</param>
      /// <returns>filtered list of Coded Notes records</returns>
      public static EnumSelectedCodedNotes GetCodedNotesForNoteUid(
         EnumSelectedCodedNotes iEnumNotes, Guid NoteUid )
      {
         // create a new results list
         EnumSelectedCodedNotes codedNotesList = new EnumSelectedCodedNotes();

         // query the nodes list
         IEnumerable<SelectedCodedNote> notes =
                            from note in iEnumNotes.CodedNote
                            where ( note.NoteUid == NoteUid )
                            select note;

         // hydrate the LINQ result
         foreach ( SelectedCodedNote thisNote in notes )
         {
            if ( thisNote != null )
            {
               // add the ancestor to the results
               codedNotesList.AddCodedNoteRecord( thisNote );
            }
         }

         // pass back the hydrated ancestor list
         return codedNotesList;

      }/* end method */


      /// <summary>
      /// Returns a Conditional POINT record for a given POINT if it exists
      /// </summary>
      /// <param name="iEnumConditionalPoints">raw list of available conditional points</param>
      /// <param name="iPoint">The POINT Record</param>
      /// <returns>A Conditional POINT Record if it exists</returns>
      public static ConditionalPointRecord GetConditionalPointRecord(
         EnumConditionalPoints iEnumConditionalPoints, PointRecord iPoint )
      {
         // create a new result list
         EnumConditionalPoints cPointList = new EnumConditionalPoints();

         IEnumerable<ConditionalPointRecord> cPoints =
                           from cPoint in iEnumConditionalPoints.ConditionalPoint
                           where ( cPoint.ConditionalPointNumber == iPoint.PointHostIndex )
                           select cPoint;

         // hydrate the LINQ result
         foreach ( ConditionalPointRecord thisCPoint in cPoints )
         {
            if ( thisCPoint != null )
            {
               // add the Conditional POINT to the results
               cPointList.AddConditionalPointRecord( thisCPoint );
            }
         }

         // if we have a record, return it
         if ( cPointList.Count > 0 )
         {
            return cPointList.ConditionalPoint[ 0 ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Returns a PointRecord for a given PointHostIndex value, if it exists
      /// </summary>
      /// <param name="iEnumPointRecords">raw list of available points</param>
      /// <param name="iPointHostIndex">The Point Record's PointHostIndex value</param>
      /// <param name="iRouteId">The ROUTE's POINT Id</param>
      /// <returns>The PointRecord if it exists, else null</returns>
      public static PointRecord GetPointRecord( EnumPointRecords iEnumPointRecords,
         int iPointHostIndex, int iRouteId )
      {
         // create a new result list
         EnumPointRecords pointList = new EnumPointRecords();

         IEnumerable<PointRecord> points =
                           from point in iEnumPointRecords.POINTS
                           where ( ( point.PointHostIndex == iPointHostIndex ) && ( point.RouteId == iRouteId ) )
                           select point;

         // hydrate the LINQ result
         foreach ( PointRecord thisPoint in points )
         {
            if ( thisPoint != null )
            {
               pointList.AddPointsRecord( thisPoint );
            }
         }

         // if we have a record, return it
         if ( pointList.Count > 0 )
         {
            return pointList.POINTS[ 0 ];
         }
         else
         {
            return null;
         }

      }/* end method */


      /// <summary>
      /// Get a Point Record from a POINT's location tag
      /// </summary>
      /// <param name="iEnumPointRecords">raw list of available points</param>
      /// <param name="iLocationTag">The location tag</param>
      /// <returns>The POINT Record(s) if it exists</returns>
      public static EnumPoints GetPointFromTag
         (
            EnumPoints iEnumPoints,
            string iLocationTag
         )
      {
         // create a new result list
         EnumPoints pointList = new EnumPoints();

         IEnumerable<PointRecord> points =
                           from point in iEnumPoints.POINTS
                           where ( ( point.LocationMethod != (byte) FieldTypes.LocationType.None ) &&
                                   ( point.LocationTag == iLocationTag ) )
                           select point;

         // hydrate the LINQ result
         foreach ( PointRecord thisPoint in points )
         {
            if ( thisPoint != null )
            {
               pointList.AddPointsRecord( thisPoint );
            }
         }

         return pointList;

      }/* end method */


      /// <summary>
      /// Return a list of points with data saved against them
      /// </summary>
      /// <param name="iEnumPointRecords">raw list of available points</param>
      /// <returns>The POINT Record(s) if it exists</returns>
      public static EnumPoints GetPointsWithNewData(EnumPoints iEnumPoints)
      {
         // create a new result list
         EnumPoints pointList = new EnumPoints();

         IEnumerable<PointRecord> points =
                           from point in iEnumPoints.POINTS
                           where point.DataSaved == true
                           select point;

         // hydrate the LINQ result
         foreach ( PointRecord thisPoint in points )
         {
            if ( thisPoint != null )
            {
               pointList.AddPointsRecord( thisPoint );
            }
         }

         return pointList;

      }/* end method */


      /// <summary>
      /// Get a Node Record from a Node's location tag
      /// </summary>
      /// <param name="iEnumPoints">Reference nodes list</param>
      /// <param name="iLocationTag">The location tag</param>
      /// <returns>The Node Record(s) if it exists</returns>
      public static EnumNodes GetNodeFromTag
         (
            EnumNodes iEnumNodes,
            string iLocationTag
         )
      {
         // create a new result list
         EnumNodes nodeList = new EnumNodes();

         IEnumerable<NodeRecord> nodes =
                           from node in iEnumNodes.NODE
                           where ( ( node.LocationMethod != (byte) FieldTypes.LocationType.None ) &&
                                   ( node.LocationTag == iLocationTag ) )
                           select node;

         // hydrate the LINQ result
         foreach ( NodeRecord thisNode in nodes )
         {
            if ( thisNode != null )
            {
               nodeList.AddNodeRecord( thisNode );
            }
         }

         return nodeList;

      }/* end method */


      /// <summary>
      /// Check if a tag is unique in the database
      /// </summary>
      /// <param name="iEnumPoints">Reference nodes list</param>
      /// <param name="iEnumPoints">raw list of available points</param>
      /// <param name="iLocationTag">The location tag</param>
      /// <returns>True if the tag alreadys exists, else false</returns>
      public static bool LocationTagIsUnique
         (
            EnumNodes iEnumNodes,
            EnumPoints iEnumPoints,
            string iLocationTag
         )
      {
         // Check the nodes first...

         // create a new result list
         EnumNodes nodeList = new EnumNodes();

         IEnumerable<NodeRecord> nodes =
                           from node in iEnumNodes.NODE
                           where ( ( node.LocationMethod != (byte) FieldTypes.LocationType.None ) &&
                                   ( node.LocationTag == iLocationTag ) )
                           select node;

         // hydrate the LINQ result
         foreach ( NodeRecord thisNode in nodes )
         {
            if ( thisNode != null )
            {
               nodeList.AddNodeRecord( thisNode );
            }
         }

         // Then check the points...

         // create a new result list
         EnumPoints pointList = new EnumPoints();

         IEnumerable<PointRecord> points =
                           from point in iEnumPoints.POINTS
                           where ( ( point.LocationMethod != (byte) FieldTypes.LocationType.None ) &&
                                   ( point.LocationTag == iLocationTag ) )
                           select point;

         // hydrate the LINQ result
         foreach ( PointRecord thisPoint in points )
         {
            if ( thisPoint != null )
            {
               pointList.AddPointsRecord( thisPoint );
            }
         }

         // if no POINTs of Nodes exists, this tag is unique
         if ( ( pointList.Count == 0 ) && ( nodeList.Count == 0 ) )
         {
            return true;
         }
         else
         {
            return false;
         }

      }/* end method */


      /// <summary>
      /// Get the name of a ROUTE based on an ID
      /// </summary>
      /// <param name="iEnumPoints">Reference nodes list</param>
      /// <param name="iRouteID">The ROUTE ID</param>
      /// <returns>The name of the ROUTE, string.Empty if not found</returns>
      public static string GetRouteName( EnumNodes iEnumNodes, int iRouteID )
      {
         // create a new result list
         EnumNodes nodeList = new EnumNodes();

         IEnumerable<NodeRecord> nodes =
                           from node in iEnumNodes.NODE
                           where ( ( node.RouteId == iRouteID ) &&
                                   ( node.HierarchyDepth == 0 ) )
                           select node;

         // hydrate the LINQ result
         foreach ( NodeRecord thisNode in nodes )
         {
            if ( thisNode != null )
            {
               nodeList.AddNodeRecord( thisNode );
            }
         }

         // if we have a record, return it
         if ( nodeList.Count > 0 )
         {
            return nodeList.NODE[ 0 ].Id;
         }
         else
         {
            return string.Empty;
         }

      }/* end method */

   }/*end class */

}/* end namespace */

//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 9th October 2009
//  Add to project
//----------------------------------------------------------------------------