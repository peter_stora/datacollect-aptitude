﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2010 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlServerCe;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// Structure containing the ordinal positions of each field within
   /// the LOCALSETTINGS table. This structure should be revised following 
   /// any changes made to this table.
   /// </summary>
   public class RowOrdinalsUserPreferences
   {
      public int OperId
      {
         get;
         set;
      }

      public int Changed
      {
         get;
         set;
      }

      public int HKCU
      {
         get;
         set;
      }

      /// <summary>
      /// Constructor (no overloads)
      /// </summary>
      /// <param name="iCeResultSet"></param>
      public RowOrdinalsUserPreferences( SqlCeResultSet iCeResultSet )
      {
         GetTableColumns( iCeResultSet );
      }/* end constructor */


      /// <summary>
      /// This method populates and returns the column ordinals associated
      /// with the LOCALSETTINGS table. This table will need to be revised
      /// in the event of any changes being made to the LOCALSETTINGS table 
      /// field structure.
      /// </summary>
      /// <param name="ceResultSet">CE Results Set object to reference</param>
      /// <returns>populated structure of column enumerable</returns>
      private void GetTableColumns( SqlCeResultSet iCeResultSet )
      {
         this.OperId = iCeResultSet.GetOrdinal( "OperId" );
         this.Changed = iCeResultSet.GetOrdinal( "Changed" );
         this.HKCU = iCeResultSet.GetOrdinal( "HKCU" );

      }/* end method */

   }/* end class */

}/* end namespace*/


//----------------------------------------------------------------------------
//  $<Version Control Keyword>$
//
//  Revision 0.0 APinkerton 3rd March 2010
//  Add to project
//----------------------------------------------------------------------------

