﻿//----------------------------------------------------------------------------
//
// This file and its contents are the property of SKF Reliability Systems
// Copyright © 2012 by SKF Reliability Systems
// ALL RIGHTS RESERVED.
//
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.RS.MicrologInspector.Database
{
   /// <summary>
   /// This class provides simulated objects for Database load testing
   /// </summary>
   public static class TestData
   {
      #region Public Methods
      /// <summary>
      /// Builds and returns an emumerated list of Random NODE records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumNodes Build_Nodes( int iCount )
      {
         // create a new Nodes list
         EnumNodes random = new EnumNodes();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddNodeRecord( Create_Node( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random POINTS records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumPoints Build_Points( int iCount )
      {
         // create a new Nodes list
         EnumPoints random = new EnumPoints();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddPointsRecord( Create_Point( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random PROCESS records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumProcess Build_Process( int iCount )
      {
         // create a new Nodes list
         EnumProcess random = new EnumProcess();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddProcessRecord( Create_Process( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random INSPECTION records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumInspection Build_Inspection( int iCount )
      {
         // create a new Nodes list
         EnumInspection random = new EnumInspection();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddInspectionRecord( Create_Inspection( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random MCC records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumMCC Build_MCC( int iCount )
      {
         // create a new Nodes list
         EnumMCC random = new EnumMCC();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddMCCRecord( Create_MCC( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random TEXTDATA records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumTextData Build_TextData( int iCount )
      {
         // create a new Nodes list
         EnumTextData random = new EnumTextData();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddTextDataRecord( Create_TextData( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random FFTPOINT records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumFFTPoints Build_FFTPoints( int iCount )
      {
         // create a new Nodes list
         EnumFFTPoints random = new EnumFFTPoints();

         // the returning parent!
         Guid parent = new Guid();

         // populate with ten records
         for ( int i=0; i < iCount; ++i )
         {
            random.AddFFTPointRecord( Create_FFTPoint( i, parent, out parent ) );
         }

         // and pass back!
         return random;
      }


      /// <summary>
      /// Builds and returns an emumerated list of Random FFTCHANNEL records
      /// with a full hierarchy (including the base node)
      /// </summary>
      /// <param name="iCount"></param>
      /// <returns></returns>
      public static EnumFFTChannelRecords Build_FFTChannels( int iCount )
      {
         // set a common time stamp
         DateTime collectedAt = PacketBase.DateTimeNow;

         // create the FFT channel object
         EnumFFTChannelRecords fftChannels = new EnumFFTChannelRecords();

         // current and point identifiers
         Guid pointUid = Guid.NewGuid();

         // populate with velocity and envelope data
         for ( int i=0; i < iCount; ++i )
         {
            // add a pre-recorded velocity spectrum 
            fftChannels.AddFFTChannelRecord( Populate_Dummy_Velocity_Channel( i, pointUid, collectedAt ) );

            // add a pre-recorded enveloping spectrum
            fftChannels.AddFFTChannelRecord( Populate_Dummy_Envelope_Channel( i, pointUid, collectedAt ) );
         }

         return fftChannels;

      }/* end method */


      /// <summary>
      /// Create and populate a dummy FFT Channel object with Velocity Data
      /// </summary>
      /// <param name="iIdentifier">POINT ID (PRISM No)</param>
      /// <param name="iPointUid">UID of parent POINT</param>
      /// <param name="iCollectedAt">Collection Time</param>
      /// <returns>Populated channel object</returns>
      public static FFTChannelRecord Populate_Dummy_Velocity_Channel( int iIdentifier, Guid iPointUid, DateTime iCollectedAt )
      {
         FFTChannelRecord fftChannel = new FFTChannelRecord();

         fftChannel.Uid = Guid.NewGuid();
         fftChannel.PointUid = iPointUid;
         fftChannel.FFTPointUid = iPointUid;
         fftChannel.PointHostIndex = iIdentifier;

         int[] myArray = new int[] 
            { 0, 0, 202, 758, 171, 151, 200, 225, 194, 621, 2178, 22268, 28877, 8876, 666, 168, 
                185, 130, 47, 187, 387, 333, 917, 6672, 7256, 2029, 364, 124, 133, 72, 44, 38, 97, 
                263, 373, 1146, 1824, 965, 251, 97, 194, 96, 20, 65, 153, 705, 1527, 5183, 3425, 
                941, 534, 128, 117, 30, 170, 255, 179, 327, 550, 858, 1157, 522, 106, 190, 230, 153, 
                90, 190, 462, 669, 2936, 4881, 2345, 616, 316, 220, 40, 46, 46, 146, 220, 293, 718, 
                250, 185, 185, 55, 94, 104, 73, 50, 63, 205, 325, 835, 490, 86, 96, 70, 106, 89, 37, 
                55, 47, 235, 194, 288, 269, 185, 109, 33, 80, 37, 13, 81, 73, 131, 181, 550, 388, 200, 
                109, 76, 41, 18, 25, 41, 41, 11, 99, 120, 67, 41, 48, 33, 34, 50, 44, 25, 63, 86, 153, 
                190, 84, 22, 13, 11, 2, 4, 20, 33, 44, 40, 65, 43, 70, 80, 60, 63, 76, 66, 18, 31, 18, 
                50, 8, 86, 99, 79, 40, 17, 37, 34, 44, 25, 50, 57, 22, 25, 63, 41, 2, 109, 220, 122, 44, 
                61, 73, 152, 250, 153, 104, 61, 55, 66, 78, 43, 14, 25, 81, 73, 22, 21, 6, 40, 35, 11, 
                40, 11, 6, 66, 55, 187, 158, 113, 44, 12, 45, 33, 77, 53, 21, 103, 143, 68, 86, 149, 74, 
                61, 115, 155, 93, 17, 6, 21, 83, 67, 22, 40, 103, 156, 128, 91, 52, 92, 97, 122, 120, 
                149, 187, 137, 28, 52, 2, 30, 2, 22, 130, 140, 179, 223, 236, 138, 18, 18, 1, 52, 47, 
                20, 73, 109, 115, 140, 70, 15, 35, 39, 31, 45, 40, 50, 89, 100, 164, 126, 48, 40, 72, 
                72, 28, 26, 9, 33, 83, 63, 61, 59, 38, 21, 25, 25, 45, 30, 34, 54, 4, 58, 54, 35, 27, 12, 
                4, 7, 13, 9, 19, 39, 54, 11, 45, 51, 12, 2, 15, 35, 26, 7, 17, 41, 17, 46, 46, 47, 35, 30, 
                15, 24, 32, 5, 26, 31, 19, 31, 32, 21, 9, 19, 8, 28, 19, 4, 12, 6, 35, 41, 31, 2, 8, 14, 13, 
                6, 9, 15, 26, 37, 28, 19, 24, 14, 14, 7, 14, 22, 19, 7, 15, 8, 14, 24, 15, 13, 6, 6, 8, 17, 
                4, 26, 31, 19, 9, 17, 15, 6, 11, 15, 14, 5, 7, 2, 9 };

         fftChannel.AddDataLines( myArray );
         fftChannel.ChannelId = (byte) FieldTypes.FFTChannelId.Velocity;
         fftChannel.CompressionType = 2;
         fftChannel.DataType = 0;
         fftChannel.Timestamp = iCollectedAt;
         fftChannel.NumberOfLines = 400;
         fftChannel.Factor = 0.0002;
         fftChannel.LowFreq = 0;
         fftChannel.HiFreq = 1000;
         fftChannel.NumberOfAverages = 1;
         fftChannel.Units = 0;

         fftChannel.IsOverloaded = Random_Boolean();
         fftChannel.Load = 0;
         fftChannel.Offset = Random_Byte( 4 );
         fftChannel.SerialNo = "WMCD001";
         fftChannel.SpecNum = Random_Byte( 2 );
         fftChannel.SpeedOrPPF = Random_Byte( 2 );
         fftChannel.UserIndex = Random_Byte( 20 );
         fftChannel.Windowing = Random_Byte( 3 );

         return fftChannel;

      }/* end method */


      /// <summary>
      /// Create and populate a dummy FFT Channel object with Envelope Data
      /// </summary>
      /// <param name="iIdentifier">POINT ID (PRISM No)</param>
      /// <param name="iPointUid">UID of parent POINT</param>
      /// <param name="iCollectedAt">Collection Time</param>
      /// <returns>Populated channel object</returns>
      public static FFTChannelRecord Populate_Dummy_Envelope_Channel( int iIdentifier, Guid iPointUid, DateTime iCollectedAt )
      {
         FFTChannelRecord fftChannel = new FFTChannelRecord();

         fftChannel.Uid = Guid.NewGuid();
         fftChannel.PointUid = iPointUid;
         fftChannel.FFTPointUid = iPointUid;
         fftChannel.PointHostIndex = iIdentifier;

         int[] myArray = new int[] { 
                0, 0, 8106, 5487, 6250, 9611, 14588, 12288, 7994, 9333, 6834, 10580, 14476, 
                8057, 2893, 2860, 3017, 4725, 6353, 4219, 2379, 3013, 5168, 22972, 27920, 
                11310, 4679, 3987, 3867, 3344, 8869, 9229, 4754, 3473, 2905, 6643, 5843, 3970, 
                4269, 8297, 8110, 4820, 3241, 3025, 3560, 2715, 12105, 24095, 13345, 2656, 
                5131, 8421, 6374, 4509, 2955, 4530, 3788, 5056, 9540, 14977, 12740, 5740, 2097, 
                2822, 5777, 6834, 4575, 5338, 5296, 920, 22582, 27775, 10178, 2851, 2076, 4169, 
                4074, 6436, 4389, 2988, 2603, 5114, 10315, 11210, 8732, 5823, 2611, 1127, 4198, 
                4633, 2586, 2089, 3995, 6299, 11036, 7364, 3315, 3614, 2250, 240, 526, 1911, 
                3038, 3639, 2313, 2134, 4729, 5234, 4161, 4012, 3224, 1720, 1625, 3303, 2109, 
                1853, 2818, 4754, 4111, 2346, 2582, 3498, 3216, 2876, 2441, 1310, 2412, 1757, 
                4177, 4973, 4509, 4637, 3344, 692, 883, 1857, 758, 1509, 1181, 1745, 4165, 7103, 
                3291, 1687, 1587, 2134, 2122, 2458, 2031, 966, 1239, 2010, 2325, 2955, 3618, 2561, 
                2238, 1591, 1513, 1890, 2167, 2942, 1857, 2043, 4376, 3535, 1343, 1674, 2429, 1828, 
                1405, 680, 1500, 1765, 1790, 1351, 1894, 3759, 5023, 3846, 1314, 1144, 1906, 1438, 
                932, 564, 2126, 3647, 3983, 3494, 2201, 1869, 2138, 1061, 1363, 932, 1554, 1670, 
                2777, 1890, 1886, 1575, 1567, 1152, 734, 1678, 1687, 775, 1741, 2325, 2980, 5301, 
                3738, 2143, 3626, 2984, 588, 1637, 2105, 2118, 1057, 746, 1078, 1998, 1981, 1625, 
                1956, 1044, 783, 1032, 1504, 1451, 1380, 1956, 2433, 2387, 1898, 1455, 2076, 2395, 
                1782, 874, 1616, 1451, 953, 1156, 1409, 1322, 1380, 1803, 1819, 1380, 1761, 1397, 
                1102, 1078, 1608, 1301, 1712, 1906, 1363, 1575, 879, 526, 456, 655, 551, 970, 1252, 
                1823, 1330, 1409, 1368, 1513, 1214, 667, 1654, 1633, 2180, 1819, 916, 2967, 4182, 
                2027, 1090, 1032, 2495, 1359, 601, 236, 858, 2143, 1774, 1003, 2176, 2752, 2184, 
                1633, 1169, 903, 1247, 1343, 937, 1720, 1898, 2478, 3154, 1355, 2205, 1807, 1044, 
                1654, 1111, 630, 1359, 2093, 1430, 1198, 1591, 1724, 2051, 1086, 1144, 1645, 1301, 
                924, 1202, 721, 1363, 1989, 1285, 1247, 1998, 1538, 655, 572, 493, 1086, 1873, 547, 
                1272, 2458, 2930, 3166, 2375, 2027, 1426, 1040, 1326, 572, 1330, 1703, 2188, 1844, 
                3270, 2905, 2043, 1438, 1421, 1567, 1057, 2350, 2424, 1272, 1960, 2901, 4024, 2470, 
                1156, 978, 1140, 1148, 1525, 1426, 1459, 1492, 1003, 1484, 1285, 688, 539, 1252, 1061, 
                1998, 1521, 2072, 1658, 808, 850, 1542, 1952, 1840, 1836, 974, 307, 746, 684, 1032, 
                1683, 2105, 1028, 775, 1670, 1707, 1256, 1823, 472, 543, 1264, 2051, 1848, 1911, 2400, 
                2395, 750, 1111, 2010, 1699, 1496, 1927, 1620, 2300, 1799, 974, 1231, 1413, 2064, 1662, 
                1198, 1177, 1579, 879, 812, 1015, 1185, 1310, 1148, 1973, 601, 1927, 2623, 1790, 1289, 
                850, 1504, 2503, 1790, 1712, 1330, 1032, 1289, 1649, 2068, 1790, 613, 870, 601, 1090, 
                1430, 2615, 3282, 2710, 2159, 1670, 1728, 945, 1761, 4227, 1857, 2901, 2201, 1956, 2835, 
                2246, 1819, 1198, 1281, 2205, 912, 1421, 2520, 3336, 2918, 1575, 1869, 1886, 1127, 1488, 
                2134, 2930, 4957, 4965, 2391, 1886, 2404, 3809, 5827, 5707, 5023, 3444, 1666, 2632, 3759, 
                3473, 2768, 1724, 1455, 2234, 2329, 3067, 2884, 924, 1877, 1173, 2080, 4658, 4219, 2652, 
                3038, 2706, 4617, 5570, 5433, 2918, 1703, 2093, 3933, 3548, 3332, 3680, 3436, 3025, 4940, 
                5023, 1625, 1633, 1542, 2516, 3883, 3170, 3974, 3676, 1616, 1467, 2598, 2594, 2835, 2184, 
                2242, 2317, 1202, 1131, 2076, 2520, 2495, 2586, 1633, 1322, 2114, 1181, 1144, 2988, 3009, 
                1935, 2134, 688, 2619, 1438, 1206, 1591, 1935, 2072, 1587, 1911, 1500, 821, 986, 2466, 3096, 
                2926, 2930, 3386, 1998, 315, 1053, 1363, 2777, 3050, 2234, 1355, 2789, 1732, 1811, 1832, 
                1844, 1670, 2826, 4393, 2254, 1529, 2288, 3630, 3916, 3755, 3286, 3498, 576, 829, 1165, 
                1140, 2748, 3502, 825, 2590, 2337, 2677, 1952, 1869, 2487, 1629, 1032, 1098, 953, 966, 833, 
                1223, 2234, 1728, 3034, 1712, 1434, 1484, 1977, 385, 2101, 2263, 2080, 4012, 3444, 1301, 
                1144, 1757, 2955, 2391, 2151, 2627, 1662, 2250, 2018, 1492, 2963, 2694, 1940, 700, 2524, 
                2748, 3009, 2901, 1658, 961, 3324, 4451, 3212, 1542, 576, 1198, 1334, 2420, 2147, 1152, 1753, 
                1757, 1836, 2963, 2400, 597, 1218, 833, 850, 1036, 1674, 1699, 928, 1115, 2134, 2122, 1583, 
                1123, 605, 1455, 1745, 634, 1720, 1260, 800, 1098, 1281, 999, 1036, 870, 1082, 1011, 845, 1645, 
                1844, 2449, 2768, 1815, 1993, 1790, 1256, 1061, 1567, 1873, 501, 1397, 1940, 2076, 1471, 1094, 
                2143, 2880, 1467, 1318, 879, 2151, 1575, 1554, 1169, 1844, 2416, 887, 1699, 1604, 634, 700, 
                1940, 1695, 1115, 1285, 1036, 1111, 1239, 2143, 1940, 1318, 1343, 1194, 870, 663, 1036, 630, 
                1057, 1649, 638, 1815, 742, 684, 928, 841, 1231, 1811, 2031, 1517, 883, 1339, 1687, 825, 1231, 
                1119, 1579, 899, 1148, 854, 361, 1488, 2582, 2437, 1165, 1927, 1459, 2350, 1989, 1210, 995, 
                903, 1417, 2109, 2632, 2263, 1123, 957, 1608, 1463, 1678, 1732, 1388, 721, 1384, 1546 };

         fftChannel.AddDataLines( myArray );
         fftChannel.ChannelId = (byte) FieldTypes.FFTChannelId.EnvAccl;
         fftChannel.CompressionType = 2;
         fftChannel.DataType = 0;
         fftChannel.Timestamp = iCollectedAt;
         fftChannel.NumberOfLines = 800;
         fftChannel.Factor = 0.000004;
         fftChannel.LowFreq = 0;
         fftChannel.HiFreq = 1000;
         fftChannel.NumberOfAverages = 1;
         fftChannel.Units = 0;

         fftChannel.IsOverloaded = Random_Boolean();
         fftChannel.Load = 0;
         fftChannel.Offset = Random_Byte( 4 );
         fftChannel.SerialNo = "WMCD001";
         fftChannel.SpecNum = Random_Byte( 2 );
         fftChannel.SpeedOrPPF = Random_Byte( 2 );
         fftChannel.UserIndex = Random_Byte( 20 );
         fftChannel.Windowing = Random_Byte( 3 );

         return fftChannel;

      }/* end method */
      #endregion


      #region Private Methods
      /// <summary>
      /// Creates a simple PROCESS record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static ProcessRecord Create_Process( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         ProcessRecord process = new ProcessRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            process.Uid = new Guid();
            process.PointUid = new Guid();
         }
         else
         {
            process.Uid = Guid.NewGuid();
            process.PointUid = iParent;
         }

         // return the current Node UID
         oUid = process.Uid;

         // and populate
         process.AlarmState = Random_Byte( 5 );
         process.AlarmType = Random_Byte( 3 );
         process.AutoRange = Random_Boolean();
         process.BaselineDataTime = DateTime.Now;
         process.BaselineDataValue = 0.0;
         process.Form = Random_Byte( 3 );
         process.LowerAlert = Random_Byte( 100 );
         process.LowerDanger = process.LowerAlert - 1;
         process.Number = iIdentifier;
         process.RangeMax = Random_Byte( 200 ) + 100;
         process.RangeMin = 1 - process.RangeMax;
         process.Units = Random_String( 4 );
         process.UpperAlert = Random_Byte( 50 ) + 100;
         process.UpperDanger = process.RangeMax - 10;

         return process;
      }


      /// <summary>
      /// Creates a simple INSPECTION record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static InspectionRecord Create_Inspection( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         InspectionRecord inspection = new InspectionRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            inspection.Uid = new Guid();
            inspection.PointUid = new Guid();
         }
         else
         {
            inspection.Uid = Guid.NewGuid();
            inspection.PointUid = iParent;
         }

         // return the current Node UID
         oUid = inspection.Uid;

         // and populate
         inspection.Number = iIdentifier;
         inspection.AlarmType1 = Random_Byte( 5 );
         inspection.AlarmType2 = Random_Byte( 5 );
         inspection.AlarmType3 = Random_Byte( 5 );
         inspection.AlarmType4 = Random_Byte( 5 );
         inspection.AlarmType5 = Random_Byte( 5 );
         inspection.AlarmType6 = Random_Byte( 5 );
         inspection.AlarmType7 = Random_Byte( 5 );
         inspection.AlarmType8 = Random_Byte( 5 );
         inspection.AlarmType9 = Random_Byte( 5 );
         inspection.AlarmType10 = Random_Byte( 5 );
         inspection.AlarmType11 = Random_Byte( 5 );
         inspection.AlarmType12 = Random_Byte( 5 );
         inspection.AlarmType13 = Random_Byte( 5 );
         inspection.AlarmType14 = Random_Byte( 5 );
         inspection.AlarmType15 = Random_Byte( 5 );
         inspection.AlertText = Random_String( 32 );
         inspection.BaselineDataTime = DateTime.Now;
         inspection.BaselineDataValue = 0;
         inspection.Choice1 = Random_String( 15 );
         inspection.Choice2 = Random_String( 15 );
         inspection.Choice3 = Random_String( 15 );
         inspection.Choice4 = Random_String( 15 );
         inspection.Choice5 = Random_String( 15 );
         inspection.Choice6 = Random_String( 15 );
         inspection.Choice7 = Random_String( 15 );
         inspection.Choice8 = Random_String( 15 );
         inspection.Choice9 = Random_String( 15 );
         inspection.Choice10 = Random_String( 15 );
         inspection.Choice11 = Random_String( 15 );
         inspection.Choice12 = Random_String( 15 );
         inspection.Choice13 = Random_String( 15 );
         inspection.Choice14 = Random_String( 15 );
         inspection.Choice15 = Random_String( 15 );
         inspection.DangerText = Random_String( 32 );
         inspection.NumberOfChoices = 15;
         inspection.Question = Random_String( 22 );

         return inspection;
      }


      /// <summary>
      /// Creates a simple MCC record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static MCCRecord Create_MCC( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         MCCRecord mcc = new MCCRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            mcc.Uid = new Guid();
            mcc.PointUid = new Guid();
         }
         else
         {
            mcc.Uid = Guid.NewGuid();
            mcc.PointUid = iParent;
         }

         // return the current Node UID
         oUid = mcc.Uid;

         // and populate
         mcc.Number = iIdentifier;
         mcc.EnvAlarm1 = Random_Byte( 100 ) * 16.3;
         mcc.EnvAlarm2 = Random_Byte( 100 ) * 9.3;
         mcc.EnvAlarmState = Random_Byte( 5 );
         mcc.EnvBaselineDataTime = DateTime.Now;
         System.Threading.Thread.Sleep( 25 );
         mcc.EnvBaselineDataValue = 0;
         mcc.SystemUnits = Random_Byte( 2 );
         mcc.TempUnits = Random_Byte( 2 );
         mcc.TmpAlarm1 = Random_Byte( 100 ) * 16.3;
         mcc.TmpAlarm2 = Random_Byte( 100 ) * 9.3;
         mcc.TmpAlarmState = Random_Byte( 5 );
         mcc.TmpBaselineDataTime = DateTime.Now;
         System.Threading.Thread.Sleep( 25 );
         mcc.TmpBaselineDataValue = 0;
         mcc.VelAlarm1 = Random_Byte( 100 ) * 16.3;
         mcc.VelAlarm2 = Random_Byte( 100 ) * 9.3;
         mcc.VelAlarmState = Random_Byte( 5 );
         mcc.VelBaselineDataTime = DateTime.Now;
         System.Threading.Thread.Sleep( 25 );
         mcc.VelBaselineDataValue = 0;

         return mcc;
      }


      /// <summary>
      /// Creates a simple TEXTDATA record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static TextDataRecord Create_TextData( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         TextDataRecord textData = new TextDataRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            textData.Uid = new Guid();
            textData.PointUid = new Guid();
         }
         else
         {
            textData.Uid = Guid.NewGuid();
            textData.PointUid = iParent;
         }

         // return the current Node UID
         oUid = textData.Uid;

         // and populate
         textData.Number = iIdentifier;
         textData.AllowSpaces = Random_Boolean();
         System.Threading.Thread.Sleep( 25 );
         textData.AllowSpecial = Random_Boolean();
         System.Threading.Thread.Sleep( 25 );
         textData.MaxLength = Random_Byte( 240 );
         textData.ScannerInput = Random_Boolean();
         System.Threading.Thread.Sleep( 25 );
         textData.TextType = Random_Byte( 2 );
         textData.UpperCase = Random_Boolean();
         System.Threading.Thread.Sleep( 25 );

         return textData;
      }


      /// <summary>
      /// Creates a simple FFTPoint record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static FFTPointRecord Create_FFTPoint( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         FFTPointRecord fftPoint = new FFTPointRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            fftPoint.Uid = new Guid();
            fftPoint.PointUid = new Guid();
         }
         else
         {
            fftPoint.Uid = Guid.NewGuid();
            fftPoint.PointUid = iParent;
         }

         // return the current Node UID
         oUid = fftPoint.Uid;

         // and populate
         fftPoint.Number = iIdentifier;
         fftPoint.AcclAverages = Random_Byte( 15 );
         fftPoint.AcclDetection = Random_Byte( 10 );
         fftPoint.AcclFreqMax = Random_Byte( 8 ) * 100;
         fftPoint.AcclNumberOfLines = 800;
         fftPoint.AcclType = Random_Byte( 2 );
         fftPoint.AcclWindow = Random_Byte( 16 );
         fftPoint.DefaultRunningSpeed = 4000;
         fftPoint.EnvAverages = Random_Byte( 4 );
         fftPoint.EnvDetection = Random_Byte( 16 );
         fftPoint.EnvFreqMax = Random_Byte( 200 ) * 10;
         fftPoint.EnvNumberOfLines = 400;
         fftPoint.EnvType = Random_Byte( 4 );
         fftPoint.EnvWindow = Random_Byte( 16 );
         fftPoint.TakeData = Random_Byte( 4 );
         fftPoint.UseConfiguration = Random_Byte( 2 );
         fftPoint.VelAverages = Random_Byte( 200 ) * 4;
         fftPoint.VelDetection = Random_Byte( 16 );
         fftPoint.VelFreqMax = Random_Byte( 200 ) * 10;
         fftPoint.VelNumberOfLines = 400;
         fftPoint.VelType = Random_Byte( 4 );
         fftPoint.VelWindow = Random_Byte( 16 );
         System.Threading.Thread.Sleep( 25 );

         return fftPoint;
      }


      /// <summary>
      /// Creates a simple POINT record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static PointRecord Create_Point( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         PointRecord point = new PointRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            point.Uid = Guid.NewGuid();
            point.NodeUid = Guid.NewGuid();
         }
         else
         {
            point.Uid = Guid.NewGuid();
            point.NodeUid = iParent;
         }

         // return the current Node UID
         oUid = point.Uid;

         // and populate
         point.AlarmState = Random_Byte( 5 );
         point.Compliance = Random_Byte( 12 );
         point.DataSaved = true;
         point.Description = Random_String( 32 );
         point.Id = Random_String( 32 );
         point.LastModified = DateTime.Now;
         point.LocationMethod = Random_Byte( 3 );
         point.LocationTag = Random_String( 32 );
         point.Number = iIdentifier;
         point.PointHostIndex = iIdentifier;
         point.PointIndex = 0;
         point.ProcessType = Random_Byte( 3 );
         point.RefNoteCount = Random_Byte( 12 );
         point.RouteId = iIdentifier;
         point.Skipped = Random_Byte( 15 );
         point.StartFrom = DateTime.Now;
         point.TagChanged = Random_Byte( 3 );

         // write some values as text, others as numeric
         if ( Random_Byte( 20 ) > 10 )
            point.TempData = Create_Measurements( false );
         else
            point.TempData = Create_Measurements( true );

         point.VelData = Create_Measurements( false );
         point.EnvData = Create_Measurements( false );

         return point;
      }


      /// <summary>
      /// Create two measurement records
      /// </summary>
      /// <param name="iTextData">True for TextData</param>
      /// <returns>Two measurements</returns>
      private static EnumMeasurementRecords Create_Measurements( Boolean iTextData )
      {
         EnumMeasurementRecords measurements = new EnumMeasurementRecords();

         for ( int i=0; i < 2; ++i )
         {
            PointMeasurement meas = new PointMeasurement();
            if ( iTextData )
               meas.TextValue = Random_String( 32 );
            else
               meas.Value = Random_Byte( 256 );
            meas.LocalTime = DateTime.Now;
            measurements.AddMeasurement( meas );
         }
         return measurements;
      }


      /// <summary>
      /// Creates a simple NODE record
      /// </summary>
      /// <param name="iIdentifier"></param>
      /// <returns></returns>
      private static NodeRecord Create_Node( int iIdentifier, Guid iParent, out Guid oUid )
      {
         // create a dummy node record
         NodeRecord node = new NodeRecord();

         // generate the UID hierarchy 
         if ( iIdentifier == 0 )
         {
            node.Uid = new Guid();
            node.ParentUid = new Guid();
         }
         else
         {
            node.Uid = Guid.NewGuid();
            node.ParentUid = iParent;
         }

         // return the current Node UID
         oUid = node.Uid;

         // and populate
         node.AlarmState = Random_Byte( 5 );
         node.CollectionCheck = Random_Byte( 3 );
         node.DefWorkType = Random_Byte( 3 );
         node.Description = Random_String( 32 );
         node.ElementType = Random_Byte( 3 );
         node.Flags = Random_Byte( 3 );
         node.FunctionalLocation = Random_String( 32 );
         node.HierarchyDepth = 0;
         node.Id = Random_String( 32 );
         node.LastInspResult = Random_Byte( 3 );
         node.LastModified = DateTime.Now;
         node.LocationMethod = Random_Byte( 3 );
         node.LocationTag = Random_String( 32 );
         node.NoLastMeas = Random_Byte( 100 );
         node.Number = iIdentifier;
         node.PRISM = iIdentifier;
         node.RouteId = iIdentifier;
         node.Structured = Random_Byte( 3 );
         return node;
      }


      /// <summary>
      /// Generate a Random Byte value
      /// </summary>
      /// <param name="iSize"></param>
      /// <returns></returns>
      private static byte Random_Byte( int iSize )
      {
         Random random = new Random();
         return (byte) random.Next( 0, iSize );
      }


      /// <summary>
      /// Generate a random string
      /// </summary>
      /// <param name="iSize"></param>
      /// <returns></returns>
      private static string Random_String( int iSize )
      {
         StringBuilder builder = new StringBuilder();
         Random random = new Random();
         char ch;
         for ( int i = 0; i < iSize; i++ )
         {
            ch = Convert.ToChar( Convert.ToInt32
               ( Math.Floor( 26 * random.NextDouble() + 65 ) ) );
            builder.Append( ch );
         }
         System.Threading.Thread.Sleep( 25 );
         return builder.ToString();
      }


      /// <summary>
      /// Generate a random Boolean value
      /// </summary>
      /// <returns></returns>
      private static Boolean Random_Boolean()
      {
         if ( Random_Byte( 20 ) > 10 )
            return true;
         else
            return false;
      }
      #endregion
   }
}

//----------------------------------------------------------------------------
//  TestData.cs
//
//  Revision 1.0 APinkerton 29th October 2012
//  Completely rewritten
//
//  Revision 0.0 APinkerton 12th October 2009
//  Add to project
//----------------------------------------------------------------------------
