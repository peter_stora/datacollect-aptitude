﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Builder;
using NLog.Config;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.NLog.LayoutRenderers;

namespace SKF.DC.AptitudeIntegration.NLog.Dependency
{
    public class NLogLoggingDependencyModule : DependencyModule<NLogLoggingDependencyModule>
    {
        public override void RegisterDependencies(ContainerBuilder c)
        {
            RegisterLayoutRenderers();

            RegisterLogging(c);
        }

        private void RegisterLayoutRenderers()
        {
            //Register the required layout renderers
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("skfRequestIdWithTimestamp", typeof(SKFRequestIdWithTimestampLayoutRenderer));
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("skfDateTimeLogPath", typeof(SKFDateTimeLogPathLayoutRenderer));
        }

        private void RegisterLogging(ContainerBuilder c)
        {
            //Register one nlog listener to be attached to all loggers!
            c.RegisterType<NLogMessageListener>().As<ILogListener>().SingleInstance();
        }
    }
}
