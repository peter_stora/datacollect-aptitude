﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.NLog.LayoutRenderers;
using LogLevel = SKF.DC.AptitudeIntegration.Logging.LogLevel;
using NLogLogger = global::NLog.Logger;
using NLogManager = global::NLog.LogManager;

namespace SKF.DC.AptitudeIntegration.NLog
{
    public class NLogMessageListener : ILogListener
    {
        private object _syncObj;
        private Dictionary<string, NLogLogger> _cachedLoggers;
        
        public NLogMessageListener()
        {
            _syncObj = new object();
            _cachedLoggers = new Dictionary<string, NLogLogger>();
        }

        public void HandleLogMessage(ILogMessage message)
        {
            //Just write to the internal nlog logger!
            var logger = message.Logger;

            //Use the logger name per default
            var loggerName = logger.Name;
            if (loggerName == null)
            {
                loggerName = "*";
            }

            //Get the NLog logger implementation!
            var nlogLogger = GetNLogLogger(loggerName);
            if (nlogLogger != null)
            {
                LogMessage(nlogLogger, message);
            }
        }
        
        private NLogLogger GetNLogLogger(string loggerName)
        {
            lock (_syncObj)
            {
                //use caching for loggers... they are expensive to create!
                NLogLogger nlogLogger;
                if (!_cachedLoggers.TryGetValue(loggerName, out nlogLogger))
                {
                    nlogLogger = NLogManager.GetLogger(loggerName);
                    _cachedLoggers[loggerName] = nlogLogger;
                }
                
                return nlogLogger;
            }
        }

        private void LogMessage(NLogLogger nlogLoger, ILogMessage message)
        {
            if(nlogLoger == null)
                return;

            var msg = message.Message;
            var level = message.LogLevel;
            var args = GetNLogMessageArgs(message);

            switch (level)
            {
                case LogLevel.Trace:
                    nlogLoger.Trace(msg, args);
                    break;
                case LogLevel.Debug:
                    nlogLoger.Debug(msg, args);
                    break;
                case LogLevel.Info:
                    nlogLoger.Info(msg, args);
                    break;
                case LogLevel.Error:
                    nlogLoger.Error(msg, args);
                    break;
                default:
                    nlogLoger.Debug(msg, args);
                    break;
            }
        }

        private object[] GetNLogMessageArgs(ILogMessage message)
        {
            //Just pass in the logger
            return new object[] {message.Logger, message};
        }
    }
}
