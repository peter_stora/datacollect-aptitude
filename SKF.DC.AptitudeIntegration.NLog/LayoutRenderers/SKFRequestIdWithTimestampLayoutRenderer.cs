﻿using System;
using System.Linq;
using System.Text;
using NLog;
using NLog.LayoutRenderers;
using ILogger = SKF.DC.AptitudeIntegration.Logging.ILogger;

namespace SKF.DC.AptitudeIntegration.NLog.LayoutRenderers
{
    [LayoutRenderer("skfRequestIdWithTimestamp")]
    public class SKFRequestIdWithTimestampLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder sb, LogEventInfo le)
        {
            string requestId = null;
            
            //Find the "result" parameter and use the "requestId" value from the parameter!
            var logger = le.GetLogger();
            if (logger != null)
            {
                //Find the result parameter and use the request id!

                var requestIdParam = logger.GetParam("requestId");
                if (requestIdParam != null)
                {
                    try
                    {
                        requestId = requestIdParam.Value.ToString();
                    }
                    catch (Exception)
                    {
                        //Do nothing
                        requestId = null;
                    }
                }
            }

            if (requestId == null)
            {
                requestId = "now_request_id_" + Guid.NewGuid().ToString();
            }

            //Just write the request id
            sb.Append(requestId);
        }
    }
}