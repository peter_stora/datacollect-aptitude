﻿using System.Linq;
using NLog;
using NLog.LayoutRenderers;
using SKF.DC.AptitudeIntegration.Logging;
using ILogger = SKF.DC.AptitudeIntegration.Logging.ILogger;

namespace SKF.DC.AptitudeIntegration.NLog.LayoutRenderers
{
    public static class LogEventInfoExtensionMethods
    {
        public static ILogger GetLogger(this LogEventInfo le)
        {
            var parameters = le.Parameters;
            if (parameters == null)
                return null;
            
            return parameters.Select(p => p as ILogger).FirstOrDefault();
        }

        public static ILogMessage GetLogMessage(this LogEventInfo le)
        {
            var parameters = le.Parameters;
            if (parameters == null)
                return null;

            return parameters.Select(p => p as ILogMessage).FirstOrDefault();
        }
    }
}