﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.LayoutRenderers;

namespace SKF.DC.AptitudeIntegration.NLog.LayoutRenderers
{
    [LayoutRenderer("skfDateTimeLogPath")]
    public class SKFDateTimeLogPathLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder sb, LogEventInfo le)
        {
            DateTime dt;

            var logMessage = le.GetLogMessage();
            if (logMessage != null)
            {
                dt = logMessage.DateTime;
            }
            else
            {
                dt = DateTime.UtcNow;
            }
            
            //Just write the request id
            sb.Append(dt.ToString("yyyy/MM/dd"));
        }
    }
}
