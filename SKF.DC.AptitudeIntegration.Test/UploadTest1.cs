﻿using System;
using System.Collections.Generic;
using SKF.DC.AptitudeIntegration.Configuration;

namespace SKF.DC.AptitudeIntegration.Test
{
    public class UploadTest1 : AptitudeIntegrationUploadTest<UploadTest1>
    {
        public UploadTest1(IConfiguration config)
        {
            Config = config;
        }

        public IConfiguration Config { get; set; }

        /// <summary>
        /// Prepare the request data as we see fit
        /// </summary>
        /// <param name="request"></param>
        protected override void PopulateUploadRequest(IUploadSyncDataRequest r)
        {
            //Build the request?

            //Use hardcoded request data?
            r.clientId = Config.Get<Guid>("uploadTest1.clientId");
            r.userGroupTitle = "AptitudeAnalystGroupOne";
            r.userFirstName = "Martin";
            r.formDefinitionId = Config.Get<string>("uploadTest1.formDefinitionId");
            r.formDefinitionTitle = Config.Get<string>("uploadTest1.formDefinitionTitle");
            r.userGroupDescription = "Aptitude Analyst Group One";
            r.userGroupId = 1429;
            r.userName = Config.Get<string>("uploadTest1.userFirstName");
            r.description = "";
            r.completed = false;
            r.title = "AptitudeAnalystAfternoon";
            r.userEmail = Config.Get<string>("uploadTest1.userEmail");
            r.userLastName = Config.Get<string>("uploadTest1.userLastName");
            r.userFullName = r.userFirstName + " " + r.userLastName;

            CreateAndAddAnwser(r, "1");
            CreateAndAddAnwser(r, "2");
            CreateAndAddAnwser(r, "3");
            CreateAndAddAnwser(r, "4");
            CreateAndAddAnwser(r, "5");
        }

        private void CreateAndAddAnwser(IUploadSyncDataRequest r, string id)
        {
            var d = r.CreateQuestionData();
            d = PopulateAnwser(d, id);
            if (d != null)
            {
                if (r.questionsData == null)
                {
                    r.questionsData = new List<IUploadSyncQuestionData>();
                }
                r.AddQuestionData(d);
            }
        }

        private IUploadSyncQuestionData PopulateAnwser(IUploadSyncQuestionData o, string anwserId)
        {
            var id = Config.Get<string>("uploadTest1." + anwserId + ".questionId");
            if (id == null)
                return null;

            o.answeredBy = Config.Get<int>("uploadTest1." + anwserId + ".answeredBy");

            o.questionId = id;
            o.questionTitle = Config.Get<string>("uploadTest1." + anwserId + ".questionTitle");
            o.questionQuestion = Config.Get<string>("uploadTest1." + anwserId + ".questionQuestion");
            o.sectionTitle = Config.Get<string>("uploadTest1." + anwserId + ".sectionTitle");
            o.categoryTitle = Config.Get<string>("uploadTest1." + anwserId + ".categoryTitle");
            o.answer = Config.Get<string>("uploadTest1." + anwserId + ".answer");

            return o;
        }

        /// <summary>
        /// Handle the upload response as we see fit
        /// </summary>
        /// <param name="result"></param>
        protected override void HandleResult(IUploadSyncDataResult r)
        {
            //Log the result?
        }
    }
}