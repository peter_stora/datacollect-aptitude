﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using SKF.DC.AptitudeIntegration.Dependency;

namespace SKF.DC.AptitudeIntegration.Test
{
    public interface IAptitudeIntegrationBackend
    {
        IContainer Container { get; set; }
        IAptitudeIntegrationService CreateAptitudeIntegrationService();
        IUploadSyncDataRequest CreateUploadRequest();
        IEnumerable<IDependencyModule> GetDependencyModules();
    }

    public abstract class AptitudeIntegrationUploadTest<TTest> : IDisposable
        where TTest : AptitudeIntegrationUploadTest<TTest>
    {
        
        public static TTest Create(IAptitudeIntegrationBackend backend)
        {
            var bootstrapper = new AptitudeIntegrationBootstrapper();
            var test = bootstrapper.Run<TTest>(backend.GetDependencyModules().ToArray());
            backend.Container = bootstrapper.Container;
            test._backend = backend;
            return test;
        }

        private IAptitudeIntegrationBackend _backend;

        public virtual void Run()
        {
            var service = _backend.CreateAptitudeIntegrationService();
            var disposableService = service as IDisposable;

            try
            {
                var uploadRequest = _backend.CreateUploadRequest();
                PopulateUploadRequest(uploadRequest);
                var uploadResult = Upload(service, uploadRequest);
                HandleResult(uploadResult);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (disposableService != null)
                {
                    try
                    {
                        disposableService.Dispose();
                    } catch(Exception) { /*Doesn't matter*/ }
                }
            }
        }


        protected virtual void HandleResult(IUploadSyncDataResult result)
        {
            //Handle the result?
        }
        
        protected virtual IUploadSyncDataResult Upload(IAptitudeIntegrationService service, IUploadSyncDataRequest uploadRequest)
        {
            var uploadResult = service.UploadDataForClient(uploadRequest);
            return uploadResult;
        }        

        protected virtual void PopulateUploadRequest(IUploadSyncDataRequest request)
        {
            //Populate the request data?
        }

        public virtual void Dispose()
        {
            //Dispose the client or what not
        }
    }
}