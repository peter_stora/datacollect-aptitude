﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Configuration;
using SKF.DC.AptitudeIntegration.Connection;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.RequestHandlers;

namespace SKF.DC.AptitudeIntegration
{
    public abstract class AptitudeIntegrationService : IAptitudeIntegrationService
    {
        private Guid? _aptitudeSingleDeviceClientId;

        public AptitudeIntegrationService(IMIConnectionManager connectionManager, IConfiguration config, IUploadSyncDataRequestHandler uploadSyncDataRequestHandler)
        {
            Config = config;
            ConnectionManager = connectionManager;

            UploadSyncDataRequestHandler = uploadSyncDataRequestHandler;
        }

        private IUploadSyncDataRequestHandler UploadSyncDataRequestHandler { get; set; }

        private IConfiguration Config { get; set; }

        /// <summary>
        /// The connection manager keeping track of MI device connections to the Analyst MI service
        /// </summary>
        private IMIConnectionManager ConnectionManager { get; set; }

        #region IAptitudeIntegrationService implementation
        IGetSyncDataResult IAptitudeIntegrationService.GetDataForClient(IGetSyncDataRequest request)
        {
            return GetDataForClient(request);
        }

        IUploadSyncDataResult IAptitudeIntegrationService.UploadDataForClient(IUploadSyncDataRequest request)
        {
            return UploadDataForClient(request);
        }
        #endregion

        /// <summary>
        /// Retrieves route data from Aptitude - connects to the Aptitude Analyst MI service, emulating a MI device.
        /// Downlods data from Aptitude, storing in a local DB, then reads from this local DB and prepares the result data.
        /// </summary>
        /// <param name="request">The request for which to load route data</param>
        /// <returns></returns>
        protected virtual IGetSyncDataResult GetDataForClient(IGetSyncDataRequest request)
        {
            var r = CreateGetSyncDataResult();
            //prepare the request context for "global access" - it is per thread...
            RequestContext.Current.SetResult(r);
            
            var clientId = GetClientIdForRequest(request);
            var logger = GetRequestLogger(request, r);
            
            using (var con = ConnectionManager.GetConnection(clientId))
            {
                //Only allow once per client synchronization!
                lock (con.SyncObj)
                {
                    using (var client = con.GetClient(CreateMIConfig(clientId, logger)))
                    {
                        //Trigger the client synchronization!
                        client.Synchronize();
                    }
                }
            }

            return r;
        }
        
        protected abstract IGetSyncDataResult CreateGetSyncDataResult();

        /// <summary>
        /// Synchronizes the specified request data with Aptitude - connects to the Aptitude Analyst MI service, emulating a MI device.
        /// Downlods data from Aptitude, storing in a local DB, then updates the the local DB and sends this back to Aptitude.
        /// </summary>
        /// <param name="request">The data to upload to Aptitude</param>
        protected virtual IUploadSyncDataResult UploadDataForClient(IUploadSyncDataRequest request)
        {
            var r = CreateUploadSyncDataResult();
            //prepare the request context for "global access" - it is per thread...
            RequestContext.Current.SetResult(r);
            
            var clientId = GetClientIdForRequest(request);
            var logger = GetRequestLogger(request, r);

            using (var con = ConnectionManager.GetConnection(clientId))
            {
                //Only allow once per client synchronization!
                lock (con.SyncObj)
                {
                    using (var client = con.GetClient(CreateMIConfig(clientId, logger)))
                    {
                        HandleUploadSyncRequest(request, client);

                        //Trigger the client synchronization!
                        try
                        {
                            client.Synchronize();
                        }
                        catch (Exception ex)
                        {
                            client.Logger.Error(ex, "Failed to synchronize data with Aptitude!");
                        }                        
                    }
                }
            }

            return r;
        }

        private void HandleUploadSyncRequest(IUploadSyncDataRequest request, MIClient client)
        {
            try
            {
                UploadSyncDataRequestHandler.Handle(request, client);
            }
            catch (Exception e)
            {
                client.Logger.Error(e, "Error in UploadSyncDataRequestHandler.Handle(...)");
            }
        }

        protected abstract IUploadSyncDataResult CreateUploadSyncDataResult();

        private ILogger GetRequestLogger(IRequest request, IResult result)
        {
            var logger = LoggerFactory.Get();
            return logger;
        }

        /// <summary>
        /// Creates the client configuration for connecting to the Aptitude MI Service - TCP configuration.
        /// </summary>
        /// <returns></returns>
        protected virtual MIClientConfig CreateMIConfig(Guid clientId, ILogger logger)
        {
            var conf = new MIClientConfig() {
                IpAddress = Config.Get("aptitude.miservice.ip", "127.0.0.1"),
                Port = Config.Get("aptitude.miservice.port", 8086),
                DeviceFirmwareVersion = Config.Get("aptitude.miservice.devicefirmware", "1.4.5.1"),
                OperatorId = Config.Get("aptitude.miservice.operator.id", 1),
                Logger = logger
            };

            //Get the device id, in the future - perhaps loa device id per client id!?
            var deviceUid = Config.Get("aptitude.device.id", "0040056a-0e9d-0038-97b9-104060010800");
            conf.DeviceUID = deviceUid;

            return conf;
        }

        protected virtual Guid GetClientIdForRequest(IRequest request)
        {
            if (!_aptitudeSingleDeviceClientId.HasValue)
            {
                _aptitudeSingleDeviceClientId = Guid.Parse(Config.Get("aptitude.device.id", "0040056a-0e9d-0038-97b9-104060010800"));
            }

            return _aptitudeSingleDeviceClientId.Value;
        }
    }
}
