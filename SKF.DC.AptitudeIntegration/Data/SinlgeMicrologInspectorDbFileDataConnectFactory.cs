﻿using System;
using SKF.DC.AptitudeIntegration.Configuration;
using SKF.DC.AptitudeIntegration.Db;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.RS.MicrologInspector.Database;

namespace SKF.DC.AptitudeIntegration.Data
{
    public class SinlgeMicrologInspectorDbFileDataConnectFactory : IDataConnectFactory
    {
        private object _owner;
        private string _dataConnectFilePath;
        

        public SinlgeMicrologInspectorDbFileDataConnectFactory(IConfiguration config)
        {
            Config = config;
            Logger = LoggerFactory.Get();
            Logger.Debug("Created Factory!");
        }

        public IConfiguration Config { get; set; }
        private ILogger Logger { get; set; }

        public void InitOwner(object owner)
        {
            _owner = owner;
        }

        public AptitudeDataConnect Create(Guid clientId)
        {
            Logger.Debug("Creating DC!");
            string dataConnectPath = null;

            if (IsDesignTime())
            {
                //_dataConnectFilePath = @"inspectorDB.sdf";
                dataConnectPath = Config.Get<string>("aptitude.dataConnectDb.debug");
            }
            else
            {
                //_dataConnectFilePath = @"MicrologInspectorDB\inspectorDB.sdf";
                dataConnectPath = Config.Get<string>("aptitude.dataConnectDb");
            }

            if (_owner != null)
            {
                Logger.Debug("Owner exists!");
            }
            else
            {
                Logger.Debug("Owner doesn't exists!");
            }

            if (string.IsNullOrEmpty(dataConnectPath))
            {
                Logger.Debug("Loading DataConnect db with no path!");
                return new AptitudeDataConnect(_owner);
            }
            else
            {
                Logger.Debug("Loading DataConnect db with path: " + dataConnectPath);
                return new AptitudeDataConnect(_owner, dataConnectPath);
            }
        }

        private bool IsDesignTime()
        {
            System.Reflection.Assembly mscorlibAssembly = typeof(int).Assembly;
            if ((mscorlibAssembly != null))
            {
                if (mscorlibAssembly.FullName.ToUpper().EndsWith("B77A5C561934E089"))
                {
                    return true;
                }
            }
            return false;

        }
    }
}