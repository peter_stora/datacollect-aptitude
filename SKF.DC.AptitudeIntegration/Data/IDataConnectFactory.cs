﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Db;
using SKF.RS.MicrologInspector.Database;

namespace SKF.DC.AptitudeIntegration.Data
{
    public interface IDataConnectFactory
    {
        AptitudeDataConnect Create(Guid clientId);
    }
}
