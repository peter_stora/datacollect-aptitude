﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Data;

namespace SKF.DC.AptitudeIntegration.Connection
{
    public interface IMIConnectionManager
    {
        MIConnection GetConnection(Guid clientId);
    }

    public class MIConnectionManager : IMIConnectionManager
    {
        private Dictionary<Guid, MIConnectionEntry> _connections;
        private IMIClientFactory _clientFactory;
        private Form _form;

        public MIConnectionManager(IMIClientFactory clientFactory)
        {
            SyncObj = new object();
            _form = new Form();
            _connections = new Dictionary<Guid, MIConnectionEntry>();
            _clientFactory = clientFactory;
        }

        protected object SyncObj { get; private set; }

        public MIConnection GetConnection(Guid clientId)
        {
            lock (SyncObj)
            {
                var clientEntry = GetOrCreateConnectionEntry(clientId);
                lock (clientEntry.SyncObj)
                {
                    return clientEntry.GetConnection();
                }
            }
        }

        private MIConnectionEntry GetOrCreateConnectionEntry(Guid clientId)
        {
            MIConnectionEntry conEntry;
            if (!_connections.TryGetValue(clientId, out conEntry))
            {
                conEntry = new MIConnectionEntry(clientId, _clientFactory);
                _connections[clientId] = conEntry;
            }
            return conEntry;
        }
    }

    public class MIConnectionEntry : IDisposable
    {
        private MIConnection _connection;
        private IMIClientFactory _clientFactory;

        public MIConnectionEntry(Guid clientId, IMIClientFactory clientFactory)
        {
            SyncObj = new object();
            ClientID = clientId;
            _clientFactory = clientFactory;
        }

        public Guid ClientID { get; private set; }

        public object SyncObj { get; private set; }

        public MIConnection GetConnection()
        {
            if (_connection == null)
            {
                _connection = new MIConnection(ClientID, _clientFactory);
            }

            return _connection;
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                try
                {
                    _connection.Dispose();
                }
                catch (Exception) { /*doesn't matter*/}
            }
        }
    }
}
