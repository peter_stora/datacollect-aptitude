﻿using System;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Data;

namespace SKF.DC.AptitudeIntegration.Connection
{
    public class MIConnection : IDisposable
    {
        private MIClient _client;
        private IMIClientFactory _clientFactory;

        public MIConnection(Guid clientId, IMIClientFactory clientFactory)
        {
            _clientFactory = clientFactory;

            SyncObj = new object();
            ClientID = clientId;
        }

        public object SyncObj { get; private set; }

        public Guid ClientID { get; private set; }

        public MIClient GetClient(MIClientConfig config)
        {
            if (_client == null)
            {
                _client = (MIClient)_clientFactory.Create(ClientID, config);
            }

            lock (_client.SyncObj)
            {
                if (!_client.IsValid)
                {
                    //Create a new client!
                    _client = (MIClient)_clientFactory.Create(ClientID, config);
                }
            }
            
            return _client;
        }

        public void Dispose()
        {
            if (_client != null)
            {
                try
                {
                    _client.Dispose();
                }
                catch (Exception) {/*doesn't matter*/}
            }
        }
    }
}