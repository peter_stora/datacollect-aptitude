﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SKF.DC.AptitudeIntegration
{
    public interface IAptitudeIntegrationService
    {
        IGetSyncDataResult GetDataForClient(IGetSyncDataRequest request);
        
        IUploadSyncDataResult UploadDataForClient(IUploadSyncDataRequest request);
    }

    public interface IResult
    {
        Guid requestId { get; set; }

        bool success { get; set; }

        int errorCode { get; set; }

        string errorMessage { get; set; }
    }

    public interface IRequest
    {
        Guid clientId { get; set; }
 
        Guid requestId { get; set; }
    }

    public interface IGetSyncDataRequest : IRequest
    {
    }
    
    public interface IGetSyncDataResult : IResult
    {
    }
    
    public interface IUploadSyncDataRequest : IRequest
    {
        string formDefinitionId { get; set; }
        string formDefinitionTitle { get; set; }
        
        string userName { get; set; }
        
        string userEmail { get; set; }
        
        string userFirstName { get; set; }
        string userLastName { get; set; }
        
        string userFullName { get; set; }
        
        int userGroupId { get; set; }
        string userGroupTitle { get; set; }
        string userGroupDescription { get; set; }
        
        string title { get; set; }

        string description { get; set; }
        bool completed { get; set; }
        
        IList<IUploadSyncQuestionData> questionsData { get; set; }

        void AddQuestionData(IUploadSyncQuestionData questionData);

        IUploadSyncQuestionData CreateQuestionData();
    }

    public interface IUploadSyncQuestionData
    {
        string questionId { get; set; }
        
        string questionTitle { get; set; }
       
        string questionQuestion { get; set; }
        
        string categoryTitle { get; set; }
        
        string sectionTitle { get; set; }
        
        string answer { get; set; }
        
        int answeredBy { get; set; }
        
        long lastModified { get; set; }
    }

    public interface IUploadSyncDataResult : IResult
    {
    }
    
    public interface ISyncData
    {
    }

    public class RequestContext
    {
        [ThreadStatic]
        private static RequestContext _current;

        public static RequestContext Current
        {
            get
            {
                return _current;
            }
        }

        public RequestContext()
        {
        }
        
        public Guid Id { get; set; }

        public IRequest Request { get; set; }
        public IResult Result { get; set; }

        public static void Begin(Guid id, IRequest request)
        {
            if (_current == null)
            {
                _current = new RequestContext();
                _current.Id = id;
                _current.Request = request;
            }
        }

        public void SetResult(IResult result)
        {
            Result = result;
            result.requestId = Id;
        }
    }
}