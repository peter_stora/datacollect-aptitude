﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Configuration;

namespace SKF.DC.AptitudeIntegration.Logging
{
    public class ConsoleLogListener : ILogListener
    {
        public ConsoleLogListener(IConfiguration config)
        {
            //Check the config if console logging is enabled!
            Enabled = config.Get("logging.console.enabled", false);
        }

        public bool Enabled { get; set; }

        public void HandleLogMessage(ILogMessage message)
        {
            if(!Enabled)
                return;

            try
            {
                var msg = message.Message;
                Console.WriteLine(msg);
            }
            catch (Exception)
            {
                //Don't allow logging to break anything...
            }            
        }
    }
}
