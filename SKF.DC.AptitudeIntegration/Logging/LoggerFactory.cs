﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Dependency;

namespace SKF.DC.AptitudeIntegration.Logging
{
    public static class LoggerFactory
    {
        public static ILogger Get<TLogger>()
            where TLogger : ILogger
        {
            if (Resolver.Current == null)
                return new NullLogger();

            try
            {
                return Resolver.Get<TLogger>();
            }
            catch (Exception)
            {
                return new NullLogger();
            }
        }

        public static ILogger Get()
        {
            if (Resolver.Current == null)
                return new NullLogger();

            try
            {
                return Resolver.Get<DefaultLogger>();
            }
            catch (Exception)
            {
                return new NullLogger();
            }
        }
    }
}
