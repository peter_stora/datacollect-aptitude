﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.Logging
{
    public abstract class Logger : ILogger
    {
        private Dictionary<string, ILogParameter> _parameters;
        private List<ILogListener> _logListerners; 

        public Logger()
        {
            _logListerners = new List<ILogListener>();
            _parameters = new Dictionary<string, ILogParameter>();

            Name = "default";
        }

        public string Name { get; private set; }

        protected virtual void Log(LogLevel level, Exception ex, string message, params object[] args)
        {
            PrepareLogParams();

            var msg = CreateLogMessage(level, ex, message, args);

            NotifyListeners(msg);
        }

        protected virtual void PrepareLogParams()
        {
        }

        protected void AddLogListener(ILogListener listener)
        {
            if(!_logListerners.Contains(listener))
                _logListerners.Add(listener);
        }

        protected virtual void NotifyListeners(ILogMessage msg)
        {
            var listeners = _logListerners.ToList();

            //TODO: perhaps do this asynchronously for each log message???
            foreach (var logListerner in listeners)
            {
                try
                {
                    logListerner.HandleLogMessage(msg);
                }
                catch (Exception)
                {
                    //Do nothing, don't allow the logging to throw any exceptions...
                }
            }
        }

        protected virtual ILogMessage CreateLogMessage(LogLevel level, Exception exception, string message, object[] args)
        {
            var logMsg = new LogMessage(this, level, GetMessageDateTime(), exception, message, args);
            return logMsg;
        }

        protected virtual DateTime GetMessageDateTime()
        {
            return DateTime.UtcNow;
        }
        
        public virtual ILogParameter GetParam(string name)
        {
            ILogParameter p;
            if (!_parameters.TryGetValue(name, out p))
                return null;

            return p;
        }

        public IEnumerable<ILogParameter> GetParams()
        {
            return _parameters.Values.ToList();
        }

        public void AddParam(string name, object value)
        {
            _parameters[name] = new LogParameter(name, value);
        }

        public void Debug(string message, params object[] args)
        {
            Log(LogLevel.Debug, null, message, args);
        }
        public void Debug(Exception ex, string message, params object[] args)
        {
            Log(LogLevel.Debug, ex, message, args);
        }

        public void Info(string message, params object[] args)
        {
            Log(LogLevel.Info, null, message, args);
        }
        public void Info(Exception ex, string message, params object[] args)
        {
            Log(LogLevel.Info, ex, message, args);
        }

        public void Error(string message, params object[] args)
        {
            Log(LogLevel.Error, null, message, args);
        }
        public void Error(Exception ex, string message, params object[] args)
        {
            Log(LogLevel.Error, ex, message, args);
        }
    }

    public class DefaultLogger : Logger
    {
        public DefaultLogger(IEnumerable<ILogListener> logListeners)
        {
            InitLogListeners(logListeners);
        }

        protected override void PrepareLogParams()
        {
            base.PrepareLogParams();

            var context = RequestContext.Current;
            if (context == null)
                return;

            //Make sure to set the request id parameter as a param available for logging!
            AddParam("requestId", context.Id);
        }

        private void InitLogListeners(IEnumerable<ILogListener> logListeners)
        {
            //add the log listeners!
            if (logListeners != null)
            {
                foreach (var logListener in logListeners)
                {
                    AddLogListener(logListener);
                }
            }
        }
    }

    public class NullLogger : Logger
    {
    }

    public class LogMessage : ILogMessage
    {
        private string _message;

        public LogMessage(ILogger logger, LogLevel level, DateTime dateTime, Exception exception, string message, object[] args)
        {
            Logger = logger;
            LogLevel = level;
            DateTime = dateTime;
            Exception = exception;
            RawMessage = message;
            RawMessageArgs = args;
        }

        public ILogger Logger { get; private set; }
        
        public LogLevel LogLevel { get; private set; }

        public DateTime DateTime { get; private set; }

        public Exception Exception { get; private set; }

        public string RawMessage { get; private set; }

        public object[] RawMessageArgs { get; private set; }

        public string Message
        {
            get
            {
                if (_message == null)
                {
                    try
                    {
                        _message = BuildMessage();
                    }
                    catch (Exception)
                    {
                        _message = BuildSafeMessage();
                    }
                }

                if (_message == null)
                {
                    _message = BuildSafeMessage();
                }

                if (_message == null)
                    _message = string.Empty;

                return _message;
            }
        }

        private string BuildSafeMessage()
        {
            var args = RawMessageArgs;
            if (args == null)
                args = new object[0];

            var rawMessage = RawMessage;
            if (rawMessage == null)
                rawMessage = "";

            try
            {
                return string.Format(rawMessage, args);
            }
            catch (Exception)
            {
                return rawMessage;
            }
        }

        protected virtual string BuildMessage()
        {
            var msg = RawMessage;
            if (msg == null)
                msg = "";

            var msgArgs = RawMessageArgs;
            if(msgArgs == null)
                msgArgs = new object[0];

            try
            {
                msg = string.Format(msg, msgArgs);
            }
            catch (Exception)
            {
                msg = BuildSafeMessage();
            }

            if (Exception != null)
            {
                try
                {
                    if (msg == null)
                    {
                        msg = "Exception: " + Exception.ToString();
                    }

                    msg += " - Exception: " + Exception.ToString();
                }
                catch (Exception)
                {
                    //Do nothing...
                }
            }

            return msg;
        }

        public override string ToString()
        {
            return Message;
        }
    }

    public class LogParameter : ILogParameter
    {
        public LogParameter(string name, object value)
        {
            Value = value;
        }

        public string Name { get; set; }
        public object Value { get; set; }
    }
}
