﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.Logging
{
    public interface ILogger
    {
        string Name { get; }

        ILogParameter GetParam(string name);
        IEnumerable<ILogParameter> GetParams();
        void AddParam(string name, object value);

        void Debug(string message, params object[] args);
        void Debug(Exception ex, string message, params object[] args);

        void Info(string message, params object[] args);
        void Info(Exception ex, string message, params object[] args);

        void Error(string message, params object[] args);
        void Error(Exception ex, string message, params object[] args);
    }

    public interface ILogParameter
    {
        string Name { get; }
        object Value { get; }
    }

    public interface ILogMessage
    {
        ILogger Logger { get; }

        LogLevel LogLevel { get; }

        DateTime DateTime { get; }

        Exception Exception { get; }

        string RawMessage { get; }

        object[] RawMessageArgs { get; }

        string Message { get; }
    }

    public enum LogLevel
    {
        Trace,
        Debug,
        Info,
        Error
    }
}
