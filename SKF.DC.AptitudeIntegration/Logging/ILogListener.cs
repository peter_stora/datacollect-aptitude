﻿using System;

namespace SKF.DC.AptitudeIntegration.Logging
{
    public interface ILogListener
    {
        void HandleLogMessage(ILogMessage message);
    }
}