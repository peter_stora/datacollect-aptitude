﻿using SKF.DC.AptitudeIntegration.Client;

namespace SKF.DC.AptitudeIntegration.RequestHandlers
{
    public interface IUploadSyncDataRequestHandler
    {
        void Handle(IUploadSyncDataRequest request, IMIClient client);
    }
}