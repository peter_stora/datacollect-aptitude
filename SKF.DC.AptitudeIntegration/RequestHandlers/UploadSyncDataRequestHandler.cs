﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Db;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.DC.AptitudeIntegration.RequestHandlers
{
    public class UploadSyncDataRequestHandler : IUploadSyncDataRequestHandler
    {
        public void Handle(IUploadSyncDataRequest request, IMIClient client)
        {
            //Process the upload request data and update the data connect db!
            AptitudeDataConnect dc = null;
            try
            {
                dc = client.DataConnect;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to get DataConnect! - " + ex.Message, ex);
            }

            var nodes = dc.GetNodesEnum();

            client.Logger.Debug("Updating nodes!");

            foreach (NodeRecord node in nodes)
            {
                client.Logger.Debug("Node: " + node.Id);
                var points = dc.GetPointsEnum(node.Uid);
                foreach (PointRecord point in points)
                {
                    if (point.ProcessType != (byte)FieldTypes.ProcessType.SingleSelectInspection && point.ProcessType != (byte)FieldTypes.ProcessType.WildCard)
                    {
                        client.Logger.Debug("Invalid node type");
                        continue;
                    }

                    client.Logger.Debug("Finding anwser to update");
                    var anwser = FindAnwser(request, client, node, point);
                    if (anwser == null)
                    {
                        client.Logger.Debug("No anwser found: " + node.Id + ", " + point.Id);
                        continue;
                    }

                    client.Logger.Debug("Upating measurement");
                    var measurement = UpdatePointAnwser(dc, point, anwser);
                    if (measurement == null)
                    {
                        client.Logger.Debug("Measurement not updated: " + node.Id + ", " + point.Id);
                        continue;
                    }
                    
                    //Update the point!
                    try
                    {
                        client.Logger.Debug("Adding measurement: " + node.Id + ", " + point.Id);
                        dc.AddMeasurement(point, client.GetOperatorId(), 3);
                    }
                    catch (Exception ex)
                    {
                        client.Logger.Debug(ex, "Failed to add measurement: " + node.Id + ", " + point.Id);
                    }
                }
            }

            try
            {
                dc.UpdateNodes(nodes);

                client.Logger.Debug("Updated nodes!");
            }
            catch (Exception ex)
            {
                client.Logger.Debug(ex, "Failed to update Nodes");
            }
        }

        private IUploadSyncQuestionData FindAnwser(IUploadSyncDataRequest request, IMIClient client, NodeRecord node, PointRecord point)
        {
            if (request == null || request.questionsData == null)
            {
                client.Logger.Debug("No questions data available!");
                return null;
            }

            //Find the matching one!
            foreach (var o in request.questionsData)
            {
                if (!string.Equals(node.Id, o.categoryTitle))
                {
                    client.Logger.Debug("categoryTitle not match: '{0}' == '{1}'", node.Id, o.categoryTitle);
                    continue;   
                }

                if (!string.Equals(point.Id, o.questionTitle))
                {
                    client.Logger.Debug("questionTitle not match: '{0}' == '{1}'", point.Id, o.questionTitle);
                    continue;
                }

                return o;
            }

            return null;
        }

        private PointMeasurement UpdatePointAnwser(AptitudeDataConnect dc, PointRecord point, IUploadSyncQuestionData anwser)
        {
            var m = new PointMeasurement();
            
            if (point.ProcessType == (byte)FieldTypes.ProcessType.SingleSelectInspection)
            {
                var insp = dc.GetInspectionEnum().Inspection.ToList().Where(i => i.PointUid == point.Uid).FirstOrDefault();
                if (insp == null)
                    return null;

                //var choice = rndMeasurementVal.Next(1, (int)insp.NumberOfChoices + 1);
                //m.Value = choice;
                return null;
            }
            else
            {
                //Set randum number value
                //var val = rndMeasurementVal.Next(0, 100);

                double val;
                if (TryGetConvertedAnwserValue<double>(anwser.answer, out val))
                {
                    m.Value = val;
                }
            }

            //Update last time...
            m.LocalTime = PacketBase.DateTimeNow;

            //Add measurement to the point
            point.TempData.AddMeasurement(m);

            return m;
        }

        private bool TryGetConvertedAnwserValue<T>(string strVal, out T val)
        {
            val = default(T);
            if (strVal == null)
                return false;

            var tConverter = TypeDescriptor.GetConverter(typeof(T));
            if (tConverter == null)
                return false;

            if (tConverter.CanConvertFrom(typeof(string)))
            {
                try
                {
                    var convertedVal = tConverter.ConvertFrom(strVal);
                    if (convertedVal != null)
                    {
                        val = (T)convertedVal;
                        return true;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }

            var strConverter = TypeDescriptor.GetConverter(typeof(string));
            if (strConverter.CanConvertTo(typeof(T)))
            {
                try
                {
                    var convertedVal = strConverter.ConvertTo(strVal, typeof(T));
                    if (convertedVal != null)
                    {
                        val = (T)convertedVal;
                        return true;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }

            return false;
        }
    }
}
