﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.RS.MicrologInspector.Database;

namespace SKF.DC.AptitudeIntegration.Db
{
    public class AptitudeDataConnect : DataConnect
    {
        public AptitudeDataConnect(object owner)
            : base(owner)
        {
        }

        public AptitudeDataConnect(object owner, string dataSource)
            : base(owner, dataSource)
        {
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
