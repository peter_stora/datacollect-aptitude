﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.Dependency
{
    public static class Resolver
    {
        public static IDependencyResolver Current { get; set; }

        public static T Get<T>()
        {
            if (Current == null)
                return default(T);

            return Current.Get<T>();
        }
    }

    public interface IDependencyResolver
    {
        T Get<T>();
    }
}
