﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace SKF.DC.AptitudeIntegration.Dependency
{
    public class AutofacDependencyResolver : IDependencyResolver
    {
        private IContainer _container;

        public AutofacDependencyResolver(IContainer container)
        {
            _container = container;
        }

        public T Get<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
