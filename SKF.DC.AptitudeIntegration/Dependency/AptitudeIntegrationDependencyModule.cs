﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autofac;
using Autofac.Core.Registration;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Configuration;
using SKF.DC.AptitudeIntegration.Connection;
using SKF.DC.AptitudeIntegration.Data;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.RequestHandlers;

namespace SKF.DC.AptitudeIntegration.Dependency
{
    public class AptitudeIntegrationDependencyModule : DependencyModule<AptitudeIntegrationDependencyModule>
    {
        private Form _ownerForm;
        //Due to requirements from MI helper libs... we need to provide an owner form... so we use a dummy here!
        private Form OwnerForm
        {
            get
            {
                if (_ownerForm == null)
                {
                    _ownerForm = new Form();
                }

                return _ownerForm;
            }
        }

        public override void RegisterDependencies(ContainerBuilder c)
        {
            //Register any required dependencies
            RegisterConfiguration(c);
            
            RegisterLogging(c);

            RegisterServices(c);

            RegisterRequestHandlers(c);
        }
        
        private void RegisterConfiguration(ContainerBuilder c)
        {
            //Use app config as configuration source
            c.RegisterType<AppConfigConfiguration>().As<AppConfigConfiguration, IConfiguration>().SingleInstance();
        }

        private void RegisterLogging(ContainerBuilder c)
        {
            //Create a class logger as default logger - the default logger takes collection of ILogListener and appends those to the logger - these will kind of write the logging messages!
            //In order to add another listener, just register a type with the ILogListener interface
            c.RegisterType<DefaultLogger>().As<ILogger>();

            //Register console log listener as a default listener!
            c.RegisterType<ConsoleLogListener>().As<ILogListener>();
        }

        private void RegisterServices(ContainerBuilder c)
        {
            //Use a single instance of the connection manager
            c.RegisterType<MIConnectionManager>().As<MIConnectionManager, IMIConnectionManager>().SingleInstance();

            //Use a single instance of the DataConnect factory
            c.RegisterType<SinlgeMicrologInspectorDbFileDataConnectFactory>().As<SinlgeMicrologInspectorDbFileDataConnectFactory, IDataConnectFactory>().SingleInstance().OnActivated(ctx => {
                ctx.Instance.InitOwner(OwnerForm);
            });

            //Register the client factory
            c.RegisterType<MIClientFactory>().As<MIClientFactory, IMIClientFactory>().SingleInstance().OnActivated(ctx => {
                ctx.Instance.InitOwner(OwnerForm);
            });
        }

        private void RegisterRequestHandlers(ContainerBuilder c)
        {
            c.RegisterType<UploadSyncDataRequestHandler>().As<UploadSyncDataRequestHandler, IUploadSyncDataRequestHandler>();
        }
    }
}
