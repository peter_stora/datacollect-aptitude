﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace SKF.DC.AptitudeIntegration.Dependency
{
    public interface IDependencyModule
    {
        void RegisterDependencies(ContainerBuilder c);
    }

    public class DependencyModule<TModule> : IDependencyModule
    {
        public virtual void RegisterDependencies(ContainerBuilder c)
        {
        }
    }
}
