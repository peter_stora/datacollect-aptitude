﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.Configuration
{
    public interface IConfiguration
    {
        TValue Get<TValue>(string id, TValue defaultValue = default(TValue));
    }
}
