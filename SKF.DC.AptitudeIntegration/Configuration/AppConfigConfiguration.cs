﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;

namespace SKF.DC.AptitudeIntegration.Configuration
{
    public class AppConfigConfiguration : IConfiguration
    {
        private Dictionary<string, object> _configValues;

        public AppConfigConfiguration()
        {
            _configValues = new Dictionary<string, object>();
        }

        public TValue Get<TValue>(string id, TValue defaultValue = default(TValue))
        {
            return GetHardcodedConfig<TValue>(id, defaultValue);
        }

        private T GetHardcodedConfig<T>(string id, T defaultValue)
        {
            T appConfValue;
            if (TryGetAppConfigValue(id, out appConfValue))
                return appConfValue;

            //Aptitude device hardcoded info
            if (string.Equals(id, "aptitude.device.id", StringComparison.InvariantCultureIgnoreCase))
            {
                return (T) (object)"0040056a-0e9d-0038-97b9-104060010800";
            }
            //MI hardcoded connection info
            if (string.Equals(id, "aptitude.miservice.ip", StringComparison.InvariantCultureIgnoreCase))
            {
                return (T)(object)"127.0.0.1";
            }
            if (string.Equals(id, "aptitude.miservice.port", StringComparison.InvariantCultureIgnoreCase))
            {
                return (T)(object)8086;
            }
            if (string.Equals(id, "aptitude.miservice.devicefirmware", StringComparison.InvariantCultureIgnoreCase))
            {
                return (T)(object)"1.4.5.1";
            }
            if (string.Equals(id, "aptitude.miservice.operator.id", StringComparison.InvariantCultureIgnoreCase))
            {
                return (T)(object)1;
            }
            
            return defaultValue;
        }

        private bool TryGetAppConfigValue<T>(string id, out T val)
        {
            val = default(T);

            string strVal = null;
            try
            {
                strVal = ConfigurationManager.AppSettings[id];
            }
            catch (Exception)
            {
            }

            if (strVal == null)
                return false;
            
            var tConverter = TypeDescriptor.GetConverter(typeof(T));
            if (tConverter == null)
                return false;
            
            if (tConverter.CanConvertFrom(typeof (string)))
            {
                try
                {
                    var convertedVal = tConverter.ConvertFrom(strVal);
                    if (convertedVal != null)
                    {
                        val = (T)convertedVal;
                        return true;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }

            var strConverter = TypeDescriptor.GetConverter(typeof(string));
            if (strConverter.CanConvertTo(typeof (T)))
            {
                try
                {
                    var convertedVal = strConverter.ConvertTo(strVal, typeof(T));
                    if (convertedVal != null)
                    {
                        val = (T)convertedVal;
                        return true;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }
            
            return false;
        }
    }
}