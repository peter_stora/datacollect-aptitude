﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SKF.DC.AptitudeIntegration.Dependency;

namespace SKF.DC.AptitudeIntegration
{
    public class Bootstrapper
    {
        public Bootstrapper()
        {
        }

        public IContainer Container { get; private set; }

        public TApplication Run<TApplication>(params IDependencyModule[] dependencyModules)
        {
            //Prepare autofac
            var builder = new ContainerBuilder();

            //Register the application
            RegisterApplication<TApplication>(builder);

            //Register the dependencies from the specified modules
            RegisterDependencies(builder, dependencyModules);

            //Build the application
            Container = builder.Build();

            Resolver.Current = new AutofacDependencyResolver(Container);

            //Create the Application
            var app = Container.Resolve<TApplication>();

            //Return the application
            return app;
        }

        protected virtual void RegisterDependencies(ContainerBuilder builder, IEnumerable<IDependencyModule> dependencyModules)
        {
            foreach (var dependencyModule in dependencyModules)
            {
                dependencyModule.RegisterDependencies(builder);
            }
        }

        protected virtual void RegisterApplication<TApplicaion>(ContainerBuilder builder)
        {
            builder.RegisterType<TApplicaion>();
        }
    }

    public class AptitudeIntegrationBootstrapper : Bootstrapper
    {
        protected override void RegisterDependencies(ContainerBuilder builder, IEnumerable<IDependencyModule> dependencyModules)
        {
            //Register the required stuff from the AptitudeIntegration
            var modules = new List<IDependencyModule>(dependencyModules);
            modules.Insert(0, new AptitudeIntegrationDependencyModule());

            //Register any additional stuff
            base.RegisterDependencies(builder, modules);
        }
    }
    
}
