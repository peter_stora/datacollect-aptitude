﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKF.DC.AptitudeIntegration.Data;
using SKF.DC.AptitudeIntegration.Db;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.MicrologInspector;
using SKF.RS.MicrologInspector.Database;
using SKF.RS.MicrologInspector.DeviceSocketClient;

namespace SKF.DC.AptitudeIntegration.Client
{
    public interface IMIClient : IDisposable
    {
        Guid ClientID { get; }
        AptitudeDataConnect DataConnect { get; }
        int GetOperatorId();

        ILogger Logger { get; set; }
    }

    public class MIClient : IMIClient, IDisposable
    {
        private object _owner;
        private AptitudeDataConnect _dataConnect;
        private IDataConnectFactory _dataConnectFactory;
        private object _dataConnectSyncObj;

        public MIClient(Guid clientId, MIClientConfig config, IDataConnectFactory dataConnectFactory)
        {
            SyncObj = new object();
            ClientID = clientId;
            Config = config;
            Status = MIClientStatus.None;
            Logger = config.Logger;

            _dataConnectSyncObj = new object();
            _dataConnectFactory = dataConnectFactory;
        }

        public object SyncObj { get; private set; }
        public Guid ClientID { get; private set; }
        public MIClientConfig Config { get; private set; }
        public MIClientStatus Status { get; private set; }

        public AptitudeDataConnect DataConnect
        {
            get
            {
                lock (_dataConnectSyncObj)
                {
                    if (_dataConnect == null)
                    {
                        _dataConnect = _dataConnectFactory.Create(ClientID);
                        Logger.Debug("Created DC");
                        //Make sure to load the data
                        Exception connectException = null;
                        var connected = false;
                        try
                        {
                            Logger.Debug("Connecting DC");
                            connected = _dataConnect.Connect(true);
                            if (connected)
                            {
                                Logger.Debug("DC Connected");
                            }
                            else
                            {
                                Logger.Debug("DC NOT Connected");
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Debug("Error Connecting DC");
                            connected = false;
                            connectException = ex;
                            Logger.Error(ex, "" + ex.Message + "- ex:" + ex.ToString() + "- stacktrace: - " + ex.StackTrace.ToString());
                            Logger.Debug("Ok");
                        }

                        if (!connected)
                        {
                            //Failed to load the dataconnect
                            if (connectException != null)
                            {
                                throw new Exception("Failed to load DataConnect db! - " + connectException.Message, connectException);
                            }
                            else
                            {
                                throw new Exception("Failed to load DataConnect db!");
                            }
                        }
                    }
                }

                return _dataConnect;
            }
        }

        public int GetOperatorId()
        {
            return Config.OperatorId;
        }

        public bool IsValid
        {
            get
            {
                if (Status == MIClientStatus.Disposed)
                    return false;

                if (Status == MIClientStatus.Error)
                    return false;

                return true;
            }
        }

        public ILogger Logger { get; set; }

        public void InitOwner(object owner)
        {
            _owner = owner;
        }

        public void Synchronize(Action<IMISynchronizationConfig> clientEventsConfigAction = null)
        {
            //Prepare a synchronization config, that will be used to customize the synchronization pipeline
            var syncConfig = new MISynchronizationConfig();

            //Trigger the configuration action if any provided
            if (clientEventsConfigAction != null)
            {
                clientEventsConfigAction(syncConfig);
            }

            //Connect and perform the synchronization tasks that have been configured
            ConnectAndSynchronize(syncConfig);
        }

        private void ConnectAndSynchronize(MISynchronizationConfig syncConfig)
        {
            Status = MIClientStatus.Connecting;

            //Create the socket client and run int
            using (var socketClient = CreateSocketClient())
            {
                //Configure the socket client
                ConfigureSocket(socketClient, syncConfig);

                //Now we can connect!
                ConnectSocket(socketClient);

                //Wait for the synchronization to complete
                WaitForSynchronizationFinished(socketClient);
            }
        }

        private void ConfigureSocket(MISocketClient socketClient, MISynchronizationConfig syncConfig)
        {
            //configure the socket
            socketClient.Configure(Config, syncConfig);
        }

        private void ConnectSocket(MISocketClient socketClient)
        {
            socketClient.Connect();
        }

        private void WaitForSynchronizationFinished(MISocketClient socketClient)
        {
            socketClient.WaitForSyncFinished();
        }

        protected virtual MISocketClient CreateSocketClient()
        {
            var socketClient = new MISocketClient(DataConnect);
            socketClient.InitOwner(_owner);
            return socketClient;
        }

        public void Dispose()
        {
            if (_dataConnect != null)
            {
                try
                {
                    //TODO: Do not dispose the DataConnect db... we are reusing this, but in a future update we should make sure the db is disposed correctly...
                    //_dataConnect.Dispose();
                }
                catch (Exception) {/*doesn't matter*/}
            }

            Status = MIClientStatus.Disposed;
        }
    }

    public enum MIClientStatus
    {
        None,
        Connecting,
        Synchronizing,
        Error,
        Disposed
    }

    public interface IMISynchronizationConfig
    {
        void OnceAfterDownload(Action onDownloadSyncCompletedAction);
        void OnceBeforeUpload(Action onBeforeUploadSyncAction);
    }

    public class MISynchronizationConfig : IMISynchronizationConfig
    {
        public Action AfterDownload { get; set; }
        public Action BeforeUpload { get; set; }

        public void OnceAfterDownload(Action onDownloadSyncCompletedAction)
        {
            AfterDownload = onDownloadSyncCompletedAction;
        }

        public void OnceBeforeUpload(Action onBeforeUploadSyncAction)
        {
            BeforeUpload = onBeforeUploadSyncAction;
        }
    }

    public class MIClientConfig
    {
        public MIClientConfig()
        {
        }
        public ILogger Logger { get; set; }
        public string DeviceUID { get; set; }
        public string IpAddress { get; set; }
        public string SocketHost { get; set; }
        public int Port { get; set; }
        public string DeviceFirmwareVersion { get; set; }
        public int OperatorId { get; set; }
    }
}
