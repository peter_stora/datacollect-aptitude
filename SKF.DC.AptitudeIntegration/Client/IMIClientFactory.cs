﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Data;

namespace SKF.DC.AptitudeIntegration.Client
{
    public interface IMIClientFactory
    {
        IMIClient Create(Guid clientId, MIClientConfig config);
    }

    public class MIClientFactory : IMIClientFactory
    {
        private object _owner;
        private IDataConnectFactory _dataConnectFactory;

        public MIClientFactory(IDataConnectFactory dataConnectFactory)
        {
            _dataConnectFactory = dataConnectFactory;
        }

        public void InitOwner(object owner)
        {
            _owner = owner;
        }

        public IMIClient Create(Guid clientId, MIClientConfig config)
        {
            var client = new MIClient(clientId, config, _dataConnectFactory);
            client.InitOwner(_owner);
            return client;
        }
    }
}
