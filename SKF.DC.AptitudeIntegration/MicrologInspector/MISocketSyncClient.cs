﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.RS.MicrologInspector.Database;
using SKF.RS.MicrologInspector.DeviceSocketClient;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.DC.AptitudeIntegration.MicrologInspector
{
    public class PacketEventArgs : EventArgs
    {
        public PacketEventArgs(IMISynchSocketClient client, PacketType packetType, ClientStateObject stateObject)
        {
            Client = client;
            PacketType = packetType;
            StateObject = stateObject;
        }

        public PacketType PacketType { get; set; }
        public IMISynchSocketClient Client { get; set; }
        public ClientStateObject StateObject { get; set; }
    }
    public delegate void SocketSyncClientEventHandler(PacketEventArgs e);

    public class MISocketSyncClient : SynchSocketClient, IMISynchSocketClient
    {
        private AutoResetEvent _disconnectedAutoResetEvent;
        private bool _isDisconnected;

        public MISocketSyncClient(object form, ILogger logger, DataConnect dataConnect) : base(form, dataConnect)
        {
            Status = SocketClientStatus.None;
            Logger = logger;

            _disconnectedAutoResetEvent = new AutoResetEvent(false);

            ConnectionStateEvent += OnConnection;
        }

        public event SocketSyncClientEventHandler PacketReceivedEvent;

        private ILogger Logger { get; set; }

        public SocketClientStatus Status { get; set; }

        private IMISynchronizationConfig SyncConfig { get; set; }
        

        public override void Connect()
        {
            try
            {
                Status = SocketClientStatus.Connecting;
                base.Connect();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Failed to connect!");
                Status = SocketClientStatus.Error;
                throw;
            }
        }

        public virtual void WaitForDisconnected(int timeoutMs)
        {
            /*if (Status == SocketClientStatus.None || Status == SocketClientStatus.Error || Status == SocketClientStatus.Disconnected)
                return;*/

            if (_isDisconnected)
                return;

            _disconnectedAutoResetEvent.WaitOne(timeoutMs);
        }

        public virtual void SetDisconnected()
        {
            _isDisconnected = true;
            _disconnectedAutoResetEvent.Set();
        }

        private void OnConnection(bool iConnectionState, ConnectionCode iCode)
        {
            this.UpdateConnectionStatus(iConnectionState, iCode);
        }

        protected override void ProcessReceiveBytes(ClientStateObject iStateObject)
        {
            TriggerPacketReceived(iStateObject);

            //Let the base do whatever needs to be done to handle the packet data!
            base.ProcessReceiveBytes(iStateObject);
        }

        private void TriggerPacketReceived(ClientStateObject iStateObject)
        {
            var packetType = PacketBase.GetPacketType(iStateObject.ReadBuffer);

            if (PacketReceivedEvent != null)
            {
                try
                {
                    PacketReceivedEvent(new PacketEventArgs(this, packetType, iStateObject));
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Error in PacketReceivedEvent(...) event handler!");
                }
            }
        }
    }
}