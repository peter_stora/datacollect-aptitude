﻿using System;
using System.Threading;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.RS.MicrologInspector.DeviceSocketClient;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.DC.AptitudeIntegration.MicrologInspector
{
    public class MISocketTestClient : TestServer, IMISynchSocketClient
    {
        private AutoResetEvent _disconnectedAutoResetEvent;

        public MISocketTestClient(object form, ILogger logger) 
            : base(form)
        {
            Status = SocketClientStatus.None;
            Logger = logger;

            _disconnectedAutoResetEvent = new AutoResetEvent(false);

            ConnectionStateEvent += OnConnection;
        }

        private ILogger Logger { get; set; }

        public SocketClientStatus Status { get; set; }

        public override void Connect()
        {
            try
            {
                Status = SocketClientStatus.Connecting;
                base.TestConnection();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Failed to connect!");
                Status = SocketClientStatus.Error;
                throw;
            }
        }

        public virtual void WaitForDisconnected(int timeoutMs)
        {
            if(Status == SocketClientStatus.None || Status == SocketClientStatus.Error || Status == SocketClientStatus.Disconnected)
                return;

            _disconnectedAutoResetEvent.WaitOne(timeoutMs);
        }

        public virtual void SetDisconnected()
        {
            _disconnectedAutoResetEvent.Set();
        }

        private void OnConnection(bool iConnectionState, ConnectionCode iCode)
        {
            this.UpdateConnectionStatus(iConnectionState, iCode);
        }
    }
}