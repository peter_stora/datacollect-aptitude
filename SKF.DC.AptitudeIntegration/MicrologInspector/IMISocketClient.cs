﻿using System;

namespace SKF.DC.AptitudeIntegration.MicrologInspector
{
    public interface IMISocketClient : IDisposable
    {
        void Connect();
        void WaitForSyncFinished(int timeoutMs = MISocketClientHelper.DEFAULT_SYNC_TIMOUT_MS);
    }
}