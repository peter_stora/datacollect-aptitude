﻿using System;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.DC.AptitudeIntegration.MicrologInspector
{
    public interface IMISynchSocketClient : IDisposable
    {
        SocketClientStatus Status { get; set; }

        void SetDisconnected();
    }

    public enum SocketClientStatus
    {
        None,
        Connecting,
        Synchronizing,
        Disconnected,
        Error
    }

    public static class MISocketClientHelper
    {
        public const int DEFAULT_SYNC_TIMOUT_MS = 30000;

        public static bool UpdateConnectionStatus(this IMISynchSocketClient client, bool iConnectionState, ConnectionCode iCode)
        {
            switch (iCode)
            {
                case ConnectionCode.CONNECTED:
                    client.Status = SocketClientStatus.Synchronizing;
                    return true;
                    break;
                case ConnectionCode.DISCONNECTED:
                    client.Status = SocketClientStatus.Disconnected;
                    break;
                case ConnectionCode.ERR_FIRMWARE:
                case ConnectionCode.ERR_NO_SERVER:
                case ConnectionCode.ERR_UNKNOWN_DEVICE:
                case ConnectionCode.ERR_UNLICENSED:
                case ConnectionCode.ERR_SERVER_BUSY:
                case ConnectionCode.ERR_TCP_DROPPED:
                case ConnectionCode.ERR_DATA_QUALITY:
                case ConnectionCode.ERR_SYNCH_FAILED:
                case ConnectionCode.ERR_UNCRADLED:
                case ConnectionCode.SERVER_NOT_AVAILABLE:
                case ConnectionCode.ERR_FIRMWARE_UNLICENSED:
                case ConnectionCode.ERR_GENERAL:
                    client.Status = SocketClientStatus.Error;
                    break;
                default:
                    break;
            }

            if (client.Status == SocketClientStatus.Error || client.Status == SocketClientStatus.Disconnected)
            {
                client.SetDisconnected();
            }

            return false;
        }
    }
}