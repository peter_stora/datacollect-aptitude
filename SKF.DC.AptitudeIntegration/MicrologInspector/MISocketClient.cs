﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.RS.MicrologInspector.Database;
using SKF.RS.MicrologInspector.Packets;

namespace SKF.DC.AptitudeIntegration.MicrologInspector
{
    public class MISocketClient : IMISocketClient
    {
        private object _owner;
        private DataConnect _dataConnect;
        
        public MISocketClient(DataConnect dataConnect)
        {
            _dataConnect = dataConnect;

            //Setup the loggers to be used!
            SyncLogger = GetSocketClientLogger("Sync");
            TestLogger = GetSocketClientLogger("Test");
        }

        private ILogger GetSocketClientLogger(string name)
        {
            var logger = LoggerFactory.Get();
            //TODO: implement the Request/Result logger parameters as "global" parameters that are appended to every logger
            logger.AddParam("name", name);
            return logger;
        }

        private MISynchronizationConfig SyncConfig { get; set; }

        private MISocketTestClient TestClient { get; set; }
        private ILogger TestLogger { get; set; }

        private MISocketSyncClient SyncClient { get; set; }
        private ILogger SyncLogger { get; set; }

        public void InitOwner(object owner)
        {
            _owner = owner;
        }

        public void Configure(MIClientConfig config, MISynchronizationConfig syncConfig)
        {
            if (config.Logger != null)
            {
                TestLogger = config.Logger;
                SyncLogger = config.Logger;
            }

            //Setup the sockets
            SetupSocketClients();

            //Configure the sockets
            ConfigureSockets(config);

            //Configure the synchronization
            ConfigureSync(syncConfig);
        }

        private void SetupSocketClients()
        {
            //Setup the test connection client
            TestClient = new MISocketTestClient(_owner, TestLogger);
            TestClient.ConnectionStateEvent += OnTestConnection;

            SyncClient = new MISocketSyncClient(_owner, SyncLogger, _dataConnect);
            SyncClient.ConnectionStateEvent += OnSyncConnection;
            SyncClient.ClientDataExceptionEvent += OnSyncDataException;
            SyncClient.ServerExceptionEvent += OnSyncServerException;
            SyncClient.PacketReceivedEvent += OnSyncPacketReceived;
        }

        
        private void ConfigureSockets(MIClientConfig config)
        {
            //Setup the test client ip and port
            TestClient.UseIpAddress = true;
            TestClient.SocketHost = config.IpAddress;
            TestClient.TcpPortNumber = (short)config.Port;

            //Setup the actual sync client ip and port
            SyncClient.UseIpAddress = true;
            SyncClient.SocketHost = config.IpAddress;
            SyncClient.TcpPortNumber = (short)config.Port;

            // shall we sync with @ptitude's CMMS system
            //what does this do?
            SyncClient.UseAnalystCMMS = false;

            //Setup the device info
            SyncClient.DeviceUID = config.DeviceUID; // set the device UID
            SyncClient.FirmwareVersion = new HwVersion(config.DeviceFirmwareVersion); // set the firmware version
        }

        public void ConfigureSync(MISynchronizationConfig syncConfig)
        {
            SyncConfig = syncConfig;
        }

        public void Connect()
        {
            ConnectTestClient();
        }

        private void ConnectTestClient()
        {
            //Test the connection!
            TestLogger.Debug("Test connecting to server!");
            TestClient.Connect();
        }

        private void OnTestConnection(bool iConnectionState, ConnectionCode iCode)
        {
            //Handle the connection event for the Test client
            var isConnected = HandleServerConnectionStatus(iConnectionState, iCode, TestLogger);
            if (isConnected)
            {
                //Make a real connection with the sync client
                ConnectSyncClient();
            }
        }

        private void ConnectSyncClient()
        {
            SyncLogger.Debug("Sync connecting to server!");
            SyncClient.Connect();
        }

        private void OnSyncServerException(Exception ex)
        {
            SyncLogger.Error(ex, "Unhandled error from server!");
        }

        private void OnSyncDataException(DataConnectError iError)
        {
            HandleDataException(iError, SyncLogger);
        }
        
        private void OnSyncConnection(bool iConnectionState, ConnectionCode iCode)
        {
            //Handle the connection event for the Test client
            HandleServerConnectionStatus(iConnectionState, iCode, SyncLogger);
        }

        private void OnSyncPacketReceived(PacketEventArgs e)
        {
            if (e.PacketType == PacketType.SESSION)
            {
                //Trigger the upload handler
                TriggerBeforeUploadHandler();
            }
        }

        private void TriggerBeforeUploadHandler()
        {
            if (SyncConfig.BeforeUpload != null)
            {
                try
                {
                    SyncConfig.BeforeUpload();
                }
                catch (Exception e)
                {   
                    SyncLogger.Error(e, "Error in BeforeUpload(...) handler!");
                }
            }
        }

        public void WaitForSyncFinished(int timeoutMs = MISocketClientHelper.DEFAULT_SYNC_TIMOUT_MS)
        {
            /*if (TestClient.Status == SocketClientStatus.None)
            {
                //havn't initialized connection
                return;
            }

            if (TestClient.Status == SocketClientStatus.Error)
            {
                //will never connect if test client is in error state
                return;
            }

            if (SyncClient.Status == SocketClientStatus.Error)
            {
                //client is in error state....
                return;
            }

            if (SyncClient.Status == SocketClientStatus.Disconnected)
            {
                //client is disconnected
                return;
            }*/

            //Wait for the sync client to be disconnected
            SyncClient.WaitForDisconnected(timeoutMs);
        }

        public virtual void Dispose()
        {
            if (TestClient != null)
            {
                try
                {
                    TestClient.ConnectionStateEvent -= OnTestConnection;
                    //TestClient.Dispose();
                }
                catch (Exception)
                { 
                    /*Doesn't matter*/
                } 
                finally
                {
                    TestClient = null;
                }
            }

            if (SyncClient != null)
            {
                try
                {
                    SyncClient.ConnectionStateEvent -= OnSyncConnection;
                    SyncClient.ClientDataExceptionEvent -= OnSyncDataException;
                    SyncClient.ServerExceptionEvent -= OnSyncServerException;
                    SyncClient.PacketReceivedEvent -= OnSyncPacketReceived;

                    //TODO: fix this... when we dispose, the DataConnect db is disposed too!!! ... and we are reusing it in several cases!
                    //SyncClient.Dispose();
                }
                catch (Exception)
                {
                    /*Doesn't matter*/
                }
                finally
                {
                    SyncClient = null;
                }
            }
        }

        #region helper methods

        private void HandleDataException(DataConnectError iError, ILogger log)
        {
            if (iError.IOErrorType == ErrorType.Connection)
            {
                log.Debug("Unable to connect to Database ...");
            }
        }

        private bool HandleServerConnectionStatus(bool iConnectionState, ConnectionCode iCode, ILogger log)
        {
            var isConnected = false;

            switch (iCode)
            {
                case ConnectionCode.CONNECTED:
                    log.Debug("Connected to server!");
                    isConnected = true;
                    break;
                case ConnectionCode.DISCONNECTED:
                    log.Debug("Disconnected from server! - Client disconnected!");
                    break;
                case ConnectionCode.ERR_FIRMWARE:
                    log.Error("Not connected to server! - Invalid firmware!");
                    break;
                case ConnectionCode.ERR_NO_SERVER:
                    log.Error("Not connected to server! - No connection could be established!");
                    break;
                case ConnectionCode.ERR_UNKNOWN_DEVICE:
                    log.Error("Not connected to server! - Unknown device!");
                    break;
                case ConnectionCode.ERR_UNLICENSED:
                    log.Error("Not connected to server! - Invalid license!");
                    break;
                case ConnectionCode.ERR_SERVER_BUSY:
                    log.Error("Not connected to server! - Server is Busy!");
                    break;
                case ConnectionCode.ERR_TCP_DROPPED:
                    log.Info("Disconnected from server! - Lost tcp connection!");
                    break;
                case ConnectionCode.ERR_DATA_QUALITY:
                    log.Error("Synchronization failed! - Data Quality is Bad!");
                    break;
                case ConnectionCode.ERR_SYNCH_FAILED:
                    log.Error("Synchronization failed!");
                    break;
                case ConnectionCode.ERR_UNCRADLED:
                    log.Error("Not connected to server! - Device uncradled!");
                    break;
                case ConnectionCode.SERVER_NOT_AVAILABLE:
                    log.Error("Not connected to server! - Server not available!");
                    break;
                case ConnectionCode.ERR_FIRMWARE_UNLICENSED:
                    log.Error("Not connected to server! - Firmware is unlicensed!");
                    break;
                case ConnectionCode.ERR_GENERAL:
                    log.Error("Unknown error!");
                    break;
                default:
                    break;
            }

            return isConnected;
        }
        #endregion

    }
}
