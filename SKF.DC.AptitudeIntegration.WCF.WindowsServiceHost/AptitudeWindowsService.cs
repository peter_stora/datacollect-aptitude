﻿using System;
using System.ServiceProcess;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHost
{
    public class AptitudeWindowsService
    {
        public AptitudeWindowsService()
        {
        }

        protected AptitudeIntegrationWindowsServiceHost WindowsService { get; set; }

        public virtual void Run()
        {
            try
            {
                Setup();
            }
            catch (Exception e)
            {
                throw new Exception("Failed to setup the AptitudeWindowsService.", e);
            }

            //Now start the service
            StartService();
        }

        private void Setup()
        {
            if(WindowsService != null)
                return;

            var bootstrapper = new AptitudeIntegrationWcfBootstrapper();

            try
            {
                WindowsService = bootstrapper.Run<AptitudeIntegrationWindowsServiceHost>();
                WindowsService.Setup(bootstrapper.Container);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to bootstrap the AptitudeIntegrationWindowsServiceHost.", e);
            }
        }

        protected virtual void StartService()
        {
            try
            {
                ServiceBase.Run(WindowsService);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to start the AptitudeIntegrationWindowsServiceHost.", e);
            }
        }

        public void Stop()
        {
            if(WindowsService == null)
                return;

            try
            {
                WindowsService.Stop();
                WindowsService = null;
            }
            catch (Exception)
            {
                //Doesn't matter...
            }
        }
    }
}