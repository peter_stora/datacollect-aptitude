﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHost
{
    [RunInstaller(true)]
    public class AptitudeWindowsServiceInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public AptitudeWindowsServiceInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "SKF DC @ptitude Integration Service";
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
