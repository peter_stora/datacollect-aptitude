﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Logging;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHost
{
    public partial class AptitudeIntegrationWindowsServiceHost : ServiceBase
    {
        private AptitudeServiceHostFactory _hostFactory;
        private Autofac.IContainer _container;

        public AptitudeIntegrationWindowsServiceHost(ILogger logger)
        {
            InitializeComponent();

            Logger = logger;

            _hostFactory = new AptitudeServiceHostFactory();
        }

        private ILogger Logger { get; set; }

        private ServiceHost ServiceHost { get; set; }

        public virtual void Setup(Autofac.IContainer container)
        {
            _container = container;
        }
        
        protected override void OnStart(string[] args)
        {
            StartServiceHost();   
        }

        private void StartServiceHost()
        {
            //Close any previous existing
            CloseService();

            //Open the service
            OpenService();
        }

        private void OpenService()
        {
            //Setup the current autofac container
            _hostFactory.SetupContainer(_container);

            //Create the service host!
            ServiceHost = _hostFactory.CreateServiceHost<AptitudeIntegrationService>(null);

            //Now open the service host for connections!
            ServiceHost.Open();
        }

        private void CloseService()
        {
            if (ServiceHost == null)
                return;

            //Close the previo
            try
            {
                ServiceHost.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Failed to close the service.", e);
            }
            finally
            {
                ServiceHost = null;
            }
        }

        protected override void OnStop()
        {
            CloseService();
        }

        public void Start()
        {
            OnStart(null);
        }
    }
}
