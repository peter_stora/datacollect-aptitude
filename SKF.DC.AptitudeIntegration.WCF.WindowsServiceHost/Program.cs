﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SKF.DC.AptitudeIntegration.WCF.Dependency;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHost
{
    static class Program
    {
        static void Main()
        {
            new AptitudeWindowsService().Run();
        }
    }
}
