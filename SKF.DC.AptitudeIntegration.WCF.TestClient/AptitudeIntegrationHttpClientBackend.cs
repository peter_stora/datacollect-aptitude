﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.Test;

namespace SKF.DC.AptitudeIntegration.WCF.TestClient.AptitudeIntegrationService
{
    /// <summary>
    /// Aptitude integration backend implemented with a remote http client, a remote connection is made to the actual WCF endpoint.
    /// </summary>
    public class AptitudeIntegrationHttpClientBackend : IAptitudeIntegrationBackend
    {
        public IContainer Container { get; set; }

        public virtual IAptitudeIntegrationService CreateAptitudeIntegrationService()
        {
            return new JsonAptitudeIntegrationServiceClient();
        }

        public virtual IUploadSyncDataRequest CreateUploadRequest()
        {
            return new UploadSyncDataRequest();
        }

        public IEnumerable<IDependencyModule> GetDependencyModules()
        {
            yield break;
        }
    }

    public partial class JsonAptitudeIntegrationServiceClient : IAptitudeIntegrationService
    {
        public IGetSyncDataResult GetDataForClient(IGetSyncDataRequest request)
        {
            return GetDataForClient((GetSyncDataRequest) request);
        }

        public IUploadSyncDataResult UploadDataForClient(IUploadSyncDataRequest request)
        {
            return UploadDataForClient((UploadSyncDataRequest)request);
        }
    }
    
    public partial class GetSyncDataRequest : IGetSyncDataRequest
    {
    }

    public partial class GetSyncDataResult : IGetSyncDataResult
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public partial class UploadSyncDataRequest : IUploadSyncDataRequest
    {
        
        public IList<IUploadSyncQuestionData> questionsData { get; set; }
        public void AddQuestionData(IUploadSyncQuestionData questionData)
        {
            if (questionsData == null)
            {
                questionsData = new List<IUploadSyncQuestionData>();
            }

            questionsData.Add(questionData);
            this.answers = questionsData.Select(d => (UploadSyncQuestionAnwser) d).ToArray();
        }

        public IUploadSyncQuestionData CreateQuestionData()
        {
            return new UploadSyncQuestionAnwser();
        }
    }

    public partial class UploadSyncQuestionAnwser : IUploadSyncQuestionData
    {
        string IUploadSyncQuestionData.questionId
        {
            get { return this.questionId; }
            set { this.questionId = value; }
        }
    }

    public partial class UploadSyncDataResult : IUploadSyncDataResult
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}