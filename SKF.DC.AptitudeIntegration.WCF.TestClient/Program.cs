﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Test;
using SKF.DC.AptitudeIntegration.WCF.TestClient.AptitudeIntegrationService;

namespace SKF.DC.AptitudeIntegration.WCF.TestClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (var test = UploadTest1.Create(new AptitudeIntegrationHttpClientBackend()))
            {
                //Run the test!
                test.Run();
            }
        }
    }
}
