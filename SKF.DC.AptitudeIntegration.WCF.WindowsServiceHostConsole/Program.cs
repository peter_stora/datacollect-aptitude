﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHostConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new AptitudeWindowsServiceConsole().Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unhandled exception: " + ex.ToString());
            }

            Console.WriteLine("Press any key to exit...");
            Console.Read();
        }
    }
}
