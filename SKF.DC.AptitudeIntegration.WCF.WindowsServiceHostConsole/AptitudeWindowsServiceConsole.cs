﻿using SKF.DC.AptitudeIntegration.WCF.WindowsServiceHost;

namespace SKF.DC.AptitudeIntegration.WCF.WindowsServiceHostConsole
{
    public class AptitudeWindowsServiceConsole : AptitudeWindowsService
    {
        protected override void StartService()
        {
            //Don't start the windows service...
            WindowsService.Start();
        }
    }
}