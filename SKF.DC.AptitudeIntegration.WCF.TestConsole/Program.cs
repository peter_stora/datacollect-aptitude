﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.DC.AptitudeIntegration.Test;

namespace SKF.DC.AptitudeIntegration.WCF.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var test = UploadTest1.Create(new AptitudeIntegrationInstanceBackend()))
            {
                //Run the test!
                test.Run();
            }
        }
    }
}
