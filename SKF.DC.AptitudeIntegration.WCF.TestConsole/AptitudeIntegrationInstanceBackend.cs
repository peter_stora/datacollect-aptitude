﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Configuration;
using SKF.DC.AptitudeIntegration.Connection;
using SKF.DC.AptitudeIntegration.Data;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.NLog.Dependency;
using SKF.DC.AptitudeIntegration.Test;
using SKF.DC.AptitudeIntegration.WCF.Dependency;

namespace SKF.DC.AptitudeIntegration.WCF.TestConsole
{
    /// <summary>
    /// Aptitude integration backend implemented with a local AptitudeIntegrationService instance, no remote connection is made, everything is run in memory as a service interface.
    /// </summary>
    public class AptitudeIntegrationInstanceBackend : IAptitudeIntegrationBackend
    {
        public IContainer Container { get; set; }

        public IAptitudeIntegrationService CreateAptitudeIntegrationService()
        {
            return Container.Resolve<AptitudeIntegrationService>();
            /*return new AptitudeIntegrationService(
                new MIConnectionManager(new MIClientFactory(new SinlgeMicrologInspectorDbFileDataConnectFactory())),
                new AppConfigConfiguration()
            );*/
        }

        public IUploadSyncDataRequest CreateUploadRequest()
        {
            return new UploadSyncDataRequest();
        }

        public IEnumerable<IDependencyModule> GetDependencyModules()
        {
            yield return new AptitudeIntegrationWcfDependencyModule();
            yield return new AptitudeIntegrationInstanceBackendDependencyModule();
            yield return new NLogLoggingDependencyModule();
        }
    }

    public class AptitudeIntegrationInstanceBackendDependencyModule : DependencyModule<AptitudeIntegrationInstanceBackendDependencyModule>
    {
        public override void RegisterDependencies(ContainerBuilder c)
        {
            //make it a singleton to simulate a real service deployment
            c.RegisterType<AptitudeIntegrationService>().As<AptitudeIntegrationService, IJsonAptitudeIntegrationService, IAptitudeIntegrationService>().SingleInstance();
        }
    }
}
