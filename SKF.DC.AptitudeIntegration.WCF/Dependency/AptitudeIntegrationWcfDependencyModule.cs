﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using SKF.DC.AptitudeIntegration.Connection;
using SKF.DC.AptitudeIntegration.Dependency;

namespace SKF.DC.AptitudeIntegration.WCF.Dependency
{
    public class AptitudeIntegrationWcfDependencyModule : DependencyModule<AptitudeIntegrationWcfDependencyModule>
    {
        public override void RegisterDependencies(ContainerBuilder c)
        {
            //Register the integration service!
            c.RegisterType<AptitudeIntegrationService>().As<AptitudeIntegrationService, IJsonAptitudeIntegrationService, IAptitudeIntegrationService>();

            //Register the MIConnectionManager as a singleton instance, should only be one handling the connections
            c.RegisterType<MIConnectionManager>().As<MIConnectionManager, IMIConnectionManager>().SingleInstance();
        }
    }
}