﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using SKF.DC.AptitudeIntegration.Client;
using SKF.DC.AptitudeIntegration.Configuration;
using SKF.DC.AptitudeIntegration.Connection;
using SKF.DC.AptitudeIntegration.Data;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.Logging;
using SKF.DC.AptitudeIntegration.RequestHandlers;

namespace SKF.DC.AptitudeIntegration.WCF
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AptitudeIntegrationService : SKF.DC.AptitudeIntegration.AptitudeIntegrationService, IJsonAptitudeIntegrationService
    {
        protected AptitudeIntegrationService()
            : this(Resolver.Get<IMIConnectionManager>(), Resolver.Get<IConfiguration>(), Resolver.Get<IUploadSyncDataRequestHandler>())
        {
            Logger = LoggerFactory.Get();

            Logger.Debug("AptitudeIntegrationService ctor");
        }

        private ILogger Logger { get; set; }

        public AptitudeIntegrationService(IMIConnectionManager connectionManager, IConfiguration config, IUploadSyncDataRequestHandler uploadSyncDataRequestHandler) 
            : base(connectionManager, config, uploadSyncDataRequestHandler)
        {
        }

        public void GetOptions()
        {
            Logger.Debug("GetOptions");
            // The data loaded in these headers should match whatever it is you support on the endpoint
            // for your application. 
            // For Origin: The "*" should really be a list of valid cross site domains for better security
            // For Methods: The list should be the list of support methods for the endpoint
            // For Allowed Headers: The list should be the supported header for your application
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");
        }

        #region IJsonAptitudeIntegrationService
        public GetSyncDataResult GetDataForClient(GetSyncDataRequest request)
        {
            //Set a new request id
            RequestContext.Begin(Guid.NewGuid(), request);

            Logger.Debug("GetDataForClient");

            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");

            GetSyncDataResult result;
            try
            {
                result = base.GetDataForClient(request) as GetSyncDataResult;
                result.requestId = RequestContext.Current.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UploadSyncDataResult UploadDataForClient(UploadSyncDataRequest request)
        {
            //Set a new request id
            RequestContext.Begin(Guid.NewGuid(), request);
            
            Logger.Debug("UploadSyncDataRequest");

            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");

            UploadSyncDataResult result;
            try
            {
                result = base.UploadDataForClient(request) as UploadSyncDataResult;
                result.requestId = RequestContext.Current.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

            
        }
        #endregion

        protected override IGetSyncDataResult CreateGetSyncDataResult()
        {
            return new GetSyncDataResult();
        }

        protected override IUploadSyncDataResult CreateUploadSyncDataResult()
        {
            return new UploadSyncDataResult();
        }
    }
}
