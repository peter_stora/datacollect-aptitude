﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    /// <summary>
    /// Inspector for service operation messages, handles http headers on incomming / outgoing requests to enable cross site access to the invoked operation!
    /// </summary>
    public class CrossSiteEnabledMessageInspector : IDispatchMessageInspector
    {
        /// <summary>
        /// The names of the operations for which to enable cross site access
        /// </summary>
        private List<string> _operationNames;

        public CrossSiteEnabledMessageInspector(List<OperationDescription> corsEnabledOperations)
        {
            _operationNames = corsEnabledOperations.Select(o => o.Name).ToList();
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            //Find the operation in the allowed operations for cross site access, and return  configured "origin" - which we will hanle in the reply logic!
            var httpProp = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            if (httpProp == null)
                return null;

            object operationName;
            request.Properties.TryGetValue(WebHttpDispatchOperationSelector.HttpOperationNamePropertyName, out operationName);

            if (operationName == null)
                return null;

            if (!_operationNames.Contains((string) operationName))
                return null;

            var origin = httpProp.Headers[HttpConstants.Origin];
            if (origin != null)
            {
                return origin;
            }

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            var origin = correlationState as string;
            if (origin == null)
                return;

            HttpResponseMessageProperty httpProp = null;
            if (reply.Properties.ContainsKey(HttpResponseMessageProperty.Name))
            {
                httpProp = (HttpResponseMessageProperty)reply.Properties[HttpResponseMessageProperty.Name];
            }
            else
            {
                httpProp = new HttpResponseMessageProperty();
                reply.Properties.Add(HttpResponseMessageProperty.Name, httpProp);
            }

            httpProp.Headers.Add(HttpConstants.AccessControlAllowOrigin, origin);
        }
    }
}