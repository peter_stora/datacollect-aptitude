using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    /// <summary>
    /// Service host implementation that enables cross site access to any service operations that are marked with the "CrossSiteEnabled" attribute.
    /// </summary>
    public class CrossSiteEnabledServiceHost : ServiceHost
    {
        private Type _contractType;

        public CrossSiteEnabledServiceHost(Type serviceType, Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            _contractType = GetContractType(serviceType);
        }

        public CrossSiteEnabledServiceHost(object singletonInstance, Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
            var serviceType = singletonInstance.GetType();
            _contractType = GetContractType(serviceType);
        }

        protected override void OnOpening()
        {
            var endpoint = Description.Endpoints.Where(e => e.Contract.ContractType == _contractType && e.Binding.GetType() == typeof (WebHttpBinding)).FirstOrDefault();
            if (endpoint == null)
            {
                endpoint = AddServiceEndpoint(_contractType, new WebHttpBinding(), "");
                endpoint.Behaviors.Add(new WebHttpBehavior());
            }
            
            //Get the operations marked with "CrossSiteEnable" attribute
            var crossSiteOperations = endpoint.Contract.Operations.Where(o => o.Behaviors.Find<CrossSiteEnabledAttribute>() != null).ToList();

            //Configure the operations
            ConfigureCrossSiteOperations(endpoint, crossSiteOperations);

            //Add cross site access behavior!
            endpoint.Behaviors.Add(new CrossSiteEnabledEndpointBehavior());

            base.OnOpening();
        }

        private Type GetContractType(Type serviceType)
        {
            if (HasServiceContract(serviceType))
                return serviceType;

            //Extract the possible service contracts defined for the service - only 1 implementation is supported!
            var contracts = serviceType.GetInterfaces().Where(i => HasServiceContract(i)).ToArray();
            if(contracts.Length == 0)
                throw new InvalidOperationException("Service type " + serviceType.FullName + " does not implement any interface decorated with the ServiceContractAttribute.");

            if (contracts.Length > 1)
                throw new InvalidOperationException("Service type " + serviceType.FullName + " implements multiple interfaces decorated with the ServiceContractAttribute, not supported by this factory.");
            
            return contracts[0];
        }

        private static bool HasServiceContract(Type type)
        {
            //Check for the ServiceContractAttribute attribute
            return Attribute.IsDefined(type, typeof(ServiceContractAttribute), false);
        }

        /// <summary>
        /// Configures the specified list of operations for cross site access.
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="operations"></param>
        private void ConfigureCrossSiteOperations(ServiceEndpoint endpoint, List<OperationDescription> operations)
        {
            var preFlightBehaviorsByUri = new Dictionary<string, PreflightOperationBehavior>(StringComparer.OrdinalIgnoreCase);

            foreach (var operation in operations)
            {
                //not needed for GET
                if (operation.Behaviors.Find<WebGetAttribute>() != null)
                    continue;

                //not needed for OneWay
                if (operation.IsOneWay)
                    continue;

                //Extract the TemplateUri 
                string originalUriTemplate;
                var initialWebInvokeAttr = operation.Behaviors.Find<WebInvokeAttribute>();
                if (initialWebInvokeAttr != null && initialWebInvokeAttr.UriTemplate != null)
                {
                    originalUriTemplate = NormalizeTemplate(initialWebInvokeAttr.UriTemplate);
                }
                else
                {
                    originalUriTemplate = operation.Name;
                }

                //Extract the method specified
                var initialMethod = initialWebInvokeAttr != null && initialWebInvokeAttr.Method != null ? initialWebInvokeAttr.Method : "POST";
                if (preFlightBehaviorsByUri.ContainsKey(originalUriTemplate))
                {
                    //Reuse the ORIGIN preflight operation if specified!
                    var operationBehavior = preFlightBehaviorsByUri[originalUriTemplate];
                    operationBehavior.AddAllowedMethod(initialMethod);
                }
                else
                {
                    //Configure the cross site access!
                    var contract = operation.DeclaringContract;
                    var preflightOperation = new OperationDescription(operation.Name + HttpConstants.PreflightSuffix, contract);
                    var inputMessage = new MessageDescription(operation.Messages[0].Action + HttpConstants.PreflightSuffix, MessageDirection.Input);
                    inputMessage.Body.Parts.Add(new MessagePartDescription("input", contract.Namespace) { Index = 0, Type = typeof(Message) });
                    preflightOperation.Messages.Add(inputMessage);
                    MessageDescription outputMessage = new MessageDescription(operation.Messages[1].Action + HttpConstants.PreflightSuffix, MessageDirection.Output);
                    outputMessage.Body.ReturnValue = new MessagePartDescription(preflightOperation.Name + "Return", contract.Namespace) { Type = typeof(Message) };
                    preflightOperation.Messages.Add(outputMessage);

                    var webInvokeAttr = new WebInvokeAttribute();
                    webInvokeAttr.UriTemplate = originalUriTemplate;
                    webInvokeAttr.Method = "OPTIONS";

                    preflightOperation.Behaviors.Add(webInvokeAttr);
                    preflightOperation.Behaviors.Add(new DataContractSerializerOperationBehavior(preflightOperation));
                    var preflightOperationBehavior = new PreflightOperationBehavior(preflightOperation);
                    preflightOperationBehavior.AddAllowedMethod(initialMethod);
                    preflightOperation.Behaviors.Add(preflightOperationBehavior);
                    preFlightBehaviorsByUri.Add(originalUriTemplate, preflightOperationBehavior);

                    contract.Operations.Add(preflightOperation);
                }
            }
        }

        private string NormalizeTemplate(string uriTemplate)
        {
            int queryIndex = uriTemplate.IndexOf('?');
            if (queryIndex >= 0)
            {
                // no query string used for this
                uriTemplate = uriTemplate.Substring(0, queryIndex);
            }

            int paramIndex;
            while ((paramIndex = uriTemplate.IndexOf('{')) >= 0)
            {
                // Replacing all named parameters with wildcards
                int endParamIndex = uriTemplate.IndexOf('}', paramIndex);
                if (endParamIndex >= 0)
                {
                    uriTemplate = uriTemplate.Substring(0, paramIndex) + '*' + uriTemplate.Substring(endParamIndex + 1);
                }
            }

            return uriTemplate;
        }
    }
}