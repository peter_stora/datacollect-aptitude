﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    /// <summary>
    /// Behavior that adds the cross site message inspector to all operations marked for cross site access
    /// </summary>
    public class CrossSiteEnabledEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            var crossSiteEnabledOperations = endpoint.Contract.Operations.Where(o => o.Behaviors.Find<CrossSiteEnabledAttribute>() != null).ToList();

            //Add the message inspector enabling cross site access!
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new CrossSiteEnabledMessageInspector(crossSiteEnabledOperations));
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }
}