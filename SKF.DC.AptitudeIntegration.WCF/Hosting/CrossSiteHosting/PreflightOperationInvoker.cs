﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    class PreflightOperationInvoker : IOperationInvoker
    {
        private string _replyAction;
        List<string> _allowedHttpMethods;
        
        public PreflightOperationInvoker(string replyAction, List<string> allowedHttpMethods)
        {
            _replyAction = replyAction;
            _allowedHttpMethods = allowedHttpMethods;
        }

        public object[] AllocateInputs()
        {
            return new object[1];
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            Message input = (Message)inputs[0];
            outputs = null;
            return HandlePreflight(input);
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            throw new NotSupportedException("Only synchronous invocation");
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            throw new NotSupportedException("Only synchronous invocation");
        }

        public bool IsSynchronous
        {
            get { return true; }
        }

        Message HandlePreflight(Message input)
        {
            var httpRequest = (HttpRequestMessageProperty)input.Properties[HttpRequestMessageProperty.Name];
            string origin = httpRequest.Headers[HttpConstants.Origin];
            string requestMethod = httpRequest.Headers[HttpConstants.AccessControlRequestMethod];
            string requestHeaders = httpRequest.Headers[HttpConstants.AccessControlRequestHeaders];

            var reply = Message.CreateMessage(MessageVersion.None, _replyAction);
            var httpResponse = new HttpResponseMessageProperty();
            reply.Properties.Add(HttpResponseMessageProperty.Name, httpResponse);

            httpResponse.SuppressEntityBody = true;
            httpResponse.StatusCode = HttpStatusCode.OK;
            if (origin != null)
            {
                httpResponse.Headers.Add(HttpConstants.AccessControlAllowOrigin, origin);
            }

            if (requestMethod != null && _allowedHttpMethods.Contains(requestMethod))
            {
                httpResponse.Headers.Add(HttpConstants.AccessControlAllowMethods, string.Join(",", _allowedHttpMethods));
            }

            if (requestHeaders != null)
            {
                httpResponse.Headers.Add(HttpConstants.AccessControlAllowHeaders, requestHeaders);
            }

            return reply;
        }
    }
}