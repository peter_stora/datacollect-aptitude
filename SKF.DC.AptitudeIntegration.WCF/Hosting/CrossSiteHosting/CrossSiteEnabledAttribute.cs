﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    /// <summary>
    /// Marker attribute for an service operation, telling us to enable cross site access! - note that we don't need any actual implementation of the IOperationBehavior - this is just a marker class.
    /// </summary>
    public class CrossSiteEnabledAttribute : Attribute, IOperationBehavior
    {
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
        }

        public void Validate(OperationDescription operationDescription)
        {
        }
    }
}