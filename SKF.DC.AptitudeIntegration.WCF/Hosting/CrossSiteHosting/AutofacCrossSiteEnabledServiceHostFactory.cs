﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Autofac;
using Autofac.Integration.Wcf;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.NLog.Dependency;
using SKF.DC.AptitudeIntegration.WCF.Dependency;

namespace SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting
{
    /// <summary>
    /// Service host factory that creates a servicehost enabling cross site access as well as bootstrapping Autofac container for resolving the service implementation.
    /// </summary>
    public class AutofacCrossSiteEnabledServiceHostFactory : AutofacServiceHostFactory
    {
        private IContainer _container;

        public AutofacCrossSiteEnabledServiceHostFactory()
        {
        }

        public virtual ServiceHost CreateServiceHost<TServiceHost>(Uri[] baseAddresses)
        {
            return CreateServiceHost(typeof(TServiceHost), new Uri[0]);
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            //Make sure to setup the Autofac container
            SetupContainer();

            //Use the cross site enabled service host
            return new CrossSiteEnabledServiceHost(serviceType, baseAddresses);
        }

        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            //Make sure to setup the Autofac container
            SetupContainer();

            //Use the base... should trigger one of the other Create methods
            return base.CreateServiceHost(constructorString, baseAddresses);
        }

        protected override ServiceHost CreateSingletonServiceHost(object singletonInstance, Uri[] baseAddresses)
        {
            if (singletonInstance == null)
                throw new ArgumentNullException("singletonInstance");
            
            //Make sure to setup the Autofac container
            SetupContainer();

            //Use the cross site enabled service host
            return new CrossSiteEnabledServiceHost(singletonInstance, baseAddresses);
        }

        /// <summary>
        /// Setups the Autofac container for the environment, either the one specified or creates a new one.
        /// </summary>
        /// <param name="container">The container to use</param>
        public virtual void SetupContainer(IContainer container = null)
        {
            if (container != null)
                _container = container;

            if (_container == null)
            {
                //Build the container if it doesn't exist!
                var builder = new ContainerBuilder();

                _container = BuildAutofacContainer(builder, new List<IDependencyModule>() {
                    new AptitudeIntegrationDependencyModule(),
                    new AptitudeIntegrationWcfDependencyModule(),
                    new NLogLoggingDependencyModule()
                });
            }

            if (_container != null)
            {
                //Set the new container!
                Container = _container;
            }
        }
        /// <summary>
        /// Builds the autofac container, registering any depencencies given by the specified dependency modules!
        /// </summary>
        /// <param name="builder">The Autofac container builder</param>
        /// <param name="dependencyModules">The dependency modules to register dependencies</param>
        /// <returns></returns>
        private IContainer BuildAutofacContainer(ContainerBuilder builder, IEnumerable<IDependencyModule> dependencyModules)
        {
            //Register dependencies for each of the modules!
            if (dependencyModules != null)
            {
                foreach (var dependencyModule in dependencyModules)
                {
                    //Delegate the registration of services
                    dependencyModule.RegisterDependencies(builder);
                }
            }

            //Build the container!
            return builder.Build();
        }
    }
}