﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Wcf;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.WCF.Dependency;
using SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting;

namespace SKF.DC.AptitudeIntegration.WCF
{
    public class AptitudeServiceHostFactory : AutofacCrossSiteEnabledServiceHostFactory
    {
    }
}