﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SKF.DC.AptitudeIntegration.WCF.Hosting.CrossSiteHosting;

namespace SKF.DC.AptitudeIntegration.WCF
{
    [ServiceContract]
    public interface IJsonAptitudeIntegrationService
    {
        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetDataForClient", Method = "POST", ResponseFormat = WebMessageFormat.Json), CrossSiteEnabled]
        GetSyncDataResult GetDataForClient(GetSyncDataRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UploadDataForClient", Method = "POST", ResponseFormat = WebMessageFormat.Json), CrossSiteEnabled]
        UploadSyncDataResult UploadDataForClient(UploadSyncDataRequest request);
    }

    [DataContract]
    public class Result : IResult
    {
        [DataMember]
        public Guid requestId { get; set; }

        [DataMember]
        public bool success { get; set; }

        [DataMember]
        public int errorCode { get; set; }

        [DataMember]
        public string errorMessage { get; set; }
    }

    [DataContract]
    public class Request : IRequest
    {
        [DataMember]
        public Guid clientId { get; set; }
        [DataMember]
        public Guid requestId { get; set; }
    }

    [DataContract]
    public class GetSyncDataRequest : Request, IGetSyncDataRequest
    {
    }

    [DataContract]
    public class GetSyncDataResult : Result, IGetSyncDataResult
    {
        [DataMember]
        public ISyncData data { get; set; }
    }

    [DataContract]
    public class UploadSyncDataRequest : Request, IUploadSyncDataRequest
    {
        [DataMember] public string formDefinitionId { get; set; }
        [DataMember] public string formDefinitionTitle { get; set; }

        [DataMember] public string userName { get; set; }
        [DataMember] public string userEmail { get; set; }

        [DataMember] public string userFirstName { get; set; }
        [DataMember] public string userLastName { get; set; }
        [DataMember] public string userFullName { get; set; }

        [DataMember] public int userGroupId { get; set; }
        [DataMember] public string userGroupTitle { get; set; }
        [DataMember] public string userGroupDescription { get; set; }

        [DataMember] public string title { get; set; }
        [DataMember] public string description { get; set; }
        [DataMember] public bool completed { get; set; }

        [DataMember] public List<UploadSyncQuestionAnwser> answers { get; set; }

        IList<IUploadSyncQuestionData> IUploadSyncDataRequest.questionsData
        {
            get
            {
                if (answers == null)
                    return null;

                return answers.Select(d=>(IUploadSyncQuestionData)d).ToList();
            }
            set
            {
                if (value == null)
                {
                    answers = null;
                    return;
                }

                answers = value.Select(d=>(UploadSyncQuestionAnwser)d).ToList();
            }
        }

        public void AddQuestionData(IUploadSyncQuestionData questionData)
        {
            answers.Add((UploadSyncQuestionAnwser)questionData);
        }

        public IUploadSyncQuestionData CreateQuestionData()
        {
            return new UploadSyncQuestionAnwser();
        }
    }

    [DataContract]
    public class UploadSyncQuestionAnwser : IUploadSyncQuestionData
    {
        [DataMember] public string questionId { get; set; }

        [DataMember] public string questionTitle { get; set; }
        [DataMember] public string questionQuestion { get; set; }
        [DataMember] public string categoryTitle { get; set; }
        [DataMember] public string sectionTitle { get; set; }
        
        [DataMember] public string answer { get; set; }
        
        [DataMember] public int answeredBy { get; set; }
        [DataMember] public long lastModified { get; set; }
    }

    [DataContract]
    public class UploadSyncSensorQuestionAnwser : UploadSyncQuestionAnwser
    {
        //currently not supported
    }

    [DataContract]
    public class UploadSyncDataResult : Result, IUploadSyncDataResult
    {
    }

    [DataContract]
    public class SyncData : ISyncData
    {
    }
}
