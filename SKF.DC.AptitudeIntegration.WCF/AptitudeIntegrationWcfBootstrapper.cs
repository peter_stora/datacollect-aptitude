﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SKF.DC.AptitudeIntegration.Dependency;
using SKF.DC.AptitudeIntegration.WCF.Dependency;

namespace SKF.DC.AptitudeIntegration.WCF
{
    public class AptitudeIntegrationWcfBootstrapper : AptitudeIntegrationBootstrapper
    {
        protected override void RegisterDependencies(ContainerBuilder builder, IEnumerable<IDependencyModule> dependencyModules)
        {
            //Register the required stuff from the AptitudeIntegration
            var modules = new List<IDependencyModule>(dependencyModules);
            modules.Add(new AptitudeIntegrationWcfDependencyModule());

            //Register any additional stuff
            base.RegisterDependencies(builder, modules);
        }
    }
}
